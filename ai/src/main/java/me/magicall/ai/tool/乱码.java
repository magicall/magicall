/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.ai.tool;

import cn.hutool.log.Log;
import com.google.common.collect.Lists;
import dev.langchain4j.agent.tool.Tool;
import dev.langchain4j.model.chat.ChatLanguageModel;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class 乱码 {
	private static final Log LOG = Log.get(乱码.class);

	private static final String[] COMMON_CHARSETS = {"GBK", StandardCharsets.UTF_8.name(),
			StandardCharsets.ISO_8859_1.name()};

	private final ChatLanguageModel model;
	private final List<String> charsets;

	public 乱码(final ChatLanguageModel model) {
		this(model, Lists.newArrayList(COMMON_CHARSETS));
	}

	public 乱码(final ChatLanguageModel model, final List<String> charsets) {
		this.model = model;
		this.charsets = charsets;
	}

	@Tool("check whether a text is garbled/corrupted text")
	public boolean checkWhetherGarbled(final String s) {
		final var result = model.generate("判断以下文本是否乱码，仅返回true/false：" + s);
		LOG.debug("检查是否乱码" + s + " : " + result);
		return Boolean.parseBoolean(result.trim().toLowerCase());
	}

	@Tool("fixing garbled text, repairing corrupted text. return fixed text or raw text when fix failed")
	public String tryFix(final String s) {
		for (final String charset : charsets) {
			for (final String charset2 : charsets) {
				if (!charset2.equals(charset)) {
					try {
						final byte[] bs = s.getBytes(charset);
						final var reformatted = new String(bs, charset2);
						LOG.debug(String.join(":", "=====================", charset, charset2, reformatted));
						if (!checkWhetherGarbled(reformatted)) {
							LOG.debug("-----------不是乱码了！" + reformatted);
							return reformatted;
						}
					} catch (final UnsupportedEncodingException e) {
						throw new UnknownException(e);
					}
				}
			}
		}
		return s;
	}

	/**
	 * 尝试修复乱码。若非乱码或修复失败则原样返回。
	 *
	 * @return
	 */
	@Tool("return fixed text or raw text while it's not garbled text or fix failed")
	public String fix(final String s) {
		return checkWhetherGarbled(s) ? tryFix(s) : s;
	}
}
