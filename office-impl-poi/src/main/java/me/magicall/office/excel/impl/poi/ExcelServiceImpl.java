/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Excel;
import me.magicall.office.excel.ExcelService;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

//注意：本类中所有未标明的index都从1开始。从0开始的，名字会有base 0字样。
@Service
public class ExcelServiceImpl implements ExcelService {
	@Override
	public Excel create() {
		return new ExcelImpl(new XSSFWorkbook());
	}

	@Override
	public Excel open(final InputStream inputStream) {
		try {
			return new ExcelImpl(new XSSFWorkbook(inputStream));
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
	}
}
