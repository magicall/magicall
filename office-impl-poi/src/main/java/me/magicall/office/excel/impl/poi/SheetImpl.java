/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Col;
import me.magicall.office.excel.Row;
import me.magicall.office.excel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

record SheetImpl(org.apache.poi.ss.usermodel.Sheet rawSheet, ExcelImpl excel) implements Sheet {
	@Override
	public String name() {
		return rawSheet.getSheetName();
	}

	private boolean hasNoRows() {
		return rawSheet.getPhysicalNumberOfRows() == 0;
	}

	@Override
	public Stream<Row> notEmptyRows() {
		if (hasNoRows()) {
			return Stream.empty();
		}
		return rowIndexes().mapToObj(this::rowAt);
	}

	@Override
	public Row rowAt(final int n) {
		//todo：处理 n<1
		return new RowImpl(this, n);
	}

	@Override
	public Row firstNotEmptyRow() {
		final int rowIndex = toBase1(rawSheet.getFirstRowNum());
		return rowIndex == 0 ? null : rowAt(rowIndex);
	}

	@Override
	public Row lastNotEmptyRow() {
		final int rowIndex = toBase1(rawSheet.getLastRowNum());
		return rowIndex == 0 ? null : rowAt(rowIndex);
	}

	@Override
	public Row appendRow(final Stream<?> contents) {
		final Row row = nextRowAfterLastRow();
		contents.forEach(row::append);
		return row;
	}

	@Override
	public Row insertRow(final int n, final Stream<?> contents) {
		return null;//todo
	}

	@Override
	public Stream<Col> notEmptyCols() {
		final int colsSize = notEmptyRows().mapToInt(Row::countCells).max().orElse(0);
		return IntStream.range(indexBase(), colsSize + 1).mapToObj(this::colAt);
	}

	@Override
	public Col colAt(final int n) {
		//todo：处理 n<1
		return new ColImpl(this, n);
	}

	@Override
	public Col insertCol(final int n, final Stream<?> contents) {
		final List<?> contentsList = contents.toList();
		final List<Row> rows = notEmptyRows().toList();
		final int contentsSize = contentsList.size();
		final int rowsSize = rows.size();
		if (contentsSize < rowsSize) {
			IntStream.range(0, rowsSize)//
					.forEach(i -> rows.get(i).insertCell(n).val(i < contentsSize ? contentsList.get(i) : ""));
		} else {
			final AtomicInteger i = new AtomicInteger(1);
			contentsList.forEach(content -> rowAt(i.getAndIncrement()).insertCell(n).val(content));
		}
		return colAt(n);
	}

	@Override
	public Sheet transpose() {
		return null;//todo
	}

	private IntStream rowIndexes() {
		final int endExclusive = lastRowIndex() + 1;
		return IntStream.range(indexBase(), endExclusive);
	}

	int firstRowIndex() {
		return toBase1(rawSheet.getFirstRowNum());
	}

	int lastRowIndex() {
		return toBase1(hasNoRows() ? -1 : rawSheet.getLastRowNum());
	}

	private Row nextRowAfterLastRow() {
		return nextRowAfter(lastRowIndex() + 1);
	}

	private Row nextRowAfter(final int index) {
		return rowAt(index);
	}

	boolean rawRowExistsAt(final int rowIndex) {
		return rawRowAt(rowIndex) != null;
	}

	private org.apache.poi.ss.usermodel.Row ensureRawRow(final int rowIndex) {
		final org.apache.poi.ss.usermodel.Row raw = rawRowAt(rowIndex);
		return raw == null ? rawSheet.createRow(toBase0(rowIndex)) : raw;
	}

	org.apache.poi.ss.usermodel.Row rawRowAt(final int rowIndex) {
		return rawSheet.getRow(toBase0(rowIndex));
	}

	Cell rawCellAt(final int rowIndex, final int colIndex) {
		if (rawRowExistsAt(rowIndex)) {
			return rawRowAt(rowIndex).getCell(toBase0(colIndex));
		}
		return null;
	}

	boolean cellExistsAt(final int rowIndex, final int colIndex) {
		return rawCellAt(rowIndex, colIndex) != null;
	}

	Cell ensureRawCellAt(final int rowIndex, final int colIndex) {
		if (!cellExistsAt(rowIndex, colIndex)) {
			final int colIndexBased0 = toBase0(colIndex);
			return ensureRawRow(rowIndex).getCell(colIndexBased0, MissingCellPolicy.CREATE_NULL_AS_BLANK);
		}
		return rawCellAt(rowIndex, colIndex);
	}

	int toBase0(final int n) {
		return excel.toBase0(n);
	}

	int toBase1(final int n) {
		return excel.toBase1(n);
	}
}//Sheet
