/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Cell;
import me.magicall.office.excel.Col;

import java.util.stream.IntStream;
import java.util.stream.Stream;

record ColImpl(SheetImpl sheet, int indexInSheet) implements Col {
	@Override
	public Stream<Cell> cells() {
		//有的行该列无值，所以列的长度未必等于行的数量。
		final int lastNotEmptyRowIndex = lastNotEmptyRowIndex();
		if (lastNotEmptyRowIndex == 0) {
			return Stream.empty();
		}
		return IntStream.range(sheet.indexBase(), lastNotEmptyRowIndex + 1)//
				.mapToObj(i -> sheet.rowAt(i).cellAt(indexInSheet));
	}

	@Override
	public boolean isEmpty() {
		return lastNotEmptyRowIndex() == 0;
	}

	@Override
	public Cell firstNotEmptyCell() {
		for (int rowIndex = sheet.firstRowIndex(); rowIndex <= sheet.lastRowIndex(); rowIndex++) {
			if (sheet.cellExistsAt(rowIndex, indexInSheet)) {
				return sheet.rowAt(rowIndex).cellAt(indexInSheet);
			}
		}
		return null;
	}

	@Override
	public Cell cellAt(final int n) {
		return sheet.rowAt(n).cellAt(indexInSheet);
	}

	@Override
	public Col append(final Object... contents) {
		int n = lastNotEmptyRowIndex();
		for (final Object content : contents) {
			n++;
			sheet.rowAt(n)//注意：本列最后一个格子所在行未必是sheet的最后一行。
					.cellAt(indexInSheet)//
					.val(content);
		}
		return this;
	}

	private int lastNotEmptyRowIndex() {//在本列中，找出最后一个原生格子的所在行。
		final int firstRowIndex = sheet.firstRowIndex();
		return IntStream.iterate(sheet.lastRowIndex(),//
						rowIndex -> rowIndex >= firstRowIndex,//
						rowIndex -> rowIndex - 1)//
				.filter(rowIndex -> sheet.cellExistsAt(rowIndex, indexInSheet))//
				.findFirst().orElse(0);
	}
}//Col
