/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Excel;
import me.magicall.office.excel.Sheet;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.stream.Stream;

record ExcelImpl(Workbook workbook) implements Excel, AutoCloseable {
	@Override
	public Stream<Sheet> sheets() {
		final Iterator<org.apache.poi.ss.usermodel.Sheet> iterator = workbook.sheetIterator();
		return CollKit.stream(iterator).map(e -> new SheetImpl(e, this));
	}

	int toBase0(final int n) {
		return n - indexBase();
	}

	int toBase1(final int n) {
		return n + indexBase();
	}

	@Override
	public Sheet sheetAt(final int n) {
		return trans(workbook.getSheetAt(toBase0(n)));
	}

	@Override
	public Sheet findSheetNamed(final String name) {
		return trans(workbook.getSheet(name));
	}

	@Override
	public Sheet createSheet(final String name) {
		return trans(workbook.createSheet(name));
	}

	@Override
	public Sheet createSheet() {
		return trans(workbook.createSheet());
	}

	@Override
	public void writeTo(final OutputStream outputStream) {
		try {
			workbook.write(outputStream);
		} catch (final IOException e) {
			//todo
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		workbook.close();
	}

	private Sheet trans(final org.apache.poi.ss.usermodel.Sheet rawSheet) {
		return rawSheet == null ? null : new SheetImpl(rawSheet, this);
	}
}//Excel
