/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Cell;
import me.magicall.office.excel.Row;

import java.util.stream.IntStream;
import java.util.stream.Stream;

record RowImpl(SheetImpl sheet, int indexInSheet) implements Row {
	@Override
	public Stream<Cell> cells() {
		if (rawRowNotExists()) {
			return Stream.empty();
		}
		final int endExclusive = lastCellIndex() + 1;
		return IntStream.range(sheet.indexBase(), endExclusive).mapToObj(this::cellAt);
	}

	@Override
	public Cell firstNotEmptyCell() {
		final int index = sheet.toBase1(sheet.rawRowAt(indexInSheet).getFirstCellNum());
		return index == 0 ? null : cellAt(index);
	}

	@Override
	public Cell cellAt(final int n) {
		return new CellImpl(sheet, indexInSheet, n);
	}

	@Override
	public Row append(final Object... contents) {
		int n = lastCellIndex();
		for (final Object content : contents) {
			n++;
			cellAt(n).val(content);
		}
		return this;
	}

	@Override
	public Cell insertCell(final int n) {
		final org.apache.poi.ss.usermodel.Row rawRow = sheet.rawRowAt(indexInSheet);
		final int based0 = sheet.toBase0(n);
		rawRow.shiftCellsRight(based0, countCells(), 1);
		rawRow.createCell(based0);
		return cellAt(n);
	}

	private int lastCellIndex() {
		if (rawRowNotExists()) {
			return 0;
		}
		//注意：poi的getLastCellNum()【从1开始】，所以先-1，让它变为基于0的计数模式。
		return sheet.toBase1(sheet.rawRowAt(indexInSheet).getLastCellNum() - 1);
	}

	private boolean rawRowNotExists() {
		return !sheet.rawRowExistsAt(indexInSheet);
	}
}//Row
