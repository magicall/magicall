/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel.impl.poi;

import me.magicall.office.excel.Cell;
import me.magicall.office.excel.Col;
import me.magicall.office.excel.Row;
import org.apache.poi.ss.usermodel.CellType;

import java.util.Date;

record CellImpl(SheetImpl sheet, int indexInRow, int indexInCol) implements Cell {
	@Override
	public <T> T val() {
		final org.apache.poi.ss.usermodel.Cell rawCell = rawCell();
		if (rawCell == null) {
			return null;
		}
		final CellType cellType = rawCell.getCellType();
		return (T) switch (cellType) {
			case BLANK -> "";
			case STRING -> rawCell.getStringCellValue();
			case NUMERIC -> rawCell.getNumericCellValue();
			case BOOLEAN -> rawCell.getBooleanCellValue();
			default ->
				//todo
					null;
		};
	}

	@Override
	public Cell val(final Object val) {
		final org.apache.poi.ss.usermodel.Cell rawCell = ensureRawCell();
		if (val instanceof Date) {
			rawCell.setCellValue((Date) val);
		} else if (val instanceof Number) {
			rawCell.setCellValue(((Number) val).doubleValue());
		} else if (val instanceof Boolean) {
			rawCell.setCellValue((Boolean) val);
		} else {
			rawCell.setCellValue(String.valueOf(val));
		}
		return this;
	}

	@Override
	public Row row() {
		return sheet.rowAt(indexInRow());
	}

	@Override
	public Col col() {
		return sheet.colAt(indexInCol());
	}

	private org.apache.poi.ss.usermodel.Cell rawCell() {
		return sheet.rawCellAt(indexInRow, indexInCol);
	}

	private org.apache.poi.ss.usermodel.Cell ensureRawCell() {
		return sheet.ensureRawCellAt(indexInRow, indexInCol);
	}
}//Cell
