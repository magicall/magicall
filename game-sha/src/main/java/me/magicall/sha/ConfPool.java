/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sha;

import java.util.HashMap;
import java.util.Map;

public class ConfPool {

	private final Map<String, Object> confMap = new HashMap<>();

	public int size() {
		return confMap.size();
	}

	public boolean isEmpty() {
		return confMap.isEmpty();
	}

	public boolean containsKey(final Object key) {
		return confMap.containsKey(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(final Object key) {
		return (T) confMap.get(key);
	}

	public void put(final String key, final Object value) {
		confMap.put(key, value);
	}

	@Override
	public String toString() {
		return confMap.toString();
	}
}
