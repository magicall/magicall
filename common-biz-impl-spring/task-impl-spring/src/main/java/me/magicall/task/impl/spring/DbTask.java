package me.magicall.task.impl.spring;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import me.magicall.biz.Const;

import java.time.Instant;

@Entity(name = "task")
class DbTask {
	@Id
	@Column(unique = true, nullable = false, insertable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	@Column(nullable = false)
	String name;
	@Column(length = Const.NOTE_LEN)
	String description;
	@Column(length = Const.STR_ID_LEN)
	String type;
	Double allCount;
	Double doneCount;
	Instant startTime;
	Instant endTime;
}
