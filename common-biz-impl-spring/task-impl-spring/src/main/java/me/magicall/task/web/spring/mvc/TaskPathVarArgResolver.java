/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.task.web.spring.mvc;

import me.magicall.biz.EntityCrudServices;
import me.magicall.biz.task.TaskEntity;
import me.magicall.biz.task.TaskServices;
import me.magicall.task.Task;
import me.magicall.web.spring.mvc.CustomArgumentResolver;
import me.magicall.web.spring.mvc.EntityPathVarArgResolver.StrIdEntityPathVarArgResolver;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class TaskPathVarArgResolver extends StrIdEntityPathVarArgResolver<TaskEntity>
		implements CustomArgumentResolver {
	private final TaskServices services;

	public TaskPathVarArgResolver(final TaskServices services) {
		this.services = services;
	}

	@Override
	protected Stream<Class<?>> supportTypes() {
		return Stream.of(TaskEntity.class, Task.class);
	}

	@Override
	protected EntityCrudServices<String, TaskEntity, ?> servicesFor(final MethodParameter parameter,
																																	final String id) {
		return services;
	}
}
