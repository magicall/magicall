package me.magicall.task.impl.spring;

import me.magicall.biz.Repo;
import me.magicall.biz.SimpleEntityServicesSupport;
import me.magicall.biz.task.TaskEntity;
import me.magicall.biz.task.TaskServices;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.stream.Stream;

@Service
class TaskServicesImpl implements TaskServices, SimpleEntityServicesSupport<String, TaskEntity, DbTask> {
	private final TaskRepo taskRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	TaskServicesImpl(final TaskRepo taskRepo, final PlanPublisher planPublisher, final Bulletin bulletin) {
		this.taskRepo = taskRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Repo<DbTask> repo() {
		return taskRepo;
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public Stream<DbTask> repoAll() {
		return SimpleEntityServicesSupport.super.repoAll()//
																						.sorted(Comparator.comparing(e -> e.id, Comparator.reverseOrder()));
	}

	@Override
	public TaskEntity dbToDomain(final DbTask entity) {
		return entity == null ? null : new TaskImpl(entity, taskRepo);
	}

	@Override
	public DbTask voToDb(final TaskEntity vo) {
		final var entity = new DbTask();
		entity.type = vo.type();
		entity.name = vo.name();
		entity.description = vo.description();
		entity.allCount = vo.allCount();
		entity.doneCount = vo.doneCount();
		entity.startTime = vo.startTime();
		entity.endTime = vo.endTime();
		return entity;
	}

	@Override
	public DbTask findDbEntityByDomain(final TaskEntity taskRecord) {
		final var id = taskRecord.id();
		return getEntityById(id);
	}

	@Override
	public TaskEntity checkExist(final TaskEntity vo) {
		final var id = vo.id();
		return dbToDomain(getEntityById(id));
	}

	private DbTask getEntityById(final String id) {
		return filterEntities(e -> String.valueOf(e.id).equals(id)).findFirst().orElse(null);
	}
}
