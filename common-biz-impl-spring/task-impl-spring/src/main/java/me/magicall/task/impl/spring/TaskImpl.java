package me.magicall.task.impl.spring;

import me.magicall.biz.task.TaskEntity;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;
import me.magicall.scope.Scope;
import me.magicall.task.Task;

import java.time.Instant;

class TaskImpl implements TaskEntity {
	private DbTask entity;
	private final TaskRepo taskRepo;

	TaskImpl(final DbTask entity, final TaskRepo taskRepo) {
		this.entity = entity;
		this.taskRepo = taskRepo;
	}

	@Override
	public void endAt(final Instant endTime) {
		if (endTime == null) {
			throw new NullValException("endTime");
		}
		if (!isEnded()) {
			entity.endTime = endTime;
			save();
		}
	}

	@Override
	public String id() {
		return String.valueOf(entity.id);
	}

	@Override
	public String name() {
		return entity.name;
	}

	@Override
	public String type() {
		return entity.type;
	}

	@Override
	public String description() {
		return Kits.STR.isEmpty(entity.description) ? TaskEntity.super.description() : entity.description;
	}

	@Override
	public Task describedAs(final String newDescription) {
		requireNotEnded();
		if (newDescription == null) {
			throw new NullValException("newDescription");
		}
		if (!newDescription.equals(entity.description)) {
			entity.description = newDescription;
			save();
		}
		return this;
	}

	@Override
	public double allCount() {
		return entity.allCount == null ? TaskEntity.super.allCount() : entity.allCount;
	}

	@Override
	public Task setAllCount(final double allCount) {
		requireNotEnded();
		if (entity.allCount == null || Kits.DOUBLE.equals(entity.allCount, allCount)) {
			entity.allCount = allCount;
			save();
		}
		return this;
	}

	@Override
	public double doneCount() {
		return entity.doneCount == null ? TaskEntity.super.doneCount() : entity.doneCount;
	}

	@Override
	public Task setDoneCount(final double doneCount) {
		requireNotEnded();
		if (entity.doneCount == null || Kits.DOUBLE.equals(entity.doneCount, doneCount)) {
			entity.doneCount = doneCount;
			save();
		}
		return this;
	}

	@Override
	public Instant endTime() {
		return entity.endTime;
	}

	@Override
	public Instant startTime() {
		return entity.startTime;
	}

	@Override
	public void finish() {
		entity.doneCount = entity.allCount;
		entity.endTime = Instant.now();
		save();
	}

	private void save() {
		entity = taskRepo.save(entity);
	}

	private void requireNotEnded() {
		if (isEnded()) {
			throw new WrongStatusException("isEnded", true, false);
		}
	}

	@Override
	public String toString() {
		return Task.str(this);
	}

	@Override
	public int hashCode() {
		return Task.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Task.eq(this, o);
	}

	@Override
	public Scope scope() {
		return null;//todo
	}
}
