package me.magicall.task.impl.spring;

import me.magicall.db.springjpa.CachedSpringJpaRepoSupport;

interface TaskRepo extends CachedSpringJpaRepoSupport<Long, DbTask> {
}
