/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag.impl.spring;

import me.magicall.Thing;
import me.magicall.biz.tag.TagEntity;
import me.magicall.program.lang.java.贵阳DearSun.exception.EmpStrException;
import me.magicall.scope.Scope;
import me.magicall.tag.Tag;
import me.magicall.tag.Tagging;

import java.time.Instant;
import java.util.Objects;
import java.util.stream.Stream;

class TagImpl implements TagEntity {
	private final TagServicesImpl services;
	DbTag entity;

	TagImpl(final TagServicesImpl services, final DbTag entity) {
		this.services = services;
		this.entity = entity;
	}

	@Override
	public String id() {
		return entity.id;
	}

	@Override
	public Scope scope() {
		return null;
	}

	@Override
	public String name() {
		return entity.name;
	}

	@Override
	public Tag renameTo(final String newName) {
		if (newName == null || newName.isBlank()) {
			throw new EmpStrException("name");
		}
		if (!newName.equals(entity.name)) {
			entity.name = newName;
			save();
		}
		return this;
	}

	@Override
	public String description() {
		return entity.description;
	}

	@Override
	public Tag describedAs(final String description) {
		final String toUse = Objects.requireNonNullElse(description, "");
		if (!toUse.equals(entity.description)) {
			entity.description = toUse;
			save();
		}
		return this;
	}

	@Override
	public TagImpl accept(final TagEntity newVal) {
		return (TagImpl) renameTo(newVal.name()).describedAs(newVal.description());
	}

	private void save() {
		entity = services.tagRepo.save(entity);
	}

	@Override
	public Stream<Tagging> taggings() {
		final var byTagId = services.taggingRepo.findByTagId(entity.id);
		return byTagId.stream().map(tag -> new TaggingImpl(tag, this));
	}

	@Override
	public Tag tag(final Stream<Thing> things) {
		final String id = entity.id;
		services.taggingRepo.saveAll(things.map(thing -> {
			final DbTagging e = new DbTagging();
			e.tagId = id;
			e.thingType = thing.type();
			e.thingId = thing.idInType();
			e.createTime = Instant.now();
			return e;
		}).toList());
		return this;
	}

	@Override
	public Tag untagThings(final Stream<Thing> things) {
		final String id = entity.id;
		final var myTaggings = services.taggingRepo.findByTagId(id);
		final var toDel = things.flatMap(thing ->//
																				 myTaggings.stream()//
																									 .filter(
																											 e -> Objects.equals(e.thingType, thing.type()) && Objects.equals(
																													 e.thingId, thing.idInType())));
		services.taggingRepo.del(toDel);
		return this;
	}

	@Override
	public Tag untagThingsOf(final String thingType) {
		services.taggingRepo.deleteByThingType(thingType);
		return this;
	}

	@Override
	public String toString() {
		return Tag.str(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Tag.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Tag.hash(this);
	}
}//TagImpl
