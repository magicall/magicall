/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag.impl.spring;

import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.tag.Tag;
import me.magicall.tag.Tagging;

import java.time.Instant;

class TaggingImpl implements Tagging {
	private final TagImpl tag;
	private final DbTagging dbTagging;

	TaggingImpl(final DbTagging dbTagging, final TagImpl tag) {
		this.tag = tag;
		this.dbTagging = dbTagging;
	}

	@Override
	public Tag tag() {
		return tag;
	}

	@Override
	public Thing against() {
		return new ThingDto(dbTagging.thingType, dbTagging.thingId);
	}

	@Override
	public Instant taggingTime() {
		return dbTagging.createTime;
	}

	@Override
	public String toString() {
		return Tagging.str(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Tagging.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Tagging.hash(this);
	}
}//TaggingImpl
