/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag.impl.spring;

import me.magicall.db.springjpa.CachedSpringJpaRepoSupport;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

interface TaggingRepo extends CachedSpringJpaRepoSupport<Long, DbTagging> {

	@Cacheable
	DbTagging findByTagIdAndThingTypeAndThingId(String tagId, String thingType, String thingId);

	@CacheEvict(allEntries = true)
	@Transactional
	void deleteByThingType(String thingType);

	List<DbTagging> findByTagId(String tagId);
}
