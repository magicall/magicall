/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag.impl.spring;

import me.magicall.biz.Repo;
import me.magicall.biz.SimpleEntityServicesSupport;
import me.magicall.biz.tag.TagEntity;
import me.magicall.biz.tag.TagServices;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import me.magicall.math.EncryptKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Liang Wenjian.
 */
@Service
class TagServicesImpl implements TagServices, SimpleEntityServicesSupport<String, TagEntity, DbTag> {
	final TagRepo tagRepo;
	final TaggingRepo taggingRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	@Autowired
	public TagServicesImpl(final TagRepo tagRepo, final TaggingRepo taggingRepo, final PlanPublisher planPublisher,
												 final Bulletin bulletin) {
		this.tagRepo = tagRepo;
		this.taggingRepo = taggingRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Repo<DbTag> repo() {
		return tagRepo;
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public TagEntity dbToDomain(final DbTag dbEntity) {
		return dbEntity == null ? null : new TagImpl(this, dbEntity);
	}

	@Override
	public DbTag voToDb(final TagEntity vo) {
		final DbTag entity = new DbTag();
		entity.id = EncryptKit.randomRadix62Uuid();
		//todo vo.scope();
		entity.name = vo.name();
		entity.description = vo.description();
		return entity;
	}

	@Override
	public DbTag findDbEntityByDomain(final TagEntity domainEntity) {
		if (domainEntity instanceof final TagImpl t) {
			return t.entity;
		}
		return filterEntities(e -> e.name.equals(domainEntity.name())).findFirst().orElse(null);
	}
}
