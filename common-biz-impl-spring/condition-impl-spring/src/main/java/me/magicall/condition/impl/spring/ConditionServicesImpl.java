/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition.impl.spring;

import com.google.common.collect.Maps;
import me.magicall.HasWeight;
import me.magicall.Thing;
import me.magicall.biz.Repo;
import me.magicall.biz.SimpleEntityServicesSupport;
import me.magicall.biz.condition.ConditionEntity;
import me.magicall.biz.condition.ConditionServices;
import me.magicall.condition.CommonConditionItemComparator;
import me.magicall.condition.ConditionGroup.ConditionPartsComparator;
import me.magicall.condition.ConditionItemComparator;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

@Service
class ConditionServicesImpl
		implements ConditionServices, SimpleEntityServicesSupport<String, ConditionEntity, DbCondition> {
	private final ConditionRepo conditionRepo;
	private final ConditionGroupRepo conditionGroupRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;
	private final Map<String, ConditionItemComparator> conditionItemComparatorMap = Maps.newHashMap();

	ConditionServicesImpl(final ConditionRepo conditionRepo, final ConditionGroupRepo conditionGroupRepo,
												final PlanPublisher planPublisher, final Bulletin bulletin,
												final List<ConditionItemComparator> conditionItemComparators) {
		this.conditionRepo = conditionRepo;
		this.conditionGroupRepo = conditionGroupRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
		Arrays.stream(CommonConditionItemComparator.values()).forEach(e -> conditionItemComparatorMap.put(e.name(), e));
		Arrays.stream(ConditionPartsComparator.values()).forEach(e -> conditionItemComparatorMap.put(e.name(), e));
		conditionItemComparators.forEach(e -> conditionItemComparatorMap.put(e.name(), e));
	}

	@Override
	public Stream<ConditionEntity> streaming() {
		return partsOf0(null);
	}

	@Override
	public Repo<DbCondition> repo() {
		return conditionRepo;
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public ConditionEntity dbToDomain(final DbCondition dbEntity) {
		return dbEntity == null ? null : new ConditionImpl(dbEntity, this);
	}

	@Override
	public DbCondition voToDb(final ConditionEntity vo) {
		final var rt = new DbCondition();
		final var left = vo.left();
		rt.leftIsRef = left.isRef();
		final var leftVal = left.val();
		if (leftVal instanceof final Thing leftValThing) {
			rt.leftType = leftValThing.type();
			rt.leftVal = String.valueOf(leftValThing.idInType());
		} else {
			throw new UnknownException();
		}
		final var right = vo.right();
		rt.rightIsRef = right.isRef();
		final var rightVal = right.val();
		if (rightVal instanceof final Thing rightValThing) {
			rt.rightType = rightValThing.type();
			rt.rightVal = String.valueOf(rightValThing.idInType());
		} else {
			throw new UnknownException();
		}
		rt.comparatorId = vo.comparator().name();
		return rt;
	}

	@Override
	public DbCondition findDbEntityByDomain(final ConditionEntity domainEntity) {
		final var id = domainEntity.id();
		if (id == null) {
			return null;
		}
		return conditionRepo.findById(Long.valueOf(id)).orElse(null);
	}

	ConditionItemComparator findComparator(final String comparatorId) {
		return conditionItemComparatorMap.get(comparatorId);
	}

	Stream<ConditionEntity> partsOf(final ConditionGroupImpl conditionGroup) {
		return partsOf0(conditionGroup.entity.id);
	}

	private Stream<ConditionEntity> partsOf0(final Long groupId) {
		final var conditionEntities = conditionRepo.all();
		final var groupEntities = conditionGroupRepo.all();
		return Stream.concat(//
						conditionEntities.stream()//
								.filter(e -> Objects.equals(e.groupId, groupId))//
								.map(e -> new ConditionImpl(e, this)),//
						groupEntities.stream()//
								.filter(e -> Objects.equals(e.parentId, groupId))//
								.map(e -> new ConditionGroupImpl(e, this)))//
				.sorted(HasWeight.comparator());
	}
}
