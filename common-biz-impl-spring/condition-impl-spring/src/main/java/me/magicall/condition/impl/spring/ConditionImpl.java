/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition.impl.spring;

import me.magicall.Thing;
import me.magicall.biz.condition.ConditionEntity;
import me.magicall.condition.ConditionItem;
import me.magicall.condition.ConditionItemComparator;

class ConditionImpl implements ConditionEntity {
	private final DbCondition entity;
	private final ConditionServicesImpl serviceImpl;

	ConditionImpl(final DbCondition entity, final ConditionServicesImpl serviceImpl) {
		this.entity = entity;
		this.serviceImpl = serviceImpl;
	}

	@Override
	public String id() {
		return String.valueOf(entity.id);
	}

	@Override
	public ConditionItem left() {
		return ConditionItem.of(entity.leftIsRef, Thing.of(entity.leftType, entity.leftVal));
	}

	@Override
	public ConditionItemComparator comparator() {
		return serviceImpl.findComparator(entity.comparatorId);
	}

	@Override
	public ConditionItem right() {
		return ConditionItem.of(entity.rightIsRef, Thing.of(entity.rightType, entity.rightVal));
	}

	@Override
	public float weight() {
		return entity.weight;
	}

	@Override
	public String toString() {
		return ConditionEntity.str(this);
	}

	@Override
	public int hashCode() {
		return ConditionEntity.hash(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return ConditionEntity.eq(this, obj);
	}
}
