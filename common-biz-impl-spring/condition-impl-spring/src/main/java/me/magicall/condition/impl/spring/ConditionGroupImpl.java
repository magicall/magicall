/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition.impl.spring;

import me.magicall.PartsLogicRelation;
import me.magicall.biz.condition.ConditionGroupEntity;
import me.magicall.condition.Condition;

import java.util.function.Function;
import java.util.stream.Stream;

class ConditionGroupImpl implements ConditionGroupEntity {
	final DbConditionGroup entity;
	private final ConditionServicesImpl serviceImpl;

	ConditionGroupImpl(final DbConditionGroup entity, final ConditionServicesImpl serviceImpl) {
		this.entity = entity;
		this.serviceImpl = serviceImpl;
	}

	@Override
	public String id() {
		return String.valueOf(entity.id);
	}

	@Override
	public Stream<Condition> parts() {
		return serviceImpl.partsOf(this).map(Function.identity());
	}

	@Override
	public PartsLogicRelation partsRelation() {
		return entity.partsRelation;
	}

	@Override
	public float weight() {
		return entity.weight;
	}

	@Override
	public String toString() {
		return ConditionGroupEntity.str(this);
	}

	@Override
	public int hashCode() {
		return ConditionGroupEntity.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return ConditionGroupEntity.eq(this, other);
	}
}
