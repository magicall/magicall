/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition.impl.spring;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import me.magicall.biz.Const;

@Entity(name = "condition")
class DbCondition {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, insertable = false, updatable = false)
	Long id;

	@Column(nullable = false)
	boolean leftIsRef;
	@Column(nullable = false)
	String leftType;
	@Column(nullable = false)
	String leftVal;

	@Column(nullable = false, length = Const.STR_ID_LEN)
	String comparatorId;

	@Column(nullable = false)
	boolean rightIsRef;
	@Column(nullable = false)
	String rightType;
	@Column(nullable = false)
	String rightVal;

	Long groupId;
	float weight;
}
