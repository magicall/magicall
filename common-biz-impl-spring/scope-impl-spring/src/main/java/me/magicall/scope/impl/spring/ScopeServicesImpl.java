/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope.impl.spring;

import me.magicall.biz.PersistentServicesSupport;
import me.magicall.biz.Repo;
import me.magicall.biz.scope.ScopeEntity;
import me.magicall.biz.scope.ScopeServices;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import me.magicall.program.lang.java.贵阳DearSun.exception.EmpStrException;
import me.magicall.scope.Scope;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
class ScopeServicesImpl implements ScopeServices, PersistentServicesSupport<ScopeEntity, Scope, DbScope> {
	final ScopeRepo scopeRepo;
	final ScopeContainingRepo scopeContainingRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	ScopeServicesImpl(final ScopeRepo scopeRepo, final ScopeContainingRepo scopeContainingRepo,
										final PlanPublisher planPublisher, final Bulletin bulletin) {
		this.scopeRepo = scopeRepo;
		this.scopeContainingRepo = scopeContainingRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Stream<ScopeEntity> roots() {
		final var scopes = scopeRepo.findAll();
		final var scopeContainingEntities = scopeContainingRepo.all();
		final var containedIds = scopeContainingEntities.stream()//
																										.map(e -> e.smallerId)//
																										.distinct().toList();
		return scopes.stream().filter(e -> !containedIds.contains(e.id)).map(this::dbToDomain);
	}

	@Override
	public Repo<DbScope> repo() {
		return scopeRepo;
	}

	@Override
	public ScopeEntity dbToDomain(final DbScope dbEntity) {
		return dbEntity == null ? null : new ScopeImpl(this, dbEntity);
	}

	@Override
	public DbScope voToDb(final Scope scope) {
		final var entity = new DbScope();
		entity.name = scope.name();//todo:暂时不保存关系。
		return entity;
	}

	@Override
	public DbScope findDbEntityByDomain(final ScopeEntity domainEntity) {
		if (domainEntity instanceof final ScopeImpl one) {
			return one.entity;
		}
		return filterEntities(e -> e.name.equals(domainEntity.name())).findFirst().orElse(null);
	}

	@Override
	public ScopeEntity checkExist(final Scope vo) {
		return null;//todo
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public ScopeEntity merge(final ScopeEntity exist, final Scope newValue) {
		final var newName = newValue.name();
		if (newName == null || newName.isBlank()) {
			throw new EmpStrException("name");
		}
		final var entity = ((ScopeImpl) exist).entity;
		if (newName.equals(exist.name())) {
			return exist;
		}
		entity.name = newName;
		return new ScopeImpl(this, scopeRepo.save(entity));
	}
}
