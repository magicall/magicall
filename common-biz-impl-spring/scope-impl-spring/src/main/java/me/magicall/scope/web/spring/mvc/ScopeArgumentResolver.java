/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope.web.spring.mvc;

import me.magicall.biz.EntityCrudServices;
import me.magicall.biz.scope.ScopeEntity;
import me.magicall.biz.scope.ScopeServices;
import me.magicall.scope.Scope;
import me.magicall.web.spring.mvc.EntityPathVarArgResolver.StrIdEntityPathVarArgResolver;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class ScopeArgumentResolver extends StrIdEntityPathVarArgResolver<ScopeEntity> {
	private final ScopeServices services;

	public ScopeArgumentResolver(final ScopeServices services) {
		this.services = services;
	}

	@Override
	protected Stream<Class<?>> supportTypes() {
		return Stream.of(ScopeEntity.class, Scope.class);
	}

	@Override
	protected EntityCrudServices<String, ScopeEntity, ?> servicesFor(final MethodParameter parameter, final String s) {
		return services;
	}
}
