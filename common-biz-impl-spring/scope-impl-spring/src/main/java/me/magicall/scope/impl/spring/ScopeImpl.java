/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope.impl.spring;

import me.magicall.biz.scope.ScopeEntity;
import me.magicall.scope.Scope;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

class ScopeImpl implements ScopeEntity {
	private final ScopeServicesImpl services;
	final DbScope entity;

	ScopeImpl(final ScopeServicesImpl services, final DbScope entity) {
		this.services = services;
		this.entity = entity;
	}

	@Override
	public String id() {
		return String.valueOf(entity.id);
	}

	@Override
	public Stream<Scope> biggerScopes() {
		final var scopeContainingEntities = services.scopeContainingRepo.all();
		final var myId = entity.id;
		final List<Long> biggerIds = scopeContainingEntities.stream()//
																												.filter(e -> myId.equals(e.smallerId))//
																												.map(e -> e.biggerId).toList();
		return findByIds(biggerIds).map(Function.identity());
	}

	@Override
	public Stream<Scope> smallerScopes() {
		final var scopeContainingEntities = services.scopeContainingRepo.all();
		final var myId = entity.id;
		final List<Long> smallerIds = scopeContainingEntities.stream()//
																												 .filter(e -> myId.equals(e.biggerId))//
																												 .map(e -> e.smallerId).toList();
		return findByIds(smallerIds).map(Function.identity());
	}

	private Stream<ScopeEntity> findByIds(final List<Long> ids) {
		final var allById = services.scopeRepo.findAllById(ids);
		return allById.stream().map(services::dbToDomain);
	}

	@Override
	public ScopeEntity capture(final Stream<Scope> others) {
		services.scopeContainingRepo.store(others.filter(other -> !isIncluding(other))//
																						 .map(other -> {
																							 final var ensured = services.ensure(other);
																							 final var e = new DbScopeContaining();
																							 e.smallerId = ((ScopeImpl) ensured).entity.id;
																							 e.biggerId = e.id;
																							 return e;
																						 }));
		return this;
	}

	@Override
	public String name() {
		return entity.name;
	}

	@Override
	public String toString() {
		return Scope.str(this);
	}

	@Override
	public int hashCode() {
		return ScopeEntity.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return ScopeEntity.eq(this, o);
	}
}
