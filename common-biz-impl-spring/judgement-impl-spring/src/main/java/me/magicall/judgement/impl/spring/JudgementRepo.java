/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judgement.impl.spring;

import me.magicall.db.springjpa.CachedSpringJpaRepoSupport;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

interface JudgementRepo extends CachedSpringJpaRepoSupport<Long, DbJudgement> {
	@Cacheable
	DbJudgement findFirstByAgainstTypeAndAgainstIdAndJudgeTypeAndJudgeIdOrderByIdDesc(String againstType,
																																										String againstId,
																																										String judgeType,
																																										String judgeId);

	@Cacheable
	List<DbJudgement> findByAgainstTypeAndAgainstIdOrderByCertaintyDescJudgeTimeDesc(String againstType,
																																									 String againstId);

	@Cacheable
	List<DbJudgement> findByCertaintyAndResultIdContains(float certainty, String semanticGroupId);
}
