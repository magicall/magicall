/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judgement.impl.spring;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import me.magicall.biz.Const;

import java.time.Instant;

@Entity(name = "judgement")
class DbJudgement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, insertable = false, updatable = false)
	Long id;
	@Column(length = Const.NAME_LEN, nullable = false)
	String judgeType;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String judgeId;
	@Column(length = Const.NAME_LEN, nullable = false)
	String againstType;
	@Column(nullable = false)
	String againstId;
	@Column(length = Const.NAME_LEN, nullable = false)
	String resultType;
	@Column(nullable = false)
	String resultId;
	@Column(nullable = false)
	float certainty;
	@Column(length = 1000, nullable = false)
	String bases = "";//todo:移走
	@Column(nullable = false)
	Instant judgeTime;
}
