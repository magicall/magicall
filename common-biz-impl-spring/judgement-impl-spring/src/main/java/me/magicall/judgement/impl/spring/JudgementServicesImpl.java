/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judgement.impl.spring;

import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.biz.PersistentServicesSupport;
import me.magicall.biz.Repo;
import me.magicall.biz.judgement.JudgementServices;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import me.magicall.judge.Judge;
import me.magicall.judge.Judgement;
import me.magicall.judge.JudgementDto;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
class JudgementServicesImpl implements JudgementServices,
		PersistentServicesSupport<Judgement<Thing, Thing>, Judgement<Thing, Thing>, DbJudgement> {
	private final JudgementRepo judgementRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	JudgementServicesImpl(final JudgementRepo judgementRepo, final PlanPublisher planPublisher, final Bulletin bulletin) {
		this.judgementRepo = judgementRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Repo<DbJudgement> repo() {
		return judgementRepo;
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public Stream<Judgement<Thing, Thing>> streaming() {
		return PersistentServicesSupport.super.streaming()//
				.sorted(Comparator.comparing(Judgement::judgeTime, Comparator.reverseOrder()));
	}

	@Override
	public Judgement<Thing, Thing> dbToDomain(final DbJudgement dbEntity) {
		return dbEntity == null ? null : new JudgementFromDb(dbEntity);
	}

	@Override
	public DbJudgement voToDb(final Judgement<Thing, Thing> vo) {
		final DbJudgement rt = new DbJudgement();
		final var against = vo.against();
		rt.againstType = against.type();
		rt.againstId = against.idInType();
		final var result = vo.result();
		rt.resultType = result.type();
		rt.resultId = result.idInType();
		rt.certainty = vo.certainty();
//		rt.bases = Helper.toJson(vo.bases().toList());
		final var judge = vo.getJudge();
		rt.judgeType = judge.type();
		rt.judgeId = judge.idInType();
		rt.judgeTime = vo.judgeTime();
		return rt;
	}

	@Override
	public Stream<Judgement<Thing, Thing>> judgementsAgainst(final Thing against) {
		final var entities = judgementRepo.findByAgainstTypeAndAgainstIdOrderByCertaintyDescJudgeTimeDesc(against.type(),
				against.idInType());
		return entities.stream().map(this::dbToDomain);
	}

	@Override
	public DbJudgement findDbEntityByDomain(final Judgement<Thing, Thing> domainEntity) {
		return domainEntity instanceof final JudgementFromDb d ? d.entity : filterEntities(e -> {
			final var against = domainEntity.against();
			final var judge = domainEntity.getJudge();
			return e.againstType.equals(against.type())//
						 && e.againstId.equals(against.idInType())//
						 && e.judgeTime.equals(domainEntity.judgeTime())//
						 && e.judgeType.equals(judge.type())//
						 && e.judgeId.equals(judge.idInType());
		}).findFirst().orElse(null);
	}

	@Override
	public Judgement<Thing, Thing> checkExist(final Judgement<Thing, Thing> vo) {
		final var judge = vo.getJudge();
		final var against = vo.against();
		return dbToDomain(
				judgementRepo.findFirstByAgainstTypeAndAgainstIdAndJudgeTypeAndJudgeIdOrderByIdDesc(against.type(),
						against.idInType(), judge.type(), judge.idInType()));
	}

	@Override
	public Judgement<Thing, Thing> merge(final Judgement<Thing, Thing> exist, final Judgement<Thing, Thing> newValue) {
		return exist.changeResult(newValue.result()).changeCertainty(newValue.certainty()).changeBases(newValue.bases());
	}

	@Override
	public Stream<Judgement<Thing, Thing>> judgementsAgainstBy(final Thing against, final Thing judge) {
		final var entity = judgementRepo.findFirstByAgainstTypeAndAgainstIdAndJudgeTypeAndJudgeIdOrderByIdDesc(
				against.type(), against.idInType(), judge.type(), judge.idInType());
		if (entity == null) {
			return Stream.empty();
		}
		return Stream.of(entity).map(this::dbToDomain);
	}

	//===============================

	private class DbJudge implements Judge<Thing, Thing> {
		private final String judgeType;
		private final String judgeId;

		DbJudge(final String judgeType, final String judgeId) {
			this.judgeType = judgeType;
			this.judgeId = judgeId;
		}

		@Override
		public Judgement<Thing, Thing> judge(final Thing against) {
			return filter(judgement -> judgement.getJudge().equals(this) && judgement.against().equals(against))//
					.findFirst().orElse(null);
		}

		@Override
		public boolean learn(final Judgement<Thing, Thing> judgementFromOther) {
			final var judgement = new JudgementDto<Thing, Thing>();
			judgement.judgeTime = Instant.now();
			judgement.judge = this;
			judgement.against = judgementFromOther.against();
			judgement.result = judgementFromOther.result();
			judgement.certainty = judgementFromOther.certainty();
			judgement.bases = judgementFromOther.bases().toList();
			ensure(judgement);
			return true;
		}

		@Override
		public boolean dropJudgementAgainst(final Thing against) {
			final var judgement = judge(against);
			if (judgement != null) {
				drop(judgement);
			}
			return true;
		}

		@Override
		public String type() {
			return judgeType;
		}

		@Override
		public String idInType() {
			return judgeId;
		}

		@Override
		public String toString() {
			return Thing.str(this);
		}

		@Override
		public int hashCode() {
			return Thing.hash(this);
		}

		@Override
		public boolean equals(final Object o) {
			return o instanceof Judge<?, ?> && Thing.eq(this, o);
		}
	}

	private class JudgementFromDb implements Judgement<Thing, Thing> {
		private DbJudgement entity;
		private List<Thing> bases;
		private final ThingDto against;
		private final DbJudge judge;
		private ThingDto result;

		private JudgementFromDb(final DbJudgement entity) {
			this.entity = entity;
			against = new ThingDto(entity.againstType, entity.againstId);
			judge = new DbJudge(entity.judgeType, entity.judgeId);
			result = new ThingDto(entity.resultType, entity.resultId);
			calBases();
		}

		private void calBases() {
			bases = List.of();// Helper.listFromJson(entity.bases, Thing.class);
		}

		@Override
		public JudgementFromDb changeResult(final Thing newResult) {
			final var type = newResult.type();
			final var idInType = newResult.idInType();
			if (!result.type.equals(type) || !result.idInType.equals(idInType)) {
				entity.resultType = type;
				entity.resultId = idInType;
				save();
				result = new ThingDto(type, idInType);
			}
			return this;
		}

		@Override
		public JudgementFromDb changeBases(final Stream<Thing> bases) {
			final var newBases = bases.toList();
//			entity.bases = Helper.toJson(newBases);
			save();
			this.bases = newBases;
			return this;
		}

		@Override
		public JudgementFromDb addBases(final Stream<Thing> bases) {
			if (bases.filter(e -> !this.bases.contains(e)).anyMatch(e -> this.bases.add(e))) {
				saveBases();
			}
			return this;
		}

		@Override
		public JudgementFromDb removeBasesThat(final Predicate<Thing> filter) {
			if (bases.removeIf(filter)) {
				saveBases();
			}
			return this;
		}

		@Override
		public JudgementFromDb changeCertainty(final float newCertainty) {
//			if (!Helper.eq(entity.certainty, newCertainty)) {
//				entity.certainty = newCertainty;
//				save();
//			}
			return this;
		}

		private void saveBases() {
//			entity.bases = Helper.toJson(bases);
			save();
			calBases();
		}

		private void save() {
			//判断时间由我（判断服务）自动维护，外部不可更改。——？是否有需要更改的需求？
			entity.judgeTime = Instant.now();
			entity = judgementRepo.save(entity);
		}

		@Override
		public Thing against() {
			return against;
		}

		@Override
		public Thing result() {
			return result;
		}

		@Override
		public boolean isNegative() {
			return entity.certainty < 0;
		}

		@Override
		public Judge<Thing, Thing> getJudge() {
			return judge;
		}

		@Override
		public Stream<Thing> bases() {
			return bases.stream();
		}

		@Override
		public float certainty() {
			return Math.abs(entity.certainty);
		}

		@Override
		public Instant judgeTime() {
			return entity.judgeTime;
		}

		@Override
		public boolean equals(final Object o) {
			return Judgement.eq(this, o);
		}

		@Override
		public int hashCode() {
			return Judgement.hash(this);
		}

		@Override
		public String toString() {
			return Judgement.str(this);
		}
	}
}
