/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.alias.impl.spring;

import me.magicall.Thing;
import me.magicall.alias.AliasGroup;
import me.magicall.biz.alias.AliasServices;
import me.magicall.biz.relation.RelationServices;
import me.magicall.relation.Relation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

/**
 * 底层使用关系模块实现。
 */
@Service
class AliasServicesImpl implements AliasServices {
	private final RelationServices relationServices;

	AliasServicesImpl(final RelationServices relationServices) {
		this.relationServices = relationServices;
	}

	@Override
	public AliasGroup aliasGroupOf(final Thing thing) {
		final List<Thing> outgoings = outgoings0(thing).toList();
		//若有出度，视为别名组的核心词。若无出度，通过入度找核心词。
		if (outgoings.isEmpty()) {
			final var incomings = incomings0(thing);
			final var first = incomings.findFirst();
			if (first.isPresent()) {
				final var core = first.get();
				final var relations = outgoings0(core);
				return new AliasGroupImpl(relationServices, Stream.concat(Stream.of(core), relations));
			} else {//没有别名
				return new AliasGroupImpl(relationServices, thing);
			}
		} else {//作为核心词
			return new AliasGroupImpl(relationServices, Stream.concat(Stream.of(thing), outgoings.stream()));
		}
	}

	@Override
	public AliasGroup makeAlias(final Stream<Thing> things) {
		return things.reduce(null,//
												 (aliasGroup, thing) -> aliasGroup == null ? aliasGroupOf(thing) : aliasGroup.addAliases(thing),
												 (aliasGroup, aliasGroup2) -> aliasGroup2);
	}

	//---------------------------------------------------

	private Stream<Thing> incomings0(final Thing to) {
		return relationServices.incomingsOf(Const.ALIAS_SCOPE, to).map(Relation::from);
	}

	private Stream<Thing> outgoings0(final Thing from) {
		return relationServices.outgoingsOf(Const.ALIAS_SCOPE, from).map(Relation::to);
	}
}
