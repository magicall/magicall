/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.alias.impl.spring;

import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.scope.Scope;
import me.magicall.scope.ScopeDto;

interface Const {
	String RESOURCE_NAME = "Alias";
	Thing LINK_PAYLOAD = new ThingDto("", RESOURCE_NAME);
	Scope ALIAS_SCOPE = new ScopeDto(RESOURCE_NAME);
}
