/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.alias.impl.spring;

import com.google.common.collect.Lists;
import me.magicall.Thing;
import me.magicall.alias.AliasGroup;
import me.magicall.biz.relation.RelationServices;
import me.magicall.relation.Relation;
import me.magicall.relation.RelationDto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class AliasGroupImpl implements AliasGroup {
	private final RelationServices relationServices;
	private final List<Thing> aliases;

	AliasGroupImpl(final RelationServices relationServices, final Stream<Thing> aliases) {
		this.relationServices = relationServices;
		this.aliases = aliases.collect(Collectors.toList());
	}

	AliasGroupImpl(final RelationServices relationServices, final Thing thing) {
		this.relationServices = relationServices;
		aliases = Lists.newArrayList(thing);
	}

	@Override
	public Stream<Thing> aliases() {
		return aliases.stream();
	}

	@Override
	public AliasGroup addAliases(final Stream<Thing> aliases) {
		final var core = core();
		aliases.filter(e -> !this.aliases.contains(e))//
					 .forEach(alias -> makeAlias(core, alias));
		return this;
	}

	@Override
	public AliasGroup join(final Stream<AliasGroup> otherGroups) {
		if (otherGroups == null) {
			return this;
		}
		otherGroups.forEach(otherGroup -> addAliases(otherGroup.aliases()));
		return this;
	}

	private void makeAlias(final Thing core, final Thing maybeOtherCore) {
		//先连接两者本身。
		makeAlias0(core, maybeOtherCore);
		//maybeOtherCore可能也是一个核心。若是，则把它本身的关系网改为挂在core上。
		final List<Relation> existOutgoings = relationServices.outgoingsOf(Const.ALIAS_SCOPE, maybeOtherCore).toList();
		if (!existOutgoings.isEmpty()) {
			relationServices.unrelate(existOutgoings.stream());
			existOutgoings.forEach(existOutgoing -> makeAlias0(core, existOutgoing.to()));
		}
		//maybeOtherCore可能被他人连接。找到它们，连在本core上。注意，对方也可能是一个核心，所以需要递归。
		relationServices.incomingsOf(Const.ALIAS_SCOPE, maybeOtherCore)//
										.forEach(existIncoming -> {
											relationServices.unrelate(existIncoming);
											makeAlias(core, existIncoming.from());
										});
	}

	private void makeAlias0(final Thing core, final Thing alias) {
		if (!core.equals(alias) && !aliases.contains(alias)) {
			final var relation = new RelationDto();
			relation.scope = Const.ALIAS_SCOPE;
			relation.from = core;
			relation.to = alias;
			relation.forwardPayload = Const.LINK_PAYLOAD;
			relation.backwardPayload = Const.LINK_PAYLOAD;
			relationServices.ensure(relation);
			aliases.add(alias);
		}
	}

	@Override
	public AliasGroup removeAliases(final Stream<Thing> aliases) {
		final var core = core();
		boolean removeCore = false;
		for (final var alias : aliases.toList()) {
			if (alias.equals(core)) {
				removeCore = true;
				continue;
			}
			//先在本集合中删除，若能删除说明有此别名，再去关系模块删掉关系。
			// 这里有并发问题：内存中可能有多个“相同”的AliasImpl，aliases列表可能不同步。
			if (this.aliases.remove(alias)) {
				unrelate(core, alias);
			}
		}
		if (removeCore) {
			if (this.aliases.size() > 1) {
				this.aliases.remove(core);
				final var newCore = core();
				this.aliases.forEach(remain -> {
					unrelate(core, remain);
					if (!remain.equals(newCore)) {
						unrelate(core, remain);
						makeAlias0(newCore, remain);
					}
				});
			}
		}
		return this;
	}

	private void unrelate(final Thing core, final Thing alias) {
		relationServices.unrelate(Const.ALIAS_SCOPE, core, alias, Const.LINK_PAYLOAD, Const.LINK_PAYLOAD);
	}

	private Thing core() {
		return aliases.get(0);
	}
}
