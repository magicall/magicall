/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.relation.impl.spring;

import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.math.MathKit;
import me.magicall.relation.Relation;
import me.magicall.scope.Scope;

class RelationImpl implements Relation {
	private final RelationServicesImpl services;
	private DbRelation entity;

	RelationImpl(final DbRelation entity, final RelationServicesImpl services) {
		this.services = services;
		this.entity = entity;
	}

	@Override
	public Scope scope() {
		return services.scopeServices.find(entity.scopeId);
	}

	@Override
	public Relation putIn(final Scope scope) {
		final var myScope = scope();
		if (myScope.equals(scope)) {
			return this;
		}
		return checkExistOrNot(myScope, scope, from(), to(), forwardPayload(), backwardPayload(),
													 () -> saveIfModified(ensureMyScopeId(scope)));
	}

	private boolean ensureMyScopeId(final Scope scope) {
		final var newScopeId = scope.name();
		if (!entity.scopeId.equals(newScopeId)) {
			entity.scopeId = newScopeId;
			return true;
		}
		return false;
	}

	@Override
	public Thing from() {
		return new ThingDto(entity.fromType, entity.fromId);
	}

	@Override
	public Relation from(final Thing from) {
		if (from().equals(from)) {
			return this;
		}
		final var scope = scope();
		return checkExistOrNot(scope, scope, from, to(), forwardPayload(), backwardPayload(),
													 () -> saveIfModified(ensureMyFromType(from), ensureMyFromId(from)));
	}

	private boolean ensureMyFromType(final Thing from) {
		final var newFromType = from.type();
		if (!entity.fromType.equals(newFromType)) {
			entity.fromType = newFromType;
			return true;
		}
		return false;
	}

	private boolean ensureMyFromId(final Thing from) {
		final var newFromId = from.idInType();
		if (!entity.fromId.equals(newFromId)) {
			entity.fromId = newFromId;
			return true;
		}
		return false;
	}

	@Override
	public Thing to() {
		return new ThingDto(entity.toType, entity.toId);
	}

	@Override
	public Relation to(final Thing to) {
		if (to().equals(to)) {
			return this;
		}
		final var scope = scope();
		return checkExistOrNot(scope, scope, from(), to, forwardPayload(), backwardPayload(),
													 () -> saveIfModified(ensureMyToType(), ensureMyToId()));
	}

	private boolean ensureMyToType() {
		final var newToType = from().type();
		if (!entity.toType.equals(newToType)) {
			entity.toType = newToType;
			return true;
		}
		return false;
	}

	private boolean ensureMyToId() {
		final var newToId = from().idInType();
		if (!entity.toId.equals(newToId)) {
			entity.toId = newToId;
			return true;
		}
		return false;
	}

	@Override
	public Thing forwardPayload() {
		return new ThingDto(entity.forwardPayloadType, entity.forwardPayloadId);
	}

	@Override
	public Relation forwardWith(final Thing forwardPayload) {
		if (forwardPayload().equals(forwardPayload)) {
			return this;
		}
		final var scope = scope();
		return checkExistOrNot(scope, scope, from(), to(), forwardPayload, backwardPayload(),
													 () -> saveIfModified(ensureMyForwardPayloadType(forwardPayload),
																								ensureMyForwardPayloadId(forwardPayload)));
	}

	private boolean ensureMyForwardPayloadType(final Thing forwardPayload) {
		final var newForwardType = forwardPayload.type();
		if (!entity.forwardPayloadType.equals(newForwardType)) {
			entity.forwardPayloadType = newForwardType;
			return true;
		}
		return false;
	}

	private boolean ensureMyForwardPayloadId(final Thing forwardPayload) {
		final var newForwardId = forwardPayload.idInType();
		if (!entity.forwardPayloadId.equals(newForwardId)) {
			entity.forwardPayloadId = newForwardId;
			return true;
		}
		return false;
	}

	@Override
	public Thing backwardPayload() {
		return new ThingDto(entity.backwardPayloadType, entity.backwardPayloadId);
	}

	@Override
	public Relation backwardWith(final Thing backwardPayload) {
		if (backwardPayload().equals(backwardPayload)) {
			return this;
		}
		final var scope = scope();
		return checkExistOrNot(scope, scope, from(), to(), forwardPayload(), backwardPayload,
													 () -> saveIfModified(ensureMyBackwardPayloadType(backwardPayload),
																								ensureMyBackwardPayloadId(backwardPayload)));
	}

	private boolean ensureMyBackwardPayloadType(final Thing backwardPayload) {
		final var newBackwardType = backwardPayload.type();
		if (!entity.backwardPayloadType.equals(newBackwardType)) {
			entity.backwardPayloadType = newBackwardType;
			return true;
		}
		return false;
	}

	private boolean ensureMyBackwardPayloadId(final Thing backwardPayload) {
		final var newBackwardId = backwardPayload.idInType();
		if (!entity.backwardPayloadId.equals(newBackwardId)) {
			entity.backwardPayloadId = newBackwardId;
			return true;
		}
		return false;
	}

	@Override
	public float weight() {
		return entity.weight;
	}

	@Override
	public Relation weight(final float weight) {
		return saveIfModified(ensureMyWeight(weight));
	}

	private boolean ensureMyWeight(final float weight) {
		if (!MathKit.eq(entity.weight, weight)) {
			entity.weight = weight;
			return true;
		}
		return false;
	}

	private Relation saveIfModified(final boolean... modified) {
		for (final boolean b : modified) {
			if (b) {
				entity = services.relationRepo.save(entity);
				return this;
			}
		}
		return this;
	}

	private Relation checkExistOrNot(final Scope myNowScope, final Scope expectScope, final Thing expectFrom,
																	 final Thing expectTo, final Thing expectForwardPayload,
																	 final Thing expectBackwardPayload, final Runnable opWhenAbsent) {
		services.scopeRelations.get(expectScope.name()).stream()
													 .map((DbRelation dbEntity) -> new RelationImpl(dbEntity, services))//
													 .filter(e -> Relation.check(e, expectScope, expectFrom, expectTo, expectForwardPayload,
																											 expectBackwardPayload))//
													 .findFirst()//
													 .ifPresentOrElse(exist -> {
														 services.unrelateWithScope(myNowScope, e -> entity.equals(e));
														 entity = exist.entity;
													 }, opWhenAbsent);
		return this;
	}

	@Override
	public String toString() {
		return Relation.str(this);
	}

	@Override
	public int hashCode() {
		return Relation.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Relation.eq(this, o);
	}
}//RelationImpl
