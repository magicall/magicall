/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.relation.impl.spring;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import me.magicall.Thing;
import me.magicall.biz.relation.RelationServices;
import me.magicall.biz.scope.ScopeServices;
import me.magicall.math.MathKit;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;
import me.magicall.relation.Relation;
import me.magicall.scope.Scope;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
class RelationServicesImpl implements RelationServices {
	private static final Comparator<DbRelation> WEIGHT_ASC = Comparator.comparing(e -> e.weight);
	final RelationRepo relationRepo;
	final ScopeServices scopeServices;
	final Multimap<String, DbRelation> scopeRelations;

	RelationServicesImpl(final RelationRepo relationRepo, final ScopeServices scopeServices) {
		this.relationRepo = relationRepo;
		this.scopeServices = scopeServices;
		scopeRelations = MultimapBuilder.hashKeys().treeSetValues(WEIGHT_ASC).build();
		relationRepo.all().stream().sorted(WEIGHT_ASC)//
								.forEach(e -> scopeRelations.put(e.scopeId, e));
	}

	@Override
	public Stream<Relation> relationsInScope(final Scope scope) {
		return scopeRelations.get(scope.name()).stream().map(this::entityToDomain);
	}

	@Override
	public Relation ensure(final Relation relation) {
		final Thing from = relation.from();
		if (from == null) {
			throw new NullValException("from");
		}
		final Thing to = relation.to();
		if (to == null) {
			throw new NullValException("to");
		}
		final Scope scope = Objects.requireNonNullElse(relation.scope(), Scope.EMPTY);
		final Thing forwardPayload = Objects.requireNonNullElse(relation.forwardPayload(), Thing.EMPTY);
		final Thing backwardPayload = Objects.requireNonNullElse(relation.backwardPayload(), Thing.EMPTY);
		final float weight = relation.weight();
		final var example = new DbRelation();
		example.scopeId = orEmpty(scope.name());//todo:名字可能不够唯一
		example.fromType = orEmpty(from.type());
		example.fromId = orEmpty(from.idInType());
		example.toType = orEmpty(to.type());
		example.toId = orEmpty(to.idInType());
		example.forwardPayloadType = orEmpty(forwardPayload.type());
		example.forwardPayloadId = orEmpty(forwardPayload.idInType());
		example.backwardPayloadType = orEmpty(backwardPayload.type());
		example.backwardPayloadId = orEmpty(backwardPayload.idInType());
		final var exist = relationRepo.findOne(Example.of(example));
		if (exist.isPresent()) {
			final DbRelation entity = exist.get();
			if (MathKit.eq(weight, entity.weight)) {
				return entityToDomain(entity);
			} else {
				entity.weight = weight;
				return entityToDomain(relationRepo.save(entity));
			}
		} else {
			example.weight = weight;
			final var saved = relationRepo.save(example);
			scopeRelations.get(saved.scopeId).add(saved);
			return entityToDomain(saved);
		}
	}

	@Override
	public void unrelate(final Predicate<? super Relation> filter) {
		final var toDel = scopeRelations.values().stream()//
																		.filter(e -> filter.test(entityToDomain(e))).toList();
		relationRepo.del(toDel.stream());
		toDel.forEach(entity -> scopeRelations.remove(entity.scopeId, entity));
	}

	private static String orEmpty(final String s) {
		return Kits.STR.nullOrEmptyVal(s);
	}

	@Override
	public void unrelate(final Scope scope, final Thing from, final Thing to) {
		unrelateWithScope(scope, e -> Relation.checkFromTo(entityToDomain(e), from, to));
	}

	@Override
	public void unrelate(final Scope scope, final Thing from, final Thing to, final Thing forwardPayload,
											 final Thing backwardPayload) {
		unrelateWithScope(scope, e -> Relation.check(entityToDomain(e), scope, from, to, forwardPayload, backwardPayload));
	}

	@Override
	public void unrelate(final Relation relation) {
		unrelateWithScope(relation.scope(), e -> entityToDomain(e).equals(relation));
	}

	void unrelateWithScope(final Scope scope, final Predicate<DbRelation> filter) {
		final var relationEntities = scopeRelations.get(scope.name());
		if (!relationEntities.isEmpty()) {
			final var toDel = relationEntities.stream().filter(filter).toList();
			relationRepo.del(toDel.stream());
			relationEntities.removeAll(toDel);
		}
	}

	private RelationImpl entityToDomain(final DbRelation entity) {
		return new RelationImpl(entity, this);
	}
}
