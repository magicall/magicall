/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.relation.impl.spring;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import me.magicall.biz.Const;

@Entity(name = "relation")
class DbRelation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, insertable = false, updatable = false)
	Long id;
	@Column(length = Const.STR_ID_LEN, nullable = false, updatable = false)
	String scopeId;
	@Column(length = 20, nullable = false)
	String fromType;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String fromId;
	@Column(length = 20, nullable = false)
	String toType;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String toId;
	@Column(length = 20, nullable = false)
	String forwardPayloadType;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String forwardPayloadId;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String backwardPayloadType;
	@Column(length = Const.STR_ID_LEN, nullable = false)
	String backwardPayloadId;
	@Column(nullable = false)
	Float weight;
}
