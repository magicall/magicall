/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.meta.impl.spring;

import me.magicall.Identified;
import me.magicall.Op;
import me.magicall.Thing;
import me.magicall.biz.CrudServices;
import me.magicall.biz.meta.MetaServices;
import me.magicall.event.Notice;
import me.magicall.event.NoticeWatcher;
import me.magicall.meta.Meta;
import org.springframework.scheduling.annotation.Async;

/**
 * 默认不启用。需要自己注册。
 */
public class CreateMetaAfterThingCreated implements NoticeWatcher<Object, Object> {
	private final MetaServices services;

	public CreateMetaAfterThingCreated(final MetaServices services) {
		this.services = services;
	}

	@Override
	public boolean isInterestedIn(final Notice<?, ?> event) {
		return event.getOp() == Op.ADD && !(event.getResult() instanceof Meta);
	}

	@Async
	@Override
	public void doAfter(final Notice<Object, Object> event) {
		final var target = event.getTarget();
		if (target == null) {
			return;
		}
		final var targetOwner = target.owner();
		if (targetOwner == null) {
			return;
		}
		final var result = event.getResult();
		if (result == null) {
			return;
		}

		final String ownerType;
		final String ownerId;
		final String thingType;
		if (targetOwner instanceof final CrudServices s) {
			thingType = s.resourceName();
			ownerType = "Services";
			ownerId = s.getClass().getSimpleName();
		} else {
			thingType = "";
			ownerType = "";
			ownerId = "";
		}
		final String thingId;
		if (result instanceof final Identified<?> identified) {
			thingId = identified.id().toString();
		} else {
			thingId = Thing.UNKNOWN_SIGN;
		}
		services.create(Thing.of(ownerType, ownerId), Thing.of(thingType, thingId));//todo:应当用event.happenTime。等统一内部时间类型。
	}
}
