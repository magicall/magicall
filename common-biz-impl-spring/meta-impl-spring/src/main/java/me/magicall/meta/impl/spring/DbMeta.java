/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.meta.impl.spring;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.biz.Const;
import me.magicall.meta.Meta;

import java.time.Instant;

@Entity(name = "meta")
class DbMeta implements Meta {
	@Id
	@Column(nullable = false, updatable = false)
	String id;
	@Column(length = Const.STR_ID_LEN, nullable = false, updatable = false)
	String thingType;
	@Column(length = Const.STR_ID_LEN, nullable = false, updatable = false)
	String thingId;
	@Column(length = Const.STR_ID_LEN, nullable = false, updatable = false)
	String creatorId;
	@Column(nullable = false, updatable = false)
	Instant createTime;

	@Override
	public Thing owner() {
		return new ThingDto(thingType, thingId);
	}

	@Override
	public String creator() {
		return creatorId;
	}

	@Override
	public Instant createTime() {
		return createTime;
	}

	@Override
	public boolean equals(final Object o) {
		return Meta.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Meta.hash(this);
	}

	@Override
	public String toString() {
		return Meta.str(this);
	}
}
