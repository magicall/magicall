/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.meta.impl.spring;

import me.magicall.Identified;
import me.magicall.Thing;
import me.magicall.biz.CrudServices;
import me.magicall.biz.meta.MetaServices;
import me.magicall.math.EncryptKit;
import me.magicall.meta.Meta;
import me.magicall.厦门EdificeGate.user.CurUserHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
class MetaServicesImpl implements MetaServices {
	private final CurUserHolder curUserHolder;
	private final MetaRepo metaRepo;

	MetaServicesImpl(@Autowired(required = false) final CurUserHolder curUserHolder, final MetaRepo metaRepo) {
		this.curUserHolder = curUserHolder;
		this.metaRepo = metaRepo;
	}

	@Override
	public Meta create(final Thing thingOwner, final Thing createdThing, final Instant createTime) {
		if (!(createdThing instanceof final Identified<?> identified)) {
			return null;
		}
		final String thingType = thingOwner instanceof final CrudServices<?, ?> services//
														 ? services.resourceName() : createdThing.getClass().getSimpleName();
		final var thingId = identified.id().toString();
		//若已有，直接返回。不可修改，但也不报错（可选报错）。
		final var exist = findMetaOf(Thing.of(thingType, thingId));
		if (exist != null) {
			return exist;
		}
		final var entity = new DbMeta();
		entity.id = EncryptKit.randomRadix62Uuid();
		entity.thingType = thingType;
		entity.thingId = thingId;
		entity.createTime = createTime;
		entity.creatorId = curUserId();
		return metaRepo.saveAndFlush(entity);
	}

	private String curUserId() {
		if (curUserHolder == null) {
			return "";
		}
		final var currentUser = curUserHolder.curUser();
		if (currentUser == null) {
			return "";
		}
		return String.valueOf(currentUser.id());
	}

	@Override
	public Meta findMetaOf(final Thing thing) {
		final var id = thing.idInType();
		final var type = thing.type();
		if (id == null || type == null) {
			return null;
		}
		final var byThingType = metaRepo.findByThingType(type);
		return byThingType.stream().filter(e -> id.equals(e.thingId)).findFirst().orElse(null);
	}
}
