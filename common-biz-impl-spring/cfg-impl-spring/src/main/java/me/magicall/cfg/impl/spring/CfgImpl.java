/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cfg.impl.spring;

import me.magicall.biz.Const;
import me.magicall.biz.cfg.CfgEntity;
import me.magicall.cfg.Cfg;
import me.magicall.program.lang.java.贵阳DearSun.exception.EmpStrException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.scope.Scope;

import java.util.Objects;

class CfgImpl implements CfgEntity {
	private final CfgServicesImpl services;
	private DbCfg entity;

	CfgImpl(final DbCfg entity, final CfgServicesImpl services) {
		this.services = services;
		this.entity = entity;
	}

	@Override
	public String id() {
		return String.valueOf(entity.id);
	}

	@Override
	public String name() {
		return entity.name;
	}

	@Override
	public CfgImpl renameTo(final String newName) {
		if (newName == null) {
			throw new EmpStrException("name");
		}
		final String s = newName.strip();
		if (s.isEmpty()) {
			throw new EmpStrException("name");
		}
		if (s.length() > Const.STR_ID_LEN) {
			throw new WrongArgException("name`len", s.length(), Const.STR_ID_LEN);
		}
		if (!entity.name.equals(s)) {
			entity.name = s;
			save();
		}
		return this;
	}

	@Override
	public String description() {
		return entity.description;
	}

	@Override
	public CfgImpl describedAs(final String description) {
		final String s = Objects.requireNonNullElse(description, "").strip();
		if (!entity.description.equals(s)) {
			entity.description = s;
			save();
		}
		return this;
	}

	@Override
	public Object val() {
		return entity.val;
	}

	@Override
	public CfgImpl withVal(final Object val) {
		final String s = CfgServicesImpl.valStr(val);
		if (!entity.val.equals(s)) {
			entity.val = s;
			save();
		}
		return this;
	}

	@Override
	public String note() {
		return entity.note;
	}

	@Override
	public CfgImpl withNote(final String note) {
		final String s = Objects.requireNonNullElse(note, "").strip();
		if (!entity.note.equals(s)) {
			entity.note = s;
			save();
		}
		return this;
	}

	private void save() {
		entity = services.cfgRepo.save(entity);
	}

	@Override
	public String toString() {
		return Cfg.str(this);
	}

	@Override
	public int hashCode() {
		return CfgEntity.hash(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return CfgEntity.eq(this, obj);
	}

	@Override
	public Scope scope() {
		return null;//todo
	}
}
