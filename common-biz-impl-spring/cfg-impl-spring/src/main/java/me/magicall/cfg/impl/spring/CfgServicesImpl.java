/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cfg.impl.spring;

import me.magicall.biz.PersistentServicesSupport;
import me.magicall.biz.Repo;
import me.magicall.biz.cfg.CfgEntity;
import me.magicall.biz.cfg.CfgServices;
import me.magicall.cfg.Cfg;
import me.magicall.event.Bulletin;
import me.magicall.event.PlanPublisher;
import org.springframework.stereotype.Service;

@Service
class CfgServicesImpl implements CfgServices, PersistentServicesSupport<CfgEntity, Cfg, DbCfg> {
	final CfgRepo cfgRepo;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	CfgServicesImpl(final CfgRepo cfgRepo, final PlanPublisher planPublisher, final Bulletin bulletin) {
		this.cfgRepo = cfgRepo;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Repo<DbCfg> repo() {
		return cfgRepo;
	}

	@Override
	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	@Override
	public Bulletin bulletin() {
		return bulletin;
	}

	@Override
	public CfgEntity dbToDomain(final DbCfg dbEntity) {
		return dbEntity == null ? null : new CfgImpl(dbEntity, this);
	}

	@Override
	public DbCfg voToDb(final Cfg vo) {
		final var entity = new DbCfg();
		entity.name = vo.name();
		entity.description = vo.description();
		entity.val = valStr(vo.val());
		entity.note = vo.note();
		return entity;
	}

	@Override
	public DbCfg findDbEntityByDomain(final CfgEntity domainEntity) {
		return findEntityByName(domainEntity.name());
	}

	@Override
	public CfgEntity checkExist(final Cfg vo) {
		return dbToDomain(findEntityByName(vo.name()));
	}

	private DbCfg findEntityByName(final String name) {
		return repo().all().stream().filter(e -> e.name.equals(name)).findFirst().orElse(null);
	}

	@Override
	public CfgEntity merge(final CfgEntity exist, final Cfg newValue) {
		return (CfgEntity) exist.renameTo(newValue.name())//
														.withVal(newValue.val())//
														.describedAs(newValue.description())//
														.withNote(newValue.note());
	}

	static String valStr(final Object val) {
		return val == null ? "" : val.toString().strip();
	}
}
