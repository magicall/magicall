/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support;

import java.util.stream.IntStream;

/**
 * @author Liang Wenjian.
 */
public class MemoryTest {
	public static void main(final String... args) throws Exception {
		final var runtime = Runtime.getRuntime();
		System.out.println(runtime.availableProcessors());
		System.out.println(runtime.maxMemory());
		System.out.println(runtime.totalMemory());
		System.out.println(runtime.freeMemory());
		test();
	}

	public static void test() {
		// 创建 1000 个 String

		// 先垃圾回收
		System.gc();

		final long start = Runtime.getRuntime().freeMemory();
		final String[] strA = IntStream.range(0, 1000).mapToObj(i -> "").toArray(String[]::new);

		// 快要计算的时,再清理一次
		//System.gc();

		final long end = Runtime.getRuntime().freeMemory();

		System.out.println("一个String对象占内存:" + (start - end) / 1000.0);
	}
}
