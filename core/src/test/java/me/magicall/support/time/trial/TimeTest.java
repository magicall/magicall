/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.time.trial;

/**
 * @author Liang Wenjian.
 */
public class TimeTest {
	/**
	 * 测试某方法的耗时
	 *
	 * @param times 运行次数
	 */
	static void test(final int times) {
		long sum = 0;
		long max = 0;
		for (var i = 0; i < times; i++) {
			final var start = System.currentTimeMillis();
			// 修改下面这行代码,将其改为你要测试的方法
			// Calendar c = Calendar.getInstance();
			// /
			final var end = System.currentTimeMillis();
			final var sub = end - start;
			if (sub > max) {
				max = sub;
			}
			sum += sub;
		}
	}
}
