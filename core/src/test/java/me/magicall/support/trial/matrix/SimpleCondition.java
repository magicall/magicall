/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

public class SimpleCondition<O, V> implements Condition {
	private final O object;
	private final PropHandler<O, V> propHandler;
	private final Comparison<V> comparison;
	private final Exp<V> refValExp;

	public SimpleCondition(final O object, final PropHandler<O, V> propHandler, final Comparison<V> comparison,
												 final Exp<V> refValExp) {
		this.object = object;
		this.propHandler = propHandler;
		this.comparison = comparison;
		this.refValExp = refValExp;
	}

	@Override
	public boolean isSatisfied() {
		return comparison.matches(propHandler.extract(object), refValExp.getVal());
	}

	O getObject() {
		return object;
	}

	PropHandler<O, V> getPropHandler() {
		return propHandler;
	}

	Comparison<V> getComparison() {
		return comparison;
	}

	Exp<V> getRefValExp() {
		return refValExp;
	}
}
