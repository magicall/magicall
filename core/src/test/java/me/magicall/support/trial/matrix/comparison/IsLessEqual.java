/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import java.util.Comparator;

import me.magicall.program.lang.java.贵阳DearSun.coll.CompareKit;

/**
 * @author Liang Wenjian.
 */
public class IsLessEqual<V> implements ComparingComparison<V>, ReverseSingleRefValComparison<V> {

	public static final IsLessEqual<Object> NATURE_ORDER = new IsLessEqual<>(CompareKit.HASH_CODE_COMPARATOR);

	private final SingleRefValComparison<V> raw;

	public IsLessEqual(final Comparator<V> comparator) {
		raw = new IsGreater<>(comparator);
	}

	@Override
	@SuppressWarnings("unchecked")
	public int compare(final V o1, final V o2) {
		return ((Comparator<V>) getRaw()).compare(o1, o2);
	}

	@SuppressWarnings("unchecked")
	public static <V> IsLessEqual<V> inNatureOrder() {
		return (IsLessEqual<V>) NATURE_ORDER;
	}

	@Override
	public SingleRefValComparison<V> getRaw() {
		return raw;
	}
}
