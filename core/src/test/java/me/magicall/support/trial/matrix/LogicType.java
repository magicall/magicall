/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import me.magicall.support.trial.matrix.comparison.ComparingEquals;
import me.magicall.support.trial.matrix.comparison.IsGreater;
import me.magicall.support.trial.matrix.comparison.IsGreaterEqual;
import me.magicall.support.trial.matrix.comparison.IsLess;
import me.magicall.support.trial.matrix.comparison.IsLessEqual;
import me.magicall.support.trial.matrix.comparison.ComparingNotEquals;

/**
 * todo:这个接口要怎么用？
 *
 * @author Liang Wenjian.
 */
interface LogicType {

	Collection<Class<? extends Comparison<?>>> availableComparisons();

	boolean accept(Class<?> clazz);

	boolean isContinuous();

	/**
	 * 常见离散类型，包括枚举、布尔。
	 * 同时，根据算法不同，也可以令它们（甚至没有实现Comparable的类型）在业务上具有离散性，比如“按首字母排序”、“按笔画多少”等算法可以令String具有离散性。
	 * 在{@link BitSetTool}中，连续类型的一层是所有可用离散值中一个确定的值。
	 */
	class CommonDiscreteType implements LogicType {

		public static final CommonDiscreteType INSTANCE = new CommonDiscreteType();

		private CommonDiscreteType() {
		}

		@Override
		public Collection<Class<? extends Comparison<?>>> availableComparisons() {
			return Lists.newArrayList();
		}

		@Override
		public boolean accept(final Class<?> clazz) {
			return clazz.isEnum() || clazz == Boolean.class || clazz == boolean.class;
		}

		@Override
		public boolean isContinuous() {
			return false;
		}
	}

	/**
	 * 常见连续类型，包括所有数值型和时间型。
	 * 值具有逻辑上的连续性的类型。
	 * 与Comparable没有强相关性，比如Boolean、Character、String等，只表示在编程领域的底层技术中可排序，在现实生活中不具备连续性。
	 * 同时，根据算法不同，也可以令它们（甚至没有实现Comparable的类型）在业务上具有连续性。
	 * 在{@link BitSetTool}中，连续类型的一层是一个区间。
	 */
	class CommonContinuousType implements LogicType {
		public static final CommonContinuousType INSTANCE = new CommonContinuousType();
		private static final List<Class<? extends Comparison<?>>> AVAILABLE_COMPARISION_CLASSES = (List) Lists.newArrayList(
				ComparingEquals.class, IsGreater.class, IsLess.class, IsGreaterEqual.class, IsLessEqual.class,
				ComparingNotEquals.class);
		private static final Class<?>[] CLASSES = {int.class, long.class, float.class, double.class, byte.class,
				short.class, Number.class, Date.class};

		private CommonContinuousType() {
		}

		@Override
		public Collection<Class<? extends Comparison<?>>> availableComparisons() {
			return AVAILABLE_COMPARISION_CLASSES;
		}

		@Override
		public boolean accept(final Class<?> clazz) {
			return Arrays.stream(CLASSES).anyMatch(aClass -> aClass.isAssignableFrom(clazz));
		}

		@Override
		public boolean isContinuous() {
			return true;
		}
	}

	class CharSequenceType implements LogicType {

		private Collection<Class<? extends Comparison<?>>> availableComparison;
		private boolean isContinuous;

		@Override
		public Collection<Class<? extends Comparison<?>>> availableComparisons() {
			return availableComparison;
		}

		@Override
		public boolean accept(final Class<?> clazz) {
			return CharSequence.class.isAssignableFrom(clazz);
		}

		@Override
		public boolean isContinuous() {
			return isContinuous;
		}
	}
}
