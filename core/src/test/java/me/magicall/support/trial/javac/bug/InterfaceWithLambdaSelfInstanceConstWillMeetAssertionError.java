/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.javac.bug;

import java.util.function.Function;

/**
 * @author Liang Wenjian
 */
@FunctionalInterface
public interface InterfaceWithLambdaSelfInstanceConstWillMeetAssertionError<R> extends Function<Integer, R> {

	/*
	声明下面这个TEST静态属性，编译时会出错，信息如下，其实什么也看不出来。
	Information:2018/11/28 15:46 - Compilation completed with 2 errors and 0 warnings in 3s 673ms
	Error:java: java.lang.AssertionError
	Error:java: 	at com.sun.tools.javac.util.Assert.error(Assert.java:126)
	见 https://bugs.java.com/bugdatabase/view_bug.do?bug_id=8198736
	*/
	//InterfaceWithLambdaSelfInstanceConstWillMeetAssertionError TEST = a -> null;

	R f(int i);

	@Override
	default R apply(final Integer a) {
		return null;
	}

	static void main(final String... args) {
		System.out.println("hello");
	}
}
