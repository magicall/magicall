/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import me.magicall.program.lang.java.贵阳DearSun.coll.BitSwitchers;
import me.magicall.support.trial.matrix.CompositeCondition.ConditionRelation;
import me.magicall.support.trial.matrix.comparison.ComparingComparison;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Liang Wenjian.
 */
public class BitSetTool<R> {
	private final List<R> source;
	private final Multimap<PropHandler<R, ?>, Object> propAndRefVals;

	private final Map<PropHandler<R, ?>, Shelf> shelves;

	public List<R> filter(final Condition condition) {
		if (condition instanceof SimpleCondition<?, ?>) {
			@SuppressWarnings("unchecked")
			final var simpleCondition = (SimpleCondition<R, ?>) condition;
			return filterSimpleCondition(simpleCondition);
		} else if (condition instanceof CompositeCondition) {
			final var compositeCondition = (CompositeCondition) condition;
			return filterCompositeCondition(compositeCondition);
		} else if (condition instanceof ReverseCondition) {
			return filterReverseCondition((ReverseCondition) condition);
		} else {
			throw new RuntimeException();//todo：想想怎么支持上。
		}
	}

	private List<R> filterSimpleCondition(final SimpleCondition<R, ?> simpleCondition) {
		final var propHandler = simpleCondition.getPropHandler();
		final var shelf = shelves.get(propHandler);
		final var refVal = simpleCondition.getRefValExp().getVal();
		final var comparison = simpleCondition.getComparison();
		return objs(shelf.findLayer(comparison, refVal));
	}

	private List<R> filterCompositeCondition(final CompositeCondition compositeCondition) {
		final var results = compositeCondition.getConditions().stream()//
				.map(this::filter).collect(Collectors.toList());
		if (compositeCondition.getRelation() == ConditionRelation.AND) {
			return results.stream().reduce(BitSetTool::retains).orElse(Collections.emptyList());
		} else {
			return results.stream().reduce(BitSetTool::addAll).orElse(Collections.emptyList());
		}
	}

	private List<R> filterReverseCondition(final ReverseCondition reverseCondition) {
		final var rawResult = filter(reverseCondition.reverse());
		return source.stream().filter(e -> !rawResult.contains(e)).collect(Collectors.toList());//todo:挫
	}

	private static <E> List<E> retains(final List<E> source, final List<E> other) {
		source.retainAll(other);
		return source;
	}

	private static <E, C extends Collection<E>> C addAll(final C source, final Collection<E> other) {
		source.addAll(other);
		return source;
	}

	public BitSetTool(final List<R> source, final Multimap<PropHandler<R, ?>, Object> propAndRefVals) {
		//todo
		final var propHandlers = propAndRefVals.keySet();
		shelves = Maps.newHashMap();

		buildShelves(propHandlers, propAndRefVals, source.size());
		fillShelves(source, propHandlers);

		this.source = source;
		this.propAndRefVals = propAndRefVals;
	}

	private void buildShelves(final Collection<PropHandler<R, ?>> propHandlers,
														final Multimap<PropHandler<R, ?>, Object> propAndRefVals, final int size) {
		//for (final PropHandler<R, ?> propHandler : propHandlers) {
		//	shelves.put(propHandler, new Shelf(propHandler, propAndRefVals.get(propHandler), size));
		//}
	}

	private void fillShelves(final List<R> source, final Collection<PropHandler<R, ?>> propHandlers) {
		//for (int i = 0; i < source.size(); i++) {
		//	for (final PropHandler<R, ?> propHandler : propHandlers) {
		//		shelves.get(propHandler).add(i, source.get(i));
		//	}
		//}
	}

	private List<R> objs(final BitSwitchers bitSwitchers) {
		return bitSwitchers.stream().map(source::get).collect(Collectors.toList());
	}
	//public void addRecords(final List<R> newRecords) {
	//	//add to shelves
	//	final BitSetTool<R> newOne = new BitSetTool<>(newRecords, propAndRefVals);
	//	for (final Entry<PropHandler<R, ?>, Shelf<R>> entry : shelves.entrySet()) {
	//		final Shelf newShelf = newOne.shelves.get(entry.getKey());
	//		entry.getValue().merge(newShelf);
	//	}
	//	source.addAll(newRecords);
	//}

	//public void delRecords(final List<R> records) {
	//	int i = 0;
	//	for (final Iterator<R> iterator = source.iterator(); iterator.hasNext(); ) {
	//		final R element = iterator.next();
	//		if (records.remove(element)) {
	//			iterator.remove();
	//			//remove from shelves
	//			for (final Entry<PropHandler<R, ?>, Shelf<R>> entry : shelves.entrySet()) {
	//				final Shelf shelf = entry.getValue();
	//				shelf.remove(i, element);
	//			}
	//		}
	//		i++;
	//	}
	//}

	//===============================================================

	/**
	 * 货架。一个货架是多个层（Layer），包括一些特殊的层，如值为null的层。
	 */
	private static class Shelf {
		BitSwitchers otherValLayer;
		NavigableMap<Object, BitSwitchers> valLayerMapping;

		//		@SuppressWarnings("unchecked")
//		Shelf(final PropHandler<R, ?> propHandler, final Collection<Object> refVals, final int size) {
//			final Comparator<Object> valComparator = (Comparator<Object>) propHandler.getFirstValComparator();
//			valLayerMapping = Maps.newTreeMap(valComparator);
//			refVals.forEach(refVal -> valLayerMapping.put(refVal, new Layer(source, size, propHandler.getPropClass())));
//			otherValLayer = new Layer<>(source, size, propHandler.getPropClass());
//			this.propHandler = propHandler;
//		}
		@SuppressWarnings({"rawtypes"})
		private BitSwitchers findLayer(final Comparison<?> comparison, final Object refVal) {
			final var refLayer = refVal == null ? otherValLayer : findLayerForNonNull(refVal);
			if (!(comparison instanceof ComparingComparison<?>)) {
				return refLayer;
			}
			final var rt = new BitSwitchers();
			rt.addAll(refLayer);
			final ComparingComparison raw = (ComparingComparison<?>) comparison;
			Stream.iterate(valLayerMapping.lowerKey(refVal), o -> o != null && raw.matches(o, refVal),
							o -> valLayerMapping.lowerKey(o)).map(o -> valLayerMapping.get(o)).filter(e -> e != refLayer)
					.forEach(rt::addAll);
			Stream.iterate(valLayerMapping.higherKey(refVal), o -> o != null && raw.matches(o, refVal),
							o -> valLayerMapping.higherKey(o)).map(o -> valLayerMapping.get(o)).filter(e -> e != refLayer)
					.forEach(rt::addAll);
			return rt;
		}

		private BitSwitchers findLayerForNonNull(final Object val) {
			final var nearestEntity = nearestEntity(val);
			if (nearestEntity == null) {
				return otherValLayer;
			} else {
				final var layer = nearestEntity.getValue();
				if (/*layer.hasRange || */val.equals(nearestEntity.getKey())) {
					return layer;
				} else {
					return otherValLayer;
				}
			}
		}

		private Entry<Object, BitSwitchers> nearestEntity(final Object val) {
			return valLayerMapping.floorEntry(val);
		}
	}//class Shelf
}
