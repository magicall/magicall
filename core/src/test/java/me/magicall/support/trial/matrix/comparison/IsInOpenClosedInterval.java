/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import java.util.Comparator;

import me.magicall.program.lang.java.贵阳DearSun.coll.CompareKit;

/**
 * @author Liang Wenjian.
 */
public class IsInOpenClosedInterval<V> extends IntervalComparison<V> {

	public static final IsInOpenClosedInterval<Object> NATURE_ORDER = new IsInOpenClosedInterval<>(
			CompareKit.HASH_CODE_COMPARATOR);

	public IsInOpenClosedInterval(final Comparator<V> comparator) {
		super(new IsGreater<>(comparator), new IsLessEqual<>(comparator), comparator);
	}

	@SuppressWarnings("unchecked")
	public static <V> IsInOpenClosedInterval<V> inNatureOrder() {
		return (IsInOpenClosedInterval<V>) NATURE_ORDER;
	}
}
