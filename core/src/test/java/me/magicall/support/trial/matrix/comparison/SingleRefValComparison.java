/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import com.google.common.collect.Range;
import me.magicall.support.trial.matrix.Comparison;

/**
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface SingleRefValComparison<V> extends Comparison<V> {

	Range<Integer> RANGE = Range.closed(1, 1);

	@Override
	boolean matches(final V target, final V refVal);

	@Override
	default SingleRefValComparison<V> reverse() {
		return (ReverseSingleRefValComparison<V>) () -> this;
	}
}
