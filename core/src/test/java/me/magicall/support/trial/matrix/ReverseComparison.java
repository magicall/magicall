/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

/**
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface ReverseComparison<V> extends Comparison<V> {

	@Override
	default boolean matches(final V left, final V right) {
		return !getRaw().matches(left, right);
	}

	@Override
	default Comparison<V> reverse() {
		return getRaw();
	}

	Comparison<V> getRaw();
}
