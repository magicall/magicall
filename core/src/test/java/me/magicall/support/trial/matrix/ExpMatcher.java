/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import java.util.List;
import java.util.function.Function;

@Deprecated
public class ExpMatcher<T, R> {

	private final Function<T, R> targetExp;
	private final Comparison<R> comparison;
	private final Function<T, List<R>> refValExp;

	public ExpMatcher(final Function<T, R> targetExp, final Comparison<R> comparison,
										final Function<T, List<R>> refValExp) {
		this.targetExp = targetExp;
		this.comparison = comparison;
		this.refValExp = refValExp;
	}

	public Function<T, R> getTargetExp() {
		return targetExp;
	}

	public Comparison<R> getComparison() {
		return comparison;
	}

	public Function<T, List<R>> getRefValExp() {
		return refValExp;
	}
}
