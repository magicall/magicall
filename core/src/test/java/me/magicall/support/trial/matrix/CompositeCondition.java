/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collection;

/**
 * 复合条件。
 */
public final class CompositeCondition implements Condition {
	private final Collection<Condition> conditions;
	private final ConditionRelation relation;

	private CompositeCondition(final ConditionRelation relation, final Collection<Condition> conditions) {
		this.conditions = conditions;
		this.relation = relation;
	}

	public CompositeCondition(final ConditionRelation relation, final Condition first, final Condition... others) {
		this(relation, Lists.newArrayListWithExpectedSize(others.length + 1));
		addAll(first);
		addAll(others);
	}

	private void addAll(final Condition... conditions) {
		getConditions().addAll(Arrays.asList(conditions));
	}

	@Override
	public Condition and(final Condition... others) {
		if (getRelation() == ConditionRelation.AND) {
			addAll(others);
			return this;
		}
		return Condition.super.or(others);
	}

	@Override
	public Condition or(final Condition... others) {
		if (getRelation() == ConditionRelation.OR) {
			addAll(others);
			return this;
		}
		return Condition.super.and(others);
	}

	@Override
	public boolean isSatisfied() {
		return getRelation() == ConditionRelation.AND ? getConditions().stream().allMatch(Condition::isSatisfied)
																									: getConditions().stream().anyMatch(Condition::isSatisfied);
	}

	public static CompositeCondition and(final Condition first, final Condition... others) {
		return new CompositeCondition(ConditionRelation.AND, first, others);
	}

	public static CompositeCondition or(final Condition first, final Condition... others) {
		return new CompositeCondition(ConditionRelation.OR, first, others);
	}

	public Collection<Condition> getConditions() {
		return conditions;
	}

	public ConditionRelation getRelation() {
		return relation;
	}

	public enum ConditionRelation {
		AND,
		OR
	}
}
