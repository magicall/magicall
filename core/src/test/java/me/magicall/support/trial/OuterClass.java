/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

@MyAnnotation
public class OuterClass {
	@MyAnnotation
	private static class InnerClass {
		@MyAnnotation
		private void innerMethod() {
		}
	}

	@MyAnnotation
	public static void main(final String... args) throws NoSuchMethodException, NoSuchFieldException {
		final Class<?> outerClass = OuterClass.class;
		final Class<?> innerClass = InnerClass.class;
		final Method innerMethod = innerClass.getDeclaredMethod("innerMethod");

		System.out.println("Outer Class: " + getTopLevelClass(outerClass));
		System.out.println("Inner Class: " + getTopLevelClass(innerClass));
		System.out.println("Inner Method: " + getTopLevelClass(innerMethod));
	}

	public static Class<?> getTopLevelClass(final AnnotatedElement element) {
		Class<?> declaringClass = null;

		if (element instanceof Class<?>) {
			declaringClass = (Class<?>) element;
		} else if (element instanceof Member) {
			declaringClass = ((Member) element).getDeclaringClass();
		}

		while (declaringClass != null && declaringClass.getDeclaringClass() != null) {
			declaringClass = declaringClass.getDeclaringClass();
		}

		return declaringClass;
	}
}