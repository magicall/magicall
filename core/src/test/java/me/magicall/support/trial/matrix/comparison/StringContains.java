/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import java.util.Comparator;
import java.util.stream.IntStream;

/**
 * @author Liang Wenjian.
 */
public class StringContains implements SingleRefValComparison<String> {

	public static final StringContains NATURE_ORDER = new StringContains(Comparator.naturalOrder());
	public static final StringContains IGNORE_CASE = new StringContains(Comparator.comparingInt(Character::toUpperCase));

	private final Comparator<Character> comparator;

	public StringContains(final Comparator<Character> comparator) {
		this.comparator = comparator;
	}

	@Override
	public boolean matches(final String target, final String refVal) {
		final var length = target.length();
		final var refLen = refVal.length();
		if (length < refLen) {
			return false;
		}
		final var firstChar = refVal.charAt(0);
		return IntStream.range(0, length - refLen).filter(i -> comparator.compare(firstChar, target.charAt(i)) == 0)
				.anyMatch(i -> matchRemain(refLen, refVal, target.substring(i)));
	}

	private boolean matchRemain(final int length, final String s1, final String s2) {
		return IntStream.range(1, length).noneMatch(i -> comparator.compare(s1.charAt(i), s2.charAt(i)) != 0);
	}
}
