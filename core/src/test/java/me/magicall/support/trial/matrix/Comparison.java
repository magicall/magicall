/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

/**
 * 比较符/比较算法/比较操作。判断左元是否匹配右元，其中两元是相同类型。
 * 与Comparator的区别是：Comparator在比较两者的“大小”；本类对象可以不比较大小，只比较两者是否“匹配”。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Comparison<V> extends Matcher<V, V> {

	default Comparison<V> reverse() {
		return (ReverseComparison<V>) () -> this;
	}
}
