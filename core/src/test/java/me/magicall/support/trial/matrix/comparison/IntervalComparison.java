/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import com.google.common.collect.Lists;

import java.util.Comparator;

/**
 * 区间条件。
 *
 * @author Liang Wenjian.
 */
abstract class IntervalComparison<V> extends CompositeComparison<V> implements ComparingComparison<V> {

	private final Comparator<V> comparator;

	IntervalComparison(final SingleRefValComparison<V> lowerCondition, final SingleRefValComparison<V> upperCondition,
										 final Comparator<V> comparator) {
		super(Lists.newArrayList(lowerCondition, upperCondition));
		this.comparator = comparator;
	}

	@Override
	public int compare(final V o1, final V o2) {
		return comparator.compare(o1, o2);
	}

	//@Override
	//public <R> Layer<R> findLayer(final Shelf shelf0, final List<?> refVals) {
	//	checkRefVals(refVals);
	//	final Layer<R> lowerLayer = conditions.get(0).findLayer(shelf0, refVals.subList(0, 1));
	//	final Layer<R> upperLayer = conditions.get(1).findLayer(shelf0, refVals.subList(1, 2));
	//	return lowerLayer.intersects(upperLayer);
	//}
}
