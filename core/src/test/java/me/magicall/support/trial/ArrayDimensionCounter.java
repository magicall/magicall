/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class ArrayDimensionCounter {

	public static void main(final String... args) {
		// 示例测试
		try {
			// 普通数组类型
			final Type intArrayType = int[].class;
			final Type stringArrayArrayType = String[][].class;

			// 泛型数组类型
			final Type genericArrayType = new TypeReference<List<String>[]>() {
			}.getType();
			final Type genericArrayArrayType = new TypeReference<List<String>[][][]>() {
			}.getType();

			System.out.println("int[] dimensions: " + ClassKit.arrDims(intArrayType)); // 输出 1
			System.out.println("String[][] dimensions: " + ClassKit.arrDims(stringArrayArrayType)); // 输出 2
			System.out.println("List<String>[] dimensions: " + ClassKit.arrDims(genericArrayType)); // 输出 1
			System.out.println("List<String>[][][] dimensions: " + ClassKit.arrDims(genericArrayArrayType)); // 输出 3

			final var method = TypeReference.class.getMethod("a");
			final Class<?> returnType = method.getReturnType();
			System.out.println(ClassKit.arrDims(returnType));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}

// 辅助类，用于获取泛型类型
abstract class TypeReference<T> {
	private final Type type;

	protected TypeReference() {
		type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public Type getType() {
		return type;
	}

	public T[][][][] a() {
		return null;
	}
}
