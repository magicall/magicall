/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

/**
 * 与PropertyDescriptor类似，针对属性的类，主要用于处理属性。
 *
 * @author Liang Wenjian.
 */
public class PropHandler<O, V> implements Function<O, V> {
	private final String propName;
	private final Class<O> propClass;
	private final boolean nullable;
	private final Function<O, V> valExtractor;

	private final List<Comparator<V>> valComparators;

	public PropHandler(final String propName, final Class<O> propClass, final Function<O, V> valExtractor,
										 final Comparator<? super V>... valComparators) {
		this(propName, propClass, true, valExtractor, valComparators);
	}

	public PropHandler(final String propName, final Class<O> propClass, final boolean nullable,
										 final Function<O, V> valExtractor, final Comparator<? super V>... valComparators) {
		this(propName, propClass, nullable, valExtractor, Lists.newArrayList(valComparators));
	}

	public PropHandler(final String propName, final Class<O> propClass, final boolean nullable,
										 final Function<O, V> valExtractor, final List<? extends Comparator<? super V>> valComparators) {
		this.propName = propName;
		this.propClass = propClass;
		this.nullable = nullable;
		this.valExtractor = valExtractor;
		this.valComparators = (List) valComparators;
	}

	public V extract(final O obj) {
		return getValExtractor().apply(obj);
	}

	@Override
	public V apply(final O o) {
		return extract(o);
	}

	public String getPropName() {
		return propName;
	}

	public Class<O> getPropClass() {
		return propClass;
	}

	public boolean isNullable() {
		return nullable;
	}

	public Function<O, V> getValExtractor() {
		return valExtractor;
	}

	public Collection<Comparator<V>> getValComparators() {
		return valComparators;
	}

	@Deprecated
	public Comparator<V> getFirstValComparator() {
		return valComparators.get(0);
	}

	@Override
	public String toString() {
		return "PropHandler";
	}

	//=========================================

	@SuppressWarnings("unchecked")
	public static <T, V extends Comparable<V>> PropHandler<T, V> comparableProp(final String propName,
																																							final Class<T> propClass,
																																							final boolean nullable,
																																							final Function<T, V> valExtractor) {
		return new PropHandler<>(propName, propClass, nullable, valExtractor, Comparator.naturalOrder());
	}

	@SuppressWarnings("unchecked")
	public static <T, V extends Comparable<V>> PropHandler<T, V> comparableProp(final String propName,
																																							final Class<T> propClass,
																																							final Function<T, V> valExtractor) {
		return new PropHandler<>(propName, propClass, valExtractor, Comparator.naturalOrder());
	}
}
