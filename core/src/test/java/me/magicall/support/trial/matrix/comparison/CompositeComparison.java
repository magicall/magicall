/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.support.trial.matrix.Comparison;

import java.util.List;

/**
 * @author Liang Wenjian.
 */
public abstract class CompositeComparison<V> implements Comparison<V> {

	protected final List<SingleRefValComparison<V>> conditions;

	protected CompositeComparison(final List<SingleRefValComparison<V>> conditions) {
		this.conditions = conditions;
	}

	protected void checkRefVals(final List<?> refVals) {
		if (refVals.size() < conditions.size()) {
			throw new WrongArgException("refVals.size", refVals.size(), conditions.size());
		}
	}

	@Override
	public boolean matches(final V left, final V right) {
		return conditions.stream().allMatch(condition -> condition.matches(left, right));
	}
}
