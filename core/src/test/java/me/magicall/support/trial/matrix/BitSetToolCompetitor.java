/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import java.util.Collection;
import java.util.List;

/**
 * @author Liang Wenjian.
 */
public class BitSetToolCompetitor<R> {
	private final List<R> source;

	public BitSetToolCompetitor(final List<R> source) {
		this.source = source;
	}

	public Collection<R> filter(final Collection<Matcher> matchers) {
//		return source.stream()
//				.filter(r -> {
//					for (final Matcher matcher : matchers) {
//						if (!matcher.matches(r)) {
//							return false;
//						}
//					}
//					return true;
//				})
//				.collect(Collectors.toList());todo
		return null;
	}
}
