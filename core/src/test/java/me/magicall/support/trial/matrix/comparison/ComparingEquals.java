/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix.comparison;

import java.util.Comparator;

import me.magicall.program.lang.java.贵阳DearSun.coll.CompareKit;

/**
 * @author Liang Wenjian.
 */
public class ComparingEquals<V> implements SingleRefValComparison<V>, ComparingComparison<V> {

	public static final ComparingEquals<Object> NATURE_ORDER = new ComparingEquals<>(CompareKit.HASH_CODE_COMPARATOR);

	private final Comparator<V> comparator;

	public ComparingEquals(final Comparator<V> comparator) {
		this.comparator = comparator;
	}

	@Override
	public boolean matches(final V target, final V refVal) {
		return compare(target, refVal) == 0;
	}

	@Override
	public int compare(final V o1, final V o2) {
		return comparator.compare(o1, o2);
	}

	@SuppressWarnings("unchecked")
	public static <V> ComparingEquals<V> inNatureOrder() {
		return (ComparingEquals<V>) NATURE_ORDER;
	}
}
