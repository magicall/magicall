/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

import com.google.common.collect.Lists;
import me.magicall.support.trial.matrix.comparison.ComparingEquals;

import java.util.List;
import java.util.function.Function;

/**
 * 条件。适用于任何规则系统。
 * 一个条件指定一个属性，指定一种比较符，指定一组参考值。
 *
 * @author Liang Wenjian.
 */
@Deprecated
class PropMatcher<O, V> extends ExpMatcher<O, V> {

	private final PropHandler<O, V> propHandler;

	public PropMatcher(final PropHandler<O, V> propHandler, final V... refVals) {
		this(propHandler, Lists.newArrayList(refVals));
	}

	public PropMatcher(final PropHandler<O, V> propHandler, final List<V> refVals) {
		this(propHandler, ComparingEquals.inNatureOrder(), refVals);
	}

	public PropMatcher(final PropHandler<O, V> propHandler, final Comparison<V> comparison, final V... refVals) {
		this(propHandler, comparison, Lists.newArrayList(refVals));
	}

	public PropMatcher(final PropHandler<O, V> propHandler, final Comparison<V> comparison, final List<V> refVals) {
		super(propHandler, comparison, new FixedListFunction<>(refVals));
		this.propHandler = propHandler;
	}

	public PropHandler<O, V> getPropHandler() {
		return propHandler;
	}

	private static class FixedListFunction<O, V> implements Function<O, List<V>> {

		final List<V> list;

		public FixedListFunction(final List<V> list) {
			this.list = list;
		}

		@Override
		public List<V> apply(final O o) {
			return list;
		}
	}
}
