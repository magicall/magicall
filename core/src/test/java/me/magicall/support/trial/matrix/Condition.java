/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix;

/**
 * 条件。所有上下文都已内含，在运行时能够产生一个运行时值，不是一个可以存储的配置对象。
 */
@FunctionalInterface
public interface Condition extends Exp<Boolean> {

	boolean isSatisfied();

	@Override
	default Boolean getVal() {
		return isSatisfied();
	}

	default Condition and(final Condition... others) {
		return CompositeCondition.and(this, others);
	}

	default Condition or(final Condition... others) {
		return CompositeCondition.or(this, others);
	}

	default Condition reverse() {
		return new ReverseCondition(this);
	}
}
