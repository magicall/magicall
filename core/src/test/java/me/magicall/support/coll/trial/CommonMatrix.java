/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.coll.trial;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import me.magicall.support.coll.trial.MatrixUtil.MatrixTemplate;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

/**
 * @author Liang Wenjian.
 */
public class CommonMatrix<E> extends MatrixTemplate<E>//
		implements Matrix<E> {

	private final List<List<E>> list;
	private final int columnCount;
	private final E holeValue;
	private List<List<E>> verticalList;
	private List<List<E>> horizontalLists;
	private volatile int curAddIndex;

	@SuppressWarnings("unchecked")
	public CommonMatrix(final int rowCount, final int columnCount, final E holeValue) {
		super();
		final var rows = (List<E>[]) new List<?>[rowCount];
		IntStream.range(0, rowCount).forEach(i -> rows[i] = (List<E>) Arrays.asList(new Object[columnCount]));
		list = Arrays.asList(rows);
		this.columnCount = columnCount;
		this.holeValue = holeValue;
		curAddIndex = 0;
	}

	public CommonMatrix(final int rowCount, final int columnCount) {
		this(rowCount, columnCount, null);
	}

	@Override
	public List<List<E>> verticalLists() {
		if (verticalList == null) {
			verticalList = new VerticalList();
		}
		return verticalList;
	}

	@Override
	public List<List<E>> horizontalLists() {
		if (horizontalLists == null) {
			horizontalLists = Kits.LIST.unmodifiable(list);
			return horizontalLists;
		} else {
			return horizontalLists;
		}
	}

	@Override
	public boolean add(final E element) {
		final var rowIndex = curAddIndex / columnCount;
		final var columnIndex = curAddIndex - rowIndex * columnCount;
		if (columnIndex == columnCount) {
			if (rowIndex == list.size()) {
				return false;
			} else {
				set(rowIndex + 1, 0, element);
			}
		} else {
			set(rowIndex, columnIndex + 1, element);
		}
		curAddIndex++;
		return true;
	}

	@Override
	public boolean remove(final Object obj) {
		for (final var row : list) {
			for (final var i = row.listIterator(); i.hasNext(); ) {
				final var e = i.next();
				if (obj == e) {
					i.set(getHoleValue());
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty() || list.get(0).isEmpty();
	}

	@Override
	public boolean removeAll(final Collection<?> collection) {
		list.forEach(row -> {
			for (final var i = row.listIterator(); i.hasNext(); ) {
				final var e = i.next();
				if (collection.contains(e)) {
					i.set(getHoleValue());
				}
			}
		});
		return true;
	}

	@Override
	public boolean retainAll(final Collection<?> collection) {
		list.forEach(row -> {
			for (final var i = row.listIterator(); i.hasNext(); ) {
				final var e = i.next();
				if (!collection.contains(e)) {
					i.set(getHoleValue());
				}
			}
		});
		return true;
	}

	@Override
	public void clear() {
		list.clear();
	}

	@Override
	public E set(final int rowIndex, final int columnIndex, final E element) {
		return list.get(rowIndex).set(columnIndex, element);
	}

	@Override
	public E getHoleValue() {
		return holeValue;
	}

	private class ColumnList extends AbstractList<E> {

		final int columnIndex;

		public ColumnList(final int columnIndex) {
			super();
			this.columnIndex = columnIndex;
		}

		@Override
		public E get(final int index) {
			return CommonMatrix.this.get(index, columnIndex);
		}

		@Override
		public int size() {
			return list.size();
		}
	}

	private class VerticalList extends AbstractList<List<E>> {

		@SuppressWarnings("unchecked")
		List<List<E>> columns = Arrays.asList((List<E>[]) new List<?>[columnCount]);

		@Override
		public List<E> get(final int index) {
			var rt = columns.get(index);
			if (rt == null) {
				rt = new ColumnList(index);
				columns.set(index, rt);
			}
			return rt;
		}

		@Override
		public int size() {
			return columnCount;
		}
	}
}
