/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.trial;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Method;

public class MethodHandleTrial {

	private static final Super SUPER = new Super("super id 1");
	private static final Sub SUB = new Sub("sub id 2");

	public static void main(final String... args) throws Throwable {
//		identity();
//		findGetter();
//		overrideMethod();
//		defaultMethod();
//		_SuperClassImplementing();
		_SuperClassNotImplementing();
	}

	static void identity() throws Throwable {
		final var identity1 = MethodHandles.identity(Inter.class);
		System.out.println(identity1.toString());
		System.out.println(identity1.type());
		System.out.println(identity1.asFixedArity());

		final var bound = identity1.bindTo(SUPER);
		System.out.println(bound.invoke());
	}

	static void a() throws Throwable {
		final var method =//
				Super.class.getMethod("getId");
//		Super.class.getDeclaredMethod("getId");
		final var lookup = MethodHandles.lookup();
		final var in = lookup.in(Super.class);
		final var methodHandle = in.unreflectSpecial(method, Super.class);
		final var bound = methodHandle.bindTo(SUPER);
		final var invoke = bound.invoke();
		System.out.println(invoke);
	}

	static void findGetter() throws Throwable {
		final var lookup = MethodHandles.lookup();
		//findGetter并非去找getXx()方法，而是找字段，获取字段值。相当于Field类。
		//所以，即使有getXx()方法而没有字段，也会报错。
		//同时，即使getXx()没有返回xx的值而是返回另一个值，invoke得到的也是xx字段的值而非getXx()方法的返回值。
		final var locked =//
				lookup.findGetter(Sub.class, "locked", boolean.class);
		//比如这里打印的是Sub的locked字段的值false。而非Super类的isLocked方法的返回值，即使Sub类里没有覆盖此方法。
		System.out.println(locked.bindTo(SUB).invoke());
	}

	static void _SuperClassNotImplementing() throws NoSuchMethodException {
		System.out.println("================_SuperClassNotImplementing method===================");
		final var method = Inter.class.getMethod("_SuperClassNotImplementing");
		final var declaringClass = method.getDeclaringClass();
		//OK，落在Inter的default实现
		invoke(method, declaringClass, declaringClass, SUPER);
		invoke(method, declaringClass, declaringClass, SUB);
		//no private access for invokespecial: class me.magicall.support.lang.trial.MethodHandleTrial$Super, from me.magicall.support.lang.trial.MethodHandleTrial$Inter/private
		invoke(method, declaringClass, Super.class, SUPER);
		invoke(method, declaringClass, Super.class, SUB);
		//no private access for invokespecial，中间是Sub，后面是Inter
		invoke(method, declaringClass, Sub.class, SUPER);
		invoke(method, declaringClass, Sub.class, SUB);
		//no private access for invokespecial，中间是Inter，后面是Super
		invoke(method, Super.class, declaringClass, SUPER);
		invoke(method, Super.class, declaringClass, SUB);
		//OK,落在Inter的default实现
		invoke(method, Super.class, Super.class, SUPER);
		invoke(method, Super.class, Super.class, SUB);
		//no private access for invokespecial，中间是Sub，后面是Super
		invoke(method, Super.class, Sub.class, SUPER);
		invoke(method, Super.class, Sub.class, SUB);
		//no private access for invokespecial，中间是Inter，后面是Sub
		invoke(method, Sub.class, declaringClass, SUPER);
		invoke(method, Sub.class, declaringClass, SUB);
		//no private access for invokespecial，中间是Super，后面是Sub
		invoke(method, Sub.class, Super.class, SUPER);
		invoke(method, Sub.class, Super.class, SUB);
		//ClassCastException：Cannot cast Super to Sub
		invoke(method, Sub.class, Sub.class, SUPER);
		//OK，落在Inter的default实现。有点奇怪没有落在Sub的实现上。
		invoke(method, Sub.class, Sub.class, SUB);
		System.out.println("--------------------------------");
		//OK
		invoke(method, declaringClass, SUPER);//Inter
		invoke(method, declaringClass, SUB);//Sub
		invoke(method, declaringClass, SUPER);//Inter
		invoke(method, declaringClass, SUB);//Sub
		invoke(method, declaringClass, SUPER);//Inter
		invoke(method, declaringClass, SUB);//Sub

		invoke(method, Super.class, SUPER);//Inter
		invoke(method, Super.class, SUB);//Sub
		invoke(method, Super.class, SUPER);//Inter
		invoke(method, Super.class, SUB);//Sub
		invoke(method, Super.class, SUPER);//Inter
		invoke(method, Super.class, SUB);//Sub

		invoke(method, Sub.class, SUPER);//Inter
		invoke(method, Sub.class, SUB);//Sub
		invoke(method, Sub.class, SUPER);//Inter
		invoke(method, Sub.class, SUB);//Sub
		invoke(method, Sub.class, SUPER);//Inter
		invoke(method, Sub.class, SUB);//Sub
		//相当于直接调用_SuperClassNotImplementing(),对Super实例的调用最终会落到Inter里的default实现；对Sub实例调用则使用Sub类覆盖的方法。
		System.out.println("================end _SuperClassNotImplementing method===================");
	}

	static void _SuperClassImplementing() throws NoSuchMethodException {
		System.out.println("================_SuperClassImplementing method===================");
		final var method = Inter.class.getMethod("_SuperClassImplementing");
		final var declaringClass = method.getDeclaringClass();
		//这两个AbstractMethodError，message信息不明，基本上只是打印了方法签名。由于Inter上没有default实现，显然调用一个没实现的接口方法会报错。
		invoke(method, declaringClass, declaringClass, SUPER);
		invoke(method, declaringClass, declaringClass, SUB);
		//no private access for invokespecial: class me.magicall.support.lang.trial.MethodHandleTrial$Super, from me.magicall.support.lang.trial.MethodHandleTrial$Inter/private
		invoke(method, declaringClass, Super.class, SUPER);
		invoke(method, declaringClass, Super.class, SUB);
		//no private access for invokespecial，中间是Sub，后面是Inter
		invoke(method, declaringClass, Sub.class, SUPER);
		invoke(method, declaringClass, Sub.class, SUB);
		//no private access for invokespecial，中间是Inter，后面是Super
		invoke(method, Super.class, declaringClass, SUPER);
		invoke(method, Super.class, declaringClass, SUB);
		//AbstractMethodError
		invoke(method, Super.class, Super.class, SUPER);
		invoke(method, Super.class, Super.class, SUB);
//no private access for invokespecial，中间是Sub，后面是Super
		invoke(method, Super.class, Sub.class, SUPER);
		invoke(method, Super.class, Sub.class, SUB);
//no private access for invokespecial，中间是Inter，后面是Sub
		invoke(method, Sub.class, declaringClass, SUPER);
		invoke(method, Sub.class, declaringClass, SUB);
		//no private access for invokespecial，中间是Super，后面是Sub
		invoke(method, Sub.class, Super.class, SUPER);
		invoke(method, Sub.class, Super.class, SUB);
		//AbstractMethodError
		invoke(method, Sub.class, Sub.class, SUPER);
		invoke(method, Sub.class, Sub.class, SUB);
		System.out.println("--------------------------------");
		//以下全都OK。只有Super实现了，所以都落在Super的实现上。
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		System.out.println("================end _SuperClassImplementing method===================");
	}

	static void defaultMethod() throws NoSuchMethodException {
		System.out.println("================default method===================");
		final var method = Inter.class.getMethod("defaultImplementing");
		final var declaringClass = method.getDeclaringClass();
		//OK
		invoke(method, declaringClass, declaringClass, SUPER);
		invoke(method, declaringClass, declaringClass, SUB);
		//no private access for invokespecial: class me.magicall.support.lang.trial.MethodHandleTrial$Super, from me.magicall.support.lang.trial.MethodHandleTrial$Inter/private
		invoke(method, declaringClass, Super.class, SUPER);
		invoke(method, declaringClass, Super.class, SUB);
		//no private access for invokespecial，中间是Sub，后面是Inter
		invoke(method, declaringClass, Sub.class, SUPER);
		invoke(method, declaringClass, Sub.class, SUB);

		//no private access for invokespecial，中间Inter，后面Super
		invoke(method, Super.class, declaringClass, SUPER);
		invoke(method, Super.class, declaringClass, SUB);
		//OK
		invoke(method, Super.class, Super.class, SUPER);
		invoke(method, Super.class, Super.class, SUB);
		//no private access for invokespecial，中间是Sub，后面是Super
		invoke(method, Super.class, Sub.class, SUPER);
		invoke(method, Super.class, Sub.class, SUB);
		//no private access for invokespecial，中间是Inter，后面是Sub
		invoke(method, Sub.class, declaringClass, SUPER);
		invoke(method, Sub.class, declaringClass, SUB);
		//no private access for invokespecial，中间是Super，后面是Sub
		invoke(method, Sub.class, Super.class, SUPER);
		invoke(method, Sub.class, Super.class, SUB);
		//ClassCastException:Cannot cast Super to Sub
		invoke(method, Sub.class, Sub.class, SUPER);
		//OK
		invoke(method, Sub.class, Sub.class, SUB);

		System.out.println("--------------------------------");
		//以下全都OK
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, declaringClass, SUPER);
		invoke(method, declaringClass, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Super.class, SUPER);
		invoke(method, Super.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		invoke(method, Sub.class, SUPER);
		invoke(method, Sub.class, SUB);
		System.out.println("================end default method===================");
	}

	static void overrideMethod() throws NoSuchMethodException {
		System.out.println("================override method===================");
		final var method = Inter.class.getMethod("allImplementing");
		final var declaringClass = method.getDeclaringClass();
		System.out.println("declaring class:" + declaringClass.getSimpleName());
		//使用unreflectSpecial，并在in和unreflectSpecial指定为同一个class（这点有点奇怪），则会使用接口的default实现。
		//OK:Inter.allImplementing()
		invoke(method, declaringClass, declaringClass, SUPER);
		invoke(method, declaringClass, declaringClass, SUB);
		//以下2个：no private access for invokespecial: class me.magicall.support.lang.trial.MethodHandleTrial$Super, from me.magicall.support.lang.trial.MethodHandleTrial$Inter/private
		invoke(method, declaringClass, Super.class, SUPER);
		invoke(method, declaringClass, Super.class, SUB);
		//以下2个也是no private access for invokespecial，但中间的Super变成了Sub。
		invoke(method, declaringClass, Sub.class, SUPER);
		invoke(method, declaringClass, Sub.class, SUB);
		//以下2个也是no private access for invokespecial，中间是Inter，后面是Super。
		invoke(method, Super.class, declaringClass, SUPER);
		invoke(method, Super.class, declaringClass, SUB);
		//OK:Inter.allImplementing()
		invoke(method, Super.class, Super.class, SUPER);
		invoke(method, Super.class, Super.class, SUB);
		//no private access for invokespecial，中间Super，后面Sub。
		invoke(method, Super.class, Sub.class, SUPER);
		invoke(method, Super.class, Sub.class, SUB);
		//no private access for invokespecial，中间Inter，后面Sub。
		invoke(method, Sub.class, declaringClass, SUPER);
		invoke(method, Sub.class, declaringClass, SUB);
		//no private access for invokespecial，中间Super，后面Sub。
		invoke(method, Sub.class, Super.class, SUPER);
		invoke(method, Sub.class, Super.class, SUB);
		//OK:Inter.allImplementing()
		invoke(method, Sub.class, Sub.class, SUPER);
		invoke(method, Sub.class, Sub.class, SUB);
		//可见inClass和specialClass存在双向互访，会出现“父类不能访问子类那个接口”的情况，所以得是同一个类（存疑）。
		System.out.println("---------special-----------");
		{//若使用unreflect，则后面bindTo哪个类，就用离它最近的实现方法（先自己，再父类，以此类推……）。
			invoke(method, declaringClass, SUPER);//Super.allImplementing()
			invoke(method, declaringClass, SUB);//Sub.allImplementing()
			invoke(method, Super.class, SUPER);//Super.allImplementing()
			invoke(method, Super.class, SUB);//Sub.allImplementing()
			invoke(method, Sub.class, SUPER);//Super.allImplementing()
			invoke(method, Sub.class, SUB);//Sub.allImplementing()
			//可见与inClass无关。
		}
		System.out.println("================end override method===================");
	}

	private static void invoke(final Method method, final Class<?> inClass, final Object x) {
		try {
			final var result = MethodHandles.lookup().in(inClass)//
					.unreflect(method)//
					.bindTo(x)//必须bind一个对象，虽然好像应该用不上。
					.invoke();
			System.out.println(inClass.getSimpleName() + ' ' + x + '|' + result);
		} catch (final Throwable throwable) {
			System.out.println("ERROR:" + throwable.getClass().getName() + '|' + inClass.getSimpleName() + ' ' + x + '|'
					+ throwable.getMessage());
		}
	}

	private static void invoke(final Method method, final Class<?> inClass, final Class<?> specialCaller,
														 final Object x) {
		try {
			final var result = MethodHandles.lookup().in(inClass)//
					.unreflectSpecial(method, specialCaller)//
					.bindTo(x)//必须bind一个对象，即使有时好像应该用不上。比如default方法
					.invoke();
			System.out.println(inClass.getSimpleName() + ' ' + specialCaller.getSimpleName() + ' ' + x + ':' + result);
		} catch (final Throwable throwable) {
			System.out.println("ERROR:" + throwable.getClass().getName() + '|' + inClass.getSimpleName() + ' '
					+ specialCaller.getSimpleName() + ' ' + x + '|' + throwable.getMessage());
			//调用unreflectSpecial方法时，在java.lang.invoke.MethodHandles.Lookup.checkSpecialCaller:1567行，将会检查
			// !hasPrivateAccess()
			// || (specialCaller != lookupClass()
			//       && !(ALLOW_NESTMATE_ACCESS && erifyAccess.isSamePackageMember(specialCaller, lookupClass()))
			// )
			// 若为true则会抛异常 no private access for invokespecial。
			//其中ALLOW_NESTMATE_ACCESS是个static final常量，恒为false（很奇怪），所以!(ALLOW_NESTMATE_ACCESS && erifyAccess.isSamePackageMember(specialCaller, lookupClass())这一段必然为true。条件简化为：
			// !hasPrivateAccess()||specialCaller != lookupClass()
			// 其中hasPrivateAccess是判断有没有private级别的访问权限的。这也奇怪，public、package、protected、private四级访问权限，竟然连private都没有，那是什么情况？所以通常为true，取反后为false。
			// 这意味着，只要specialCaller不等于lookupClass就会抛异常。所以使用unreflectSpecial时，必须保证：
			// 前面的in方法的参数 跟 unreflectSpecial的specialCaller参数必须为同一个Class对象。
			//另，异常信息里带的是Lookup类的toString，包含lookup的class名。
		}
	}

	static void b() throws Throwable {
		final String[] ss = {"s1", "s2"};
		final var invoke = MethodHandles.arrayElementGetter(Object[].class).bindTo(ss).invoke();
		System.out.println(invoke);
	}

	private interface Inter {
		String getId();

		default String allImplementing() {
			return Inter.class.getSimpleName() + ".allImplementing()";
		}

		boolean _SuperClassImplementing();

		default int defaultImplementing() {
			return 9999;
		}

		default int _SuperClassNotImplementing() {
			return 1234;
		}
	}//interface Inter

	private static class Super implements Inter {

		String id;

		public Super(final String id) {
			this.id = id;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public boolean _SuperClassImplementing() {
			return true;
		}

		@Override
		public String allImplementing() {
			return Super.class.getSimpleName() + ".allImplementing()";
		}

		public boolean isLocked() {
			return true;
		}

		public boolean getLocked() {
			return true;
		}

		@Override
		public String toString() {
			return "Super{" + getId() + '}';
		}
	}//class Super

	private static class Sub extends Super {
		boolean locked;

		public Sub(final String id) {
			super(id);
		}

		@Override
		public String allImplementing() {
			return Sub.class.getSimpleName() + ".allImplementing()";
		}

		@Override
		public int _SuperClassNotImplementing() {
			return 2345;
		}

		@Override
		public String toString() {
			return "Sub{" + getId() + '}';
		}
	}//class Sub
}
