/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.trial;

import java.io.StringWriter;
import java.net.URI;
import java.util.Arrays;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import me.magicall.program.lang.java.贵阳DearSun.io.IOKit;

public class InMemoryJavaFileObject extends SimpleJavaFileObject {
	private final String contents;

	public InMemoryJavaFileObject(final String className, final String contents) {
		super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
		this.contents = contents;
	}

	public static void main(final String... args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		// 通过 ToolProvider 取得 JavaCompiler 对象，JavaCompiler 对象是动态编译工具的主要对象
		final var compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = null;
		try {
			// 通过 JavaCompiler 取得标准 StandardJavaFileManager 对象，StandardJavaFileManager 对象主要负责
			// 编译文件对象的创建，编译的参数等等，我们只对它做些基本设置比如编译 CLASSPATH 等。
			fileManager = compiler.getStandardFileManager(null, null, null);

			// 因为是从内存中读取 Java 源文件，所以需要创建我们的自己的 JavaFileObject，即 InMemoryJavaFileObject
			final var codeString = "public class TTT{public String toString(){return \"haha\";}}";
			final var className = "TTT";
			final JavaFileObject fileObject = new InMemoryJavaFileObject(className, codeString);
			final Iterable<? extends JavaFileObject> files = Arrays.asList(fileObject);

			// 编译结果信息的记录
			final var sw = new StringWriter();

			// 编译目的地设置
			final var classOutputFolder = "d:\\";
			final Iterable<String> options = Arrays.asList("-d", classOutputFolder);

			// 通过 JavaCompiler 对象取得编译 Task
			final var task = compiler.getTask(sw, fileManager, null, options, null, files);

			// 调用 call 命令执行编译，如果不成功输出错误信息
			if (!task.call()) {
				final var failedMsg = sw.toString();
				System.out.println("Build Error:" + failedMsg);
			}

			final var c = fileManager.getClassLoader(StandardLocation.CLASS_OUTPUT).loadClass(className);
			final var o = c.newInstance();
			System.out.println("@@@@@@InMemoryJavaFileObject.main():" + o);
		} finally {
			IOKit.close(fileManager);
		}
	}

	@Override
	public CharSequence getCharContent(final boolean ignoreEncodingErrors) {
		return contents;
	}
}
