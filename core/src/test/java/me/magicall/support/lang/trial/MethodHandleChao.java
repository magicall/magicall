/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.trial;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

public class MethodHandleChao {

	public static void main2(final String... args) throws Throwable {
		final var mt = MethodType.methodType(int.class, Boolean.class);
		var mh = MethodHandles.lookup().findVirtual(MHSuper.class, "x", mt);
		//        mh.bindTo(null).invoke(false); // NullPointerException
//        mh.invoke(false); // WrongMethodTypeException: cannot convert MethodHandle(MHSuper,Boolean)int to (boolean)void
//        mh.bindTo(new Object()).invoke(false); // ClassCastException: Cannot cast java.lang.Object to MHSuper
		mh.bindTo(new MHSuper()).invoke(false); // super::boxed
		mh.bindTo(new MHSuper()).invoke(Boolean.FALSE); // super::boxed
		// super::boxed
		Object a = (int) mh.bindTo(new MHSuper()).invokeExact(Boolean.FALSE);
//        a = (Number)mh.bindTo(new MHSuper()).invokeExact(Boolean.FALSE); // WrongMethodTypeException: expected (Boolean)int but found (Boolean)Number
//        a = (Integer)mh.bindTo(new MHSuper()).invokeExact(Boolean.FALSE); // WrongMethodTypeException: expected (Boolean)int but found (Boolean)Integer
//        mh.bindTo(new MHSuper()).invokeExact(Boolean.FALSE); // WrongMethodTypeException: expected (Boolean)int but found (Boolean)void

		mh.bindTo(new MHSub()).invoke(false); // sub::boxed
		mh.bindTo(new MHSub()).invoke(Boolean.FALSE); // sub::boxed
		a = (int) mh.bindTo(new MHSub()).invokeExact(Boolean.FALSE); // sub::boxed

		mh = MethodHandles.lookup().findStatic(MHSuper.class, "y", mt);
		mh.invoke(false); // super::static
		a = (int) mh.invokeExact(Boolean.FALSE); // super::static

		mh = MethodHandles.lookup().findStatic(MHSuper.class, "z", MethodType.methodType(int.class, MHSuper.class));
		final MHSuper sup = new MHSub();
		a = (int) mh.invokeExact(sup); // class MHSub
//        MHSub sub = new MHSub();a = (int)mh.invokeExact(sub); // WrongMethodTypeException: expected (MHSuper)int but found (MHSub)int
	}

	private static class MHSuper {
		public static int y(final Boolean a) {
			System.out.println("super::static");
			return 1;
		}

		public static int z(final MHSuper a) {
			System.out.println(a.getClass());
			return 1;
		}

		public int x(final boolean a) {
			System.out.println("super::primitive");
			return 1;
		}

		public int x(final Boolean a) {
			System.out.println("super::boxed");
			return 1;
		}
	}

	private static class MHSub extends MHSuper {
		public static int y(final Boolean a) {
			System.out.println("sub::static");
			return 1;
		}

		@Override
		public int x(final boolean a) {
			System.out.println("sub::primitive");
			return 1;
		}

		@Override
		public int x(final Boolean a) {
			System.out.println("sub::boxed");
			return 1;
		}
	}
}
