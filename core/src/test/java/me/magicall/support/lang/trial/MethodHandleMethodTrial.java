/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.trial;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.Arrays;
import java.util.List;

public class MethodHandleMethodTrial {
	public static void main(final String... args) throws NoSuchMethodException, IllegalAccessException {
		test();
//		testSpreader();
	}

	private static void testSpreader() throws NoSuchMethodException, IllegalAccessException {
		{
			final var raw = MethodHandles.publicLookup().findStatic(MethodHandleMethodTrial.class, "forTestVarargs1",
					MethodType.methodType(void.class, Object.class, Object[].class));
			final var asSpreader = raw.asSpreader(Object[].class, 2);
			//参数长度必须为arrayLength:
			invoke(asSpreader, new Object[]{});
			invoke(asSpreader, new Object[]{1, "a", "b"});

			invoke(asSpreader, new Object[]{2, "c"});
			invoke(asSpreader, new Object[]{3, new Object[]{"d", "e"}});//new Object[]{"d", "e"}被当成varargs的第一项
		}
		{
			final var raw = MethodHandles.publicLookup().findStatic(MethodHandleMethodTrial.class, "forTestVarargs2",
					MethodType.methodType(void.class, Object.class, Object[].class));
			final var asSpreader = raw.asSpreader(Object[].class, 2);
			invoke(asSpreader, new Object[]{4, "f"});
			invoke(asSpreader, new Object[]{5, new Object[]{"g", "h"}});//new Object[]{"g", "h"}被当成varargs的第一项
		}
	}

	public static void forTestVarargs1(final Object arg1, final Object... arg2) {
		System.out.println("@forTestVarargs1:" + arg1);
		System.out.println("@forTestVarargs1:" + Arrays.toString(arg2));
	}

	public static void forTestVarargs2(final Object arg1, final Object... arg2) {
		System.out.println("@forTestVarargs2:" + arg1);
		System.out.println("@forTestVarargs2:" + Arrays.toString(arg2));
	}

	private static void test() throws NoSuchMethodException, IllegalAccessException {

		final var raw = MethodHandles.publicLookup()
				.findStatic(Arrays.class, "asList", MethodType.methodType(List.class, Object[].class));
		System.out.println("------------raw--------------");
		final Object varArgs1 = "a";
		invoke(raw, varArgs1);//Arrays.asList(varArgs1)
		invokeVarArgs(raw);//Arrays.asList("a","b","c")
		final Object[] arrArg1 = {"a"};
		invoke(raw, arrArg1);//Arrays.asList((Object)arrArg1)
		final Object[] arrArg2 = {"a", "b", "c"};
		invoke(raw, arrArg2);//Arrays.asList((Object)arrArg2)
		//todo:上面这俩不是很符合使用需求，得换成asListFixed的
		System.out.println("------------asListFixed--------------");
		//asFixedArity的，不能接收可变长参数列表（varargs）
		final var asListFixed = raw.asFixedArity();
		invoke(asListFixed, varArgs1);//ClassCastException  Cannot cast java.lang.String to [Ljava.lang.Object;
		invokeVarArgs(
				asListFixed);//WrongMethodTypeException  cannot convert MethodHandle(Object[])List to (String,String,String)Object
		invoke(asListFixed, arrArg1);//Arrays.asList(arrArg1)
		invoke(asListFixed, arrArg2);//Arrays.asList(arrArg2)
		try {
			System.out.println("result:" + asListFixed.invoke((Object) arrArg2));//Arrays.asList(arrArg2)
		} catch (final Throwable e) {
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
		try {
			System.out.println("result:" + asListFixed.invoke((Object[]) arrArg2));//Arrays.asList(arrArg2)
		} catch (final Throwable e) {
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
		System.out.println("------------asVarargsCollector--------------");
		//asVarargsCollector的可以。
		final var asListVarargs = raw.asVarargsCollector(Object[].class);
		invoke(asListVarargs, varArgs1);//Arrays.asList(varArgs1)
		invokeVarArgs(asListVarargs);//Arrays.asList("a","b","c")
		invoke(asListVarargs, arrArg1);//Arrays.asList((Object)arrArg1)
		invoke(asListVarargs, arrArg2);//Arrays.asList((Object)arrArg2)
		//todo：可以看出，对于一个varargs方法，asVarargsCollector就是raw。
		System.out.println("------------asSpreader--------------");
		//asSpreader的也不行。
		final var arrayLength1 = 1;
		final var asListSpreader = raw.asSpreader(Object[].class, arrayLength1);
		invoke(asListSpreader, varArgs1);//ClassCastException  Cannot cast java.lang.String to [Ljava.lang.Object;
		invokeVarArgs(
				asListSpreader);//WrongMethodTypeException  cannot convert MethodHandle(Object[])List to (String,String,String)Object
		invoke(asListSpreader, arrArg1);//Arrays.asList(arrArg1)
		invoke(asListSpreader, arrArg2);//IllegalArgumentException  array is not of length 1
		//可见真正要提供的参数的长度要符合arrayLength……
		try {
			raw.asSpreader(Object[].class, 3);
			//arrayLength参数必须小于那个方法的参数数量，比如asList的参数数量为1，arrayLength就不能大于1……
		} catch (final Throwable e) {
			//IllegalArgumentException  bad spread array length
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
		try {
			final var spreader2 = raw.asSpreader(Object[].class, 0);
			invoke(spreader2,
					varArgs1);//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (Object)Object
			invokeVarArgs(
					spreader2);//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (String,String,String)Object
			invoke(spreader2,
					arrArg1);//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (Object)Object
			invoke(spreader2,
					arrArg2);//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (Object)Object
			//而如果arrayLength==0的话，其实什么也调不了
			invoke(spreader2,
					new Object[]{});//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (Object)Object
			invoke(spreader2,
					null);//WrongMethodTypeException  cannot convert MethodHandle(Object[],Object[])List to (Object)Object
		} catch (final Exception e) {
			//IllegalArgumentException  bad spread array length
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
		try {
			raw.asSpreader(Object.class, 3);
			//第一个参数必须是数组的Class……
		} catch (final Exception e) {
			//IllegalArgumentException: not an array type: class java.lang.Object
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
		System.out.println("------------asCollector--------------");
		//asCollector。
		final var asCollector = raw.asCollector(Object[].class, 1);
		invoke(asCollector, varArgs1);//Arrays.asList(varArgs1)
		invokeVarArgs(
				asCollector);//WrongMethodTypeException  cannot convert MethodHandle(Object[])List to (String,String,String)Object
		invoke(asCollector, arrArg1);//Arrays.asList((Object)arrArg1)
		invoke(asCollector, arrArg2);//Arrays.asList((Object)arrArg1)
		//可见真正要提供的参数的长度要符合arrayLength……
		try {
			raw.asSpreader(Object[].class, 3);
			//arrayLength参数必须小于那个方法的参数数量，比如asList的参数数量为1，arrayLength就不能大于1……
		} catch (final Throwable e) {
			//IllegalArgumentException  bad spread array length
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
	}

	private static void invokeVarArgs(final MethodHandle methodHandle) {
		try {
			System.out.println("result:" + methodHandle.invoke("a", "b", "c"));
		} catch (final Throwable e) {
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
	}

	private static void invoke(final MethodHandle methodHandle, final Object arg) {
		try {
			System.out.println("result:" + methodHandle.invoke(arg));
		} catch (final Throwable e) {
			System.out.println(e.getClass().getSimpleName() + "  " + e.getMessage());
		}
	}
}
