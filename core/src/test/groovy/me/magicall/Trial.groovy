/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall


import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.TypeVariable
import java.lang.reflect.WildcardType

class Trial {

	static void main(String[] args) {
		def field = Trial.getDeclaredField('list')
		def type = field.genericType
		println type
		def type1 = type as ParameterizedType
		println type1.actualTypeArguments
//		a()
	}

	private static List<Set<String>> list = List.of()

	protected static void a() {
// 基本类型测试
		System.out.println(isTypeCompatible(Number.class, Integer.class)) // true
		System.out.println(isTypeCompatible(Object.class, String.class)) // true
		System.out.println(isTypeCompatible(Integer.class, Double.class)) // false

		// 泛型类型测试
		System.out.println(isTypeCompatible(List.class, ArrayList.class)) // true
		System.out.println(isTypeCompatible(List.class, List.class)) // true
		System.out.println(isTypeCompatible(List.class, Map.class)) // false

		// 带泛型参数的类型测试
		System.out.println(isTypeCompatible(new TypeReference<List<String>>() {
		}.getType(), new TypeReference<ArrayList<String>>() {}.getType())) // true
		System.out.println(isTypeCompatible(new TypeReference<List<String>>() {
		}.getType(), new TypeReference<List<Integer>>() {}.getType())) // false

		// 通配符测试
		System.out.println(isTypeCompatible(new TypeReference<List<? extends Number>>() {
		}.getType(), new TypeReference<List<Integer>>() {}.getType())) // true
		System.out.println(isTypeCompatible(new TypeReference<List<? super Integer>>() {
		}.getType(), new TypeReference<List<Number>>() {}.getType())) // true
		System.out.println(isTypeCompatible(new TypeReference<List<? extends Number>>() {
		}.getType(), new TypeReference<List<String>>() {}.getType())) // false

		// 类型变量测试
//		System.out.println(isTypeCompatible(new TypeReference<List<T>>() {}.getType(), new TypeReference<List<String>>() {
//		}.getType())) // true
	}

	// Helper class to create Type instances with generic parameters
	static abstract class TypeReference<T> {
		private final Type type

		protected TypeReference() {
			Type superClass = getClass().getGenericSuperclass()
			if (superClass instanceof ParameterizedType) {
				this.type = ((ParameterizedType) superClass).getActualTypeArguments()[0]
			} else {
				throw new RuntimeException("Missing type parameter.")
			}
		}

		Type getType() {
			return this.type
		}
	}

	static boolean isTypeCompatible(Class<?> a, Class<?> b) {
		return isTypeCompatible0(a, b, new HashMap<>())
	}

	private static boolean isTypeCompatible0(Class<?> a, Class<?> b, Map<Type, Type> typeMapping) {
		if (a.equals(b)) {
			return true
		}

		if (a.isAssignableFrom(b)) {
			return true
		}

		Type[] aTypeParams = a.getTypeParameters()
		Type[] bTypeParams = b.getTypeParameters()

		if (aTypeParams.length != bTypeParams.length) {
			return false
		}

		for (int i = 0; i < aTypeParams.length; i++) {
			Type aTypeParam = aTypeParams[i]
			Type bTypeParam = bTypeParams[i]

			if (!isTypeParameterCompatible(aTypeParam, bTypeParam, typeMapping)) {
				return false
			}
		}

		return true
	}

	private static boolean isTypeParameterCompatible(Type a, Type b, Map<Type, Type> typeMapping) {
		if (a instanceof Class && b instanceof Class) {
			return isTypeCompatible((Class<?>) a, (Class<?>) b, typeMapping)
		} else if (a instanceof ParameterizedType && b instanceof ParameterizedType) {
			ParameterizedType aParamType = (ParameterizedType) a
			ParameterizedType bParamType = (ParameterizedType) b

			if (!isTypeCompatible((Class<?>) aParamType.getRawType(), (Class<?>) bParamType.getRawType(), typeMapping)) {
				return false
			}

			Type[] aTypeArgs = aParamType.getActualTypeArguments()
			Type[] bTypeArgs = bParamType.getActualTypeArguments()

			if (aTypeArgs.length != bTypeArgs.length) {
				return false
			}

			for (int i = 0; i < aTypeArgs.length; i++) {
				if (!isTypeParameterCompatible(aTypeArgs[i], bTypeArgs[i], typeMapping)) {
					return false
				}
			}

			return true
		} else if (a instanceof TypeVariable && b instanceof TypeVariable) {
			TypeVariable<?> aTypeVar = (TypeVariable<?>) a
			TypeVariable<?> bTypeVar = (TypeVariable<?>) b

			if (!aTypeVar.getName().equals(bTypeVar.getName())) {
				return false
			}

			Type[] aBounds = aTypeVar.getBounds()
			Type[] bBounds = bTypeVar.getBounds()

			if (aBounds.length != bBounds.length) {
				return false
			}

			for (int i = 0; i < aBounds.length; i++) {
				if (!isTypeParameterCompatible(aBounds[i], bBounds[i], typeMapping)) {
					return false
				}
			}

			return true
		} else if (a instanceof WildcardType && b instanceof WildcardType) {
			WildcardType aWildcard = (WildcardType) a
			WildcardType bWildcard = (WildcardType) b

			Type[] aUpperBounds = aWildcard.getUpperBounds()
			Type[] bUpperBounds = bWildcard.getUpperBounds()

			if (aUpperBounds.length != bUpperBounds.length) {
				return false
			}

			for (int i = 0; i < aUpperBounds.length; i++) {
				if (!isTypeParameterCompatible(aUpperBounds[i], bUpperBounds[i], typeMapping)) {
					return false
				}
			}

			Type[] aLowerBounds = aWildcard.getLowerBounds()
			Type[] bLowerBounds = bWildcard.getLowerBounds()

			if (aLowerBounds.length != bLowerBounds.length) {
				return false
			}

			for (int i = 0; i < aLowerBounds.length; i++) {
				if (!isTypeParameterCompatible(aLowerBounds[i], bLowerBounds[i], typeMapping)) {
					return false
				}
			}

			return true
		} else if (a instanceof TypeVariable) {
			Type mappedType = typeMapping.get(a)
			if (mappedType != null) {
				return isTypeParameterCompatible(mappedType, b, typeMapping)
			} else {
				typeMapping.put(a, b)
				return true
			}
		} else if (b instanceof TypeVariable) {
			Type mappedType = typeMapping.get(b)
			if (mappedType != null) {
				return isTypeParameterCompatible(a, mappedType, typeMapping)
			} else {
				typeMapping.put(b, a)
				return true
			}
		}

		return false
	}

}
