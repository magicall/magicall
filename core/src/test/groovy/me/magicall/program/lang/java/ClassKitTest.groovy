/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java

import groovy.transform.PackageScope
import me.magicall.program.lang.java.贵阳DearSun.ClassKit
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.AccessLv
import spock.lang.Specification

class ClassKitTest extends Specification {
	public static class PublicClass {
	}

	private static class PrivateClass {
	}

	protected static class ProtectedClass {
	}

	@PackageScope
	static class PackageClass {
	}

	def "AccessLvOf class"() {
		when:
		def toTest = ClassKit.accessLvOf(c)
		then:
		toTest == expect
		where:
		c              | expect
		Object         | AccessLv.PUBLIC
		String         | AccessLv.PUBLIC
		int            | AccessLv.PUBLIC
		void           | AccessLv.PUBLIC
		PublicClass    | AccessLv.PUBLIC
		PrivateClass   | AccessLv.PRIVATE
		ProtectedClass | AccessLv.PROTECTED
		PackageClass   | AccessLv.DEFAULT
	}

	def 'classCalled'() {
		when:
		def toTest = ClassKit.classCalled(name)
		then:
		toTest == expect
		where:
		name                                                        | expect
		'java.lang.Object'                                          | Object
		'java.lang.String'                                          | String
		'java.lang.Class'                                           | Class
		'int'                                                       | int
		'void'                                                      | void
		'me.magicall.program.lang.java.ClassKitTest'                | ClassKitTest
		'me.magicall.program.lang.java.ClassKitTest.PublicClass'    | PublicClass
		'me.magicall.program.lang.java.ClassKitTest$PublicClass'    | PublicClass
		'me.magicall.program.lang.java.ClassKitTest.PrivateClass'   | PrivateClass
		'me.magicall.program.lang.java.ClassKitTest.ProtectedClass' | ProtectedClass
		'me.magicall.program.lang.java.ClassKitTest.PackageClass'   | PackageClass
		'java.lang.Object[]'                                        | Object[]
		'java.lang.String[]'                                        | String[]
		'int[]'                                                     | int[]
//		'void[]'                                                    | void[]//这个是真没有，写出来就编译错误
		'a'                                                         | null
		'io.springframework.ApplicationContext'                     | null
	}
}
