/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.locale

class EncodingTrial {
	static void main(String[] args) {
		final String[] charsets = ["US-ASCII", "UTF-8", "GBK", "ISO8859-1"]
		for (int i = 0; i < charsets.length; i++) {
			for (int j = 0; j < charsets.length; j++) {
				if (i != j) {
					final var bytes = "中国通/数学 语文".getBytes(charsets[i])
					System.out.println(charsets[i] + ' ' + charsets[j] + ' ' + new String(bytes, charsets[j]))
				}
			}
		}
	}
}
