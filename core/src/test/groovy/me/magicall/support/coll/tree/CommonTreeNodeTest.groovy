/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.coll.tree

import me.magicall.program.lang.java.贵阳DearSun.coll.CommonTree
import spock.lang.Specification

import java.util.stream.Collectors

class CommonTreeNodeTest extends Specification {
	def "GetElement"() {
		def toTest = new CommonTree(1).root
		expect:
		toTest.getElement() == 1
	}

	def "GetParent"() {
		def parent = new CommonTree(2).root
		def toTest = parent.child(1)
		expect:
		toTest.parent() == parent
	}

	def "SetElement"() {
		when:
		def toTest = new CommonTree(1).root
		then:
		toTest.getElement() == 1
		when:
		toTest.setElement(2)
		then:
		toTest.getElement() == 2
	}

	def "ClearElement"() {
		when:
		def toTest = new CommonTree(1).root
		then:
		toTest.getElement() == 1
		when:
		toTest.clearElement()
		then:
		toTest.getElement() == null
	}

	def "IsRoot"() {
		def parent = new CommonTree(2).root
		def toTest = parent.child(1)
		expect:
		parent.isRoot()
		!toTest.isRoot()
	}

	def "PathToRoot"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		expect:
		node3.pathToRoot() == [node2, root]
	}

	def "PathFromRoot"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		expect:
		node3.pathFromRoot() == [root, node2]
	}

	def "IsChildOf"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		expect:
		!root.isChildOf(root)
		!root.isChildOf(node2)
		!root.isChildOf(node3)
		!root.isChildOf(node4)

		node2.isChildOf(root)
		!node2.isChildOf(node2)
		!node2.isChildOf(node3)
		!node2.isChildOf(node4)

		node3.isChildOf(root)
		node3.isChildOf(node2)
		!node3.isChildOf(node3)
		!node3.isChildOf(node4)

		node4.isChildOf(root)
		!node4.isChildOf(node2)
		!node4.isChildOf(node3)
		!node4.isChildOf(node4)
	}

	def "GetLayer"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		expect:
		root.getLayer() == 0
		node2.getLayer() == 1
		node3.getLayer() == 2
		node4.getLayer() == 1
	}

	def "CountChildren"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		expect:
		root.countChildren() == 2
		node2.countChildren() == 1
		node3.countChildren() == 0
		node4.countChildren() == 0
	}

	def "IsLeaf"() {
		when:
		def root = new CommonTree(2).root
		then:
		root.isLeaf()
		when:
		def node2 = root.child(1)
		then:
		!root.isLeaf()
		node2.isLeaf()
		when:
		def node3 = node2.child(3)
		then:
		!root.isLeaf()
		!node2.isLeaf()
		node3.isLeaf()
		when:
		def node4 = root.child(4)
		then:
		!root.isLeaf()
		!node2.isLeaf()
		node3.isLeaf()
		node4.isLeaf()
	}

	def "AddChild"() {
		when:
		def root = new CommonTree(2).root
		then:
		root.children().count() == 0
		when:
		def node2 = root.child(1)
		then:
		root.children().collect(Collectors.toList()).containsAll(node2)
		node2.children().count() == 0
		when:
		def node3 = node2.child(3)
		then:
		root.children().collect(Collectors.toList()).containsAll(node2)
		node2.children().collect(Collectors.toList()).containsAll(node3)
		node3.children().count() == 0
		when:
		def node4 = root.child(4)
		then:
		root.children().collect(Collectors.toList()).containsAll(node2, node4)
		node2.children().collect(Collectors.toList()).containsAll(node3)
		node3.children().count() == 0
		node4.children().count() == 0
	}

	def "AddChildren"() {
		def root = new CommonTree(2).root
		def map = root.children([1, 3, 4])
		expect:
		root.children().count() == 3
		root.children().collect(Collectors.toList()).containsAll(map.values())
	}

	def "RemoveChild"() {
		def root = new CommonTree(2).root
		root.children([1, 3, 4])
		root.removeChild({ e -> e.element == 3 })
		expect:
		root.children().count() == 2
		root.children().noneMatch({ e -> e.element == 3 })
	}

	def "RemoveChildren"() {
		def root = new CommonTree(2).root
		root.children([1, 3, 4])
		root.removeChildren()
		expect:
		root.children().count() == 0
	}

	def "GetDescendants"() {
		when:
		def root = new CommonTree(2).root
		then:
		root.getDescendants().isEmpty()
		when:
		def node2 = root.child(1)
		then:
		root.getDescendants().size() == 1
		root.getDescendants() == [node2]
		node2.getDescendants().isEmpty()
		when:
		def node3 = node2.child(3)
		then:
		root.getDescendants().size() == 2
		root.getDescendants().toSet() == [node2, node3].toSet()
		node2.getDescendants().size() == 1
		node2.getDescendants() == [node3]
		node3.getDescendants().isEmpty()
		when:
		def node4 = root.child(4)
		then:
		root.getDescendants().size() == 3
		root.getDescendants().toSet() == [node2, node3, node4].toSet()
		node2.getDescendants().size() == 1
		node2.getDescendants() == [node3]
		node3.getDescendants().isEmpty()
		node4.getDescendants().isEmpty()
	}

	def "CountDescendants"() {
		when:
		def root = new CommonTree(2).root
		then:
		root.countDescendants() == 0
		when:
		def node2 = root.child(1)
		then:
		root.countDescendants() == 1
		node2.countDescendants() == 0
		when:
		def node3 = node2.child(3)
		then:
		root.countDescendants() == 2
		node2.countDescendants() == 1
		when:
		def node4 = root.child(4)
		then:
		root.countDescendants() == 3
		node2.countDescendants() == 1
		node3.countDescendants() == 0
		node4.countDescendants() == 0
	}

	def "GetLeafNodes"() {
		when:
		def root = new CommonTree(2).root
		then:
		root.getLeafNodes().size() == 1
		root.getLeafNodes()[0].element == root.element
		when:
		def node2 = root.child(1)
		then:
		root.getLeafNodes().size() == 1
		root.getLeafNodes()[0].element == node2.element
		node2.getLeafNodes().size() == 1
		node2.getLeafNodes()[0].element == node2.element
		when:
		def node3 = node2.child(3)
		then:
		root.getLeafNodes().size() == 1
		root.getLeafNodes()[0].element == node3.element
		node2.getLeafNodes().size() == 1
		node2.getLeafNodes()[0].element == node3.element
		node3.getLeafNodes().size() == 1
		node3.getLeafNodes()[0].element == node3.element
		when:
		def node4 = root.child(4)
		then:
		root.getLeafNodes().size() == 2
		root.getLeafNodes().stream().map({ e -> e.element }).collect(Collectors.toSet()) == [node3.element, node4.element].toSet()
		node2.getLeafNodes().size() == 1
		node2.getLeafNodes()[0].element == node3.element
		node3.getLeafNodes().size() == 1
		node3.getLeafNodes()[0].element == node3.element
		node4.getLeafNodes().size() == 1
		node4.getLeafNodes()[0].element == node4.element
	}

	def "TreeFromMe"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		def me = root.treeFromMe()
		expect:
		me.containsAll(root.element, node2.element, node3.element, node4.element)
		me.root == root
		me.root.children().collect(Collectors.toSet()) == [node2, node4].toSet()
		me.root.children().filter({ e -> e == node2 })
				.findFirst().orElse(null)
				.children().collect(Collectors.toList()) == [node3]
	}

	def "GetSubTrees"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		when:
		def subTrees = root.subTrees
		then:
		subTrees.size() == 2
		subTrees.toSet() == [node2, node4].stream().map({ e -> e.treeFromMe() }).collect(Collectors.toSet())
		when:
		subTrees = node2.subTrees
		then:
		subTrees.size() == 1
		subTrees.toSet() == [node3].stream().map({ e -> e.treeFromMe() }).collect(Collectors.toSet())
		when:
		subTrees = node3.subTrees
		then:
		subTrees.isEmpty()
		when:
		subTrees = node4.subTrees
		then:
		subTrees.isEmpty()
	}

	def "AddSubTree"() {
		def root = new CommonTree(2).root
		def toAdd = new CommonTree(1)
		when:
		root.addSubTree(toAdd)
		then:
		root.children().findFirst().orElseThrow().treeFromMe() == toAdd
	}

	def "GetSiblings"() {
		def root = new CommonTree(2).root
		def node2 = root.child(1)
		def node3 = node2.child(3)
		def node4 = root.child(4)
		expect:
		root.siblings.isEmpty()
		node2.siblings == [node4]
		node3.siblings.isEmpty()
		node4.siblings == [node2]
	}
}
