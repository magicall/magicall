/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.coll.tree

import me.magicall.program.lang.java.贵阳DearSun.coll.CommonTree
import spock.lang.Specification

import java.util.stream.Collectors

class CommonTreeTest extends Specification {
	private static final Object ROOT_ELE = new Object()

	def "GetRoot"() {
		def toTest = new CommonTree(ROOT_ELE)
		expect:
		toTest.getRoot().getElement() == ROOT_ELE
	}

	def "NodeStream"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.nodeStream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1]
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.nodeStream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1, 1, 1, 1]
		when:
		root.child(1)
		then:
		toTest.nodeStream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1, 1, 1, 1, 1]
	}

	def "elementStream"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.elementStream().collect(Collectors.toList()) == [1]
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.elementStream().collect(Collectors.toList()) == [1, 1, 1, 1]
		when:
		root.child(1)
		then:
		toTest.elementStream().collect(Collectors.toList()) == [1, 1, 1, 1, 1]
	}

	def "Stream"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.stream().collect(Collectors.toList()) == [1]
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.stream().collect(Collectors.toList()) == [1, 1, 1, 1]
		when:
		root.child(1)
		then:
		toTest.stream().collect(Collectors.toList()) == [1, 1, 1, 1, 1]
	}

	def "CountLayers"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.countLayers() == 1
		when:
		def root = toTest.getRoot()
		root.child(1)
		then:
		toTest.countLayers() == 2
		when:
		root.child(1).child(1).child(1)
		then:
		toTest.countLayers() == 4
		when:
		root.child(1)
		then:
		toTest.countLayers() == 4
	}

	def "GetLeafNodes"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.getLeafNodes().stream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1]
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.getLeafNodes().stream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1]
		when:
		root.child(1)
		then:
		toTest.getLeafNodes().stream().map({ n -> n.getElement() }).collect(Collectors.toList()) == [1, 1]
	}

	def "CountLeaves"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.countLeaves() == 1
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.countLeaves() == 1
		when:
		root.child(1)
		then:
		toTest.countLeaves() == 2
	}

	def "Size"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.size() == 1
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.size() == 4
		when:
		root.child(1)
		then:
		toTest.size() == 5
	}

	def "IsEmpty"() {
		def toTest = new CommonTree(1)
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		root.child(1)
		expect:
		!toTest.isEmpty()
	}

	def "Iterator"() {
		def toTest = new CommonTree(1)
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		root.child(1)
		def i = 0
		for (def iterator = toTest.iterator(); iterator.hasNext();) {
			assert iterator.next() == 1
			i++
		}
		expect:
		i == 5
	}

	def "Contains"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.contains(1)
		when:
		def root = toTest.getRoot()
		root.child(2).child(3).child(4)
		then:
		toTest.contains(1)
		toTest.contains(2)
		toTest.contains(3)
		toTest.contains(4)
		when:
		root.child(5)
		then:
		toTest.contains(1)
		toTest.contains(2)
		toTest.contains(3)
		toTest.contains(4)
		toTest.contains(5)
	}

	def "ContainsAll"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.containsAll(1)
		when:
		def root = toTest.getRoot()
		root.child(2).child(3).child(4)
		then:
		toTest.containsAll(2, 3, 4, 1)
		when:
		root.child(5)
		then:
		toTest.containsAll(1, 2, 3, 4, 5)
	}

	def "ToArray"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.toArray() == [1].toArray()
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.toArray() == [1, 1, 1, 1].toArray()
		when:
		root.child(1)
		then:
		toTest.toArray() == [1, 1, 1, 1, 1].toArray()
	}

	def "TestToArray"() {
		when:
		def toTest = new CommonTree(1)
		then:
		toTest.toArray(new Integer[0]) == [1].toArray(new Integer[0])
		when:
		def root = toTest.getRoot()
		root.child(1).child(1).child(1)
		then:
		toTest.toArray(new Integer[0]) == [1, 1, 1, 1].toArray(new Integer[0])
		when:
		root.child(1)
		then:
		toTest.toArray(new Integer[0]) == [1, 1, 1, 1, 1].toArray(new Integer[0])
	}

	def "Add"() {
		def toTest = new CommonTree(1)
		when:
		toTest.add(2)
		then:
		thrown(UnsupportedOperationException)
	}

	def "AddAll"() {
		def toTest = new CommonTree(1)
		when:
		toTest.addAll([1, 2, 3])
		then:
		thrown(UnsupportedOperationException)
	}

	def "Remove"() {
		def toTest = new CommonTree(1)
		when:
		toTest.remove(2)
		then:
		thrown(UnsupportedOperationException)
	}

	def "RemoveAll"() {
		def toTest = new CommonTree(1)
		when:
		toTest.removeAll([1, 2, 3])
		then:
		thrown(UnsupportedOperationException)
	}

	def "RetainAll"() {
		def toTest = new CommonTree(1)
		when:
		toTest.retainAll([2, 3])
		then:
		thrown(UnsupportedOperationException)
	}

	def "Clear"() {
		def toTest = new CommonTree(1)
		when:
		toTest.clear()
		then:
		thrown(UnsupportedOperationException)
	}
}
