/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.coll.tree

import me.magicall.program.lang.java.贵阳DearSun.coll.CommonTree
import me.magicall.program.lang.java.贵阳DearSun.coll.TreeWalker
import spock.lang.Specification

import java.util.stream.Collectors

class TreeWalkerTest extends Specification {
	def "wide first"() {
		def tree = new CommonTree("1")
		def root = tree.root
		def children = root.children(["1.1", "1.2", "1.3", "1.4"])
		children.get("1.1").children(["1.1.1", "1.1.2", "1.1.3"])
		children.get("1.2").children(["1.2.1", "1.2.2", "1.2.3"])
		children.get("1.3").children(["1.3.1", "1.3.2", "1.3.3"])
		children.get("1.4").children(["1.4.1", "1.4.2", "1.4.3"])
		def walk = TreeWalker.wideFirst().walk(tree).map({ e -> e.element }).collect(Collectors.toList())
		expect:
		walk == ["1", "1.1", "1.2", "1.3", "1.4", "1.1.1", "1.1.2", "1.1.3", "1.2.1", "1.2.2", "1.2.3",
						 "1.3.1", "1.3.2", "1.3.3", "1.4.1", "1.4.2", "1.4.3"]
	}

	def "deep first"() {
		def tree = new CommonTree("1")
		def root = tree.root
		def children = root.children(["1.1", "1.2", "1.3", "1.4"])
		children.get("1.1").children(["1.1.1", "1.1.2", "1.1.3"])
		children.get("1.2").children(["1.2.1", "1.2.2", "1.2.3"])
		children.get("1.3").children(["1.3.1", "1.3.2", "1.3.3"])
		children.get("1.4").children(["1.4.1", "1.4.2", "1.4.3"])
		def walk = TreeWalker.deepFirst().walk(tree).map({ e -> e.element }).collect(Collectors.toList())
		expect:
		walk == ["1", "1.1", "1.1.1", "1.1.2", "1.1.3", "1.2", "1.2.1", "1.2.2", "1.2.3", "1.3", "1.3.1", "1.3.2", "1.3.3",
						 "1.4", "1.4.1", "1.4.2", "1.4.3"]
	}
}
