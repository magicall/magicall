/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.coll

import me.magicall.program.lang.java.贵阳DearSun.coll.BitSwitchers
import me.magicall.support.TestSupport
import spock.lang.Specification

import java.util.stream.Collectors

class BitSwitchersTest extends Specification {
	private static Collection<Integer> specials = Arrays.asList(0, 1, -1, 2, Integer.MAX_VALUE, Integer.MIN_VALUE)

	private static TreeSet<Integer> source() {
		return source(100)
	}

	private static TreeSet<Integer> source(long count) {
		def rt = new TreeSet()
		for (int i = 0; i < count; i++) {
			rt.add(TestSupport.randI())
		}
		//加一些特殊值
		rt.addAll(specials)
		return rt
	}

	def "Contains i"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def b = true
		for (def i : source) {
			b = b && toTest.contains(i)
			if (!b) {
				break
			}
		}
		expect:
		b
	}

	def "Iterator"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def iterator = toTest.iterator()
		def list = new ArrayList(source)
		Collections.sort(list)
		def iterator1 = list.iterator()
		def b = true
		while (iterator.hasNext() && iterator1.hasNext()) {
			def next = iterator.next()
			def next1 = iterator1.next()
			b = b && (next == next1)
			if (!b) {
				break
			}
		}
		expect:
		b
		!iterator.hasNext()
		!iterator1.hasNext()
	}

	def "Size"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		expect:
		toTest.size() == source.size()
	}

	def "size2"() {
		def toTest = new BitSwitchers()
		toTest.negative.set(0, Integer.MAX_VALUE)
		toTest.positive.set(0, Integer.MAX_VALUE)
		expect:
		toTest.size() == Integer.MAX_VALUE
	}

	def "LongSize"() {
		def toTest = new BitSwitchers()
		toTest.negative.set(0, Integer.MAX_VALUE)
		toTest.positive.set(0, Integer.MAX_VALUE)
		expect:
		toTest.longSize() == (long) Integer.MAX_VALUE * 2
	}

	def "Add"() {
		def toTest = new BitSwitchers()
		def i = 1
		for (def n : specials) {
			toTest.add(n)
			assert toTest.contains(n)
			assert i++ == toTest.size()
		}
		expect: true
	}

	def "Remove"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def size = toTest.size()
		def i = 1
		for (def n : specials) {
			toTest.remove(n)
			assert !toTest.contains(n)
			assert toTest.size() == size - i++
		}
		expect: true
	}

	def "Clear"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		toTest.clear()
		expect:
		toTest.isEmpty()
	}

	def "Contains"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		for (def i : source) {
			assert toTest.contains(i)
		}
		expect: true
	}

	def "Stream"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def set = toTest.stream().collect(Collectors.toSet())
		expect:
		set == source
	}

	def "AddAll"() {
		def source = source()
		def toTest = new BitSwitchers()
		toTest.addAll(source)
		expect:
		toTest == source
	}

	def "RemoveAll"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		toTest.removeAll(source)
		expect:
		toTest.isEmpty()
	}

	def "ContainsAll"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		expect:
		toTest.containsAll(source)
	}

	def "RetainAll"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		toTest.retainAll(source)
		expect:
		toTest == source
	}

	def "Comparator"() {
		def toTest = new BitSwitchers()
		expect:
		toTest.comparator() == null
	}

	def "FirstOrNull"() {
		def toTest = new BitSwitchers(s)
		expect:
		toTest.firstOrNull() == expectResult
		where:
		s        | expectResult
		source() | Integer.MIN_VALUE
		[]       | null
	}

	def "LastOrNull"() {
		def toTest = new BitSwitchers(s)
		expect:
		toTest.lastOrNull() == expectResult
		where:
		s        | expectResult
		source() | Integer.MAX_VALUE
		[]       | null
	}

	def "Lower"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def list = new ArrayList<>(source)
		Collections.sort(list)
		for (int i = 1; i < list.size(); i++) {
			assert toTest.lower(list.get(i)) == list.get(i - 1)
		}
		expect: true
	}

	def "Floor"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def list = new ArrayList<>(source)
		Collections.sort(list)
		for (int i = 0; i < list.size(); i++) {
			def n = list.get(i)
			assert toTest.floor(n) == n
		}
		expect: true
	}

	def "Higher"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def list = new ArrayList<>(source)
		Collections.sort(list)
		for (int i = 0; i < list.size() - 1; i++) {
			assert toTest.higher(list.get(i)) == list.get(i + 1)
		}
		expect: true
	}

	def "Ceiling"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		def list = new ArrayList<>(source)
		Collections.sort(list)
		for (int i = 0; i < list.size(); i++) {
			def n = list.get(i)
			assert toTest.ceiling(n) == n
		}
		expect: true
	}

	def "SubSet"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		expect:
		toTest.subSet(3, 8) == source.subSet(3, 8)
	}

	def "HeadSet"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		expect:
		toTest.headSet(6) == source.headSet(6)
	}

	def "TailSet"() {
		def source = source()
		def toTest = new BitSwitchers(source)
		expect:
		toTest.tailSet(6) == source.tailSet(6)
	}
}
