/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.java

import spock.lang.Specification

class ClassTrial extends Specification {
	def 'trial'() {
		def cc = [Object, int, List, [1, 2, 3].class, _SetImpl]
		for (final def c in cc) {
			println c
			println '|- .superclass : ' + c.superclass
			println '|- .genericSuperclass : ' + c.genericSuperclass
			println '|- .interfaces : ' + c.interfaces
			println '|- .genericInterfaces : ' + c.genericInterfaces
			println '|- .toGenericString() : ' + c.toGenericString()
		}
		expect:
		true
	}

	private static class _SetImpl extends AbstractSet<Long> implements Set<Long> {
		@Override
		Iterator<Long> iterator() { null }

		@Override
		int size() { 0 }
	}
}
