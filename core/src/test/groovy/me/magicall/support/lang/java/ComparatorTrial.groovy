/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.lang.java

import spock.lang.Specification

class ComparatorTrial extends Specification {
	def a() {
		def map1 = Map.of('a', 1)
		def map2 = Map.of('a', 2)
		def map3 = Map.of()
		def c = Comparator.comparing(e -> e.a, Comparator.nullsLast(Comparator.naturalOrder()))
		def r1 = c.compare(map1, map2)
		println r1
		def r2 = c.compare(map1, map3)
		println r2
		expect:
		true
	}
}
