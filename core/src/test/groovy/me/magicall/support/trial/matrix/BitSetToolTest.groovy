/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support.trial.matrix


import me.magicall.support.TestSupport

/**
 * @author Liang Wenjian.
 */
class BitSetToolTest extends TestSupport {
//	private static id = PropHandler.comparableProp(long.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.id
//		}
//	})
//	private static name = PropHandler.comparableProp(String.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.name
//		}
//	})
//	private static createTime = PropHandler.comparableProp(Date.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.createTime
//		}
//	})
//	private static enabled = PropHandler.comparableProp(boolean.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.enabled
//		}
//	})
//	private static age = PropHandler.comparableProp(int.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.age
//		}
//	})
//	private static money = PropHandler.comparableProp(float.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.money
//		}
//	})
//	private static color = PropHandler.comparableProp(Color.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.color
//		}
//	})
//	private
//	static Map propHandlers = [id   : id, name: name, createTime: createTime, enabled: enabled, age: age, money: money,
//														 color: color]
//
//	def "init for id"() {
//		def propName = 'id'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e.longValue())
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose   | layers    | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]        | []        | b()              | null
//		[]        | [1]       | b()              | [1: b()]
//		[1]       | []        | b()              | null
//		//相等
//		[1]       | [1]       | b()              | [1: b(0)]
//		[1, 2]    | [1, 2]    | b()              | [1: b(0), 2: b(1)]
//		[1, 2, 3] | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b(2)]
//		//左包含右
//		[1, 2, 3] | [1, 2]    | b()              | [1: b(0), 2: b(1, 2)]
//		[1, 2, 3] | [1]       | b()              | [1: b(0, 1, 2)]
//		//右包含左
//		[1]       | [1, 2, 3] | b()              | [1: b(0), 2: b(), 3: b()]
//		[1, 2]    | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b()]
//		//有交集
//		[1, 2, 3] | [2, 3, 4] | b(0)             | [2: b(1), 3: b(2), 4: b()]
//		//无交集
//		[1, 2, 3] | [4]       | b(0, 1, 2)       | [4: b()]
//	}
//
//	def "init for name"() {
//		def propName = 'name'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e == null ? null : e.toString()
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e == null ? null : e.toString())
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose   | layers    | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]        | []        | b()              | null
//		[]        | [1]       | b()              | [1: b()]
//		[1]       | []        | b()              | null
//		//相等
//		[1]       | [1]       | b()              | [1: b(0)]
//		[1, 2]    | [1, 2]    | b()              | [1: b(0), 2: b(1)]
//		[1, 2, 3] | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b(2)]
//		//左包含右
//		[1, 2, 3] | [1, 2]    | b()              | [1: b(0), 2: b(1, 2)]
//		[1, 2, 3] | [1]       | b()              | [1: b(0, 1, 2)]
//		//右包含左
//		[1]       | [1, 2, 3] | b()              | [1: b(0), 2: b(), 3: b()]
//		[1, 2]    | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b()]
//		//有交集
//		[1, 2, 3] | [2, 3, 4] | b(0)             | [2: b(1), 3: b(2), 4: b()]
//		//无交集
//		[1, 2, 3] | [4]       | b(0, 1, 2)       | [4: b()]
//	}
//
//	def "init for createTime"() {
//		def propName = 'createTime'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = new Date(e)
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, new Date(e))
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose   | layers    | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]        | []        | b()              | null
//		[]        | [1]       | b()              | [1: b()]
//		[1]       | []        | b()              | null
//		//相等
//		[1]       | [1]       | b()              | [1: b(0)]
//		[1, 2]    | [1, 2]    | b()              | [1: b(0), 2: b(1)]
//		[1, 2, 3] | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b(2)]
//		//左包含右
//		[1, 2, 3] | [1, 2]    | b()              | [1: b(0), 2: b(1, 2)]
//		[1, 2, 3] | [1]       | b()              | [1: b(0, 1, 2)]
//		//右包含左
//		[1]       | [1, 2, 3] | b()              | [1: b(0), 2: b(), 3: b()]
//		[1, 2]    | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b()]
//		//有交集
//		[1, 2, 3] | [2, 3, 4] | b(0)             | [2: b(1), 3: b(2), 4: b()]
//		//无交集
//		[1, 2, 3] | [4]       | b(0, 1, 2)       | [4: b()]
//	}
//
//	def "init for enabled"() {
//		def propName = 'enabled'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e)
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose        | layers        | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]             | []            | b()              | null
//		[]             | [true]        | b()              | [true: b()]
//		[]             | [false]       | b()              | [false: b()]
//		[true]         | []            | b(0)             | null
//		[false]        | []            | b(0)             | null
//
//		[true]         | [true]        | b()              | [true: b(0)]
//		[true]         | [false]       | b(0)             | [false: b()]//fixme
//		[true]         | [true, false] | b()              | [true: b(0), false: b()]
//		[true]         | [false, true] | b()              | [true: b(0), false: b()]
//
//		[false]        | [false]       | b()              | [false: b(0)]
//		[false]        | [true]        | b(0)             | [true: b()]
//		[false]        | [true, false] | b()              | [true: b(), false: b(0)]
//		[false]        | [false, true] | b()              | [true: b(), false: b(0)]
//
//		[true, true]   | [true]        | b()              | [true: b(0, 1)]
//		[true, true]   | [false]       | b(0, 1)          | [false: b()]
//		[true, true]   | [true, false] | b()              | [true: b(0, 1), false: b()]
//		[true, true]   | [false, true] | b()              | [true: b(0, 1), false: b()]
//
//		[false, false] | [true]        | b(0, 1)          | [true: b()]
//		[false, false] | [false]       | b()              | [false: b(0, 1)]
//		[false, false] | [true, false] | b()              | [true: b(), false: b(0, 1)]
//		[false, false] | [false, true] | b()              | [true: b(), false: b(0, 1)]
//
//		[true, false]  | [true]        | b(1)             | [true: b(0)]
//		[true, false]  | [false]       | b(0)             | [false: b(1)]
//		[true, false]  | [true, false] | b()              | [true: b(0), false: b(1)]
//		[true, false]  | [false, true] | b()              | [true: b(0), false: b(1)]
//
//		[false, true]  | [true]        | b(0)             | [true: b(1)]
//		[false, true]  | [false]       | b(1)             | [false: b(0)]
//		[false, true]  | [false, true] | b()              | [true: b(1), false: b(0)]
//		[false, true]  | [true, false] | b()              | [true: b(1), false: b(0)]
//	}
//
//	def "init for age"() {
//		def propName = 'age'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e)
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose   | layers    | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]        | []        | b()              | null
//		[]        | [1]       | b()              | [1: b()]
//		[1]       | []        | b()              | null
//		//相等
//		[1]       | [1]       | b()              | [1: b(0)]
//		[1, 2]    | [1, 2]    | b()              | [1: b(0), 2: b(1)]
//		[1, 2, 3] | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b(2)]
//		//左包含右
//		[1, 2, 3] | [1, 2]    | b()              | [1: b(0), 2: b(1, 2)]
//		[1, 2, 3] | [1]       | b()              | [1: b(0, 1, 2)]
//		//右包含左
//		[1]       | [1, 2, 3] | b()              | [1: b(0), 2: b(), 3: b()]
//		[1, 2]    | [1, 2, 3] | b()              | [1: b(0), 2: b(1), 3: b()]
//		//有交集
//		[1, 2, 3] | [2, 3, 4] | b(0)             | [2: b(1), 3: b(2), 4: b()]
//		//无交集
//		[1, 2, 3] | [4]       | b(0, 1, 2)       | [4: b()]
//	}
//
//	def "init for money"() {
//		def propName = 'money'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e.floatValue())
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose         | layers          | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]              | []              | b()              | null
//		[]              | [1]             | b()              | [1F: b()]
//		[1]             | []              | b()              | null
//		//相等
//		[1]             | [1]             | b()              | [1F: b(0)]
//		[1, 2]          | [1, 2]          | b()              | [1F: b(0), 2F: b(1)]
//		[1, 2, 3]       | [1, 2, 3]       | b()              | [1F: b(0), 2F: b(1), 3F: b(2)]
//		[1.1, 2.2, 3.2] | [1.1, 2.2, 3.2] | b()              | [1.1: b(0), 2.2: b(1), 3.2: b(2)]
//		//左包含右
//		[1, 2, 3]       | [1, 2]          | b()              | [1F: b(0), 2F: b(1, 2)]
//		[1.1, 2.2, 3.2] | [1.1, 2.2]      | b()              | [1.1: b(0), 2.2: b(1, 2)]
//		[1, 2, 3]       | [1]             | b()              | [1F: b(0, 1, 2)]
//		//右包含左
//		[1]             | [1, 2, 3]       | b()              | [1F: b(0), 2F: b(), 3F: b()]
//		[1, 2]          | [1, 2, 3]       | b()              | [1F: b(0), 2F: b(1), 3F: b()]
//		[1.1, 2.2]      | [1.1, 2.2, 3.2] | b()              | [1.1: b(0), 2.2: b(1), 3.2: b()]
//		//有交集
//		[1, 2, 3]       | [2, 3, 4]       | b(0)             | [2F: b(1), 3F: b(2), 4F: b()]
//		[1.1, 2.2, 3.2] | [2.2, 3.2, 4.5] | b(0)             | [2.2: b(1), 3.2: b(2), 4.5: b()]
//		//无交集
//		[1, 2, 3]       | [4]             | b(0, 1, 2)       | [4F: b()]
//		[1.1, 2.2, 3.2] | [4.5]           | b(0, 1, 2)       | [4.5: b()]
//	}
//
//	def "init for color"() {
//		def propName = 'color'
//		def propHandler = propHandlers[propName]
//		def source = []
//		for (def e in suppose) {
//			def helper = new BitSetTestHelper()
//			helper[propName] = e
//			source.add(helper)
//		}
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in layers) {
//			propAndRefVals.put(propHandler, e)
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		toTest.source == source
//		toTest.propAndRefVals == propAndRefVals
//		for (def entry in toTest.shelves.entrySet()) {
//			assert entry.key == propHandler
//			def shelf = entry.value
//			shelf.propHandler == propHandler
//			assert shelf.otherValLayer.locations == expectOtherFlags
//			assert toValFlagsMap(shelf.valLayerMapping) == keyToStr(expectValToBitsetMapping)
//		}
//		where:
//		suppose            | layers                | expectOtherFlags | expectValToBitsetMapping
//		//空
//		[]                 | []                    | b()              | null
//		[]                 | [RED]                 | b()              | [RED: b()]
//		[RED]              | []                    | b()              | null
//		//相等
//		[RED]              | [RED]                 | b()              | [RED: b(0)]
//		[RED, BLUE]        | [RED, BLUE]           | b()              | [RED: b(0), BLUE: b(1)]
//		[RED, BLUE, GREEN] | [RED, BLUE, GREEN]    | b()              | [RED: b(0), BLUE: b(1), GREEN: b(2)]
//		//左包含右
//		[RED, BLUE, GREEN] | [RED, BLUE]           | b(2)             | [RED: b(0), BLUE: b(1)]
//		[RED, BLUE, GREEN] | [RED]                 | b(1, 2)          | [RED: b(0)]
//		//右包含左
//		[RED]              | [RED, BLUE, GREEN]    | b()              | [RED: b(0), BLUE: b(), GREEN: b()]
//		[RED, BLUE]        | [RED, BLUE, GREEN]    | b()              | [RED: b(0), BLUE: b(1), GREEN: b()]
//		//有交集
//		[RED, BLUE, GREEN] | [BLUE, GREEN, YELLOW] | b(0)             | [BLUE: b(1), GREEN: b(2), YELLOW: b()]
//		//无交集
//		[RED, BLUE, GREEN] | [YELLOW]              | b(0, 1, 2)       | [YELLOW: b()]
//	}
//
//	def "Filter"() {
//		def size = 3
//		def source = randomSource(size)
//		def propAndRefVals = ArrayListMultimap.create()
//		for (def e in propHandlers.entrySet()) {
//			for (def layer in layers) {
//				propAndRefVals.put(propHandler, layer)
//			}
//		}
//		when:
//		def toTest = new BitSetTool(source, propAndRefVals)
//		then:
//		true//todo
//		where:
//		propHandlers                | _
//		[id: [1, 2, 3], name: [""]] | _
//	}
//
//	def "AddRecords"() {
//		expect: true//todo
//	}
//
//	def "DelRecords"() {
//		expect: true//todo
//	}
//
//	private static b(int ... bits) {
//		def bs = new BitSet()
//		for (def i in bits) {
//			bs.set(i)
//		}
//		bs
//	}
//
//	private static List<BitSetTestHelper> randomSource(int size) {
//		def rt = []
//		for (int i = 0; i < size; i++) {
//			rt.add(new BitSetTestHelper(id: randL(), name: randS(), createTime: randT(), enabled: randB(), age: randI(),
//					money: randD(), color: randC()))
//		}
//		rt
//	}
//
//	private static Color randC() {
//		values()[TestSupport.RANDOM.nextInt(values().size())]
//	}
//
//	private static keyToStr(Map map) {
//		def rt = Maps.newHashMap()
//		for (def e in map.entrySet()) {
//			def key = e.key
//			rt.put(key == null ? null : key.toString(), e.value)
//		}
//		rt
//	}
//
//	private static Map toValFlagsMap(Map valToLayer) {
//		Map rt = Maps.newHashMap()
//		for (def e in valToLayer.entrySet()) {
//			def key = e.key
//			if (key instanceof Date) {
//				key = key.time
//			}
//			rt.put(key == null ? null : key.toString(), e.value.locations)
//		}
//		rt
//	}
//
//	static class BitSetTestHelper {
//		Long id
//		String name
//		Date createTime
//		boolean enabled
//		int age
//		float money
//		Color color
//	}
//
//	static enum Color {
//		YELLOW, RED, GREEN, BLUE
//	}

}
