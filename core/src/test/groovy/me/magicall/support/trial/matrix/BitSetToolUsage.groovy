/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

//package me.magicall.support.trial.matrix
//
//import com.google.common.collect.ArrayListMultimap
//import me.magicall.support.TestSupport
//import me.magicall.support.trial.matrix.BitSetToolTest.BitSetTestHelper
//import me.magicall.support.trial.matrix.BitSetToolTest.Color
//import me.magicall.support.trial.matrix.comparison.*
//
//import java.text.SimpleDateFormat
//import java.util.function.Function
//
//import static me.magicall.support.trial.matrix.BitSetToolTest.Color.*
//
///**
// * @author Liang Wenjian.
// */
//class BitSetToolUsage extends TestSupport {
//	static idHandler = PropHandler.comparableProp(long.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.id
//		}
//	})
//	static nameHandler = PropHandler.comparableProp(String.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.name
//		}
//	})
//	static createTimeHandler = PropHandler.comparableProp(Date.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.createTime
//		}
//	})
//	static enabledHandler = PropHandler.comparableProp(boolean.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.enabled
//		}
//	})
//	static ageHandler = PropHandler.comparableProp(int.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.age
//		}
//	})
//	static moneyHandler = PropHandler.comparableProp(float.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.money
//		}
//	})
//	static colorHandler = PropHandler.comparableProp(Color.class, new Function<BitSetTestHelper, Object>() {
//		@Override
//		Object apply(final BitSetTestHelper o) {
//			o.color
//		}
//	})
//	static dateFormat = new SimpleDateFormat('yyyy-MM-dd')
//
//	static t(str) {
//		dateFormat.parse(str)
//	}
//
//	private static Color randC() {
//		values()[RANDOM.nextInt(values().size())]
//	}
//
//	static void main(String[] args) {
//		//构建源列表
//		def size = 10
//		def source = []
//		def idIndex = 10000
//		def startDate = dateFormat.parse('1997-07-01')
//		def maxMoney = 50000.00
//		for (int i = 0; i < size; i++) {
//			def calendar = Calendar.instance
//			calendar.time = startDate
//			calendar.add(Calendar.DATE, RANDOM.nextInt(365 * 40))
//			source.add(new BitSetTestHelper(
//					id: idIndex++,
//					name: "",
//					createTime: calendar.time,
//					enabled: RANDOM.nextBoolean(),
//					age: RANDOM.nextInt(100),
//					money: RANDOM.nextFloat() * maxMoney,
//					color: randC()))
//		}
//		println 'id:' + idIndex
//		println 'source size:' + source.size()
//		println source
//		//构建要测试的对象
//		def propAndRefVals = ArrayListMultimap.create()
//		propAndRefVals.putAll(idHandler, [10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000])
////		propAndRefVals.putAll(nameHandler,[])//todo
//		propAndRefVals.putAll(createTimeHandler, [t('2000-01-01'), t('2008-01-01'), t('2009-01-01'), t('2010-01-01')])
//		propAndRefVals.putAll(enabledHandler, [true, false])
//		propAndRefVals.putAll(ageHandler, [20, 30, 40, 50, 60])
//		propAndRefVals.putAll(moneyHandler, [500.00, 1000.00, 2000.00, 3000.00, 4000.00, 5000.00, 10000.00])
//		propAndRefVals.putAll(colorHandler, [RED, YELLOW, BLUE, GREEN])
//		def toTest = new BitSetTool<BitSetTestHelper>(source, propAndRefVals)
//		//构建过滤条件
//		def idLess = new PropMatcher(idHandler, IsLess.inNatureOrder(), [100000])//将找到90000层，前面8层都算；因90000层最高，还要遍历此层
//		def nameMatch = null//todo
//		def createTimeGreater = new PropMatcher(createTimeHandler, IsGreater.inNatureOrder(), [t('2008-08-08')])
//		//将找到2008-01-01层，后面两层都算；然后加入本层8月8日之后的日子
//		def enabledEq = new PropMatcher(enabledHandler, ComparingEquals.inNatureOrder(), [true])
//		def ageBetween = new PropMatcher(ageHandler, IsInClosedClosedInterval.inNatureOrder(), [20, 30])
//		def moneyGe = new PropMatcher(moneyHandler, IsGreaterEqual.NATURE_ORDER, [10000.00])
//		def colorIn = new PropMatcher(colorHandler, IsGreaterEqual.NATURE_ORDER, [RED, YELLOW])
//		//计算耗时
//		def start = System.currentTimeMillis()
//		def result = toTest.filter(idLess & createTimeGreater & enabledEq & ageBetween & moneyGe & colorIn)
//		def cost = System.currentTimeMillis() - start
//		println cost + ' ms'
//
//		//检查结果正确性
//		for (def r in result) {
//			assert idLess.matches(r)
//			assert createTimeGreater.matches(r)
//			assert enabledEq.matches(r)
//			assert ageBetween.matches(r)
//			assert moneyGe.matches(r)
//			assert colorIn.matches(r)
//		}
//		//检查非结果的正确性
//		source.removeAll(result)
//		for (def r in source) {
//			assert !idLess.matches(r) || !createTimeGreater.matches(r) || !enabledEq.matches(r) || !ageBetween.matches(r) || !moneyGe.matches(r) || !colorIn.matches(r)
//		}
//	}
//}
