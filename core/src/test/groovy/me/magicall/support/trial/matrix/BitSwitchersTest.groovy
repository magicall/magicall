/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

//package me.magicall.support.trial.matrix
//
//import com.google.common.collect.Sets
//import me.magicall.support.coll.BitSwitchers
//import spock.lang.Specification
//
///**
// * @author Liang Wenjian.
// */
//class BitSwitchersTest extends Specification {
//	private static set = Sets.newTreeSet([0, 1, 6, 9, 100, -0, -1, -100])
//	private static edge = Sets.newTreeSet(set + [-Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE])
//
//
//	def "Iterator"() {
//		def sorted = sort(set)
//		def toTest = new BitSwitchers(set)
//		def iterator = toTest.iterator()
//		expect:
//		for (int i = 0; i < set.size(); i++) {
//			assert iterator.hasNext()
//			assert iterator.next() == sorted[i]
//			iterator.remove()
//			assert !toTest.contains(sorted[i])
//		}
//		!iterator.hasNext()
//		toTest.toArray() == [].toArray()
//	}
//
//	def "Size"() {
//		def toTest = new BitSwitchers()
//		expect:
//		toTest.size() == 0
//	}
//
//	def "Add"() {
//		def toTest = new BitSwitchers()
//		expect:
//		toTest.add(1)
//		toTest.size() == 1
//		sort(toTest) == sort([1])
//
//		toTest.add(1) == false
//		toTest.size() == 1
//		sort(toTest) == sort([1])
//
////		toTest.add(Integer.MAX_VALUE)
////		toTest.size() == 2
////		sort(toTest) == sort([1, Integer.MAX_VALUE])
//	}
//
//	def "add -?"() {
//		def toTest = new BitSwitchers()
//		expect:
//		toTest.add(-1)
//		toTest.size() == 1
//		sort(toTest) == sort([-1])
//
//		toTest.add(-1) == false
//		toTest.size() == 1
//		sort(toTest) == sort([-1])
//
//		toTest.add(-11)
//		toTest.size() == 2
//		sort(toTest) == sort([-1, -11])
//
////		toTest.add(Integer.MIN_VALUE)
////		toTest.size() == 3
////		sort(toTest) == sort([-1, -11, Integer.MIN_VALUE])
//	}
//
//	def "Remove"() {
//		def toTest = new BitSwitchers()
//		toTest.addAll(set)
//		def expectR = Sets.newHashSet(set)
//		expect:
//		for (def i in set) {
//			assert toTest.remove(i)
//			assert expectR.remove(i)
//			assert toTest.size() == expectR.size()
//			assert sort(toTest) == sort(expectR)
//			assert toTest.remove(i) == false
//			assert expectR.remove(i) == false
//			assert toTest.size() == expectR.size()
//			assert sort(toTest) == sort(expectR)
//		}
//		assert toTest.isEmpty()
//		assert expectR.isEmpty()
//	}
//
//	def "containsAll other collection"() {
//		expect:
//		new BitSwitchers([1, 2, 3, 4]).containsAll(set) == false
//		new BitSwitchers(set).containsAll(set)
//		new BitSwitchers(set + 2).containsAll(set)
//		new BitSwitchers(set + [2, 3]).containsAll(set)
//	}
//
//	def "containsAll BitSwitchers"() {
//		def b1 = new BitSwitchers(set)
//		def b2 = new BitSwitchers([1, 2, 3, 4])
//		def b3 = new BitSwitchers(set.subSet(1, 9))
//		expect:
//		b1.containsAll(b2) == false
//		b1.containsAll(b3)
//	}
//
//	def "retainsAll"() {
//		def b1 = new BitSwitchers([1, 2, 3, 4])
//		def b2 = new BitSwitchers(set)
//		def b3 = new BitSwitchers(set + 2)
//		b3.remove(1)
//		expect:
//		b1.retainAll(set)
//		b1.toArray() == [1].toArray()
//		b2.retainAll(set) == false
//		sort(b2) == sort(set)
//		b3.retainAll(set)
//		sort(b3) == sort(set - 1)
//	}
//
//	def "retainsAll BitSwitchers"() {
//		def b1 = new BitSwitchers([1, 2, 3, 4])
//		def b2 = new BitSwitchers(set)
//		b1.retainAll(b2)
//
//		def b3 = new BitSwitchers(set + 2)
////		b3.retainAll(b2)
//// todo:这里会OutOfMemory，似乎因为Integer.MIN_VALUE~Integer.MAX_VALUE这个范围对于BitSet来说太大了，clone时很容易内存不够。
//// 但单单针对BitSet，把0~Integer.MAX_VALUE都置位然后克隆，不会oom。
//		expect:
//		sort(b1) == [1].toArray()
////		sort(b3)==sort(other.toArray())
//	}
//
//	def "first"() {
//		when:
//		def toTest = new BitSwitchers(set)
//		def other = Sets.newTreeSet(set)
//		then:
//		while (!toTest.isEmpty()) {
//			assert toTest.first() == other.first()
//			assert toTest.pollFirst() == other.pollFirst()
//		}
//
//		when:
//		toTest.first()
//		then:
//		thrown(NoSuchElementException)
//	}
//
//	def "last"() {
//		when:
//		def toTest = new BitSwitchers(set)
//		def other = Sets.newTreeSet(set)
//		then:
//		while (!toTest.isEmpty()) {
//			assert toTest.last() == other.last()
//			assert toTest.pollLast() == other.pollLast()
//		}
//
//		when:
//		toTest.last()
//		then:
//		thrown(NoSuchElementException)
//	}
//
//	def "tail"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		sort(toTest.tailSet(0)) == sort(set.tailSet(0))
//		sort(toTest.tailSet(1)) == sort(set.tailSet(1))
//		sort(toTest.tailSet(2)) == sort(set.tailSet(2))
//		sort(toTest.tailSet(Integer.MAX_VALUE)) == sort(set.tailSet(Integer.MAX_VALUE))
//
//		sort(toTest.tailSet(-1)) == sort(set.tailSet(-1))
//		sort(toTest.tailSet(-100)) == sort(set.tailSet(-100))
//		sort(toTest.tailSet(-101)) == sort(set.tailSet(-101))
//		sort(toTest.tailSet(-Integer.MAX_VALUE)) == sort(set.tailSet(-Integer.MAX_VALUE))
//		sort(toTest.tailSet(Integer.MIN_VALUE)) == sort(set.tailSet(Integer.MIN_VALUE))
//	}
//
//	def "head"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		sort(toTest.headSet(0)) == sort(set.headSet(0))
//		sort(toTest.headSet(1)) == sort(set.headSet(1))
//		sort(toTest.headSet(2)) == sort(set.headSet(2))
//		sort(toTest.headSet(Integer.MAX_VALUE)) == sort(set.headSet(Integer.MAX_VALUE))
//
//		sort(toTest.headSet(-1)) == sort(set.headSet(-1))
//		sort(toTest.headSet(-100)) == sort(set.headSet(-100))
//		sort(toTest.headSet(-101)) == sort(set.headSet(-101))
//		sort(toTest.headSet(-Integer.MAX_VALUE)) == sort(set.headSet(-Integer.MAX_VALUE))
//		sort(toTest.headSet(Integer.MIN_VALUE)) == sort(set.headSet(Integer.MIN_VALUE))
//	}
//
//	def "lower"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		for (def i in set) {
//			assert toTest.lower(i) == set.lower(i)
//			if (i != Integer.MIN_VALUE) {
//				assert toTest.lower(i - 1) == set.lower(i - 1)
//			}
//		}
//	}
//
//	def "floor"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		for (def i in set) {
//			assert toTest.floor(i) == set.floor(i)
//			if (i != Integer.MIN_VALUE) {
//				assert toTest.floor(i - 1) == set.floor(i - 1)
//			}
//		}
//	}
//
//	def "higher"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		for (def i in set) {
//			assert toTest.higher(i) == set.higher(i)
//			if (i != Integer.MAX_VALUE) {
//				assert toTest.higher(i + 1) == set.higher(i + 1)
//			}
//		}
//	}
//
//	def "ceiling"() {
//		def toTest = new BitSwitchers(set)
//		expect:
//		for (def i in set) {
//			assert toTest.ceiling(i) == set.ceiling(i)
//			if (i != Integer.MAX_VALUE) {
//				assert toTest.ceiling(i + 1) == set.ceiling(i + 1)
//			}
//		}
//	}
//
//	def "pollFirst"() {
//		def toTest = new BitSwitchers()
//		def other = (TreeSet) set.clone()
//		expect:
//		toTest.pollFirst() == null
//		toTest.addAll(other)
//		while (!toTest.isEmpty()) {
//			assert toTest.pollFirst() == other.pollFirst()
//		}
//		toTest.isEmpty()
//		other.isEmpty()
//	}
//
//	def "pollLast"() {
//		def toTest = new BitSwitchers()
//		def other = (TreeSet) set.clone()
//		expect:
//		toTest.pollLast() == null
//		toTest.addAll(other)
//		while (!toTest.isEmpty()) {
//			assert toTest.pollLast() == other.pollLast()
//		}
//		toTest.isEmpty()
//		other.isEmpty()
//	}
//
//	static sort(Collection coll) {
//		def a = Sets.newHashSet(coll).toArray()
//		Arrays.sort(a)
//		a
//	}
//}
