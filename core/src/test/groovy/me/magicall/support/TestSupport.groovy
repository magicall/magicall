/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.support

import spock.lang.Specification

/**
 * @author Liang Wenjian.
 */
class TestSupport extends Specification {
	static Random RANDOM = new Random()

	static String randS() {
		String.valueOf(RANDOM.nextLong())
	}

	static int randI() {
		RANDOM.nextInt()
	}

	static int randI(int bound) {
		RANDOM.nextInt(bound)
	}

	static long randL() {
		RANDOM.nextLong()
	}

	static float randF() {
		RANDOM.nextFloat()
	}

	static double randD() {
		RANDOM.nextDouble()
	}

	static boolean randB() {
		RANDOM.nextBoolean()
	}

	static Date randT() {
		new Date(randL())
	}
}
