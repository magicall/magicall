/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import com.google.common.collect.Range;

import java.time.Instant;

/**
 * 实现此接口，表示对象有“开始时间”和“结束时间”。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface HasPeriod extends HasStartTime, HasEndTime {
	/**
	 * 获取有效期。
	 *
	 * @return 返回null表示既没有开始时间也没有结束时间。
	 */
	Range<Instant> period();

	@Override
	default Instant endTime() {
		final var period = period();
		if (period == null || !period.hasUpperBound()) {
			return null;
		}
		return period.upperEndpoint();
	}

	@Override
	default Instant startTime() {
		final var period = period();
		if (period == null || !period.hasLowerBound()) {
			return null;
		}
		return period.lowerEndpoint();
	}

	@Override
	default boolean includeEndingTime() {
		final var period = period();
		if (period == null) {
			return false;
		}
		return period.hasUpperBound();
	}

	@Override
	default boolean includeStartingTime() {
		final var period = period();
		if (period == null) {
			return false;
		}
		return period.hasLowerBound();
	}
}
