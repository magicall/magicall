/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public interface TimeConst {

	long SECOND = TimeUnit.SECONDS.toMillis(1);
	long MINUTE = TimeUnit.MINUTES.toMillis(1);
	long HOUR = TimeUnit.HOURS.toMillis(1);
	/**
	 * 8小时的毫秒数.用于时区计算.中国是+8时区.
	 */
	long HOUR8 = TimeUnit.HOURS.toMillis(8);
	long DAY7 = TimeUnit.DAYS.toMillis(7);
	long DAY28 = TimeUnit.DAYS.toMillis(28);
	long DAY29 = TimeUnit.DAYS.toMillis(29);
	long DAY30 = TimeUnit.DAYS.toMillis(30);
	long DAY31 = TimeUnit.DAYS.toMillis(31);
	long DAY365 = TimeUnit.DAYS.toMillis(365);
	long YEAR_COMMON = DAY365;
	long LEAP_DAY_END_IN_4Y = YEAR_COMMON * 2 + DAY31 + DAY29;
	long LEAP_DAY_START_IN_4Y = YEAR_COMMON * 2 + DAY31 + DAY28;
	long YEAR2 = YEAR_COMMON * 2;
	long DAY366 = TimeUnit.DAYS.toMillis(366);
	long YEAR_LEAP = DAY366;
	long YEAR4 = YEAR_COMMON * 3 + YEAR_LEAP;

	interface YearConst {
		/**
		 * java日历的时间原点的年份
		 */
		int START_YEAR = 1970;
		/**
		 * java日历中时间原点后的第一个闰年
		 */
		int FIRST_LEAP_YEAR = 1972;
	}

	interface DateConst {

		/**
		 * java long型能表示的最小时间
		 */
		Date START = new Date(Long.MIN_VALUE);
		/**
		 * 0毫秒的时间
		 */
		Date _0 = new Date(0);
		/**
		 * 0毫秒的时间
		 */
		Date _1970_1_1 = _0;
		/**
		 * java long型能表示的最大时间
		 */
		Date END = new Date(Long.MAX_VALUE);
	}
}
