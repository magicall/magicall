/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Deprecated
public enum TimeFormatter {
	// 以下排序,是按使用率从高到低排列.保证parse的时候能最快地遇到合适的pattern出来

	/**
	 * yyyy-MM-dd HH:mm:ss.SSS
	 */
	Y4_M2_D2_H2_MIN2_S2_MS3("yyyy-MM-dd HH:mm:ss.SSS"),
	/**
	 * 按照url格式的日期,其中日期和时间中间的空格被改成+ yyyy-MM-dd+HH:mm:ss
	 */
	Y4_M2_D2_H2_MIN2_S2_URL("yyyy-MM-dd+HH:mm:ss"),
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	Y4_M2_D2_H2_MIN2_S2("yyyy-MM-dd HH:mm:ss"),
	/**
	 * 年(2位)-月-日 时:分:秒.从00到29解析为20xx年,从30到99解析为19xx年 这个也可以解析 yyyy-MM-dd
	 * HH:mm:ss
	 */
	Y2_M2_D2_H2_MIN2_S2("yy-MM-dd HH:mm:ss"),
	/**
	 * yyyy-MM-dd HH:mm
	 */
	Y4_M2_D2_H2_M2("yyyy-MM-dd HH:mm"),
	/**
	 * yy-MM-dd HH:mm
	 */
	Y2_M2_D2_H2_MIN2("yy-MM-dd HH:mm"),
	/**
	 * yyyy-MM-dd
	 */
	Y4_M2_D2("yyyy-MM-dd"),
	/**
	 * yyyyMMddHHmmssSSS
	 */
	Y4M2D2H2MIN2S2MS3("yyyyMMddHHmmssSSS"),
	/**
	 * yyyyMMdd
	 */
	Y4M2D2("yyyyMMdd"),
	/**
	 * yyyy年MM月dd日
	 */
	Y4年M2月D2日("yyyy年MM月dd日"),
	Y4年M2月D2日H2时M2分S2秒("yyyy年MM月dd日HH时mm分ss秒"),
	/**
	 * MM-dd
	 */
	M2_D2("MM-dd"),
	/**
	 * yyyy-MM
	 */
	Y4_M2("yyyy-MM"),
	/**
	 * HH:mm
	 */
	H2_MIN2("HH:mm"),
	/**
	 * M.d
	 */
	M_D("M.d"),
	/**
	 * HHmm
	 */
	H2MIN2("HHmm"),
	/**
	 * yyyy-MM-dd-HH-mm-ss log4j默认格式
	 */
	Y4_M2_D2_H2_m2_s2("yyyy-MM-dd-HH-mm-ss"),
	M("M"),
	;
	public final String pattern;

	TimeFormatter(final String pattern) {
		this.pattern = pattern;
	}

	public static String tryToFormat(final Date date) {
		final var formatters = values();
		for (final var f : formatters) {
			try {
				return f.format(date);
			} catch (final Exception ignored) {
			}
		}
		return date.toString();
	}

	public static Date tryToParse(final String s, final ParsePosition pos) {
		final var index = pos.getIndex();
		final var formatters = values();
		for (final var f : formatters) {
			final var date = f.parse(s, pos);
			if (date == null) {
				pos.setIndex(index);
			} else {
				return date;
			}
		}
		return null;
	}

	public static Date tryToParse(final String s) {
		final var formatters = values();
		return Arrays.stream(formatters).map(f -> f.parse(s, new ParsePosition(0))).filter(Objects::nonNull).findFirst()
				.orElse(null);
	}

	public String format(final Date date) {
		return new SimpleDateFormat(pattern).format(date);
	}

	public Date parse(final String s) {
		try {
			return new SimpleDateFormat(pattern).parse(s);
		} catch (final ParseException ignored) {
			return null;
		}
	}

	public Date parse(final String s, final ParsePosition pos) {
		return new SimpleDateFormat(pattern).parse(s, pos);
	}

	@Override
	public String toString() {
		return pattern;
	}
}
