/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import com.google.common.collect.Range;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.time.TimeConst.YearConst;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public interface TimeKit {
	String YYYY_MM = "yyyy-MM";
	String YYYY_MM_DD = "yyyy-MM-dd";
	Pattern YYYY_MM_PATTERN = Pattern.compile("\\d{4}-\\d{2}");
	Pattern YYYY_MM_DD_PATTERN = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
	Pattern YYYY_PATTERN = Pattern.compile("\\d{4}");

	/**
	 * 闰日。
	 * 正式的单词应该是intercalary day，但这单词太生僻了……
	 */
	MonthDay LEAP_DAY = MonthDay.of(Month.FEBRUARY, 29);

	/**
	 * 指定日期的上一天。
	 *
	 * @return
	 */
	static Date dateBefore(final Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -1);
		return calendar.getTime();
	}

	/**
	 * 指定日期的下一天。
	 *
	 * @return
	 */
	static Date dateAfter(final Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}

	/**
	 * 是否符合年份格式（4位数）。
	 *
	 * @return
	 */
	static boolean isYear(final String s) {
		return YYYY_PATTERN.matcher(s).matches();
	}

	static boolean isYearMonth(final String s) {
		return YYYY_MM_PATTERN.matcher(s).matches();
	}

	static boolean isYearMonthDate(final String s) {
		return YYYY_MM_DD_PATTERN.matcher(s).matches();
	}

	/**
	 * 若指定日期不晚于今天则返回指定日期，否则返回今天。
	 *
	 * @return
	 */
	static Date notLaterThanToday(final Date date) {
		return min(date, new Date());
	}

	static Date min(final Date d1, final Date d2) {
		return d1.after(d2) ? d2 : d1;
	}

	static boolean isSameYear(final Date d1, final Date d2) {
		return yearNumOf(d1) == yearNumOf(d2);
	}

	/**
	 * 直接减一年。
	 * 注意：实测闰日减一年会变成去年2月28日（跟闰日前一天减一年一致）
	 *
	 * @return
	 */
	static Date lastYear(final Date date) {
		final Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, -1);
		return c.getTime();
	}

	/**
	 * 指定时间段内是否包含闰日。
	 *
	 * @param timeRange 可跨越多年。
	 * @return
	 */
	static boolean includingLeapDay(final Range<Date> timeRange) {
		return countLeapDays(timeRange) == 0;
	}

	/**
	 * 指定时间段内包含几个闰日。
	 *
	 * @param timeRange 可跨越多年。
	 * @return
	 */
	static int countLeapDays(final Range<Date> timeRange) {
		final Range<Date> closed = toCloseDateRange(timeRange);
		final LocalDate localDate1 = LocalDate.from(closed.lowerEndpoint().toInstant());
		final LocalDate localDate2 = LocalDate.from(closed.upperEndpoint().toInstant());
		final int year1 = localDate1.getYear();
		final int year2 = localDate2.getYear();
		return (int) IntStream.range(localDate1.isLeapYear() ? year1 : year1 + 1,
						localDate2.isLeapYear() ? year2 : year2 - 1).mapToObj(Year::of).filter(Year::isLeap)//该时间段包含的所有闰年
				.map(year -> year.atMonthDay(LEAP_DAY))//所有闰日
				.filter(leapDate -> leapDate.equals(localDate1)//这里会多比较几次leapDate vs localDate1、localDate2
						|| leapDate.equals(localDate2)//
						|| leapDate.isAfter(localDate1) && leapDate.isBefore(localDate2))//
				.count();
	}

	static boolean isLeapDay(final LocalDate date) {
		return MonthDay.from(date).equals(LEAP_DAY);
	}

	static boolean isLeapDay(final Date date) {
		return isLeapDay(LocalDate.from(date.toInstant()));
	}

	static int yearNumOf(final Date date) {
		return date.toInstant().get(ChronoField.YEAR);
	}

	/**
	 * 把不确定的日期区间变为确定的开区间。
	 * 即，若原区间包含某方向的边界，则返回的新区间在该方向的边界向外“移动”一天（因此变为不包含）。
	 * 仅适用于日期区间，不适用于时间区间。
	 *
	 * @return
	 */
	static Range<Date> toOpenDateRange(final Range<Date> dateRange) {
		final Date d1 = dateRange.lowerEndpoint();
		final Date d2 = dateRange.upperEndpoint();
		return Range.open(//
				dateRange.contains(d1) ? dateBefore(d1) : d1,//
				dateRange.contains(d2) ? dateAfter(d2) : d2);
	}

	/**
	 * 把不确定的日期区间变为确定的闭区间。
	 * 即，若原区间不包含某方向的边界，则返回的新区间在该方向的边界向内“移动”一天（因此变为包含）。
	 * 仅适用于日期区间，不适用于时间区间。
	 *
	 * @return
	 */
	static Range<Date> toCloseDateRange(final Range<Date> dateRange) {
		final Date d1 = dateRange.lowerEndpoint();
		final Date d2 = dateRange.upperEndpoint();
		return Range.closed(//
				dateRange.contains(d1) ? d1 : dateAfter(d1),//
				dateRange.contains(d2) ? d2 : dateBefore(d2));
	}

	/**
	 * 时间段是否跨年。
	 *
	 * @return
	 */
	static boolean isDatesCrossingYear(final Range<Date> timeRange) {
		final Date d1 = timeRange.lowerEndpoint();
		final Date d2 = timeRange.upperEndpoint();
		return !isSameYear(timeRange.contains(d1) ? d1 : dateAfter(d1), timeRange.contains(d2) ? d2 : dateBefore(d2));
	}

	static Date parse(final String s) {
		if (Kits.STR.isEmpty(s)) {
			return null;
		}
		final String trimed = s.trim();
		// return parse(defaultType, s);
		// SimpleDateFormat每次都需要new,因为其不是线程安全的!
		final var format = new SimpleDateFormat();
		for (final var f : TimeFormatter.values()) {
			// ParsePosition每次都要new,因为它在parse方法里会被改变!
			format.applyPattern(f.pattern);
			final var d = format.parse(trimed, new ParsePosition(0));
			if (d != null) {
				return d;
			}
		}
		return null;
	}

	/**
	 * 判断两个时间是否在同一天内.如果escapeYear为true,则不计较年份.可用于节日,生日等每年都有的日子
	 *
	 * @param escapeYear 如果escapeYear为true,则不计较年份
	 * @return
	 */
	static boolean isTheSameDay(final Date day1, final Date day2, final boolean escapeYear) {
		return isTheSameDay(day1, day2, escapeYear, true);
	}

	static boolean isTheSameDay(final Date day1, final Date day2, final boolean escapeYear,
															final boolean checkSummerTime) {
		return checkSummerTime ? isTheSameDayCheckSummerTime(day1, day2, escapeYear)
													 : isTheSameDayNoSummerTimeCheck(day1, day2, escapeYear);
	}

	private static boolean isTheSameDayCheckSummerTime(final Date day1, final Date day2, final boolean escapeYear) {
		final var c = Calendar.getInstance();
		c.setTime(day1);
		final var y = c.get(Calendar.YEAR);
		final var m = c.get(Calendar.MONTH);
		final var d = c.get(Calendar.DATE);
		c.setTime(day2);
		return (escapeYear || y == c.get(Calendar.YEAR)) && m == c.get(Calendar.MONTH) && d == c.get(Calendar.DATE);
	}

	/**
	 * 已验证从0L毫秒起的4万天正确.1970年以前,2089年以后未验证
	 *
	 * @return
	 */
	private static boolean isTheSameDayNoSummerTimeCheck(final Date day1, final Date day2, final boolean escapeYear) {
		final long t1 = day1.getTime(), t2 = day2.getTime();

		if (escapeYear) {
			final long offset4Y1 = (t1 + TimeConst.HOUR8) % TimeConst.YEAR4, offset4Y2 = (t2 + TimeConst.HOUR8)
					% TimeConst.YEAR4;
			if (offset4Y1 < TimeConst.LEAP_DAY_START_IN_4Y)// d1在闰日前
			{
				if (offset4Y2 < TimeConst.LEAP_DAY_START_IN_4Y) {
					return offset4Y1 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1)
							== offset4Y2 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1);
				} else if (offset4Y2 >= TimeConst.LEAP_DAY_END_IN_4Y) {
					return offset4Y1 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1)
							== (offset4Y2 - TimeUnit.DAYS.toMillis(1)) % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1);
				} else {
					return false;// d2是闰日
				}
			} else if (offset4Y1 >= TimeConst.LEAP_DAY_END_IN_4Y) {
				if (offset4Y2 < TimeConst.LEAP_DAY_START_IN_4Y) {
					return (offset4Y1 - TimeUnit.DAYS.toMillis(1)) % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1)
							== offset4Y2 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1);
				} else if (offset4Y2 >= TimeConst.LEAP_DAY_END_IN_4Y) {
					return offset4Y1 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1)
							== offset4Y2 % TimeConst.YEAR_COMMON / TimeUnit.DAYS.toMillis(1);
				} else {
					return false;
				}
			} else {
				// d1是闰日
				return offset4Y2 >= TimeConst.LEAP_DAY_START_IN_4Y && offset4Y2 < TimeConst.LEAP_DAY_END_IN_4Y;
			}
		} else {
			final long t81 = t1 / TimeConst.HOUR8, t82 = t2 / TimeConst.HOUR8;
			if (t81 == t82) {
				return true;
			}
			return (t81 + 1) / 3 == (t82 + 1) / 3;
		}
	}

	/**
	 * 将指定的年月日时分秒毫秒转化成毫秒数
	 *
	 * @return
	 */
	static long toMillisecond(final int year, final int month, final int date, final int hour, final int minute,
														final int second, final int millisecond) {
		final long y;
		if (year > YearConst.FIRST_LEAP_YEAR) {
			final var lyCount = leapYearCount(year);
			y = (year - YearConst.START_YEAR) * TimeSpan.YEAR.getTime() + lyCount * TimeSpan.DATE.getTime();
		} else {
			y = (year - YearConst.START_YEAR) * TimeSpan.YEAR.getTime();
		}

		final var ms = month > 2 && Year.isLeap(year) ? TimeSpan.msOfLeapYearMonths() : TimeSpan.msOfCommonYearMonths();
		final long m = IntStream.range(0, month - 1).mapToLong(i -> ms.get(i).getTime()).sum();
		return y + m + (date - 1) * TimeSpan.DATE.getTime()//
				+ hour * TimeSpan.HOUR.getTime() + minute * TimeSpan.MINUTE.getTime() + second * TimeSpan.SECOND.getTime()
				+ millisecond;
	}

	/**
	 * 返回从1970年到指定年份间的闰年数
	 *
	 * @return
	 */
	static int leapYearCount(final int year) {
		if (year < YearConst.START_YEAR) {
			throw new IllegalArgumentException("year < " + YearConst.START_YEAR + ':' + year);
		}
		if (year < YearConst.FIRST_LEAP_YEAR) {
			return 0;
		}
		return (year - YearConst.FIRST_LEAP_YEAR) / 4 + 1;
	}

	/**
	 * 获取本时间段所包含的小时数零头,即去掉整天后所剩的小时数(0~23)
	 *
	 * @return
	 */
	static int getHours(final long milliseconds) {
		return (int) (milliseconds % TimeUnit.DAYS.toMillis(1) / TimeConst.HOUR);
	}

	/**
	 * 获取本时间段所包含的分钟数零头,即去掉整小时后所剩的分钟数(0~59)
	 *
	 * @return
	 */
	static int getMinutes(final long milliseconds) {
		return (int) (milliseconds % TimeConst.HOUR / TimeConst.MINUTE);
	}

	/**
	 * 获取本时间段所包含的秒数零头,即去掉整分钟后所剩的秒数(0~59)
	 *
	 * @return
	 */
	static int getSeconds(final long milliseconds) {
		return (int) (milliseconds % TimeConst.MINUTE / TimeConst.SECOND);
	}

	/**
	 * 获取本时间段所包含的毫秒数零头,即去掉整天后所剩的毫秒数(0~999)
	 *
	 * @return
	 */
	static int getMilliseconds(final long milliseconds) {
		return (int) (milliseconds % TimeConst.SECOND);
	}

	static List<Date> thisWeek() {
		final var c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_WEEK, 1);
		var size = 7;
		final List<Date> list = new ArrayList<>(size);
		while (true) {
			list.add(c.getTime());
			--size;
			if (size == 0) {
				break;
			}
			c.add(Calendar.DATE, 1);
		}
		return list;
	}

	static int[] daysOfCommonYearMonth() {
		return Arrays.stream(Month.values()).mapToInt(e -> e.length(false)).toArray();
	}

	static int[] daysOfLeapYearMonth() {
		return Arrays.stream(Month.values()).mapToInt(e -> e.length(true)).toArray();
	}

	static LocalDateTime now() {
		return LocalDateTime.now(ZoneId.systemDefault());
	}
}
