/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import me.magicall.program.lang.java.贵阳DearSun.coll.CompareKit;

import java.time.Instant;
import java.util.Comparator;

/**
 * 实现此接口，表示对象有“结束时间”。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface HasEndTime {

	boolean DEFAULT_INCLUDING_END_TIME = false;

	/**
	 * 获取结束时间。
	 *
	 * @return 返回null表示没有结束时间，即不会过期。
	 */
	Instant endTime();

	/**
	 * 结束时间那个瞬间是否有效。
	 *
	 * @return 是否包含结束时间。
	 */
	default boolean includeEndingTime() {
		return DEFAULT_INCLUDING_END_TIME;
	}

	/**
	 * 当前是否已过期。
	 *
	 * @return 若在指定的时间已过期，则返回true。
	 */
	default boolean isEnded() {
		return isEndedWhen(System.currentTimeMillis());
	}

	/**
	 * 在指定的时间是否已过期。
	 *
	 * @param when 指定的时间。
	 * @return 若在指定的时间已过期，则返回true。
	 */
	default boolean isEndedWhen(final long when) {
		final var endTime = endTime();
		if (endTime == null) {
			return false;
		}
		return isEndedWhen(endTime.toEpochMilli(), when, includeEndingTime());
	}

	//==================================================

	static boolean isEndedWhen(final long endTime, final long when, final boolean includeEndingTime) {
		final var timeOffset = when - endTime;
		return includeEndingTime ? timeOffset >= 0 : timeOffset > 0;
	}

	static Comparator<HasEndTime> endTimeAscNullLast() {
		return CompareKit.keyAscNullLast(HasEndTime::endTime);
	}
}
