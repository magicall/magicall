/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.time.Instant;

/**
 * 实现此接口，表示对象在某个时间将会开始。这个时间通常是在未来。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface WillStart extends HasStartTime {

	/**
	 * 预定的开始时间。为null表示没有预定开始时间，即不会自动开始（此时是否可以通过调用 {@link #start()} 手动开启，取决于 {@link #start()} 的实现）。
	 *
	 * @return 返回null表示没有开始时间，即不会自动开始。
	 */
	default Instant scheduledStartTime() {
		return null;
	}

	/**
	 * 是否设置了预定开始时间。
	 *
	 * @return 是否设置了预定开始时间。
	 */
	default boolean hasScheduledStartTime() {
		return scheduledStartTime() != null;
	}

	/**
	 * 启动。
	 * 默认实现为抛出 {@link OpNotAllowedException}。
	 */
	default void start() {
		throw new OpNotAllowedException(Thing.of(WillStart.class.getSimpleName(), "?"), "start");
	}
}
