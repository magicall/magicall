/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.time;

import me.magicall.program.lang.java.贵阳DearSun.coll.CompareKit;

import java.time.Instant;
import java.util.Comparator;

/**
 * 实现此接口，表示对象有“开始时间”。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface HasStartTime {

	boolean DEFAULT_INCLUDING_START_TIME = true;

	/**
	 * 获取开始时间。
	 * 若未开始，返回预定的开始时间，若无预定开始时间（不会自动开始）则返回null；若已开始，返回实际的开始时间。
	 *
	 * @return
	 */
	Instant startTime();

	/**
	 * “正在开始的时间”是否算在“已开始的时间”内。“正在开始的时间”通常只是 {@link #startTime()} 表示的时间点。
	 * todo：待详细设计“Starting”状态。
	 *
	 * @return 是否包含开始时间。
	 */
	default boolean includeStartingTime() {
		return DEFAULT_INCLUDING_START_TIME;
	}

//	/**
//	 * 已开始的时间段
//	 * @return
//	 */
//	default Range<Instant> startedTimeRange() {
//		final var startTime = startTime();
//		if (startTime == null) {
//			return null;
//		}
//		return includingStartingTime() ? Range.atLeast(startTime) : Range.greaterThan(startTime);
//	}

	/**
	 * 当前是否已经开始。
	 *
	 * @return 若当前时刻已开始则返回true。
	 */
	default boolean isStarted() {
		return isStartedWhen(System.currentTimeMillis());
	}

	/**
	 * 在指定的时间是否已开始。
	 *
	 * @param when 指定的时间。
	 * @return 若在指定的时间已开始，则返回true。
	 */
	default boolean isStartedWhen(final long when) {
		final var startTime = startTime();
		if (startTime == null) {
			return false;
		}
		return isStartedWhen(startTime.toEpochMilli(), when, includeStartingTime());
	}

	//===================================

	static boolean isStartedWhen(final long startTime, final long when, final boolean includeStartingTime) {
		final var timeOffset = when - startTime;
		return includeStartingTime ? timeOffset >= 0 : timeOffset > 0;
	}

	static Comparator<HasStartTime> startTimeAscNullLast() {
		return CompareKit.keyAscNullLast(HasStartTime::startTime);
	}
}
