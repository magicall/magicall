/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

public class TextDto implements Text {
	public String content;

	public TextDto() {
	}

	public TextDto(final String content) {
		this.content = content;
	}

	@Override
	public String content() {
		return content;
	}

	@Override
	public String toString() {
		return Text.str(this);
	}

	@Override
	public int hashCode() {
		return Text.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Text.eq(this, o);
	}
}
