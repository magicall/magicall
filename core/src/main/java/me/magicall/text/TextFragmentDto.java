/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

public class TextFragmentDto implements TextFragment {
	public String title;
	public String content;
	public TextFragment context;

	@Override
	public String title() {
		return title;
	}

	@Override
	public String content() {
		return content;
	}

	@Override
	public TextFragment context() {
		return context;
	}

	@Override
	public String toString() {
		return TextFragment.str(this);
	}

	@Override
	public int hashCode() {
		return TextFragment.hash(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return TextFragment.eq(this, obj);
	}
}
