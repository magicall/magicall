/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.msg;

import me.magicall.Thing;

import java.time.Instant;

public class MsgDto implements Msg {
	public Thing sender;
	public Instant sendTime;
	public Thing receiver;
	public Instant receiveTime;
	public String content;

	@Override
	public Thing sender() {
		return sender;
	}

	@Override
	public Instant sendTime() {
		return sendTime;
	}

	@Override
	public Thing receiver() {
		return receiver;
	}

	@Override
	public Instant receiveTime() {
		return receiveTime == null ? Msg.super.receiveTime() : receiveTime;
	}

	@Override
	public String content() {
		return content;
	}

	@Override
	public String toString() {
		return Msg.str(this);
	}

	@Override
	public int hashCode() {
		return Msg.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Msg.eq(this, other);
	}
}
