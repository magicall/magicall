/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.msg;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.text.Text;

import java.time.Instant;
import java.util.Objects;

public interface Msg extends Text {

	Thing sender();

	Instant sendTime();

	Thing receiver();

	default Instant receiveTime() {
		return sendTime();
	}

	static String str(final Msg one) {
		return StrKit.format("{0}@{1} -> {2}@{3}", one.sender(), one.sendTime(), one.receiver(), one.receiveTime());
	}

	static int hash(final Msg one) {
		return Objects.hash(one.sender(), one.sendTime(), one.receiver(), one.receiveTime());
	}

	static boolean eq(final Msg one, final Object other) {
		return other instanceof final Msg o//
				&& Objects.equals(one.sender(), o.sender())//
				&& Objects.equals(one.sendTime(), o.sendTime())//
				&& Objects.equals(one.receiver(), o.receiver())//
				&& Objects.equals(one.receiveTime(), o.receiveTime());
	}
}
