/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import me.magicall.math.Fractal;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <pre>
 * 文本部分。
 * 文本部分是更大的文本部分（上下文{@link #context()}）的完全分解的各个部分。
 * 	由于完全分解，所以内容等于所有组成部分之和（组合）。
 * 与“文本片段（{@link TextFragment}）”不同的是，文本部分是文本的结构成分，文本片段则可以是任意的片段，可以只是一两个词，或跨段落、章节等。
 * 没有“离散的/游离的”文本部分——这种游离的文本，本身就是片段，哪怕其“不完整”，但那是针对内容进行了逻辑思考才能得出的结论，“文本”本身不需要对完整性负责。
 * </pre>
 */
public interface TextPart<_Part extends TextPart<_Part>> extends TextFragment, Fractal<_Part> {
	@Override
	default String content() {
		return parts().map(TextPart::content).collect(Collectors.joining());
	}

	//=============================================

	static String str(final TextPart<?> o) {
		final var title = o.title();
		if (Kits.STR.isEmpty(title)) {
			return o.content();
		}
		return StrKit.format("{0} {1}", title, o.content());
	}

	static int hash(final TextPart<?> o) {
		return Objects.hash(o.content());
	}

	static boolean eq(final TextPart<?> o, final Object obj) {
		if (o == obj) {
			return true;
		}
		return obj instanceof final TextPart<?> other && Objects.equals(o.content(), other.content());
	}
}
