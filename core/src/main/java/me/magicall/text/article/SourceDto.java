/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.version.VersionInfo;

public class SourceDto implements Source {
	public String name;
	public VersionInfo versionInfo;
	public String place;

	@Override
	public String name() {
		return name;
	}

	@Override
	public String place() {
		return place;
	}

	@Override
	public VersionInfo versionInfo() {
		return versionInfo;
	}

	@Override
	public String toString() {
		return Source.str(this);
	}

	@Override
	public int hashCode() {
		return Source.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Source.eq(this, o);
	}
}
