/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.version.Versioned;

import java.util.Comparator;
import java.util.Objects;

/**
 * 来源。
 * 来源=出处的名字（{@link #name()}）+出处的版本（{@link #versionInfo()}）+事物在出处里的位置（{@link #place()}）。
 * 典型的用法：用于一个文本引用的引文描述，如：
 * <pre>
 * 除前款规定的情形外，行使共有的专利申请权或者专利权应当取得全体共有人的同意
 * 						——《中华人民共和国专利法（2020）》 第十四条第二款
 * </pre>
 * 其中的“《中华人民共和国专利法（2020）》 第十四条第二款”部分。
 * 暂时未加入作者。
 */
//todo：这里不应该实现Versioned接口，如有需要，应该有一个子接口VersionedSource。
//todo：其实也不应该实现Comparable，应该由使用处自行实现Comparator。
public interface Source extends Named, Comparable<Source>, Versioned {

	Comparator<Source> COMPARATOR = Comparator.comparing(Source::name, Comparator.nullsLast(Comparator.naturalOrder()))//
			.thenComparing(Source::versionInfo)//
			.thenComparing(Source::place);

	/**
	 * 所引用内容在文档中的位置。暂时叫这么挫的名字。
	 *
	 * @return
	 */
	String place();

	@Override
	default int compareTo(final Source o) {
		return COMPARATOR.compare(this, o);
	}

	static String str(final Source source) {
		return StrKit.format("《{0}（{1}）》{2}", source.name(), source.versionInfo(), source.place());
	}

	static int hash(final Source source) {
		return Objects.hash(source.name(), source.versionInfo(), source.place());
	}

	static boolean eq(final Source source, final Object other) {
		if (source == other) {
			return true;
		}
		if (other instanceof final Source o) {
			return Objects.equals(source.name(), o.name())//
					&& Objects.equals(source.versionInfo(), o.versionInfo())//
					&& Objects.equals(source.place(), o.place());
		}
		return false;
	}
}
