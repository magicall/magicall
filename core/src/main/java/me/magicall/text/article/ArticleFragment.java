/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.text.TextFragment;

/**
 * 文章片段：在一篇完整文章里的部分片段。
 * 与“文章部分（{@link ArticlePart}）”不同的是，文章部分是文章的结构成分，文章片段则可以是任意的片段，可以只是一两个词，或跨段落、章节等。
 */
public interface ArticleFragment extends TextFragment {
	Article article();

	//转化成引用的方法： Ref toRef();

	//转化成文章的方法： Article toArticle();
}
