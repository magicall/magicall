/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import java.util.List;
import java.util.stream.Stream;

public class ArticleDto<T extends ArticlePart<T>> implements Article<T> {
	public String title;
	public String subtitle;
	public List<T> parts;

	@Override
	public String title() {
		return title;
	}

	@Override
	public String subtitle() {
		return subtitle;
	}

	@Override
	public Stream<T> parts() {
		return parts.stream();
	}

	@Override
	public String toString() {
		return Article.str(this);
	}

	@Override
	public int hashCode() {
		return Article.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Article.eq(this, o);
	}
}
