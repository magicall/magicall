/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.text.HasSubtitle;
import me.magicall.text.TextPart;

import java.util.Objects;

/**
 * <pre>
 * 文章部分。
 * 文章部分是文章或更大的文章部分（上下文{@link #context()}）的完全分解的各个部分。是章、回、节、段、句……等的最小公倍数/共有抽象/超类。
 * 	不会比“句”小，那已经进入文法分析层面。
 * 	由于完全分解，所以内容等于所有组成部分之和（组合）。
 * 与“文章片段（{@link ArticleFragment}）”不同的是，文章部分是文章的结构成分，文章片段则可以是任意的片段，可以只是一两个词，或跨段落、章节等。
 * 没有“离散的/游离的”文章部分——这种游离的文本，本身就是一篇文章或片段，哪怕其“不完整”，但那是针对内容进行了逻辑思考才能得出的结论，“文章”本身不需要对完整性负责。
 *
 * 关于文章部分的“类型”
 * 	文章被作者分为多个部分，这些部分可能是编/章/回/段……也可能包含前言、后记等。
 * 	基于以下原因：
 * 		1，粒度有大有小，甚至也有相同或看不出粒度的，比如“章”和“回”，完全取决于作者选用哪个。
 * 		2，相同粒度文章部分，其组成部分不一定粒度相同。比如“第一章”下分了节，“第二章”没分，直接是段。
 * 	因此，文章部分不存在统一的类型划分
 *
 * 注意，有些比较明显的“文章片段”不是文章部分。
 * 比如，《水浒传》武将登场的“定场诗”就不是“文章部分”，因为它（以及前文）不是上下文（更大的文章部分）中的一个分割，而是引用了一首诗。排版时可以排版成“诗词引用”的格式，也可以按照普通格式排版。
 * </pre>
 */
public interface ArticlePart<T extends ArticlePart<T>> extends ArticleFragment, HasSubtitle, TextPart<T> {

	//=============================================

	static String str(final ArticlePart<?> o) {
		final var title = o.title();
		if (Kits.STR.isEmpty(title)) {
			return o.content();
		}
		return StrKit.format("{0} {1}", title, o.content());
	}

	static int hash(final ArticlePart<?> o) {
		return Objects.hash(o.title(), o.content());
	}

	static boolean eq(final ArticlePart<?> o, final Object obj) {
		if (o == obj) {
			return true;
		}
		return obj instanceof final ArticlePart<?> other//
				&& Objects.equals(o.title(), other.title()) && Objects.equals(o.content(), other.content());
	}
}
