/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.text.Text;

import java.util.Objects;

/**
 * 引用。（<a href="https://www.cihai.com.cn/baike/detail/72/5617329">辞海链接</a>）
 * 表示对一段文本的引用。该文本出自某种来源，比如：
 * <pre>
 * 例1
 * 滚滚长江东逝水，浪花淘尽英雄。——明·杨慎《临江仙》
 * {@link #source()}='明·杨慎《临江仙》'
 *  {@link Source#name()}=《临江仙》
 *  {@link Source#place()}=上阙
 * {@link #content()}='滚滚长江东逝水，浪花淘尽英雄。'
 * 此例只引用了文档片段中的部分内容（一句）。
 * </pre>
 * <pre>
 * 例2
 * 勿以恶小而为之，勿以善小而不为。——刘备遗言
 * {@link #source()}='刘备遗言'
 *  {@link Source#name()}=刘备遗言
 *  {@link Source#place()}=（全文）
 * {@link #content()}='勿以恶小而为之，勿以善小而不为。'
 * </pre>
 */
public interface Ref extends Text {

	/**
	 * 所引用的内容。是引用对象的核心意义所在。
	 * “小于等于”{@link #fullContent()}。比如当全文较长时，所引用内容可以删减部分词句。如：“张三是一个代称，在罗翔的刑法课中是法外狂徒”，可以构造一个引用“张三……是法外狂徒”。
	 *
	 * @return
	 */
	@Override
	String content();

	/**
	 * 引用来源。
	 * 可以不提供，表示<a href="https://www.cihai.com.cn/baike/detail/72/5617329">“暗用”</a>。此时返回null。
	 *
	 * @return 可以为null。
	 */
	Source source();

	/**
	 * 引用部分的全文。
	 * 即上下文。但context与content太像，所以不用context。
	 * “大于等于”{@link #content()}。
	 * 可以不提供，此时返回null。
	 *
	 * @return
	 */
	String fullContent();

	static String str(final Ref ref) {
		final var content = ref.content();
		final var fullContent = ref.fullContent();
		final var source = ref.source();
		if (Kits.STR.isEmpty(fullContent) || content.equals(fullContent)) {
			if (source == null) {
				return StrKit.format("“{0}”", content);
			} else {
				return StrKit.format("“{0}”——{1}", content, source);
			}
		} else {
			if (source == null) {
				return StrKit.format("“{0}”（{1}）", content, fullContent);
			} else {
				return StrKit.format("“{0}”——{1}（{2}）", content, source, fullContent);
			}
		}
	}

	static int hash(final Ref ref) {
		return Objects.hash(ref.source(), ref.content());
	}

	static boolean eq(final Ref ref, final Object other) {
		if (ref == other) {
			return true;
		}
		if (other instanceof final Ref o) {
			return Objects.equals(ref.content(), o.content()) && Objects.equals(ref.source(), o.source());
		}
		return false;
	}
}
