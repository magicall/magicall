/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import java.util.Objects;

public class RefDto implements Ref {

	public Source source;
	public String content;
	public String fullContent;

	@Override
	public Source source() {
		return source;
	}

	@Override
	public String content() {
		return Objects.requireNonNullElse(content, "");
	}

	@Override
	public String fullContent() {
		return Objects.requireNonNullElse(fullContent, "");
	}

	@Override
	public String toString() {
		return Ref.str(this);
	}

	@Override
	public int hashCode() {
		return Ref.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Ref.eq(this, o);
	}
}
