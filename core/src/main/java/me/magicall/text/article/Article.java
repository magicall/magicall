/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.article;

import me.magicall.HasParts;
import me.magicall.text.HasSubtitle;
import me.magicall.text.HasTitle;
import me.magicall.text.Text;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <pre>
 * “文章”就是公共领域里的“文章”。
 * 它不是文本片段——我们缩窄了“片段”的含义。它是整体而非片段。‘它是片段’是‘用片段实现树形结构’的技术方案的一部分。
 * 它由许多文章部分组合。这种解构发生在公共领域——我们会说“这篇文章共有10段”之类的话。所以这是业务概念。
 * 这种组合是完全分解的，即，文章里不存在“游离的”内容，组成文章的片段也不存在重叠。
 * 文章的部分，是文章诞生时固有的，不是后来的、引入其他领域知识的划分。
 * 前言、后记等也是文章的部分，但它们可能有也可能没有。
 * 如何判定相同？
 * 	暂时只使用标题和内容判定。
 * </pre>
 */
public interface Article<T extends ArticlePart<T>> extends Text, HasTitle, HasSubtitle, HasParts<T> {
	@Override
	default String content() {
		return parts().map(Text::content).collect(Collectors.joining());
	}

	//========================

	static String str(final Article<?> o) {
		return o.content();
	}

	static int hash(final Article<?> o) {
		return Objects.hash(o.title(), o.content());
	}

	/**
	 * 标题、创建信息、正文内容相同即认为相同。
	 *
	 * @return
	 */
	static boolean eq(final Article<?> a, final Object b) {
		if (a == b) {
			return true;
		}
		if (a != null && b instanceof final Article<?> other) {
			return Objects.equals(a.title(), other.title()) && Objects.equals(a.content(), other.content());
		}
		return false;
	}
}
