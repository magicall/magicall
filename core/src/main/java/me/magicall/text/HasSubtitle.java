/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

@FunctionalInterface
public interface HasSubtitle extends HasTitle {
	/**
	 * 副标题，可以无。
	 *
	 * @return
	 */
	default String subtitle() {
		return "";
	}

	@Override
	default String fullName() {
		final var title = title();
		if (Kits.STR.isEmpty(title)) {
			return "";
		}
		final var subtitle = subtitle();
		return Kits.STR.isEmpty(subtitle) ? title : title + ' ' + subtitle;
	}

	@Override
	default String shortName() {
		return title();
	}
}
