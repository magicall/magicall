/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import java.util.Arrays;
import java.util.Objects;

/**
 * 文本类。
 * 文本类是业务的类而非技术的类。与之相对，字符串（String）是技术类。
 * 业务的类可以认为是接口，技术的类可以认为是实现。即，字符串是一种文本，但可以比业务所需要的文本多出一些技术上的方法，比如编码方式之类。
 * 理论上，文本类对象可以像现在字符串一样用（用于各种参数类型和返回值），而且已有toString方法可以用来转化为字符串，所以实际上不需要一个“内容”属性。
 * 但实际上，Object自带了toString方法的实现，因此我们不能强制要求子类实现正确的toString方法，只能改名为{@link #content()}。
 */
@FunctionalInterface
public interface Text extends Comparable<Text> {

	String content();

	default long length() {
		return content().length();
	}

	default boolean isBlank() {
		return content().isBlank();
	}

	default boolean isEmpty() {
		return content().isEmpty();
	}

	default Text trim() {
		return of(content().strip());
	}

	default Text trimLeading() {
		return of(content().stripLeading());
	}

	default Text trimTrailing() {
		return of(content().stripTrailing());
	}

	default boolean equalsIgnoreCase(final Text other) {
		return content().equalsIgnoreCase(other.content());
	}

	default Text concat(final Text one, final Text... others) {
		final var sb = new StringBuilder(one.content());
		Arrays.stream(others).forEach(sb::append);
		return of(sb.toString());
	}

	default Text repeat(final int count) {
		return of(content().repeat(count));
	}

	default boolean contains(final Text other) {
		return content().contains(other.content());
	}

	default boolean startsWith(final Text other) {
		return content().startsWith(other.content());
	}

	default boolean endsWith(final Text other) {
		return content().endsWith(other.content());
	}

	default int placeOf(final Text other) {
		return placeOf(other, TextTechSupport.FIRST_PLACE);
	}

	default int placeOf(final Text other, final int fromPlace) {
		return TextTechSupport.offsetToPlace(content().indexOf(other.content(), TextTechSupport.placeToOffset(fromPlace)));
	}

	@Override
	default int compareTo(final Text other) {
		return content().compareTo(other.content());
	}

	/**
	 * 建议返回{@link #content()}。直接调用{@link #str(Text) Text.toString(this)}亦可。
	 *
	 * @return
	 */
	@Override
	String toString();

	//===========================================================

	Text EMPTY = new FixedText("");

	static String str(final Text o) {
		return o.content();
	}

	static int hash(final Text o) {
		return Objects.hash(o.content());
	}

	static boolean eq(final Text a, final Object b) {
		if (a == b) {
			return true;
		}
		return a != null && b instanceof final Text t && Objects.equals(a.content(), t.content());
	}

	static Text of(final String s) {
		return new FixedText(s);
	}
}
