/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.linguistics;

import java.util.regex.Pattern;

public class RegularDto implements Regular {
	public Pattern pattern;
	public String description;

	@Override
	public String description() {
		return description;
	}

	@Override
	public RegularDto describeBy(final String description) {
		this.description = description;
		return this;
	}

	@Override
	public Pattern pattern() {
		return pattern;
	}

	@Override
	public RegularDto usePattern(final Pattern pattern) {
		this.pattern = pattern;
		return this;
	}

	@Override
	public String toString() {
		return Regular.str(this);
	}

	@Override
	public int hashCode() {
		return Regular.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Regular.eq(this, o);
	}
}
