/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text.linguistics;

import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * 行文规则。
 */
public interface Regular {

	Pattern pattern();

	Regular usePattern(Pattern pattern);

	default Regular usePattern(final String patternStr) {
		return usePattern(Pattern.compile(patternStr));
	}

	String description();

	Regular describeBy(String description);

	static String str(final Regular regular) {
		return StrKit.format("{0}【{1}】", regular.description(), regular.pattern());
	}

	static int hash(final Regular regular) {
		//java的Pattern类挫到连hashCode和equals方法都没有重写，所以只能比较Pattern的pattern（话说Pattern.pattern()这个方法签名也是醉）。
		return Objects.hash(regular.pattern().pattern());
	}

	static boolean eq(final Regular regular, final Object other) {
		//java的Pattern类挫到连hashCode和equals方法都没有重写，所以只能比较Pattern的pattern（话说Pattern.pattern()这个方法签名也是醉）。
		return other instanceof final Regular o && regular.pattern().pattern().equals(o.pattern().pattern());
	}
}
