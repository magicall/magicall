/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * 给 {@link Text} 提供技术支持。
 */
public interface TextTechSupport {
	int OFFSET_TO_PLACE_DELTA = 1;
	int FIRST_PLACE = offsetToPlace(0);
	//长度也相同就对比文本本身
	Comparator<Entry<? extends Text, Integer>> SUB_FRAGMENT_POSITION_COMPARATOR
			= (Comparator) Entry.comparingByValue()//先对比位置
			.thenComparingLong(e -> ((Text) e.getKey()).length())//位置相同再对比文本长度
			.thenComparing(Entry::getKey, Comparator.comparing(Object::toString));

	static Stream<Text> partsMatches(final Text text, final Pattern pattern) {
		final var content = text.content();
		final List<Text> rt = Lists.newLinkedList();
		for (final var matcher = pattern.matcher(content); matcher.find(); ) {
			rt.add(Text.of(content.substring(matcher.start(), matcher.end())));
		}
		return rt.stream();
	}

	static int placeToOffset(final int place) {
		return place - OFFSET_TO_PLACE_DELTA;
	}

	static int offsetToPlace(final int offset) {
		return offset + OFFSET_TO_PLACE_DELTA;
	}
}
