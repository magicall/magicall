/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

public interface TextServices {
	/**
	 * 压缩：把一个不定长的文本对象压缩成一个定长文本，可以通过解压重新获得原来的文本对象。具体算法可以是某种可恢复的散列、映射表。必须幂等。
	 *
	 * @return
	 */
	Text compress(Text raw);

	/**
	 * 解压：压缩的逆函数。
	 *
	 * @return 不能解压则返回null。
	 */
	Text decompress(Text raw);
}
