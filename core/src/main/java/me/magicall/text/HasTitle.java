/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import me.magicall.Named;

@FunctionalInterface
public interface HasTitle extends Named {
	/**
	 * 标题，可以无。
	 *
	 * @return
	 */
	String title();

	/**
	 * 默认情况下等同于标题{@link #title()}。
	 *
	 * @return
	 */
	@Override
	default String name() {
		return title();
	}
}
