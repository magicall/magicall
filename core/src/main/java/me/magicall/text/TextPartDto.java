/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.stream.Stream;

public class TextPartDto<_Part extends TextPart<_Part>> implements TextPart<_Part> {
	public String title;
	public TextFragment context;
	public List<_Part> parts;

	@Override
	public String title() {
		return title;
	}

	@Override
	public TextFragment context() {
		return context;
	}

	@Override
	public Stream<_Part> parts() {
		if (parts == null) {
			parts = Lists.newArrayList();
		}
		return parts.stream();
	}

	@Override
	public String toString() {
		return TextPart.str(this);
	}

	@Override
	public int hashCode() {
		return TextPart.hash(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return TextPart.eq(this, obj);
	}
}
