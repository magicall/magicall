/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.text;

import me.magicall.Named;

/**
 * 文本片段。
 * 文本片段不对其内容负责。即，内容在逻辑上可以是错误的、无效的、无意义的，由使用者自行安排。
 * 是很宽泛的概念，是更大的文本片段（上下文）中随意的一个部分。不要求完全等同于段落、章节等。所以不要求有标题/名字。
 * 不要求分形，即没有结构
 */
public interface TextFragment extends Text, Named, HasTitle {
	/**
	 * 上下文。
	 *
	 * @return 无上下文则为null。
	 */
	TextFragment context();

	//==========================================================

	static String str(final TextFragment o) {
		return Text.str(o);
	}

	static int hash(final TextFragment o) {
		return Text.hash(o);
	}

	/**
	 * 标题、创建信息、正文内容相同即认为相同。
	 *
	 * @return
	 */
	static boolean eq(final TextFragment a, final Object b) {
		return b instanceof TextFragment && Text.eq(a, b);
	}
}
