/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.version;

import me.magicall.Named;

import java.util.Comparator;
import java.util.Objects;

/**
 * 版本。
 * 比较规则：自然顺序是更早的版本在前。
 */
@FunctionalInterface
public interface VersionInfo extends Named, Comparable<VersionInfo> {
	@Override
	default int compareTo(final VersionInfo o) {
		return compareName(this, o);
	}

	static int compareName(final VersionInfo v1, final VersionInfo v2) {
		return Comparator.<String>nullsLast(Comparator.naturalOrder()).compare(v1.name(), v2.name());
	}

	static String str(final VersionInfo versionInfo) {
		return versionInfo.name();
	}

	static int hash(final VersionInfo versionInfo) {
		return Objects.hash(versionInfo.name());
	}

	static boolean eq(final VersionInfo versionInfo, final Object o) {
		return o instanceof VersionInfo && versionInfo.name().equals(((VersionInfo) o).name());
	}
}
