/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.version;

public interface Versioned {
	VersionInfo versionInfo();
//
//	/**
//	 * 版本号。在某一范围内的唯一标识符，用以区别于其他版本同类事物。
//	 * “某一范围”通常指一个父级领域对象，不够全局，所以不算“ID”。
//	 *
//	 * @return
//	 */
//	String versionCode();
//
//	/**
//	 * 版本名字。人类可读。
//	 * 默认使用版本号（{@link #versionCode()}）
//	 *
//	 * @return
//	 */
//	default String versionName() {
//		return versionCode();
//	}
//
//	/**
//	 * 本版本的发布信息。
//	 *
//	 * @return
//	 */
//	Publication publication();
//
//	/**
//	 * 本版本的有效期。
//	 *
//	 * @return
//	 */
//	Range<Instant> period();
}
