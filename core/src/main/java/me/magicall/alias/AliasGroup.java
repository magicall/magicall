/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.alias;

import me.magicall.Thing;

import java.util.stream.Stream;

/**
 * 别名组。
 * 别名不一定是“名字”，也可以是ID。
 * 一个别名组里至少有一个名字。
 * 名字可能在某一范围才有效。不同范围不同名字可能会成为别名，比如甲村称呼老虎为“大虫”，乙村称呼老虎为“山君”。
 */
public interface AliasGroup {

	/**
	 * 本别名组的所有别名。别名之间关系是平等的。
	 *
	 * @return
	 */
	Stream<Thing> aliases();

	/**
	 * 别名数量。
	 *
	 * @return
	 */
	default int size() {
		return (int) aliases().count();
	}

	default boolean contains(final Thing name) {
		return aliases().anyMatch(e -> e.equals(name));
	}

	/**
	 * 在本别名组里添加一些别名。
	 * 若别名已在其他别名组，则合并两组别名。
	 */
	AliasGroup addAliases(Stream<Thing> aliases);

	/**
	 * 在本别名组里添加一些别名。
	 * 若别名已在其他别名组，则合并两组别名。
	 */
	default AliasGroup addAliases(final Thing... aliases) {
		return addAliases(Stream.of(aliases));
	}

	/**
	 * 合并多个别名组。
	 *
	 * @return
	 */
	AliasGroup join(Stream<AliasGroup> otherGroups);

	/**
	 * 合并多个别名组。
	 */
	default AliasGroup join(final AliasGroup... otherGroups) {
		return join(Stream.of(otherGroups));
	}

	/**
	 * 从本别名组里移除一些别名。
	 *
	 * @return 不包含参数指定的别名的别名组，但至少保留一个名字，即使它在需要移除的名单里。
	 */
	AliasGroup removeAliases(Stream<Thing> aliases);

	/**
	 * 从本别名组里移除一些别名。
	 *
	 * @return 不包含参数指定的别名的别名组，但至少保留一个名字，即使它在需要移除的名单里。
	 */
	default AliasGroup removeAliases(final Thing... aliases) {
		return removeAliases(Stream.of(aliases));
	}

	/**
	 * 解散本别名组。解散后，本组中原来的各别名互相之间不再是彼此的别名。
	 *
	 * @return 不包含参数指定的别名的别名组，但至少保留一个名字，即使它在需要移除的名单里。
	 */
	default AliasGroup dismiss() {
		return removeAliases(aliases());
	}
}
