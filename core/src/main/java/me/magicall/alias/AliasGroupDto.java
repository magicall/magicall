/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.alias;

import com.google.common.collect.Sets;
import me.magicall.Thing;

import java.util.Set;
import java.util.stream.Stream;

public class AliasGroupDto implements AliasGroup {
	private final Set<Thing> aliases = Sets.newHashSet();

	@Override
	public Stream<Thing> aliases() {
		return aliases.stream();
	}

	@Override
	public AliasGroup addAliases(final Stream<Thing> aliases) {
		aliases.forEach(this.aliases::add);
		return this;
	}

	@Override
	public AliasGroup join(final Stream<AliasGroup> otherGroups) {
		otherGroups.forEach(otherGroup -> addAliases(otherGroup.aliases()));
		return this;
	}

	@Override
	public AliasGroup removeAliases(final Stream<Thing> aliases) {
		aliases.forEach(this.aliases::remove);
		return this;
	}
}
