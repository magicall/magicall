/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

/**
 * 实现此接口，表示对象有范围。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Scoped {
	/**
	 * 本物所在范围。
	 *
	 * @return
	 */
	Scope scope();

	/**
	 * 把本物放进一个新范围。若不支持更改范围，可直接返回本对象或抛出异常。
	 * 默认实现为抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 */
	default Scoped putIn(final Scope newScope) {
		final var scope = scope();
		throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, "putIn",
				Thing.of(Scoped.class.getSimpleName(), scope == null ? null : scope.name()));
	}
}
