/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope;

import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.util.List;
import java.util.stream.Stream;

public class ScopeDto implements Scope {
	public String name;
	public List<Scope> biggerScopes;
	public List<Scope> smallerScopes;

	public ScopeDto() {
	}

	public ScopeDto(final String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public Stream<Scope> biggerScopes() {
		return biggerScopes.stream();
	}

	@Override
	public Stream<Scope> smallerScopes() {
		return smallerScopes.stream();
	}

	@Override
	public Scope capture(final Stream<Scope> others) {
		others.filter(e -> !isIncluding(e))//
				.forEach(other -> {
					if (other.isIncluding(this)) {
						throw new WrongStatusException("", true, false);
					}
					smallerScopes.add(other);
				});
		return this;
	}

	@Override
	public String toString() {
		return Scope.str(this);
	}

	@Override
	public int hashCode() {
		return Scope.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Scope.eq(this, o);
	}
}
