/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.scope;

import me.magicall.Container;
import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.coll.NodeWalker;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 范围。
 * 注意：
 * 范围可以嵌套。但要注意不要循环。
 * 不同范围内的同名事物视为不同事物。
 * 范围也有名字。
 * 所以不同范围内的同名范围也视为不同范围。
 *
 * @author Liang Wenjian.
 */
public interface Scope extends Named, Container<Scoped> {
	String TYPE = "Scope";
	String EMPTY_SCOPE_NAME = "";
	Scope EMPTY = new Scope() {
		@Override
		public Stream<Scope> biggerScopes() {
			return Stream.empty();
		}

		@Override
		public Stream<Scope> smallerScopes() {
			return Stream.empty();
		}

		@Override
		public String name() {
			return EMPTY_SCOPE_NAME;
		}
	};

	Stream<Scope> biggerScopes();

	default boolean isIn(final Scope maybeBiggerOne) {
		return biggerScopes().anyMatch(e -> e.equals(maybeBiggerOne));
	}

	Stream<Scope> smallerScopes();

	/**
	 * 是否已包含指定范围。
	 *
	 * @return
	 */
	default boolean isIncluding(final Scope otherScope) {
		return NodeWalker.deepFirst().walk(this, Scope::smallerScopes).anyMatch(e -> e.equals(otherScope));
	}

	/**
	 * 捕获指定的其他范围。其他范围不变，依然存在，只是本范围扩大。
	 * 若已包含指定的范围则跳过。
	 * 不用合并（merge），是因为合并隐含“参与合并的范围都消失，诞生一个新的范围”。
	 * 不用吞并（swallow），是因为吞并隐含“被合并者消失”。
	 * 不用包含（contain/include），是因为有歧义：范围是两个维度上的分组，一是业务上包含某类事物，二是范围之间的包含关系。已将“包含”的含义给予“在业务上包含事物”。
	 *
	 * @return
	 * @throws WrongStatusException 当指定范围包含本范围。
	 */
	default Scope capture(final Stream<Scope> others) {
		throw new OpNotAllowedException(Thing.of(TYPE, name()), "capture",
																		Thing.of(TYPE, others.map(Named::name).toList()));
	}

	/**
	 * 捕获指定的其他范围。
	 * 若已包含指定的范围则跳过。
	 * 不用合并（merge），是因为合并隐含“参与合并的范围都消失，诞生一个新的范围”。
	 * 不用吞并（swallow），是因为吞并隐含“被合并者消失”。
	 * 不用包含（contain/include），是因为有歧义：范围是两个维度上的分组，一是业务上包含某类事物，二是范围之间的包含关系。已将“包含”的含义给予“在业务上包含事物”。
	 *
	 * @return
	 * @throws WrongStatusException 当指定范围包含本范围。
	 */
	default Scope capture(final Scope... others) {
		return capture(Stream.of(others));
	}

	/**
	 * 本范围是否包含指定的事物。
	 *
	 * @return
	 */
	@Override
	default boolean isContaining(final Scoped sth) {
		return isIncluding(sth.scope());
	}

	//===========================================================

	static String str(final Scope scope) {
		return "Scope#" + scope.name();
	}

	static int hash(final Scope scope) {
		return Objects.hash(scope.name());
	}

	static boolean eq(final Scope scope, final Object other) {
		return other instanceof final Scope o && scope.name().equals(o.name());
	}
}
