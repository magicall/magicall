/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * 范围。
 * 范围是一种对事物的特殊分组方法。普通的“分组”，事物对组无感知；而划分范围的事物则感知范围，甚至这些事物的有些业务方法与范围强相关，比如通常声明本对象仅在范围内有意义。
 * 范围具有网状结构（而非树状结构），一个范围可包含多个其他范围。范围不能循环包含。
 * 事物通过实现{@link me.magicall.scope.Scoped}接口，表明自己具有范围。
 */
package me.magicall.scope;