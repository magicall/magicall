/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * 全局统一元信息管理
 * 管理是指：
 * 1，记录任一资源的资源类型
 * 2，记录任一资源的创建时间（createTime）
 * 3，记录任一资源的申请人（creator）
 * 不包括
 * 1，使用同一id序列生成id（暂定UUID）
 * 目的：
 * 1，减少各模块记录创建时间、创建人等琐事压力。
 * 记录这些元信息，可以是后有系统。生成id则必然是先有系统，所以把二者拆分，本系统只管元信息。
 * 各模块需要做：
 * 1，自定义一种与众不同的type（暂时没有注册机制，靠人工管理）。
 * 2，创建本类资源实例后，发布created事件。metaServices监听created事件，创建这些元信息。
 */
package me.magicall.meta;