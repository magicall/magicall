/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.meta;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.time.Instant;
import java.util.Objects;

/**
 * 元数据。
 */
public interface Meta {

	Thing owner();

	String creator();

	Instant createTime();

	//============================================

	static String str(final Meta meta) {
		return StrKit.format("Meta(thing={0},creator={1},create-time={2})", meta.owner(), meta.creator(),
												 meta.createTime());
	}

	static boolean eq(final Meta meta, final Object o) {
		return o instanceof final Meta other && meta.owner().equals(other.owner());
	}

	static int hash(final Meta meta) {
		return Objects.hash(meta.owner());
	}
}
