/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import com.google.common.collect.Lists;

import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Stream;

/**
 * 并非真正的类型的类型。
 */
abstract class NotExactType extends Type_<Object> {
	protected final JavaRuntimeElement<?> owner;
	protected final List<Type_<?>> directInterfaces;
	protected final Cls<?> superClass;

	NotExactType(final Type raw, final JavaRuntimeElement<?> owner, final Type... upperBounds) {
		super(raw);
		this.owner = owner;

		//bounds 是<T>的上限，比如<T extends T1 & T2 & T3>的T1、T2、T3。其中最多只有一个可以是具体类，且必须排第一位。
		directInterfaces = Lists.newArrayList();
		boolean first = true;
		Cls<?> superClass = null;
		for (final Type bound : upperBounds) {
			if (first) {
				if (bound instanceof final Class<?> c) {
					superClass = new Cls<>(c);
				} else {
					directInterfaces.add(Type_.of(bound));
				}
				first = false;
			} else {
				directInterfaces.add(Type_.of(bound));
			}
		}
		this.superClass = superClass == null ? Cls.OBJECT : superClass;
	}

	@Override
	public JavaRuntimeElement<?> owner() {
		return owner;
	}

	@Override
	public Type_<?> superClass() {
		return superClass;
	}

	@Override
	public Stream<Type_<?>> interfaces() {
		return Stream.concat(directInterfaces.stream(), superClasses().flatMap(Type_::interfaces))//
				.distinct();
	}

	@Override
	public Stream<Constructor_<Object>> constructors() {
		return Stream.empty();
	}

	@Override
	public Stream<Method_> myOwnMethods() {
		return Stream.empty();
	}

	@Override
	public Stream<Field_> fields() {
		//自己没有字段，简化一下实现
		return superClass == null ? Stream.empty() : superClass.fields();
	}

	@Override
	public Stream<Field_> myOwnFields() {
		return Stream.empty();
	}

	@Override
	public boolean canNewInstance() {
		return false;//<T>肯定不能创建实例
	}
}
