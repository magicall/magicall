/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class ByteKit extends NotFractionKit<Byte, byte[]> {
	@Serial
	private static final long serialVersionUID = -2749786467498972174L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Byte.class, byte.class);

	public static final ByteKit INSTANCE = new ByteKit();

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Byte parse(final String source) {
		try {
			return Byte.parseByte(source);
		} catch (final NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String primitiveArrFlag() {
		return "B";
	}

	@Override
	public Byte fromLong(final long value) {
		return (byte) value;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
