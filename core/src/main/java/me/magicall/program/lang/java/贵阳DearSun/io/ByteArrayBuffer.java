/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.io;

public final class ByteArrayBuffer {
	private byte[] buffer;
	private int len;

	public ByteArrayBuffer(final int capacity) {
		if (capacity < 0) {
			throw new IllegalArgumentException("Buffer capacity may not be negative");
		}
		buffer = new byte[capacity];
	}

	private void expand(final int newLen) {
		final var newBuffer = new byte[Math.max(buffer.length << 1, newLen)];
		System.arraycopy(buffer, 0, newBuffer, 0, len);
		buffer = newBuffer;
	}

	public void append(final byte[] bytes, final int off, final int length) {
		if (bytes == null) {
			return;
		}
		if (off < 0 || off > bytes.length || length < 0 || off + length > bytes.length) {
			throw new IndexOutOfBoundsException();
		}
		if (length == 0) {
			return;
		}
		final var newLen = len + length;
		if (newLen > buffer.length) {
			expand(newLen);
		}
		System.arraycopy(bytes, off, buffer, len, length);
		len = newLen;
	}

	public void append(final int val) {
		final var newLen = len + 1;
		if (newLen > buffer.length) {
			expand(newLen);
		}
		buffer[len] = (byte) val;
		len = newLen;
	}

	public void append(final char[] chars, final int off, final int length) {
		if (chars == null) {
			return;
		}
		if (off < 0 || off > chars.length || length < 0 || off + length > chars.length) {
			throw new IndexOutOfBoundsException();
		}
		if (length == 0) {
			return;
		}
		final var newLen = len + length;
		if (newLen > buffer.length) {
			expand(newLen);
		}
		var i1 = off;
		for (var i2 = len; i2 < newLen; ++i2) {
			buffer[i2] = (byte) chars[i1];
			++i1;
		}

		len = newLen;
	}

	public void clear() {
		len = 0;
	}

	public byte[] toByteArray() {
		final var b = new byte[len];
		if (len > 0) {
			System.arraycopy(buffer, 0, b, 0, len);
		}
		return b;
	}

	public int byteAt(final int index) {
		return buffer[index];
	}

	public int capacity() {
		return buffer.length;
	}

	public int length() {
		return len;
	}

	public void setLength(final int length) {
		if (length < 0 || length > buffer.length) {
			throw new IndexOutOfBoundsException();
		}
		len = length;
	}

	public boolean isEmpty() {
		return len == 0;
	}

	public boolean isFull() {
		return len == buffer.length;
	}
}
