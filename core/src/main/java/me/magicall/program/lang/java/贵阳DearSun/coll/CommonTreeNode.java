/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.coll.Tree.TreeNode;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class CommonTreeNode<E> extends TreeNodeTemplate<E> {

	protected E element;
	protected TreeNode<E> parent;
	protected Collection<TreeNode<E>> children;

	public CommonTreeNode() {
		this(Lists.newArrayList());
	}

	protected CommonTreeNode(final Collection<TreeNode<E>> children) {
		this((E) null, children);
	}

	public CommonTreeNode(final E element) {
		this(element, Lists.newArrayList());
	}

	protected CommonTreeNode(final E element, final Collection<TreeNode<E>> children) {
		this(element, null, children);
	}

	protected CommonTreeNode(final TreeNode<E> parent) {
		this(parent, Lists.newArrayList());
	}

	protected CommonTreeNode(final TreeNode<E> parent, final Collection<TreeNode<E>> children) {
		this(null, parent, children);
	}

	protected CommonTreeNode(final E element, final TreeNode<E> parent) {
		this(element, parent, Lists.newArrayList());
	}

	protected CommonTreeNode(final E element, final TreeNode<E> parent, final Collection<TreeNode<E>> children) {
		this.parent = parent;
		this.element = element;
		this.children = Lists.newArrayList(children);
	}

	@Override
	public TreeNode<E> addSubTree(final Tree<E> tree) {
		final TreeNode<E> n = this;
		children.add(new TreeNodeWrapper<>(tree.getRoot()) {
			@Override
			public TreeNode<E> parent() {
				return n;
			}
		});
		return this;
	}

	@Override
	public Stream<? extends TreeNode<E>> children() {
		return children.stream();
	}

	@Override
	public TreeNode<E> child(final E child) {
		final TreeNode<E> wrap = new CommonTreeNode<>(child, this);
		children.add(wrap);
		return wrap;
	}

	@Override
	public boolean removeChild(final Predicate<? super TreeNode<?>> predicate) {
		return children.removeIf(predicate);
	}

	@Override
	public E getElement() {
		return element;
	}

	@Override
	public E setElement(final E newElement) {
		final var rt = element;
		element = newElement;
		return rt;
	}

	@Override
	public TreeNode<E> parent() {
		return parent;
	}
}//AbsTreeNode
