/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

@FunctionalInterface
public interface StreamWrapper<E> extends Stream<E>, Wrapper<Stream<E>> {

	@Override
	default Stream<E> filter(final Predicate<? super E> predicate) {
		return unwrap().filter(predicate);
	}

	@Override
	default <R> Stream<R> map(final Function<? super E, ? extends R> mapper) {
		return unwrap().map(mapper);
	}

	@Override
	default IntStream mapToInt(final ToIntFunction<? super E> mapper) {
		return unwrap().mapToInt(mapper);
	}

	@Override
	default LongStream mapToLong(final ToLongFunction<? super E> mapper) {
		return unwrap().mapToLong(mapper);
	}

	@Override
	default DoubleStream mapToDouble(final ToDoubleFunction<? super E> mapper) {
		return unwrap().mapToDouble(mapper);
	}

	@Override
	default <R> Stream<R> flatMap(final Function<? super E, ? extends Stream<? extends R>> mapper) {
		return unwrap().flatMap(mapper);
	}

	@Override
	default IntStream flatMapToInt(final Function<? super E, ? extends IntStream> mapper) {
		return unwrap().flatMapToInt(mapper);
	}

	@Override
	default LongStream flatMapToLong(final Function<? super E, ? extends LongStream> mapper) {
		return unwrap().flatMapToLong(mapper);
	}

	@Override
	default DoubleStream flatMapToDouble(final Function<? super E, ? extends DoubleStream> mapper) {
		return unwrap().flatMapToDouble(mapper);
	}

	@Override
	default Stream<E> distinct() {
		return unwrap().distinct();
	}

	@Override
	default Stream<E> sorted() {
		return unwrap().sorted();
	}

	@Override
	default Stream<E> sorted(final Comparator<? super E> comparator) {
		return unwrap().sorted(comparator);
	}

	@Override
	default Stream<E> peek(final Consumer<? super E> action) {
		return unwrap().peek(action);
	}

	@Override
	default Stream<E> limit(final long maxSize) {
		return unwrap().limit(maxSize);
	}

	@Override
	default Stream<E> skip(final long n) {
		return unwrap().skip(n);
	}

	@Override
	default Stream<E> takeWhile(final Predicate<? super E> predicate) {
		return unwrap().takeWhile(predicate);
	}

	@Override
	default Stream<E> dropWhile(final Predicate<? super E> predicate) {
		return unwrap().dropWhile(predicate);
	}

	@Override
	default void forEach(final Consumer<? super E> action) {
		unwrap().forEach(action);
	}

	@Override
	default void forEachOrdered(final Consumer<? super E> action) {
		unwrap().forEachOrdered(action);
	}

	@Override
	default Object[] toArray() {
		return unwrap().toArray();
	}

	@Override
	default <A> A[] toArray(final IntFunction<A[]> generator) {
		return unwrap().toArray(generator);
	}

	@Override
	default E reduce(final E identity, final BinaryOperator<E> accumulator) {
		return unwrap().reduce(identity, accumulator);
	}

	@Override
	default Optional<E> reduce(final BinaryOperator<E> accumulator) {
		return unwrap().reduce(accumulator);
	}

	@Override
	default <U> U reduce(final U identity, final BiFunction<U, ? super E, U> accumulator,
											 final BinaryOperator<U> combiner) {
		return unwrap().reduce(identity, accumulator, combiner);
	}

	@Override
	default <R> R collect(final Supplier<R> supplier, final BiConsumer<R, ? super E> accumulator,
												final BiConsumer<R, R> combiner) {
		return unwrap().collect(supplier, accumulator, combiner);
	}

	@Override
	default <R, A> R collect(final Collector<? super E, A, R> collector) {
		return unwrap().collect(collector);
	}

	@Override
	default Optional<E> min(final Comparator<? super E> comparator) {
		return unwrap().min(comparator);
	}

	@Override
	default Optional<E> max(final Comparator<? super E> comparator) {
		return unwrap().max(comparator);
	}

	@Override
	default long count() {
		return unwrap().count();
	}

	@Override
	default boolean anyMatch(final Predicate<? super E> predicate) {
		return unwrap().anyMatch(predicate);
	}

	@Override
	default boolean allMatch(final Predicate<? super E> predicate) {
		return unwrap().allMatch(predicate);
	}

	@Override
	default boolean noneMatch(final Predicate<? super E> predicate) {
		return unwrap().noneMatch(predicate);
	}

	@Override
	default Optional<E> findFirst() {
		return unwrap().findFirst();
	}

	@Override
	default Optional<E> findAny() {
		return unwrap().findAny();
	}

	@Override
	default Iterator<E> iterator() {
		return unwrap().iterator();
	}

	@Override
	default Spliterator<E> spliterator() {
		return unwrap().spliterator();
	}

	@Override
	default boolean isParallel() {
		return unwrap().isParallel();
	}

	@Override
	default Stream<E> sequential() {
		return unwrap().sequential();
	}

	@Override
	default Stream<E> parallel() {
		return unwrap().parallel();
	}

	@Override
	default Stream<E> unordered() {
		return unwrap().unordered();
	}

	@Override
	default Stream<E> onClose(final Runnable closeHandler) {
		return unwrap().onClose(closeHandler);
	}

	@Override
	default void close() {
		unwrap().close();
	}
}
