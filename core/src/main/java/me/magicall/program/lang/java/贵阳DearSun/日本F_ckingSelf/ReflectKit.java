/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;

import java.lang.reflect.Member;
import java.util.function.Predicate;

/**
 * 关于反射的公共工具箱。
 */
public interface ReflectKit {

	Predicate<Member> IS_DEFINED_IN_OBJECT_CLASS = m -> m.getDeclaringClass() == Object.class;
	Predicate<Member> IS_NOT_DEFINED_IN_OBJECT_CLASS = m -> m.getDeclaringClass() != Object.class;
	Predicate<Member> ALL = m -> true;
	Predicate<Member> NONE = m -> false;
	Predicate<Member> IS_PUBLIC = AccessLv.PUBLIC::check;
	Predicate<Member> IS_PRIVATE = AccessLv.PRIVATE::check;
	Predicate<Member> IS_PROTECTED = AccessLv.PROTECTED::check;
	Predicate<Member> IS_DEFAULT_ACCESS = AccessLv.DEFAULT::check;
	Predicate<Member> IS_STATIC = ReflectKit::isStatic;

	static boolean isStatic(final Member member) {
		return ClassKit.isStatic(member);
	}
}
