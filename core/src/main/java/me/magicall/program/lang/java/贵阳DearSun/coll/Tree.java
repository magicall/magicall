/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import com.google.common.collect.Lists;
import me.magicall.Parent;
import me.magicall.Child;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 树
 *
 * @param <E> 元素的类型。
 */
@FunctionalInterface
public interface Tree<E> extends Collection<E> {

	/**
	 * 根节点
	 *
	 * @return 根节点
	 */
	TreeNode<E> getRoot();

	/**
	 * 节点流。默认按广度优先组织。
	 *
	 * @return 节点流
	 */
	default Stream<TreeNode<E>> nodeStream() {
		return TreeWalker.wideFirst().walk(this);
	}

	default Stream<E> elementStream() {
		return nodeStream().map(TreeNode::getElement);
	}

	@Override
	default Stream<E> stream() {
		return elementStream();
	}

	/**
	 * 树的深度
	 *
	 * @return 树的深度
	 */
	default int countLayers() {
		return isEmpty() ? 0 : getRoot().countTreeLayers();
	}

	/**
	 * 所有叶子节点
	 *
	 * @return 所有叶子节点
	 */
	default Collection<TreeNode<E>> getLeafNodes() {
		return nodeStream().filter(TreeNode::isLeaf).collect(Collectors.toList());
	}

	/**
	 * 叶子数
	 *
	 * @return 叶子数
	 */
	default int countLeaves() {
		return getLeafNodes().size();
	}

	@Override
	default int size() {
		return (int) Math.min(nodeStream().count(), Integer.MAX_VALUE);
	}

	@Override
	default boolean isEmpty() {
		return getRoot() == null;
	}

	@Override
	default Iterator<E> iterator() {
		final Iterator<TreeNode<E>> raw = nodeStream().iterator();
		return new Iterator<>() {
			TreeNode<E> curNode;

			@Override
			public boolean hasNext() {
				return raw.hasNext();
			}

			@Override
			public E next() {
				curNode = raw.next();
				return curNode.getElement();
			}

			@Override
			public void remove() {
				raw.remove();
				Tree.this.remove(curNode.getElement());
			}
		};
	}

	@Override
	default boolean contains(final Object o) {
		return nodeStream().anyMatch(n -> Objects.equals(n.getElement(), o));
	}

	@Override
	default boolean containsAll(final Collection<?> c) {
		return c.stream().allMatch(this::contains);
	}

	@Override
	default Object[] toArray() {
		return elementStream().toArray();
	}

	@Override
	default <T> T[] toArray(final T[] a) {
		return elementStream().collect(Collectors.toList()).toArray(a);
	}

	/**
	 * todo：Tree没有一个定义清晰的“添加元素”行为。所以暂时定义：所有Tree都不可直接添加元素。
	 * 受到影响的包括add、addAll
	 *
	 * @param e 元素
	 * @return 是否添加了元素
	 */
	@Override
	default boolean add(final E e) {
		throw new UnsupportedOperationException();
	}

	@Override
	default boolean addAll(final Collection<? extends E> c) {
		return Objects.requireNonNull(c).stream().map(this::add)
				.reduce((preResult, addedResult) -> preResult && addedResult).orElse(false);
	}

	/**
	 * todo：Tree没有一个定义清晰的“移除元素”行为。所以暂时定义：所有Tree都不可直接移除元素。
	 * 是只移除某节点的元素，还是连同节点一并移除？如果也移除节点，则还要处理该节点的子节点。
	 * 是按广度优先遍历还是深度优先遍历来寻找第一个包含此元素的节点？
	 * 受到影响的包括remove、removeAll、retainAll、clear
	 *
	 * @param o 元素
	 * @return 是否移除了元素
	 */
	@Override
	default boolean remove(final Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	default boolean removeAll(final Collection<?> c) {
		Objects.requireNonNull(c);
		boolean modified = false;
		for (final Iterator<?> it = iterator(); it.hasNext(); ) {
			if (c.contains(it.next())) {
				it.remove();
				modified = true;
			}
		}
		return modified;
	}

	@Override
	default boolean retainAll(final Collection<?> c) {
		Objects.requireNonNull(c);
		boolean modified = false;
		for (final Iterator<E> it = iterator(); it.hasNext(); ) {
			if (!c.contains(it.next())) {
				it.remove();
				modified = true;
			}
		}
		return modified;
	}

	@Override
	default void clear() {
		for (final Iterator<E> it = iterator(); it.hasNext(); ) {
			it.next();
			it.remove();
		}
	}

	//==========================================================================

	/**
	 * 树的节点。
	 *
	 * @param <E> 值的类型。
	 */
	interface TreeNode<E> extends Child<TreeNode<E>>, Parent<TreeNode<E>> {

		/**
		 * 获取节点上附带的数据。
		 *
		 * @return 元素
		 */
		E getElement();

		default E setElement(final E newElement) {
			throw new UnsupportedOperationException();
		}

		default E clearElement() {
			return setElement(null);
		}

		default TreeNode<E> adopt(final TreeNode<E> node) {
			return addSubTree(node.treeFromMe());
		}

		/**
		 * 本节点到达根的路径
		 * 不包含本节点
		 * 与pathFromRoot反序
		 *
		 * @return 本节点到根节点的路径
		 */
		default List<? extends TreeNode<E>> pathToRoot() {
			return Stream.iterate(parent(), Objects::nonNull, Child::parent).collect(Collectors.toList());
		}

		/**
		 * 从根到本节点的路径
		 * 不包含本节点
		 * 与pathToRoot反序
		 *
		 * @return 根节点到本节点的路径
		 */
		default List<? extends TreeNode<E>> pathFromRoot() {
			final List<TreeNode<E>> rt = Lists.newLinkedList();
			Stream.iterate(parent(), Objects::nonNull, TreeNode::parent).forEach(n -> rt.add(0, n));
			return rt;
		}

		/**
		 * 判断本节点是否参数节点的子孙
		 * 若参数节点即为本节点,也返回false(自己不是自己的子)
		 * 若参数为null,也返回false
		 *
		 * @param maybeParent 目标节点
		 * @return 若本节点是目标节点的后代则返回true。
		 */
		@Override
		default boolean isChildOf(final TreeNode<E> maybeParent) {
			if (isRoot()) {
				return false;
			}
			return maybeParent.children().anyMatch(this::equals) || parent().isChildOf(maybeParent);
		}

		/**
		 * 本节点在树的第几层.根节点为第0层
		 *
		 * @return 本节点所在的层数。
		 */
		default int getLayer() {
			return pathToRoot().size();
		}

		/**
		 * 直接子节点
		 *
		 * @return 直接子节点
		 */
		@Override
		Stream<? extends TreeNode<E>> children();

		/**
		 * 是否叶子节点
		 *
		 * @return 若是叶子节点则返回true。
		 */
		default boolean isLeaf() {
			return children().count() == 0;
		}

		/**
		 * 喜得贵子
		 *
		 * @param child 新的子元素
		 * @return 新的子节点
		 */
		default TreeNode<E> child(final E child) {
			throw new UnsupportedOperationException();
		}

		default Map<E, TreeNode<E>> children(final Collection<E> children) {
			return children.stream().collect(Collectors.toMap(Function.identity(), this::child));
		}

		default boolean removeChild(final Predicate<? super TreeNode<?>> predicate) {
			throw new UnsupportedOperationException();
		}

		default boolean removeChildren() {
			final var count = children().count();
			boolean rt = true;
			for (int i = 0; i < count; i++) {
				rt = rt && removeChild(e -> true);
			}
			return rt;
		}

		/**
		 * 所有后代节点
		 *
		 * @return 所有后代节点
		 */
		default Collection<? extends TreeNode<E>> getDescendants() {
			return treeFromMe().nodeStream().skip(1).collect(Collectors.toList());
		}

		/**
		 * 所有后代节点的数量
		 *
		 * @return 后代节点的数量
		 */
		default int countDescendants() {
			return getDescendants().size();
		}

		/**
		 * 此节点的后代节点中的所有叶子节点
		 *
		 * @return 本节点下的所有叶子节点
		 */
		default Collection<? extends TreeNode<E>> getLeafNodes() {
			return treeFromMe().getLeafNodes();
		}

		/**
		 * 以本节点为根的子树。（&lt;=原树）
		 *
		 * @return 以本节点为根的子树。
		 */
		default Tree<E> treeFromMe() {
			return new SubTree<>(this);
		}

		/**
		 * 计算最深的子树的深度
		 *
		 * @return 深度
		 */
		private int countTreeLayers() {
			return children().map(TreeNode::countTreeLayers).reduce(Math::max).orElse(0) + 1;
		}

		/**
		 * 子树：以各直接子节点为根的树。
		 *
		 * @return 以直接子节点为根的森林。
		 */
		default Collection<? extends Tree<E>> getSubTrees() {
			return children().map(TreeNode::treeFromMe).collect(Collectors.toList());
		}

		default TreeNode<E> addSubTree(final Tree<E> tree) {
			throw new UnsupportedOperationException();
		}

		/**
		 * 同级其他节点
		 *
		 * @return 兄弟节点。
		 */
		default Collection<? extends TreeNode<E>> getSiblings() {
			if (isRoot()) {
				return Kits.COLL.emptyVal();
			}
			return parent().children().filter(n -> !equals(n)).collect(Collectors.toList());
		}
	}
}
