/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Constructor_<_Type> extends Executable_<Constructor<_Type>> {
	public Constructor_(final Constructor<_Type> raw) {
		super(raw);
	}

	@Override
	public Type_<_Type> elementType() {
		return new Cls<>(raw.getDeclaringClass());
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V> V tryToFetchVal(final Object... obj) {
		ensureAccessible();
		try {
			return (V) raw.newInstance(obj);
		} catch (final InstantiationException | IllegalAccessException | InvocationTargetException e) {
			return null;
		}
	}
}
