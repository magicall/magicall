/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import me.magicall.program.lang.java.proxy.dynamic.DynamicProxyKit;
import me.magicall.program.lang.java.proxy.dynamic.MapBeanInvocationHandler;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.MethodKit;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class BeanKit {

	public static final String SET = "set";
	public static final int SET_LEN = SET.length();
	public static final Predicate<Method> IS_GETTER = BeanKit::isGetter;
	public static final Predicate<Method> IS_EXACT_GETTER = BeanKit::isExactGetter;
	public static final Predicate<Method> IS_BOOL_GETTER_START_WITH_IS = BeanKit::isBoolGetterStartWithIs;
	public static final Predicate<Method> IS_STRICT_SETTER = BeanKit::isStrictSetter;
	private static final int FIELD_NAME_START_INDEX_OF_SETTER = SET_LEN + 1;
	public static final String GET = "get";
	public static final int GET_LEN = GET.length();
	private static final int FIELD_NAME_START_INDEX_OF_GETTER = GET_LEN + 1;
	public static final String IS = "is";
	public static final int IS_LEN = IS.length();
	private static final int FIELD_NAME_START_INDEX_OF_GETTER_USING_IS = IS_LEN + 1;
	private static final Map<Class<?>, Collection<Method>> ALL_GETTERS_WITHOUT_GET_CLASS_CACHE
			= new ConcurrentHashMap<>();
	private static final Map<Class<?>, Collection<Method>> ALL_SETTERS_CACHE = new ConcurrentHashMap<>();

	public static String toGetterName(final String fieldName) {
		return GET + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	public static String toSetterName(final String fieldName) {
		return SET + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	public static String toGetterNameStartWithIs(final String fieldName) {
		return IS + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	public static String getterToFieldName(final Method method) {
		final var methodName = method.getName();
		if (isExactGetter(method)) {
			return Character.toLowerCase(methodName.charAt(GET_LEN)) + methodName.substring(FIELD_NAME_START_INDEX_OF_GETTER);
		}
		if (isBoolGetterStartWithIs(method)) {
			return Character.toLowerCase(methodName.charAt(IS_LEN)) + methodName.substring(
					FIELD_NAME_START_INDEX_OF_GETTER_USING_IS);
		}
		return null;
	}

	public static String setterNameToFieldName(final String methodName) {
		return Character.toLowerCase(methodName.charAt(SET_LEN)) + methodName.substring(FIELD_NAME_START_INDEX_OF_SETTER);
	}

	/**
	 * 创建参数Class的一个实例对象. 如果参数Class代表一个类(class),则使用其无参构造函数创建一个实例,如果该类没有无参构造函数,则返回null;
	 * 如果参数Class代表一个接口(interface),则使用动态代理生成一个代理实例.由于是bean,因此该代理实例仅代理getter和setter方法.
	 *
	 * @param clazz 类型
	 * @return 新实例
	 */
	public static <T> T newPojo(final Class<? extends T> clazz) {
		if (clazz.isInterface()) {
			return DynamicProxyKit.proxy(new MapBeanInvocationHandler(), clazz);
		} else {
			return ClassKit.newInstance(clazz);
		}
	}

	/**
	 * 将一个bean“拍平”——把所有字段放到一个map里，key为字段名；如果字段是复杂对象，则也针对该字段展开，key为xx.yy。
	 *
	 * @param bean 一个bean
	 * @return 键值对
	 * @throws IllegalAccessException 读取字段失败
	 */
	public static Map<String, Object> flat(final Object bean) throws IllegalAccessException {
		final Map<String, Object> map = new LinkedHashMap<>();

		putValues(bean, map, null);

		return map;
	}

	private static void putValues(final Object bean, final Map<String, Object> map, final String prefix)
			throws IllegalAccessException {
		final var cls = bean.getClass();

		for (final var field : cls.getDeclaredFields()) {
			field.setAccessible(true);

			final var value = field.get(bean);
			final String key;
			if (prefix == null) {
				key = field.getName();
			} else {
				key = prefix + '.' + field.getName();
			}

			if (isValue(value)) {
				map.put(key, value);
			} else {
				putValues(value, map, key);
			}
		}
	}

	private static boolean isValue(final Object value) {
		if (value == null) {
			return true;
		}
		if (value.getClass() == Object.class) {
			return true;
		}
		return Kits.getAll().stream().filter(kit -> !(kit instanceof ObjKit)).anyMatch(kit -> kit.isSuitForInstance(value));
	}

	/**
	 * 判断一个方法是否getter：以get开头+其后至少有一个大写字母+无参+有返回值，或以is开头++其后至少有一个大写字母+无参+返回布尔型（包括Boolean）
	 *
	 * @param method 方法
	 * @return 是否getter
	 */
	public static boolean isGetter(final Method method) {
		return isExactGetter(method) || isBoolGetterStartWithIs(method);
	}

	public static boolean isExactGetter(final Method method) {
		return method.getDeclaringClass() != Object.class && firstWordMatch(method, GET) && MethodKit.hasNoArg(method)
					 && MethodKit.hasReturn(method);
	}

	public static boolean isBoolGetterStartWithIs(final Method method) {
		return firstWordMatch(method, IS) && isReturningBool(method) && MethodKit.hasNoArg(method);
	}

	public static boolean isStrictSetter(final Method method) {
		return isSetter(method, true);
	}

	public static boolean isSetter(final Method method, final boolean checkReturnType) {
		return firstWordMatch(method, SET) && method.getParameterTypes().length == 1//
					 && (!checkReturnType || method.getReturnType() == void.class);
	}

	private static boolean firstWordMatch(final Method method, final String prefix) {
		final var methodName = method.getName();
		final var prefixLen = prefix.length();
		if (methodName.length() <= prefixLen) {
			return false;
		}
		if (methodName.startsWith(prefix)) {
			//get/is后的首字母不能是小写.
			//注:在无大小写区别的语言中(比如汉语),isLowerCase和isUpperCase都返回false,
			//因此只能检查该字符是否小写字符,若是,则认为是一个以"get/is"开头的单词的一部分,而非一个"get/is xxx"
			return !Character.isLowerCase(methodName.charAt(prefixLen));
		}
		return false;
	}

	private static boolean isReturningBool(final Method method) {
		final var returnType = method.getReturnType();
		return returnType == boolean.class || returnType == Boolean.class;
	}

	/**
	 * 获取某字段的setter，忽略名字大小写，并且是参数包容的。该字段的名字为Class的简单名，首字母小写。
	 * （如：某类A有setString(Object o)方法，用A.class和String.class调用本方法，可以获得此Method对象)。
	 *
	 * @param clazz 类型
	 * @param fieldClass 要获取的字段的类型
	 * @return 该字段的setter
	 */
	public static Method getSetterIgnoreNameCaseAndTypeAssigned(final Class<?> clazz,
																															final Class<?> fieldClass) {//ignore case and type
		return MethodKit.findMethodIgnoreCaseAndArgsTypesAssigned(clazz.getMethods(),
				toSetterName(fieldClass.getSimpleName()), fieldClass);
	}

	/**
	 * 获取一个类的所有getter,不包括getClass
	 *
	 * @param clazz 类型
	 * @return 所有getter集合
	 */
	public static Collection<Method> getAllGettersWithoutGetClass(final Class<?> clazz) {
		var collection = ALL_GETTERS_WITHOUT_GET_CLASS_CACHE.get(clazz);
		if (collection == null) {
			collection = new ArrayList<>();
			final var methods = clazz.getMethods();
			for (final var m : methods) {
				final var name = m.getName();
				if (name.startsWith(GET)//以get开头
						&& !"getClass".equals(name)//不是getClass
						&& name.length() > GET_LEN//get后面还有字
						&& Character.isUpperCase(name.charAt(GET_LEN))//get后面的字符是大写字符
						&& m.getParameterTypes().length == 0//没有参数
						&& m.getReturnType() != Void.class//有返回值
				) {
					collection.add(m);
				}
			}
			ALL_GETTERS_WITHOUT_GET_CLASS_CACHE.put(clazz, collection);
		}
		return collection;
	}

	/**
	 * 获取一个类的所有setter.
	 * 对返回结果进行修改是安全的.
	 *
	 * @param clazz 类型
	 * @return 所有setter的集合
	 */
	public static Collection<Method> getAllSetters(final Class<?> clazz) {
		var collection = ALL_SETTERS_CACHE.get(clazz);
		if (collection == null) {
			final var methods = clazz.getMethods();
			collection = new ArrayList<>(methods.length);
			for (final var m : methods) {
				final var name = m.getName();
				if (name.startsWith(SET)//以set开头
						&& name.length() > SET_LEN//set后面还有字
						&& Character.isUpperCase(name.charAt(SET_LEN))//set后面的字符是大写字符
						&& m.getParameterTypes().length == 1//有一个参数
				) {
					collection.add(m);
				}
			}
			ALL_SETTERS_CACHE.put(clazz, collection);
		}
		return new ArrayList<>(collection);
	}

	/**
	 * 获取一个类的所有setter.如果存在两个名字相同(且都是1个参数)的setter,保留参数的类型(Class)较"小"者.
	 * 对返回结果进行修改是安全的.
	 *
	 * @param clazz 类型
	 * @return 所有setter的集合
	 * @throws RuntimeException 如果存在两个名字相同(且都是1个参数)的setter,但它们的参数不相容.
	 */
	public static Collection<Method> getAllSettersForField(final Class<?> clazz) throws RuntimeException {
		final var setters0 = getAllSetters(clazz);
		if (Kits.COLL.isEmpty(setters0)) {
			return setters0;
		}
		final Map<String, Method> setterNameMap = new LinkedHashMap<>(setters0.size());
		setters0.stream().filter(setter -> isSetter(setter, false)).forEach(setter -> {
			final var fieldClass = setter.getParameterTypes()[0];
			final var fieldName = setterNameToFieldName(setter.getName());
			final var setterFromMap = setterNameMap.get(fieldName);
			if (setterFromMap == null) {
				setterNameMap.put(fieldName, setter);
			} else {
				final var setterArgFromMap = setterFromMap.getParameterTypes()[0];
				if (fieldClass.isAssignableFrom(setterArgFromMap)) {
					return;
				} else if (setterArgFromMap.isAssignableFrom(fieldClass)) {
					setterNameMap.put(fieldName, setter);
				} else {
					throw new RuntimeException(
							'类' + clazz.getSimpleName() + '的' + fieldName + "字段有两个setter方法,但是参数却又不相容");
				}
			}
		});
		return new ArrayList<>(setterNameMap.values());
	}

	public static Method getGetter(final String fieldName, final Class<?> c) {
		final var getterName = toGetterName(fieldName);
		try {
			return c.getMethod(getterName);
		} catch (final SecurityException e) {
			e.printStackTrace();
		} catch (final NoSuchMethodException e) {
			throw new NoSuchThingException("Method", getterName, e);
		}
		return null;
	}
}
