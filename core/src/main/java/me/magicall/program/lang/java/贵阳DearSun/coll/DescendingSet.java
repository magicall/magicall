/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.NoSuchElementException;

/**
 * 翻转的有序散列表。
 *
 * @author Liang Wenjian.
 */
public class DescendingSet<E> extends AbstractSet<E> implements NavigableSet<E> {

	protected final NavigableSet<E> raw;

	public DescendingSet(final NavigableSet<E> raw) {
		this.raw = raw;
	}

	@Override
	public NavigableSet<E> descendingSet() {
		return raw;
	}

	@Override
	public Iterator<E> iterator() {
		return new DescendingIterator<>(raw);
	}

	@Override
	public int size() {
		return descendingSet().size();
	}

	@Override
	public boolean add(final E e) {
		return descendingSet().add(e);
	}

	@Override
	public boolean remove(final Object o) {
		return descendingSet().remove(o);
	}

	@Override
	public E lower(final E e) {
		return descendingSet().higher(e);
	}

	@Override
	public E floor(final E e) {
		return descendingSet().ceiling(e);
	}

	@Override
	public E ceiling(final E e) {
		return descendingSet().floor(e);
	}

	@Override
	public E higher(final E e) {
		return descendingSet().lower(e);
	}

	@Override
	public NavigableSet<E> subSet(final E fromElement, final boolean fromInclusive, final E toElement,
																final boolean toInclusive) {
		return descendingSet().subSet(toElement, toInclusive, fromElement, fromInclusive);
	}

	@Override
	public NavigableSet<E> subSet(final E fromElement, final E toElement) {
		return descendingSet().subSet(toElement, false, fromElement, true);
	}

	@Override
	public NavigableSet<E> headSet(final E toElement) {
		return descendingSet().tailSet(toElement, false);
	}

	@Override
	public NavigableSet<E> tailSet(final E fromElement) {
		return descendingSet().headSet(fromElement, false);
	}

	@Override
	public NavigableSet<E> headSet(final E toElement, final boolean inclusive) {
		return descendingSet().tailSet(toElement, inclusive);
	}

	@Override
	public NavigableSet<E> tailSet(final E fromElement, final boolean inclusive) {
		return descendingSet().headSet(fromElement, inclusive);
	}

	@Override
	public Comparator<? super E> comparator() {
		final var comparator = descendingSet().comparator();
		return comparator == null ? null : comparator.reversed();
	}

	@Override
	public boolean contains(final Object o) {
		return descendingSet().contains(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return descendingSet().containsAll(c);
	}

	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return descendingSet().addAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return descendingSet().retainAll(c);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return descendingSet().removeAll(c);
	}

	@Override
	public void clear() {
		descendingSet().clear();
	}

	@Override
	public boolean isEmpty() {
		return descendingSet().isEmpty();
	}

	@Override
	public Iterator<E> descendingIterator() {
		return descendingSet().iterator();
	}

	@Override
	public E first() {
		return descendingSet().last();
	}

	@Override
	public E last() {
		return descendingSet().first();
	}

	@Override
	public E pollFirst() {
		return descendingSet().pollLast();
	}

	@Override
	public E pollLast() {
		return descendingSet().pollFirst();
	}

	@Override
	public boolean equals(final Object o) {
		return descendingSet().equals(o);
	}

	@Override
	public int hashCode() {
		return descendingSet().hashCode();
	}

	static class DescendingIterator<E> implements Iterator<E> {

		protected final NavigableSet<E> raw;
		protected E cur;

		DescendingIterator(final NavigableSet<E> raw) {
			this.raw = raw;
		}

		@Override
		public boolean hasNext() {
			if (cur == null) {
				return !raw.isEmpty();
			} else {
				return raw.lower(cur) != null;
			}
		}

		@Override
		public E next() {
			if (cur == null) {
				cur = raw.last();
			} else {
				cur = raw.lower(cur);
				if (cur == null) {
					throw new NoSuchElementException();
				}
			}
			return cur;
		}

		@Override
		public void remove() {
			raw.remove(cur);
		}
	}
}
