/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class NegativeByteException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = 8742753474125593983L;
	private static final Range<Byte> AVAILABLE_RANGE = Range.closed((byte) 0, Byte.MAX_VALUE);

	public NegativeByteException(final String argName, final byte actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public NegativeByteException(final String argName, final byte actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public NegativeByteException(final String argName, final byte actualVal, final byte maxAvailable) {
		super(argName, actualVal, Range.closed((byte) 0, maxAvailable));
	}

	public NegativeByteException(final String argName, final byte actualVal, final byte maxAvailable,
															 final Throwable cause) {
		this(argName, actualVal, maxAvailable);
		initCause(cause);
	}
}
