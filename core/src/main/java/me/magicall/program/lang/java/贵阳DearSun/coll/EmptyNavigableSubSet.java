/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;

/**
 * @author Liang Wenjian.
 */
public class EmptyNavigableSubSet<E> extends AbstractSet<E> implements NavigableSetSupport<E> {

	private final Comparator<? super E> comparator;

	public EmptyNavigableSubSet(final Comparator<? super E> comparator) {
		this.comparator = comparator;
	}

	@Override
	public E firstOrNull() {
		return null;
	}

	@Override
	public E lastOrNull() {
		return null;
	}

	@Override
	public NavigableSet<E> descendingSet() {
		return new DescendingEmptyNavigableSubSet<>(this);
	}

	@Override
	public NavigableSet<E> subSet(final E fromElement, final boolean fromInclusive, final E toElement,
																final boolean toInclusive) {
		throw new IllegalArgumentException("fromKey out of range");
	}

	@Override
	public NavigableSetSupport<E> headSet(final E toElement, final boolean inclusive) {
		throw new IllegalArgumentException("fromKey out of range");
	}

	@Override
	public NavigableSetSupport<E> tailSet(final E fromElement, final boolean inclusive) {
		throw new IllegalArgumentException("fromKey out of range");
	}

	@Override
	public E lower(final E e) {
		return null;
	}

	@Override
	public E floor(final E e) {
		return null;
	}

	@Override
	public E ceiling(final E e) {
		return null;
	}

	@Override
	public E higher(final E e) {
		return null;
	}

	@Override
	public Iterator<E> iterator() {
		return EmptyIterator.instance();
	}

	@Override
	public Comparator<? super E> comparator() {
		return comparator;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public boolean contains(final Object o) {
		return false;
	}

	@Override
	public Object[] toArray() {
		return new Object[0];
	}

	@Override
	public boolean add(final E e) {
		return false;
	}

	@Override
	public boolean remove(final Object o) {
		return false;
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return false;
	}

	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return false;
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return false;
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return false;
	}

	@Override
	public void clear() {
	}

	private static final EmptyNavigableSubSet<Comparable<?>> IN_NATURE_ORDER = new EmptyNavigableSubSet<>(null);

	public static <E extends Comparable<E>> EmptyNavigableSubSet<E> inNatureOrder() {
		return (EmptyNavigableSubSet) IN_NATURE_ORDER;
	}

	protected static class DescendingEmptyNavigableSubSet<E> extends EmptyNavigableSubSet<E> {
		private final EmptyNavigableSubSet<E> me;

		public DescendingEmptyNavigableSubSet(final EmptyNavigableSubSet<E> me) {
			super(me.comparator() == null ? null : me.comparator().reversed());
			this.me = me;
		}

		@Override
		public NavigableSetSupport<E> descendingSet() {
			return me;
		}
	}
}
