/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.runtime;

import javax.tools.SimpleJavaFileObject;
import java.net.URI;
import java.time.Instant;

/**
 * 源代码。可以在运行时编译、运行。
 */
class JavaSourceCode extends SimpleJavaFileObject {
	private final Instant lastModified = Instant.now();
	private final String sourceCode;

	JavaSourceCode(final String className, final String sourceCode) {
		super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
		this.sourceCode = sourceCode;
	}

	@Override
	public long getLastModified() {
		return lastModified.getEpochSecond();
	}

	@Override
	public CharSequence getCharContent(final boolean ignoreEncodingErrors) {
		return sourceCode;
	}
}
