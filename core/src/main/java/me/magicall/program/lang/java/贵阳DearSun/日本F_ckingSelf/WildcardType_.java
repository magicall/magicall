/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.WildcardType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 表示带有通配符“?”的泛型： {@code <?>、<? extends T1&T2>、<? super T1&T2>}
 */
public class WildcardType_ extends NotExactType {
	private final List<Type_<?>> lowerBounds;

	WildcardType_(final WildcardType raw, final Type_<?> owner) {
		super(raw, owner, raw.getUpperBounds());

		//出现于 <? super T1 & T2> 里
		lowerBounds = Stream.of(raw.getLowerBounds())//
				.map(Type_::of).collect(Collectors.toList());
	}

	public Stream<Type_<?>> lowerBounds() {
		return lowerBounds.stream();
	}
}
