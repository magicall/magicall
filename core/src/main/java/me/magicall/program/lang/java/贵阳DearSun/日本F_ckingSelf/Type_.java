/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;

import java.lang.reflect.Type;
import java.util.stream.Stream;

/**
 * 类型。是java.lang.reflect.Type的增强类。
 *
 * @param <ObjType> 注意：泛型变量不再是包装的对象（{@link #unwrap()}，一个Type对象）的类型了，而是本类型代表的所有实例的类型。比如增强{@code Class<Integer>}的Type_，ObjType将是Integer
 */
public abstract class Type_<ObjType> implements JavaRuntimeElement<Type> {
	protected final Type raw;
	protected final String fullName;
	protected final String shortName;
//	private final boolean isGeneric;

//	Type_(final String fullName, final boolean isFromGeneric) {
//		final String s = fullName.strip();
//		raw = ClassKit.classCalled(s);
//		this.fullName = s;
//		shortName = StrKit.subStringAfterLastSeq(s, ".");
//	}

	Type_(final Type raw) {
		this.raw = raw;
		fullName = raw.getTypeName();
		if (raw instanceof final Class<?> c) {
			shortName = c.getSimpleName();
		} else {
			shortName = fullName;
		}
	}

	@Override
	public int hashCode() {
		return fullName().hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof final Type_<?> other) {
			return fullName().equals(other.fullName());
		}
		return false;
	}

	@Override
	public String toString() {
		return name();
	}

	//======================================= 元信息

	@Override
	public Type unwrap() {
		return raw;
	}

	@Override
	public String name() {
		return fullName;
	}

	@Override
	public String shortName() {
		return shortName;
	}

	@Override
	public Type_<?> elementType() {
		return this;
	}

	/**
	 * 是否基本类型。注意：包括void.class。
	 *
	 * @return
	 */
	public boolean isPrimitive() {
		return elementType().isPrimitive();
	}

	/**
	 * 是否基本类型的包装类。注意：包括Void.class（是void.class的包装类）
	 *
	 * @return
	 */
	public boolean isBoxing() {
		return elementType().isBoxing();
	}

	/**
	 * 是否类（而非接口、枚举、注解）
	 *
	 * @return 是否类（而非接口、枚举、注解）
	 */
	public boolean isExactClass() {
		return elementType().isExactClass();
	}

	/**
	 * 是否可以具有多个值的类型。包括集合、迭代、映射、数组等。
	 *
	 * @return
	 */
	public boolean isMultiVal() {
		return elementType().isMultiVal();
	}

	public boolean isMap() {
		return elementType().isMap();
	}

	public boolean isList() {
		return elementType().isList();
	}

	public boolean isSet() {
		return elementType().isSet();
	}

	public boolean isStream() {
		return elementType().isStream();
	}

	public boolean isArr() {
		return elementType().isArr();
	}

	public boolean isInterface() {
		return elementType().isInterface();
	}

	public boolean isEnum() {
		return elementType().isEnum();
	}

	public boolean isAnnotation() {
		return elementType().isAnnotation();
	}

	public boolean isNum() {
		return elementType().isNum();
	}

	public boolean isText() {
		return elementType().isText();
	}

	public boolean isBool() {
		return elementType().isBool();
	}

	/**
	 * 是否与时间有关。
	 *
	 * @return 是否
	 */
	public boolean isAboutTime() {
		return elementType().isAboutTime();
	}

	public boolean isThrowable() {
		return elementType().isThrowable();
	}

	public boolean isException() {
		return elementType().isException();
	}

	public boolean isRuntimeException() {
		return elementType().isRuntimeException();
	}

	public boolean isError() {
		return elementType().isError();
	}

	//=======================================继承

	/**
	 * 祖先类们，不包括自己。包括类和接口。无序。
	 *
	 * @return 祖先类型的增强对象。
	 */
	public Stream<Type_<?>> ancestors() {
		return Stream.concat(superClasses(), interfaces());
	}

	/**
	 * 获取父类的{@link Type_}对象。
	 *
	 * @return 父类的{@link EnhancedClass}对象。
	 */
	public abstract Type_<?> superClass();

	/**
	 * 获取本对象所有超类。
	 *
	 * @return 本对象所有超类。
	 */
	public Stream<Type_<?>> superClasses() {
		final Type_<?> s = superClass();
		if (s == null) {
			return Stream.of(Cls.OBJECT);
		}
		return Stream.concat(Stream.of(s), s.superClasses());
	}

	/**
	 * 获取所有实现/继承来的接口。递归，去重。
	 *
	 * @return 接口集
	 */
	public abstract Stream<Type_<?>> interfaces();

	public boolean hasInterfaces() {
		return interfaces().findAny().isPresent();
	}

	/**
	 * 是否参数类型或其后代。
	 *
	 * @param maybeMeOrAncestor 指定的类型
	 * @return 是否参数类型或其后代类。
	 */
	public boolean isOrSubOf(final Type_<?> maybeMeOrAncestor) {
		return equals(maybeMeOrAncestor) || ancestors().anyMatch(e -> e.equals(maybeMeOrAncestor));
	}

	/**
	 * 是否指定类型或其超类。
	 *
	 * @param maybeMeOrProgeny 指定的类型
	 * @return 是否指定类型或其超类。
	 */
	public boolean isOrSuperOf(final Type_<?> maybeMeOrProgeny) {
		return equals(maybeMeOrProgeny) || maybeMeOrProgeny.isOrSubOf(this);
	}

	//===========================================成员

	/**
	 * 获取所有构造器。
	 * todo:若未显式定义，是否包含默认构造器？
	 *
	 * @return 所有的构造器
	 */
	public abstract Stream<Constructor_<ObjType>> constructors();

	/**
	 * 获取默认构造器（即那个无参的）
	 *
	 * @return
	 */
	public Constructor_<ObjType> defaultConstructor() {
		return constructors().filter(e -> !e.hasParams()).findFirst().orElse(null);
	}

	/**
	 * 是否存在默认构造器（即那个无参的）
	 *
	 * @return
	 */
	public boolean hasDefaultConstructor() {
		return constructors().anyMatch(e -> !e.hasParams());
	}

	/**
	 * 本类自己定义的方法。包括覆写的。
	 *
	 * @return 方法集
	 */
	public abstract Stream<Method_> myOwnMethods();

	public boolean hasMyOwnMethods() {
		return myOwnMethods().findAny().isPresent();
	}

	/**
	 * 获取所有方法，包括继承来的、静态的。去重。无序。
	 *
	 * @return 方法集
	 */
	public Stream<Method_> methods() {
		return Stream.concat(myOwnMethods(), ancestors().flatMap(Type_::methods));
	}

	/**
	 * 根据名字、参数类型列表获取一个“合适的”方法。包括继承的、静态的。
	 *
	 * @param name 方法名
	 * @param paramTypes 参数类型列表
	 * @return 若无，返回null。
	 */
	public Method_ methodOf(final String name, final Class<?>... paramTypes) {
		return methods()//
				.filter(e -> e.name().equals(name)//
						&& ClassKit.checkAllAssignable(e.paramClasses0(), paramTypes))//
				.findFirst()//
				.orElse(null);
	}

	/**
	 * 是否存在指定的方法。包括继承的。
	 *
	 * @param name 方法名
	 * @param paramTypes 参数类型集
	 * @return 是否
	 */
	public boolean hasMethod(final String name, final Class<?>... paramTypes) {
		return methodOf(name, paramTypes) != null;
	}

	/**
	 * 自有的字段，包括静态的。
	 *
	 * @return 字段集。
	 */
	public abstract Stream<Field_> myOwnFields();

	public boolean hasOwnFields() {
		return myOwnFields().findAny().isPresent();
	}

	/**
	 * 获取所有字段，包括继承的、静态的。
	 *
	 * @return 字段集。
	 */
	public Stream<Field_> fields() {
		return Stream.concat(myOwnFields(), superClass().fields());
	}

	public boolean hasFields() {
		return fields().findAny().isPresent();
	}

	public Field_ field(final String fieldName) {
		return fields()//
				.filter(e -> e.name().equals(fieldName))//
				.findFirst()//
				.orElse(null);
	}

	public boolean hasField(final String fieldName) {
		return field(fieldName) != null;
	}

	public Field_ field(final Type_<?> type, final String fieldName) {
		return fields()//
				.filter(e -> e.name().equals(fieldName) && e.elementType().equals(type))//
				.findFirst()//
				.orElse(null);
	}

	public boolean hasField(final Type_<?> type, final String fieldName) {
		return field(type, fieldName) != null;
	}

	//======================================对象

	/**
	 * 是否本类型的实例。
	 *
	 * @param obj 对象
	 * @return 是否
	 */
	public boolean isPrototype(final Object obj) {
		return getClass().isInstance(obj);
	}

	/**
	 * todo：通过反射创建的算吗？
	 *
	 * @return
	 */
	public abstract boolean canNewInstance();

	/**
	 * 判断指定对象是否本类型的实例
	 *
	 * @return
	 */
	public boolean canInstanceBe(final Object obj) {
		if (obj == null) {
			return !isPrimitive();
		}
		return canInstanceBeType(obj.getClass());
	}

	/**
	 * 判断指定类型是否本类型的子类
	 *
	 * @return
	 */
	public boolean canInstanceBeType(final Class<?> otherType) {
		return isOrSuperOf(of(otherType));
	}

	//===============================================

	public static <T> Type_<T> of(final Type type) {
		return null;//todo
	}

	public static <T> Type_<T> of(final Class<T> clazz) {
		if (clazz == null) {
			return null;
		}
		return new Cls<>(clazz);
	}

//	public static <D extends GenericDeclaration> Type_<TypeVariable<D>> of(final TypeVariable<D> typeVariable) {
//		if (typeVariable == null) {
//			return null;
//		}
//		return new TypeVariable_(typeVariable);
//	}
//
//	public static Type_<ParameterizedType> of(final ParameterizedType parameterizedType, final Type_<?> owner) {
//		if (parameterizedType == null) {
//			return null;
//		}
//		return new ParameterizedType_(parameterizedType, owner);
//	}
//
//	public static Type_<GenericArrayType> of(final GenericArrayType genericArrayType, final Type_<?> owner) {
//		if (genericArrayType == null) {
//			return null;
//		}
//		return new GenericArrayType_(genericArrayType, owner);
//	}

//	public static Type_<?> of(final String fullName) {
//		return fullName == null ? ofNull() : new Type_<>(fullName);
//	}

	/**
	 * 表示“空”的对象的类型，我们用void.class（Void.Type）代替“null的类型”。
	 * java用null表示“空对象”，但null并不是一个特殊的对象，所以本身没有“类型”。这与其他一些语言不同，比如python，用None表示“空对象”，它确实是个对象，所以有类型。
	 *
	 * @return
	 */
	public static Cls<Void> ofNull() {
		return Cls.VOID_ENHANCED_CLASS;
	}
}
