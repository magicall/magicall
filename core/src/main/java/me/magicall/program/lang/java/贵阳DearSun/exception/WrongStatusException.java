/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class WrongStatusException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = -8744417173265795920L;
	private final String statusName;
	private final Object actualVal;
	private final Object expectVal;

	public WrongStatusException(final String statusName, final Object actualVal) {
		this(statusName, actualVal, null);
	}

	public WrongStatusException(final String statusName, final Object actualVal, final Throwable cause) {
		this(statusName, actualVal, null, cause);
	}

	public WrongStatusException(final String statusName, final Object actualVal, final Object expectVal) {
		this.statusName = statusName;
		this.actualVal = actualVal;
		this.expectVal = expectVal;
	}

	public WrongStatusException(final String statusName, final Object actualVal, final Object expectVal,
															final Throwable cause) {
		this(statusName, actualVal, expectVal);
		initCause(cause);
	}

	@Override
	public final synchronized Throwable initCause(final Throwable cause) {
		return super.initCause(cause);
	}

	public String getStatusName() {
		return statusName;
	}

	public Object getActualVal() {
		return actualVal;
	}

	public Object getExpectVal() {
		return expectVal;
	}
}
