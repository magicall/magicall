/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class NegativeDoubleException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = -4590855522537098660L;
	private static final Range<Double> AVAILABLE_RANGE = Range.closed(0.0D, Double.MAX_VALUE);

	public NegativeDoubleException(final String argName, final double actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public NegativeDoubleException(final String argName, final double actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public NegativeDoubleException(final String argName, final double actualVal, final double maxAvailable) {
		super(argName, actualVal, Range.closed(0.0D, maxAvailable));
	}

	public NegativeDoubleException(final String argName, final double actualVal, final double maxAvailable,
																 final Throwable cause) {
		this(argName, actualVal, maxAvailable);
		initCause(cause);
	}
}
