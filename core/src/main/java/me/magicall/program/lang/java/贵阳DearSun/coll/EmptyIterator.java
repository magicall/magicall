/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author Liang Wenjian.
 */
public final class EmptyIterator<E> implements Iterator<E> {

	public static final EmptyIterator<Object> INSTANCE = new EmptyIterator<>();

	private EmptyIterator() {
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public E next() {
		throw new NoSuchElementException();
	}

	@Override
	public void remove() {
		throw new IllegalStateException();
	}

	@SuppressWarnings("unchecked")
	public static <E> EmptyIterator<E> instance() {
		return (EmptyIterator<E>) INSTANCE;
	}
}
