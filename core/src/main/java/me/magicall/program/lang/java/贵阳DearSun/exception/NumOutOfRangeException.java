/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class NumOutOfRangeException extends WrongArgException {

	@Serial
	private static final long serialVersionUID = 1826680677475720337L;

	public <T extends Number & Comparable<T>> NumOutOfRangeException(final String argName, final Number actualVal,
																																	 final Range<T> range) {
		super(argName, actualVal, range);
	}

	public <T extends Number & Comparable<T>> NumOutOfRangeException(final String argName, final Number actualVal,
																																	 final Range<T> range, final Throwable cause) {
		this(argName, actualVal, range);
		initCause(cause);
	}

	@Override
	public Number getActualVal() {
		return (Number) super.getActualVal();
	}

	@SuppressWarnings("unchecked")
	public Range<? extends Number> getRange() {
		return (Range<? extends Number>) getExpectVal();
	}
}
