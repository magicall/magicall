/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;

/**
 * 类型的成员
 */
abstract class TypeMember<_Member extends AccessibleObject & Member>
		implements JavaRuntimeElement<_Member>, HasModifierFlags<_Member> {
	protected final _Member raw;

	TypeMember(final _Member raw) {
		this.raw = raw;
	}

	@Override
	public _Member unwrap() {
		return raw;
	}

	/**
	 * 类名/字段名/方法名/……
	 *
	 * @return
	 */
	@Override
	public String name() {
		return raw.getName();
	}

	@Override
	public Cls<?> owner() {
		return declaringClass();
	}

	public Cls<?> declaringClass() {
		return new Cls<>(raw.getDeclaringClass());
	}

	//------------------------------------值和类型

	/**
	 * 尝试获取本成员的值。
	 * - 字段：获取字段值。若为静态字段，无需参数；否则，第一个参数为字段值的主子对象（获取谁的该字段值）。
	 * - 方法：获取方法返回值，即尝试调用方法。若为静态方法，参数即方法参数；否则，第一个参数为方法调用者对象（调用谁的该方法），后续参数是方法参数。
	 * - 构造器：构造器返回值，即构造新对象。参数是构造器的参数。
	 *
	 * @param obj 参数
	 * @param <V> 期望的值的类型
	 * @return 值
	 */
	public abstract <V> V tryToFetchVal(final Object... obj);

	//------------------修饰符

	@Override
	public int modifierFlags() {
		return raw.getModifiers();
	}

	/**
	 * 指定对象是否可调用本成员？
	 * 比如 List.size() 方法，肯定不能用一个String来调用。
	 *
	 * @return
	 */
	@Override
	public boolean isAccessibleBy(final Object obj) {
		return raw.canAccess(obj);
	}

	/**
	 * 确保本成员可被反射。
	 * 对于非public的，设置之后就可以反射了。（public的本来就可以反射）
	 *
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public TypeMember<_Member> ensureAccessible() {
		if (!raw.isAccessible()) {//虽然isAccessible方法被弃用，但代替的方法canAccess实际上不能检查是否设置过。
			ensureNotFinal();
			raw.setAccessible(true);
		}
		return this;
	}

	/**
	 * 把本成员的可访问性恢复回去，是 {@link #ensureAccessible()} 的反操作。
	 * 对非public的，恢复之后就不可以反射了。
	 *
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public TypeMember<_Member> ensureBackAccessible() {
		final var unwrap = unwrap();
		if (unwrap.isAccessible()) {
			ensureNotFinal();
			unwrap.setAccessible(false);
		}
		return this;
	}
}
