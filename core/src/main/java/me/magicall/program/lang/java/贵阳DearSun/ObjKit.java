/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import com.google.common.collect.Sets;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.Serial;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ObjKit extends Kit<Object> implements Serializable {
	@Serial
	private static final long serialVersionUID = 2807499119655747354L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Collections.singletonList(Object.class);

	public static final ObjKit INSTANCE = new ObjKit();

	private static final Object EMPTY_VALUE = new Object();

	//===============================

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Stream<String> supportedTypeNames() {
		return Stream.concat(super.supportedTypeNames(), Stream.of("obj"));
	}

	@Override
	public Object parse(final String source) {
		return source;
	}

	@Override
	public <T1, T2> Object emptyVal() {
		return EMPTY_VALUE;
	}

	//==========================================================================

	@SuppressWarnings("unchecked")
	public static <T> T cast(final Object obj) {
		return (T) obj;
	}

	public static Field field(final Class<?> clazz, final String fieldName) {
		return FieldUtils.getDeclaredField(clazz, fieldName, true);
	}

	/**
	 * 取字段值。
	 *
	 * @return
	 */
	public static Object fieldVal(final Object o, final String fieldName) {
		if (o instanceof Map) {
			return ((Map<?, ?>) o).get(fieldName);
		}
		//todo：目前仅支持直接读字段。以后可增加支持getter
		try {
			return FieldUtils.readField(o, fieldName, true);
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 令字段等于指定值。
	 */
	public static boolean letFieldBe(final Object target, final Field field, final Object value) {
		try {
			FieldUtils.writeField(field, target, value);
			return true;
		} catch (final IllegalAccessException e) {
			//暂时先这样吧
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 把所有疑似多对象的东西转化为列表。支持Iterable、Iterator、Map、数组、罗列（Enumeration）、Stream、Spliterator。
	 * 若参数非以上对象，则返回一个单元素列表，唯一元素就是参数。
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Stream<?> toList(final Object o) {
		if (o == null) {
			return Stream.empty();
		}
		if (o instanceof Iterable) {
			return StreamSupport.stream(((Iterable<?>) o).spliterator(), false);
		}
		if (o instanceof Iterator) {
			return CollKit.stream((Iterator<?>) o);
		}
		if (isArr(o)) {
			return ArrKit.asList(o).stream();
		}
		if (o instanceof Stream) {
			return (Stream<?>) o;
		}
		if (o instanceof Spliterator) {
			return StreamSupport.stream((Spliterator) o, false);
		}
		if (o instanceof Enumeration) {
			return Collections.list((Enumeration<Object>) o).stream();
		}
		return Stream.of(o);
	}

	public static boolean isMultiValue(final Object o) {
		return o instanceof Iterable//
				|| o instanceof Iterator//
				|| o instanceof Stream//
				|| isArr(o)//
				|| o instanceof Spliterator//
				|| o instanceof Enumeration<?>;
	}

	/**
	 * 深度比较两个对象是否相等.
	 * tips:
	 * 【java.utils.Arrays.deepEquals没有解决两个数组互相引用的问题,该问题会引发一个死循环(java.lang.StackOverflowError)
	 * 此方法会对这种情况返回false
	 * 示例:
	 * Object[] o1 = { null };	Object[] o2 = { null };
	 * o1[0] = o2;		o2[0] = o1;
	 * System.out.print(ObjUtil.OBJ.deepEquals(o1,o2);
	 * System.out.print(Arrays.deepEquals(o1, o2));】
	 *
	 * @param o1 对象1
	 * @param o2 对象2
	 * @return 是否深度相等
	 */
	public static boolean deepEquals(final Object o1, final Object o2) {
		if (o1 == o2) {
			return true;
		}
		if (o1 == null) {
			return false;
		}
		if (o1.equals(o2)) {
			return true;
		}

		return deepEquals(o1, o2, null);
	}

	static boolean deepEquals(final Object o1, final Object o2, final Set<Object> escape) {
		if (escape != null && (escape.contains(o1) || escape.contains(o2))) {
			return false;
		}

		final var c1 = o1.getClass();
		final var c2 = o2.getClass();
		//其中一个不是数组而另一个是数组的情况
		if (!c1.isArray()) {
			if (!c2.isArray()) {//都不是数组
				if (o1.equals(o2)) {
					return true;
				}
				if (o1 instanceof Number && o2 instanceof Number) {//数字比较
					return NumKit.deepEquals((Number) o1, (Number) o2);
				}
			}
			return false;
		} else if (!c2.isArray()) {
			return false;
		}
		//都是数组
		final var cc1 = c1.getComponentType();
		final var cc2 = c2.getComponentType();
		if (cc1.isPrimitive()) {
			if (cc2.isPrimitive()) {
				return ArrKit.primitivesArrEquals(o1, o2);
			} else {//这里暂时没算外包类
				return false;
			}
		} else if (cc2.isPrimitive()) {
			return false;
		}
		//都不是原始类型数组,是普通数组
		final Set<Object> escapeToUse;
		if (escape == null) {
			escapeToUse = Sets.newHashSet();
		} else {
			escapeToUse = escape;
		}
		escapeToUse.add(o1);
		escapeToUse.add(o2);
		return ArrKit.deepEquals((Object[]) o1, (Object[]) o2, escapeToUse);
	}

	/**
	 * 判断一个对象是不是数组。可以处理基础类型数组。
	 * 注意：基础类型数组不是Object[]的子类！所以不能直接用 instanceof Object[] 来判断。
	 *
	 * @param obj 对象
	 * @return 是否数组
	 */
	public static boolean isArr(final Object obj) {
		return obj != null && obj.getClass().isArray();
	}

	/**
	 * 把一个身份不明的疑似数组的对象搞成一个数组。
	 *
	 * @param source 对象
	 * @return 如果参数为null、非数组， 返回null。
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] toObjectArray(final Object source) {
		if (source == null) {
			return null;
		}
		if (source instanceof Object[]) {
			return (T[]) source;
		}
		final var c = source.getClass();
		if (!c.isArray()) {
			return null;
		}
		final var length = Array.getLength(source);
		final var cc = c.getComponentType();
		if (length == 0) {
			return (T[]) Array.newInstance(cc, 0);
		}
		final var newArray = (Object[]) Array.newInstance(cc, length);
		IntStream.range(0, length).forEach(i -> newArray[i] = Array.get(source, i));
		return (T[]) newArray;
	}

	public static String deepToString(final Object obj) {
		if (obj == null) {
			return StrKit.NULL;
		}
		return deepToString(obj, new StringBuilder(), new HashSet<>()).toString();
	}

	static StringBuilder deepToString(final Object obj, final StringBuilder sb, final Set<Object> escape) {
		if (obj == null) {
			return sb.append(StrKit.NULL);
		}
		if (escape.contains(obj)) {
			return sb.append("...");
		}

		final var c = obj.getClass();
		if (c.isArray()) { //需要对数组每一个元素进行toString
			escape.add(obj);
			sb.append('[');
			final var len = Array.getLength(obj);
			IntStream.range(0, len).forEach(i -> {
				if (i != 0) {
					sb.append(',');
				}
				deepToString(Array.get(obj, i), sb, escape);
			});
			sb.append(']');
		} else if (obj instanceof Iterable<?>) { //需要对可遍历类每一个元素进行toString
			escape.add(obj);
			final var iterable = (Iterable<?>) obj;
			for (final Object o : iterable) {
				deepToString(o, sb, escape);
			}
		} else if (obj instanceof Map<?, ?>) {
			escape.add(obj);
			final var m = (Map<?, ?>) obj;
			sb.append('{');
			if (!m.isEmpty()) {
				m.forEach((key, value) -> {
					deepToString(key, sb, escape).append(" = ");
					deepToString(value, sb, escape).append(',');
				});
				sb.deleteCharAt(sb.length() - 1);
			}
			sb.append('}');
		} else {
			sb.append(obj);
		}
		return sb;
	}

	//======================================================
	private Object readResolve() {
		return INSTANCE;
	}
}
