/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class LongKit extends NotFractionKit<Long, long[]> {
	@Serial
	private static final long serialVersionUID = -5401432022261827640L;

	public static final LongKit INSTANCE = new LongKit();

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Long.class, long.class);

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Stream<String> supportedTypeNames() {
		return Stream.concat(super.supportedTypeNames(), Stream.of("bigint"));
	}

	@Override
	public Long parse(final String source) {
		try {
			return Long.parseLong(source);
		} catch (final NumberFormatException ignored) {
			return null;
		}
	}

	@Override
	public String primitiveArrFlag() {
		return "J";
	}

	@Override
	public Long fromLong(final long value) {
		return value;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
