/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.AbstractSet;
import java.util.BitSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 二进制位开关。支持2的32次方个位。
 *
 * @author Liang Wenjian.
 */
public class BitSwitchers extends AbstractSet<Integer> implements NavigableSetSupport<Integer> {
	private final BitSet negative;
	private final BitSet positive;

	public BitSwitchers() {
		this(new BitSet(), new BitSet());
	}

	public BitSwitchers(final Collection<Integer> c) {
		this();
		addAll0(c);
	}

	private BitSwitchers(final BitSet negative, final BitSet positive) {
		this.negative = negative;
		this.positive = positive;
	}

	/**
	 * 获取指定整数的包含状态。
	 *
	 * @param integer 指定的整数
	 * @return 本散列表中是否包含该整数。
	 */
	public boolean contains(final Integer integer) {
		return integer < 0 ? negative.get(negativeValToIndex(integer)) : positive.get(integer);
	}

	private static int negativeValToIndex(final Integer negative) {
		return negative - Integer.MIN_VALUE;
	}

	private static int indexToNegativeVal(final int index) {
		return index + Integer.MIN_VALUE;
	}

	/**
	 * 将某位设置为true。
	 *
	 * @param bitSet 指定的位开关
	 * @param bitIndex 指定的位
	 * @return 是否发生了修改。
	 */
	private static boolean set(final BitSet bitSet, final Integer bitIndex) {
		if (bitSet.get(bitIndex)) {
			return false;
		}
		bitSet.set(bitIndex);
		return true;
	}

	/**
	 * 将某位设置为false。
	 *
	 * @param bitSet 指定的位开关
	 * @param bitIndex 指定的位
	 * @return 是否发生了修改。
	 */
	private static boolean clear(final BitSet bitSet, final Integer bitIndex) {
		if (bitSet.get(bitIndex)) {
			bitSet.clear(bitIndex);
			return true;
		}
		return false;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new BitSwitchersIterator(this);
	}

	@Override
	public int size() {
		final var size = longSize();
		//注：Set.size()注释上写着： If this set contains more than {@code Integer.MAX_VALUE} elements, returns {@code Integer.MAX_VALUE}.
		return size > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) size;
	}

	public long longSize() {
		final long cardinality1 = negative.cardinality();
		final long cardinality2 = positive.cardinality();
		return cardinality1 + cardinality2;
	}

	@Override
	public boolean add(final Integer integer) {
		return integer < 0 ? set(negative, negativeValToIndex(integer)) : set(positive, integer);
	}

	@Override
	public boolean remove(final Object o) {
		if (!(o instanceof Integer)) {
			return false;
		}
		final var integer = (Integer) o;
		return integer < 0 ? clear(negative, negativeValToIndex(integer)) : clear(positive, integer);
	}

	@Override
	public void clear() {
		negative.clear();
		positive.clear();
	}

	@Override
	public boolean contains(final Object o) {
		return o instanceof Integer && contains((Integer) o);
	}

	@Override
	public Stream<Integer> stream() {
		return IntStream.concat(negative.stream().filter(negative::get).map(BitSwitchers::indexToNegativeVal),
				positive.stream().filter(positive::get)).boxed();
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean addAll(final Collection<? extends Integer> c) {
		return addAll0((Collection<Integer>) c);
	}

	private boolean addAll0(final Collection<Integer> c) {
		if (c instanceof BitSwitchers) {
			final var other = (BitSwitchers) c;
			negative.or(other.negative);
			positive.or(other.positive);
		}
		return super.addAll(c);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		if (c instanceof BitSwitchers) {
			final var other = (BitSwitchers) c;
			negative.andNot(other.negative);
			positive.andNot(other.positive);
		}
		return super.removeAll(c);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		if (c instanceof BitSwitchers) {
			final var other = (BitSwitchers) c;
			return other.negative.stream().allMatch(negative::get) && other.positive.stream().allMatch(positive::get);
		}
		return super.containsAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		if (c instanceof BitSwitchers) {
			final var other = (BitSwitchers) c;
			return retain(negative, other.negative) && retain(positive, other.positive);
		}
		return super.retainAll(c);
	}

	private static boolean retain(final BitSet bitSet1, final BitSet bitSet2) {
		final boolean intersects = bitSet1.intersects(bitSet2);
		if (intersects) {
			bitSet1.and(bitSet2);
		} else {
			bitSet1.clear();
		}
		return intersects;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof BitSwitchers) {
			final BitSwitchers other = (BitSwitchers) o;
			return negative.equals(other.negative) && positive.equals(other.positive);
		}
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		var result = super.hashCode();
		result = 31 * result + negative.hashCode();
		result = 31 * result + positive.hashCode();
		return result;
	}

	@Override
	public Comparator<? super Integer> comparator() {
		return null;
	}

	@Override
	public Integer firstOrNull() {
		if (negative.isEmpty()) {
			if (positive.isEmpty()) {
				return null;
			}
			return positive.nextSetBit(0);
		} else {
			return indexToNegativeVal(negative.nextSetBit(0));
		}
	}

	@Override
	public Integer lastOrNull() {
		if (positive.isEmpty()) {
			if (negative.isEmpty()) {
				return null;
			}
			return indexToNegativeVal(negative.length() - 1);
		}
		return positive.length() - 1;
	}

	@Override
	public Integer lower(final Integer target) {//<
		return target == Integer.MIN_VALUE ? null : floor(target - 1);
	}

	@Override
	public Integer floor(final Integer target) {//<=
		if (target >= 0) {
			final var i = positive.previousSetBit(target);
			if (i >= 0) {
				return i;
			}
		}
		final var from = target < 0 ? negativeValToIndex(target) : Integer.MAX_VALUE;
		final var i = negative.previousSetBit(from);
		return i < 0 ? null : indexToNegativeVal(i);
	}

	@Override
	public Integer higher(final Integer target) {//>
		return Objects.equals(target, Integer.MAX_VALUE) ? null : ceiling(target + 1);
	}

	@Override
	public Integer ceiling(final Integer target) {//>=
		if (target < 0) {
			final var i = negative.nextSetBit(negativeValToIndex(target));
			if (i >= 0) {
				return indexToNegativeVal(i);
			}
		}
		final var i = positive.nextSetBit(Math.max(0, target));
		return i < 0 ? null : i;
	}

	@Override
	public NavigableSet<Integer> subSet(final Integer fromElement, final boolean fromInclusive, final Integer toElement,
																			final boolean toInclusive) {
		if (fromInclusive && fromElement == Integer.MAX_VALUE || toInclusive && toElement == Integer.MIN_VALUE) {
			return new EmptyNavigableSubSet<>(comparator());
		}
		if (Objects.equals(fromElement, toElement) && !(fromInclusive && toInclusive)) {
			return new EmptyNavigableSubSet<>(comparator());
		}
		return new NavigableSubSet<>(this, fromElement, fromInclusive, toElement, toInclusive);
	}

	@Override
	public NavigableSet<Integer> headSet(final Integer toElement, final boolean inclusive) {
		return subSet(Integer.MIN_VALUE, true, toElement, inclusive);
	}

	@Override
	public NavigableSet<Integer> tailSet(final Integer fromElement, final boolean inclusive) {
		return subSet(fromElement, inclusive, Integer.MAX_VALUE, true);
	}

	//==========================

	private static class BitSwitchersIterator implements Iterator<Integer> {
		final NavigableSet<Integer> raw;
		Integer cur;
		boolean started;

		private BitSwitchersIterator(final NavigableSet<Integer> raw) {
			this.raw = raw;
		}

		@Override
		public boolean hasNext() {
			return nextOrNull() != null;
		}

		@Override
		public Integer next() {
			final var i = nextOrNull();
			if (i == null) {
				throw new NoSuchElementException();
			}
			cur = i;
			started = true;
			return i;
		}

		private Integer nextOrNull() {
			final int i;
			if (cur == null) {
				if (started) {
					return null;
				}
				i = Integer.MIN_VALUE;
			} else {
				if (cur == Integer.MAX_VALUE) {
					return null;
				}
				i = cur + 1;//这里有cur+1的操作，所以这个Iterator不能用于其他类型。
			}
			return raw.ceiling(i);
		}

		@Override
		public void remove() {
			if (!started || cur == null) {
				throw new IllegalStateException();
			}
			raw.remove(cur);
		}
	}//class BitSwitchersIterator
}
