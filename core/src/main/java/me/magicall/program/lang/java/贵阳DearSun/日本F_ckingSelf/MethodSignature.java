/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.util.Arrays;
import java.util.Objects;

/**
 * 本类描述一个方法的签名。包括名字（全限定名）、参数（擦除泛型）列表。不包括返回值、异常定义。
 */
public final class MethodSignature {
	private static final Class<?>[] EMPTY_CLASS_ARR = new Class[0];

	public static final MethodSignature TO_STRING = new MethodSignature(Object.class, "toString");
	public static final MethodSignature EQUALS = new MethodSignature(Object.class, "equals", Object.class);
	public static final MethodSignature HASH_CODE = new MethodSignature(Object.class, "hashCode");

	private final Class<?> ownerClass;
	private final String name;
	private final Class<?>[] argClasses;

	public MethodSignature(final Class<?> ownerClass, final String name, final Class<?>... argClasses) {
		this.ownerClass = ownerClass;
		this.name = name;
		this.argClasses = argClasses == null ? EMPTY_CLASS_ARR : argClasses;
	}

	public Class<?> getOwnerClass() {
		return ownerClass;
	}

	public String getName() {
		return name;
	}

	public Class<?>[] getArgClasses() {
		return argClasses;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final var that = (MethodSignature) o;
		return Objects.equals(ownerClass, that.ownerClass) && Objects.equals(name, that.name) && Arrays.equals(argClasses,
				that.argClasses);
	}

	@Override
	public int hashCode() {
		var result = Objects.hash(ownerClass, name);
		result = 31 * result + Arrays.hashCode(argClasses);
		return result;
	}

	@Override
	public String toString() {
		final var sb = new StringBuilder(ownerClass.getName()).append('.').append(name).append('(');
		if (argClasses.length > 0) {
			var i = 0;
			for (final var argClass : argClasses) {
				sb.append(argClass.getName()).append(" arg").append(i).append(',');
				i++;
			}
			sb.deleteCharAt(sb.length() - 1);
		}
		return sb.append(')').toString();
	}
}
