/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

/**
 * @author Liang Wenjian
 */
public class NoSuchThingException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = -6741746013297678313L;

	private final String thingType;
	private final String thingId;

	public NoSuchThingException(final String thingType, final Object thingId) {
		this(thingType, String.valueOf(thingId));
	}

	public NoSuchThingException(final String thingType, final String thingId) {
		super("No such " + thingType + '\'' + thingId + '\'');
		this.thingType = thingType;
		this.thingId = thingId;
	}

	public NoSuchThingException(final String thingType, final String thingId, final Throwable cause) {
		this(thingType, thingId);
		initCause(cause);
	}

	public String getThingType() {
		return thingType;
	}

	public String getThingId() {
		return thingId;
	}
}
