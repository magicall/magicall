/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.runtime;

import javax.tools.SimpleJavaFileObject;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.time.Instant;

class VirtualJavaClassFile extends SimpleJavaFileObject {
	private final Instant lastModified = Instant.now();
	private final ByteArrayOutputStream outputStream;

	VirtualJavaClassFile(final String className, final Kind kind) {
		super(URI.create("string:///" + className.replace('.', '/') + kind.extension), kind);
		outputStream = new ByteArrayOutputStream();
	}

	public byte[] toBytes() {
		return outputStream.toByteArray();
	}

	@Override
	public OutputStream openOutputStream() {
		return outputStream;
	}

	@Override
	public long getLastModified() {
		return lastModified.getEpochSecond();
	}

	@Override
	public CharSequence getCharContent(final boolean ignoreEncodingErrors) {
		return outputStream.toString();
	}
}
