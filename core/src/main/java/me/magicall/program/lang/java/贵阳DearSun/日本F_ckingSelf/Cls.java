/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.stream.Stream;

public class Cls<ObjType> extends Type_<ObjType> implements Accessible, HasModifierFlags<Type> {
	public static final Cls<Object> OBJECT = new Cls<>(Object.class);
	public static final Cls<Void> VOID_ENHANCED_CLASS = new Cls<>(void.class);

	private final Class<ObjType> raw;

	public Cls(final Class<ObjType> raw) {
		super(raw);
		this.raw = raw;
	}

	public Class<ObjType> toClass() {
		return raw;
	}

	private boolean isNotNull() {
		return raw != null;
	}

	@Override
	public JavaRuntimeElement<?> owner() {
		if (isInMethod()) {
			return new Method_(toClass().getEnclosingMethod());
		}
		if (isInConstructor()) {
			return new Constructor_<>(toClass().getEnclosingConstructor());
		}
		if (isInner() || isInInitBlock()) {
			return of(toClass().getEnclosingClass());
		}
		return null;
	}

	@Override
	public Package pack() {
		if (!isNotNull()) {
			return null;
		}
		final var owner = owner();
		if (owner == null) {
			return toClass().getPackage();
		}
		return owner.pack();
	}

	@Override
	public Type_<?> topType() {
		if (!isNotNull()) {
			return null;
		}
		Class<?> cur = toClass();
		Class<?> next = cur.getEnclosingClass();
		while (next != null) {
			cur = next;
			next = cur.getEnclosingClass();
		}
		return of(cur);
	}

	//-------------------------------泛型

	@Override
	public Stream<Type_<?>> generics() {
		return isNotNull() ? Arrays.stream(toClass().getTypeParameters()).map(Type_::of) : Stream.empty();
	}

	/**
	 * 本类是否实现了某个泛型参数。
	 *
	 * @return 是否实现了某个泛型参数
	 */
	public boolean isImplementingGenericTypes() {
		return ClassKit.isImplementingGenericTypes(toClass());
	}

	//-------------------------------------

	public boolean isAbstract() {
		return isNotNull() && ClassKit.isAbstract(toClass());
	}

	/**
	 * strictfp 也可用于修饰class、method。
	 *
	 * @return 是否使用了strictfp
	 */
	public boolean isStrict() {
		return isNotNull() && ClassKit.isStrictFloatingPoint(toClass());
	}

	/**
	 * 是否匿名类
	 *
	 * @return 是否匿名类
	 */
	public boolean isAnonymous() {
		return isNotNull() && ClassKit.isAnonymous(toClass());
	}

	/**
	 * 是否内部类
	 *
	 * @return 是否内部类
	 */
	public boolean isInner() {
		return isNotNull() && ClassKit.isInner(toClass());
	}

	public boolean isInMethod() {
		return isNotNull() && ClassKit.isInMethod(toClass());
	}

	public boolean isInConstructor() {
		return isNotNull() && ClassKit.isInConstructor(toClass());
	}

	public boolean isInInitBlock() {
		return isNotNull() && ClassKit.isInInitBlock(toClass());
	}

	@Override
	public int modifierFlags() {
		return isNotNull() ? raw.getModifiers() : 0;
	}

	//==========================================继承

	/**
	 * 祖先类们。包括类和接口。无序。
	 *
	 * @return 祖先类型的增强对象。
	 */
	@Override
	public Stream<Type_<?>> ancestors() {
		return isNotNull() ? ClassKit.ancestorsOf(toClass()).map(Type_::of) : Stream.empty();
	}

	@Override
	public Type_<?> superClass() {
		if (!isNotNull()) {
			return null;
		}
		final Class<?> parent = raw.getSuperclass();
		return parent == null ? null : of(parent);
	}

	/**
	 * 获取本对象所有超类。
	 *
	 * @return 本对象所有超类。
	 */
	@Override
	public Stream<Type_<?>> superClasses() {
		return isNotNull() ? ClassKit.superClassesOf(toClass()).map(Type_::of) : Stream.empty();
	}

	/**
	 * 获取所有实现/继承来的接口。递归，去重。
	 *
	 * @return 接口集
	 */
	@Override
	public Stream<Type_<?>> interfaces() {
		return isNotNull() ? ClassKit.interfacesOf(toClass()).map(Type_::of) : Stream.empty();
	}

	@Override
	public boolean hasInterfaces() {
		return isNotNull() && ClassKit.hasInterface(toClass());
	}

	//===========================================成员

	/**
	 * 获取所有构造方法。
	 *
	 * @return 所有的构造方法
	 */
	@Override
	public Stream<Constructor_<ObjType>> constructors() {
		return isNotNull() ? ClassKit.constructorsOf(toClass()).map(Constructor_::new) : Stream.empty();
	}

	/**
	 * 本类自己定义的方法。包括覆写的。
	 *
	 * @return 方法集
	 */
	@Override
	public Stream<Method_> myOwnMethods() {
		return isNotNull() ? ClassKit.ownMethodsOf(toClass()).map(Method_::new) : Stream.empty();
	}

	/**
	 * 获取所有方法，包括继承来的、静态的。去重。无序。
	 *
	 * @return 方法集
	 */
	@Override
	public Stream<Method_> methods() {
		return isNotNull() ? ClassKit.methodsOf(toClass()).map(Method_::new) : Stream.empty();
	}

	/**
	 * 根据名字、参数类型列表获取一个“合适的”方法。包括继承的、静态的。
	 *
	 * @param name 方法名
	 * @param paramTypes 参数类型列表
	 * @return 若无，返回null。
	 */
	@Override
	public Method_ methodOf(final String name, final Class<?>... paramTypes) {
		return isNotNull() ? new Method_(ClassKit.methodOf(toClass(), name, paramTypes)) : null;
	}

	/**
	 * 是否存在指定的方法。包括继承的。
	 *
	 * @param name 方法名
	 * @param paramTypes 参数类型集
	 * @return 是否
	 */
	@Override
	public boolean hasMethod(final String name, final Class<?>... paramTypes) {
		return isNotNull() && ClassKit.hasMethod(toClass(), name, paramTypes);
	}

	/**
	 * 自有的字段，包括静态的。
	 *
	 * @return 字段集。
	 */
	@Override
	public Stream<Field_> myOwnFields() {
		return isNotNull() ? ClassKit.ownFieldsOf(toClass()).map(Field_::new) : Stream.empty();
	}

	@Override
	public Stream<Field_> fields() {
		return isNotNull() ? ClassKit.fieldsOf(toClass()).map(Field_::new) : Stream.empty();
	}

	@Override
	public boolean hasFields() {
		return isNotNull() && ClassKit.hasFields(toClass());
	}

	@Override
	public Field_ field(final String fieldName) {
		return isNotNull() ? new Field_(ClassKit.fieldCalled(toClass(), fieldName)) : null;
	}

	@Override
	public boolean hasField(final String fieldName) {
		return isNotNull() && ClassKit.hasFieldCalled(toClass(), fieldName);
	}

	public Field_ field(final Class<?> type, final String fieldName) {
		return isNotNull() ? new Field_(ClassKit.fieldOf(toClass(), fieldName, type)) : null;
	}

	public boolean hasField(final Class<?> type, final String fieldName) {
		return isNotNull() && ClassKit.hasField(toClass(), fieldName, type);
	}

	//======================================对象

	@Override
	public boolean canNewInstance() {
		return isNotNull() && ClassKit.canNewInstance(toClass());
	}

	@Override
	public boolean canInstanceBe(final Object obj) {
		if (!isNotNull()) {
			return false;
		}
		if (obj == null) {
			return !isPrimitive();
		}
		return toClass().isInstance(obj) || canInstanceBeType(obj.getClass());
	}

	@Override
	public boolean canInstanceBeType(final Class<?> otherType) {
		if (isNotNull()) {
			return ClassKit.fit(toClass(), otherType) || checkNumTypes(otherType);
		}
		return false;
	}

	private boolean checkNumTypes(final Class<?> c) {
		if (raw == double.class || raw == Double.class) {
			return c == float.class || c == Float.class//
					|| c == long.class || c == Long.class//
					|| c == int.class || c == Integer.class//
					|| c == short.class || c == Short.class//
					|| c == byte.class || c == Byte.class//
					|| c == char.class || c == Character.class//
					|| c == double.class || c == Double.class;
		} else if (raw == float.class || raw == Float.class) {
			return c == float.class || c == Float.class//
					|| c == long.class || c == Long.class//
					|| c == int.class || c == Integer.class//
					|| c == short.class || c == Short.class//
					|| c == byte.class || c == Byte.class//
					|| c == char.class || c == Character.class;
		} else if (raw == long.class || raw == Long.class) {
			return c == long.class || c == Long.class//
					|| c == int.class || c == Integer.class//
					|| c == short.class || c == Short.class//
					|| c == byte.class || c == Byte.class//
					|| c == char.class || c == Character.class;
		} else if (raw == int.class || raw == Integer.class) {
			return c == int.class || c == Integer.class//
					|| c == short.class || c == Short.class//
					|| c == byte.class || c == Byte.class//
					|| c == char.class || c == Character.class;
		} else if (raw == short.class || raw == Short.class) {
			return c == short.class || c == Short.class//
					|| c == byte.class || c == Byte.class//
					|| c == char.class || c == Character.class;
		}
		return false;
	}
}
