/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Kit;
import me.magicall.program.lang.java.贵阳DearSun.ObjKit;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MapKit extends Kit<Map<?, ?>> implements Serializable {
	@Serial
	private static final long serialVersionUID = 1689123743472449851L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Collections.singletonList(Map.class);

	public static final MapKit INSTANCE = new MapKit();

	//================================================

	private Object readResolve() {
		return INSTANCE;
	}

	//--------------------------------

	@Override
	public boolean isEmpty(final Map<?, ?> source) {
		return source == null || source.isEmpty();
	}

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Map<?, ?> parse(final String source) {
		return null;//todo 好像没法搞。
	}

	@Override
	public <K, V> Map<K, V> emptyVal() {
		return Collections.emptyMap();
	}

	@Override
	public boolean greater(final Map<?, ?> source, final Map<?, ?> target) {
		if (source == null) {
			return false;
		}
		if (target == null) {
			return true;
		}
		return source.size() > target.size() && source.entrySet().containsAll(target.entrySet());
	}

	@Override
	public boolean greaterEquals(final Map<?, ?> source, final Map<?, ?> target) {
		if (target == null) {
			return true;
		}
		if (source == null) {
			return false;
		}
		return source.size() >= target.size() && source.entrySet().containsAll(target.entrySet());
	}

	//--------------------------------------------

	/**
	 * 将一个来历不明的map强转成某种类型的map
	 * 可用于类似如下情况:
	 * <code>
	 * Map&lt;Object,Object&gt; map=new HashMap&lt;String,Integer&gt;();
	 * Map&lt;String,Integer&gt; map2=forceSuit(map);
	 * </code>
	 * 注:这是"非安全的"，可能取出来的值类型不对。使用者自己保证。
	 *
	 * @param <K> 键的类型
	 * @param <V> 值的类型
	 * @param source 源映射
	 * @return 强转泛型后的映射
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> cast(final Map<?, ?> source) {
		return (Map<K, V>) source;
	}

	public boolean deepEquals(final Map<?, ?> map1, final Map<?, ?> map2) {
		if (isEmpty(map1) && isEmpty(map2)) {
			return true;
		}
		for (final Entry<?, ?> e : map1.entrySet()) {
			final var v1 = e.getValue();
			final var v2 = map2.get(e.getKey());
			if (!ObjKit.deepEquals(v1, v2)) {
				return false;
			}
		}
		return true;
	}

	public <K, V> Entry<K, V> getEntry(final Map<K, V> map, final int targetIndex) {
		if (map == null) {
			return null;
		}
		if (targetIndex >= map.size()) {
			return null;
		}
		var index = 0;
		for (final var entry : map.entrySet()) {
			if (index == targetIndex) {
				return entry;
			}
			index++;
		}
		return null;
	}

	static <A, B, D> Map<A, D> remapVal(final Map<A, B> source,
																			final Function<? super Entry<A, B>, ? extends D> valMapper) {
		return remap(source, Entry::getKey, valMapper);
	}

	static <A, B, C, D> Map<C, D> remap(final Map<A, B> source,
																			final Function<? super Entry<A, B>, ? extends C> keyMapper,
																			final Function<? super Entry<A, B>, ? extends D> valMapper) {
		return source.entrySet().stream().collect(Collectors.toMap(keyMapper, valMapper));
	}

	static <K, V> Map<K, V> map(final Stream<? extends K> source, final Function<? super K, ? extends V> valueMapper) {
		return source.collect(Collectors.toMap(Function.identity(), valueMapper));
	}
}
