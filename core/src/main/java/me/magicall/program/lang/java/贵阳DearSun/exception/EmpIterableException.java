/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;

import java.io.Serial;
import java.util.Collections;

/**
 * @author Liang Wenjian.
 */
public class EmpIterableException extends NoElementException {

	@Serial
	private static final long serialVersionUID = 2620958511386269749L;

	private EmpIterableException(final String argName, final Iterable<?> actualVal) {
		super(argName, actualVal);
	}

	public static EmpIterableException coll(final String argName) {
		return new EmpIterableException(argName, CollKit.INSTANCE.emptyVal());
	}

	public static EmpIterableException list(final String argName) {
		return new EmpIterableException(argName, Collections.emptyList());
	}

	public static EmpIterableException set(final String argName) {
		return new EmpIterableException(argName, Collections.emptySet());
	}
}
