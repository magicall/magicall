/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.regex;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Pattern;

public interface Patterns {
	Pattern NUM_PATTERN = Pattern.compile("\\d+");
	Pattern COMMA_PATTERN = Pattern.compile(",");
	Pattern SPACES_PATTERN = Pattern.compile("\\s+");
	Pattern PATH_PATTERN = Pattern.compile("/");
	Pattern HYPHEN_PATTERN = Pattern.compile("-");
	Pattern DOT_PATTERN = Pattern.compile("\\.");
	Pattern REGEX_GROUP_NAME_PATTERN = Pattern.compile("\\(\\?<([^>]+)>.*?\\)");//todo：暂不处理组中嵌套右括号字面值（转义的右括号，即“\\)”）

	static String[] commaSplit(final String s) {
		return COMMA_PATTERN.split(s);
	}

	static String[] hyphenSplit(final String s) {
		return HYPHEN_PATTERN.split(s);
	}

	static String[] dotSplit(final String s) {
		return DOT_PATTERN.split(s);
	}

	static String[] spaceSplit(final String s) {
		return SPACES_PATTERN.split(s);
	}

	/**
	 * 获取一个模式中的所有具名组的名字。
	 * 注意，非捕获组都没有名字。
	 *
	 * @param pattern 指定的模式。
	 * @return 组名列表。
	 */
	static List<String> groupNamesIn(final Pattern pattern) {
		final var patternStr = pattern.pattern();
		final List<String> groupNames = Lists.newLinkedList();
		for (final var patternMeta = REGEX_GROUP_NAME_PATTERN.matcher(patternStr); patternMeta.find(); ) {
			groupNames.add(patternMeta.group(1));
		}
		return groupNames;
	}
}
