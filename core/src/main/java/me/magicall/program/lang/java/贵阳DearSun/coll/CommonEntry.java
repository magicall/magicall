/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.Map.Entry;

/**
 * 通用的{@link Entry}.
 *
 * @param <K> 键的类型。
 * @param <V> 值的类型。
 */
public class CommonEntry<K, V> implements Entry<K, V> {

	private final K key;
	private V val;

	public CommonEntry(final K key, final V value) {
		super();
		this.key = key;
		val = value;
	}

	@Override
	public K getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return val;
	}

	@Override
	public V setValue(final V value) {
		final var rt = val;
		val = value;
		return rt;
	}

	@Override
	public String toString() {
		return key + "=" + val;
	}
}
