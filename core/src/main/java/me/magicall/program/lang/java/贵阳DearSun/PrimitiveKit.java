/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;

import java.io.Serial;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public abstract class PrimitiveKit<P, A> extends Kit<P> implements Serializable {

	//-----------------------------类型

	protected Class<?> primitiveClass() {
		return supportedClasses().collect(Collectors.toList()).get(1);
	}

	//---------------------------数组类型
	public abstract String primitiveArrFlag();

	/**
	 * 返回基本类型的n维数组的Class对象。
	 * 注意:低维数组并非高维数组的父类！
	 * 即:int[][]对象 并非 一个int[]对象 | int[][].class不是int[].class的子类 |  (new int[][] instanceof new int[])==false
	 *
	 * @param nDim 维数
	 * @return 数组的{@link Class}对象
	 */
	public Class<?> primitiveArrClass(final int nDim) {
		return ClassKit.arrClass(primitiveArrFlag(), nDim);
	}

	/**
	 * 返回原始类型的1维数组的Class对象。
	 *
	 * @return 原始类型的1维数组的Class对象
	 */
	@SuppressWarnings("unchecked")
	public Class<A> primitiveArrClass() {
		return (Class<A>) primitiveArrClass(1);
	}

	//----------------------------数组实例

	/**
	 * 返回一个基本类型（而非包装类）的数组，包含n个指定值。
	 *
	 * @param size 长度
	 * @param val 指定值
	 * @return 数组
	 */
	public A nPrimitive(final int size, final P val) {
		if (size < 0) {
			throw new WrongArgException("size", size, ">=0");
		}
		final var rt = newPrimitiveArr(size);
		IntStream.iterate(0, i -> i < size - 1, i -> i + 1).forEach(i -> setValAt(rt, i, val));
		return rt;
	}

	/**
	 * 返回一个指定长度的基本类型（而非包装类）的数组，每个元素都是该种基本类型的“空”值（{@link #emptyVal()}。
	 * 用于创建稀疏矩阵很方便。
	 *
	 * @param size 数量
	 * @return 指定长度的基本类型（而非包装类）的数组
	 */
	public A nPrimitiveEmptyVals(final int size) {
		return nPrimitive(size, emptyVal());
	}

	/**
	 * 返回空的（长度为0）基本类型（而非包装类）的数组。
	 *
	 * @return 空的（长度为0）基本类型（而非包装类）的数组。
	 */
	public A emptyPrimitiveArr() {
		return nPrimitiveEmptyVals(0);
	}

	public int arrLen(final A arr) {
		return arr == null ? 0 : arrLen0(arr);
	}

	/**
	 * 判断一个原始类型数组是否为空
	 *
	 * @param arr 数组
	 * @return 若为空则返回true
	 */
	public boolean isEmptyArr(final A arr) {
		return arrLen(arr) == 0;
	}

	public Stream<P> toStream(final A arr) {
		return Arrays.stream(wrap(arr));
	}

	public Iterator<P> iterate(final A arr) {
		return toStream(arr).iterator();
	}

	@SuppressWarnings("unchecked")
	public P valAt(final A arr, final int index) {
		return (P) Array.get(arr, index);
	}

	public void setValAt(final A arr, final int index, final P val) {
		Array.set(arr, index, val);
	}

	public int frequency(final P target, final A arr) {
		return (int) toStream(arr).filter(Predicate.isEqual(target)).count();
	}

	/**
	 * 将一个原始类型数组复制为一个包装类型的数组.
	 *
	 * @param arr 数组
	 * @return 包装类数组
	 */
	public P[] wrap(final A arr) {
		if (arr == null) {
			return emptyArr();
		}
		final var len = arrLen0(arr);
		final var rt = arr(len);
		IntStream.range(0, len).forEach(i -> Array.set(rt, i, valAt(arr, i)));
		return rt;
	}

	/**
	 * 将一个包装类型的数组复制为一个原始类型的数组
	 *
	 * @param arr 数组
	 * @return 原始类型数组
	 */
	@SafeVarargs
	public final A unwrap(final P... arr) {
		if (arr == null) {
			return emptyPrimitiveArr();
		}
		final var len = arr.length;
		if (len == 0) {
			return emptyPrimitiveArr();
		}
		final var rt = newPrimitiveArr(len);
		IntStream.range(0, len).forEach(i -> setValAt(rt, i, arr[i]));
		return rt;
	}

	/**
	 * 比较一个原始类型数组与另一个对象是否相等。
	 * 可以比较一个原始类型数组和其外包类型数组，长度相等且每个元素都相等则认为是相等的。
	 *
	 * @param arr 数组1
	 * @param other 数组2
	 * @return 若相等则返回true。
	 */
	public boolean arrEquals(final A arr, final Object other) {
		if (Objects.equals(arr, other)) {
			return true;
		}
		if (arr == null || other == null) {
			return false;
		}
		final var c = other.getClass();
		if (!c.isArray()) {
			return false;
		}
		if (!isSuitFor(c.getComponentType())) {
			return false;
		}
		final var len = arrLen(arr);
		if (Array.getLength(other) != len) {
			return false;
		}
		for (var i = 0; i < len; ++i) {
			final var arrayElement = valAt(arr, i);
			final var obj = Array.get(other, i);
			if (arrayElement == null || obj == null) {
				return false;
			}
			if (!arrayElement.equals(obj)) {
				return false;
			}
		}
		return true;
	}

	//==============================================

	@SuppressWarnings("unchecked")
	protected A newPrimitiveArr(final int size) {
		return (A) Array.newInstance(primitiveClass(), size);
	}

	/**
	 * 返回一个原始类型数组的长度
	 *
	 * @param arr 数组
	 * @return 长度
	 */
	protected int arrLen0(final A arr) {
		return Array.getLength(arr);
	}

	//==============================================
	@Serial
	private static final long serialVersionUID = -4266565949179920712L;
}
