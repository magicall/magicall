/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.runtime;

import com.google.common.collect.Maps;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import java.util.Map;

class SourceCodeFileManager extends ForwardingJavaFileManager<JavaFileManager> {

	private final Map<String, VirtualJavaClassFile> nameClassFileMap = Maps.newHashMap();
	private final SourceCodeClassLoader sourceCodeClassLoader = new SourceCodeClassLoader();

	SourceCodeFileManager(final JavaFileManager raw) {
		super(raw);
	}

	@Override
	public ClassLoader getClassLoader(final Location location) {
		return sourceCodeClassLoader;
	}

	@Override
	public JavaFileObject getJavaFileForOutput(final Location location, final String className, final Kind kind,
																						 final FileObject sibling) {
		final var classFile = new VirtualJavaClassFile(className, kind);
		nameClassFileMap.put(className, classFile);
		return classFile;
	}

	private class SourceCodeClassLoader extends ClassLoader {
		@Override
		protected Class<?> findClass(final String name) throws ClassNotFoundException {
			final var classFile = nameClassFileMap.get(name);
			if (classFile == null) {
				throw new ClassNotFoundException();
			}
			final var b = classFile.toBytes();
			return defineClass(name, b, 0, b.length);
		}
	}
}
