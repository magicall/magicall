/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ArrKit;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

public interface MethodKit {

	Predicate<Method> IS_EQUALS = MethodKit::isTheEquals;
	Predicate<Method> IS_HASH_CODE = MethodKit::isHashCode;
	Predicate<Method> IS_TO_STRING = MethodKit::isToString;
	Predicate<Method> IS_GET_CLASS = MethodKit::isGetClass;
	Predicate<Method> IS_DEFAULT = Method::isDefault;

	Predicate<Method> IS_DEFINED_IN_OBJECT_CLASS = (Predicate) ReflectKit.IS_DEFINED_IN_OBJECT_CLASS;
	Predicate<Method> IS_NOT_DEFINED_IN_OBJECT_CLASS = (Predicate) ReflectKit.IS_NOT_DEFINED_IN_OBJECT_CLASS;
	Predicate<Method> ALL = (Predicate) ReflectKit.ALL;
	Predicate<Method> NONE = (Predicate) ReflectKit.NONE;
	Predicate<Method> IS_PUBLIC = (Predicate) ReflectKit.IS_PUBLIC;
	Predicate<Method> IS_PRIVATE = (Predicate) ReflectKit.IS_PRIVATE;
	Predicate<Method> IS_PROTECTED = (Predicate) ReflectKit.IS_PROTECTED;
	Predicate<Method> IS_DEFAULT_ACCESS = (Predicate) ReflectKit.IS_DEFAULT_ACCESS;
	Predicate<Method> IS_STATIC = (Predicate) ReflectKit.IS_STATIC;

	Class<?>[] JUST_ONE_OBJ_CLASS = {Object.class};

	static String toSignature(final Method method) {
		final var parameterTypes = method.getParameterTypes();
		return StrKit.format("{0}.{1}({2})", method.getDeclaringClass().getName(), method.getName(),
				parameterTypes.length == 0 ? "" : Arrays.toString(parameterTypes));
	}

	/**
	 * 执行方法。不会抛出受检异常。如果执行时抛出异常，则返回null。对执行结果关心度不高的场景很有效。
	 *
	 * @param obj 对象
	 * @param method 方法
	 * @param args 参数
	 * @return 返回值
	 */
	static Object invoke(final Object obj, final Method method, final Object... args) {
		if (!method.getDeclaringClass().isInstance(obj)) {
			throw new WrongArgException("caller", obj);
		}
		try {
			if (!method.isAccessible()) {
				method.setAccessible(true);
			}
			return method.invoke(obj, args);
		} catch (final IllegalArgumentException e) {
			throw new WrongArgException("args", args, e);
		} catch (final IllegalAccessException e) {
			throw new UnknownException(e);
		} catch (final InvocationTargetException e) {
			throw new WrongStatusException("method owner", obj, e);
		}
	}

	/**
	 * 从一些方法中按名字和参数获取某方法，忽略名字大小写，并且是参数包容的。该字段的名字为Class的简单名，首字母小写。
	 * （如：某类A有setString(Object o)方法，用A.class和String.class调用本方法，可以获得此Method对象)。
	 *
	 * @param methods 方法们
	 * @param methodName 方法名
	 * @param argsClasses 参数类型列表
	 * @return 找到的方法
	 * @deprecated 这个方法好奇怪，为什么要放进来一堆method来筛选？那不是明摆着让使用者调用Class.getMethod()或Class.getDeclaredMethods()吗？
	 */
	@Deprecated
	static Method findMethodIgnoreCaseAndArgsTypesAssigned(final Method[] methods, final String methodName,
																												 final Class<?>... argsClasses) {
		return findMethodIgnoreCaseAndArgsTypesAssigned(Arrays.asList(methods), methodName, argsClasses);
	}

	/**
	 * 从一些方法中按名字和参数获取某方法，忽略名字大小写，并且是参数包容的。该字段的名字为Class的简单名，首字母小写。
	 * （如：某类A有setString(Object o)方法，用A.class和String.class调用本方法，可以获得此Method对象)。
	 *
	 * @param methods 方法们
	 * @param methodName 方法名
	 * @param argsClasses 参数类型列表
	 * @return 找到的方法
	 * @deprecated 这个方法好奇怪，为什么要放进来一堆method来筛选？那不是明摆着让使用者调用Class.getMethod()或Class.getDeclaredMethods()吗？
	 */
	@Deprecated
	static Method findMethodIgnoreCaseAndArgsTypesAssigned(final Collection<Method> methods, final String methodName,
																												 final Class<?>... argsClasses) {
		Method method = null;
		for (final var m : methods) {
			final var name = m.getName();
			final var argTypes = m.getParameterTypes();
			if (argsClasses.length != argTypes.length) {
				continue;
			}
			if (name.equalsIgnoreCase(methodName)) {
				if (Arrays.equals(argTypes, argsClasses)) {
					return m;
				}
				var i = 0;
				for (final var argType : argTypes) {
					if (argType.isAssignableFrom(argsClasses[i])) {
						++i;
					} else {
						break;
					}
				}
				if (i == argsClasses.length) {
					method = m;
				}
			}
		}
		return method;
	}

	static boolean isStatic(final Method method) {
		return ReflectKit.isStatic(method);
	}

	/**
	 * 判断一个方法是否有返回值。
	 *
	 * @param method 方法
	 * @return 是否有返回值
	 */
	static boolean hasReturn(final Method method) {
		return !hasNoReturn(method);
	}

	/**
	 * 判断一个方法是否无返回值
	 *
	 * @param method 方法
	 * @return 是否无返回值
	 */
	static boolean hasNoReturn(final Method method) {
		return method.getReturnType() == void.class;
	}

	/**
	 * 判断指定的方法是否equals方法。
	 *
	 * @param method 方法
	 * @return 是否 {@link Object#equals(Object)}
	 */
	static boolean isTheEquals(final Method method) {
		return nameMatch(method, "equals")//
				&& Arrays.equals(JUST_ONE_OBJ_CLASS, method.getParameterTypes());
	}

	/**
	 * 判断指定的方法是否hashCode方法。
	 *
	 * @param method 方法
	 * @return 是否 {@link Object#hashCode()}
	 */
	static boolean isHashCode(final Method method) {
		return nameMatchAndNoArg(method, "hashCode");
	}

	/**
	 * 判断指定的方法是否toString方法。
	 *
	 * @param method 方法
	 * @return 是否 {@link Object#toString()}
	 */
	static boolean isToString(final Method method) {
		return nameMatchAndNoArg(method, "toString");
	}

	/**
	 * 判断指定的方法是否getClass方法。
	 *
	 * @param method 方法
	 * @return 是否 {@link Object#getClass()}
	 */
	static boolean isGetClass(final Method method) {
		return nameMatchAndNoArg(method, "getClass");
	}

	/**
	 * 判断指定的方法是否有参数。
	 *
	 * @param method 方法
	 * @return 是否有参数
	 */
	static boolean hasArgs(final Method method) {
		return !hasNoArg(method);
	}

	/**
	 * 判断指定的方法是否无参数。
	 *
	 * @param method 方法
	 * @return 是否无参数
	 */
	static boolean hasNoArg(final Method method) {
		return ArrKit.isEmpty(method.getParameterTypes());
	}

	private static boolean nameMatchAndNoArg(final Method method, final String name) {
		return nameMatch(method, name) && hasNoArg(method);
	}

	private static boolean nameMatch(final Method method, final String name) {
		return name.equals(method.getName());
	}
}
