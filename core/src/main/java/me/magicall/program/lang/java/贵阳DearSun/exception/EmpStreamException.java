/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import me.magicall.program.lang.java.贵阳DearSun.coll.StreamWrapper;

import java.io.Serial;
import java.io.Serializable;
import java.util.stream.Stream;

/**
 * @author Liang Wenjian.
 */
public class EmpStreamException extends NoElementException {

	@Serial
	private static final long serialVersionUID = -7729345692147297462L;

	private static final class SerializableEmptyStream implements StreamWrapper<Object>, Serializable {
		@Serial
		private static final long serialVersionUID = 6348129727564360114L;

		@Override
		public Stream<Object> unwrap() {
			return Stream.empty();
		}

		private Object readResolve() {
			return EMPTY;
		}
	}

	private static final SerializableEmptyStream EMPTY = new SerializableEmptyStream();

	public EmpStreamException(final String argName) {
		super(argName, EMPTY);
	}
}
