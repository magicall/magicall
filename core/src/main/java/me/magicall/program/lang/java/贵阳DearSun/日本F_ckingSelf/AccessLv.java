/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.Member;
import java.lang.reflect.Modifier;

public enum AccessLv {
	PRIVATE,
	DEFAULT,
	PROTECTED,
	PUBLIC;

	public boolean check(final Class<?> c) {
		return checkModifiers(c.getModifiers());
	}

	public boolean check(final Member member) {
		return checkModifiers(member.getModifiers());
	}

	public boolean checkModifiers(final int modifierFlags) {
		return of(modifierFlags) == this;
	}

	public boolean isMorePublicThan(final AccessLv other) {
		return ordinal() > other.ordinal();
	}

	//=====================================================

	public static AccessLv of(final int modifierFlag) {
		if (Modifier.isPublic(modifierFlag)) {
			return PUBLIC;
		}
		if (Modifier.isProtected(modifierFlag)) {
			return PROTECTED;
		}
		if (Modifier.isPrivate(modifierFlag)) {
			return PRIVATE;
		}
		return DEFAULT;
	}

	public static AccessLv of(final Class<?> c) {
		return c == null ? null : of(c.getModifiers());
	}

	public static AccessLv of(final Member member) {
		return member == null ? null : of(member.getModifiers());
	}
}
