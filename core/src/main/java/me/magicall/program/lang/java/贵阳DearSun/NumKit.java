/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;

/**
 * 数字型的工具类
 */
public abstract class NumKit<N extends Number & Comparable<N>, A> extends PrimitiveKit<N, A> {
	@Serial
	private static final long serialVersionUID = 5946172798711848876L;

	public static boolean deepEquals(final Number o1, final Number o2) {
		return Double.compare(o1.doubleValue(), o2.doubleValue()) == 0;
	}

	public abstract N fromLong(long value);
}
