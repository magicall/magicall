///*
// * Copyright (c) 2024 Liang Wenjian
// * magicall is licensed under Mulan PSL v2.
// * You can use this software according to the terms and conditions of the Mulan PSL v2.
// * You may obtain a copy of Mulan PSL v2 at:
// *          http://license.coscl.org.cn/MulanPSL2
// * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// * See the Mulan PSL v2 for more details.
// */
//
//package me.magicall.program.lang.java;
//
//import me.magicall.relation.Wrapper;
//
//import java.util.Enumeration;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Objects;
//import java.util.stream.Stream;
//
///**
// * 增强对象。提供一些有用便利的方法。
// *
// * @param <T> 增强的对象的类型。
// */
//public class EnhancedObj<T> implements Wrapper<T> {
//	private final T raw;
//	private final boolean isPrimitive;
//
//	private EnhancedObj(final T raw) {
//		this(raw, false);
//	}
//
//	private EnhancedObj(final T raw, final boolean isPrimitive) {
//		this.raw = raw;
//		this.isPrimitive = isPrimitive;
//	}
//
//	//=================================================
//
//	public boolean isNull() {
//		return raw == null;
//	}
//
//	public boolean isPrimitive() {
//		return isPrimitive;
//	}
//
//	public boolean is(final Class<?> clazz) {
//		return !isNull() && clazz.isInstance(raw);
//	}
//
////	public boolean is(final EnhancedClass<?> type) {
////		return type.exists() && is(type.toClass());
////	}
//
//	public boolean isNum() {
//		return raw instanceof Number;
//	}
//
//	public boolean isEnum() {
//		return raw instanceof Enum;
//	}
//
//	public boolean isMultiVal() {
//		return isIterable() || isArr() || isMap() || isIterator() || isStream() || isEnumeration();
//	}
//
//	public boolean isMap() {
//		return raw instanceof Map;
//	}
//
//	public boolean isStream() {
//		return raw instanceof Stream;
//	}
//
//	public boolean isIterator() {
//		return raw instanceof Iterator;
//	}
//
//	public boolean isEnumeration() {
//		return raw instanceof Enumeration;
//	}
//
//	public boolean isIterable() {
//		return raw instanceof Iterable;
//	}
//
//	public boolean isArr() {
//		return !isNull() && raw.getClass().isArray();
//	}
//
//	public boolean isPrimitiveArr() {
//		return isArr() && !(raw instanceof Object[]);
//	}
//
//	/**
//	 * 是否多维数组。
//	 *
//	 * @return 是否
//	 */
//	public boolean isMultiDimArr() {
//		return isArr() && raw.getClass().getComponentType().isArray();
//	}
//
//	//======================================
//
//	@SuppressWarnings("unchecked")
//	public EnhancedClass<T> type() {
//		return isNull() ? (EnhancedClass<T>) EnhancedClass.ofNull() : EnhancedClass.of(raw.getClass());
//	}
//
//	/**
//	 * 若是多值对象，返回元素的类型；否则返回对象的类型。
//	 *
//	 * @return 若是多值对象，返回元素的类型；否则返回对象的类型。
//	 */
//	public EnhancedClass<?> componentType() {
//		if (isNull()) {
//			return null;
//		}
//		final var rawClass = raw.getClass();
//		if (!isMultiVal()) {
//			return EnhancedClass.of(rawClass);
//		}
//		if (rawClass.isArray()) {
//			var componentClass = rawClass.getComponentType();
//			while (componentClass.isArray()) {
//				componentClass = componentClass.getComponentType();
//			}
//			return EnhancedClass.of(componentClass);
//		}
//		return EnhancedClass.of(Object.class);//多值而又不是数组，那么就是泛型类了，component类型被擦除之后只剩Object。
//	}
//
//	//======================================方法
//
//	/**
//	 * 获取所有方法，包括静态的
//	 *
//	 * @return
//	 */
//	public Stream<Method_> methods() {
//		return isNull() ? Stream.empty() : type().methods();
//	}
//
//	public Stream<Method_> methodsNamed(final String name) {
//		return methods().filter(e -> e.name().equals(name));
//	}
//
//	public Method_ methodOf(final String name, final Class<?> argClasses) {
//		return isNull() ? null : new Method_(ClassKit.methodOf(raw.getClass(), name, argClasses));
//	}
//
//	//====================================================
//
//	@Override
//	public T unwrap() {
//		return raw;
//	}
//
//	@Override
//	public String toString() {
//		return String.valueOf(raw);
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(raw);
//	}
//
//	@Override
//	public boolean equals(final Object o) {
//		return Objects.equals(raw, o);
//	}
//
//	//====================================================
//
//	public static <T> EnhancedObj<T> of(final T o) {
//		return o == null ? null : new EnhancedObj<>(o);
//	}
//
//	public static EnhancedObj<Boolean> of(final boolean o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Byte> of(final byte o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Short> of(final short o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Integer> of(final int o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Long> of(final long o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Float> of(final float o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Double> of(final double o) {
//		return new EnhancedObj<>(o, true);
//	}
//
//	public static EnhancedObj<Character> of(final char o) {
//		return new EnhancedObj<>(o, true);
//	}
//}
