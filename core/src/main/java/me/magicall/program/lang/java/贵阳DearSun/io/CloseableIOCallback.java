/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.io;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

/**
 * @param <C> 可关闭（{@link Closeable}）的对象的具体类型。
 * @deprecated 用法太复杂
 */
@Deprecated
@FunctionalInterface
public interface CloseableIOCallback<C extends Closeable> {
	void callback(C closeable) throws IOException;

	@FunctionalInterface
	interface InputStreamCallback extends CloseableIOCallback<InputStream> {

	}

	@FunctionalInterface
	interface OutputSteamCallback extends CloseableIOCallback<OutputStream> {

	}

	@FunctionalInterface
	interface ReaderCallback extends CloseableIOCallback<Reader> {

	}

	@FunctionalInterface
	interface WriterCallback extends CloseableIOCallback<Writer> {

	}
}
