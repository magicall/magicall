/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Unmodifiable;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedSet;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * 这是一个通用实现类，实现了Collection、List、Set、SortedSet等常见的集合接口
 *
 * @author MaGiCalL
 */
public final class EmptyColl extends TreeTemplate<Object> //
		implements Collection<Object>,//
		List<Object>,//
		Set<Object>, SortedSet<Object>,//
		//
		RandomAccess, Serializable, Unmodifiable, Sorted, Fixed {

	public static final EmptyColl INSTANCE = new EmptyColl();
	@Serial
	private static final long serialVersionUID = -7070927356384498504L;

	private EmptyColl() {
		super();
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public boolean isEmpty() {
		return true;
	}

	@Override
	public boolean contains(final Object o) {
		return false;
	}

	@Override
	public Iterator<Object> iterator() {
		return raw().iterator();
	}

	private static List<Object> raw() {
		return Collections.emptyList();
	}

	@Override
	public Object[] toArray() {
		return new Object[0];
	}

	@Override
	public <T> T[] toArray(final T[] a) {
		return Collections.<T>emptyList().toArray(a);
	}

	@Override
	public boolean add(final Object o) {
		return raw().add(o);
	}

	@Override
	public boolean remove(final Object o) {
		return raw().remove(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return raw().containsAll(c);
	}

	@Override
	public boolean addAll(final Collection<?> c) {
		return raw().addAll(c);
	}

	@Override
	public boolean addAll(final int index, final Collection<?> c) {
		return raw().addAll(index, c);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return raw().removeAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return raw().retainAll(c);
	}

	@Override
	public void replaceAll(final UnaryOperator<Object> operator) {
		raw().replaceAll(operator);
	}

	@Override
	public void sort(final Comparator<? super Object> c) {
	}

	@Override
	public void clear() {
	}

	@Override
	public boolean equals(final Object o) {
		if (o instanceof List) {
			return raw().equals(o);
		} else if (o instanceof Set) {
			return Collections.emptySet().equals(o);
		} else if (o instanceof Tree) {
			final Tree<?> tree = (Tree<?>) o;
			return tree.isEmpty();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return raw().hashCode();
	}

	@Override
	public Object get(final int index) {
		return raw().get(index);
	}

	@Override
	public Object set(final int index, final Object element) {
		return raw().set(index, element);
	}

	@Override
	public void add(final int index, final Object element) {
		raw().add(index, element);
	}

	@Override
	public Object remove(final int index) {
		return raw().remove(index);
	}

	@Override
	public int indexOf(final Object o) {
		return raw().indexOf(o);
	}

	@Override
	public int lastIndexOf(final Object o) {
		return raw().lastIndexOf(o);
	}

	@Override
	public ListIterator<Object> listIterator() {
		return raw().listIterator();
	}

	@Override
	public ListIterator<Object> listIterator(final int index) {
		return raw().listIterator(index);
	}

	@Override
	public List<Object> subList(final int fromIndex, final int toIndex) {
		return raw().subList(fromIndex, toIndex);
	}

	@Override
	public Spliterator<Object> spliterator() {
		return raw().spliterator();
	}

	@Override
	public boolean removeIf(final Predicate<? super Object> filter) {
		return raw().removeIf(filter);
	}

	@Override
	public Stream<Object> stream() {
		return Stream.empty();
	}

	@Override
	public Stream<Object> parallelStream() {
		return raw().parallelStream();
	}

	@Override
	public void forEach(final Consumer<? super Object> action) {
		raw().forEach(action);
	}

	@Override
	public Comparator<? super Object> comparator() {
		return Collections.emptySortedSet().comparator();
	}

	@Override
	public SortedSet<Object> subSet(final Object fromElement, final Object toElement) {
		return Collections.emptySortedSet().subSet(fromElement, toElement);
	}

	@Override
	public SortedSet<Object> headSet(final Object toElement) {
		return Collections.emptySortedSet().headSet(toElement);
	}

	@Override
	public SortedSet<Object> tailSet(final Object fromElement) {
		return Collections.emptySortedSet().tailSet(fromElement);
	}

	@Override
	public Object first() {
		return Collections.emptySortedSet().first();
	}

	@Override
	public Object last() {
		return Collections.emptySortedSet().last();
	}

	@Override
	public TreeNode<Object> getRoot() {
		return null;
	}

	@Override
	public int countLayers() {
		return 0;
	}

	@Override
	public Collection<TreeNode<Object>> getLeafNodes() {
		return (Collection) this;
	}

	@Override
	public int countLeaves() {
		return 0;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
