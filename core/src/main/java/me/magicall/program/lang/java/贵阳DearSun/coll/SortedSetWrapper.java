/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 有序散列表包装器适配器。包装了另一个有序散列表，用代理的方式实现了有序散列表接口所有方法，子类根据具体需求实现所需的方法即可。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface SortedSetWrapper<E> extends SortedSet<E>, Wrapper<SortedSet<E>> {

	/**
	 * 返回原始的有序散列表。
	 * 注意：写方法将直接影响返回的有序散列表。所以实现类若想返回原始有序散列表的副本，则必须覆写那些写方法。
	 *
	 * @return 原始的有序散列表
	 */
	@Override
	SortedSet<E> unwrap();

	@Override
	default int size() {
		return unwrap().size();
	}

	@Override
	default boolean isEmpty() {
		return unwrap().isEmpty();
	}

	@Override
	default boolean contains(final Object o) {
		return unwrap().contains(o);
	}

	@Override
	default Iterator<E> iterator() {
		return unwrap().iterator();
	}

	@Override
	default Object[] toArray() {
		return unwrap().toArray();
	}

	@Override
	default <T1> T1[] toArray(final T1[] a) {
		return unwrap().toArray(a);
	}

	@Override
	default boolean add(final E e) {
		return unwrap().add(e);
	}

	@Override
	default boolean remove(final Object o) {
		return unwrap().remove(o);
	}

	@Override
	default boolean containsAll(final Collection<?> c) {
		return unwrap().containsAll(c);
	}

	@Override
	default boolean addAll(final Collection<? extends E> c) {
		return unwrap().addAll(c);
	}

	@Override
	default boolean removeAll(final Collection<?> c) {
		return unwrap().removeAll(c);
	}

	@Override
	default boolean removeIf(final Predicate<? super E> filter) {
		return unwrap().removeIf(filter);
	}

	@Override
	default boolean retainAll(final Collection<?> c) {
		return unwrap().retainAll(c);
	}

	@Override
	default void clear() {
		unwrap().clear();
	}

	@Override
	default Stream<E> stream() {
		return unwrap().stream();
	}

	@Override
	default Stream<E> parallelStream() {
		return unwrap().parallelStream();
	}

	@Override
	default void forEach(final Consumer<? super E> action) {
		unwrap().forEach(action);
	}

	@Override
	default <T> T[] toArray(final IntFunction<T[]> generator) {
		return unwrap().toArray(generator);
	}

	@Override
	default Spliterator<E> spliterator() {
		return unwrap().spliterator();
	}

	@Override
	default Comparator<? super E> comparator() {
		return unwrap().comparator();
	}

	@Override
	default SortedSet<E> subSet(final E fromElement, final E toElement) {
		return unwrap().subSet(fromElement, toElement);
	}

	@Override
	default SortedSet<E> headSet(final E toElement) {
		return unwrap().headSet(toElement);
	}

	@Override
	default SortedSet<E> tailSet(final E fromElement) {
		return unwrap().tailSet(fromElement);
	}

	@Override
	default E first() {
		return unwrap().first();
	}

	@Override
	default E last() {
		return unwrap().last();
	}
}
