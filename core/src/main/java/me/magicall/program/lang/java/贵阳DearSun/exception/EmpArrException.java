/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import me.magicall.program.lang.java.JavaConst;
import me.magicall.program.lang.java.贵阳DearSun.BoolKit;
import me.magicall.program.lang.java.贵阳DearSun.ByteKit;
import me.magicall.program.lang.java.贵阳DearSun.CharKit;
import me.magicall.program.lang.java.贵阳DearSun.DoubleKit;
import me.magicall.program.lang.java.贵阳DearSun.FloatKit;
import me.magicall.program.lang.java.贵阳DearSun.IntKit;
import me.magicall.program.lang.java.贵阳DearSun.LongKit;
import me.magicall.program.lang.java.贵阳DearSun.ShortKit;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class EmpArrException extends NoElementException {

	@Serial
	private static final long serialVersionUID = 8723715851712533225L;

	private EmpArrException(final String argName, final Object actualVal) {
		super(argName, actualVal);
	}

	public static EmpArrException objs(final String argName) {
		return new EmpArrException(argName, JavaConst.EMP_OBJ_ARR);
	}

	public static EmpArrException bytes(final String argName) {
		return new EmpArrException(argName, ByteKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException shorts(final String argName) {
		return new EmpArrException(argName, ShortKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException ints(final String argName) {
		return new EmpArrException(argName, IntKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException longs(final String argName) {
		return new EmpArrException(argName, LongKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException floats(final String argName) {
		return new EmpArrException(argName, FloatKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException doubles(final String argName) {
		return new EmpArrException(argName, DoubleKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException bools(final String argName) {
		return new EmpArrException(argName, BoolKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException chars(final String argName) {
		return new EmpArrException(argName, CharKit.INSTANCE.emptyPrimitiveArr());
	}

	public static EmpArrException strs(final String argName) {
		return new EmpArrException(argName, StrKit.INSTANCE.emptyArr());
	}
}
