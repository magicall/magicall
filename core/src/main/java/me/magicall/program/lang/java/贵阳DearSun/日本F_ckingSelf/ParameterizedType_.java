/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 表示一个泛型类，即比Class多了具体泛型的部分。
 * 比如{@code class A extends B<String>} 中的{@code B<String>}（通过 {@code A.class.getGenericSuperclass()} 得到）。
 * 注意：是整个“带有泛型的具体类型”（{@code B<String>}），而非“带有泛型的具体类型中的泛型部分”（{@code <String>}）。
 * 所以本身可能包含多个“下级”泛型（{@code Type[] getActualTypeArguments()}），比如{@code Map<Integer,String>}就有俩。
 * 且“下级”泛型本身也可能不是确定的，比如是{@code <T>}、{@code <?>}。
 */
public class ParameterizedType_ extends Type_<Object> {
	private final Type_<?> owner;
	private final List<Type_<?>> actualTypes;
	private final Type_<?> rawType;
	private final Type_<?> outerType;

	ParameterizedType_(final ParameterizedType raw, final Type_<?> owner) {
		super(raw);
		this.owner = owner;

		//当raw为非静态内部类时，ownerType才有值，是其外部类（即owner）。
		//若外部类不是泛型类，则ownerType是个Class，即rawType.declaringClass。
		// 否则，则返回的也是个ParameterizedType对象，它的rawType==raw.rawType.declaringClass
		final var ownerType = raw.getOwnerType();
		outerType = ownerType == null ? null : Type_.of(ownerType);

		//rawType必然是Class。
		//Object.class没有泛型，所以rawType不可能是Object.class。
		rawType = Type_.of(raw.getRawType());

		actualTypes = Stream.of(raw.getActualTypeArguments())//
				.map(Type_::of).collect(Collectors.toList());
	}

	@Override
	public JavaRuntimeElement<?> owner() {
		return owner;
	}

	@Override
	public Type_<?> superClass() {
		//todo：需要处理superClass中的泛型
		return rawType.superClass();
	}

	@Override
	public Stream<Type_<?>> superClasses() {
		//todo：需要处理superClass中的泛型
		return rawType.superClasses();
	}

	@Override
	public Stream<Type_<?>> interfaces() {
		//todo：需要处理interfaces中的泛型
		return rawType.interfaces();
	}

	@Override
	public Stream<Constructor_<Object>> constructors() {
//todo		return rawType.constructors().map(e -> e.withGeneric(actualTypes));
		return Stream.empty();
	}

	@Override
	public Stream<Method_> myOwnMethods() {
		//todo:可能需要处理泛型
		return rawType.myOwnMethods();
	}

	@Override
	public Stream<Field_> fields() {
		//todo:可能需要处理泛型
		return rawType.fields();
	}

	@Override
	public Stream<Field_> myOwnFields() {
		//todo:可能需要处理泛型
		return rawType.myOwnFields();
	}

	@Override
	public boolean canNewInstance() {
		return rawType.canNewInstance();
	}

	@Override
	public boolean canInstanceBe(final Object obj) {
		//todo:可能要检查泛型
		return rawType.canInstanceBe(obj);
	}

	@Override
	public boolean canInstanceBeType(final Class<?> otherType) {
		//todo:可能要检查泛型
		return rawType.canInstanceBeType(otherType);
	}
}
