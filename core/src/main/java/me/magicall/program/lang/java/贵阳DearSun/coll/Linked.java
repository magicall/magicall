/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.Child;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 连成一条链的。
 * 注意与{@link Child}的区别。{@link Child}表示一种带有层次结构的关系，本类不表示层次结构。比如链表中的节点相互间是Linked关系，和链表则是${@link Child}，
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Linked<T extends Linked<T>> {

	/**
	 * 获取前导节点。
	 *
	 * @return 前导节点。
	 */
	T preNode();

	default boolean isRoot() {
		return preNode() == null;
	}

	@SuppressWarnings("unchecked")
	default Stream<T> streamToRoot() {
		return Stream.iterate((T) this, t -> !t.isRoot(), Linked::preNode);
	}

	@SuppressWarnings("unchecked")
	default T findInPreNodes(final Predicate<T> predicate) {
		for (var t = (T) this; true; ) {
			if (predicate.test(t)) {
				return t;
			}
			if (t == null) {
				break;
			}
			t = t.preNode();
		}
		return null;
	}

	default T findRoot() {
		return findInPreNodes(Linked::isRoot);
	}

	default T findInPreNodes(final Class<? extends T> clazz) {
		return findInPreNodes(clazz::isInstance);
	}
}
