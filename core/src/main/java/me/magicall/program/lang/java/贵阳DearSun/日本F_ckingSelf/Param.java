/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * 形参。
 * 形参名在编译时默认是丢弃的，除非使用javac -parameters 编译器选项来保留它们。
 */
public class Param implements JavaRuntimeElement<Parameter>, HasModifierFlags<Parameter> {
	private final Parameter raw;
	private final Type_<Parameter> type;
	private final JavaRuntimeElement<?> owner;

	public Param(final Parameter raw) {
		this.raw = raw;
		type = Type_.of(raw.getParameterizedType());
		final var executable = raw.getDeclaringExecutable();
		owner = executable instanceof final Method m ? new Method_(m)//
																								 : new Constructor_<>((Constructor<Object>) executable);
	}

	@Override
	public String name() {
		return raw.getName();
	}

	@Override
	public Type_<Parameter> elementType() {
		return type;
	}

	@Override
	public JavaRuntimeElement<?> owner() {
		return owner;
	}

	/**
	 * 是否保留了参数的真实名称（即是否使用了javac -parameters 编译选项）。
	 *
	 * @return
	 */
	public boolean isNamePresent() {
		return raw.isNamePresent();
	}

	/**
	 * 是否是隐式的（比如在非静态内部类的构造函数中隐式传递的外部类的引用）。
	 *
	 * @return
	 */
	public boolean isImplicit() {
		return raw.isImplicit();
	}

	/**
	 * 是否是编译器引入的（如在某些情况下生成的桥接方法参数）。
	 *
	 * @return
	 */
	public boolean isSynthetic() {
		return raw.isSynthetic();
	}

	/**
	 * 是否是可变参数列表
	 *
	 * @return
	 */
	public boolean isVarArgs() {
		return raw.isVarArgs();
	}

	@Override
	public int modifierFlags() {
		return raw.getModifiers();
	}

	@Override
	public Parameter unwrap() {
		return raw;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof final Param o && unwrap().equals(o.unwrap());
	}

	@Override
	public int hashCode() {
		return unwrap().hashCode();
	}

	@Override
	public String toString() {
		return unwrap().toString();
	}
}
