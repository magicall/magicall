/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;

import java.lang.reflect.Executable;
import java.lang.reflect.TypeVariable;
import java.util.stream.Stream;

/**
 * 可执行的成员，包括方法、构造方法。
 */
abstract class Executable_<_Executable extends Executable> extends TypeMember<_Executable> {
	Executable_(final _Executable raw) {
		super(raw);
	}

	//------------------------------类型

	public Stream<TypeVariable<?>> contextTypeVariables() {
		return Stream.of(unwrap().getTypeParameters());
	}

	public boolean hasContextTypeVariables() {
		return contextTypeVariables().findAny().isEmpty();
	}

	//---------------------------------形参

	/**
	 * 是否有形参
	 *
	 * @return
	 */
	public boolean hasParams() {
		return unwrap().getParameterCount() > 0;
	}

	protected Class<?>[] paramClasses0() {
		return unwrap().getParameterTypes();
	}

	/**
	 * 形参类型列表
	 *
	 * @return
	 */
	public Stream<Cls<?>> paramTypes() {
		return Stream.of(paramClasses0()).map(Cls::new);
	}

	/**
	 * 是否接受可变长参数。
	 *
	 * @return
	 */
	public boolean hasVarParams() {
		return unwrap().isVarArgs();
	}

	/**
	 * 是否可使用指定的实参列表。
	 *
	 * @return
	 */
	public boolean canUseArgs(final Object... args) {
		//todo:若有泛型，似乎需要检查泛型上下界。
		return ClassKit.checkAllAssignable(paramClasses0(), args);
	}

	//--------------------------------异常

	/**
	 * 可能抛出的异常或错误。
	 *
	 * @return
	 */
	public Stream<Cls<?>> mayThrowTypes() {
		return Stream.of(unwrap().getExceptionTypes()).map(Cls::new);
	}

	/**
	 * 是否可能抛异常或错误
	 *
	 * @return
	 */
	public boolean mayThrow() {
		return unwrap().getExceptionTypes().length > 0;
	}
}
