/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.js;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;
import java.io.Reader;

public class HutoolJs implements Js {
	private final ScriptEngine jsEngine;

	public HutoolJs() {
		jsEngine = JsKit.jsEngine();
	}

	public Object eval(final String script, final ScriptContext context) throws ScriptException {
		return jsEngine.eval(script, context);
	}

	public Object eval(final Reader reader, final ScriptContext context) throws ScriptException {
		return jsEngine.eval(reader, context);
	}

	public Object eval(final String script) throws ScriptException {
		return jsEngine.eval(script);
	}

	public Object eval(final Reader reader) throws ScriptException {
		return jsEngine.eval(reader);
	}

	public Object eval(final String script, final Bindings n) throws ScriptException {
		return jsEngine.eval(script, n);
	}

	public Object eval(final Reader reader, final Bindings n) throws ScriptException {
		return jsEngine.eval(reader, n);
	}

	public void put(final String key, final Object value) {
		jsEngine.put(key, value);
	}

	public Object get(final String key) {
		return jsEngine.get(key);
	}

	public Bindings getBindings(final int scope) {
		return jsEngine.getBindings(scope);
	}

	public void setBindings(final Bindings bindings, final int scope) {
		jsEngine.setBindings(bindings, scope);
	}

	public Bindings createBindings() {
		return jsEngine.createBindings();
	}

	public ScriptContext getContext() {
		return jsEngine.getContext();
	}

	public void setContext(final ScriptContext context) {
		jsEngine.setContext(context);
	}

	public ScriptEngineFactory getFactory() {
		return jsEngine.getFactory();
	}
}
