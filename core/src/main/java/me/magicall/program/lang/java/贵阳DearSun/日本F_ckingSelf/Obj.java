/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.util.stream.Stream;

/**
 * 增强的对象。
 *
 * @param <_Type> 本对象的类型。
 */
public interface Obj<_Type> extends Wrapper<_Type> {

	/**
	 * 返回被增强的那个java对象。
	 *
	 * @return
	 */
	@Override
	_Type unwrap();

	/**
	 * 所代表的东西是否存在。相当于!=null
	 * 子类可以是通过某种代码片段创建的，该片段所使用的元素未必存在。
	 *
	 * @return
	 */
	default boolean isNotNull() {
		return unwrap() != null;
	}

	//================================= is：与类型有关的

	/**
	 * 类型：本对象的数据类型。相当于getClass()。
	 * 即使本对象为空，也是有类型的，即声明它时的类型。
	 * 特殊：
	 * - 方法：返回值类型
	 * - 构造器：由于返回值类型就是本类的Class本身，所以返回值类型就是本类的类类型
	 *
	 * @return
	 */
	default Type_<_Type> type() {
		if (!isNotNull()) {//todo 即使本对象为空，也是有类型的，即声明它时的类型。
			return null;
		}
		return (Type_<_Type>) Type_.of(unwrap().getClass());
	}

	/**
	 * 若为对象，判断是否指定类型的实例；若为类型，判断是否指定类型的子类型。
	 *
	 * @return
	 */
	default boolean isType(final Type_<?> type) {
		return isNotNull() && type.canInstanceBe(unwrap());
	}

	//==================================== has：与属性、方法有关的
	//--------------------------------方法

	default <T> T invoke(final Context context) {
		//todo
		return null;
	}

	default Stream<Field_> props() {
		//todo
		return null;
	}

	default Obj<?> get(final String propName) {
		//todo
		return null;
	}

	default Obj<_Type> set(final String propName, final Obj<?> obj) {
		//todo
		return this;
	}

	//=====================================================================

	static <T> Obj<T> of(final T raw) {
		//todo
		return null;
	}

	interface Context {

	}
}
