/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import me.magicall.program.lang.java.JavaConst;
import me.magicall.program.lang.java.贵阳DearSun.coll.ElementTransformer;

import java.io.Serial;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StrKit extends Kit<String> implements Serializable {

	public static final String[] EMPTY_STR_ARR = {};
	public static final String NULL = String.valueOf((Object) null);

	private static final String NEW_LINE = System.lineSeparator();
	private static final String[] SHORT_NAMES = {"str", "varchar"};

	public static final StrKit INSTANCE = new StrKit();

	//=========================================

	@Override
	public Stream<String> supportedTypeNames() {
		return Stream.concat(super.supportedTypeNames(), Stream.of(SHORT_NAMES));
	}

	@Override
	public String parse(final String source) {
		return source;
	}

	@Override
	public <T1, T2> String emptyVal() {
		return "";
	}

	@Override
	public boolean isEmpty(final String source) {
		return source == null || source.isBlank();
	}

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public boolean equals(final String o1, final Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		return o2 instanceof final CharSequence o && o1.equalsIgnoreCase(o.toString());
	}

	//-------------------------------------

	public static String newLine() {
		return INSTANCE.nullOrDefaultVal(NEW_LINE, "\r\n");
	}

	public static char lastChar(final String s) {
		return s.charAt(s.length() - 1);
	}

	public static String toUnicode(final String source) {
		final var s = INSTANCE.nullOrEmptyVal(source);
		final var sb = new StringBuilder(Kits.STR.emptyVal());
		final var size = s.length();
		IntStream.range(0, size).map(s::codePointAt).forEach(c -> {
			if (c < 16) {
				sb.append("\\u000").append(Integer.toHexString(c));
			} else if (c < 256) {
				sb.append("\\u00").append(Integer.toHexString(c));
			} else if (c < 4096) {
				sb.append("\\u0").append(Integer.toHexString(c));
			} else {
				sb.append("\\u").append(Integer.toHexString(c));
			}
		});
		return sb.toString();
	}

	/**
	 * 半角转全角
	 *
	 * @param input 半角字符串.
	 * @return 全角字符串.
	 */
	public static String toFullWidth(final String input) {
		final var c = input.toCharArray();
		IntStream.range(0, c.length).forEach(i -> c[i] = CharKit.toFullWidth(c[i]));
		return new String(c);
	}

	/**
	 * 全角转半角
	 *
	 * @param input 全角字符串.
	 * @return 半角字符串
	 */
	public static String toHalfWidth(final String input) {
		final var c = input.toCharArray();
		IntStream.range(0, c.length).forEach(i -> c[i] = CharKit.toHalfWidth(c[i]));
		return new String(c);
	}

	public static boolean startsWithIgnoreCase(final String source, final String target,
																						 final boolean escapeStartingBlank) {
		final String s1, s2;
		if (escapeStartingBlank) {
			s1 = source.stripLeading();
			s2 = target.stripLeading();
		} else {
			s1 = source;
			s2 = target;
		}
		return s1.toLowerCase().startsWith(s2.toLowerCase());
	}

	public static boolean endsWithIgnoreCase(final String source, final String target, final boolean escapeEndingBlank) {
		final String s1, s2;
		if (escapeEndingBlank) {
			s1 = source.stripTrailing();
			s2 = target.stripTrailing();
		} else {
			s1 = source;
			s2 = target;
		}
		return s1.toLowerCase().endsWith(s2.toLowerCase());
	}

	public static <E> StringBuilder join(final StringBuilder sb, final Iterable<E> iterable, final String separator,
																			 final ElementTransformer<? super E, String> tf) {
		final var spliterator = iterable.spliterator();
		spliterator.tryAdvance(obj -> sb.append(tf.transform(0, obj)));
		final AtomicInteger i = new AtomicInteger(1);
		spliterator.forEachRemaining(e -> sb.append(separator).append(tf.transform(i.getAndIncrement(), e)));
		return sb;
	}

	public static String middle(final String source, final String before, final boolean includeBefore, final String after,
															final boolean includeAfter) {
		final var beforeIndex = source.indexOf(before);
		final int startIndex;
		if (beforeIndex == JavaConst.NOT_FOUND_INDEX) {
			startIndex = 0;
		} else if (!includeBefore) {//注意里需要个叹号!
			startIndex = beforeIndex + before.length();
		} else {
			startIndex = beforeIndex;
		}
		final var afterIndex = source.indexOf(after, beforeIndex + before.length());
		final int endIndex;
		if (afterIndex == JavaConst.NOT_FOUND_INDEX) {
			endIndex = source.length();
		} else if (includeAfter) {
			endIndex = afterIndex + after.length();
		} else {
			endIndex = afterIndex;
		}
		return source.substring(startIndex, endIndex);
	}

	public static String subStringBefore(final String source, final String seq, final boolean lastSeq) {
		final var index = lastSeq ? source.lastIndexOf(seq) : source.indexOf(seq);
		if (index == JavaConst.NOT_FOUND_INDEX) {
			return source;
		}
		return source.substring(0, index);
	}

	public static String subStringAfter(final String source, final String seq) {
		return source.substring(indexAfter(source, seq));
	}

	public static String subStringAfterLastSeq(final String source, final String seq) {
		final var lastIndexOf = source.lastIndexOf(seq);
		return lastIndexOf == JavaConst.NOT_FOUND_INDEX ? source : source.substring(lastIndexOf + seq.length());
	}

	public static int indexAfter(final String source, final String seq) {
		return indexAfter(source, seq, 0);
	}

	public static int indexAfter(final String source, final String seq, final int fromIndex) {
		final var index = source.indexOf(seq, fromIndex);
		return index == JavaConst.NOT_FOUND_INDEX ? fromIndex : index + seq.length();
	}

	public static boolean containsIgnoreCase(final String longOne, final String shortOne) {
		return indexOfIgnoreCase(longOne, shortOne) != JavaConst.NOT_FOUND_INDEX;
	}

	public static int indexOfIgnoreCase(final String longOne, final String shortOne) {
		return longOne.toLowerCase().indexOf(shortOne.toLowerCase());
	}

	/**
	 * 占位符从{0}开始。
	 *
	 * @return
	 */
	public static String format(final String source, final Object... args) {
		return MessageFormat.format(source, args);
	}

	//===============================================

	private Object readResolve() {
		return INSTANCE;
	}

	@Serial
	private static final long serialVersionUID = -6560796645889471562L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(String.class);
}
