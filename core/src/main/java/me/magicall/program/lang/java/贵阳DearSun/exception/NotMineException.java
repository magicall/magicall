/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

public class NotMineException extends WrongArgException {
	@Serial
	private static final long serialVersionUID = 2991238701312929241L;

	public NotMineException(final String argName, final Object actualOwner) {
		super(buildArgName(argName), actualOwner);
	}

	public NotMineException(final String argName, final Object actualOwner, final Throwable cause) {
		super(buildArgName(argName), actualOwner, cause);
	}

	public NotMineException(final String argName, final Object actualOwner, final Object expectOwner) {
		super(buildArgName(argName), actualOwner, expectOwner);
	}

	public NotMineException(final String argName, final Object actualOwner, final Object expectOwner,
													final Throwable cause) {
		super(buildArgName(argName), actualOwner, expectOwner, cause);
	}

	private static String buildArgName(final String argName) {
		return argName + "`owner";
	}
}
