/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.coll.Tree.TreeNode;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Objects;
import java.util.stream.Collectors;

public abstract class TreeNodeTemplate<E> implements TreeNode<E> {

	@Override
	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof TreeNode)) {
			return false;
		}
		final TreeNode<?> other = (TreeNode) o;
		if (!Objects.equals(getElement(), other.getElement())) {
			return false;
		}
		return children().collect(Collectors.toList()).equals(other.children().collect(Collectors.toList()));
	}

	@Override
	public int hashCode() {
		final E element = getElement();
		return (element == null ? 0 : element.hashCode()) + children().collect(Collectors.toList()).hashCode() * 31;
	}

	@Override
	public String toString() {
		return StrKit.format("[{0}]({1})", getElement(), children().count());
	}
}
