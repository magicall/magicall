/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class NegativeIntException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = -5760869579054428448L;
	private static final Range<Integer> AVAILABLE_RANGE = Range.closed(0, Integer.MAX_VALUE);

	public NegativeIntException(final String argName, final int actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public NegativeIntException(final String argName, final int actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public NegativeIntException(final String argName, final int actualVal, final int maxAvailable) {
		super(argName, actualVal, Range.closed(0, maxAvailable));
	}

	public NegativeIntException(final String argName, final int actualVal, final int maxAvailable,
															final Throwable cause) {
		this(argName, actualVal, maxAvailable);
		initCause(cause);
	}
}
