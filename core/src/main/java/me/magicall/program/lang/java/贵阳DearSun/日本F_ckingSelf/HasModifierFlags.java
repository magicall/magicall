/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.lang.reflect.Modifier;

/**
 * 有修饰符的对象。
 */
public interface HasModifierFlags<ObjType> extends Accessible, JavaRuntimeElement<ObjType> {

	/**
	 * 获取修饰符标记位。
	 * 最好别用。谁特么要记这数的标记位。
	 *
	 * @return
	 */
	int modifierFlags();

	/**
	 * 确保本成员修饰符为指定值。
	 *
	 * @return
	 */
	default HasModifierFlags<ObjType> ensureModifier(final int newModifiers) {
		try {
			final var unwrap = unwrap();
			final var modifiersField = unwrap.getClass().getDeclaredField("modifiers");
			final var enhancedModifiersField = new Field_(modifiersField);
			enhancedModifiersField.ensureAccessible();
			enhancedModifiersField.tryToSetVal(unwrap, newModifiers);
		} catch (final NoSuchFieldException e) {
			throw new UnknownException(e);
		}
		return this;
	}

	/**
	 * 增加指定的修饰符。
	 *
	 * @return
	 */
	default HasModifierFlags<ObjType> addModifiers(final int newModifiers) {
		return ensureModifier(modifierFlags() | newModifiers);
	}

	/**
	 * 移除指定的修饰符。
	 *
	 * @return
	 */
	default HasModifierFlags<ObjType> removeModifiers(final int newModifiers) {
		return ensureModifier(modifierFlags() & ~newModifiers);
	}

	@Override
	default AccessLv accessLv() {
		return AccessLv.of(modifierFlags());
	}

	//---------------------------------

	default boolean isFinal() {
		return Modifier.isFinal(modifierFlags());
	}

	/**
	 * 设置final修饰符。
	 *
	 * @return
	 */
	default void ensureFinal() {
		if (!isFinal()) {
			addModifiers(Modifier.FINAL);
		}
	}

	/**
	 * 移除final修饰符
	 *
	 * @return
	 */
	default void ensureNotFinal() {
		if (isFinal()) {
			removeModifiers(Modifier.FINAL);
		}
	}

	//-----------------------------------

	//理论上有modifierFlags就可以计算各种修饰符。但有些修饰符不适用于所有元素。所以算法放在这里，适用的子类自行放出来。
	default boolean isStatic() {
		return Modifier.isStatic(modifierFlags());
	}
}
