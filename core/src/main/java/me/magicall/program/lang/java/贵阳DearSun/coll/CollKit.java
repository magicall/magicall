/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import me.magicall.program.lang.java.JavaConst;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.ObjKit;

import java.io.Serial;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * 多值对象的工具类。
 */
public final class CollKit extends AbsCollKit<Collection<?>> implements Serializable {
	@Serial
	private static final long serialVersionUID = -8028661636852704700L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Collections.singletonList(Collection.class);
	private static final Random RANDOM = new SecureRandom();

	public static final CollKit INSTANCE = new CollKit();

	//=============================================

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Stream<String> supportedTypeNames() {
		return Stream.concat(super.supportedTypeNames(), Stream.of("coll"));
	}

	@Override
	public Collection<?> parse(final String source) {
		return null;//todo
	}

	@Override
	public <E, E1> Collection<E> emptyVal() {
		return (Collection) EmptyColl.INSTANCE;
	}

	/**
	 * 将一个来历不明的集合的泛型强转成另一种类型的集合泛型.
	 * 注意:这是"非安全的"，适用场合：当使用者很清楚地确定source（是一个Collection&lt;S&gt;）中的S是T的父类，用此方法帮助做强制类型转换，可少写很多恶心代码
	 * 注：跟父类实现一致，覆写一遍是为了返回类型能带上泛型T。
	 *
	 * @param <T> 集合的元素类型。
	 * @param source 源集合。
	 * @return 强转的集合。
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> Collection<T> cast(final Collection<?> source) {
		return (Collection<T>) source;
	}

	private Object readResolve() {
		return INSTANCE;
	}

	//================================================================转换

	public static <T> Stream<T> stream(final Iterator<T> iterator) {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
	}

	public static <T> Stream<T> stream(final Iterable<T> source) {
		return Streams.stream(source);
	}

	@SafeVarargs
	public static <T> Stream<T> stream(final T... source) {
		return Stream.of(source);
	}

	//===============================================================

	public static <E> E first(final Collection<E> source, final Predicate<? super E> predicate) {
		return source.stream().filter(predicate).findFirst().orElse(null);
	}

	public static <E> boolean deepContains(final Collection<E> source, final E target) {
		return source.stream().anyMatch(e -> ObjKit.deepEquals(e, target));
	}

	public static <E> int locateFirst(final Collection<E> source, final Predicate<? super E> predicate) {
		final var count = (int) source.stream().takeWhile(predicate.negate()).count();
		return count == source.size() ? JavaConst.NOT_FOUND_INDEX : count;
	}

	public static int locateFirst(final Object target, final Collection<?> source) {
		return locateFirst(source, Predicate.isEqual(target));
	}

	/**
	 * 从集合中随机取出一个元素.
	 *
	 * @param <T> 元素的类型
	 * @param source 源集合
	 * @return 随机取出来的一个元素.如果集合为空, 则返回null
	 */
	public static <T> T randomOne(final Collection<T> source) {
		if (source instanceof List<?>) {
			return Kits.LIST.random((List<T>) source);
		}
		if (INSTANCE.isEmpty(source)) {
			return null;
		}
		final var index = RANDOM.nextInt(source.size());
		final var it = source.iterator();
		IntStream.range(0, index).forEach(i -> it.next());
		return it.next();
	}

	/**
	 * 从源集合中随机选取size个元素组成一个新的集合。
	 *
	 * @param <T> 元素的类型。
	 * @param source 源集合。
	 * @param size 长度
	 * @return 新的集合。
	 */
	public static <T> Collection<T> randomSome(final Collection<T> source, final int size) {
		final List<T> list = Lists.newArrayList(source);
		if (source.size() <= size) {
			return list;
		}
		Collections.shuffle(list);
		return list.subList(0, size);
	}

	static <T> T dropFrom(final T toRemove, final Collection<T> coll) {
		for (final Iterator<T> iterator = coll.iterator(); iterator.hasNext(); ) {
			final T e = iterator.next();
			if (e.equals(toRemove)) {
				iterator.remove();
				return e;
			}
		}
		return null;
	}

	/**
	 * 两个集合分别移除交集。
	 *
	 * @param c1 集合1
	 * @param c2 集合2
	 * @return 是否移除了元素。
	 */
	public static boolean removeEachOther(final Collection<?> c1, final Collection<?> c2) {
		return removeEachOther(c1, c2, Objects::equals);
	}

	/**
	 * 两个集合分别移除交集。
	 *
	 * @param c1 集合1
	 * @param c2 集合2
	 * @param equaler 比较器
	 * @return 是否移除了元素。
	 */
	public static boolean removeEachOther(final Collection<?> c1, final Collection<?> c2,
																				final BiPredicate<Object, Object> equaler) {
		return c1.removeIf(e -> c2.removeIf(e2 -> equaler.test(e, e2)));
	}

	@SuppressWarnings("unchecked")
	public static <T> Stream<T> concat(final Stream<? extends T>... streams) {
		return (Stream<T>) Stream.of(streams).reduce(Stream::concat).orElse(Stream.empty());
	}

	public static <A, B> void biIterate(final Iterator<A> items1, final Iterator<B> items2,
																			final BiConsumer<A, B> consumer) {
		while (items1.hasNext()) {
			consumer.accept(items1.next(), items2.hasNext() ? items2.next() : null);
		}
		//若1遍历完，2还有剩余元素
		while (items2.hasNext()) {
			consumer.accept(null, items2.next());
		}
	}

	public static <A, B, C> Stream<C> biIterate(final Iterator<A> items1, final Iterator<B> items2,
																							final BiFunction<A, B, C> function) {
		final List<C> rt = Lists.newArrayList();
		biIterate(items1, items2, (a, b) -> {
			rt.add(function.apply(a, b));
		});
		return rt.stream();
	}

	public static <A, B> void biIterate(final Iterable<A> items1, final Iterable<B> items2,
																			final BiConsumer<A, B> consumer) {
		biIterate(items1.iterator(), items2.iterator(), consumer);
	}

	public static <A, B, C> Stream<C> biIterate(final Iterable<A> items1, final Iterable<B> items2,
																							final BiFunction<A, B, C> function) {
		return biIterate(items1.iterator(), items2.iterator(), function);
	}
}
