/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Objects;
import java.util.stream.Collectors;

public class EnumKit<E extends Enum<E>> {

	private EnumKit() {
		super();
	}

	public static <E extends Enum<E>> E valueOf(final Class<E> clazz, final String name) {
		final var es = clazz.getEnumConstants();
		return Arrays.stream(es).filter(e -> e.name().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	@SafeVarargs
	public static <E extends Enum<E>> EnumSet<E> toEnumSet(final E... es) {
		return EnumSet.copyOf(Arrays.asList(es));
	}

	public static <E extends Enum<E>> long enumSetToMasks(final Collection<E> enums) {
		if (enums.isEmpty()) {
			return 0;
		}
		final EnumSet<E> enumSet = enums instanceof EnumSet ? (EnumSet<E>) enums : EnumSet.copyOf(enums);
		final BitSet bitSet = new BitSet();
		enumSet.forEach(e -> bitSet.set(e.ordinal()));
		return bitSet.toLongArray()[0];
	}

	public static <E extends Enum<E>> EnumSet<E> masksToEnumSet(final long masks, final Class<E> enumClass) {
		final var enums = enumClass.getEnumConstants();
		return BitSet.valueOf(new long[]{masks}).stream()//
				.mapToObj(i -> i < enums.length ? enums[i] : null)//
				.filter(Objects::nonNull)//
				.collect(Collectors.toCollection(() -> EnumSet.noneOf(enumClass)));
	}
}
