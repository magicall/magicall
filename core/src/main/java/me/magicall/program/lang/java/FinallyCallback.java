/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java;

/**
 * 本类对象用来在finally中作为回调
 *
 * @author MaGiCalL
 * @deprecated 用法复杂。
 */
@FunctionalInterface
public interface FinallyCallback {

	FinallyCallback DO_NOTHING = () -> {
	};

	/**
	 * 将在finally块中调用的方法.
	 */
	void finallyExecute();
}
