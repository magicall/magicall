/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class PositiveFloatException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = -4999209369612743130L;
	private static final Range<Float> AVAILABLE_RANGE = Range.closed(Float.MIN_VALUE, 0.0F);

	public PositiveFloatException(final String argName, final float actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public PositiveFloatException(final String argName, final float actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public PositiveFloatException(final String argName, final float actualVal, final float minAvailable) {
		super(argName, actualVal, Range.closed(minAvailable, 0.0F));
	}

	public PositiveFloatException(final String argName, final float actualVal, final float minAvailable,
																final Throwable cause) {
		this(argName, actualVal, minAvailable);
		initCause(cause);
	}
}
