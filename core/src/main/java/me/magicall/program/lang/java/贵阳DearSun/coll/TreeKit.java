/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TreeKit {

	public static <E> Tree<E> emptyTree() {
		return (Tree) EmptyColl.INSTANCE;
	}

	public static String str(final Tree<?> tree) {
		if (tree.isEmpty()) {
			return "[empty tree]";
		}
		final var sb = new StringBuilder();
		TreeWalker.deepFirst().walk(tree).forEach(n -> {
			n.pathFromRoot().forEach(parent -> sb.append(parent).append('→'));
			sb.append(n).append(System.lineSeparator());
		});
		return sb.toString();
	}

	public static void main(final String... args) {
		final var root = 0;
		final var size = 10;
		final List<Integer> source = new ArrayList<>(ListKit.seq(root, size));
		Collections.shuffle(source);
		System.out.println("@@@@@@" + source);
	}
}
