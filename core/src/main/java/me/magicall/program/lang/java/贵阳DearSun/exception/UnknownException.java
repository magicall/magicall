/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

/**
 * @author Liang Wenjian
 */
public class UnknownException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 1135572476826903850L;

	public static final String DEFAULT_MSG = "Unknown exception happened.";

	public UnknownException() {
		super(DEFAULT_MSG);
	}

	public UnknownException(final Throwable cause) {
		super(DEFAULT_MSG, cause);
	}

	public UnknownException(final String message) {
		super(message);
	}

	public UnknownException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
