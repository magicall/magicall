/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.io;

import com.google.common.collect.Maps;
import me.magicall.program.lang.java.贵阳DearSun.exception.ExceptionHandler;
import me.magicall.program.lang.java.FinallyCallback;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class IOKit {

	/**
	 * 关闭对象，若关闭时抛出{@link IOException}，不抛出，而是作为返回值返回。
	 *
	 * @param closeables 可关闭的对象
	 * @return 关闭对象时抛的异常。若某对象未抛出异常，则返回中不存在其为键的映射。
	 */
	public static Map<Closeable, IOException> close(final Collection<Closeable> closeables) {
		final Map<Closeable, IOException> rt = Maps.newHashMap();
		closeables.stream().filter(Objects::nonNull).forEach(closeable -> {
			try {
				closeable.close();
			} catch (final IOException e) {
				rt.put(closeable, e);
			}
		});
		return rt;
	}

	/**
	 * 关闭对象，若关闭时抛出{@link IOException}，不抛出，而是作为返回值返回。
	 *
	 * @param closeables 可关闭的对象
	 * @return 关闭对象时抛的异常。若某对象未抛出异常，则返回中不存在其为键的映射。
	 */
	public static Map<Closeable, IOException> close(final Closeable... closeables) {
		return close(Arrays.asList(closeables));
	}

	/**
	 * 关闭对象，若关闭时抛出{@link IOException}，不抛出，而是作为返回值返回。
	 *
	 * @param closeable 可关闭的对象
	 * @return 关闭时抛出的异常
	 */
	public static IOException close(final Closeable closeable) {
		return close(Collections.singleton(closeable)).get(closeable);
	}

	/**
	 * 关闭对象，不抛出{@link IOException}。
	 *
	 * @param closeables 可关闭的对象
	 */
	public static void closeSilently(final Closeable... closeables) {
		close(closeables);
	}

	/**
	 * 关闭对象，不抛出{@link IOException}。
	 *
	 * @param closeables 可关闭的对象
	 */
	public static void closeSilently(final Collection<Closeable> closeables) {
		close(closeables);
	}

	/**
	 * 获取一个文件,如果不存在,将创建之.如果其路径中有的父文件夹不存在,也会创建.
	 *
	 * @param path 路径
	 * @return 文件
	 */
	public static File ensureFile(final String path) {
		final var f = new File(path);
		if (f.exists()) {
			return f;
		}
		final var dir = f.getParentFile();
		if (!dir.exists()) {
			if (!tryMakeDirs(dir)) {
				return null;
			}
		}
		try {
			if (f.createNewFile()) {
				return f;
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean tryMakeDirs(final File dir) {
		if (dir.exists()) {
			return !dir.isFile();
		} else {
			return dir.mkdirs();
		}
	}

	@Deprecated
	public static <C extends Closeable> void io(final CloseableCreator<C> creator, final CloseableIOCallback<C> callback,
																							final ExceptionHandler<IOException> exceptionHandler) {
		final var c = new CloseableCreatorWithFinallyCloseCallback<>(creator);
		io(c, callback, exceptionHandler, c);
	}

	@Deprecated
	public static <C extends Closeable> void io(final CloseableCreator<C> creator, final CloseableIOCallback<C> callback,
																							final ExceptionHandler<IOException> exceptionHandler,
																							final FinallyCallback finallyCallback) {
		try {
			final var closeable = creator.create();
			callback.callback(closeable);
		} catch (final IOException e) {
			if (exceptionHandler != null) {
				exceptionHandler.handle(e);
			}
		} finally {
			if (finallyCallback != null) {
				finallyCallback.finallyExecute();
			}
		}
	}

	@Deprecated
	private static class CloseableCreatorWithFinallyCloseCallback<C extends Closeable>
			implements CloseableCreator<C>, FinallyCallback {
		private final CloseableCreator<C> creator;
		private C closeable;

		CloseableCreatorWithFinallyCloseCallback(final CloseableCreator<C> creator) {
			super();
			this.creator = creator;
		}

		@Override
		public void finallyExecute() {
			close(closeable);
		}

		@Override
		public C create() throws IOException {
			closeable = creator.create();
			return closeable;
		}
	}
}
