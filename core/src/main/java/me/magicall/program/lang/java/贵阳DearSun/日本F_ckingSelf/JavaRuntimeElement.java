/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.Named;
import me.magicall.Owned;
import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.stream.Stream;

/**
 * <pre>
 * java运行时可以通过反射得到的元素。包括：
 * - 类型：包括泛型的类型。
 * - 构造器
 * - 方法
 * - 字段
 * - 注解等。
 *
 * 这些元素都有类型。“类型”是此种元素的实例作为值时的类型，可以赋值给相同类型变量名。因此
 * - 方法：返回值的类型
 * - 构造器：即是类本身的类型。
 * - 类型：自己本身
 * 一切基于类型的判断都基于{@link #elementType()}，比如是否基本类型、是否包装类等。
 *
 * 子类可以是通过某种代码片段创建的，该片段所使用的元素未必存在。比如一个叫com.foo.Bar的类，在上下文中不存在。
 * </pre>
 *
 * @param <_Type> 由于 java.lang.reflect.Type 没有继承 AnnotatedElement，所以不能加限定。但实际上，jdk原生的Type实现类现在都能加注解。
 */
public interface JavaRuntimeElement<_Type> extends Named, Owned<JavaRuntimeElement<?>>, CanBeAnnotated, Wrapper<_Type> {
	/**
	 * <pre>
	 * 对于存在的类、方法、构造器、字段，返回全限定名。
	 * 若本元素是通过名字构造的，则全限定名取决于构造时所用的名字。
	 * 若本元素代表一个泛型类型，全限定名会有些特殊处理。
	 * </pre>
	 *
	 * @return
	 */
	@Override
	String name();

	/**
	 * <pre>
	 * 直接持有本元素的对象。比如：
	 * - 方法：该方法所在的类。未必是定义该方法的类。
	 * - 构造器：该构造器所在的类。
	 * - 类型（非内部类型）：无
	 * 对于内部类型：
	 * - 类的内部类型：其所在的类型，相当于enclosingClass
	 * - 方法的内部类型：其所在的方法，相当于enclosingMethod
	 * - 构造器的内部类型：其所在构造器，相当于enclosingConstructor
	 * - 块的内部类型：其所在的类型
	 * </pre>
	 *
	 * @return
	 */
	@Override
	JavaRuntimeElement<?> owner();

//	/**
//	 * 所代表的元素是否真实存在。还是仅靠一个名字虚构的。
//	 * todo：似乎只对Cls有用
//	 * @return
//	 */
//	boolean exists();

	/**
	 * 所代表的元素的实例的类型。
	 * - 字段：该字段的类型
	 * - 方法：该方法的结果的类型
	 * - 构造器：该构造器的结果的类型，即该构造器的类本身。
	 *
	 * @return
	 */
	Type_<?> elementType();

	/**
	 * 本元素所在的顶层类型
	 *
	 * @return
	 */
	default Type_<?> topType() {
		return owner().topType();
	}

	/**
	 * 所属包。
	 * todo：EnhancedPackage
	 *
	 * @return 包
	 */
	default Package pack() {
		return owner().pack();
	}

	//=================================与类型有关的

	//--------------------------------------泛型

	/**
	 * <pre>
	 * 本成员的类型的泛型参数：
	 * - 字段、参数、变量：类型的泛型参数，比如 private List&lt;E&gt; list; 中的E
	 * - 类：类的泛型参数，比如default class C&lt;T&gt;中的T
	 * - 方法：返回值类型的泛型参数，比如 default &lt;T&gt; List&lt;E&gt; list(T t) 中的E，而非T，这需要特别注意。
	 *   - 构造方法：由于返回值类型就是本类的Class本身，所以泛型参数就是本类的类泛型参数
	 * </pre>
	 *
	 * @return
	 */
	default Stream<Type_<?>> generics() {
		return elementType().generics();
	}

	/**
	 * 本成员的类型定义了几个泛型
	 *
	 * @return
	 */
	default int genericsCount() {
		return (int) generics().count();
	}

	/**
	 * 本成员的类型是否定义了泛型
	 *
	 * @return 是否泛型
	 */
	default boolean hasGenerics() {
		return generics().findAny().isEmpty();
	}

	//======================================注解

	@Override
	default Stream<Obj<Annotation>> annotations() {
		return unwrap() instanceof final AnnotatedElement a//
					 ? Stream.of(a.getAnnotations()).map(Obj::of) : Stream.empty();
	}

	@Override
	default <A extends Annotation> Obj<A> findAnnotation(final Cls<A> type) {
		return findAnnotation(type.toClass());
	}

	@Override
	default <A extends Annotation> Obj<A> findAnnotation(final Class<A> type) {
		return type.isAnnotation() && unwrap() instanceof final AnnotatedElement a//
					 ? Obj.of(a.getAnnotation(type)) : null;
	}

	@Override
	default Stream<Obj<Annotation>> directAnnotations() {
		return unwrap() instanceof final AnnotatedElement a//
					 ? Stream.of(a.getDeclaredAnnotations()).map(Obj::of) : Stream.empty();
	}

	@Override
	default <A extends Annotation> Obj<A> findDirectAnnotation(final Cls<A> type) {
		return findDirectAnnotation(type.toClass());
	}

	@Override
	default <A extends Annotation> Obj<A> findDirectAnnotation(final Class<A> type) {
		//为免泛型强转，还是检查一下是否注解吧。
		return type.isAnnotation() && unwrap() instanceof final AnnotatedElement a//
					 ? Obj.of(a.getDeclaredAnnotation(type)) : null;
	}
}
