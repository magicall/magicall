/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

@FunctionalInterface
public interface ProxyMethodInvoker {

	Object invoke(final InvocationHandler invocationHandler, final Object methodOwner, final Method method,
								final Object... argsForMethod) throws Exception;

	//===========================

	/**
	 * 无论方法是什么，直接返回null。也许有用。
	 */
	ProxyMethodInvoker RETURN_NULL = (invocationHandler, methodOwner, method, argsForMethod) -> null;

	/**
	 * 无论方法是什么，直接抛出{@link AbstractMethodError}异常。也许有用。
	 */
	ProxyMethodInvoker NO_IMPLEMENT = (invocationHandler, methodOwner, method, argsForMethod) -> {
		throw new AbstractMethodError();
	};

	/**
	 * 对invocationHandler调用方法。
	 */
	ProxyMethodInvoker METHOD_INVOKE_USING_INVOCATION_HANDLER
			= (invocationHandler, methodOwner, method, argsForMethod) -> method.invoke(invocationHandler, argsForMethod);
}
