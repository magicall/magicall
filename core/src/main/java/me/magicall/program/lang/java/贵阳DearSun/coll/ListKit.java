/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.Unmodifiable;

import java.io.Serial;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class ListKit extends AbsCollKit<List<?>> implements Serializable {
	@Serial
	private static final long serialVersionUID = 7338971757402227665L;
	private static final List<Class<?>> SUPPORTED_CLASSES = Collections.singletonList(List.class);
	/**
	 * <code>java.util.Collections.UnmodifiableList</code>的Class对象，由于该类不可见，只能通过这个方法获得它的Class对象
	 */
	private static final Class<?> JDK_COLLECTIONS_UNMODIFIABLE_LIST_CLASS = Collections.unmodifiableList(
			Collections.emptyList()).getClass();

	public static final ListKit INSTANCE = new ListKit();
	public static final Random RANDOM = new SecureRandom();

	//===============================================

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public List<?> parse(final String source) {
		return null;//todo
	}

	//这里覆盖父类方法,是为了返回值能带上泛型.这个方法不好
	@Override
	public <E, E1> List<E> emptyVal() {
		return (List) EmptyColl.INSTANCE;
	}

	/**
	 * 将一个来历不明的集合的泛型强转成另一种类型的集合泛型.
	 * 注意:这是"非安全的"，适用场合：当使用者很清楚地确定source（是一个Collection&lt;S&gt;）中的S是T的父类，用此方法帮助做强制类型转换，可少写很多恶心代码
	 * 注：跟父类实现一致，覆写一遍是为了返回类型能带上泛型T。
	 *
	 * @param <T> 集合的元素类型。
	 * @param source 源集合。
	 * @return 强转的集合。
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> cast(final List<?> source) {
		return (List<T>) source;
	}

	/**
	 * 公差为1的等差数列，从from开始，共size个。
	 *
	 * @param from 开始
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Long> seq(final long from, final int size) {
		return Stream.iterate(from, i -> i + 1).limit(size).collect(Collectors.toList());
	}

	/**
	 * 公差为step的等差数列，从from开始，共size个。
	 *
	 * @param from 开始
	 * @param step 步长
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Long> seq(final long from, final long step, final int size) {
		return step == 0 ? Collections.nCopies(size, from)
										 : Stream.iterate(from, aLong -> aLong + step).limit(size).collect(Collectors.toList());
	}

	/**
	 * 公差为1的等差数列，从from开始，共size个。
	 *
	 * @param from 开始
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Integer> seq(final int from, final int size) {
		return seq(from, 1, size);
	}

	/**
	 * 公差为step的等差数列，从from开始，共size个。
	 *
	 * @param from 开始
	 * @param step 步长
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Integer> seq(final int from, final int step, final int size) {
		return step == 0 ? Collections.nCopies(size, from)
										 : Stream.iterate(from, aLong -> aLong + step).limit(size).collect(Collectors.toList());
	}

	/**
	 * 正整数数列（从1开始）
	 *
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Integer> positiveSeq(final int size) {
		return seq(1, 1, size);
	}

	/**
	 * 自然数数列(从0开始)
	 *
	 * @param size 数量
	 * @return 列表
	 */
	public static List<Integer> natureSeq(final int size) {
		return seq(0, 1, size);
	}

	public <E> E last(final List<E> source) {
		return isEmpty(source) ? null : source.get(source.size() - 1);
	}

	public <E> E random(final List<E> list) {
		if (isEmpty(list)) {
			return null;
		}
		final var size = list.size();
		return list.get(size == 1 ? 0 : RANDOM.nextInt(size));
	}

	@Deprecated
	public <F, T> List<T> transform(final List<? extends F> source,
																	final ElementTransformer<? super F, ? extends T> transformer) {
		final List<T> rt = Lists.newArrayList();
		var index = 0;
		for (final F element : source) {
			rt.add(transformer.transform(index, element));
			++index;
		}
		return rt;
	}

	/**
	 * 返回一个迭代顺序相反的List
	 *
	 * @param <T> 元素的类型
	 * @param source 源列表
	 * @return 反序列表
	 */
	public <T> List<T> reverse(final List<T> source) {
		return source instanceof ReverseList<?> ? ((ReverseList<T>) source).unwrap() : new ReverseList<>(source);
	}

	public <T> List<T> unmodifiable(final List<T> source) {
		if (source instanceof Unmodifiable) {
			return source;
		}
		if (source == Collections.emptyList()) {
			return source;
		}
		if (source.getClass() == JDK_COLLECTIONS_UNMODIFIABLE_LIST_CLASS) {
			return source;
		}
		return Collections.unmodifiableList(source);
	}

	/**
	 * 返回前面的一部分作为一个list
	 *
	 * @param <T> 元素的类型
	 * @param source 源列表
	 * @param to 结束的下标
	 * @return 头部列表
	 */
	public <T> List<T> head(final List<T> source, final int to) {
		return subList(source, 0, to);
	}

	/**
	 * 返回后面一部分作为一个list
	 *
	 * @param <T> 元素的类型
	 * @param source 源列表
	 * @param from 开始的下标
	 * @return 尾部列表
	 */
	public <T> List<T> tail(final List<T> source, final int from) {
		return subList(source, from, Integer.MAX_VALUE);
	}

	/**
	 * 加强版subList.对于from&gt;to的情况,会反转两个参数的值.如下情况会返回空列表而非抛异常或者null:
	 * 1，source是"空的";
	 * 2，from==to;
	 * 3，from,to都&gt;size;
	 * 4，from,to都&lt;0;
	 * 5，一切内部异常
	 *
	 * @param <T> 元素的类型
	 * @param source 源
	 * @param from 起始下标（包含）
	 * @param to 终止下标（不包含）
	 * @return 你要的东西！
	 */
	public <T> List<T> subList(final List<T> source, final int from, final int to) {
		if (isEmpty(source)) {
			return emptyVal();
		}
		if (from == to) {
			return emptyVal();
		}

		int fromIndex;
		int toIndex;
		if (from < to) {
			fromIndex = from;
			toIndex = to;
		} else {
			fromIndex = to;
			toIndex = from;
		}
		//此时f必<t
		final var size = source.size();
		if (fromIndex < 0) {
			if (toIndex > size) { //源list是所需的子集
				return source;
			} else if (toIndex < 0) {
				return emptyVal();
			} else {
				fromIndex = 0;
			}
		} else if (fromIndex > size) { //
			return emptyVal();
		} else if (toIndex > size) {
			toIndex = size;
		}
		return source.subList(fromIndex, toIndex);
	}

	/**
	 * 从一个list里随机选取一段区间的简单实现.
	 * 注:事实上每个元素能被选出的概率不一样大.所以仅适用于不严格要求随机性的场景
	 *
	 * @param <E> 元素的列表
	 * @param source 源列表
	 * @param size 指定数量
	 * @return 区段列表
	 */
	public <E> List<E> randomSection(final List<E> source, final int size) {
		if (isEmpty(source)) {
			return emptyVal();
		}
		if (source.size() <= size) {
			return source;
		}
		final var maxFromIndex = RANDOM.nextInt(source.size() - size);
		return subList(source, maxFromIndex, maxFromIndex + size);
	}

	/**
	 * 从一个list里随机取出size个元素的简单实现.
	 * 注:事实上每个元素能被选出的概率不一样大.所以仅适用于不严格要求随机性的场景
	 *
	 * @param <E> 元素的类型
	 * @param source 源列表
	 * @param size 指定的数量
	 * @return 随机元素列表
	 */
	public <E> List<E> randomSome(final List<E> source, final int size) {
		if (isEmpty(source)) {
			return emptyVal();
		}
		final var sourceSize = source.size();
		if (sourceSize <= size) {
			return source;
		}
		final List<E> rt = new ArrayList<>(size);
		var remainSourceSize = sourceSize;
		var remainNeedSize = size;
		var nextFromIndex = 0;
		while (rt.size() < size) {
			final var index = nextFromIndex + RANDOM.nextInt(remainSourceSize / remainNeedSize);
			rt.add(source.get(index));
			remainNeedSize--;
			nextFromIndex = index + 1;
			remainSourceSize = sourceSize - nextFromIndex;
		}
		return rt;
	}

	//=======================================================

	private Object readResolve() {
		return INSTANCE;
	}
}
