/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class WrongNumException extends WrongArgException {

	@Serial
	private static final long serialVersionUID = 6123972395990543683L;

	public WrongNumException(final String argName, final Number actualVal) {
		super(argName, actualVal);
	}

	public WrongNumException(final String argName, final Number actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public WrongNumException(final String argName, final Number actualVal, final Object expectVal) {
		super(argName, actualVal, expectVal);
	}

	public WrongNumException(final String argName, final Number actualVal, final Object expectVal,
													 final Throwable cause) {
		this(argName, actualVal, expectVal);
		initCause(cause);
	}

	@Override
	public Number getActualVal() {
		return (Number) super.getActualVal();
	}
}
