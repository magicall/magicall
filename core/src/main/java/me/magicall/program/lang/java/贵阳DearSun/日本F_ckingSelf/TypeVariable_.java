/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;

/**
 * 代表泛型声明的类型。比如 class C&lt;T&gt;中的&lt;T&gt;
 */
class TypeVariable_ extends NotExactType {
	TypeVariable_(final TypeVariable<?> raw) {
		super(raw, calOwner(raw), raw.getBounds());
	}

	private static JavaRuntimeElement<?> calOwner(final TypeVariable<?> raw) {
		final var genericDeclaration = raw.getGenericDeclaration();
		if (genericDeclaration instanceof final Class<?> c) {
			return of(c);
		} else if (genericDeclaration instanceof final Method method) {
			return new Method_(method);
		} else if (genericDeclaration instanceof final Constructor<?> constructor) {
			return new Constructor_<>(constructor);
		}
		throw new UnknownException();
	}
}
