/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 映射表包装器适配器。包装了另一个集合，用代理的方式实现了集合接口所有方法，子类根据具体需求实现所需的方法即可。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface MapWrapper<K, V> extends Map<K, V>, Wrapper<Map<K, V>> {
	/**
	 * 返回原始的映射表。
	 * 注意：写方法将直接影响返回的映射表。所以实现类若想返回原始映射表的副本，则必须覆写那些写方法。
	 *
	 * @return 原始的映射表
	 */
	@Override
	Map<K, V> unwrap();

	@Override
	default int size() {
		return unwrap().size();
	}

	@Override
	default boolean isEmpty() {
		return unwrap().isEmpty();
	}

	@Override
	default boolean containsKey(final Object key) {
		return unwrap().containsKey(key);
	}

	@Override
	default boolean containsValue(final Object value) {
		return unwrap().containsValue(value);
	}

	@Override
	default V get(final Object key) {
		return unwrap().get(key);
	}

	@Override
	default V put(final K key, final V value) {
		return unwrap().put(key, value);
	}

	@Override
	default V remove(final Object key) {
		return unwrap().remove(key);
	}

	@Override
	default void putAll(final Map<? extends K, ? extends V> m) {
		unwrap().putAll(m);
	}

	@Override
	default void clear() {
		unwrap().clear();
	}

	@Override
	default Set<K> keySet() {
		return unwrap().keySet();
	}

	@Override
	default Collection<V> values() {
		return unwrap().values();
	}

	@Override
	default Set<Entry<K, V>> entrySet() {
		return unwrap().entrySet();
	}

	@Override
	default V getOrDefault(final Object key, final V defaultValue) {
		return unwrap().getOrDefault(key, defaultValue);
	}

	@Override
	default void forEach(final BiConsumer<? super K, ? super V> action) {
		unwrap().forEach(action);
	}

	@Override
	default void replaceAll(final BiFunction<? super K, ? super V, ? extends V> function) {
		unwrap().replaceAll(function);
	}

	@Override
	default V putIfAbsent(final K key, final V value) {
		return unwrap().putIfAbsent(key, value);
	}

	@Override
	default boolean remove(final Object key, final Object value) {
		return unwrap().remove(key, value);
	}

	@Override
	default boolean replace(final K key, final V oldValue, final V newValue) {
		return unwrap().replace(key, oldValue, newValue);
	}

	@Override
	default V replace(final K key, final V value) {
		return unwrap().replace(key, value);
	}

	@Override
	default V computeIfAbsent(final K key, final Function<? super K, ? extends V> mappingFunction) {
		return unwrap().computeIfAbsent(key, mappingFunction);
	}

	@Override
	default V computeIfPresent(final K key, final BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return unwrap().computeIfPresent(key, remappingFunction);
	}

	@Override
	default V compute(final K key, final BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		return unwrap().compute(key, remappingFunction);
	}

	@Override
	default V merge(final K key, final V value, final BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
		return unwrap().merge(key, value, remappingFunction);
	}
}
