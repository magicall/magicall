/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.BeanKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.TwoTuple;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * 从某一类的实例对象中取出或设置值的工具.
 *
 * @param <E> 针对的对象的类型。
 * @author MaGiCalL
 */
public interface FieldValueAccessor<E> {

	FieldValueAccessor<Map<String, Object>> MAP_VALUE_ACCESSOR = new FieldValueAccessor<>() {

		@Override
		public Object getValue(final Map<String, Object> obj, final String fieldName) {
			return obj.get(fieldName);
		}

		@Override
		public void setValue(final Map<String, Object> obj, final String fieldName, final Object value) {
			obj.put(fieldName, value);
		}
	};
	FieldValueAccessor<Object> BEAN_VALUE_ACCESSOR = new FieldValueAccessor<>() {

		private final Map<TwoTuple<Class<?>, String>, Method> getters = new WeakHashMap<>();
		private final Map<TwoTuple<Class<?>, String>, Method> setters = new WeakHashMap<>();

		@Override
		public Object getValue(final Object obj, final String fieldName) {
			final var c = obj.getClass();
			final TwoTuple<Class<?>, String> key = new TwoTuple<>(c, fieldName);
			final var method = getters.computeIfAbsent(key, k -> BeanKit.getGetter(fieldName, c));
			return MethodKit.invoke(obj, method);
		}

		@Override
		public void setValue(final Object obj, final String fieldName, final Object value) {
			if (value == null) {
				return;
			}
			final var c = obj.getClass();
			final TwoTuple<Class<?>, String> key = new TwoTuple<>(c, fieldName);
			final var method = setters.computeIfAbsent(key,
					k -> BeanKit.getSetterIgnoreNameCaseAndTypeAssigned(c, value.getClass()));
			MethodKit.invoke(obj, method, value);
		}
	};

	Object getValue(E obj, String fieldName);

	void setValue(E obj, String fieldName, Object value);
}
