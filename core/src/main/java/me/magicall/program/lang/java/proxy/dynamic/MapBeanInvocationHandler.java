/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.proxy.dynamic;

import com.google.common.collect.Maps;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.Field_;
import me.magicall.program.lang.java.贵阳DearSun.BeanKit;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.MethodKit;
import me.magicall.program.lang.java.贵阳DearSun.ObjKit;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 使用一个Map，通过动态代理，代理一个java bean。
 */
public class MapBeanInvocationHandler implements InvocationHandler {

	/**
	 * 代理bean所有字段和值
	 */
	private final Map<String, Object> fields;

	public MapBeanInvocationHandler() {
		this(Maps.newHashMap());
	}

	public MapBeanInvocationHandler(final Map<String, Object> fields) {
		this.fields = fields;
	}

	@Override
	public final Object invoke(final Object obj, final Method method, final Object[] args)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//setter
		if (BeanKit.isSetter(method, false)) {
			set(BeanKit.setterNameToFieldName(method.getName()), args[0]);
			return null;
		}
		//getter
		if (BeanKit.isGetter(method)) {
			final var fieldName = BeanKit.getterToFieldName(method);
			if (fields.containsKey(fieldName)) {
				return get(fieldName);
			}
			if (method.isDefault()) {
				return invokeDefaultMethod(obj, method);
			}
			set(fieldName, null);
			return null;
		}
		//hashCode
		if (MethodKit.isHashCode(method)) {
			return invokeHashCode();
		}
		//equals
		if (MethodKit.isTheEquals(method)) {
			return invokeEquals(method, args);
		}
		//toString
		if (MethodKit.isToString(method)) {
			return invokeToString(method, args);
		}
		return invokeOther(method, args);
	}

	protected Object invokeOther(final Method method, final Object... args)
			throws IllegalAccessException, InvocationTargetException {
		return invokeMyself(method, args);
	}

	protected Object invokeToString(final Method method, final Object... args)
			throws InvocationTargetException, IllegalAccessException {
		return invokeMyself(method, args);
	}

	protected Object invokeEquals(final Method method, final Object... args)
			throws InvocationTargetException, IllegalAccessException {
		return invokeMyself(method, args);
	}

	protected Object invokeHashCode() {
		return fields.hashCode();
	}

	protected final Object invokeMyself(final Method method, final Object... args)
			throws IllegalAccessException, InvocationTargetException {
		return method.invoke(this, args);
	}

	private static Object invokeDefaultMethod(final Object obj, final Method method) {
		final var declaringClass = method.getDeclaringClass();
		final var lookup = MethodHandles.publicLookup().in(declaringClass);
		hackLookup(lookup);
		try {
			return lookup.unreflectSpecial(method, declaringClass)//
					.bindTo(obj)//
					.invokeWithArguments();
		} catch (final Throwable throwable) {
			throwable.printStackTrace();
			throw new UnknownException(throwable);
		}
	}

	private static void hackLookup(final Lookup lookup) {
		try {
			final var f = Lookup.class.getDeclaredField("allowedModes");
			final var enhancedAllowedModes = new Field_(f);
			enhancedAllowedModes.ensureAccessible();
			f.set(lookup, Lookup.PRIVATE);
		} catch (final NoSuchFieldException | IllegalAccessException e) {
			//不可能到这里
			e.printStackTrace();
			throw new UnknownException(e);
		}
	}

	public final void set(final String fieldName, final Object value) {
		fields.put(fieldName, value);
	}

	public final Object get(final String fieldName) {
		return fields.get(fieldName);
	}

	public final Map<String, Object> toMap() {
		return new LinkedHashMap<>(fields);
	}

	@Override
	public String toString() {
		final var sb = new StringBuilder();
		fields.forEach((k, v) -> sb.append(ObjKit.deepToString(k)).append('=')//
				.append(ObjKit.deepToString(v)).append(','));
		final var length = sb.length();
		final var last = length - 1;
		if (length > 0 && sb.charAt(last) == ',') {
			sb.deleteCharAt(last);
		}
		return sb.toString();
	}
}
