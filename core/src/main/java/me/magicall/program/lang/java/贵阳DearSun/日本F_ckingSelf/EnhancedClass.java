///*
// * Copyright (c) 2024 Liang Wenjian
// * magicall is licensed under Mulan PSL v2.
// * You can use this software according to the terms and conditions of the Mulan PSL v2.
// * You may obtain a copy of Mulan PSL v2 at:
// *          http://license.coscl.org.cn/MulanPSL2
// * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// * See the Mulan PSL v2 for more details.
// */
//
//package me.magicall.program.lang.java;
//
//import java.lang.reflect.TypeVariable;
//import java.util.Arrays;
//import java.util.stream.Stream;
//
//public class EnhancedClass<T> extends Type_<T> implements Accessible, CanBeReflected<Class<T>> {
//	private final Class<T> raw;
//	private final String fullName;
//	private final String shortName;
//
//	@SuppressWarnings("unchecked")
//	public EnhancedClass(final String fullName) {
//		final String s = fullName.strip();
//		raw = (Class<T>) ClassKit.classCalled(s);
//		this.fullName = s;
//		shortName = StrKit.subStringAfterLastSeq(s, ".");
//	}
//
//	public EnhancedClass(final Class<T> raw) {
//		this.raw = raw;
//		if (raw == null) {
//			fullName = "null";
//			shortName = "null";
//		} else {
//			fullName = raw.getName();
//			shortName = raw.getSimpleName();
//		}
//	}
//
//	//======================================= 元信息
//
//	@Override
//	public boolean exists() {
//		return raw != null && raw != VOID_ENHANCED_CLASS.raw;
//	}
//
//	@Override
//	public Class<T> unwrap() {
//		return raw;
//	}
//
//	@Override
//	public Class<T> toClass() {
//		return raw;
//	}
//
//	@Override
//	public String name() {
//		return fullName;
//	}
//
//	@Override
//	public String shortName() {
//		return shortName;
//	}
//
//	@Override
//	public Package pack() {
//		return exists() ? PackageKit.packageOf(toClass()) : null;
//	}
//
//	@Override
//	public CanBeReflected<?> owner() {
//		if (isInMethod()) {
//			return new Method_(toClass().getEnclosingMethod());
//		}
//		if (isInConstructor()) {
//			return new Constructor_<>(toClass().getEnclosingConstructor());
//		}
//		if (isInner() || isInInitBlock()) {
//			return of(toClass().getEnclosingClass());
//		}
//		return null;
//	}
//
//	//======================================类型
//
//	@Override
//	public EnhancedClass<?> type() {
//		return this;
//	}
//
//	@Override
//	public EnhancedClass<?> topType() {
//		if (!exists()) {
//			return null;
//		}
//		Class<?> c = raw;
//		while (c != null && c.getDeclaringClass() != null) {
//			c = c.getDeclaringClass();
//		}
//		return of(c);
//	}
//
//	//-------------------------------泛型
//
//	@Override
//	public Stream<TypeVariable<?>> generics() {
//		return exists() ? Arrays.stream(toClass().getTypeParameters()) : Stream.empty();
//	}
//
////	/**
////	 * 本类定义了几个泛型
////	 *
////	 * @return 泛型数量。
////	 */
////	public int countGenericTypes() {
////		return ClassKit.countGenericTypes(toClass());
////	}
////
////	/**
////	 * 本类是否有泛型。
////	 *
////	 * @return 是否有泛型
////	 */
////	public boolean hasGenericTypes() {
////		return ClassKit.hasGenericTypes(toClass());
////	}
//
//	/**
//	 * 本类是否实现了某个泛型参数。
//	 *
//	 * @return 是否实现了某个泛型参数
//	 */
//	@Override
//	public boolean isImplementingGenericTypes() {
//		return ClassKit.isImplementingGenericTypes(toClass());
//	}
//
//	//----------------------
//
//	@Override
//	public AccessLv accessLv() {
//		if (!exists()) {
//			return AccessLv.PRIVATE;
//		}
//		return AccessLv.of(raw);
//	}
//
//	@Override
//	public boolean isAbstract() {
//		return exists() && ClassKit.isAbstract(toClass());
//	}
//
//	@Override
//	public boolean isExtendable() {
//		return exists() && ClassKit.isExtendable(toClass());
//	}
//
//	/**
//	 * strictfp 也可用于修饰class、method。
//	 *
//	 * @return 是否使用了strictfp
//	 */
//	@Override
//	public boolean isStrict() {
//		return exists() && ClassKit.isStrict(toClass());
//	}
//
//	/**
//	 * 是否匿名类
//	 *
//	 * @return 是否匿名类
//	 */
//	@Override
//	public boolean isAnonymous() {
//		return exists() && ClassKit.isAnonymous(toClass());
//	}
//
//	/**
//	 * 是否内部类
//	 *
//	 * @return 是否内部类
//	 */
//	@Override
//	public boolean isInner() {
//		return exists() && ClassKit.isInner(toClass());
//	}
//
//	@Override
//	public boolean isInMethod() {
//		return exists() && ClassKit.isInMethod(toClass());
//	}
//
//	@Override
//	public boolean isInConstructor() {
//		return exists() && ClassKit.isInConstructor(toClass());
//	}
//
//	@Override
//	public boolean isInInitBlock() {
//		return exists() && ClassKit.isInInitBlock(toClass());
//	}
//
//	@Override
//	public boolean isStatic() {
//		return exists() && ClassKit.isStatic(toClass());
//	}
//
//	//==========================================继承
//
//	/**
//	 * 祖先类们。包括类和接口。无序。
//	 *
//	 * @return 祖先类型的增强对象。
//	 */
//	@Override
//	public Stream<EnhancedClass<?>> ancestors() {
//		return exists() ? ClassKit.ancestorsOf(toClass()).map(EnhancedClass::of) : Stream.empty();
//	}
//
//	/**
//	 * 获取父类的{@link EnhancedClass}对象。
//	 *
//	 * @return 父类的{@link EnhancedClass}对象。
//	 */
//	@Override
//	public EnhancedClass<? super T> superClass() {
//		if (!exists()) {
//			return null;
//		}
//		final Class<? super T> parent = toClass().getSuperclass();
//		return parent == null ? null : of(parent);
//	}
//
//	/**
//	 * 获取本对象所有超类。
//	 *
//	 * @return 本对象所有超类。
//	 */
//	@Override
//	public Stream<? extends EnhancedClass<? super T>> superClasses() {
//		return exists() ? ClassKit.superClassesOf(toClass()).map(EnhancedClass::of) : Stream.empty();
//	}
//
//	/**
//	 * 获取所有实现/继承来的接口。递归，去重。
//	 *
//	 * @return 接口集
//	 */
//	@Override
//	public Stream<EnhancedClass<? super T>> interfaces() {
//		return exists() ? ClassKit.interfacesOf(toClass()).map(EnhancedClass::of) : Stream.empty();
//	}
//
//	@Override
//	public boolean hasInterfaces() {
//		return exists() && ClassKit.hasInterface(toClass());
//	}
//
//	/**
//	 * 是否参数类型或其后代。
//	 *
//	 * @param maybeMeOrAncestor 指定的类型
//	 * @return 是否参数类型或其后代类。
//	 */
//	@Override
//	public boolean isOrSubOf(final Class<?> maybeMeOrAncestor) {
//		return exists() && ClassKit.isOrSubOf(toClass(), maybeMeOrAncestor);
//	}
//
//	/**
//	 * 是否指定类型或其超类。
//	 *
//	 * @param maybeMeOrProgeny 指定的类型
//	 * @return 是否指定类型或其超类。
//	 */
//	@Override
//	public boolean isOrSuperOf(final Class<?> maybeMeOrProgeny) {
//		return exists() && ClassKit.isOrSuperOf(toClass(), maybeMeOrProgeny);
//	}
//
//	//===========================================成员
//
//	/**
//	 * 获取所有构造方法。
//	 *
//	 * @return 所有的构造方法
//	 */
//	@Override
//	public Stream<Constructor_<T>> constructors() {
//		return exists() ? ClassKit.constructorsOf(toClass()).map(constructor -> new Constructor_<>(constructor)) : Stream.empty();
//	}
//
//	/**
//	 * 获取默认构造器（即那个无参的）
//	 *
//	 * @return
//	 */
//	@Override
//	public Constructor_<T> defaultConstructor() {
//		return constructors().filter(e -> !e.hasParams()).findFirst().orElse(null);
//	}
//
//	/**
//	 * 是否存在默认构造器（即那个无参的）
//	 *
//	 * @return
//	 */
//	@Override
//	public boolean hasDefaultConstructor() {
//		return exists() && constructors().anyMatch(e -> !e.hasParams());
//	}
//
//	/**
//	 * 获取所有方法，包括继承来的、静态的。去重。无序。
//	 *
//	 * @return 方法集
//	 */
//	@Override
//	public Stream<Method_> methods() {
//		return exists() ? ClassKit.methodsOf(toClass()).map(method -> new Method_(method)) : Stream.empty();
//	}
//
//	/**
//	 * 本类自己定义的方法。包括覆写的。
//	 *
//	 * @return 方法集
//	 */
//	@Override
//	public Stream<Method_> myOwnMethods() {
//		return exists() ? ClassKit.ownMethodsOf(toClass()).map(method -> new Method_(method)) : Stream.empty();
//	}
//
//	@Override
//	public boolean hasMyOwnMethods() {
//		return exists() && myOwnMethods().findAny().isPresent();
//	}
//
//	/**
//	 * 根据名字、参数类型列表获取一个“合适的”方法。包括继承的、静态的。
//	 *
//	 * @param name 方法名
//	 * @param paramTypes 参数类型列表
//	 * @return 若无，返回null。
//	 */
//	@Override
//	public Method_ methodOf(final String name, final Class<?>... paramTypes) {
//		return exists() ? new Method_(ClassKit.methodOf(toClass(), name, paramTypes)) : null;
//	}
//
//	/**
//	 * 是否存在指定的方法。包括继承的。
//	 *
//	 * @param name 方法名
//	 * @param paramTypes 参数类型集
//	 * @return 是否
//	 */
//	@Override
//	public boolean hasMethod(final String name, final Class<?>... paramTypes) {
//		return exists() && ClassKit.hasMethod(toClass(), name, paramTypes);
//	}
//
//	/**
//	 * 获取所有字段，包括继承的、静态的。
//	 *
//	 * @return 字段集。
//	 */
//	@Override
//	public Stream<Field_> fields() {
//		return exists() ? ClassKit.fieldsOf(toClass()).map(field -> new Field_(field)) : Stream.empty();
//	}
//
//	@Override
//	public boolean hasFields() {
//		return exists() && ClassKit.hasFields(toClass());
//	}
//
//	/**
//	 * 自有的字段，包括静态的。
//	 *
//	 * @return 字段集。
//	 */
//	@Override
//	public Stream<Field_> myOwnFields() {
//		return exists() ? ClassKit.ownFieldsOf(toClass()).map(field -> new Field_(field)) : Stream.empty();
//	}
//
//	@Override
//	public boolean hasOwnFields() {
//		return exists() && ClassKit.hasOwnFields(toClass());
//	}
//
//	@Override
//	public Field_ field(final String fieldName) {
//		return exists() ? new Field_(ClassKit.fieldCalled(toClass(), fieldName)) : null;
//	}
//
//	@Override
//	public boolean hasField(final String fieldName) {
//		return exists() && ClassKit.hasFieldCalled(toClass(), fieldName);
//	}
//
//	@Override
//	public Field_ field(final Class<?> type, final String fieldName) {
//		return exists() ? new Field_(ClassKit.fieldOf(toClass(), fieldName, type)) : null;
//	}
//
//	@Override
//	public boolean hasField(final Class<?> type, final String fieldName) {
//		return exists() && ClassKit.hasField(toClass(), fieldName, type);
//	}
//
//	//======================================对象
//
//	/**
//	 * 是否本类型的实例。
//	 *
//	 * @param obj 对象
//	 * @return 是否
//	 */
//	@Override
//	public boolean isPrototype(final Object obj) {
//		return exists() && getClass().isInstance(obj);
//	}
//
//	@Override
//	public boolean canNewInstance() {
//		return exists() && ClassKit.canNewInstance(toClass());
//	}
//
//	@Override
//	public boolean canInstanceBe(final Object obj) {
//		if (!exists()) {
//			return false;
//		}
//		if (obj == null) {
//			return !isPrimitive();
//		}
//		return unwrap().isInstance(obj) || canInstanceBeType(obj.getClass());
//	}
//
//	@Override
//	public boolean canInstanceBeType(final Class<?> otherType) {
//		if (exists()) {
//			return ClassKit.fit(unwrap(), otherType) || checkNumTypes(otherType);
//		}
//		return false;
//	}
//
//	private boolean checkNumTypes(final Class<?> c) {
//		if (raw == double.class || raw == Double.class) {
//			return c == float.class || c == Float.class//
//					|| c == long.class || c == Long.class//
//					|| c == int.class || c == Integer.class//
//					|| c == short.class || c == Short.class//
//					|| c == byte.class || c == Byte.class//
//					|| c == char.class || c == Character.class//
//					|| c == double.class || c == Double.class;
//		} else if (raw == float.class || raw == Float.class) {
//			return c == float.class || c == Float.class//
//					|| c == long.class || c == Long.class//
//					|| c == int.class || c == Integer.class//
//					|| c == short.class || c == Short.class//
//					|| c == byte.class || c == Byte.class//
//					|| c == char.class || c == Character.class;
//		} else if (raw == long.class || raw == Long.class) {
//			return c == long.class || c == Long.class//
//					|| c == int.class || c == Integer.class//
//					|| c == short.class || c == Short.class//
//					|| c == byte.class || c == Byte.class//
//					|| c == char.class || c == Character.class;
//		} else if (raw == int.class || raw == Integer.class) {
//			return c == int.class || c == Integer.class//
//					|| c == short.class || c == Short.class//
//					|| c == byte.class || c == Byte.class//
//					|| c == char.class || c == Character.class;
//		} else if (raw == short.class || raw == Short.class) {
//			return c == short.class || c == Short.class//
//					|| c == byte.class || c == Byte.class//
//					|| c == char.class || c == Character.class;
//		}
//		return false;
//	}
//
//	//===============================================
//
//	@SuppressWarnings("unchecked")
//	public static <T> EnhancedClass<T> of(final Class<?> clazz) {
//		return (EnhancedClass<T>) (clazz == null ? ofNull() : new EnhancedClass<>(clazz));
//	}
//
//	public static EnhancedClass<?> of(final String fullName) {
//		return fullName == null ? ofNull() : new EnhancedClass<>(fullName);
//	}
//
//	/**
//	 * 表示“空”的对象的类型，我们用void.class（Void.Type）代替“null的类型”。
//	 * java用null表示“空对象”，但null并不是一个特殊的对象，所以本身没有“类型”。这与其他一些语言不同，比如python，用None表示“空对象”，它确实是个对象，所以有类型。
//	 *
//	 * @return
//	 */
//	public static EnhancedClass<Void> ofNull() {
//		return VOID_ENHANCED_CLASS;
//	}
//
//	private static final EnhancedClass<Void> VOID_ENHANCED_CLASS = new EnhancedClass<>(void.class);
//}
