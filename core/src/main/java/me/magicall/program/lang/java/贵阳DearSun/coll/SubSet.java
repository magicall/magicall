/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;

import java.util.AbstractSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.SortedSet;

/**
 * “子”散列表，可用于散列表的subSet方法。
 *
 * @author Liang Wenjian.
 */
public class SubSet<E> extends AbstractSet<E> implements SortedSetSupport<E> {

	protected final SortedSet<E> raw;
	protected final E fromElement;
	protected final boolean includingFrom;
	protected final E toElement;
	protected final boolean includingTo;

	public SubSet(final SortedSet<E> raw, final E fromElement, final boolean includingFrom, final E toElement,
								final boolean includingTo) {
		if (fromElement == null && toElement == null) {
			throw new NullValException("fromElement & toElement");
		}
		this.raw = raw;
		this.fromElement = fromElement;
		this.includingFrom = includingFrom;
		this.toElement = toElement;
		this.includingTo = includingTo;
	}

	@Override
	public Comparator<? super E> comparator() {
		return raw.comparator();
	}

	protected Comparator<? super E> checkComparator() {
		return SortedSetSupport.checkComparator(comparator(), fromElement == null ? toElement : fromElement);
	}

	@Override
	public SubSet<E> subSet(final E fromElement, final boolean includingFrom, final E toElement,
													final boolean includingTo) {
		final var comparator = checkComparator();
		if (comparator.compare(fromElement, toElement) > 0) {
			throw new IllegalArgumentException("fromElement > toElement");
		}
		if (this.fromElement != null) {
			if (comparator.compare(fromElement, this.fromElement) < 0 || !this.includingFrom && this.fromElement.equals(
					fromElement)) {
				throw new IllegalArgumentException("fromElement out of range");
			}
		}
		if (this.toElement != null) {
			if (comparator.compare(toElement, this.toElement) > 0 || !this.includingTo && this.toElement.equals(toElement)) {
				throw new IllegalArgumentException("toElement out of range");
			}
		}
		return new SubSet<>(raw, fromElement, includingFrom, toElement, includingTo);
	}

	@Override
	public Iterator<E> iterator() {
		final var comparator = checkComparator();
		final var iterator = raw.iterator();
		E first = null;
		while (iterator.hasNext()) {
			final var e = iterator.next();
			if (outOfFromElement(comparator, e)) {
				if (outOfToElement(comparator, e)) {
					break;
				}
				first = e;
				break;
			}
		}
		if (first == null) {
			return EmptyIterator.instance();
		} else {
			return new SubSetIterator(first, iterator);
		}
	}

	private boolean outOfFromElement(final Comparator<? super E> comparator, final E e) {
		final var compareFrom = comparator.compare(e, fromElement);
		return compareFrom == 0 && includingFrom || compareFrom > 0;
	}

	private boolean outOfToElement(final Comparator<? super E> comparator, final E e) {
		final var compareTo = comparator.compare(e, toElement);
		return compareTo == 0 && !includingTo || compareTo > 0;
	}

	@Override
	public int size() {
		return SortedSetSupport.super.size();
	}

	protected class SubSetIterator implements Iterator<E> {
		protected final Iterator<E> rawIterator;
		protected E cur;

		protected SubSetIterator(final E first, final Iterator<E> rawIterator) {
			if (first == null) {
				throw new NullValException("first");
			}
			cur = first;
			this.rawIterator = rawIterator;
		}

		@Override
		public boolean hasNext() {
			return cur != null;
		}

		@Override
		public E next() {
			if (cur == null) {
				throw new NoSuchElementException();
			}
			final var rt = cur;
			if (rawIterator.hasNext()) {
				final var rawNext = rawIterator.next();
				if (outOfToElement(nonNullComparator(), rawNext)) {
					cur = null;
				} else {
					cur = rawNext;
				}
			} else {
				cur = null;
			}
			return rt;
		}

		@Override
		public void remove() {
			raw.remove(cur);
		}
	}//class SubSetIterator
}//class SubSet
