/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

/**
 * @author Liang Wenjian
 */
public class WrongArgException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 8982825937274932409L;

	private final String argName;
	private final Object actualVal;
	private final Object expectVal;

	public WrongArgException(final String argName, final Object actualVal) {
		this(argName, actualVal, null);
	}

	public WrongArgException(final String argName, final Object actualVal, final Throwable cause) {
		this(argName, actualVal, null, cause);
	}

	public WrongArgException(final String argName, final Object actualVal, final Object expectVal) {
		super("Wrong argument of '" + argName + "' : " + actualVal);
		this.argName = argName;
		this.actualVal = actualVal;
		this.expectVal = expectVal;
	}

	public WrongArgException(final String argName, final Object actualVal, final Object expectVal,
													 final Throwable cause) {
		this(argName, actualVal, expectVal);
		initCause(cause);
	}

	@Override
	public final synchronized Throwable initCause(final Throwable cause) {
		return super.initCause(cause);
	}

	public String getArgName() {
		return argName;
	}

	public Object getActualVal() {
		return actualVal;
	}

	public Object getExpectVal() {
		return expectVal;
	}
}
