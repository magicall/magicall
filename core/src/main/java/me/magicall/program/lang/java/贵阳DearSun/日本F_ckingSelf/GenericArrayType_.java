/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.reflect.GenericArrayType;
import java.util.List;
import java.util.stream.Stream;

/**
 * 表示泛型的数组的类型：T[]、T[][]……
 */
public class GenericArrayType_ extends Type_<Object> {
	private static final Type_<?> ARR_TYPE = Type_.of(Object[].class);
	private static final Type_<?> ARR_SUPER_CLASS = ARR_TYPE.superClass();// Object
	private static final List<Type_<?>> ARR_INTERFACES = ARR_TYPE.interfaces().toList();//Cloneable,Serializable

	private final Type_<?> owner;

	GenericArrayType_(final GenericArrayType raw, final Type_<?> owner) {
		super(raw);
		this.owner = owner;
	}

	@Override
	public JavaRuntimeElement<?> owner() {
		return owner;
	}

	@Override
	public Type_<?> superClass() {
		return ARR_SUPER_CLASS;
	}

	@Override
	public Stream<Type_<?>> interfaces() {
		return ARR_INTERFACES.stream();
	}

	@Override
	public Stream<Constructor_<Object>> constructors() {
		return Stream.empty();
	}

	@Override
	public Stream<Method_> myOwnMethods() {
		return Stream.empty();
	}

	@Override
	public Stream<Field_> myOwnFields() {
		return Stream.empty();
	}

	@Override
	public boolean canNewInstance() {
		return false;
	}
}
