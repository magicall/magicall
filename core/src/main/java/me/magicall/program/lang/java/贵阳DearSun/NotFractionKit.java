/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 整数工具类的父类。
 */
abstract class NotFractionKit<N extends Number & Comparable<N>, A> //
		extends NumKit<N, A> {
	@Serial
	private static final long serialVersionUID = -4160614628198930694L;

	@Override
	public <T1, T2> N emptyVal() {
		return fromLong(0);
	}

	private static boolean maskTest0(final long bigLongValue, final long smallLongValue) {
		return (bigLongValue & smallLongValue) == smallLongValue;
	}

	public boolean maskTest(final N big, final N small) {
		return maskTest0(big.longValue(), small.longValue());
	}

	/**
	 * 按位与检测待测数是否包含材料数组中的每一个.
	 *
	 * @param big 待测数
	 * @param smalls 材料数组。
	 * @return 是否包含。若材料数组为空，则认为被包含，返回true。
	 */
	@SafeVarargs
	public final boolean maskTest(final N big, final N... smalls) {
		return smalls == null || maskTest0(big.longValue(), Arrays.stream(smalls));
	}

	/**
	 * 按位与检测待测数是否包含材料数组中的每一个.
	 *
	 * @param big 待测数
	 * @param smalls 材料数组。
	 * @return 是否包含。若材料数组为空，则认为被包含，返回true。
	 */
	public boolean maskTest(final N big, final A smalls) {
		return smalls == null//
				|| maskTest0(big.longValue(), IntStream.range(0, arrLen(smalls)).mapToObj(i -> valAt(smalls, i)));
	}

	private boolean maskTest0(final long bigLongValue, final Stream<N> stream) {
		return stream.mapToLong(Number::longValue).allMatch(smallLongValue -> maskTest0(bigLongValue, smallLongValue));
	}

	@SafeVarargs
	public final N and(final N source, final N... masks) {
		if (masks == null) {
			return source;
		}
		var rt = source;
		for (final var n : masks) {
			rt = and(rt, n);
		}
		return rt;
	}

	public N and(final N source, final A masks) {
		if (masks == null) {
			return source;
		}
		final var len = arrLen(masks);
		var rt = source;
		for (var i = 0; i < len; ++i) {
			rt = and(rt, valAt(masks, i));
		}
		return rt;
	}

	@SafeVarargs
	public final N or(final N source, final N... ns) {
		if (ns == null) {
			return source;
		}
		var rt = source;
		for (final var n : ns) {
			rt = or(rt, n);
		}
		return rt;
	}

	public N or(final N source, final A ns) {
		if (ns == null) {
			return source;
		}
		final var len = arrLen(ns);
		var rt = source;
		for (var i = 0; i < len; ++i) {
			rt = or(rt, valAt(ns, i));
		}
		return rt;
	}

	public N and(final N source, final N mask) {
		return fromLong(source.longValue() & mask.longValue());
	}

	public N or(final N source, final N n) {
		return fromLong(source.longValue() | n.longValue());
	}

	public N not(final N source) {
		return fromLong(~source.longValue());
	}
}
