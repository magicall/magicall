/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 专用于subTree()这个方法返回的类
 *
 * @author Administrator
 */
class SubTree<E> extends TreeTemplate<E> {

	private final SubTreeRootNode root;
	private final int layerIndex;

	SubTree(final TreeNode<E> rootNode) {
		layerIndex = rootNode.getLayer();
		root = new SubTreeRootNode(rootNode);
	}

	@Override
	public TreeNode<E> getRoot() {
		return root;
	}

	/**
	 * SubTree专用的节点类，包装原来树的节点。
	 */
	private class SubTreeNode extends TreeNodeWrapper<E> {

		SubTreeNode(final TreeNode<E> raw) {
			super(raw);
		}

		@Override
		public TreeNode<E> parent() {
			final var parent = raw.parent();
			if (root.unwrap().equals(parent)) {
				return root;
			}
			return wrap(parent);
		}

		@Override
		public boolean isRoot() {
			return false;//子类SubTreeRootNode返回true。
		}

		@Override
		public Stream<? extends TreeNode<E>> children() {
			return raw.children().map(this::wrap);
		}

		@Override
		public Collection<TreeNode<E>> getLeafNodes() {
			return raw.getLeafNodes().stream().map(this::wrap).collect(Collectors.toList());
		}

		@Override
		public Collection<TreeNode<E>> getDescendants() {
			return raw.getDescendants().stream().map(this::wrap).collect(Collectors.toList());
		}

		@Override
		public Collection<TreeNode<E>> getSiblings() {
			return raw.getSiblings().stream().map(this::wrap).collect(Collectors.toList());
		}

		@Override
		public List<TreeNode<E>> pathFromRoot() {
			return raw.pathFromRoot().stream().map(this::wrap).collect(Collectors.toList());
		}

		@Override
		public List<TreeNode<E>> pathToRoot() {
			return raw.pathToRoot().stream().map(this::wrap).collect(Collectors.toList());
		}

		@Override
		public int getLayer() {
			return raw.getLayer() - layerIndex;
		}

		private SubTreeNode wrap(final TreeNode<E> raw) {
			return new SubTreeNode(raw);
		}
	} //SubTreeNode

	/**
	 * SubTree专用的根节点类。
	 */
	private class SubTreeRootNode extends SubTreeNode {

		private SubTreeRootNode(final TreeNode<E> rootNode) {
			super(rootNode);
		}

		@Override
		public Collection<TreeNode<E>> getSiblings() {
			return Kits.COLL.emptyVal();
		}

		@Override
		public boolean isRoot() {
			return true;
		}

		@Override
		public List<TreeNode<E>> pathFromRoot() {
			return Kits.LIST.emptyVal();
		}

		@Override
		public List<TreeNode<E>> pathToRoot() {
			return Kits.LIST.emptyVal();
		}

		@Override
		public Tree<E> treeFromMe() {
			return SubTree.this;
		}

		@Override
		public TreeNode<E> parent() {
			return null;
		}

		@Override
		public int getLayer() {
			return 0;
		}
	}
} //SubTree
