/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.Function;

public interface CompareKit {

	Comparator<Object> HASH_CODE_COMPARATOR = (o1, o2) -> Objects.equals(o1, o2) ? 0 : Integer.compare(o1.hashCode(),
			o2.hashCode());

	@SuppressWarnings("unchecked")
	static <T> Comparator<T> cast(final Comparator<? super T> c) {
		return (Comparator<T>) c;
	}

	static <F, T extends Comparable<T>> Comparator<F> keyAscNullLast(final Function<F, T> keyExtractor) {
		return Comparator.comparing(keyExtractor, Comparator.nullsLast(Comparator.naturalOrder()));
	}

	static <F, T extends Comparable<T>> Comparator<F> keyDescNullLast(final Function<F, T> keyExtractor) {
		return Comparator.comparing(keyExtractor, Comparator.nullsLast(Comparator.reverseOrder()));
	}
}
