/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.coll.Tree.TreeNode;

import java.util.stream.Stream;

@FunctionalInterface
public interface TreeWalker {

	<E> Stream<TreeNode<E>> walk(Tree<E> tree);

	static TreeWalker deepFirst() {
		return new TreeWalker() {
			@Override
			public <E> Stream<TreeNode<E>> walk(final Tree<E> tree) {
				return NodeWalker.deepFirst().walk(tree.getRoot());
			}
		};
	}

	static TreeWalker wideFirst() {
		return new TreeWalker() {
			@Override
			public <E> Stream<TreeNode<E>> walk(final Tree<E> tree) {
				return NodeWalker.wideFirst().walk(tree.getRoot());
			}
		};
	}
}
