/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import com.google.common.collect.Lists;
import me.magicall.Parent;

import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 节点漫步器。
 * 给定一个节点作为当前节点，指定一个“获得下一批节点”函数，本接口对象将生成一个节点流（{@link Stream}），可以遍历所有节点。
 * 不仅仅可以用于遍历一般的树形结构，也可以遍历倒树形结构、图。
 * 默认提供广度优先和深度优先两种。也可自行实现其他遍历模式。
 */
@FunctionalInterface
public interface NodeWalker {
	<T> Stream<T> walk(T node, Function<T, Stream<? extends T>> nextNodesSupplier);

	default <T extends Parent<T>> Stream<T> walk(final T node) {
		return walk(node, Parent::children);
	}

	//=============================================

	NodeWalker DEEP_FIRST = new NodeWalker() {
		@Override
		public <T> Stream<T> walk(final T node, final Function<T, Stream<? extends T>> nextNodesSupplier) {
			final LinkedList<T> waiting = Lists.newLinkedList();
			return Stream.iterate(node,//seed
					Objects::nonNull,//has next。注意是先判断hasNext，然后才消费node，与for一致
					curNode -> {
						waiting.addAll(0, nextNodesSupplier.apply(curNode).collect(Collectors.toList()));
						return waiting.poll();
					});
		}
	};

	NodeWalker WIDE_FIRST = new NodeWalker() {
		@Override
		public <T> Stream<T> walk(final T node, final Function<T, Stream<? extends T>> nextNodesSupplier) {
			final LinkedList<T> waiting = Lists.newLinkedList();
			return Stream.iterate(node,//seed
					curNode -> curNode != null || !waiting.isEmpty(),//has next
					curNode -> {//next
						//不需要判空，因为若列表为空，则has next就会返回false。
						nextNodesSupplier.apply(curNode).forEach(waiting::add);
						return waiting.poll();
					});
		}
	};

	static NodeWalker deepFirst() {
		return DEEP_FIRST;
	}

	static NodeWalker wideFirst() {
		return WIDE_FIRST;
	}
}
