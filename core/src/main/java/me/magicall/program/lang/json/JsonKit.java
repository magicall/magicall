/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.json;

/**
 * @author Liang Wenjian.
 */
public interface JsonKit {
	String EMPTY_OBJ = "{}";
	String EMPTY_ARR = "[]";
	char K_V_SEPARATOR_CHAR = ':';
	String K_V_SEPARATOR = String.valueOf(K_V_SEPARATOR_CHAR);
	char ELEMENT_SEPARATOR_CHAR = ',';
	String ELEMENT_SEPARATOR = String.valueOf(ELEMENT_SEPARATOR_CHAR);
	char KEY_QUOTER_CHAR = '"';
	String KEY_QUOTER = String.valueOf(KEY_QUOTER_CHAR);
}
