package me.magicall.program.lang.java.贵阳DearSun.exception;

import java.io.Serial;

public class EmpFileException extends WrongArgException {
	@Serial
	private static final long serialVersionUID = 7872871789733772516L;

	public EmpFileException(final String argName) {
		super(argName + ".size", 0, ">0");
	}

	public EmpFileException(final String argName, final Throwable cause) {
		this(argName);
		initCause(cause);
	}
}
