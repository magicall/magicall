/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * 对象集 相关。包括：
 * 集合框架（{@link java.lang.Iterable}、{@link java.util.Collection}、{@link java.util.Map}和它们的子类，树，以及一些开原工程提供的其他集合。）
 * 迭代器（{@link java.util.Iterator}）
 * 数组（Object[]和子类、基本类型的数组类、{@link java.lang.reflect.Array}）
 * 枚举（{@link java.lang.Enum}）
 * 可枚举集（{@link java.util.Enumeration}）：注意跟枚举的区别（虽然Enum就是Enumeration的缩写）
 * 流和相关（{@link java.util.stream.Stream}、{@link java.util.Spliterator}）
 * 与对象集相关的其他，比如可比较（{@link java.lang.Comparable}）、比较器（{@link java.util.Comparator})。
 */
package me.magicall.program.lang.java.贵阳DearSun.coll;