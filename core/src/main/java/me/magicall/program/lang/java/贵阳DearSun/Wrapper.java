/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

/**
 * 实现此接口，表示包装了另一个对象，两者实现了同一个接口，实现类将代理包装对象的方法。
 * 装饰模式
 *
 * @author MaGicalL
 */
@FunctionalInterface
public interface Wrapper<T> {

	T unwrap();

	@SuppressWarnings("unchecked")
	static <T, F extends T> F unwrapTo(final Wrapper<T> wrapper, final Class<F> clazz) {
		for (Object o = wrapper; o != null; ) {
			if (clazz.isInstance(o)) {
				return (F) o;
			}
			if (o instanceof final Wrapper<?> w) {
				o = w.unwrap();
			} else {
				return null;
			}
		}
		return null;
	}
}
