/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Kit;

import java.io.Serial;
import java.util.Collection;
import java.util.Collections;

public abstract class AbsCollKit<C extends Collection<?>> extends Kit<C> {
	@Serial
	private static final long serialVersionUID = 9218029105407906234L;

	/**
	 * <code>java.util.Collections.UnmodifiableCollection</code>的Class对象，由于该类不可见，只能通过这个方法获得它的Class对象
	 */
	protected static final Class<?> JDK_UNMODIFIABLE_COLLECTION_CLASS//
			= Collections.unmodifiableCollection(Collections.emptyList()).getClass();

	//--------------------------------------

	/**
	 * 判断一个Collection是否为“空”。空的定义为null或没有元素
	 */
	@Override
	public boolean isEmpty(final C source) {
		return source == null || source.isEmpty();
	}

	/**
	 * 检测target是否source的真子集
	 */
	@Override
	public boolean greater(final C source, final C target) {
		return source.size() > target.size() && source.containsAll(target);
	}

	/**
	 * 检测target是否source的子集
	 */
	@Override
	public boolean greaterEquals(final C source, final C target) {
		return source.size() >= target.size() && source.containsAll(target);
	}

	/**
	 * 将一个来历不明的集合的泛型强转成另一种类型的集合泛型.
	 * 注意:这是"非安全的"，适用场合：当使用者很清楚地确定source（是一个Collection&lt;S&gt;）中的S是T的父类，用此方法帮助做强制类型转换，可少写很多恶心代码
	 *
	 * @param <T> 集合的元素类型。
	 * @param source 源集合。
	 * @return 强转的集合。
	 */
	public <T> C cast(final C source) {
		return source;
	}

	/**
	 * {@link Collection}的addAll方法返回为void，无法做成链式调用。本方法就是为了链式调用的。
	 *
	 * @param target 目标列表
	 * @param other 另一个列表
	 * @param <T> 元素的类型
	 * @param <A> 集合的类型
	 * @return 添加了另一个列表的所有元素的目标列表。
	 */
	public <T, A extends Collection<T>> A addAll(final A target, final Collection<? extends T> other) {
		target.addAll(other);
		return target;
	}
}
