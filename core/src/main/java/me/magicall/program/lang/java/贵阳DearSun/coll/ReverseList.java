/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.util.AbstractList;
import java.util.List;

/**
 * 翻转的列表。
 * 若原列表太大，真的翻转一次太浪费，可以用本类。
 *
 * @param <E> 元素的类型。
 */
public class ReverseList<E> extends AbstractList<E> implements List<E>, Wrapper<List<E>>, Sorted {

	private final List<E> source;

	public ReverseList(final List<E> source) {
		this.source = source;
	}

	@Override
	public E get(final int index) {
		return source.get(indexMapping(index));
	}

	@Override
	public int size() {
		return source.size();
	}

	@Override
	public List<E> unwrap() {
		return source;
	}

	@Override
	public void add(final int index, final E element) {
		source.add(indexMapping(index), element);
	}

	@Override
	public E set(final int index, final E element) {
		return source.set(indexMapping(index), element);
	}

	@Override
	public E remove(final int index) {
		return source.remove(indexMapping(index));
	}

	private int indexMapping(final int index) {
		return size() - index - 1;
	}
}
