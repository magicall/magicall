/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;

public class Field_ extends TypeMember<Field> {
	private final Type_<Object> elementType;

	public Field_(final Field raw) {
		super(raw);
		elementType = Type_.of(raw.getGenericType());
	}

	@Override
	public Type_<?> elementType() {
		return elementType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V> V tryToFetchVal(final Object... obj) {
		try {
			if (isStatic()) {
				return (V) FieldUtils.readField(unwrap(), (Object) null, true);
			} else {
				if (obj.length == 0) {
					throw new NullValException("caller");
				} else {
					return (V) FieldUtils.readField(unwrap(), obj[0], true);
				}
			}
		} catch (final IllegalAccessException e) {
			throw new UnknownException(e);
		}
	}

	/**
	 * 尝试给某目标对象设置其本字段的值。
	 *
	 * @return
	 */
	public boolean tryToSetVal(final Object target, final Object val) {
		try {
			FieldUtils.writeField(unwrap(), target, val, true);
			return true;
		} catch (final IllegalAccessException e) {
			return false;
		}
	}
}
