/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * 一个奇怪的工厂
 *
 * @author MaGicalL
 */
@Deprecated
public class CollFactory {

	@Deprecated
	public static class I {

		public static <E> Iterator<E> comboIterator(final Collection<? extends Iterable<? extends E>> source) {
			return new ComboIterator<>(Kits.COLL.cast(source));
		}

		public static <E> ListIterator<E> nCopy(final int length, final E element) {
			if (length < 0) {
				throw new IllegalArgumentException("length < 0.it's " + length);
			} else if (length == 0) {
				return Collections.emptyListIterator();
			}
			return new NCopyIterator<>(element, length);
		}

		private static final class NCopyIterator<E> implements ListIterator<E> {

			private final E element;
			private final int len;
			private int curIndex;

			NCopyIterator(final E element, final int len) {
				super();
				this.element = element;
				this.len = len;
				curIndex = 0;
			}

			@Override
			public boolean hasNext() {
				return curIndex < len;
			}

			@Override
			public E next() {
				if (hasNext()) {
					curIndex++;
					return element;
				} else {
					throw new NoSuchElementException();
				}
			}

			@Override
			public boolean hasPrevious() {
				return curIndex > 0;
			}

			@Override
			public E previous() {
				if (hasPrevious()) {
					curIndex--;
					return element;
				} else {
					throw new NoSuchElementException();
				}
			}

			@Override
			public int nextIndex() {
				return curIndex + 1;
			}

			@Override
			public int previousIndex() {
				return curIndex - 1;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public void set(final E e) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void add(final E e) {
				throw new UnsupportedOperationException();
			}
		}

		private static final class ComboIterator<E> implements Iterator<E> {

			private final Collection<Iterable<E>> source;
			private Iterator<Iterable<E>> curIterable;
			private Iterator<E> curElement;

			private ComboIterator(final Collection<Iterable<E>> source) {
				super();
				this.source = source;
			}

			private Iterator<E> curElement() {
				if (curElement == null) {
					final var curIterator = curIterable();
					if (curIterator == null) {
						return null;
					} else {
						curElement = curIterator.next().iterator();
					}
				}
				return curElement;
			}

			private Iterator<Iterable<E>> curIterable() {
				if (curIterable == null) {
					if (source.isEmpty()) {
						return null;
					} else {
						curIterable = source.iterator();
					}
				}
				return curIterable;
			}

			@Override
			public boolean hasNext() {
				final var curElement = curElement();
				if (curElement == null) {
					return false;
				}
				if (curElement.hasNext()) {
					return true;
				}
				final var curIterable = curIterable();
				if (curIterable.hasNext()) {
					this.curElement = curIterable.next().iterator();
					return this.curElement.hasNext();
				}
				return false;
			}

			@Override
			public E next() {
				return curElement.next();
			}
		}
	}
}
