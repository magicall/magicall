/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import me.magicall.Thing;

import java.io.Serial;

/**
 * 操作不可行异常。通常是由已知原因引起，比如权限限制。
 *
 * @author Liang Wenjian
 */
public class OpNotAllowedException extends RuntimeException {
	@Serial
	private static final long serialVersionUID = -6336035506223253531L;

	public static final Thing UNKNOWN_THING = Thing.of("?", "?");

	private static String buildMsg(final Thing operator, final String opName, final Thing target, final Object refVal) {
		if (refVal == null) {
			return "\uD83D\uDEAB " + operator + " - " + opName + " → " + target;
		}
		return "\uD83D\uDEAB " + operator + " : " + target + " - " + opName + " → " + refVal;
	}

	public final Thing operator;
	public final String opName;
	public final Thing target;
	public final Object refVal;

	public OpNotAllowedException(final Thing operator, final String opName, final Thing target, final Object refVal,
															 final Throwable cause) {
		super(buildMsg(operator, opName, target, refVal));
		this.operator = operator;
		this.opName = opName;
		this.target = target;
		this.refVal = refVal;
		if (cause != null) {
			initCause(cause);
		}
	}

	public OpNotAllowedException(final Thing operator, final String opName, final Thing target, final Throwable cause) {
		this(operator, opName, target, null, cause);
	}

	public OpNotAllowedException(final Thing operator, final String opName, final Throwable cause) {
		this(operator, opName, null, cause);
	}

	public OpNotAllowedException(final Thing operator, final String opName, final Thing target, final Object refVal) {
		this(operator, opName, target, refVal, null);
	}

	public OpNotAllowedException(final Thing operator, final String opName, final Thing target) {
		this(operator, opName, target, null);
	}

	public OpNotAllowedException(final Thing operator, final String opName) {
		this(operator, opName, (Thing) null);
	}

	@Override//为了加个final修饰符。
	public final synchronized Throwable initCause(final Throwable cause) {
		return super.initCause(cause);
	}
}
