/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.SortedSet;

/**
 * @author Liang Wenjian.
 */
public class NavigableSubSet<E> extends SubSet<E> implements NavigableSetSupport<E> {

	public NavigableSubSet(final SortedSet<E> raw, final E fromElement, final boolean includingFrom, final E toElement,
												 final boolean includingTo) {
		super(raw, fromElement, includingFrom, toElement, includingTo);
	}

	@Override
	public NavigableSubSet<E> subSet(final E fromElement, final boolean includingFrom, final E toElement,
																	 final boolean includingTo) {
		return new NavigableSubSet<>(raw, fromElement, includingFrom, toElement, includingTo);
	}
}
