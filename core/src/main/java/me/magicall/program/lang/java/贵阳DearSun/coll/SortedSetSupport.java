/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.SortedSet;

/**
 * @author Liang Wenjian.
 */
public interface SortedSetSupport<E> extends SortedSet<E> {

	/**
	 * 默认实现是挨个数。建议子类重写。
	 *
	 * @return 元素个数
	 */
	@Override
	default int size() {
		var rt = 0;
		for (final var ignored : this) {
			rt++;
			if (rt == Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			}
		}
		return rt;
	}

	@Override
	default boolean isEmpty() {
		return size() == 0;
	}

	@Override
	default SortedSet<E> headSet(final E toElement) {
		return subSet(null, true, toElement, false);
	}

	@Override
	default SortedSet<E> tailSet(final E fromElement) {
		return subSet(fromElement, true, null, true);
	}

	@Override
	default SortedSet<E> subSet(final E fromElement, final E toElement) {
		return subSet(fromElement, true, toElement, false);
	}

	default SortedSet<E> subSet(final E fromElement, final boolean includingFrom, final E toElement,
															final boolean includingTo) {
		return new SubSet<>(this, fromElement, includingFrom, toElement, includingTo);
	}

	@Override
	default E first() {
		final var e = firstOrNull();
		if (e == null) {
			throw new NoSuchElementException();
		}
		return e;
	}

	default E firstOrNull() {
		final var iterator = iterator();
		return iterator.hasNext() ? iterator.next() : null;
	}

	@Override
	default E last() {
		final var e = lastOrNull();
		if (e == null) {
			throw new NoSuchElementException();
		}
		return e;
	}

	/**
	 * 通过遍历来查找的，所以建议子类重写。
	 *
	 * @return 最后一个元素。若散列表为空则返回null。
	 */
	default E lastOrNull() {
		E next = null;
		for (final var e : this) {
			next = e;
		}
		return next;
	}

	default Comparator<? super E> nonNullComparator() {
		return nonNullComparator(this);
	}

	@SuppressWarnings("unchecked")
	static <E> Comparator<? super E> nonNullComparator(final SortedSet<E> sortedSet) {
		final var comparator = sortedSet.comparator();
		return comparator == null ? (Comparator<? super E>) Comparator.naturalOrder() : comparator;
	}

	static <E> Comparator<? super E> checkComparator(final Comparator<? super E> comparator, final E element) {
		if (comparator == null) {
			if (!(element instanceof Comparable<?>)) {
				throw new ClassCastException();
			}
			return (Comparator) Comparator.naturalOrder();
		}
		return comparator;
	}

	/**
	 * 返回sortedSet中有几个元素在target之前。
	 * 相当于若把target插入sortedSet，其将出现在Iterator遍历中的第几个元素，返回的最大值&lt;=size()。
	 *
	 * @param sortedSet 散列表
	 * @param target 目标对象
	 * @param <E> 元素的类型
	 * @return 排在目标对象之前的元素的数量。
	 */
	static <E> int countElementsStrictBefore(final SortedSet<E> sortedSet, final E target) {
		return countElementsBefore(sortedSet, target, true);
	}

	/**
	 * 返回sortedSet中有几个元素在target之前，由includingTarget指定是否包含target本身。
	 * 相当于若把target插入sortedSet，其将出现在Iterator遍历中的第几个元素，返回的最大值&lt;=size()。
	 *
	 * @param sortedSet 散列表
	 * @param target 目标对象
	 * @param includingTarget 是否包含目标的数量
	 * @param <E> 元素的类型
	 * @return 排在目标对象之前的元素的数量。
	 */
	static <E> int countElementsBefore(final SortedSet<E> sortedSet, final E target, final boolean includingTarget) {
		final var comparator = checkComparator(sortedSet.comparator(), target);
		return (int) sortedSet.stream().mapToInt(e -> comparator.compare(e, target))
				.takeWhile(compareResult -> (compareResult != 0 || includingTarget) && compareResult <= 0).count();
	}
}
