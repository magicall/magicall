/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class NegativeShortException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = 6310152371104405571L;
	private static final Range<Short> AVAILABLE_RANGE = Range.closed((short) 0, Short.MAX_VALUE);

	public NegativeShortException(final String argName, final short actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public NegativeShortException(final String argName, final short actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public NegativeShortException(final String argName, final short actualVal, final short maxAvailable) {
		super(argName, actualVal, Range.closed((short) 0, maxAvailable));
	}

	public NegativeShortException(final String argName, final short actualVal, final short maxAvailable,
																final Throwable cause) {
		this(argName, actualVal, maxAvailable);
		initCause(cause);
	}
}
