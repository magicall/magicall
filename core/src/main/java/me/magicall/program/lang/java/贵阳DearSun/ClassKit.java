/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.AccessLv;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.MethodKit;
import me.magicall.text.Text;
import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.math.BigDecimal;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ClassKit {
	private static final char ARR_CLASS_START_CHAR = '[';
	private static final char OBJ_ARR_FLAG_CHAR = 'L';
	private static final char OBJ_ARR_CLASS_END_CHAR = ';';
	private static final Range<Integer> AVAILABLE_DIM_RANGE = Range.closed(1, Integer.MAX_VALUE);
	public static final String FILE_SUFFIX = ".class";
	public static final int FILE_SUFFIX_LEN = FILE_SUFFIX.length();

	private ClassKit() {
	}

	//=========================================修饰符

	public static AccessLv accessLvOf(final int modifierFlag) {
		return AccessLv.of(modifierFlag);
	}

	public static AccessLv accessLvOf(final Class<?> c) {
		return c == null ? null : AccessLv.of(c.getModifiers());
	}

	public static AccessLv accessLvOf(final Member member) {
		return member == null ? null : AccessLv.of(member.getModifiers());
	}

	public static boolean isPublic(final int modifierFlag) {
		return accessLvOf(modifierFlag) == AccessLv.PUBLIC;
	}

	public static boolean isPublic(final Class<?> c) {
		return c != null && isPublic(c.getModifiers());
	}

	public static boolean isPublic(final Member member) {
		return member != null && isPublic(member.getModifiers());
	}

	public static boolean isDefaultAccess(final int modifierFlag) {
		return accessLvOf(modifierFlag) == AccessLv.DEFAULT;
	}

	public static boolean isDefaultAccess(final Class<?> c) {
		return c != null && isDefaultAccess(c.getModifiers());
	}

	public static boolean isDefaultAccess(final Member member) {
		return member != null && isDefaultAccess(member.getModifiers());
	}

	public static boolean isProtected(final int modifierFlag) {
		return accessLvOf(modifierFlag) == AccessLv.PROTECTED;
	}

	public static boolean isProtected(final Class<?> c) {
		return c != null && isProtected(c.getModifiers());
	}

	public static boolean isProtected(final Member member) {
		return member != null && isProtected(member.getModifiers());
	}

	public static boolean isPrivate(final int modifierFlag) {
		return accessLvOf(modifierFlag) == AccessLv.PRIVATE;
	}

	public static boolean isPrivate(final Class<?> c) {
		return c != null && isPrivate(c.getModifiers());
	}

	public static boolean isPrivate(final Member member) {
		return member != null && isPrivate(member.getModifiers());
	}

	public static boolean isStatic(final int modifierFlag) {
		return Modifier.isStatic(modifierFlag);
	}

	public static boolean isStatic(final Class<?> c) {
		return c != null && isStrictFloatingPoint(c.getModifiers());
	}

	public static boolean isStatic(final Member member) {
		return member != null && isStatic(member.getModifiers());
	}

	public static boolean isFinal(final int modifierFlag) {
		return Modifier.isFinal(modifierFlag);
	}

	public static boolean isFinal(final Class<?> c) {
		return c != null && isFinal(c.getModifiers());
	}

	public static boolean isFinal(final Member member) {
		return member != null && isFinal(member.getModifiers());
	}

	//-------------- abstract 只能用于Class和Method

	public static boolean isAbstract(final int modifierFlag) {
		return Modifier.isAbstract(modifierFlag);
	}

	public static boolean isAbstract(final Class<?> c) {
		return c != null && isAbstract(c.getModifiers());
	}

	public static boolean isAbstract(final Method method) {
		return method != null && isAbstract(method.getModifiers());
	}

	//------------- strictfp 只能用于Class和Method

	public static boolean isStrictFloatingPoint(final int modifierFlag) {
		return Modifier.isStrict(modifierFlag);
	}

	public static boolean isStrictFloatingPoint(final Class<?> c) {
		return c != null && isStrictFloatingPoint(c.getModifiers());
	}

	public static boolean isStrictFloatingPoint(final Member member) {
		return member != null && isStrictFloatingPoint(member.getModifiers());
	}

	//------------- native 只能用于Method

	public static boolean isNative(final int modifierFlag) {
		return Modifier.isNative(modifierFlag);
	}

	public static boolean isNative(final Method method) {
		return method != null && isNative(method.getModifiers());
	}

	//-------------- volatile 只能用于字段

	public static boolean isVolatile(final int modifierFlag) {
		return Modifier.isVolatile(modifierFlag);
	}

	public static boolean isVolatile(final Field field) {
		return field != null && Modifier.isVolatile(field.getModifiers());
	}
	//-------------- transient 只能用于字段

	public static boolean isTransient(final int modifierFlag) {
		return Modifier.isTransient(modifierFlag);
	}

	public static boolean isTransient(final Field f) {
		return f != null && isTransient(f.getModifiers());
	}

	//====================================类型（class/interface）相关
	//-----------------------元信息

	/**
	 * 根据类全名获取class对象。若无则返回null。
	 * 支持以下名字（Class.forName都不支持）：
	 * - 9种基本类型：int long short byte char float double boolean void。
	 * - 内部类的形如a.b.C.D格式的名字（原生的a.b.C$D这种格式也支持）。
	 * - 数组的形如int[]、String[]、int[][]、String[][]的名字。
	 *
	 * @param fullName 类全名
	 * @return {@link Class}对象或null。
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> classCalled(final String fullName) {
		return (Class<T>) switch (fullName) {
			case "int" -> int.class;
			case "long" -> long.class;
			case "short" -> short.class;
			case "byte" -> byte.class;
			case "char" -> char.class;
			case "float" -> float.class;
			case "double" -> double.class;
			case "boolean" -> boolean.class;
			case "void" -> void.class;
			default -> forName1(fullName);
		};
	}

	private static Class<?> forName1(final String fullClassName) {
		// 处理数组类型
		if (fullClassName.endsWith("[]")) {
			return getArrayClass(fullClassName);
		}
		//对于a.b.C的内部类D，Class.forName不识别a.b.C.D这种名字，要求用a.b.C$D这种名字。
		final var c = forName0(fullClassName);
		if (c != null) {
			return c;
		}
		// 从最后一个.开始替换为$，依次尝试加载类
		for (int i = fullClassName.lastIndexOf('.'); i != -1; i = fullClassName.lastIndexOf('.', i - 1)) {
			final String modifiedClassName = fullClassName.substring(0, i) + '$' + fullClassName.substring(i + 1);
			final var c1 = forName0(modifiedClassName);
			if (c1 != null) {
				return c1;
			}
		}
		return null;
	}

	private static Class<?> getArrayClass(final String fullClassName) {
		int arrayDimension = 0;
		var className = fullClassName;
		while (className.endsWith("[]")) {
			arrayDimension++;
			className = className.substring(0, className.length() - 2);
		}
		final Class<?> componentClass = classCalled(className);
		if (componentClass != null) {
			return Array.newInstance(componentClass, new int[arrayDimension]).getClass();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T> Class<T> forName0(final String fullClassName) {
		try {
			return (Class<T>) Class.forName(fullClassName);
		} catch (final ClassNotFoundException ignore) {
			return null;
		}
	}

	/**
	 * 判断参数是否刚好指向一个“类”（而非接口/枚举/注解）
	 *
	 * @param c 类
	 * @return 是否
	 */
	public static boolean isExactClass(final Class<?> c) {
		return c != null && !c.isInterface() && !c.isAnnotation() && !c.isEnum();
	}

	public static boolean isInterface(final Class<?> c) {
		return c != null && c.isInterface();
	}

	/**
	 * 是否匿名类
	 *
	 * @return 是否匿名类
	 */
	public static boolean isAnonymous(final Class<?> c) {
		return c != null && c.isAnonymousClass();
	}

	/**
	 * 是否内部类。
	 * 包括匿名内部类或非匿名内部类（成员类），
	 * 不包括“局部类（local Class）”：在方法/构造器/块里定义的类。
	 *
	 * @return 是否内部类
	 */
	public static boolean isInner(final Class<?> c) {
		//c.isMemberClass()不包含匿名内部类，不能用。
		return c != null && c.getEnclosingClass() != null && !isInMethod(c) && !isInConstructor(c);
	}

	/**
	 * 判断一个类是否在方法中定义的。
	 *
	 * @return
	 */
	public static boolean isInMethod(final Class<?> c) {
		return c != null && c.getEnclosingMethod() != null;
	}

	/**
	 * 判断一个类是否在构造器中定义的。
	 *
	 * @return
	 */
	public static boolean isInConstructor(final Class<?> c) {
		return c != null && c.getEnclosingConstructor() != null;
	}

	/**
	 * 判断一个类是否在初始化块中定义的。
	 *
	 * @return
	 */
	public static boolean isInInitBlock(final Class<?> c) {
		return c != null && c.isLocalClass() && !isInMethod(c) && !isInConstructor(c);
	}

	public static boolean isMultiVal(final Class<?> c) {
		if (c == null) {
			return false;
		}
		return isOrSubOf(c, Iterable.class)//
				|| isOrSubOf(c, Map.class) //
				|| isOrSubOf(c, Stream.class)//
				|| c.isArray()//
				|| isOrSubOf(c, Iterator.class)//
				|| isOrSubOf(c, Enumeration.class);//
	}

	//-------------------------------------特殊类型

	public static boolean isPrimitive(final Class<?> c) {
		return c != null && c.isPrimitive();
	}

	public static boolean isNum(final Class<?> c) {
		return isPrimitiveNum(c) || isOrSubOf(c, Number.class);
	}

	public static boolean isPrimitiveNum(final Class<?> c) {
		return isPrimitive(c) && c != char.class && c != boolean.class && c != void.class;
	}

	public static boolean isVoid(final Class<?> c) {
		return c == void.class || c == Void.class;
	}

	public static boolean isByte(final Class<?> c) {
		return c == byte.class || c == Byte.class;
	}

	public static boolean isShort(final Class<?> c) {
		return c == short.class || c == Short.class;
	}

	public static boolean isInt(final Class<?> c) {
		return c == int.class || c == Integer.class;
	}

	public static boolean isLong(final Class<?> c) {
		return c == long.class || c == Long.class;
	}

	public static boolean isFloat(final Class<?> c) {
		return c == float.class || c == Float.class;
	}

	public static boolean isDouble(final Class<?> c) {
		return c == double.class || c == Double.class;
	}

	/**
	 * 是否数值型，且带有小数。目前仅判断float、double、BigDecimal。含包装类。
	 *
	 * @param c 类
	 * @return 是否
	 */
	public static boolean hasFraction(final Class<?> c) {
		return isFloat(c) || isDouble(c) || isOrSubOf(c, BigDecimal.class);
	}

	public static boolean isBool(final Class<?> c) {
		return c == boolean.class || c == Boolean.class;
	}

	public static boolean isChar(final Class<?> c) {
		return c == char.class || c == Character.class;
	}

	public static boolean isText(final Class<?> c) {
		return isChar(c)//
				|| isOrSubOf(c, CharSequence.class)//
				|| isOrSubOf(c, Text.class);
	}

	/**
	 * 是否与时间有关。目前判断Date、Calendar、Temporal、TemporalAmount。
	 *
	 * @return 是否
	 */
	public static boolean isAboutTime(final Class<?> c) {
		return Stream.of(Date.class, Calendar.class, Temporal.class, TemporalAmount.class).anyMatch(e -> isOrSubOf(c, e));
	}

	public static boolean isDate(final Class<?> c) {
		return isOrSubOf(c, Date.class);
	}

	//	/**
//	 * 是否一个“类枚举”的类。包括本身是枚举类（enum），以及一些这样的类：不可继承（final或无外部可访问的构造方法）且自己提供了若干个确定的实例。
//	 *
//	 * @param c 类
//	 * @return 是否
//	 */
//	public static boolean isEnumLike(final Class<?> c) {
//		//其他情况的实现难度：
//		// 1，即使构造器是private的，内部的其他类和方法依然可以调用，要排除这种工厂模式；
//		// 2，如何判断“自己”预置了若干个实例？
//		return isEnum(c);
//	}

	//-------------------------------------继承关系

	public static <T> Class<? super T> parentOf(final Class<T> c) {
		return c == null ? null : c.getSuperclass();
	}

	public static boolean isOrSubOf(final Class<?> maybeProgeny, final Class<?> maybeAncestor) {
		return maybeAncestor != null && maybeProgeny != null && maybeAncestor.isAssignableFrom(maybeProgeny);
	}

	public static boolean isOrSuperOf(final Class<?> maybeAncestor, final Class<?> maybeProgeny) {
		return maybeAncestor != null && maybeProgeny != null && maybeAncestor.isAssignableFrom(maybeProgeny);
	}

	/**
	 * 获取指定类的所有超类。不含自己。
	 *
	 * @return 本对象所有超类。
	 */
	public static Stream<Class<?>> superClassesOf(final Class<?> c) {
		return Stream.iterate(parentOf(c), Objects::nonNull, Class::getSuperclass);
	}

	/**
	 * 获取参数实现的所有接口，递归（即，包括继承来的接口），去重。
	 *
	 * @param c 类型
	 * @return 接口类型集合
	 */
	public static Stream<Class<?>> interfacesOf(final Class<?> c) {
		if (c == null) {
			return Stream.empty();
		}
		return superClassesOf(c).flatMap(e -> Stream.of(e.getInterfaces())).distinct();
	}

	/**
	 * 判断参数class是否实现过接口。感觉没啥用。
	 *
	 * @param c 类
	 * @return 是否
	 */
	public static boolean hasInterface(final Class<?> c) {
		return interfacesOf(c).findAny().isPresent();
	}

	/**
	 * 所有的祖先类型。包括接口。不包括自己。
	 *
	 * @param c 类
	 * @return 是否
	 */
	@SuppressWarnings("unchecked")
	public static Stream<Class<?>> ancestorsOf(final Class<?> c) {
		return c == null ? Stream.empty() : CollKit.concat(superClassesOf(c), interfacesOf(c));
	}

	@SuppressWarnings("unchecked")
	public static Stream<Class<?>> directParents(final Class<?> c) {
		return c == null ? Stream.empty() : CollKit.concat(Stream.of(c.getSuperclass()), Stream.of(c.getInterfaces()));
	}

	//-------------------------------------包含关系

	/**
	 * 若参数是个内部类，则返回其外部类。否则返回null。
	 *
	 * @param c 类
	 * @return 是否
	 */
	public static Class<?> owner(final Class<?> c) {
		if (c == null) {
			return null;
		}
		final Class<?> declaringClass = c.getDeclaringClass();
		if (declaringClass != null) {
			return declaringClass;
		}
		return c.getEnclosingClass();
	}

	/**
	 * 指定的类的内部类。
	 *
	 * @param c 类
	 * @return 是否
	 */
	public static Stream<Class<?>> memberClasses(final Class<?> c) {
		return c == null ? Stream.empty() : Stream.of(c.getDeclaredClasses());
	}

	//===============================================================对象

	/**
	 * 将对象数组转化成它们的Class对象组成的数组，位置一一对应。
	 *
	 * @param objs 对象
	 * @return 对象的{@link Class}对象。若参数为null，则返回null。
	 */
	public static Class<?>[] toClasses(final Object... objs) {
		return ClassUtils.toClass(objs);
	}

	/**
	 * 检查第二个参数的每一个对象是否都是第一个参数对应位置Class的实例。
	 *
	 * @param superClasses 类型们
	 * @param objects 对象们
	 * @return 是否对应的实例
	 */
	public static boolean checkAllAssignable(final Class<?>[] superClasses, final Object... objects) {
		return checkAllAssignable(superClasses, toClasses(objects));
	}

	/**
	 * 检查第二个参数的每一个Class是否都是第一个参数的对应位置的Class或其子类。
	 *
	 * @param superClasses 超类们
	 * @param subClasses 子类们
	 * @return 是否全部适合
	 */
	public static boolean checkAllAssignable(final Class<?>[] superClasses, final Class<?>... subClasses) {
		if (ArrKit.isEmpty(superClasses)) {
			return ArrKit.isEmpty(subClasses);
		}
		if (ArrKit.isEmpty(subClasses)) {
			return false;
		}
		if (superClasses.length != subClasses.length) {
			return false;
		}
		var index = 0;
		for (final var c : superClasses) {
			if (!fit(c, subClasses[index])) {
				return false;
			}
			++index;
		}
		return true;
	}

	public static boolean fit(final Class<?> c1, final Class<?> c2) {
		//todo：没有检查泛型的上下界
		if (c1 == null || c2 == null) {
			return false;
		}
		return c1.isAssignableFrom(c2)//
				|| c1.isPrimitive() && PRIMITIVE_WRAPPER_MAP.get(c1) == c2//
				|| c2.isPrimitive() && PRIMITIVE_WRAPPER_MAP.get(c2) == c1;
	}

	private static final Map<Class<?>, Class<?>> PRIMITIVE_WRAPPER_MAP//
			= Map.of(boolean.class, Boolean.class,//
			byte.class, Byte.class,//
			char.class, Character.class,//
			double.class, Double.class,//
			float.class, Float.class,//
			int.class, Integer.class,//
			long.class, Long.class,//
			short.class, Short.class,//
			void.class, Void.class);

	//------------------------------------------------------数组

	public static Class<?> arrClass(final String componentTypeName, final int dim) {
		if (dim < 1) {
			throw new WrongArgException("dim", dim, AVAILABLE_DIM_RANGE);
		}
		final String sb = IntStream.iterate(dim, i -> i > 0, i -> i - 1).mapToObj(i -> String.valueOf(ARR_CLASS_START_CHAR))
				.collect(Collectors.joining("", "", componentTypeName));
		return classCalled(sb);
	}

	public static Class<?> arrClass(final Class<?> clazz, final int dim) {
		if (dim < 1) {
			throw new WrongArgException("dim", dim, AVAILABLE_DIM_RANGE);
		}
		final var factDim = clazz.isArray() ? dim + dimOfArrayClass(clazz) : dim;
		final var componentType = componentType(clazz);
		return arrClass(componentType.isPrimitive() ? Kits.getPrimitiveKit(componentType).primitiveArrFlag()
																								: OBJ_ARR_FLAG_CHAR + componentType.getName() + OBJ_ARR_CLASS_END_CHAR,
				factDim);
	}

	/**
	 * 获取数组的元素的类型。支持多维。若非数组，返回原类型。
	 *
	 * @param arrClass 数组{@link Class}
	 * @return 数组的元素的{@link Class}
	 */
	public static Class<?> componentType(final Class<?> arrClass) {
		var c = arrClass;
		while (c.isArray()) {
			c = c.getComponentType();
		}
		return c;
	}

	/**
	 * 给定一个Class对象，如果它表示一个数组类，则返回它所表示的数组的维数，如果它不是，返回0
	 *
	 * @param arrClass 一个{@link Class}
	 * @return 维数
	 */
	public static int dimOfArrayClass(final Class<?> arrClass) {
		return Kits.CHAR.frequency(ARR_CLASS_START_CHAR, arrClass.getName().toCharArray());
	}

//===========================================构造器相关

	@SuppressWarnings("unchecked")
	public static <T> Stream<Constructor<T>> constructorsOf(final Class<T> c) {
		//c.getConstructors()返回的是public的构造器。
		//c.getDeclaredConstructors()返回的是所有构造器。
		return c == null ? Stream.empty() : (Stream) Stream.of(c.getDeclaredConstructors());
	}

	public static <T> Constructor<T> constructorOf(final Class<T> c, final Class<?>... argTypes) {
		final var constructor = constructorStrictFitArgs(c, argTypes);
		return constructor == null ? findCompatibleConstructor(c, argTypes) : constructor;
	}

	public static <T> Constructor<T> findCompatibleConstructor(final Class<T> clazz, final Class<?>... argTypes) {
		@SuppressWarnings("unchecked")
		final var constructors = (Constructor<T>[]) clazz.getConstructors();
		Constructor<T> c = null;
		for (final var constructor : constructors) {
			final var parametersTypes = constructor.getParameterTypes();
			if (parametersTypes.length == argTypes.length) {
				if (Arrays.equals(parametersTypes, argTypes)) {
					return constructor;
				}
				if (checkAllAssignable(parametersTypes, argTypes)) {
					c = constructor;
				}
			}
		}
		return c;
	}

	public static <T> Constructor<T> constructorStrictFitArgs(final Class<T> clazz, final Class<?>... argTypes) {
		try {
			//todo:getConstructor只返回public的
			return clazz.getConstructor(argTypes);
		} catch (final NoSuchMethodException ignored) {
			return null;
		} catch (final SecurityException e) {
			throw new UnknownException(e);
		}
	}

	public static boolean canNewInstance(final Class<?> c) {
		return isExactClass(c) && !Modifier.isAbstract(c.getModifiers());
	}

	@SuppressWarnings("unchecked")
	public static <T> T newInstance(final ClassLoader classLoader, final String className,
																	final Object... constructorArgs) {
		try {
			return (T) newInstance(Class.forName(className, true, classLoader), constructorArgs);
		} catch (final ClassNotFoundException ignored) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T newInstance(final String className, final Object... constructorArgs) {
		try {
			return (T) newInstance(Class.forName(className), constructorArgs);
		} catch (final ClassNotFoundException ignored) {
			return null;
		}
	}

	/**
	 * 创建实例。不会抛出异常，如果创建中有异常，将返回null
	 *
	 * @param <T> 类型
	 * @param clazz {@link Class}对象
	 * @param params 构造器参数列表
	 * @return 新实例或null。
	 */
	public static <T> T newInstance(final Class<T> clazz, final Object... params) {
		final var constructor = constructorOf(clazz, toClasses(params));
		if (constructor != null) {
			try {
				return constructor.newInstance(params);
			} catch (final InstantiationException | IllegalAccessException | IllegalArgumentException |
										 InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

//=========================================泛型相关

	public static int countGenericTypes(final Class<?> c) {
		return 0;//todo
	}

	public static boolean hasGenericTypes(final Class<?> c) {
		//toGenericString 不包含 extends、implements部分，所以一旦有泛型（<开始），肯定是自己的。
		return c != null && c.toGenericString().contains("<");
	}

	public static boolean isImplementingGenericTypes(final Class<?> c) {
		return directParents(c).anyMatch(ClassKit::hasGenericTypes);
	}

	/**
	 * 为指定的类对象构造一个多值映射表，键为其超类，值为指定类对象覆盖的该超类的泛型的具体类型。
	 * 若未覆盖某超类的泛型，则该超类不出现在映射表中。
	 *
	 * @param clazz 类型
	 * @return 超类-泛型的具体类型映射
	 */
	public static ListMultimap<Class<?>, Class<?>> implementedGenericTypes(final Class<?> clazz) {
		final Map<Class<?>, Map<String, Class<?>>> genericTypes = Maps.newLinkedHashMap();
		addImplementedGenericTypes(clazz, genericTypes);
		final ListMultimap<Class<?>, Class<?>> rt = ArrayListMultimap.create();
		genericTypes.forEach((k, v) -> rt.putAll(k, v.values()));
		return rt;
	}

	public static boolean isDefault(final Method method) {
		return method.isDefault();
	}

	private static final Set<Class<?>> BOXING_TYPES = Set.of(Boolean.class, Character.class, Byte.class, Short.class,
			Integer.class, Long.class, Float.class, Double.class);

	public static boolean isBoxing(final Class<?> c) {
		return BOXING_TYPES.contains(c);
	}

	public static boolean isThrowable(final Class<?> c) {
		return isOrSubOf(c, Throwable.class);
	}

	public static boolean isError(final Class<?> c) {
		return isOrSubOf(c, Error.class);
	}

	public static boolean isException(final Class<?> c) {
		return isOrSubOf(c, Exception.class);
	}

	public static boolean isRuntimeException(final Class<?> c) {
		return isOrSubOf(c, RuntimeException.class);
	}

	public static int arrDims(final Type type) {
		Type t = type;
		int dimensions = 0;

		while (true) {
			if (t instanceof final Class<?> clazz) {
				if (clazz.isArray()) {
					dimensions++;
					t = clazz.getComponentType();
				} else {
					break;
				}
			} else if (t instanceof GenericArrayType) {
				dimensions++;
				t = ((GenericArrayType) t).getGenericComponentType();
			} else {
				break;
			}
		}

		return dimensions;
	}

	private static void addImplementedGenericTypes(final Class<?> clazz, final Map<Class<?>, Map<String, Class<?>>> rt) {
		//返回本类/接口直接实现的超接口（即声明里直接写的）
		final var parentInterfaces = clazz.getGenericInterfaces();
		Arrays.stream(parentInterfaces)
				.forEach(parentInterfaceType -> addSuperInterfaceImplementedGenericTypes(clazz, parentInterfaceType, rt));
	}

	private static void addSuperInterfaceImplementedGenericTypes(final Class<?> clazz, final Type parentInterfaceType,
																															 final Map<Class<?>, Map<String, Class<?>>> rt) {
		final Class<?> parentInterfaceClass;
		if (parentInterfaceType instanceof final ParameterizedType paramedParentInterface) { //若此超接口有泛型
			handleTransitivity(clazz, paramedParentInterface, rt);
			parentInterfaceClass = (Class<?>) paramedParentInterface.getRawType();
		} else { //没有泛型，通常是Class
			parentInterfaceClass = (Class<?>) parentInterfaceType;
		}
		addImplementedGenericTypes(parentInterfaceClass, rt);
	}

	private static void handleTransitivity(final Class<?> clazz, final ParameterizedType paramedParentInterface,
																				 final Map<Class<?>, Map<String, Class<?>>> rt) {
		//声明时对父类泛型的赋值。注意本身可能也是一个泛型，得靠子类对本类泛型赋值的传递性。
		final var actualTypeArgs = paramedParentInterface.getActualTypeArguments();
		final var parentClass = (Class<?>) paramedParentInterface.getRawType();
		final var parentTypeParams = parentClass.getTypeParameters();
		assert parentTypeParams.length == actualTypeArgs.length;
		var i = 0;
		for (final var typeParam : parentTypeParams) {
			final var parentGenericParamName = typeParam.getName();//父类泛型形参名
			final var actualTypeArg = actualTypeArgs[i];
			if (actualTypeArg instanceof TypeVariable<?>) { //子类用本身的泛型赋值给父类泛型
				final var actualType = genericParamActualClassFromChild(rt, clazz, (TypeVariable<?>) actualTypeArg);
				if (actualType != null) {
					checkAndPut(rt, parentClass, parentGenericParamName, actualType);
				}
			} else {
				checkAndPut(rt, parentClass, parentGenericParamName, (Class<?>) actualTypeArg);
			}
			i++;
		}
	}

	private static Class<?> genericParamActualClassFromChild(final Map<Class<?>, Map<String, Class<?>>> rt,
																													 final Class<?> clazz, final TypeVariable<?> variableType) {
		final var childGenericParamName = variableType.getName();
		final var genericMapping = rt.get(clazz);
		return genericMapping == null ? null : genericMapping.get(childGenericParamName);
	}

	private static void checkAndPut(final Map<Class<?>, Map<String, Class<?>>> rt, final Class<?> paramedParentRawType,
																	final String genericParamName, final Class<?> actualType) {
		rt.computeIfAbsent(paramedParentRawType, k -> Maps.newLinkedHashMap()).put(genericParamName, actualType);
	}

//=========================================字段相关

	/**
	 * 获取所有字段，包括继承的、静态的。
	 *
	 * @param c 类
	 * @return 字段集。
	 */
	public static Stream<Field> fieldsOf(final Class<?> c) {
		//Class.getDeclaredFields：只拿自己定义的，但包含全部访问级别。
		//Class.getFields：只拿public的，但递归超类、超接口。
		return c == null ? Stream.empty() : CollKit.concat(Stream.of(c.getDeclaredFields()),//
						fieldsOf(parentOf(c)),//
						interfacesOf(c).flatMap(ClassKit::fieldsOf))//
				.distinct();
	}

	public static boolean hasFields(final Class<?> c) {
		return fieldsOf(c).findAny().isPresent();
	}

	public static Stream<Field> ownFieldsOf(final Class<?> c) {
		return c == null ? Stream.empty() : Stream.of(c.getDeclaredFields());
	}

	public static boolean hasOwnFields(final Class<?> c) {
		return ownFieldsOf(c).findAny().isPresent();
	}

	public static Field fieldCalled(final Class<?> c, final String name) {
		if (Kits.STR.isEmpty(name)) {
			return null;
		}
		return fieldsOf(c).filter(e -> e.getName().equals(name)).findFirst().orElse(null);
	}

	public static boolean hasFieldCalled(final Class<?> c, final String name) {
		return fieldCalled(c, name) != null;
	}

	public static Field fieldOf(final Class<?> c, final String name, final Class<?> type) {
		if (type == null) {
			return null;
		}
		final var field = fieldCalled(c, name);
		return field == null || type != field.getType() ? null : field;
	}

	public static boolean hasField(final Class<?> c, final String name, final Class<?> type) {
		return fieldOf(c, name, type) != null;
	}

	public static void callSetter(final Object target, final Object fieldValue) {
		final var fieldClass = fieldValue.getClass();
		final var objClass = target.getClass();
		final var setter = BeanKit.getSetterIgnoreNameCaseAndTypeAssigned(objClass, fieldClass);
		if (setter != null) {
			MethodKit.invoke(target, setter, fieldValue);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T valOf(final Object target, final Field field) {
		try {
			field.setAccessible(true);
			return (T) field.get(target);
		} catch (final IllegalAccessException e) {
			throw new UnknownException(e);
		}
	}

//==========================================方法相关

	/**
	 * 自有方法靠前。
	 *
	 * @param c 类
	 * @return 方法集
	 */
	public static Stream<Method> methodsOf(final Class<?> c) {
		//Class.getDeclaredMethods：只拿自己定义的，但包含全部访问级别。
		//Class.getMethods：只拿public的，但递归超类、超接口。
		return c == null ? Stream.empty() : CollKit.concat(ownMethodsOf(c),//
						methodsOf(parentOf(c)),//
						interfacesOf(c).flatMap(ClassKit::methodsOf))//
				.distinct();
	}

	public static boolean hasMethods(final Class<?> c) {
		return methodsOf(c).findAny().isPresent();
	}

	public static Stream<Method> ownMethodsOf(final Class<?> c) {
		return c == null ? Stream.empty() : Stream.of(c.getDeclaredMethods());
	}

	public static boolean hasOwnMethods(final Class<?> c) {
		return ownMethodsOf(c).findAny().isPresent();
	}

	public static Stream<Method> instanceMethods(final Class<?> c) {
		return methodsOf(c).filter(e -> !isStatic(e));
	}

	public static Stream<Method> staticMethods(final Class<?> c) {
		return methodsOf(c).filter(ClassKit::isStatic);
	}

	/**
	 * 获取一个指定名字的方法，其形参列表的类型匹配参数类型。优先获取类型上最适合的。
	 * 若有多个方法都是“最适合”的，则选择“第一个”。
	 *
	 * @param c 类
	 * @param name 方法名
	 * @param paramTypes 参数类型
	 * @return 方法对象
	 */
	public static Method methodOf(final Class<?> c, final String name, final Class<?>... paramTypes) {
		if (c == null || Kits.STR.isEmpty(name)) {
			return null;
		}
		return methodsOf(c)//
				.filter(e -> e.getName().equals(name)//
						&& checkAllAssignable(e.getParameterTypes(), paramTypes))//
				.findFirst().orElse(null);
	}

	public static boolean hasMethod(final Class<?> c, final String name, final Class<?>... paramTypes) {
		return methodOf(c, name, paramTypes) != null;
	}

	public static Method methodStrictFitArgs(final Class<?> c, final String name, final Class<?>... paramTypes) {
		if (c == null || Kits.STR.isEmpty(name)) {
			return null;
		}
		return methodsOf(c)//
				.filter(e -> e.getName().equals(name)//
						&& Arrays.equals(e.getParameterTypes(), paramTypes))//
				.findFirst().orElse(null);
	}
}
