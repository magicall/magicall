/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class PositiveIntException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = 3614207387873151323L;
	private static final Range<Integer> AVAILABLE_RANGE = Range.closed(Integer.MIN_VALUE, 0);

	public PositiveIntException(final String argName, final int actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public PositiveIntException(final String argName, final int actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public PositiveIntException(final String argName, final int actualVal, final int minAvailable) {
		super(argName, actualVal, Range.closed(minAvailable, 0));
	}

	public PositiveIntException(final String argName, final int actualVal, final int minAvailable,
															final Throwable cause) {
		this(argName, actualVal, minAvailable);
		initCause(cause);
	}
}
