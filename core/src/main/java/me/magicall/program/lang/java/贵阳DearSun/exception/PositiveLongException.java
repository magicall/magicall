/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class PositiveLongException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = -3533992595502164235L;
	private static final Range<Long> AVAILABLE_RANGE = Range.closed(Long.MIN_VALUE, 0L);

	public PositiveLongException(final String argName, final long actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public PositiveLongException(final String argName, final long actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public PositiveLongException(final String argName, final long actualVal, final long minAvailable) {
		super(argName, actualVal, Range.closed(minAvailable, 0L));
	}

	public PositiveLongException(final String argName, final long actualVal, final long minAvailable,
															 final Throwable cause) {
		this(argName, actualVal, minAvailable);
		initCause(cause);
	}
}
