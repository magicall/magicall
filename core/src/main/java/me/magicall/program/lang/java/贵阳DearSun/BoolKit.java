/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class BoolKit extends PrimitiveKit<Boolean, boolean[]> implements Serializable {
	public static final BoolKit INSTANCE = new BoolKit();

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Boolean.class, boolean.class);

	//暂时只有TRUE的各种大小写。
	private static final String[] TRUE_STR = {//
			Boolean.TRUE.toString(),//
	};

	//-----------------------------------------

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Stream<String> supportedTypeNames() {
		return Stream.concat(super.supportedTypeNames(), Stream.of("bool"));
	}

	@Override
	public Boolean parse(final String source) {
		//Boolean.parseBoolean(source); 对大小写敏感，故不用。
		return Arrays.stream(TRUE_STR).anyMatch(s -> s.equalsIgnoreCase(source));
	}

	@Override
	public <T1, T2> Boolean emptyVal() {
		return Boolean.FALSE;
	}

	@Override
	public String primitiveArrFlag() {
		return "Z";
	}

	//================================================

	private Object readResolve() {
		return INSTANCE;
	}

	@Serial
	private static final long serialVersionUID = -3005476553700563936L;
}
