/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.js;

import cn.hutool.core.io.IoUtil;
import cn.hutool.log.LogFactory;
import cn.hutool.script.ScriptUtil;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public interface JsKit {

	static Object callFunction(final String functionName, final Object... args) throws NoSuchMethodException {
		return callFunction(jsEngine(), functionName, args);
	}

	static Object callFunction(final ScriptEngine engine, final String functionName, final Object... args)
			throws NoSuchMethodException {
		try {
			return ((Invocable) engine).invokeFunction(functionName, args);
		} catch (final ScriptException e) {
			LogFactory.get(JsKit.class).warn(e);
			return null;
		}
	}

	static List<Object> eval(final String... jsCodes) {
		final var engine = jsEngine();
		return Arrays.stream(jsCodes)//
				.map(jsCode -> eval(engine, jsCode)).toList();
	}

	static Object eval(final ScriptEngine engine, final InputStream inputStream) {
		return eval(engine, IoUtil.read(inputStream, StandardCharsets.UTF_8));
	}

	static Object eval(final ScriptEngine engine, final String jsCode) {
		//		engine.put("out", System.out);
		try {
			return engine.eval(jsCode);
		} catch (final ScriptException e) {
			LogFactory.get(JsKit.class).warn(e);
			return null;
		}
	}

	static ScriptEngine jsEngine() {
		return ScriptUtil.getJavaScriptEngine();
	}
}
