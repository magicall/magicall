/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.JavaConst;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Magical Liang
 */
public class ArrKit {

	/**
	 * 返回某个类的指定维数的数组的Class对象(Class&lt;T[]&gt;对象)
	 * 注意:低维数组并非高维数组的父类!（String[]不是String[][]的父类）
	 *
	 * @param componentType 元素类型
	 * @param dim 维数
	 * @return 数组的{@link Class}对象
	 */
	public static Class<?> arrClass(final Class<?> componentType, final int dim) {
		return ClassKit.arrClass(componentType, dim);
	}

	/**
	 * 指定元素的类、指定维数，返回一个空数组。
	 *
	 * @param componentType 元素类型
	 * @param dim 维数
	 * @return 空数组
	 */
	public static Object nDimEmptyArr(final Class<?> componentType, final int dim) {
		return nDimArr(componentType, dim, 0);
	}

	/**
	 * 指定元素的类、指定维数、指定大小，返回一个数组。
	 *
	 * @param componentType 元素类型
	 * @param dim 维数
	 * @param size 数组长度
	 * @return 数组
	 */
	public static Object nDimArr(final Class<?> componentType, final int dim, final int size) {
		if (dim < 1) {
			throw new IllegalArgumentException();
		}
		return Array.newInstance(dim == 1 ? componentType : arrClass(componentType, dim - 1), size);
	}

	/**
	 * 返回数组的最终元素的class(对多维数组适用)。
	 *
	 * @param arrClass 数组的{@link Class}对象
	 * @return 数组的元素的{@link Class}对象
	 */
	public static Class<?> componentType(final Class<?> arrClass) {
		return ClassKit.componentType(arrClass);
	}

	/**
	 * 返回数组的维数。
	 *
	 * @param arrClass 数组的{@link Class}对象
	 * @return 维数
	 */
	public static int dimOfArrayClass(final Class<?> arrClass) {
		return ClassKit.dimOfArrayClass(arrClass);
	}

	public static List<Object> asList(final Object o) {
		if (o == null) {
			return Kits.LIST.emptyVal();
		}
		if (o instanceof final Object[] arr) {
			return Arrays.asList(arr);
		}
		final var c = o.getClass();
		if (c.isArray()) {
			final var len = Array.getLength(o);
			if (len == 0) {
				return Kits.LIST.emptyVal();
			} else {
				final List<Object> rt = IntStream.range(0, len).mapToObj(i -> Array.get(o, i))
						.collect(Collectors.toCollection(() -> Lists.newArrayListWithExpectedSize(len)));
				return rt;
			}
		} else {
			return Lists.newArrayList(o);
		}
	}

	/**
	 * 使用==在数组中查找元素，如果找到，返回它的下标，否则返回-1
	 *
	 * @param <T> 元素的类型
	 * @param target 目标
	 * @param source 源数组
	 * @return 若找到，返回下标；未找到返回-1。
	 */
	@SafeVarargs
	public static <T> int findInstance(final T target, final T... source) {
		var index = 0;
		for (final var t : source) {
			if (t == target) {
				return index;
			}
			index++;
		}
		return JavaConst.NOT_FOUND_INDEX;
	}

	@SafeVarargs
	public static <T> int find(final T target, final T... source) {
		var index = 0;
		if (target == null) {
			for (final var t : source) {
				if (t == null) {
					return index;
				}
				index++;
			}
		} else {
			for (final var t : source) {
				if (target.equals(t)) {
					//				if(ObjectUtil.deepEquals(target, t)){
					return index;
				}
				index++;
			}
		}
		return JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 返回参数本身。本方法接受可变长度参数列表，因此可以快速将一堆单个元素组合成一个数组而无需显式地new一个T[]
	 *
	 * @param <T> 数组元素的类型
	 * @param ts 数组
	 * @return 数组本身
	 */
	@SafeVarargs
	public static <T> T[] asArray(final T... ts) {
		return ts;
	}

	/**
	 * 深度比较两个数组是否相等.
	 * tips:
	 * 【java.utils.Arrays.deepEquals没有解决两个数组互相引用的问题,该问题会引发一个死循环(java.lang.StackOverflowError)
	 * 此方法会对这种情况返回false
	 * 示例:		Object[] o1 = { null };
	 * Object[] o2 = { null };
	 * o1[0] = o2;
	 * o2[0] = o1;
	 * System.out.print(ObjUtil.OBJ.deepEquals(o1,o2);
	 * System.out.print(Arrays.deepEquals(o1, o2));】
	 *
	 * @param a1 数组1
	 * @param a2 数组2
	 * @return 若深度相等则返回true。
	 */
	public static boolean deepEquals(final Object[] a1, final Object... a2) {
		if (a1 == a2) {
			return true;
		}
		if (a1 == null || a2 == null) {
			return false;
		}
		return deepEquals(a1, a2, null);
	}

	static boolean deepEquals(final Object[] a1, final Object[] a2, final Set<Object> escape) {
		if (escape != null && (escape.contains(a1) || escape.contains(a2))) {
			return false;
		}
		//检查长度
		final var length = a1.length;
		if (a2.length != length) {
			return false;
		}

		return IntStream.range(0, length).allMatch(i -> ObjKit.deepEquals(a1[i], a2[i], escape));
	}

	/**
	 * 返回数组的一个子集，从fromIndex（包含）开始到最后。
	 *
	 * @param <T> 元素的类型。
	 * @param fromIndex 开始的下标
	 * @param source 源数组
	 * @return 尾部数组
	 */
	@SafeVarargs
	public static <T> T[] sub(final int fromIndex, final T... source) {
		if (source == null) {
			return null;
		}
		return sub(fromIndex, source.length, source);
	}

	/**
	 * 返回数组的一个子集，从fromIndex（包含）开始，到toIndex（不包含）结束。
	 * 能对fromIndex和toIndex进行纠错
	 *
	 * @param <T> 元素的类型。
	 * @param fromIndex 开始的下标
	 * @param toIndex 结束的下标
	 * @param source 源数组
	 * @return 数组子集
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] sub(final int fromIndex, final int toIndex, final T... source) {
		return source == null ? null
													: Arrays.copyOfRange(source, Math.min(fromIndex, toIndex), Math.max(fromIndex, toIndex));
	}

	public static boolean primitivesArrEquals(final Object e1, final Object e2) {
		//由调用者保证e1是一个原始类型数组
		final PrimitiveKit<?, Object> ps = Kits.ofPrimitive(e1.getClass().getComponentType().getName());
		return ps.arrEquals(e1, e2);
	}

	/**
	 * 在数组的指定区间中查找指定的实例(即采用==判断是否是同一实例)
	 *
	 * @param <T> 元素的类型
	 * @param target 要查找的实例
	 * @param fromIndex 从哪里开始(包含)
	 * @param toIndex 到哪里结束(不包含)
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int instanceIndexOf(final Object target, final int fromIndex, final int toIndex,
																				final T... source) {
		if (source == null) {
			return JavaConst.NOT_FOUND_INDEX;
		}

		final int s, e;
		if (fromIndex <= toIndex) {
			s = fromIndex;
			e = toIndex;
		} else {
			s = toIndex;
			e = fromIndex;
		}
		final var f = Math.max(s, 0);
		final var t = Math.min(e, source.length);
		return IntStream.range(f, t).filter(i -> source[i] == target).findFirst().orElse(JavaConst.NOT_FOUND_INDEX);
	}

	/**
	 * 在数组的指定区间中查找指定的实例(即采用==判断是否是同一实例)
	 *
	 * @param <T> 元素的类型
	 * @param target 要查找的实例
	 * @param fromIndex 从哪里开始(包含)
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int instanceIndexOf(final Object target, final int fromIndex, final T... source) {
		if (source == null) {
			return JavaConst.NOT_FOUND_INDEX;
		}
		return instanceIndexOf(target, fromIndex, source.length, source);
	}

	/**
	 * 在数组中查找指定的实例(即采用==判断是否是同一实例)
	 *
	 * @param <T> 元素的类型
	 * @param target 要查找的实例
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int instanceIndexOf(final Object target, final T... source) {
		return instanceIndexOf(target, 0, source);
	}

	/**
	 * 在数组的指定区间查找指定的对象(使用deepEquals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param toIndex 到哪里结束(不包含)
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int indexOf(final Object target, final int fromIndex, final int toIndex, final T... source) {
		if (isEmpty(source)) {
			return JavaConst.NOT_FOUND_INDEX;
		}
		final int s, e;
		if (fromIndex <= toIndex) {
			s = fromIndex;
			e = toIndex;
		} else {
			s = toIndex;
			e = fromIndex;
		}
		final var f = Math.max(s, 0);
		final var t = Math.min(e, source.length);
		if (target == null) {
			return IntStream.range(f, t).filter(i -> source[i] == null).findFirst().orElse(JavaConst.NOT_FOUND_INDEX);
		} else {
			return IntStream.range(f, t).filter(i -> ObjKit.deepEquals(target, source[i])).findFirst()
					.orElse(JavaConst.NOT_FOUND_INDEX);
		}
	}

	/**
	 * 在数组的指定区间查找指定的对象(使用deepEquals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int indexOf(final Object target, final int fromIndex, final T... source) {
		if (source == null) {
			return JavaConst.NOT_FOUND_INDEX;
		}
		return indexOf(target, fromIndex, source.length, source);
	}

	/**
	 * 在数组中查找指定的对象(使用deepEquals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param source 源
	 * @return 若找到，返回下标；否则返回{@link JavaConst#NOT_FOUND_INDEX}。
	 */
	@SafeVarargs
	public static <T> int indexOf(final Object target, final T... source) {
		return indexOf(target, 0, source);
	}

	/**
	 * 数组指定区间中是否包含指定对象(使用equals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param toIndex 到哪里结束(不包含)
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean contains(final Object target, final int fromIndex, final int toIndex, final T... source) {
		return indexOf(target, fromIndex, toIndex, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 数组指定区间中是否包含指定对象(使用equals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean contains(final Object target, final int fromIndex, final T... source) {
		return indexOf(target, fromIndex, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 数组中是否包含指定对象(使用deepEquals方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean contains(final Object target, final T... source) {
		return indexOf(target, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 数组指定区间中是否包含指定对象(使用==方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param toIndex 到哪里结束(不包含)
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean containsInstance(final Object target, final int fromIndex, final int toIndex,
																						 final T... source) {
		return instanceIndexOf(target, fromIndex, toIndex, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 数组指定区间中是否包含指定对象(使用==方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param fromIndex 从哪里开始(包含)
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean containsInstance(final Object target, final int fromIndex, final T... source) {
		return instanceIndexOf(target, fromIndex, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 数组中是否包含指定对象(使用==方法对比)
	 *
	 * @param <T> 元素的类型
	 * @param target 指定的对象
	 * @param source 源
	 * @return 若包含则返回true。
	 */
	@SafeVarargs
	public static <T> boolean containsInstance(final Object target, final T... source) {
		return instanceIndexOf(target, source) > JavaConst.NOT_FOUND_INDEX;
	}

	/**
	 * 反转数组中指定区间的元素顺序
	 *
	 * @param <T> 元素的类型
	 * @param source 源数组
	 * @param fromIndex 从哪里开始(包含)
	 * @param toIndex 到哪里结束(不包含)
	 */
	public static <T> void reverse(final T[] source, final int fromIndex, final int toIndex) {
		if (source == null) {
			return;
		}
		final int s, e;
		if (fromIndex <= toIndex) {
			s = fromIndex;
			e = toIndex;
		} else {
			s = toIndex;
			e = fromIndex;
		}
		for (int i = s, j = e - 1; i < j; ++i, --j) {
			swap(source, i, j);
		}
	}

	/**
	 * 反转数组中的元素
	 *
	 * @param <T> 元素的类型
	 * @param source 源数组
	 * @param fromIndex 从哪里开始(包含)
	 */
	public static <T> void reverse(final T[] source, final int fromIndex) {
		if (source == null) {
			return;
		}
		reverse(source, fromIndex, source.length);
	}

	/**
	 * 反转数组中的元素
	 *
	 * @param <T> 元素的类型
	 * @param source 源数组
	 */
	@SafeVarargs
	public static <T> void reverse(final T... source) {
		reverse(source, 0);
	}

	/**
	 * 交换数组中两个元素的位置
	 *
	 * @param <T> 元素的类型
	 * @param array 数组
	 * @param index1 位置1
	 * @param index2 位置2
	 */
	private static <T> void swap(final T[] array, final int index1, final int index2) {
		final var o = array[index1];
		array[index1] = array[index2];
		array[index2] = o;
	}

	/**
	 * 判断数组是否为空
	 *
	 * @param <T> 元素的类型
	 * @param array 数组
	 * @return 若为空则返回true
	 */
	@SafeVarargs
	public static <T> boolean isEmpty(final T... array) {
		return array == null || array.length == 0;
	}

	/**
	 * 把一些元素组成一个list(可扩展的.Arrays.asList()是不可扩展的,不可add()的)
	 *
	 * @param <T> 元素的类型
	 * @param array 数组
	 * @return 列表
	 */
	@SafeVarargs
	public static <T> List<T> asAddableList(final T... array) {
		if (array == null) {
			return new ArrayList<>();
		}
		return Stream.of(array).collect(Collectors.toList());
	}

	/**
	 * 把一些元素包装成一个不可变的list
	 *
	 * @param <T> 元素的类型
	 * @param array 数组
	 * @return 列表
	 */
	@SafeVarargs
	public static <T> List<T> asUnmodifiableList(final T... array) {
		return List.of(array);
	}

	/**
	 * 返回某个类的一维数组的Class对象
	 *
	 * @param <T> 元素的类型
	 * @param componentType 元素的{@link Class}对象
	 * @return 数组的{@link Class}对象
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T[]> arrClass(final Class<T> componentType) {
		return (Class<T[]>) arrClass(componentType, 1);
	}

	/**
	 * 返回某个类的一维数组（空数组）
	 *
	 * @param <T> 元素的类型
	 * @param clazz 元素的{@link Class}对象
	 * @return 空数组
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] emptyArray(final Class<T> clazz) {
		return (T[]) Array.newInstance(clazz, 0);
	}

	/**
	 * 从数组中找出最大值。比较方式由comparator指定
	 *
	 * @param <T> 元素的类型
	 * @param comparator 比较器
	 * @param source 源数组
	 * @return 最大的元素
	 */
	@SafeVarargs
	public static <T extends Comparable<T>> T max(final Comparator<? super T> comparator, final T... source) {
		return Collections.max(Arrays.asList(source), comparator);
	}

	/**
	 * 从数组中找出最大值，按照元素的自然顺序
	 *
	 * @param <T> 元素的类型
	 * @param source 源数组
	 * @return 最大的元素
	 */
	@SafeVarargs
	public static <T extends Comparable<T>> T max(final T... source) {
		return Collections.max(Arrays.asList(source));
	}

	/**
	 * 从数组中找出最小值。比较方式由comparator指定
	 *
	 * @param <T> 元素的类型
	 * @param comparator 比较器
	 * @param source 源数组
	 * @return 最小的元素
	 */
	@SafeVarargs
	public static <T> T min(final Comparator<? super T> comparator, final T... source) {
		return Collections.min(Arrays.asList(source), comparator);
	}

	/**
	 * 从数组中找出最小值，按元素的自然顺序
	 *
	 * @param <T> 元素的类型
	 * @param source 源数组
	 * @return 最小的元素
	 */
	@SafeVarargs
	public static <T extends Comparable<T>> T min(final T... source) {
		return Collections.min(Arrays.asList(source));
	}
}
