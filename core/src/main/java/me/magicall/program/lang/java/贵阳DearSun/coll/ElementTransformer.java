/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.function.BiFunction;
import java.util.function.Function;

@FunctionalInterface
public interface ElementTransformer<F, T> extends BiFunction<Integer, F, T> {

	ElementTransformer<Object, Integer> TO_INDEX = (index, element) -> index;
	ElementTransformer<Object, Object> NO_TRANS = (index, from) -> from;
	ElementTransformer<Object, Class<?>> TO_CLASS = (index, element) -> element.getClass();

	//=====================================================
	ElementTransformer<Object, Integer> TO_HASH_CODE = (index, element) -> element.hashCode();
	ElementTransformer<Object, String> TO_STRING = (index, element) -> element.toString();
	ElementTransformer<String, String> TO_LOWER_CASE = (index, element) -> element.toLowerCase();
	ElementTransformer<Enum<?>, String> ENUM_TO_NAME = (index, element) -> element.name();

	/**
	 * 不变形的变形器.调用它的transform将返回参数对象本身
	 *
	 * @param <F> 转换前的类型
	 * @param <T> 转换后的类型
	 * @return 转换器
	 */
	@SuppressWarnings("unchecked")
	static <F, T> ElementTransformer<F, T> noTrans() {
		return (ElementTransformer<F, T>) NO_TRANS;
	}

	T transform(int index, F element);

	@Override
	default T apply(final Integer integer, final F f) {
		return transform(integer, f);
	}

	default Function<F, T> ignoreIndex() {
		return f -> transform(-1, f);
	}
}
