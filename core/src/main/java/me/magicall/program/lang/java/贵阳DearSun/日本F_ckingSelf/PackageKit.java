/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.computer.Encode;
import me.magicall.program.lang.java.贵阳DearSun.ClassKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.stream.Stream;

/**
 * 一些关于包的但是可能没什么用的工具方法。
 * 注：没有“初始化”过的包是不存在的。比如me.magicall这个包,必须“访问过”该包下的类，它才存在。
 *
 * @author MaGiCalL
 */
public class PackageKit {

	/**
	 * 在包继承链上查找指定的注解。
	 *
	 * @return
	 */
	public static boolean isAnnotationPresentInHierarchy(final Package pack,
																											 final Class<? extends Annotation> annotationClass) {
		return Stream.iterate(pack, Objects::nonNull, PackageKit::parent)
				.anyMatch(p -> p.isAnnotationPresent(annotationClass));
	}

	public static Package parent(final Package pack) {
		final var name = pack.getName();
		final var index = name.lastIndexOf('.');
		if (index < 0) {
			return null;
		}
		return pack.getClass().getClassLoader().getDefinedPackage(name.substring(0, index));
	}

	public static Package[] parents(final Package pack) {
		return Stream.iterate(parent(pack), Objects::nonNull, PackageKit::parent).toArray(Package[]::new);
	}

	public static Package[] parents(final Class<?> clazz) {
		return Stream.iterate(clazz.getPackage(), Objects::nonNull, PackageKit::parent).toArray(Package[]::new);
	}

	public static Package packageOf(final Class<?> c) {
		return c == null ? null : c.getPackage();
	}

	/**
	 * 从包package中获取所有的Class。
	 * 这是一个从网上抄来的方法，没有验证过。
	 *
	 * @param pack 包
	 * @return 所有{@link Class}
	 */
	public static Set<Class<?>> classesOf(final Package pack) {
		//获取包的名字 并进行替换
		var packageName = pack.getName();
		final var packageDirName = packageName.replace('.', '/');
		final List<URL> dirs;
		try {
			dirs = Collections.list(Thread.currentThread().getContextClassLoader().getResources(packageDirName));
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
		//循环迭代下去
		final Set<Class<?>> rt = new LinkedHashSet<>();
		for (final var url : dirs) {
			final var protocol = url.getProtocol();
			//如果是以文件的形式保存在服务器上
			if ("file".equals(protocol)) {
				//获取包的物理路径
				final String filePath;
				try {
					filePath = URLDecoder.decode(url.getFile(), Encode.UTF8.getName());
				} catch (final UnsupportedEncodingException e) {
					//impossible
					throw new UnknownException(e);
				}
				//以文件的方式扫描整个包下的文件 并添加到集合中
				findAndAddClassesInPackageByFile(packageName, filePath, true, rt);
			} else if ("jar".equals(protocol)) {
				//如果是jar包文件
				final JarURLConnection jarURLConnection;
				try {
					jarURLConnection = (JarURLConnection) url.openConnection();
				} catch (final IOException e) {
					throw new UnknownException(e);
				}
				//定义一个JarFile，获取jar
				try (final var jar = jarURLConnection.getJarFile()) {
					final List<JarEntry> entries = Collections.list(jar.entries());
					for (final var entry : entries) {
						//获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
						var name = entry.getName();
						if (name.charAt(0) == '/') {
							name = name.substring(1);
						}
						//如果前半部分和定义的包名相同
						if (name.startsWith(packageDirName)) {
							//如果是一个.class文件 而且不是目录
							if (name.endsWith(ClassKit.FILE_SUFFIX) && !entry.isDirectory()) {
								//去掉后面的".class" 获取真正的类名
								final var className = name.substring(packageName.length() + 1,
										name.length() - ClassKit.FILE_SUFFIX_LEN);
								rt.add(ClassKit.classCalled(packageName + '.' + className));
							} else {
								final var idx = name.lastIndexOf('/');
								if (idx != -1) {//如果以"/"结尾 是一个包
									//获取包名 把"/"替换成"."
									packageName = name.substring(0, idx).replace('/', '.');
								}
							}
						}
					}
				} catch (final IOException e) {
					throw new UnknownException(e);
				}
			}
		}
		return rt;
	}

	/**
	 * 以文件的形式来获取包下的所有Class
	 *
	 * @param packageName 包名
	 * @param packagePath 包路径
	 * @param recursive 是否递归
	 * @param toAddTo 接收集合
	 */
	private static void findAndAddClassesInPackageByFile(final String packageName, final String packagePath,
																											 final boolean recursive, final Set<Class<?>> toAddTo) {
		//获取此包的目录 建立一个File
		final var dir = new File(packagePath);
		//如果不存在或者 也不是目录就直接返回
		if (!dir.exists() || !dir.isDirectory()) {
			return;
		}
		//如果存在 就获取包下的所有文件 包括目录
		final var dirFiles = dir.listFiles(
				file -> recursive && file.isDirectory() || file.getName().endsWith(ClassKit.FILE_SUFFIX));
		if (dirFiles != null) {
			//如果是目录 则继续扫描
			Arrays.stream(dirFiles).forEach(file -> {
				if (file.isDirectory()) {
					findAndAddClassesInPackageByFile(packageName + '.' + file.getName(), file.getAbsolutePath(), recursive,
							toAddTo);
				} else {
					//如果是java类文件 去掉后面的.class 只留下类名
					final var className = file.getName().substring(0, file.getName().length() - ClassKit.FILE_SUFFIX_LEN);
					toAddTo.add(ClassKit.classCalled(packageName + '.' + className));
				}
			});
		}
	}
}
