/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.Serial;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 这个类的子类表示针对某一类型T的工具类，包含针对T类对象的许多常用工具方法，如：返回一个空的一维数组。
 * 它是单例的，不需要到处去new它。
 * 本类则是这些子类一些公有方法的实现。
 * 本类子类建议单例
 *
 * @param <T> 针对的对象的类型。
 * @author MaGiCalL
 */
public abstract class Kit<T> implements Comparator<T>, Serializable {
	@Serial
	private static final long serialVersionUID = -8454331944139332595L;

	@Override
	public String toString() {
		return supportedTypeNames().collect(Collectors.joining(", "));
	}

	//=========================================

	//------------------------------------- 检查类型

	/**
	 * 返回一个Class对象的数组,表示所代表类型的同义词类型或近义词类型.
	 * 如:Int会返回[int.class,Class&lt;Integer&gt;]
	 *
	 * @return 本工具类可以处理的对象的类型。
	 */
	public abstract Stream<Class<?>> supportedClasses();

	/**
	 * 返回一个字符串数组,表示所代表类型的同义词类型名或近义词类型名.
	 * 如:Int会返回[int,Integer,java.lang.Integer]
	 *
	 * @return 支持的类型的名字列表
	 */
	@SuppressWarnings("unchecked")
	public Stream<String> supportedTypeNames() {
		final var supportedClasses = supportedClasses().collect(Collectors.toList());
		//
		final Stream<? extends String>[] streams = new Stream[]{supportedClasses.stream().map(Class::getName),
				supportedClasses.stream().map(Class::getSimpleName)};
		return CollKit.concat(streams);
	}

	/**
	 * 判断本工具类是否适合这个参数
	 *
	 * @param obj 指定的对象
	 * @return 若本工具类适用于该对象则返回true。
	 */
	public boolean isSuitForInstance(final Object obj) {
		var c = obj.getClass();
		while (c.isArray()) {
			c = ClassKit.componentType(c);
		}
		final Class<?> clazz = c;
		return supportedClasses().anyMatch(e -> e.isAssignableFrom(clazz));
	}

	/**
	 * 判断名字代表的类是否是本util代表的类的"同义词"
	 * tips:
	 * 【可以析解类全名,类短名,基本类型关键字等】
	 * 【不区分大小写】
	 *
	 * @param className 类型名
	 * @return 若支持则返回true。
	 */
	public boolean isSuitForTypeName(final String className) {
		return supportedTypeNames().anyMatch(e -> e.equalsIgnoreCase(className));
	}

	/**
	 * 判断一个类是否是本util代表的类或其外包类
	 *
	 * @param clazz 要检测的{@link Class}对象。
	 * @return 若本工具类可以处理该{@link Class}代表的对象则返回true。
	 */
	public boolean isSuitFor(final Class<?> clazz) {
		return supportedClasses().anyMatch(e -> e.isAssignableFrom(clazz));
	}

	//------------------------------ 实例基本套路。

	/**
	 * 判断两个对象是否相等
	 *
	 * @param o1 对象1
	 * @param o2 对象2
	 * @return 两个对象是否相等。
	 */
	public boolean equals(final T o1, final Object o2) {
		return Objects.equals(o1, o2);
	}

	/**
	 * 将一个String对象解析为一个所代表类型的对象.通常情况下是该对象toString方法的反解
	 * 注：鉴于各个外包类所提供的解析字符串方法名字不尽相同，本方法实际上将它们代理为相同名字的方法。
	 *
	 * @param source 源
	 * @return 等同于源字符串的该类对象的值。若源字符串为null则返回null；为空字符串则返回{@link #emptyVal()}；若不能解析则返回null。
	 */
	public T fromString(final String source) {
		if (source == null) {
			return null;
		}
		if (source.isBlank()) {
			return emptyVal();
		}
		return parse(source);
	}

	public abstract T parse(String source);

	//----------------------------- 实例判空、默认值

	/**
	 * 返回一个非null的代表"空"或"原点"的值.
	 * 注意:有些类(比如Collection)的"空"值不止一个,此方法仅返回其中一个
	 *
	 * @param <T1> 元素的类型或映射中键的类型
	 * @param <T2> 映射中值的类型
	 * @return 该类型的空值。
	 */
	//这里有两个泛型参数,是为了MapUtil覆盖这个方法需要两个泛型参数
	public abstract <T1, T2> T emptyVal();

	/**
	 * 判断参数是否为"空"(包括null,默认的"空"值,"原点"值)
	 * 注意:此方法返回true并不表示参数即为null或defaultValue()方法所返回的值.
	 *
	 * @param source 源
	 * @return 源是否该类对象的空值。
	 */
	public boolean isEmpty(final T source) {
		return emptyVal().equals(nullOrEmptyVal(source));
	}

	/**
	 * 如果参数为"空",则返回默认的"空"值,否则返回参数本身.
	 * 用于去除讨厌的空指针异常很管用
	 *
	 * @param <T1> 源的类型
	 * @param source 源
	 * @return 若源为null，则返回该类对象的“空值”（比如字符串类的空值是空字符串）
	 */
	@SuppressWarnings("unchecked")
	public <T1 extends T> T1 nullOrEmptyVal(final T1 source) {
		return nullOrDefaultVal(source, (T1) emptyVal());
	}

	/**
	 * 检查source是否为null，如果是，则返回defaultValue，否则返回source本身
	 *
	 * @param <T1> T的某个子类
	 * @param source 源
	 * @param defaultValue 默认值
	 * @return 若源为null，则返回指定的默认值
	 */
	public <T1 extends T> T1 nullOrDefaultVal(final T1 source, final T1 defaultValue) {
		if (source == null) {
			return defaultValue;
		} else {
			return source;
		}
	}

	//--------------------------------实例比较大小

	/**
	 * 尝试比较。
	 *
	 * @param o1 对象1
	 * @param o2 对象2
	 * @return 若对象1小于对象2，返回负数；若对象1大于对象2，返回正数；否则返回0。若对象不能比较，则认为前者就是“前者”（更小），返回-1。
	 */
	@Override
	public int compare(final T o1, final T o2) {
		if (o1 instanceof Comparable<?>) {
			return ((Comparable) o1).compareTo(o2);
		}
		return -1;
	}

	/**
	 * 从数组中找出最大值
	 *
	 * @param source 数组
	 * @return 最大值
	 */
	@SafeVarargs
	public final T max(final T... source) {
		return source == null ? null : Stream.of(source).max(this).orElse(null);
	}

	/**
	 * 从数组中找出最小值
	 *
	 * @param source 数组
	 * @return 最小值
	 */
	@SafeVarargs
	public final T min(final T... source) {
		return source == null ? null : Stream.of(source).min(this).orElse(null);
	}

	/**
	 * 判断前一个参数(source)所表示的范围是否真包含后一个参数(target)所表示的范围(source &gt; target).
	 * 注:null与null相等,小于其他任何值.
	 * 即:
	 * 若source为null,则无论target是什么,有source &lt;= target,返回false;
	 * 若target为null,source不为null,有source &gt; target,返回true;
	 *
	 * @param source null等于null,小于任何其他值
	 * @param target null等于null,小于任何其他值
	 * @return source &gt; target
	 */
	public boolean greater(final T source, final T target) {
		return compare(source, target) > 0;
	}

	/**
	 * 判断前一个参数(source)所表示的范围是否包含后一个参数(target)所表示的范围(source &gt;= target).
	 * 注:null与null相等,小于其他任何值.
	 * 即:
	 * 若target为null,则无论source为何值,有source &gt;= target,返回true;
	 * 若target不为null,source为null,有source &lt; target,返回false;
	 *
	 * @param source null等于null,小于任何其他值
	 * @param target null等于null,小于任何其他值
	 * @return source &gt; target
	 */
	public boolean greaterEquals(final T source, final T target) {
		return compare(source, target) >= 0;
	}

	/**
	 * 判断前一个参数(source)所表示的范围是否真包含于后一个参数(target)所表示的范围(source &lt; target).
	 * 注:null与null相等,小于其他任何值.
	 * 即:
	 * 若target为null,则无论source是什么,有source &gt;= target,返回false;
	 * 若target不为null,source为null,有source &lt; target,返回true;
	 *
	 * @param source null等于null,小于任何其他值
	 * @param target null等于null,小于任何其他值
	 * @return source &gt; target
	 */
	public boolean less(final T source, final T target) {
		return greater(target, source);
	}

	/**
	 * 判断前一个参数(source)所表示的范围是否包含于后一个参数(target)所表示的范围(source &lt;= target).
	 * 注:null与null相等,小于其他任何值.
	 * 即:
	 * 若source为null,则无论target是什么,有source &lt;= target,返回false;
	 * 若source不为null,target为null,有source &gt; target,返回true;
	 *
	 * @param source null等于null,小于任何其他值
	 * @param target null等于null,小于任何其他值
	 * @return source &gt; target
	 */
	public boolean lessEquals(final T source, final T target) {
		return greaterEquals(target, source);
	}

	//--------------------------- 数组

	/**
	 * 新建一个所表示类型的对象的数组
	 *
	 * @param size 数组长度
	 * @return 本工具所代表的类的数组
	 */
	@SuppressWarnings("unchecked")
	public T[] arr(final int size) {
		return size < 1 ? emptyArr() : (T[]) Array.newInstance(mainClass(), size);
	}

	/**
	 * 返回所表示类型的对象的空数组
	 *
	 * @return 空数组
	 */
	@SuppressWarnings("unchecked")
	public T[] emptyArr() {
		return (T[]) Array.newInstance(mainClass(), 0);
	}

	/**
	 * 返回一个长度为size的数组,所有元素均为指定值
	 *
	 * @param size 长度
	 * @param value 指定值
	 * @return 数组
	 */
	public T[] nCopy(final int size, final T value) {
		final var rt = arr(size);
		IntStream.range(0, size).forEach(i -> rt[i] = value);
		return rt;
	}

	/**
	 * 返回一个长度为size的数组,所有元素均为默认的"空"值
	 * tips:【用于创建稀疏矩阵很方便】
	 *
	 * @param size 指定的长度
	 * @return 数组
	 */
	public T[] emptyValArr(final int size) {
		return nCopy(size, emptyVal());
	}

	/**
	 * 返回指定维数的T的数组的Class对象(Class&lt;T[]&gt;对象)
	 * 注意:低维数组并非高维数组的父类!（String[]不是String[][]的父类）
	 *
	 * @param dim 维数
	 * @return 数组的{@link Class}对象。
	 */
	public Class<? extends Object[]> arrClass(final int dim) {
		return (Class<? extends Object[]>) ArrKit.arrClass(mainClass(), dim);
	}

	@SuppressWarnings("unchecked")
	protected Class<T> mainClass() {
		return (Class<T>) supportedClasses().findFirst().orElseThrow(UnknownException::new);
	}
}
