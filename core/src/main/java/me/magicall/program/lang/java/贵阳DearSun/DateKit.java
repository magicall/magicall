/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import me.magicall.time.TimeConst.DateConst;
import me.magicall.time.TimeKit;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Stream;

public final class DateKit extends Kit<Date> implements Serializable {
	@Serial
	private static final long serialVersionUID = 3971538824614754296L;
	private static final Collection<Class<?>> SUPPORTED_CLASSES = Collections.singleton(Date.class);

	public static final DateKit INSTANCE = new DateKit();

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Date parse(final String source) {
		return TimeKit.parse(source);
	}

	@Override
	public <T1, T2> Date emptyVal() {
		return DateConst._0;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
