/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.util.ArrayList;
import java.util.List;

/**
 * 编程语言里标识符中各单词的分割风格。
 */
public enum LabelStyle {
	SPACE_SEPARATED(' '),
	/**
	 * 驼峰式
	 */
	CAMEL {
		@Override
		public boolean isWordEdge(final char c) {
			return Character.isUpperCase(c);
		}

		@Override
		public void handleNextWordFirstChar(final StringBuilder sb, final char nextWordHeaderChar) {
			sb.append(Character.toUpperCase(nextWordHeaderChar));
		}

		@Override
		void handleFirstChar(final StringBuilder sb, final char firstChar) {
			sb.append(Character.toLowerCase(firstChar));
		}
	},
	/**
	 * 下划线式
	 */
	UNDERLINE_SEPARATED('_') {
		@Override
		void handleFirstChar(final StringBuilder sb, final char firstChar) {
			sb.append(Character.toLowerCase(firstChar));
		}
	},
	/**
	 * 中划线式
	 */
	MINUS_SEPARATED('-') {
		@Override
		void handleFirstChar(final StringBuilder sb, final char firstChar) {
			sb.append(Character.toUpperCase(firstChar));
		}
	},
	;
	private final Character separator;

	LabelStyle() {
		this(null);
	}

	LabelStyle(final Character separator) {
		this.separator = separator;
	}

	public boolean isWordEdge(final char c) {
		return c == getSeparator();
	}

	void handleNextWordFirstChar(final StringBuilder sb, final char nextWordHeaderChar) {
		sb.append(getSeparator());
		handleFirstChar(sb, nextWordHeaderChar);
	}

	void handleFirstChar(final StringBuilder sb, final char firstChar) {
		sb.append(firstChar);
	}

	public boolean hasSeparator() {
		return getSeparator() != null;
	}

	public String convertTo(final LabelStyle targetFormat, final String source) {
		if (Kits.STR.isEmpty(source)) {
			return source;
		}
		final var sb = new StringBuilder();
		targetFormat.handleFirstChar(sb, source.charAt(0));
		final var len = source.length();
		for (var i = 1; i < len; ++i) {
			final var c = source.charAt(i);
			if (isWordEdge(c)) {
				++i;
				if (i < source.length()) {
					targetFormat.handleNextWordFirstChar(sb, source.charAt(i));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public String[] splitWords(final String s) {
		return Kits.STR.isEmpty(s) ? Kits.STR.emptyArr() : listWords(s).toArray(new String[0]);
	}

	public List<String> listWords(final String s) {
		final List<String> list = new ArrayList<>();
		final var len = s.length();
		var lastIndex = 0;
		for (var i = 1; i < len; ++i) {
			final var c = s.charAt(i);
			if (isWordEdge(c)) {
				list.add(s.substring(lastIndex, i));
				if (hasSeparator()) {
					lastIndex = i + 1;
				} else {
					lastIndex = i;
				}
			}
		}
		list.add(s.substring(lastIndex));
		return list;
	}

	public Character getSeparator() {
		return separator;
	}
}
