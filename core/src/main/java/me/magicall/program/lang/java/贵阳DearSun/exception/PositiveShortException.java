/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import com.google.common.collect.Range;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class PositiveShortException extends NumOutOfRangeException {

	@Serial
	private static final long serialVersionUID = 5025840885678129901L;
	private static final Range<Short> AVAILABLE_RANGE = Range.closed(Short.MIN_VALUE, (short) 0);

	public PositiveShortException(final String argName, final short actualVal) {
		super(argName, actualVal, AVAILABLE_RANGE);
	}

	public PositiveShortException(final String argName, final short actualVal, final Throwable cause) {
		this(argName, actualVal);
		initCause(cause);
	}

	public PositiveShortException(final String argName, final short actualVal, final short minAvailable) {
		super(argName, actualVal, Range.closed(minAvailable, (short) 0));
	}

	public PositiveShortException(final String argName, final short actualVal, final short minAvailable,
																final Throwable cause) {
		this(argName, actualVal, minAvailable);
		initCause(cause);
	}
}
