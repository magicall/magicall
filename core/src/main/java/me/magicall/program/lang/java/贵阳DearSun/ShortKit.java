/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class ShortKit extends NotFractionKit<Short, short[]> {
	@Serial
	private static final long serialVersionUID = -6638443404131445197L;

	public static final ShortKit INSTANCE = new ShortKit();

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Short.class, short.class);

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Short parse(final String source) {
		try {
			return Short.parseShort(source);
		} catch (final NumberFormatException e) {
			return null;
		}
	}

	@Override
	public String primitiveArrFlag() {
		return "S";
	}

	@Override
	public Short fromLong(final long value) {
		return (short) value;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
