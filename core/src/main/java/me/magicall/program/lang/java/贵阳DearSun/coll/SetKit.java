/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.io.Serial;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Stream;

public final class SetKit extends AbsCollKit<Set<?>> {
	@Serial
	private static final long serialVersionUID = 1680811375727526538L;
	private static final List<Class<?>> SUPPORTED_CLASSES = Collections.singletonList(Set.class);

	public static final SetKit INSTANCE = new SetKit();

	//===========================================

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Set<?> parse(final String source) {
		return null;//todo
	}

	@Override
	public <E, E1> Set<E> emptyVal() {
		return (Set) EmptyColl.INSTANCE;
	}

	public <E> E randomOne(final Set<E> source) {
		return CollKit.randomOne(source);
	}

	public static <E> SortedSet<E> emptySortedSet() {
		return (SortedSet) EmptyColl.INSTANCE;
	}

	/**
	 * 将一个来历不明的集合的泛型强转成另一种类型的集合泛型.
	 * 注意:这是"非安全的"，适用场合：当使用者很清楚地确定source（是一个Collection&lt;S&gt;）中的S是T的父类，用此方法帮助做强制类型转换，可少写很多恶心代码
	 * 注：跟父类实现一致，覆写一遍是为了返回类型能带上泛型T。
	 *
	 * @param <T> 集合的元素类型。
	 * @param source 源集合。
	 * @return 强转的集合。
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> Set<T> cast(final Set<?> source) {
		return (Set<T>) source;
	}
	//====================================================

	private Object readResolve() {
		return INSTANCE;
	}
}
