/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import me.magicall.program.lang.java.贵阳DearSun.ClassKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Method_ extends Executable_<Method> {
	private final Type_<Object> elementType;

	public Method_(final Method raw) {
		super(raw);
		elementType = Type_.of(raw.getGenericReturnType());
	}

	@Override
	public Type_<?> elementType() {
		return elementType;
	}

	public boolean isAbstract() {
		return ClassKit.isAbstract(unwrap());
	}

	public boolean isDefault() {
		return ClassKit.isDefault(unwrap());
	}

	public boolean isNoReturn() {
		return unwrap().getReturnType() == void.class;
	}

	//---------------------------------------调用方法

	public boolean canBeCalledBy(final Object caller, final Object... args) {
		if (caller == null) {
			return isStatic() && canUseArgs(args);
		}
		if (!owner().canInstanceBe(caller)) {
			return false;
		}
		if (!canUseArgs(args)) {
			return false;
		}
		if (isStatic()) {//跟Method原生的canAccess不同，它不允许“用实例调用静态方法”，但实际上应当是可行的。
			return true;
		}
		return unwrap().canAccess(caller);
	}

	/**
	 * 用指定的调用者调用本方法。
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T calledBy(final Object caller, final Object... args) {
		//org.apache.commons.lang3.reflect.MethodUtils.invokeMethod：会处理varArgs（含装箱拆箱）
		//todo 但根据实参对象的类型获取方法时，没处理形参是实参父类的情况
		final var method = unwrap();
		try {
			if (isStatic()) {
				return (T) MethodUtils.invokeStaticMethod(method.getDeclaringClass(), method.getName(), args);
			} else {
				return (T) MethodUtils.invokeMethod(caller, true, method.getName(), args);
			}
		} catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			throw new UnknownException(e);
		}
	}

	/**
	 * 静态调用
	 *
	 * @return
	 */
	public <T> T staticCalled(final Object... params) {
		if (isStatic()) {
			return calledBy(null, params);
		} else {
			throw new NullValException("caller");
		}
	}

	@Override
	public <V> V tryToFetchVal(final Object... obj) {
		if (isStatic()) {
			try {
				return calledBy(null, obj);
			} catch (final RuntimeException ignore) {
				return null;
			}
		}
		if (obj.length == 0) {
			throw new NullValException("caller");
		} else if (obj.length == 1) {
			try {
				return calledBy(obj[0]);
			} catch (final RuntimeException ignore) {
				return null;
			}
		} else {
			final Object[] factArgs = new Object[obj.length - 1];
			System.arraycopy(obj, 1, factArgs, 0, obj.length - 1);
			try {
				return calledBy(obj[0], factArgs);
			} catch (final RuntimeException ignore) {
				return null;
			}
		}
	}

	//----------------------------------一些特殊方法

	public boolean isToString() {
		return checkSame(METHOD_TO_STRING);
	}

	public boolean isEquals() {
		return checkSame(METHOD_EQUALS);
	}

	public boolean isHashCode() {
		return checkSame(METHOD_HASH_CODE);
	}

	public boolean isGetClass() {
		return checkSame(METHOD_GET_CLASS);
	}

	private boolean checkSame(final Method other) {
		final var method = unwrap();
		return method.getName().equals(other.getName())//
				&& Arrays.equals(method.getParameterTypes(), other.getParameterTypes());
	}

	private static final Method METHOD_TO_STRING;
	private static final Method METHOD_EQUALS;
	private static final Method METHOD_HASH_CODE;
	private static final Method METHOD_GET_CLASS;

	static {
		try {
			METHOD_TO_STRING = Object.class.getMethod("toString");
			METHOD_EQUALS = Object.class.getMethod("equals", Object.class);
			METHOD_HASH_CODE = Object.class.getMethod("hashCode");
			METHOD_GET_CLASS = Object.class.getMethod("getClass");
		} catch (final NoSuchMethodException e) {
			throw new UnknownException(e);
		}
	}
}
