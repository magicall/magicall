/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

public abstract class TreeTemplate<E> implements Tree<E> {

	public boolean equals(final Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Tree)) {
			return false;
		}
		final Tree<?> other = (Tree) o;
		return getRoot().equals(other.getRoot());
	}

	public int hashCode() {
		return getRoot().hashCode();
	}

	public String toString() {
		return TreeKit.str(this);
	}
}
