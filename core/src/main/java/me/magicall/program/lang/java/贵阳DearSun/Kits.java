/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.ListKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.MapKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.SetKit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Kits {

	public static final IntKit INT = IntKit.INSTANCE;
	public static final LongKit LONG = LongKit.INSTANCE;
	public static final ByteKit BYTE = ByteKit.INSTANCE;
	public static final ShortKit SHORT = ShortKit.INSTANCE;
	public static final FloatKit FLOAT = FloatKit.INSTANCE;
	public static final DoubleKit DOUBLE = DoubleKit.INSTANCE;
	public static final CharKit CHAR = CharKit.INSTANCE;
	public static final BoolKit BOOL = BoolKit.INSTANCE;

	public static final StrKit STR = StrKit.INSTANCE;
	public static final ObjKit OBJ = ObjKit.INSTANCE;
	public static final DateKit DATE = DateKit.INSTANCE;

	public static final CollKit COLL = CollKit.INSTANCE;
	public static final SetKit SET = SetKit.INSTANCE;
	public static final ListKit LIST = ListKit.INSTANCE;
	public static final MapKit MAP = MapKit.INSTANCE;

	private static final Collection<Kit<?>> ALL = Collections.unmodifiableCollection(Arrays.asList(//
			//最常用
			INT, STR, BOOL, DATE, OBJ,
			//次常用
			COLL, LIST, SET, MAP,//
			FLOAT, LONG, DOUBLE,//
			//不常用
			BYTE, SHORT, CHAR));

	private static final List<Kit<?>> SUIT_LIST = LIST.unmodifiable(Arrays.asList(//
			//数字型
			INT, LONG, DOUBLE, FLOAT, CHAR, BYTE, SHORT,//
			//其他
			STR, BOOL, DATE,
			//集合型
			LIST, SET, MAP, COLL, //
			//垫底
			OBJ));

	private static final Collection<PrimitiveKit<?, ?>> ALL_PRIMITIVE_CLASS_UTILS = Collections.unmodifiableCollection(
			Arrays.asList(//
					INT, BOOL, FLOAT, LONG, DOUBLE, BYTE, SHORT, CHAR));
	private static final BiMap<Class<?>, Class<?>> PRIMITIVE_CLASS_REF;

	static {
		final BiMap<Class<?>, Class<?>> tmp = HashBiMap.create(8);
		tmp.put(int.class, Integer.class);
		tmp.put(boolean.class, Boolean.class);
		tmp.put(float.class, Float.class);
		tmp.put(long.class, Long.class);
		tmp.put(double.class, Double.class);
		tmp.put(byte.class, Byte.class);
		tmp.put(short.class, Short.class);
		tmp.put(char.class, Character.class);
		PRIMITIVE_CLASS_REF = tmp;
	}

	public static Kit<?> suitableUtil(final Object obj) {
		final Collection<Kit<?>> all = SUIT_LIST;
		return all.stream().filter(u -> u.isSuitForInstance(obj)).findFirst().orElse(OBJ);
	}

	/**
	 * 仿enum做的一个方法.获取所有的已知的ClassUtils
	 *
	 * @return 所有的{@link Kit}
	 */
	public static Collection<Kit<?>> getAll() {
		return ALL;
	}

	/**
	 * 仿enum做的一个方法,根据名字获取某个util.
	 * tips:
	 * 【可以析解类全名,类短名,基本类型关键字等】
	 * 【不区分大小写】
	 *
	 * @param <T> 类型
	 * @param name 类的全名/短名/基本类型的关键字等
	 * @return 针对指定类型的工具类
	 */
	@SuppressWarnings("unchecked")
	public static <T> Kit<T> of(final String name) {
		return (Kit<T>) getAll().stream()//
				.filter(u -> u.supportedTypeNames().anyMatch(s -> s.equalsIgnoreCase(name)))//
				.findFirst().orElse(null);
	}

	@SuppressWarnings("unchecked")
	public static <T> Kit<T> of(final Class<?> clazz) {
		return (Kit<T>) getAll().stream()//
				.filter(u -> u.supportedClasses().anyMatch(c -> c.isAssignableFrom(clazz)))//
				.findFirst().orElse(null);
	}

	public static Class<?> wrapClass(final Class<?> primitiveClass) {
		return PRIMITIVE_CLASS_REF.get(primitiveClass);
	}

	public static Class<?> primitiveClass(final Class<?> primitiveClass) {
		return PRIMITIVE_CLASS_REF.inverse().get(primitiveClass);
	}

	/**
	 * 仿enum做的一个方法,根据名字获取某个util. 可以析解类全名,类短名,基本类型关键字等 不区分大小写
	 *
	 * @param <A> 基本类型
	 * @param <T> 包装类类型
	 * @param name 基本类型或包装类的类型名：全名/短名/基本类型的关键字
	 * @return 适用于该类型的工具类
	 */
	@SuppressWarnings("unchecked")
	public static <T, A> PrimitiveKit<T, A> ofPrimitive(final String name) {
		return (PrimitiveKit<T, A>) getAllPrimitiveClassUtils().stream()//
				.filter(u -> u.supportedTypeNames().anyMatch(s -> s.equalsIgnoreCase(name)))//
				.findFirst().orElse(null);
	}

	@SuppressWarnings("unchecked")
	public static <T, A> PrimitiveKit<T, A> getPrimitiveKit(final Class<?> clazz) {
		final var all = getAllPrimitiveClassUtils();
		return (PrimitiveKit<T, A>) all.stream().filter(u -> u.isSuitFor(clazz)).findFirst().orElse(null);
	}

	/**
	 * 获取基本类型及其外包类的工具类
	 *
	 * @return 工具类集合
	 */
	public static Collection<PrimitiveKit<?, ?>> getAllPrimitiveClassUtils() {
		return ALL_PRIMITIVE_CLASS_UTILS;
	}

	/**
	 * 判断某{@link Class}是否基本类型或其外包类.
	 *
	 * @param clazz 类型
	 * @return 是否基本类型或其外包类.
	 */
	public static boolean isPrimitiveOrWrapper(final Class<?> clazz) {
		return PRIMITIVE_CLASS_REF.containsKey(clazz) || PRIMITIVE_CLASS_REF.containsValue(clazz);
	}

	public static Collection<Class<?>> allPrimitiveClasses() {
		return PRIMITIVE_CLASS_REF.keySet();
	}

	public static Collection<Class<?>> allWrapClasses() {
		return PRIMITIVE_CLASS_REF.values();
	}

	public static void main(final String... args) {
	}
}
