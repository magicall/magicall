/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.util.stream.Stream;

/**
 * 可被注解的事物，包括包、类型（类、接口、枚举、注解）、函数（构造器、方法）、字段、参数、变量等。参见{@link ElementType}
 */
public interface CanBeAnnotated {
	/**
	 * 注释所有注解对象。包括继承的。
	 *
	 * @return 注解集。
	 */
	Stream<Obj<Annotation>> annotations();

	/**
	 * 是否有任何注解。包括继承的。
	 *
	 * @return 是否有注解。
	 */
	default boolean hasAnnotations() {
		return annotations().findAny().isPresent();
	}

	/**
	 * 是否存在指定类型的注解。包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @param <A> 指定的注解的类型。
	 * @return 注解的增强对象。
	 */
	<A extends Annotation> Obj<A> findAnnotation(Cls<A> type);

	/**
	 * 是否存在指定类型的注解。包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @param <A> 指定的注解的类型。
	 * @return 注解的增强对象。
	 */
	default <A extends Annotation> Obj<A> findAnnotation(final Class<A> type) {
		return findAnnotation(new Cls(type));
	}

	/**
	 * 判断是否有指定类型的注解对象。包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @return 是否有。
	 */
	default boolean hasAnnotation(final Cls<? extends Annotation> type) {
		return findAnnotation(type) != null;
	}

	/**
	 * 判断是否有指定类型的注解对象。包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @return 是否有。
	 */
	default boolean hasAnnotation(final Class<? extends Annotation> type) {
		return findAnnotation(type) != null;
	}

	//----------------------------------------------------------

	/**
	 * 获取自有的注解对象。即不包含继承来的注解。
	 *
	 * @return 注解集。
	 */
	Stream<Obj<Annotation>> directAnnotations();

	/**
	 * 是否自有注解。即不包含继承来的注解。
	 *
	 * @return 是否有。
	 */
	default boolean hasDirectAnnotations() {
		return directAnnotations().findAny().isPresent();
	}

	/**
	 * 获取指定类型的自有注解。即不包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @param <A> 指定的注解的类型。
	 * @return 注解的增强对象。
	 */
	<A extends Annotation> Obj<A> findDirectAnnotation(Cls<A> type);

	/**
	 * 获取指定类型的自有注解。即不包括继承的。
	 *
	 * @param type 指定的注解类。
	 * @param <A> 指定的注解的类型。
	 * @return 注解的增强对象。
	 */
	default <A extends Annotation> Obj<A> findDirectAnnotation(final Class<A> type) {
		return findDirectAnnotation(new Cls(type));
	}

	/**
	 * 判断是否自有指定类型的注解对象。
	 *
	 * @param type 指定的注解类。
	 * @return 是否有。
	 */
	default boolean hasDirectAnnotation(final Cls<? extends Annotation> type) {
		return findDirectAnnotation(type) != null;
	}

	/**
	 * 判断是否自有指定类型的注解对象。
	 *
	 * @param type 指定的注解类。
	 * @return 是否有。
	 */
	default boolean hasDirectAnnotation(final Class<? extends Annotation> type) {
		return findDirectAnnotation(type) != null;
	}
}
