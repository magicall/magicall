/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.runtime;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import javax.tools.ToolProvider;
import java.util.Collections;

public class RuntimeKit {
	public static int objHash(final Object o) {
		return System.identityHashCode(o);
	}

	/**
	 * 运行时动态编译源码，获取类对象。
	 *
	 * @param source 源代码
	 * @param classFullName 类全名
	 * @return 编译后得到的{@link Class}对象
	 */
	public static Class<?> compile(final String source, final String classFullName) {
		final var compiler = ToolProvider.getSystemJavaCompiler();

		//FileManager不要求线程安全，所以不能做静态常量。
		final var fileManager = new SourceCodeFileManager(compiler.getStandardFileManager(null, null, null));

		final var task = compiler.getTask(null, fileManager, null, null, null,//
				Collections.singletonList(new JavaSourceCode(classFullName, source)));
		if (task.call()) {
			final var classLoader = fileManager.getClassLoader(null);
			try {
				return classLoader.loadClass(classFullName);
			} catch (final ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		throw new UnknownException();
	}
}
