/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import java.util.Iterator;
import java.util.NavigableSet;

/**
 * @author Liang Wenjian.
 */
public interface NavigableSetSupport<E> extends NavigableSet<E>, SortedSetSupport<E> {

	@Override
	default E pollFirst() {
		final var first = firstOrNull();
		if (first == null) {
			return null;
		}
		remove(first);
		return first;
	}

	@Override
	default E pollLast() {
		final var last = lastOrNull();
		if (last == null) {
			return null;
		}
		remove(last);
		return last;
	}

	@Override
	default NavigableSet<E> descendingSet() {
		return new DescendingSet<>(this);
	}

	@Override
	default Iterator<E> descendingIterator() {
		return descendingSet().iterator();
	}

	@Override
	default NavigableSet<E> headSet(final E toElement) {
		return headSet(toElement, false);
	}

	@Override
	default NavigableSet<E> headSet(final E toElement, final boolean inclusive) {
		return subSet(firstOrNull(), true, toElement, inclusive);
	}

	@Override
	default NavigableSet<E> tailSet(final E fromElement) {
		return tailSet(fromElement, true);
	}

	@Override
	default NavigableSet<E> tailSet(final E fromElement, final boolean inclusive) {
		return subSet(fromElement, inclusive, lastOrNull(), true);
	}

	@Override
	default E lower(final E target) {
		final var comparator = nonNullComparator();
		E last = null;
		for (final var e : this) {
			final var compare = comparator.compare(e, target);
			if (compare < 0) {
				last = e;
			} else {
				return last;
			}
		}
		return null;
	}

	@Override
	default E floor(final E target) {
		final var comparator = nonNullComparator();
		E last = null;
		for (final var e : this) {
			final var compare = comparator.compare(e, target);
			if (compare == 0) {
				return e;
			} else if (compare < 0) {
				last = e;
			} else {
				return last;
			}
		}
		return null;
	}

	@Override
	default E higher(final E target) {
		final var comparator = nonNullComparator();
		for (final var e : this) {
			final var compare = comparator.compare(e, target);
			if (compare > 0) {
				return e;
			}
		}
		return null;
	}

	@Override
	default E ceiling(final E target) {
		final var comparator = nonNullComparator();
		for (final var e : this) {
			final var compare = comparator.compare(e, target);
			if (compare >= 0) {
				return e;
			}
		}
		return null;
	}

	@Override
	default NavigableSet<E> subSet(final E fromElement, final boolean fromInclusive, final E toElement,
																 final boolean toInclusive) {
		return new NavigableSubSet<>(this, fromElement, fromInclusive, toElement, toInclusive);
	}

	@Override
	default NavigableSet<E> subSet(final E fromElement, final E toElement) {
		return subSet(fromElement, true, toElement, false);
	}
}
