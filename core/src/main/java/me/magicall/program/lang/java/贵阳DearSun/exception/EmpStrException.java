/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.exception;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.io.Serial;

/**
 * @author Liang Wenjian.
 */
public class EmpStrException extends NoElementException {

	@Serial
	private static final long serialVersionUID = -1898758542304385468L;

	public EmpStrException(final String argName) {
		super(argName, Kits.STR.emptyVal(), "at least 1 char.");
	}

	@Override
	public String getActualVal() {
		return (String) super.getActualVal();
	}
}
