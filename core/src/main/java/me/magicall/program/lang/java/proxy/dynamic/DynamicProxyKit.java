/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.proxy.dynamic;

import me.magicall.program.lang.java.贵阳DearSun.ArrKit;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Collection;

public interface DynamicProxyKit {

	static <T> T proxy(final InvocationHandler h, final Collection<Class<?>> interfacesToImplement) {
		return proxy(h.getClass().getClassLoader(), h, interfacesToImplement.toArray(new Class<?>[0]));
	}

	static <T> T proxy(final InvocationHandler h, final Class<T> interfacesToImplement) {
		return proxy(h, ArrKit.asArray(interfacesToImplement));
	}

	static <T> T proxy(final InvocationHandler h, final Class<?>... interfacesToImplement) {
		return proxy(h.getClass().getClassLoader(), h, interfacesToImplement);
	}

	@SuppressWarnings("unchecked")
	static <T> T proxy(final ClassLoader loader, final InvocationHandler h, final Class<?>... interfacesToImplement) {
		return (T) Proxy.newProxyInstance(loader, interfacesToImplement, h);
	}
}
