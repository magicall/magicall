/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.json.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.collect.Range;
import me.magicall.supporter.spring.AbsStrToRangeConverter;

import java.io.IOException;

public class RangeDeserializer<T extends Comparable<T>> extends JsonDeserializer<Range<T>> {
	private final AbsStrToRangeConverter<T> converter;

	public RangeDeserializer(final AbsStrToRangeConverter<T> converter) {
		this.converter = converter;
	}

	@Override
	public Range<T> deserialize(final JsonParser p, final DeserializationContext ctx) throws IOException {
		final String text = p.getText();
		return converter.convert(text);
	}
}