/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.coll;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;
import me.magicall.program.lang.java.贵阳DearSun.coll.Tree.TreeNode;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class TreeNodeWrapper<E> extends TreeNodeTemplate<E> implements Wrapper<TreeNode<E>> {
	protected TreeNode<E> raw;

	public TreeNodeWrapper(final TreeNode<E> raw) {
		this.raw = raw;
	}

	@Override
	public E getElement() {
		return raw.getElement();
	}

	@Override
	public Tree<E> treeFromMe() {
		return raw.treeFromMe();
	}

	@Override
	public Stream<? extends TreeNode<E>> children() {
		return raw.children();
	}

	@Override
	public int countChildren() {
		return raw.countChildren();
	}

	@Override
	public Collection<? extends TreeNode<E>> getDescendants() {
		return raw.getDescendants();
	}

	@Override
	public int countDescendants() {
		return raw.countDescendants();
	}

	@Override
	public Collection<? extends Tree<E>> getSubTrees() {
		return raw.getSubTrees();
	}

	@Override
	public boolean isLeaf() {
		return raw.isLeaf();
	}

	@Override
	public List<? extends TreeNode<E>> pathToRoot() {
		return raw.pathToRoot();
	}

	@Override
	public List<? extends TreeNode<E>> pathFromRoot() {
		return raw.pathFromRoot();
	}

	@Override
	public int getLayer() {
		return raw.getLayer();
	}

	@Override
	public Collection<? extends TreeNode<E>> getSiblings() {
		return raw.getSiblings();
	}

	@Override
	public Collection<? extends TreeNode<E>> getLeafNodes() {
		return raw.getLeafNodes();
	}

	@Override
	public boolean isRoot() {
		return raw.isRoot();
	}

	@Override
	public boolean isChildOf(final TreeNode<E> maybeParent) {
		return raw.isChildOf(maybeParent);
	}

	@Override
	public TreeNode<E> child(final E child) {
		return raw.child(child);
	}

	@Override
	public TreeNode<E> addSubTree(final Tree<E> tree) {
		return raw.addSubTree(tree);
	}

	@Override
	public Map<E, TreeNode<E>> children(final Collection<E> children) {
		return raw.children(children);
	}

	@Override
	public E setElement(final E newElement) {
		return raw.setElement(newElement);
	}

	@Override
	public E clearElement() {
		return raw.clearElement();
	}

	@Override
	public TreeNode<E> parent() {
		return raw.parent();
	}

	@Override
	public TreeNode<E> unwrap() {
		return raw;
	}
}
