/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class CharKit extends PrimitiveKit<Character, char[]> implements Serializable {
	public static final CharKit INSTANCE = new CharKit();

	//------------------------------ ASCII码表
	/**
	 * 空字符（0）
	 */
	public static final char NULL = 0;
	/**
	 * 标题开始（1）
	 */
	public static final char START_HEAD_LINE = 1;
	/**
	 * 正文开始（2）
	 */
	public static final char START_OF_TEXT = 2;
	/**
	 * 正文结束（3）
	 */
	public static final char END_OF_TEXT = 3;
	/**
	 * 传输结束（4）
	 */
	public static final char END_OF_TRANSMISSION = 4;
	/**
	 * 请求（5）
	 */
	public static final char ENQUIRY = 5;
	/**
	 * 收到通知（6）
	 */
	public static final char ACKNOWLEDGE = 6;
	/**
	 * 响铃（7）
	 */
	public static final char BELL = 7;
	/**
	 * 退格（8、\b）
	 */
	public static final char BACKSPACE = 8;
	/**
	 * 水平制表符（9、\t）
	 */
	public static final char HORIZONTAL_TAB = 9;
	/**
	 * 换行（10、\n）
	 */
	public static final char NEW_LINE = 10;
	/**
	 * 换行（10、\n）
	 */
	public static final char LINE_FEED = NEW_LINE;
	/**
	 * 垂直制表符（11）
	 */
	public static final char VERTICAL_TAB = 11;
	/**
	 * 换页（12）
	 */
	public static final char NEW_PAGE = 12;
	/**
	 * 换页（12）
	 */
	public static final char FORM_FEED = NEW_PAGE;
	/**
	 * 回车（13、\r）
	 */
	public static final char CARRIAGE_RETURN = 13;
	/**
	 * 不用切换（14）
	 */
	public static final char SHIFT_OUT = 14;
	/**
	 * 启用切换（15）
	 */
	public static final char SHIFT_IN = 15;
	/**
	 * 数据链路转义（16）
	 */
	public static final char DATA_LINK_ESCAPE = 16;
	/**
	 * 设备控制1（17）
	 */
	public static final char DEVICE_CONTROL_1 = 17;
	/**
	 * 设备控制2（18）
	 */
	public static final char DEVICE_CONTROL_2 = 18;
	/**
	 * 设备控制3（19）
	 */
	public static final char DEVICE_CONTROL_3 = 19;
	/**
	 * 设备控制4（20）
	 */
	public static final char DEVICE_CONTROL_4 = 20;
	/**
	 * 拒绝接收（21）
	 */
	public static final char NEGATIVE_ACKNOWLEDGE = 21;
	/**
	 * 同步空闲（22）
	 */
	public static final char SYNCHRONOUS_IDLE = 22;
	/**
	 * 结束传输块（23）
	 */
	public static final char END_OF_TRANS_BLOCK = 23;
	/**
	 * 取消（24）
	 */
	public static final char CANCEL = 24;
	/**
	 * 媒介结束（25）
	 */
	public static final char END_OF_MEDIA = 25;
	/**
	 * 代替（26）
	 */
	public static final char SUBSTITUTE = 26;
	/**
	 * 换码/溢出（27）
	 */
	public static final char ESCAPE = 27;
	/**
	 * 文件分隔符（28）
	 */
	public static final char FILE_SEPARATOR = 28;
	/**
	 * 分组符（29）
	 */
	public static final char GROUP_SEPARATOR = 29;
	/**
	 * 记录分隔符（30）
	 */
	public static final char RECORD_SEPARATOR = 30;
	/**
	 * 单元分隔符（31）
	 */
	public static final char UNIT_SEPARATOR = 31;
	/**
	 * 空格（32）
	 */
	public static final char SPACE = 32;

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Character.class, char.class);
	private static final char EMPTY_VALUE = NULL;
	private static final int HALF_FULL_SIGN_DELTA = 65248;

	//=================================================

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	//--------------------------------------

	public boolean equalsIgnoreCase(final char c1, final char c2) {
		return Character.toLowerCase(c1) == Character.toLowerCase(c2);
	}

	@Override
	public Character parse(final String source) {
		return source.charAt(0);
	}

	@Override
	public <T1, T2> Character emptyVal() {
		return EMPTY_VALUE;
	}

	@Override
	public String primitiveArrFlag() {
		return "C";
	}

	//========================================

	/**
	 * 半角字符转全角字符（如有）。
	 *
	 * @param c 字符
	 * @return 全角字符（如有）
	 */
	public static char toFullWidth(final char c) {
		if (c == ' ') {
			return '\u3000';
		} else if (c < '\177') {
			return (char) (c + HALF_FULL_SIGN_DELTA);
		} else {
			return c;
		}
	}

	/**
	 * 全角字符转半角字符（如有）。
	 *
	 * @param c 字符
	 * @return 半角字符（如有）
	 */
	public static char toHalfWidth(final char c) {
		if (c == '\u3000') {
			return ' ';
		} else if (c > '\uFF00' && c < '｟') {
			return (char) (c - HALF_FULL_SIGN_DELTA);
		} else {
			return c;
		}
	}

	public static boolean in(final char c, final char... arr) {
		for (final char c1 : arr) {
			if (c1 == c) {
				return true;
			}
		}
		return false;
	}

	public static boolean isEnLetter(final char ch) {
		return isEnUpperCase(ch) || isEnLowerCase(ch);
	}

	public static boolean isEnLetter(final int ch) {
		return isEnUpperCase(ch) || isEnLowerCase(ch);
	}

	public static boolean isEnUpperCase(final char ch) {
		return 'A' <= ch && ch <= 'Z';
	}

	public static boolean isEnUpperCase(final int ch) {
		return 'A' <= ch && ch <= 'Z';
	}

	public static boolean isEnLowerCase(final char ch) {
		return 'a' <= ch && ch <= 'z';
	}

	public static boolean isEnLowerCase(final int ch) {
		return 'a' <= ch && ch <= 'z';
	}

	//=====================================================
	@Serial
	private Object readResolve() {
		return INSTANCE;
	}

	@Serial
	private static final long serialVersionUID = -8786078246893893279L;
}
