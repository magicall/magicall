/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf;

/**
 * 在一个java包中会出现的事物。
 */
@FunctionalInterface
public interface Accessible {
	AccessLv accessLv();

	default boolean is(final AccessLv accessLv) {
		return accessLv() == accessLv;
	}

	default boolean isPublic() {
		return is(AccessLv.PUBLIC);
	}

	default boolean isDefaultAccess() {
		return is(AccessLv.DEFAULT);
	}

	default boolean isProtected() {
		return is(AccessLv.PROTECTED);
	}

	default boolean isPrivate() {
		return is(AccessLv.PRIVATE);
	}

	default boolean isAccessibleBy(final Object o) {
		return false;//todo
	}

	default boolean isAccessibleBy(final Class<?> c) {
		return false;//todo
	}
}
