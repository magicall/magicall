/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.program.lang.java.贵阳DearSun;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class FloatKit extends NumKit<Float, float[]> implements Serializable {
	@Serial
	private static final long serialVersionUID = 3697543384462295628L;

	private static final List<Class<?>> SUPPORTED_CLASSES = Arrays.asList(Float.class, float.class);

	public static final FloatKit INSTANCE = new FloatKit();

	@Override
	public Stream<Class<?>> supportedClasses() {
		return SUPPORTED_CLASSES.stream();
	}

	@Override
	public Float parse(final String source) {
		try {
			return Float.parseFloat(source.trim());
		} catch (final NumberFormatException e) {
			return null;
		}
	}

	@Override
	public <T1, T2> Float emptyVal() {
		return 0.0F;
	}

	@Override
	public String primitiveArrFlag() {
		return "F";
	}

	@Override
	public Float fromLong(final long value) {
		return (float) value;
	}

	private Object readResolve() {
		return INSTANCE;
	}
}
