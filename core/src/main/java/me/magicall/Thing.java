/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Date;
import java.util.Objects;

/**
 * 事物。
 * 由（类型，在该类型的事物集里的id）二元组唯一标识。（暂时未统一成URI（统一资源标识符））
 */
public interface Thing {
	String UNKNOWN_SIGN = "?";
	String STR_TYPE_SIGN = "''";
	String NUM_TYPE_SIGN = "0";
	String BOOL_TYPE_SIGN = "bool";
	String TIME_TYPE_SIGN = "time";
	Thing UNKNOWN = of(UNKNOWN_SIGN, UNKNOWN_SIGN);
	/**
	 * 类型和id都为空字符串的东西。可以用在一些特殊用途。
	 */
	Thing EMPTY = of("", "");

	String type();

	String idInType();

	//=============================================

	static Thing of(final String type, final Object idInType) {
		return new SimpleThing(type, idInType == null ? UNKNOWN_SIGN : idInType.toString());
	}

	static Thing ofUnknownType(final Object idInType) {
		return of(UNKNOWN_SIGN, idInType);
	}

	static Thing ofStr(final String s) {
		return of(STR_TYPE_SIGN, s);
	}

	static Thing ofNum(final Number n) {
		return of(n.getClass().getSimpleName(), n);
	}

	static Thing ofBool(final Boolean b) {
		return of(BOOL_TYPE_SIGN, b);
	}

	static Thing ofTime(final Date d) {
		return of(d.getClass().getSimpleName(), d.getTime());
	}

//	static Thing ofTime(final TemporalAccessor t) {
//		return of(TIME_TYPE_SIGN, t);//todo：待实现
//	}

	static Thing ofObj(final Object val) {
		return of(val.getClass().getName(), val);
	}

	static Thing ofType(final String type) {
		return new SimpleThing(type, "");
	}

	static Thing parseThing(final String s) {
		final var index = s.indexOf('#');
		if (index < 0) {
			return null;
		}
		final var idInType = s.substring(index + 1);
		return of(s.substring(0, index), idInType);
	}

	static String str(final Thing thing) {
		return StrKit.format("{0}#{1}", thing.type(), thing.idInType());
	}

	static int hash(final Thing thing) {
		return Objects.hash(thing.type(), thing.idInType());
	}

	static boolean eq(final Thing thing, final Object o) {
		if (thing == o) {
			return true;
		}
		if (o instanceof final Thing other) {
			return Objects.equals(thing.type(), other.type()) && Objects.equals(thing.idInType(), other.idInType());
		}
		return false;
	}
}
