/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import com.google.common.collect.Lists;
import me.magicall.language.semantic.nlp.port.GrammarRole;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface NlpKit {
	/**
	 * 当需要把拆分后的词语重新组装成词组或子句时，若存在词语跨越的情况，使用此此占位符代替被跨域的词语。
	 * 例：“取得资格的”->“取得…的”。
	 */
	char MISSING_CONTENT_PART_PLACEHOLDER = '…';

	EnumSet<TextPartRelation> ADJUNCT_RELATIONS//
			= EnumSet.of(TextPartRelation.ATTRIBUTE,//
			TextPartRelation.ADVERBIAL,//
			TextPartRelation.COMPLEMENT,//
			TextPartRelation.LEFT_ADJUNCT,//
			TextPartRelation.RIGHT_ADJUNCT,//
			TextPartRelation.PUNCTUATION);

	static boolean isAdjunct(final GrammarRole grammarRole) {
		return ADJUNCT_RELATIONS.contains(grammarRole);
	}

	static List<TextPart> contract(final TextPart textPart) {
		final List<TextPart> rt = Lists.newArrayList();
		textPart.children().forEach(child -> {
			final var roleInParent = child.roleInParent();
			if (roleInParent == SentencePartRole.SUBJECT//
					|| roleInParent == SentencePartRole.PREDICATE//
					|| roleInParent == SentencePartRole.DIRECT_OBJECT//
					|| roleInParent == SentencePartRole.INDIRECT_OBJECT) {
				addPredicatePathLeaf(rt, child.children());
			}
		});
		return rt;
	}

	static void addPredicatePathLeaf(final List<TextPart> toAddTo, final Stream<? extends TextPart> textParts) {
		textParts.filter(textPart -> textPart.roleInParent() == SentencePartRole.PREDICATE)//
				.forEach(textPart -> {
					final Collection<? extends TextPart> children = textPart.children().collect(Collectors.toList());
					if (children.isEmpty()) {
						toAddTo.add(textPart);
					} else {
						addPredicatePathLeaf(toAddTo, children.stream());
					}
				});
	}

	static void addPredicatePathLeaf(final List<TextPart> toAddTo, final TextPart... textParts) {
		addPredicatePathLeaf(toAddTo, Stream.of(textParts));
	}
}
