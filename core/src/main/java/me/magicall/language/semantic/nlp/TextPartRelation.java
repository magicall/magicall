/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import me.magicall.language.semantic.nlp.port.GrammarRole;

/**
 * 文本部分之间的关系。
 *
 * @deprecated 用CommonGrammarRole
 */
@Deprecated
public enum TextPartRelation implements GrammarRole {
	/**
	 * 核心关系，用于指示根与句子核心词语的特殊关系。
	 */
	ROOT(SentencePartRole.SENTENCE, SentencePartRole.PREDICATE),
	/**
	 * 独立结构。两个单句在结构上彼此独立
	 */
	INDEPENDENT_STRUCTURE(SentencePartRole.SENTENCE, SentencePartRole.SENTENCE),
	/**
	 * 主谓。例：我送她一束花 (我 <– 送)
	 */
	SUBJECT_VERB(SentencePartRole.PREDICATE, SentencePartRole.SUBJECT),
	/**
	 * 动宾关系（直接宾语）。例：我送她一束花 (送 –> 花)
	 */
	VERB_OBJECT(SentencePartRole.PREDICATE, SentencePartRole.DIRECT_OBJECT),
	/**
	 * 间宾关系（间接宾语）。例：我送她一束花 (送 –> 她)
	 */
	INDIRECT_OBJECT(SentencePartRole.PREDICATE, SentencePartRole.INDIRECT_OBJECT),
	/**
	 * 前置宾语。例：他什么书都读 (书 <– 读)
	 */
	FRONTING_OBJECT(SentencePartRole.PREDICATE, SentencePartRole.DIRECT_OBJECT),
	/**
	 * 兼语。例：他请我吃饭 (请 –> 我)
	 */
	DOUBLE(TextPartRole.UNKNOWN, TextPartRole.UNKNOWN),
	/**
	 * 定中关系。例：红苹果 (红 <– 苹果)
	 */
	ATTRIBUTE(TextPartRole.UNKNOWN, SentencePartRole.ATTRIBUTE),
	/**
	 * 状中关系。例：非常美丽 (非常 <– 美丽)
	 */
	ADVERBIAL(SentencePartRole.PREDICATE, SentencePartRole.ADVERBIAL),
	/**
	 * 动补关系。例：做完了作业 (做 –> 完)
	 */
	COMPLEMENT(SentencePartRole.PREDICATE, SentencePartRole.COMPLEMENT),
	/**
	 * 并列关系。例：大山和大海 (大山 –> 大海)
	 */
	COORDINATE(TextPartRole.UNKNOWN, TextPartRole.UNKNOWN),
	/**
	 * 介宾关系。例：在贸易区内 (在 –> 内)
	 */
	PREPOSITION_OBJECT(SentencePartRole.PREDICATE, SentencePartRole.DIRECT_OBJECT),
	/**
	 * 左附加关系。例：大山和大海 (和 <– 大海)
	 */
	LEFT_ADJUNCT(TextPartRole.UNKNOWN, SentencePartRole.ADJUNCT),
	/**
	 * 右附加关系。例：孩子们 (孩子 –> 们)
	 */
	RIGHT_ADJUNCT(TextPartRole.UNKNOWN, SentencePartRole.ADJUNCT),
	/**
	 * 标点。
	 */
	PUNCTUATION(TextPartRole.UNKNOWN, SentencePartRole.ADJUNCT);

	/**
	 * 前导/父级节点应有的角色。
	 */
	public final TextPartRole fromRole;
	/**
	 * 本节点的角色。
	 */
	public final TextPartRole myRole;

	TextPartRelation(final TextPartRole fromRole, final TextPartRole myRole) {
		this.fromRole = fromRole;
		this.myRole = myRole;
	}

	public TextPartRole getFromRole() {
		return fromRole;
	}

	public TextPartRole getMyRole() {
		return myRole;
	}
}
