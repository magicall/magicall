/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import me.magicall.Child;
import me.magicall.Parent;
import me.magicall.language.linguistics.Characteristic;

import java.util.List;

/**
 * 句子结构分析器。
 *
 * @deprecated 使用 {@link TextAnalyzer}
 */
@Deprecated
@FunctionalInterface
public interface SentenceStructureAnalyzer {
	/**
	 * 从文本中分析句子结构。文本可能包含多个句子，将为每个句子返回各自的句子结构。
	 *
	 * @return
	 */
	List<SentenceStructure> analyse(String text);

	/**
	 * 句子结构。
	 */
	interface SentenceStructure {
		/**
		 * 原句。
		 *
		 * @return
		 */
		String getSentence();

		/**
		 * 根节点。通常是一个动词。
		 *
		 * @return
		 */
		SentenceUnit getRoot();
	}

	/**
	 * 句子单元。
	 */
	interface SentenceUnit extends Child<SentenceUnit>, Parent<SentenceUnit> {
		/**
		 * 本单元与父单元的关系名。应当为所有的名称建立枚举，然后通过本关系名获取枚举值来进行进一步使用。
		 *
		 * @return
		 */
		String getRelationToParent();

		/**
		 * 上下文。即原句。
		 *
		 * @return
		 */
		String getContext();

		/**
		 * 本单元词语在原句中的位置。
		 *
		 * @return
		 */
		int getOffset();

		/**
		 * 本单元所包含的文本。
		 * 注意：可能是多个词语甚至是句子，所以用text而非word。
		 *
		 * @return
		 */
		String getText();

		/**
		 * 词性。
		 *
		 * @return
		 */
		Characteristic getCharacteristic();
	}
}
