/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import java.util.Objects;

public class ContentWithOffset {
	public final String content;
	public final int offset;

	public ContentWithOffset(final String content, final int offset) {
		this.content = content;
		this.offset = offset;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ContentWithOffset that = (ContentWithOffset) o;
		return offset == that.offset && content.equals(that.content);
	}

	@Override
	public int hashCode() {
		return Objects.hash(content, offset);
	}

	@Override
	public String toString() {
		return content + '@' + offset;
	}
}
