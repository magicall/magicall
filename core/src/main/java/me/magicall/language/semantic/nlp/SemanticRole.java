/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import com.google.common.collect.Sets;
import me.magicall.Named;

import java.util.Arrays;
import java.util.Collection;

/**
 * 通用语义角色。
 * 不是所有的NLP的语义角色表都相同。本表期望全集，不同NLP可缺失不同部分。
 * 参考：
 * 哈工大LTP：http://ltp.ai/docs/appendix.html#id4
 * 云孚科技：https://www.yunfutech.com/demo?tab=7
 * 腾讯TexSmart：https://ai.tencent.com/ailab/nlp/texsmart/table_html/table8.html
 * 注：这些文档有些语焉不详。按其举例实测，有些词语（主要是文档靠后部分的那些令人迷惑的角色）的结果也与文档不匹配，所返回的基本上也是“施事者”、“受事者”这些。
 */
public enum SemanticRole implements Named {
	核心词,//
	/**
	 * 行为/谓语
	 */
	行为(核心词),//
	/**
	 * 主语的角色。
	 */
	主体,//
	/**
	 * 施事者。指发出具体行为动作的主体或动作持续以及表现心理活动的有意识的主体，具有主观能动性。
	 * 例：【政府】鼓励个人投资服务业。
	 */
	施事者(主体),//
	/**
	 * 当事者。指非行为动作的发出者，不具有主观能动性。包括无意识运动的主体、非可控运动的主体以及各种关系的主体。
	 * LTP 例：【宝塔】很高。
	 */
	当事者(主体),//
	/**
	 * 宾语的角色。
	 */
	客体,//
	/**
	 * 受事者。指受主体的行为动作所改变的直接客体。一般与施事相对应，是受施事影响而导致位置、性质、结果等发生改变的对象，最明显的语义特征是具有被动性。
	 * LTP 例：从桌子上把【书】拿走。
	 * 例：政府鼓励【个人】投资服务业。
	 */
	受事者(客体),//
	/**
	 * 指事件所涉及但是并未改变的客体以及动作行为产生的新事物或结果。
	 * LTP 例：我相信苏珊的【话】。
	 */
	客事(客体),//
	/**
	 * 也称与事，是动作行为的非主动参与者，也可以说，涉事角色是语义事件的第三方参与者。
	 * 从语义上说，是事件接受者、伴随者、来源者以及比较的基准等等；从句法上说，经常出现在双宾语句中的间接宾语位置。
	 * LTP 例：班长给【他】一套工具。
	 */
	涉事(客体),//
	/**
	 * 是跟事件的参与者有关系的客体。一般由表示某种关联的动词连接主客体，表达主客体之间的某种具体的、或抽象的关系。
	 * LTP 例：弟弟是【中学生】。
	 */
	系事(客体),//
	/**
	 * 工具。是事件中所使用的工具。典型的工具角色主要由介词“用”等引出，通常是由物体充当，但是一些有生命的、或者是抽象的事物也可以作为事件所凭借的工具。
	 * LTP 例：妈妈用【砂锅】熬稀饭。
	 */
	工具,//
	/**
	 * 是事件中所使用的材料。
	 * LTP 例：学生们用【纸】折飞机。
	 */
	材料,//
	/**
	 * 方式。包括事件中出现的方式、方法以及事件所依照的根据、凭借。
	 * LTP 例：军士【齐声】高喊。
	 * 例：以便他能继续【作为俄罗斯官员】从事他在一个特殊机构中的工作。
	 */
	方式,//
	/**
	 * 范围。指事件中所关涉的方面、限定的界限、被审视的角度、发生作用的范围，通常都为抽象名词。一般由不同的介词引出，可以出现在“在……方面”、“在……角度上”、“在……中”等的结构中。
	 * LTP 例：数学【方面】他是专家。
	 * 例：政府鼓励个人【投资服务业】。
	 */
	范围,//
	/**
	 * 包括引起事件发生或发展变化的原因以及事件所要达到的目的。
	 * LTP 例：他因为堵车迟到了。（迟到→堵车）
	 */
	缘由,//
	/**
	 * 目的。
	 * LTP 例：执政党和在野党【为了应付这场攻守战】都发出了紧急动员令。
	 */
	目的,//
	/**
	 * 时间。指事件发生所涉及到的各种时间因素。
	 * LTP 例：周一【早上】升旗。（“周一”对“早上”是修饰）
	 * 例：【下星期】布什将提出一项周密计划。
	 */
	时间,//
	/**
	 * 空间。指事件所涉及到的各种空间因素。
	 * LTP 例：我明天去【哈尔滨】。
	 * 例：请听美国之音特邀记者康妮【在加拿大温哥华】发来的报道。
	 */
	空间,//
	/**
	 * LTP 度量。指事件中的数量，名量或动量。
	 * 例：一年有【365】天。
	 * 例：每年创汇【１００万】美元。
	 */
	度量,//
	/**
	 * 表示事件发生时的情形、状态等。
	 * LTP 例：人类是从【类人猿】进化来的吗？。
	 */
	状态,//
	修饰语,
	/**
	 * 修饰。包括描写主体属性、特征的标签以及时间修饰语和名词修饰语的标签。
	 * LTP 例：她是个【漂亮】的女孩。
	 */
	定语(修饰语),//
	/**
	 * 状语、附加的
	 * LTP 例：我们【即将】迎来新年。
	 */
	状语(修饰语),//
	/**
	 * starting point——开始点？
	 * LTP 例：巴基斯坦【对谈判前景】表示悲观。
	 */
	开始点,//
	/**
	 * end point——结束点？
	 * LTP 例：产检部门将产检时间缩短到【一至三天】。
	 */
	结束点,//
	/**
	 * 受益人
	 * LTP 例：义务【为学童及老师】做超音波检查 。
	 */
	受益人,//
	/**
	 * 条件
	 * LTP 例：【如果早期发现】，便能提醒当事人注意血压的变化。
	 */
	条件,//
	/**
	 * 程度
	 * LTP 例：贫铀弹含有放射性比铀强【20万倍】。
	 */
	程度,//
	/**
	 * 方向。
	 * LTP 例：【从此处】我们可以发现寇克斯报告的精髓。
	 */
	方向,//
	/**
	 * 语气连接，连词（ltp称为“会话标记”）
	 * LTP 例：警方上午针对目击者做笔录，【而】李士东仍然不见踪影。
	 */
	连词,//
	/**
	 * 范围扩展
	 * LTP 例：回归３年多【来】，香港成为台商对大陆贸易的财务运作及资金调度中心。
	 */
	EXTENT,//
	/**
	 * 频率
	 * LTP 例：这类听证会在赖昌兴拘押期间每３０天举行【一次】。
	 */
	频率,//
	/**
	 * 话题
	 * LTP 例：【这么大的事情】，你怎么不和我说？
	 * 【广西对外开放】成绩斐然。
	 */
	话题,//
	/**
	 * 持有者
	 */
	持有者,//
	/**
	 * 被持有者
	 */
	被持有者,//
	/**
	 * 第二谓词
	 * 例：广州立志实业有限公司集种植、加工、贸易为【一体】。（LTP为“系事”）
	 */
	SECONDARY_PREDICATION,//
	/**
	 * aspect——方面？
	 * 例：说【着】说着就大哭起来 。
	 */
	方面,//
	/**
	 * 并列。指两个或多个平行的语义事件，重在叙述和描写。
	 * LTP 例：跟南韩、跟美国谋求和平关系的举动也更加积极。（美国→南韩）
	 * 兄弟俩边走边说。（走→说）
	 */
	并列,//
	/**
	 * 指在时间、空间上发生有序的事件或在逻辑、语义上关联性较强的先行发生的事件。
	 * A是B的先行含义等同于B是A的后继，提供前者还是后者，取决于哪个词出现在前。
	 * LTP 例：要想成功，就要努力。（“努力”是“成功”的先行）
	 */
	先行,
	/**
	 * 主要描述语义上更进一步的结果类事件，包括时间、空间或逻辑、语义上后续发生的事件。
	 * LTP 例：他穿好衣服，走出门去。（“走”是“穿”的后继）
	 */
	后继,
	/**
	 * 是对事件中否定关系的标记。
	 * LTP 例：她不像她母亲。（“不”是“像”的否定）
	 */
	否定,
	/**
	 * 是对句法事件中各种关系的标记，如一些连词、介词等。
	 * LTP 例：但是我没有放弃看书。（“但是”是“放弃”的（与前半句谓语的）表示关系的标记）
	 */
	关系,
	/**
	 * 是概念或事件的语义依附的形式标记，如一些助词，的得地。
	 * LTP 例：他在这里住了三十年。（“了”是“住”的依附）
	 */
	依附,
	/**
	 * 是对语义事件中的出现的标点符号的标注。
	 * LTP 例：他喜欢音乐。（“。”是“喜欢”（核心词）的标点）
	 */
	标点,
	;

	private final Collection<SemanticRole> parents = Sets.newHashSet();

	SemanticRole(final SemanticRole... parents) {
		addParents(Arrays.asList(parents));
	}

	private void addParents(final Collection<SemanticRole> parents) {
		parents.forEach(e -> {
			this.parents.add(e);
			addParents(e.parents);
		});
	}

	public boolean is(final SemanticRole maybeAncestor) {
		return this == maybeAncestor || parents.stream().anyMatch(e -> e.is(maybeAncestor));
	}
}
