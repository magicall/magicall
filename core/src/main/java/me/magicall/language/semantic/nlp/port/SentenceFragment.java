/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp.port;

import me.magicall.text.TextPart;

import java.util.stream.Stream;

/**
 * （语法）句子片段。
 * 是上下文（{@link #context()}）中的一个部分，在语法分析中有意义，即，是上下文的一个结构成分。可能是一个句子、从句、词语等。
 * AI分析语义的基础是句子。比句子更大的片段，会切为句子。所以我们做ai适配组件，其实只要关注句子级别。
 * 注意！！！句子片段代表的是一个片段，即该片段的全文，而非仅仅代表该片段的直接文本。在计算“在上下文中的位置”之类的方法时需要特别注意。
 * 对于一个句子，其所有后代节点中不仅包含词语节点，还包含从句、短语等。
 */
public interface SentenceFragment<_Part extends SentenceFragment<_Part>> extends TextPart<_Part> {
	@Override
	default String title() {
		return "";
	}

	String directContent();

	String fullContent();

	@Override
	default String content() {
		return fullContent();
	}

	/**
	 * 上下文。
	 *
	 * @return
	 */
	@Override
	SentenceFragment<_Part> context();

	/**
	 * 在上下文（{@link #context()}）中的位置，以本片段全文（{@link #fullContent()}）的首字出现位置为准，从1开始。
	 * 注意：本部分的内容文本（{@link #content()}）在上下文（{@link #context()}）中可能出现多次，但它们不是同一个文本结构部分（{@link SentenceFragment}）
	 *
	 * @return
	 */
	int placeInContext();

	/**
	 * 语法结构的子级。按其在父级中的位置（{@link #placeInContext()}）的先后排序。
	 *
	 * @return
	 */
	@Override
	Stream<_Part> parts();

	/**
	 * 本片段在上下文中的语法角色。
	 *
	 * @return
	 */
	GrammarRole grammarRole();

	/**
	 * 本片段是否一个句子或从句。
	 * 若本片段有主语或宾语，即认为是。
	 *
	 * @return
	 */
	default boolean isSentenceOrClause() {
		return parts().anyMatch(e -> {
			final var grammarRole = e.grammarRole();
			return grammarRole == CommonGrammarRole.主语 || grammarRole == CommonGrammarRole.宾语;
		});
	}

	//===================================================

	static String toString(final SentenceFragment<?> part) {
		return TextPart.str(part);
	}

	static int hash(final SentenceFragment<?> part) {
		return TextPart.hash(part);
	}

	static boolean equals(final SentenceFragment<?> part, final Object other) {
		return TextPart.eq(part, other) && other instanceof SentenceFragment;
	}
}
