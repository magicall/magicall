/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * 对自然语言处理的支持包。
 * <p>
 * 一些缩写：
 * NLP：Natural Language Processing，自然语言处理。
 * NER：Named Entity Recognition，具名实体识别，很多地方也称“命名实体识别”。
 * POS：Part Of Speech，词性，即“名词”、“动词”等。
 * SRL：Semantic Role Labeling，语义角色标注。
 * CASE：Context-Aware Semantic Expansion，上下文相关的语义联想。
 * CRF：Conditional Random Field，条件随机场。
 * DNN：Deep Neural Network，深度神经网络。
 * <p>
 * 多个在线NLP平台：
 * 腾讯 TexSmart 文本理解工具与服务：https://ai.tencent.com/ailab/nlp/texsmart/zh/index.html
 * 哈工大 LTP ：http://ltp.ai
 * 云孚科技：https://www.yunfutech.com
 * 腾讯AI开放平台 基础文本分析：https://ai.qq.com/product/nlpbase.shtml
 */
package me.magicall.language.semantic.nlp;
