/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

/**
 * 现代汉语的几种句子成分，外加几个特殊的。
 */
public enum SentencePartRole implements TextPartRole {
	/**
	 * 句子、子句或从句。
	 */
	SENTENCE,
	/**
	 * 主语
	 */
	SUBJECT,
	/**
	 * 谓语
	 */
	PREDICATE,
	/**
	 * 直接宾语
	 */
	DIRECT_OBJECT,
	/**
	 * 间接宾语
	 */
	INDIRECT_OBJECT,
	/**
	 * 定语
	 */
	ATTRIBUTE,
	/**
	 * 状语
	 */
	ADVERBIAL,
	/**
	 * 补语
	 */
	COMPLEMENT,
	/**
	 * 附加成分，比如的、地、得。
	 */
	ADJUNCT,
	/**
	 * 未知
	 */
	UNKNOWN
}
