/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import me.magicall.Child;
import me.magicall.Parent;
import me.magicall.judge.Judgement;
import me.magicall.language.semantic.nlp.port.SentenceFragment;
import me.magicall.text.Text;

import java.util.Objects;

/**
 * 文本部分，可能是字、词、词组、句、段落、章、篇、书、卷、集……等。
 * Term、Token这些词要么太专用，要么用得太滥，我不想用。
 *
 * @deprecated 使用 {@link SentenceFragment}
 */
@Deprecated
public interface TextPart extends Child<TextPart>, Parent<TextPart>, Text {

	String myCoreWord();

	default int placeInParent() {
		return isRoot() ? 0 : offsetInText() - parent().offsetInText();
	}

	/**
	 * 在上一层级中的角色。
	 * 比如句子成分：主语、谓语等。
	 * 比如，【美丽的地球是我们的家园】，“美丽”的上一层级是“美丽的地球”，在其中的角色是“定语”。
	 * 注意：上一层级不一定是句子本身，可能只是其中一个短语或从句；
	 * 在更高的层级中，要么不再属于一种确定角色（比如动宾短语中的宾语，不一定是整句的宾语），要么依然是相同角色（比如定语、状语等）。
	 *
	 * @return
	 */
	TextPartRole roleInParent();

	/**
	 * 全文（？）
	 *
	 * @return
	 */
	default String text() {
		return isRoot() ? content() : parent().text();
	}

	/**
	 * 在全文中的起始位置。
	 * this.offsetText = this.isRoot ? 0 : this.placeInParent + parent.offsetInText
	 *
	 * @return
	 */
	int offsetInText();

	/**
	 * 在上下文中所呈现的词性。（名词、动词、形容词），同一个词在不同上下文中词性可能不同。
	 * 比如，【韦小宝掏出“含沙射影”】中，光从句子中无法识别“含沙射影”的真正词性，要从更大的上下文中，才能知道它是一个名词（一个暗器机关的名字）。
	 * 词组、句子也可以有词性，比如名词性词组、定语从句是形容词。
	 *
	 * @return
	 */
	TextPartType type();

	/**
	 * 在上下文中的实体类型。若本文本部分在上下文中表示一个实体，则返回实体类型，否则返回null。
	 * 注：WordType里只有几个是实体类型。
	 *
	 * @return
	 */
	EntityType entityType();

	/**
	 * 在上下文中是否实体。
	 *
	 * @return
	 */
	default boolean isEntity() {
		return entityType() != null;
	}

	/**
	 * 在上下文中所表达的情感。
	 *
	 * @return
	 */
	default Judgement<String, Sentiment> sentiment() {
		return null;//todo：暂不实现。
	}

	/**
	 * 实现类一定要重写toString方法，且返回{@link #content()}的值。
	 *
	 * @return
	 */
	@Override
	String toString();

	static String toString(final TextPart one) {
		return one.content();
	}

	static int hash(final TextPart one) {
		return Objects.hash(one.offsetInText(), one.content());
	}

	static boolean equals(final TextPart one, final Object other) {
		if (!(other instanceof final TextPart o)) {
			return false;
		}
		return o.offsetInText() == one.offsetInText() && o.content().equalsIgnoreCase(one.content());
	}
}
