/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp;

import me.magicall.language.semantic.nlp.port.TextStructureAnalyzer;

/**
 * 文本分析器。
 *
 * @deprecated 使用 {@link TextStructureAnalyzer}
 */
@Deprecated
@FunctionalInterface
public interface TextAnalyzer {
	/**
	 * 分析出文本结构树，每个节点都是一个{@link TextPart}，返回根节点。
	 * 以句子为例：
	 * * 1，根节点将是文本本身。
	 * * 2，二级节点是每个并列谓语。
	 * * 3，三级节点是这些谓语各自的主语列表、宾语列表、状语列表。
	 * * 4，四级节点是主语、宾语的定语。
	 *
	 * @return
	 */
	TextPart analyze(String text);
}
