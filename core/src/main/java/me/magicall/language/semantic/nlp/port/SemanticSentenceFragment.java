/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.semantic.nlp.port;

import com.google.common.collect.Multimap;
import me.magicall.language.semantic.nlp.SemanticRole;

/**
 * 语义句子的部分。
 * 此时语义角色具有了意义。
 */
public interface SemanticSentenceFragment extends SentenceFragment<SemanticSentenceFragment> {
	/**
	 * 语义角色（{@link SemanticRole}，施事者、受事者之类）。
	 * 非在直接上下文（{@link #context()}）中：直接上下文是语法结构中的上下文。
	 * 在语义上，一个片段可能同时是多个片段的某种或多种语义角色，这些片段可能在语法结构上相距甚远。
	 * 比如“我吃饭、唱歌、跳舞”中的“我”，是“吃饭、唱歌、跳舞”三个子句的施事者；“他看我吃饭”中的“我”（语法角色为兼语）是“他看我”中的受事者，是“我吃饭”的施事者。
	 * 与属性集（{@link #props()}）相对。
	 *
	 * @return
	 */
	Multimap<SemanticRole, SemanticSentenceFragment> semanticRoles();

	/**
	 * 属性集。暂名。
	 * 键为属性名，值为该属性的值的片段（若干个，不为空）。
	 * 与语义角色（{@link #semanticRoles()}）相对。
	 *
	 * @return
	 */
	Multimap<SemanticRole, SemanticSentenceFragment> props();
	//	/**
//	 * 谓语。
//	 * 可能为空，比如当子级是好多个分句时。
//	 *
//	 * @return
//	 */
//	Predicate predicate();
//
//	/**
//	 * 主语集。
//	 *
//	 * @return
//	 */
//	SubjectsPart subjects();
//
//	/**
//	 * 直接宾语集。
//	 *
//	 * @return
//	 */
//	DirectObjectsPart directObjects();
//
//	/**
//	 * 间接宾语集。
//	 *
//	 * @return
//	 */
//	IndirectObjectsPart indirectObjects();
}
