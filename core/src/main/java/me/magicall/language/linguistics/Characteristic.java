/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import com.google.common.collect.Sets;
import me.magicall.language.semantic.nlp.SemanticRole;
import me.magicall.language.semantic.nlp.TextPartType;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

/**
 * 词性。
 * 参考
 * <a href="https://www.yunfutech.com/demo?tab=7">哈工大LTP</a>
 * <a href="http://ltp.ai/docs/appendix.html#id2">云孚科技</a>（注意前者比后者多了一个简写为"z"的形容词，不知何故。）
 * <a href="https://ai.tencent.com/ailab/nlp/texsmart/table_html/table6.html">腾讯TexSmart</a>
 * todo:availableRoles应该还不是很准。以后要好好研究
 * todo: 研究对 SemanticRole 的依赖是否有必要。最后要移到magicall工程中。
 */
public enum Characteristic implements TextPartType {
	/**
	 * 未知词性。
	 */
	UNKNOWN {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return EnumSet.allOf(SemanticRole.class);
		}
	},//0
	/**
	 * 动词
	 */
	VERB {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.行为);
		}
	},//1
	/**
	 * 名词
	 */
	NOUN {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(//
					SemanticRole.主体, SemanticRole.施事者, SemanticRole.持有者,//主
					SemanticRole.客体, SemanticRole.受事者, SemanticRole.被持有者, SemanticRole.客事, SemanticRole.受益人,//宾
					SemanticRole.系事, SemanticRole.涉事,//
					SemanticRole.工具, SemanticRole.方向, SemanticRole.方式, SemanticRole.时间, SemanticRole.材料,
					SemanticRole.条件, SemanticRole.状态, SemanticRole.目的, SemanticRole.程度, SemanticRole.空间,
					SemanticRole.缘由, SemanticRole.范围, SemanticRole.话题, SemanticRole.频率);
		}
	},//2
	/**
	 * 专有名词
	 */
	PROPER_NOUN(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return ancestors.stream().map(Characteristic::getAvailableRoles)
					.collect(() -> EnumSet.noneOf(SemanticRole.class), Collection::addAll, Collection::addAll);
		}
	},//3
	/**
	 * 一般名词。如：苹果
	 */
	GENERAL_NOUN(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return ancestors.stream().map(Characteristic::getAvailableRoles)
					.collect(() -> EnumSet.noneOf(SemanticRole.class), Collection::addAll, Collection::addAll);
		}
	},//4
	/**
	 * 方位名词。如：右侧
	 */
	DIRECTION(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.方向);
		}
	},//5
	/**
	 * 人名。如：杜甫 汤姆
	 */
	HUMAN_NAME(PROPER_NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(//
					SemanticRole.主体, SemanticRole.施事者, SemanticRole.持有者,//主
					SemanticRole.客体, SemanticRole.受事者, SemanticRole.被持有者, SemanticRole.客事, SemanticRole.受益人,//宾
					SemanticRole.系事, SemanticRole.涉事);
		}
	},//6
	/**
	 * 机构种类名。如：保险公司
	 */
	ORG_NAME(PROPER_NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(//
					SemanticRole.主体, SemanticRole.施事者, SemanticRole.持有者,//主
					SemanticRole.客体, SemanticRole.受事者, SemanticRole.被持有者, SemanticRole.客事, SemanticRole.受益人,//宾
					SemanticRole.系事, SemanticRole.涉事);
		}
	},//7
	/**
	 * 位置名词。如：城郊
	 */
	LOCATION(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(//
					SemanticRole.主体, SemanticRole.施事者, SemanticRole.持有者,//主
					SemanticRole.客体, SemanticRole.受事者, SemanticRole.被持有者, SemanticRole.客事, SemanticRole.受益人,//宾
					SemanticRole.系事, SemanticRole.涉事,//
					SemanticRole.方向, SemanticRole.空间, SemanticRole.范围);
		}
	},//8
	/**
	 * 地理名词。如：北京
	 */
	GEOGRAPHICAL(PROPER_NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(//
					SemanticRole.主体, SemanticRole.施事者, SemanticRole.持有者,//主
					SemanticRole.客体, SemanticRole.受事者, SemanticRole.被持有者, SemanticRole.客事, SemanticRole.受益人,//宾
					SemanticRole.系事, SemanticRole.涉事,//
					SemanticRole.方向, SemanticRole.空间, SemanticRole.范围);
		}
	},//9
	/**
	 * 时间名词。如：近日 明代
	 */
	TEMPORAL(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.时间);
		}
	},//10
	/**
	 * 缩写。如：公检法
	 */
	ABBREVIATION(NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return NOUN.getAvailableRoles();
		}
	},//11
	/**
	 * 外来词。如：CPU
	 */
	FOREIGN_WORDS {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return EnumSet.allOf(SemanticRole.class);
		}
	},//12
	/**
	 * 其他专有名词。如：诺贝尔奖
	 */
	OTHER_PROPER(PROPER_NOUN) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return PROPER_NOUN.getAvailableRoles();
		}
	},//13
	/**
	 * 代词。如：本法
	 */
	PRONOUN {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return NOUN.getAvailableRoles();
		}
	},//14
	/**
	 * 名词修饰词
	 */
	NOUN_MODIFIER {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.定语);
		}
	},//15
	/**
	 * 形容词。如：美丽
	 */
	ADJECTIVE(NOUN_MODIFIER) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.定语);
		}
	},//16
	/**
	 * 其他名词修饰语。如：大型 西式
	 */
	OTHER_NOUN_MODIFIER(NOUN_MODIFIER) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return NOUN_MODIFIER.getAvailableRoles();
		}
	},//17
	/**
	 * 数词。如：一 第一
	 */
	NUMBER {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.度量, SemanticRole.频率);
		}
	},//18
	/**
	 * 量词。如：个
	 */
	QUANTITY {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.度量, SemanticRole.频率);
		}
	},//19
	/**
	 * 习语，包括成语等。如：百花齐放
	 */
	IDIOM {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return EnumSet.allOf(SemanticRole.class);
		}
	},//20
	/**
	 * 连词。如：和 虽然
	 */
	CONJUNCTION {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.连词);
		}
	},//21
	/**
	 * 副词。如：很
	 */
	ADVERB {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.状语);
		}
	},//22
	/**
	 * 介词。如：在 把
	 */
	PREPOSITION {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.连词);//todo：好像没有合适的
		}
	},//23
	/**
	 * 拟声词。如：哗啦
	 */
	ONOMATOPOEIA {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.开始点, SemanticRole.结束点);//todo：好像没有合适的
		}
	},//24
	/**
	 * 叹词。如：哎
	 */
	EXCLAMATION(ONOMATOPOEIA) {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.开始点, SemanticRole.结束点);//todo：好像没有合适的
		}
	},//25
	/**
	 * 助词。如：的 地
	 */
	AUXILIARY {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.EXTENT);//todo:是吗？
		}
	},//26
	/**
	 * 词素。如：茨 甥
	 */
	MORPHEME {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of();
		}
	},//27
	/**
	 * 前缀。如：阿 伪
	 */
	PREFIX {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.EXTENT);//todo:是吗？
		}
	},//28
	/**
	 * 后缀。如：界 率
	 */
	SUFFIX {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.EXTENT);//todo:是吗？
		}
	},//29
	/**
	 * 非词素。如：萄 翱
	 */
	NON_LEXEME {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of();
		}
	},//30
	/**
	 * 标点
	 */
	PUNCTUATION {
		@Override
		public Set<SemanticRole> getAvailableRoles() {
			return Set.of(SemanticRole.连词);
		}
	},//31
	;

	final Collection<Characteristic> ancestors = Sets.newHashSet();

	Characteristic(final Characteristic... ancestors) {
		addAncestors(Arrays.asList(ancestors));
	}

	private void addAncestors(final Collection<Characteristic> ancestors) {
		ancestors.forEach(e -> {
			this.ancestors.add(e);
			addAncestors(e.ancestors);
		});
	}

	public boolean is(final Characteristic other) {
		return ancestors.stream().anyMatch(e -> e == other || e.is(other));
	}

	public abstract Set<SemanticRole> getAvailableRoles();//todo:不要这方法
}
