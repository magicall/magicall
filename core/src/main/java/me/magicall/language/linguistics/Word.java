/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import me.magicall.Named;
import me.magicall.text.Text;

import java.util.stream.Stream;

/**
 * 词/词语/词汇。
 * 现实生活中有些词语在不同范围甚至相同范围内有多种意思，词性可能不同也可能相同。表现为一个“词语（{@link Word}）”对应多个“词义（{@link WordMeaning}）”。
 * 有些词语有同义词或近义词；有些词语有反义词。
 * 有些词语有“上/下级”，比如“动物”和“猫”。在一个范围里，一个词只有一个直属上级。
 */
public interface Word extends Named, Text {
	/**
	 * 词语的名字。不会为空。
	 *
	 * @return
	 */
	@Override
	String name();

	/**
	 * 词语的内容，即词语的名字。注意，不是词语的意思。词语的意思是 {@link  WordMeaning#content()} 。
	 *
	 * @return
	 */
	@Override
	default String content() {
		return name();
	}

	/**
	 * 词义集。
	 *
	 * @return
	 */
	Stream<? extends WordMeaning> meanings();

	Word dropMeaning(WordMeaning meaning);

	Word addMeaning(WordMeaning meaning);

	//========================================

	static String toString(final Word one) {
		return one.name();
	}

	static int hash(final Word one) {
		return one.name().hashCode();
	}

	static boolean equals(final Word one, final Object o) {
		return o instanceof final Word w && one.name().equals(w.name());
	}
}
