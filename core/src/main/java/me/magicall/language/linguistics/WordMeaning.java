/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.scope.Scoped;
import me.magicall.text.Text;
import me.magicall.text.article.Source;

import java.util.Objects;

/**
 * 词义。词语的一个确定性的义项。
 */
public interface WordMeaning extends Scoped, Text {
	/**
	 * 所属词语。
	 *
	 * @return
	 */
	Word word();

	/**
	 * 词性。
	 *
	 * @return
	 */
	Characteristic characteristic();

	/**
	 * 词义的完整解释。
	 * 注意：不是词语本身的文本。
	 *
	 * @return
	 */
	@Override
	String content();

	/**
	 * 出处。
	 *
	 * @return
	 */
	Source source();

	//===============================

	static String toString(final WordMeaning one) {
		final var scope = one.scope();
		final var wordName = one.word().name();
		final var characteristic = one.characteristic();
		final var content = one.content();
		return scope == null ? StrKit.format("{0}（{1}）：{2}", wordName, characteristic, content)
												 : StrKit.format("{0}@{1}（{2}）：{3}", wordName, scope, characteristic, content);
	}

	static int hash(final WordMeaning one) {
		return Objects.hash(one.word(), one.scope(), one.characteristic(), one.content());
	}

	static boolean equals(final WordMeaning one, final Object o) {
		if (one == o) {
			return true;
		}
		if (!(o instanceof final WordMeaning other)) {
			return false;
		}
		return one.word().equals(other.word())//
				&& Objects.equals(one.scope(), other.scope())//
				&& one.characteristic() == other.characteristic()//
				&& one.content().equals(other.content());
	}
}
