/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import me.magicall.scope.Scope;
import me.magicall.text.article.Source;

public class WordMeaningDto implements WordMeaning {
	public Word word;
	public Scope scope;
	public Characteristic characteristic;
	public String content;
	public Source source;

	@Override
	public Word word() {
		return word;
	}

	@Override
	public Characteristic characteristic() {
		return characteristic;
	}

	@Override
	public String content() {
		return content;
	}

	@Override
	public Source source() {
		return source;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public WordMeaningDto putIn(final Scope newScope) {
		scope = newScope;
		return this;
	}

	@Override
	public String toString() {
		return WordMeaning.toString(this);
	}

	@Override
	public int hashCode() {
		return WordMeaning.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return WordMeaning.equals(this, o);
	}
}
