/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import me.magicall.biz.UniqNameResourceCrudServices;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
import me.magicall.scope.Scope;

import java.util.stream.Stream;

/**
 * 词典。
 * 即“词语服务”。
 */
public interface Dictionary extends UniqNameResourceCrudServices<Word, Word> {
	@Override
	default String resourceName() {
		return "词语";
	}

	/**
	 * 在字典里查找词语。
	 *
	 * @return
	 */
	default Word find(final String wordName) {
		return filter(e -> e.content().equals(wordName)).findFirst().orElse(null);
	}

	default Word get(final String wordName) {
		final var one = find(wordName);
		if (one == null) {
			throw new NoSuchThingException(resourceName(), wordName);
		}
		return one;
	}

	@Override
	default Word ensure(final Word one) {
		return learn(one);
	}

	/**
	 * 学习新的词语，确保词典中存在该词语。
	 * 若词典中已有词语，则返回已有词语。
	 *
	 * @return
	 */
	Word learn(Word word);

	/**
	 * 同义词集。若无同义词，则返回空集。
	 *
	 * @return
	 */
	Stream<Word> synonymsOf(Word word);

	/**
	 * 反义词集。若无反义词，则返回空集。
	 *
	 * @return
	 */
	Stream<Word> antonymsOf(Word word);

	default Stream<Word> ofScope(final Scope scope) {
		return filter(word -> word.meanings().anyMatch(meaning -> meaning.scope().isIn(scope)));
	}

	//todo:暂不提供：根据词义描述、范围、词性等排列组合获取词义对象或词语对象。
}
