/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.language.linguistics;

import java.util.List;
import java.util.stream.Stream;

public class WordDto implements Word {
	public String name;
	public List<WordMeaning> meanings;

	@Override
	public String name() {
		return name;
	}

	@Override
	public WordDto renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public Stream<WordMeaning> meanings() {
		return meanings == null ? Stream.empty() : meanings.stream();
	}

	@Override
	public WordDto dropMeaning(final WordMeaning meaning) {
		meanings.remove(meaning);
		return this;
	}

	@Override
	public WordDto addMeaning(final WordMeaning meaning) {
		meanings.add(meaning);//暂未检查meaning的word
		return this;
	}

	@Override
	public String toString() {
		return Word.toString(this);
	}

	@Override
	public int hashCode() {
		return Word.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Word.equals(this, o);
	}
}
