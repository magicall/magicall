/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

/**
 * 简单接口。实现本接口，意味着有描述（{@link #description}）。
 */
public interface Described<_MyType extends Described<_MyType>> {

	/**
	 * 描述。
	 *
	 * @return
	 */
	default String description() {
		return "";
	}

	/**
	 * 重新描述。若不支持重新描述，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 */
	default _MyType describedAs(final String description) {
		throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, "describe",
				Thing.of(Described.class.getSimpleName(), description()));
	}
}
