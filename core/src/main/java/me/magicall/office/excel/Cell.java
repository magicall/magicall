/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel;

public interface Cell {
	default Excel excel() {
		return sheet().excel();
	}

	default Sheet sheet() {
		return row().sheet();
	}

	/**
	 * 所属行。
	 *
	 * @return
	 */
	Row row();

	/**
	 * 所属列。
	 *
	 * @return
	 */
	Col col();

	/**
	 * 取值。
	 *
	 * @return
	 */
	<T> T val();

	/**
	 * 设置值。
	 *
	 * @return
	 */
	Cell val(Object val);

	/**
	 * 在行中的下标，从行的{@link Row#indexBase()}开始计数。
	 *
	 * @return
	 */
	int indexInRow();

	/**
	 * 在列中的下标，从列的{@link Col#indexBase()}开始计数。
	 *
	 * @return
	 */
	int indexInCol();

	default boolean isFirstCellInRow() {
		return indexInCol() == row().sheet().indexBase();
	}

	default Cell leftCell() {
		return isFirstCellInRow() ? null : row().cellAt(indexInCol() - 1);
	}

	default Cell rightCell() {
		return row().cellAt(indexInCol() + 1);
	}
}
