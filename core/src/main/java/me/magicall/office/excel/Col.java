/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel;

import java.util.List;
import java.util.stream.Stream;

public interface Col {
	default int indexBase() {
		return sheet().indexBase();
	}

	/**
	 * 所属表。
	 *
	 * @return
	 */
	Sheet sheet();

	int indexInSheet();

	/**
	 * 格子列表。
	 * 包含空白格子。以最后一个有值的格子视为结束。
	 *
	 * @return
	 */
	Stream<Cell> cells();

	default int countCells() {
		return (int) cells().count();
	}

	default boolean isEmpty() {
		return countCells() == 0;
	}

	default Cell firstNotEmptyCell() {
		return cells().filter(e -> e.val() != null).findFirst().orElse(null);
	}

	/**
	 * 最后一个有值的格子。
	 *
	 * @return
	 */
	default Cell lastNotEmptyCell() {
		final List<Cell> cells = cells().toList();
		if (cells.isEmpty()) {
			return null;
		}
		return cells.get(cells.size() - 1);
	}

	/**
	 * 查找第n个格子。从{@link #indexBase()}开始。
	 * 不会返回null。
	 * 若n超过最后一个有值的格子（即， {@link #cells()} 返回结果不包含该格子），则返回一个空白格子，若之后在该格子上写入数据，则此后 {@link #cells()} 返回结果将包含该格子。
	 *
	 * @return
	 */
	Cell cellAt(int n);

	/**
	 * 用指定内容，在最后一个有值格子之后创建新格子。
	 *
	 * @return
	 */
	Col append(Object... contents);

	default boolean isFirstCol() {
		return indexInSheet() == sheet().indexBase();
	}

	default Col preCol() {
		return isFirstCol() ? null : sheet().colAt(indexInSheet() - 1);
	}

	default Col nextCol() {
		return sheet().colAt(indexInSheet() + 1);
	}
}
