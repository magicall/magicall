/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel;

import me.magicall.Named;

import java.util.List;
import java.util.stream.Stream;

/**
 * 注意：所有未标明的index都从1开始，这样才符合日常生活认知（从0开始是编程领域的知识）。
 * 尤其对表格来说，我们平时不会说“第0行”、“第0列”，excel界面中也是从1开始。从0开始容易在边界值处引起混乱。不应该额外增加使用者负担。
 */
public interface Sheet extends Named {
	/**
	 * 返回所属的excel。
	 *
	 * @return
	 */
	Excel excel();

	/**
	 * 下标（行、列序号）的起始数值，默认为1。
	 *
	 * @return
	 */
	default int indexBase() {
		return excel().indexBase();
	}

	/**
	 * 从第一行开始，到有值的最后一行（含），包括中间的空白行。
	 *
	 * @return
	 */
	Stream<Row> notEmptyRows();

	default boolean isEmpty() {
		return notEmptyRows().findAny().isEmpty();
	}

	/**
	 * 第一个非空行。
	 * 工作表为空，返回null。
	 *
	 * @return
	 */
	default Row firstNotEmptyRow() {
		return notEmptyRows().filter(e -> !e.isEmpty()).findFirst().orElse(null);
	}

	/**
	 * 返回最后一个有值的行。
	 * 若所有行都无值，则返回null。
	 *
	 * @return
	 */
	default Row lastNotEmptyRow() {
		final List<Row> rows = notEmptyRows().toList();
		if (rows.isEmpty()) {
			return null;
		}
		return rows.get(rows.size() - 1);
	}

	/**
	 * 获取第n行（从{@link #indexBase()}开始）。
	 * 不会返回null。
	 * 若n超过最后一个有值行的行数（即， {@link #notEmptyRows()} 返回结果不包含该行），则返回一个空白行，若之后在该行上写入数据，则此后 {@link #notEmptyRows()} 返回结果将包含该行。
	 * 注意：目前获取过的行视为有值。
	 *
	 * @return
	 */
	Row rowAt(int n);

	/**
	 * 在最后一个有值行之后加一行，把指定的内容列表依次加到该行，每个内容一个格子。
	 *
	 * @return
	 */
	Row appendRow(Stream<?> contents);

	default Stream<Row> appendRows(final Stream<Stream<?>> rows) {
		return rows.map(this::appendRow);
	}

	/**
	 * 在第n行（从{@link #indexBase()}开始）的位置上插入行，使之成为新的第n行。大于n的行往后顺延。
	 * 若原本有内容的行数m小于n，则中间会有（n-m-{@link #indexBase()}）个空行。
	 *
	 * @return
	 */
	Row insertRow(int n, Stream<?> contents);

	/**
	 * 从第一列开始，到有值的最后一列（含），包括中间的空白列。
	 *
	 * @return
	 */
	Stream<Col> notEmptyCols();

	/**
	 * 第一个非空列。
	 *
	 * @return
	 */
	default Col firstNotEmptyCol() {
		return notEmptyCols().filter(e -> !e.isEmpty()).findFirst().orElse(null);
	}

	/**
	 * 返回最后一个有值的列。
	 * 若所有列都无值，则返回null。
	 *
	 * @return
	 */
	default Col lastNotEmptyCol() {
		final List<Col> cols = notEmptyCols().toList();
		if (cols.isEmpty()) {
			return null;
		}
		return cols.get(cols.size() - 1);
	}

	/**
	 * 获取第n列（从{@link #indexBase()}开始）。
	 * 不会返回null。
	 * 若n超过最后一个有值列的列数（即， {@link #notEmptyCols()} 返回结果不包含该列），则返回一个空白列，若之后在该列上写入数据，则此后 {@link #notEmptyCols()} 返回结果将包含该列。
	 *
	 * @return
	 */
	Col colAt(int n);

	/**
	 * 在第n列（从{@link #indexBase()}开始）的位置上插入列，使之成为新的第n列。大于n的列往后顺延。
	 * 若原本有内容的列数m小于n，则中间会有（n-m-{@link #indexBase()}）个空列。
	 * 若contents长度比已有行数小，则多出来的行在此列的格子将会是空白格子。
	 *
	 * @return
	 */
	Col insertCol(int n, Stream<?> contents);

	/**
	 * 转置。
	 *
	 * @return
	 */
	Sheet transpose();
}
