/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.office.excel;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.io.OutputStream;
import java.util.stream.Stream;

@FunctionalInterface
public interface Excel {
	/**
	 * 下标（行、列序号）的起始数值，默认为1。
	 *
	 * @return
	 */
	default int indexBase() {
		return 1;
	}

	/**
	 * 获取所有工作表，按排位顺序。
	 *
	 * @return
	 */
	Stream<Sheet> sheets();

	/**
	 * 获取第n个工作表，从{@link #indexBase()}开始。
	 *
	 * @param n 从{@link #indexBase()}开始
	 * @return
	 */
	default Sheet sheetAt(final int n) {
		return sheets().skip(n - indexBase()).findFirst().orElse(null);
	}

	default Sheet findSheetNamed(final String name) {
		return sheets().filter(e -> e.name().equals(name)).findFirst().orElse(null);
	}

	default Sheet createSheet(final String name) {
		throw new OpNotAllowedException(Thing.of(Excel.class.getSimpleName(), toString()), "createSheet",
				Thing.of(Sheet.class.getSimpleName(), name));
	}

	default Sheet createSheet() {
		return createSheet("sheet " + (sheets().count() + indexBase()));
	}

	default void writeTo(final OutputStream outputStream) {
		throw new OpNotAllowedException(Thing.of(Excel.class.getSimpleName(), toString()), "write",
				Thing.of(outputStream.getClass().getSimpleName(), "?"));
	}
}
