/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 键值对。
 *
 * @author Liang Wenjian.
 */
public interface NameValue<V> extends Named {

	V val();

	/**
	 * 实现类可自行决定返回自身还是返回新对象。
	 *
	 * @return
	 */
	NameValue<V> withValue(V value);

	class NameValue_<V> implements NameValue<V> {
		private String name;
		private V value;

		public NameValue_() {
		}

		public NameValue_(final String name, final V value) {
			this.name = name;
			this.value = value;
		}

		@Override
		public String name() {
			return name;
		}

		@Override
		public V val() {
			return value;
		}

		@Override
		public NameValue_<V> withValue(final V value) {
			this.value = value;
			return this;
		}

		@Override
		public NameValue<V> renameTo(final String newName) {
			name = newName;
			return this;
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", NameValue_.class.getSimpleName() + '[', "]")//
					.add("name='" + name + '\'').add("value=" + value).toString();
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final NameValue_<?> that = (NameValue_<?>) o;
			return Objects.equals(name, that.name) && Objects.equals(value, that.value);
		}

		@Override
		public int hashCode() {
			return Objects.hash(name, value);
		}
	}
}
