/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.logic;

import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

/**
 * 概念。
 */
public interface Concept extends Named {
	/**
	 * 定义。
	 * 给人看的。
	 *
	 * @return
	 */
	String definition();

	/**
	 * 重新定义。若不支持重新定义，可直接返回本对象或抛出异常。
	 * 默认实现为抛出 {@link OpNotAllowedException}
	 *
	 * @return 新定义的概念。
	 */
	default Concept redefine(final String newDefinition) {
		throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, "redefine",
				Thing.of(Concept.class.getSimpleName(), definition()));
	}
}
