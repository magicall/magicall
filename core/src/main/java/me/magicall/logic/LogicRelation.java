/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.logic;

import java.util.function.Predicate;
import java.util.stream.Stream;

public enum LogicRelation {
	与 {
		public <T> boolean test(final Stream<? extends T> items, final Predicate<? super T> predicate) {
			return items.allMatch(predicate);
		}

		public boolean reduce(final Stream<Boolean> bools) {
			return bools.allMatch(Boolean::booleanValue);
		}
	},
	或 {
		public <T> boolean test(final Stream<? extends T> items, final Predicate<? super T> predicate) {
			return items.anyMatch(predicate);
		}

		public boolean reduce(final Stream<Boolean> bools) {
			return bools.anyMatch(Boolean::booleanValue);
		}
	},
	非(1);

	public final int operatorCount;

	LogicRelation() {
		this(2);
	}

	LogicRelation(final int operatorCount) {
		this.operatorCount = operatorCount;
	}
}
