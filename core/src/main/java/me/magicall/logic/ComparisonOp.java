/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.logic;

import me.magicall.Named;

import java.util.function.BiPredicate;

/**
 * 比较操作：判断两个对象是否符合某种逻辑。
 * 最常见的就是数学上的等于、大于、小于、大于等于、小于等于这一类比较符。
 */
public interface ComparisonOp<T1, T2> extends Named, BiPredicate<T1, T2> {

	ComparisonOp<Object, Object> 内存地址 = new ComparisonOp<>() {
		@Override
		public String name() {
			return "内存地址";
		}

		@Override
		public boolean test(final Object o, final Object o2) {
			return o == o2;
		}
	};
	ComparisonOp<Object, Object> 代码内相等 = new ComparisonOp<>() {
		@Override
		public String name() {
			return "代码内相等";
		}

		@Override
		public boolean test(final Object o, final Object o2) {
			return o.equals(o2);
		}
	};

	@SuppressWarnings("unchecked")
	static <T> ComparisonOp<T, T> instanceCompare() {
		return (ComparisonOp<T, T>) 内存地址;
	}

	@SuppressWarnings("unchecked")
	static <T> ComparisonOp<T, T> codeCompare() {
		return (ComparisonOp<T, T>) 代码内相等;
	}

	enum CommonStrComparisonOp implements ComparisonOp<String, String> {
		包含 {
			@Override
			public boolean test(final String s, final String s2) {
				return s.contains(s2);
			}
		},
		始于 {
			@Override
			public boolean test(final String s, final String s2) {
				return s.startsWith(s2);
			}
		},
		结于 {
			@Override
			public boolean test(final String s, final String s2) {
				return s.endsWith(s2);
			}
		},
		匹配 {
			@Override
			public boolean test(final String s, final String s2) {
				return s.matches(s2);
			}
		}
	}
}
