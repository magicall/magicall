/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.decision;

/**
 * 意见。
 */
public enum Opinion {
	/**
	 * 同意
	 */
	AGREED,
	/**
	 * 反对
	 */
	OPPOSED,
	/**
	 * 弃权
	 */
	ABSTAINED,
	/**
	 * 随大流：若同意的多于反对的则视为同意，若反对的多于同意的则视为反对，若同意反对相等则视为弃权。
	 */
	FOLLOWED
}
