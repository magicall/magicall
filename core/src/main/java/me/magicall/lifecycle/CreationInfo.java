/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.lifecycle;

import me.magicall.IdName;

import java.time.LocalDateTime;

/**
 * 创建信息。
 */
public class CreationInfo {
	private final IdName creator;
	private final LocalDateTime createTime;

	public CreationInfo(final Object id, final String name, final LocalDateTime createTime) {
		this(new IdName(String.valueOf(id), name), createTime);
	}

	public CreationInfo(final IdName creator, final LocalDateTime createTime) {
		this.creator = creator;
		this.createTime = createTime;
	}

	public IdName creator() {
		return creator;
	}

	public LocalDateTime createTime() {
		return createTime;
	}
}
