/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.lifecycle;

import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.Named;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * 关于发布的信息。
 */
public interface Publication {
	OffsetDateTime publishTime();

	Named publisher();

	static String str(final Publication publication) {
		return StrKit.format("{0}@{1}", publication.publisher(), publication.publishTime());
	}

	static boolean eq(final Publication publication, final Object other) {
		if (other instanceof final Publication o) {
			return publication.publisher().equals(o.publisher())//
					&& publication.publishTime().equals(o.publishTime());
		}
		return false;
	}

	static int hash(final Publication publication) {
		return Objects.hash(publication.publisher(), publication.publishTime());
	}
}
