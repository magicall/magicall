/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.lifecycle;

import me.magicall.Named;
import me.magicall.Named.SimpleNamed;

import java.time.OffsetDateTime;

public class SimplePublication implements Publication {
	private final PublicationDto raw;

	public SimplePublication(final Named publisher, final OffsetDateTime publishTime) {
		this(new PublicationDto(publisher, publishTime));
	}

	public SimplePublication(final String publisherName, final OffsetDateTime publishTime) {
		this(new PublicationDto(publishTime, new SimpleNamed(publisherName)));
	}

	public SimplePublication(final OffsetDateTime publishTime, final Named publisher) {
		this(new PublicationDto(publisher, publishTime));
	}

	public SimplePublication(final OffsetDateTime publishTime, final String publishName) {
		this(new PublicationDto(publishTime, new SimpleNamed(publishName)));
	}

	private SimplePublication(final PublicationDto raw) {
		this.raw = raw;
	}

	@Override
	public OffsetDateTime publishTime() {
		return raw.publishTime;
	}

	@Override
	public Named publisher() {
		return raw.publisher;
	}

	@Override
	public String toString() {
		return raw.toString();
	}

	@Override
	public int hashCode() {
		return raw.hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		return raw.equals(o);
	}
}
