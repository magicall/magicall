/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.lifecycle;

/**
 * 可锁定的。
 *
 * @author Liang Wenjian.
 */
public interface Lockable {

	boolean isLocked();

	default boolean isUnlocked() {
		return !isLocked();
	}

	Lockable lock();

	Lockable unlock();

	default Lockable switchLock() {
		return isLocked() ? unlock() : lock();
	}

	default Lockable switchLockTo(final boolean locked) {
		return locked ? lock() : unlock();
	}
}
