/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.lifecycle;

import me.magicall.Named;

import java.time.OffsetDateTime;

public class PublicationDto implements Publication {
	public Named publisher;
	public OffsetDateTime publishTime;

	public PublicationDto() {
	}

	public PublicationDto(final OffsetDateTime publishTime, final Named publisher) {
		this.publishTime = publishTime;
		this.publisher = publisher;
	}

	public PublicationDto(final Named publisher, final OffsetDateTime publishTime) {
		this(publishTime, publisher);
	}

	@Override
	public OffsetDateTime publishTime() {
		return publishTime;
	}

	@Override
	public Named publisher() {
		return publisher;
	}

	@Override
	public String toString() {
		return Publication.str(this);
	}

	@Override
	public int hashCode() {
		return Publication.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Publication.eq(this, o);
	}
}
