/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.Comparator;

/**
 * 有权重的。
 */
@FunctionalInterface
public interface HasWeight {

	float weight();

	/**
	 * 修改权重。
	 *
	 * @return
	 */
	default HasWeight weight(final float weight) {
		throw new OpNotAllowedException(Thing.of(HasWeight.class.getSimpleName(), toString()), "weight",
																		Thing.of("float", weight));
	}

	float HIGHEST_WEIGHT = Float.MAX_VALUE;
	float LOWEST_WEIGHT = Float.MIN_VALUE;
	float COMMON_WEIGHT = 0.0F;
	Comparator<HasWeight> COMPARATOR = Comparator.comparing(HasWeight::weight);

	@SuppressWarnings("unchecked")
	static <T extends HasWeight> Comparator<T> comparator() {
		return (Comparator<T>) COMPARATOR;
	}
}
