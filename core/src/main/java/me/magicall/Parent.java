/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 实现此接口，表示对象有“子对象”。
 * 注意：
 * 1，具体实现类可能允许或不允许拥有多个“相同”的子节点。若允许，则所有增加子节点的方法不会检查参数元素是否已存在于子节点列表中；若不允许则需要检查，以确保“相同”子节点只出现一次。
 * 2，若子节点类型也实现了{@link Child}，要保证子节点的{@link Child#parent()}方法的返回值为本对象。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Parent<_Child> {
	Stream<? extends _Child> children();

	default int countChildren() {
		return (int) children().count();
	}

	/**
	 * 领取子节点。
	 *
	 * @param children 要领取的子节点。
	 * @return 更新后的对象。
	 */
	default Parent<_Child> adopt(final Stream<? extends _Child> children) {
		throw new OpNotAllowedException(toOperator(), "adopt", toThing(children));
	}

	default Parent<_Child> adopt(final _Child... children) {
		return adopt(Stream.of(children));
	}

	/**
	 * 在子节点头部插入新的子节点。todo：使用更业务化的名字。
	 *
	 * @param children 要插入的子节点。
	 * @return 更新后的对象。
	 */
	default Parent<_Child> unshift(final Stream<? extends _Child> children) {
		throw new OpNotAllowedException(toOperator(), "unshift", toThing(children));
	}

	default Parent<_Child> unshift(final _Child... children) {
		return unshift(Stream.of(children));
	}

	default Parent<_Child> dropChildren(final Stream<? extends _Child> children) {
		throw new OpNotAllowedException(toOperator(), "dropChildren", toThing(children));
	}

	default Parent<_Child> dropChildren(final _Child... children) {
		return dropChildren(Stream.of(children));
	}

	default Parent<_Child> setChildren(final Stream<? extends _Child> children) {
		return dropChildren(children()).adopt(children);
	}

	default Integer findChildPlace(final _Child maybeChild) {
		return (int) children().takeWhile(Predicate.isEqual(maybeChild)).count();
	}

	default _Child findChild(final _Child maybeChild) {
		return children().filter(Predicate.isEqual(maybeChild)).findFirst().orElse(null);
	}

	private Thing toOperator() {
		return Thing.of("Parent", toString());
	}

	private static <_Child> Thing toThing(final Stream<? extends _Child> children) {
		return Thing.of("T", children.toList().toString());
	}
}
