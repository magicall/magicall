/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.domain;

import me.magicall.biz.UniqNameResourceCrudServices;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
import me.magicall.scope.Scoped;

import java.util.stream.Stream;

/**
 * 术语表。
 * 即“术语服务”。
 */
public interface Glossary<T extends Terminology> extends Scoped, UniqNameResourceCrudServices<T, T> {
	@Override
	default String resourceName() {
		return "术语";
	}

	/**
	 * 在术语表里查找术语。没有则返回null。
	 *
	 * @return
	 */
	T find(String name);

	/**
	 * 在术语表里查找术语。没有则抛出异常。
	 *
	 * @return
	 */
	default T get(final String name) {
		final var one = find(name);
		if (one == null) {
			throw new NoSuchThingException(resourceName(), name);
		}
		return one;
	}

	default boolean isExists(final String name) {
		return find(name) != null;
	}

	/**
	 * 学习新的术语，确保术语表中存在该术语。
	 * 等同于 {@link #learn(T)}
	 *
	 * @param one 要创建的对象。
	 * @return
	 */
	@Override
	default T ensure(final T one) {
		return learn(one);
	}

	/**
	 * 学习新的术语，确保术语表中存在该术语。
	 *
	 * @return
	 */
	T learn(T terminology);

	/**
	 * 废弃一个术语，确保术语表中不再存在该术语。
	 *
	 * @return
	 */
	@Override
	T drop(T terminology);

	@Override
	default Stream<T> drop(final Stream<T> some) {
		return some.map(this::drop);
	}

	/**
	 * 同义词集。若无同义词，则返回空集。
	 *
	 * @return
	 */
	Stream<T> synonymsOf(T terminology);

	/**
	 * 反义词集。若无反义词，则返回空集。
	 *
	 * @return
	 */
	Stream<T> antonymsOf(T terminology);
}
