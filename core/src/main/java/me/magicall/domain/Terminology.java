/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.domain;

import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.language.linguistics.Word;
import me.magicall.language.linguistics.WordMeaning;
import me.magicall.logic.Concept;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.scope.Scope;
import me.magicall.scope.Scoped;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 该领域内的术语。
 * 术语是一类特殊的词语，属于某个领域，且在该领域内有明确的定义。这种定义也正是术语作为词语的义项，因此术语通常只有一个义项。
 * 可能是从其他领域或公共领域引入的。若在其他领域内有同名词语，它们的义项会较为相似，但附带了领域背景知识，所以又不完全一致。
 * 在实现上，为了简单起见，令术语同时是词语和义项。
 */
public interface Terminology extends Named, Scoped, Word, WordMeaning, Concept {

	@Override
	Terminology redefine(String newDefinition);

	/**
	 * 术语的意义是确定的、唯一的，只能修改，不能增减。将抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 */
	@Override
	default Terminology dropMeaning(final WordMeaning meaning) {
		throw new OpNotAllowedException(toThing(), "dropMeaning", Thing.of("meaning", meaning.content()));
	}

	/**
	 * 术语的意义是确定的、唯一的，只能修改，不能增减。将抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 */
	@Override
	default Terminology addMeaning(final WordMeaning meaning) {
		throw new OpNotAllowedException(toThing(), "addMeaning", Thing.of("meaning", meaning.content()));
	}

	@Override
	default Word word() {
		return this;
	}

	/**
	 * 术语的内容，是术语的定义。
	 *
	 * @return
	 */
	@Override
	default String content() {
		return definition();
	}

	/**
	 * 术语所属领域（范围）。
	 * 义项的范围未必与之相同。
	 *
	 * @return
	 */
	@Override
	Scope scope();

	/**
	 * 义项。只有一个义项，即“定义（{@link #definition()}）”
	 *
	 * @return
	 */
	@Override
	default Stream<? extends Terminology> meanings() {
		return Stream.of(this);
	}

	private Thing toThing() {
		return Thing.of("Terminology", name());
	}
	//===================================================

	static String toString(final Terminology terminology) {
		return terminology.name();
	}

	static int hash(final Terminology terminology) {
		return Objects.hash(terminology.name(), terminology.scope());
	}

	static boolean equals(final Terminology terminology, final Object other) {
		if (other instanceof final Terminology o) {
			return terminology.name().equals(o.name()) && terminology.scope().equals(o.scope());
		}
		return false;
	}
}
