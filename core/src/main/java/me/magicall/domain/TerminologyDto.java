/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.domain;

import me.magicall.language.linguistics.Characteristic;
import me.magicall.scope.Scope;
import me.magicall.scope.Scoped;
import me.magicall.text.article.Source;

public class TerminologyDto implements Terminology {
	public String name;
	public Scope scope;
	public String definition;
	public Characteristic characteristic;
	public Source source;

	@Override
	public String name() {
		return name;
	}

	@Override
	public TerminologyDto renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public Scoped putIn(final Scope newScope) {
		scope = newScope;
		return this;
	}

	@Override
	public String definition() {
		return definition;
	}

	@Override
	public TerminologyDto redefine(final String newDefinition) {
		definition = newDefinition;
		return this;
	}

	@Override
	public Characteristic characteristic() {
		return characteristic;
	}

	@Override
	public Source source() {
		return source;
	}

	@Override
	public String toString() {
		return Terminology.toString(this);
	}

	@Override
	public int hashCode() {
		return Terminology.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Terminology.equals(this, other);
	}
}
