/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.domain;

import me.magicall.Op;
import me.magicall.event.Bulletin;
import me.magicall.event.Notice;
import me.magicall.event.Plan;
import me.magicall.event.PlanPublisher;
import me.magicall.event.SimpleTarget;
import me.magicall.language.linguistics.Characteristic;
import me.magicall.language.linguistics.Dictionary;
import me.magicall.language.linguistics.Word;
import me.magicall.language.linguistics.WordMeaning;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.exception.EmpStrException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.scope.Scope;
import me.magicall.text.article.Source;

import java.util.stream.Stream;

public class SimpleGlossary implements Glossary<Terminology> {
	private final Dictionary dictionary;
	private final Scope scope;
	private final PlanPublisher planPublisher;
	private final Bulletin bulletin;

	public SimpleGlossary(final Dictionary dictionary, final Scope scope, final PlanPublisher planPublisher,
												final Bulletin bulletin) {
		this.dictionary = dictionary;
		this.scope = scope;
		this.planPublisher = planPublisher;
		this.bulletin = bulletin;
	}

	@Override
	public Terminology find(final String name) {
		return wrap(dictionary.find(name));
	}

	@Override
	public Terminology learn(final Terminology terminology) {
		checkName(terminology.name());
		checkDefinition(terminology.definition());
		checkScope(terminology.scope());
		final var plan = Plan.from(this)//
				.goingTo(Op.ADD)//
				.against(new SimpleTarget<>(this, resourceName(), terminology)).build();
		planPublisher.publish(plan);
		final var rt = wrap(dictionary.learn(terminology));
		bulletin.announce(Notice.resultOf(plan, rt));
		return rt;
	}

	@Override
	public Stream<Terminology> streaming() {
		return dictionary.ofScope(scope).map(this::wrap);
	}

	@Override
	public Terminology drop(final Terminology terminology) {
		final var plan = Plan.from(this)//
				.goingTo(Op.CLEAR)//
				.against(new SimpleTarget<>(this, resourceName(), terminology)).build();
		planPublisher.publish(plan);
		final var rt = wrap(dictionary.drop(terminology));
		bulletin.announce(Notice.resultOf(plan, rt));
		return rt;
	}

	@Override
	public Stream<Terminology> synonymsOf(final Terminology terminology) {
		return null;//todo
	}

	@Override
	public Stream<Terminology> antonymsOf(final Terminology terminology) {
		return null;//todo
	}

	@Override
	public Scope scope() {
		return scope;
	}

	public PlanPublisher planPublisher() {
		return planPublisher;
	}

	public Bulletin bulletin() {
		return bulletin;
	}

	private static void checkName(final String name) {
		if (Kits.STR.isEmpty(name)) {
			throw new EmpStrException("name");
		}
	}

	private static void checkDefinition(final String definition) {
		if (Kits.STR.isEmpty(definition)) {
			throw new EmpStrException("definition");
		}
	}

	private void checkScope(final Scope scope) {
		if (!this.scope.isIncluding(scope)) {
			throw new WrongArgException("scope", scope, "<=" + this.scope);
		}
	}

	private Terminology wrap(final Word word) {
		return word == null ? null : new TerminologyImpl(word);
	}

	private class TerminologyImpl implements Terminology {
		private Word word;
		private WordMeaning wordMeaning;

		private TerminologyImpl(final Word word) {
			this.word = word;
			checkMeaning();
		}

		private void checkMeaning() {
			wordMeaning = word.meanings().findFirst().orElseThrow(UnknownException::new);
		}

		@Override
		public String name() {
			return word.name();
		}

		@Override
		public Terminology renameTo(final String newName) {
			checkName(newName);
			if (name().equals(newName)) {
				return this;
			}
			final var plan = Plan.from(this)//
					.goingTo(Op.UPDATE)//
					.against(new SimpleTarget<>(this, "name", newName)).build();
			planPublisher.publish(plan);

			word = (Word) word.renameTo(newName);

			final var rt = this;
			bulletin.announce(Notice.resultOf(plan, rt));
			return rt;
		}

		@Override
		public String definition() {
			return wordMeaning.content();
		}

		@Override
		public Terminology redefine(final String newDefinition) {
			checkDefinition(newDefinition);
			if (!definition().equals(newDefinition)) {
				word.dropMeaning(wordMeaning);
				word.addMeaning(new WordMeaning() {
					@Override
					public Word word() {
						return word;
					}

					@Override
					public Characteristic characteristic() {
						return wordMeaning.characteristic();
					}

					@Override
					public String content() {
						return newDefinition;
					}

					@Override
					public Source source() {
						return wordMeaning.source();
					}

					@Override
					public Scope scope() {
						return wordMeaning.scope();
					}
				});
				checkMeaning();
				bulletin.announce(Notice.from(this)//
						.done(Op.UPDATE)//
						.against(new SimpleTarget<>(this, "definition", newDefinition))//
						.got(wordMeaning).build());
			}
			return this;
		}

		@Override
		public Scope scope() {
			return wordMeaning.scope();
		}

		@Override
		public Characteristic characteristic() {
			return wordMeaning.characteristic();
		}

		@Override
		public Source source() {
			return wordMeaning.source();
		}

		@Override
		public String toString() {
			return Terminology.toString(this);
		}

		@Override
		public int hashCode() {
			return Terminology.hash(this);
		}

		@Override
		public boolean equals(final Object other) {
			return Terminology.equals(this, other);
		}
	}
}


