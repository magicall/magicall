/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import com.google.common.collect.Range;
import me.magicall.opinion.Opinion;

import java.util.stream.Stream;

/**
 * 主观确信度/客观可信度的级别。
 * 如果需要用一个统一的词来同时涵盖“主观判断的确定程度”和“客观上的可信度”，一般可以使用“credibility”（可信度）。
 * 虽然“credibility”更经常与信息、说法或人的信誉和信任度关联，它既可以应用于个人的主观信念或判断，也可以用于描述某个观点、数据或消息的客观可信性。
 * 因此，“credibility”既能反映主观信心的层面，也能覆盖到客观可靠性的方面，使其成为一个比较全面的选择。
 * ——ChatGPT4
 */
public enum CredibilityLv {
	完全不确定(Range.singleton(Opinion.CERTAINTY_INDEFINITE)),
	不完全确定(Range.open(Opinion.CERTAINTY_INDEFINITE, Opinion.CERTAINTY_CONFIRMED)),
	完全确定(Range.singleton(Opinion.CERTAINTY_CONFIRMED)),
	;

	public final Range<Float> reliabilityRange;

	CredibilityLv(final Range<Float> reliabilityRange) {
		this.reliabilityRange = reliabilityRange;
	}

	public boolean contains(final float reliability) {
		return reliabilityRange.contains(reliability);
	}

	//==========================================================

	public static CredibilityLv of(final float reliability) {
		return Stream.of(values()).filter(e -> e.reliabilityRange.contains(reliability)).findFirst().orElse(null);
	}
}
