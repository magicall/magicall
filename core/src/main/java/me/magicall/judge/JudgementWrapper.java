/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

import java.time.Instant;
import java.util.stream.Stream;

public record JudgementWrapper<_Against, _Result>(Judgement<_Against, _Result> raw)
		implements Judgement<_Against, _Result>, Wrapper<Judgement<_Against, _Result>> {

	@Override
	public Judgement<_Against, _Result> unwrap() {
		return raw;
	}

	@Override
	public _Against against() {
		return raw.against();
	}

	@Override
	public _Result result() {
		return raw.result();
	}

	@Override
	public boolean isNegative() {
		return raw.isNegative();
	}

	@Override
	public Judge<_Against, _Result> getJudge() {
		return raw.getJudge();
	}

	@Override
	public Stream<Thing> bases() {
		return raw.bases();
	}

	@Override
	public float certainty() {
		return raw.certainty();
	}

	@Override
	public Instant judgeTime() {
		return raw.judgeTime();
	}

	@Override
	public String toString() {
		return Judgement.str(this);
	}

	@Override
	public int hashCode() {
		return Judgement.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Judgement.eq(this, other);
	}
}
