/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import com.google.common.collect.Range;
import me.magicall.Thing;
import me.magicall.opinion.Opinion;

import java.time.Instant;

/**
 * 判断者。
 */
public interface Judge<_Against, _Result> extends Thing {//todo：against似乎可以直接使用Thing？

	/**
	 * 是否可判断指定事物。
	 *
	 * @return
	 */
	default boolean canJudge(final _Against against) {
		return judge(against) != null;
	}

	/**
	 * 判断。当下对指定事物的判断
	 * “非当下”可能是过去的判断，也可能是将来的判断（若支持“将来的判断”）。
	 *
	 * @return 不会返回null。
	 */
	Judgement<_Against, _Result> judge(final _Against against);

	/**
	 * 学习一个新结论。
	 * 可以拒绝接受该判断。
	 * 可以根据参数的判断者、确信度、依据等信息决定本判断者是否接受该判断。
	 * 默认实现：不做任何事（不学习）。
	 *
	 *
	 * @return 是否学到了。若选择拒绝则返回false。若未学会应抛异常。
	 */
	default boolean learn(final Judgement<_Against, _Result> judgementFromOther) {
		return false;
	}

	/**
	 * 丢弃针对指定事物的所有判断。
	 * 丢弃后，{@link #judge(_Against)}将返回空。
	 * 默认实现：不做任何事（不丢弃）。
	 *
	 * @return 是否已不存在针对指定事物的判断。若丢弃失败应抛异常。
	 */
	default boolean dropJudgementAgainst(final _Against against) {
		return false;
	}

	/**
	 * 丢弃指定时间做出的针对指定事物的所有判断。
	 * 默认实现：不做任何事（不丢弃）。
	 *
	 * @return 是否已不存在指定时间对指定事物的判断。若丢弃失败应抛异常。
	 */
	default boolean dropJudgementAgainst(final _Against against, final Instant judgeTime) {
		return false;
	}

	/**
	 * 丢弃针对指定事物、结果为指定结果的所有判断。
	 * 默认实现：不做任何事（不丢弃）。
	 *
	 * @return 是否已不存在针对指定事物、结果为指定结果的判断。若丢弃失败应抛异常。
	 */
	default boolean dropJudgementAgainst(final _Against against, final _Result result) {
		return false;
	}

	/**
	 * 丢弃指定判断。
	 * 若判断非本判断者判断，则抛异常。
	 * 默认实现：不做任何事（不丢弃）。
	 *
	 * @return 是否已不存在该判断。若丢弃失败应抛异常。
	 */
	default boolean dropJudgement(final Judgement<_Against, _Result> judgement) {
		return false;
	}

	/**
	 * 客观置信度范围。本判断者做出的每个判断的确信度在此范围内。
	 *
	 * @return
	 */
	default Range<Float> certaintyRange() {
		return Opinion.CERTAINTY_FULL_RANGE;
	}
}
