/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import me.magicall.Thing;

import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

public class JudgementDto<_Against, _Result> implements Judgement<_Against, _Result> {
	public _Against against;
	public _Result result;
	public boolean isNegative;
	public Judge<_Against, _Result> judge;
	public List<Thing> bases;
	public float certainty;
	public Instant judgeTime;

	@Override
	public _Against against() {
		return against;
	}

	@Override
	public _Result result() {
		return result;
	}

	@Override
	public boolean isNegative() {
		return isNegative;
	}

	@Override
	public Judge<_Against, _Result> getJudge() {
		return judge;
	}

	@Override
	public Stream<Thing> bases() {
		return bases == null ? Stream.empty() : bases.stream();
	}

	@Override
	public float certainty() {
		return certainty;
	}

	@Override
	public Instant judgeTime() {
		return judgeTime;
	}

	@Override
	public String toString() {
		return Judgement.str(this);
	}

	@Override
	public int hashCode() {
		return Judgement.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Judgement.eq(this, other);
	}
}
