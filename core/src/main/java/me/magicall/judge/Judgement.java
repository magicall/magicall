/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import me.magicall.Thing;
import me.magicall.opinion.Opinion;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 对某事的判断。
 * “相反”的结果有两种构造：
 * 1，把确信度改为相反值。
 * 2，把结果改为“相反”的结果。
 * todo：所有修改方法，应由判断者操作，而不是在判断对象上直接操作。暂时保留，待删
 */
public interface Judgement<_Against, _Result> extends Opinion<_Result> {
	String TYPE = Judgement.class.getSimpleName();

	/**
	 * 判断针对的事物。
	 *
	 * @return
	 */
	_Against against();

	@Override
	default _Result content() {
		return result();
	}

	/**
	 * 判断结果。
	 *
	 * @return
	 */
	_Result result();

	/**
	 * 修改判断结果。若不支持修改，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> changeResult(final _Result newResult) {
		return notAllow(OpNotAllowedException.UNKNOWN_THING, "changeResult");
	}

	/**
	 * 判官/判断者：做出本次判断的事物。
	 *
	 * @return
	 */
	Judge<_Against, _Result> getJudge();

	/**
	 * 判断依据。
	 *
	 * @return
	 */
	Stream<Thing> bases();

	/**
	 * 修改判断依据。若不支持修改，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> changeBases(final Stream<Thing> bases) {
		return notAllow(OpNotAllowedException.UNKNOWN_THING, "changeBases");
	}

	/**
	 * 添加判断依据。若不支持修改，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> addBases(final Stream<Thing> bases) {
		return notAllow(OpNotAllowedException.UNKNOWN_THING, "addBases");
	}

	/**
	 * 根据过滤条件删除判断依据。若不支持修改，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> removeBasesThat(final Predicate<Thing> filter) {
		return notAllow(OpNotAllowedException.UNKNOWN_THING, "removeBasesThat");
	}

	/**
	 * 是否否定结果。
	 *
	 * @return
	 */
	default boolean isNegative() {
		return false;
	}

	/**
	 * 修改确信度。若不支持修改，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> changeCertainty(final float newCertainty) {
		return notAllow(OpNotAllowedException.UNKNOWN_THING, "changeCertainty");
	}

	/**
	 * 指定的判断者确认本判断对象的判断，返回由指定判断者确认的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @return
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> confirm(final Judge<_Against, _Result> judge) {
		return confirm(judge, Stream.empty());
	}

	/**
	 * 指定的判断者确认本判断对象的判断，返回由指定判断者确认的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> confirm(final Judge<_Against, _Result> judge, final Stream<Thing> bases) {
		return notAllow(judge, "confirm");
	}

	/**
	 * 指定的判断者待定本判断对象的判断，返回由指定判断者待定的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> toBeDetermine(final Judge<_Against, _Result> judge) {
		return toBeDetermine(judge, Stream.empty());
	}

	/**
	 * 指定的判断者待定本判断对象的判断，返回由指定判断者待定的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> toBeDetermine(final Judge<_Against, _Result> judge, final Stream<Thing> bases) {
		return notAllow(judge, "toBeDetermine");
	}

	/**
	 * 指定的判断者否定本判断对象的判断，返回由指定判断者否定的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> deny(final Judge<_Against, _Result> judge) {
		return deny(judge, Stream.empty());
	}

	/**
	 * 指定的判断者否定本判断对象的判断，返回由指定判断者否定的判断对象。
	 * 若不支持，可直接返回本对象或抛出异常。
	 * 默认实现抛出 {@link OpNotAllowedException}
	 *
	 * @deprecated
	 */
	@Deprecated
	default Judgement<_Against, _Result> deny(final Judge<_Against, _Result> judge, final Stream<Thing> bases) {
		return notAllow(judge, "deny");
	}

	private static <T> T notAllow(final Thing operator, final String operationName) {
		throw new OpNotAllowedException(operator, operationName, Thing.of(Judgement.class.getSimpleName(), "..."));
	}

	/**
	 * 按断时间。
	 *
	 * @return
	 */
	Instant judgeTime();

	//--------------------------------------------

	Comparator<Judgement<?, ?>> CERTAINTY_ASC = (o1, o2) -> Float.compare(o1.certainty(), o2.certainty());
	Comparator<Judgement<?, ?>> CERTAINTY_DESC = CERTAINTY_ASC.reversed();
	Comparator<Judgement<?, ?>> MOST_CERTAINTY_NEWEST_COMPARATOR = CERTAINTY_ASC.thenComparing(Judgement::judgeTime)
																																							.reversed();

	@SuppressWarnings("unchecked")
	static <F, T, J extends Judgement<F, T>> Comparator<J> judgementCertaintyAscComparator() {
		return (Comparator<J>) CERTAINTY_ASC;
	}

	@SuppressWarnings("unchecked")
	static <F, T, J extends Judgement<F, T>> Comparator<J> judgementCertaintyDescComparator() {
		return (Comparator<J>) CERTAINTY_DESC;
	}

	static <F, T, J extends Judgement<F, T>> Judgement<F, T> mostCertain(final Stream<J> judgements) {
		return judgements.max(CERTAINTY_DESC).orElse(null);
	}

	static String str(final Judgement<?, ?> judgement) {
		return StrKit.format("{0} → {1} ({2}:{3})", judgement.against(), judgement.result(), judgement.getJudge(),
												 judgement.certainty());
	}

	static int hash(final Judgement<?, ?> judgement) {
		return Objects.hash(judgement.getJudge(), judgement.against());
	}

	static boolean eq(final Judgement<?, ?> judgement, final Object other) {
		if (other instanceof final Judgement<?, ?> o) {
			return judgement.getJudge().equals(o.getJudge()) && judgement.against().equals(o.against());
		}
		return false;
	}
}
