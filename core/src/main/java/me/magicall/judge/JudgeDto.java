/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.judge;

import com.google.common.collect.Maps;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.NotMineException;

import java.time.Instant;
import java.util.Map;

public class JudgeDto<_Against, _Result> implements Judge<_Against, _Result> {
	public String type;
	public String idInType;
	private final Map<_Against, Judgement<_Against, _Result>> map = Maps.newHashMap();

	public JudgeDto() {
	}

	public JudgeDto(final String type, final String idInType) {
		this.type = type;
		this.idInType = idInType;
	}

	@Override
	public Judgement<_Against, _Result> judge(final _Against against) {
		return map.get(against);
	}

	@Override
	public boolean learn(final Judgement<_Against, _Result> judgementFromOther) {
		final var judgement = new JudgementDto<_Against, _Result>();
		judgement.against = judgementFromOther.against();
		judgement.result = judgementFromOther.result();
		judgement.certainty = judgementFromOther.certainty();
		judgement.bases = judgementFromOther.bases().toList();
		judgement.judge = this;
		judgement.judgeTime = Instant.now();
		map.put(judgementFromOther.against(), judgement);
		return true;
	}

	@Override
	public boolean dropJudgementAgainst(final _Against against) {
		map.remove(against);
		return true;
	}

	@Override
	public boolean dropJudgementAgainst(final _Against against, final Instant judgeTime) {
		final var exist = map.get(against);
		if (exist != null && exist.judgeTime().equals(judgeTime)) {
			map.remove(against);
		}
		return true;
	}

	@Override
	public boolean dropJudgement(final Judgement<_Against, _Result> judgement) {
		final var owner = judgement.getJudge();
		if (!equals(owner)) {
			throw new NotMineException("judgement", owner, this);
		}
		map.remove(judgement.against(), judgement);
		return true;
	}

	@Override
	public String type() {
		return type;
	}

	@Override
	public String idInType() {
		return idInType;
	}

	@Override
	public String toString() {
		return Thing.str(this);
	}

	@Override
	public int hashCode() {
		return Thing.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return o instanceof Judge && Thing.eq(this, o);
	}
}
