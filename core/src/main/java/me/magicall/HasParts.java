/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 实现本接口，表示有多个部分。
 */
@FunctionalInterface
public interface HasParts<_Part> {
	Stream<_Part> parts();

	/**
	 * 在各部分中寻找指定部分的位置，从1开始。未找到返回0。
	 *
	 * @return
	 */
	default int placeOf(final _Part part) {
		return indexOf(part) + 1;
	}

	/**
	 * 在各部分中寻找指定部分的下标索引，从0开始。未找到返回-1。
	 *
	 * @return
	 */
	default int indexOf(final _Part part) {
		return Math.toIntExact(parts().takeWhile(e -> !Objects.equals(e, part)).count());
	}
}
