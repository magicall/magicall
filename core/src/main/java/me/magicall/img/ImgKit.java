/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.img;

import me.magicall.Op;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

public interface ImgKit {
	/**
	 * 将图片文件转换为 Base64 编码字符串
	 *
	 * @param file 图片文件
	 * @return Base64 编码字符串
	 * @throws IOException 如果文件读取失败
	 */
	static String imgToBase64(final File file) {
		try (final FileInputStream fileInputStream = new FileInputStream(file)) {
			final byte[] imageBytes = new byte[(int) file.length()];
			fileInputStream.read(imageBytes);

			// 将字节数组转换为 Base64 字符串
			return Base64.getEncoder().encodeToString(imageBytes);
		} catch (final FileNotFoundException e) {
			throw new WrongArgException("file`exists", false, true, e);
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
	}

	/**
	 * 将 Base64 编码字符串转换为图片文件
	 *
	 * @param base64String Base64 编码字符串
	 * @param outputPath 输出图片文件路径
	 * @throws IOException 如果写入文件失败
	 */
	static void base64ToImg(final String base64String, final String outputPath)
			throws IOException, FileNotFoundException {
		// 将 Base64 字符串解码为字节数组
		final byte[] imageBytes = Base64.getDecoder().decode(base64String);
		final var file = ensureFile(outputPath);
		// 写入字节数组到文件
		try (final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			fileOutputStream.write(imageBytes);
		}
	}

	/**
	 * 确保指定文件路径的合法性（检查/创建目录，处理文件存在的情况）
	 *
	 * @param path 文件路径
	 * @return 一个合法的、存在的 File 对象
	 */
	static File ensureFile(final String path) {
		final File file = new File(path);
		// 检查父目录是否存在，如果不存在则创建
		final File parentDir = file.getParentFile();
		if (parentDir != null && !parentDir.exists()) {
			if (!parentDir.mkdirs()) {
				throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, "create dirs", Thing.of("dir", parentDir));
			}
		}
		// 检查文件是否已存在
		if (file.exists()) {
			if (!file.delete()) {
				throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, Op.CLEAR.name(), Thing.of("file", file));
			}
		}
		try {
			if (!file.createNewFile()) {
				throw new OpNotAllowedException(OpNotAllowedException.UNKNOWN_THING, Op.ADD.name(), Thing.of("file", file));
			}
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
		return file;
	}
}
