/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

import me.magicall.program.lang.java.贵阳DearSun.CharKit;

import java.util.stream.IntStream;

public enum BigUnitMode implements ChineseNumMode {
	亿亿,
	兆京 {
		@Override
		public boolean matches(final String s) {
			return IntStream.range(0, nameLen).anyMatch(i -> CharKit.in(name().charAt(i), s.toCharArray()));
		}
	};

	final int nameLen = name().length();

	@Override
	public boolean matches(final String s) {
		return true;
	}

	@Override
	public String trans(final String s) {
		return null;//todo
	}
}
