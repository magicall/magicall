/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public enum ChineseDigit {
	//todo:据说，〇仅用于序数计数，数字都用零。
	〇('零', 0),
	一('壹', 1),
	二('贰', 2),
	三('叁', 3),
	四('肆', 4),
	五('伍', 5),
	六('陆', 6),
	七('柒', 7),
	八('捌', 8),
	九('玖', 9),
	十('拾', 10),
	百('佰', 100),
	千('仟', 1000),
	万('万', 1_0000),
	亿('亿', 1_0000_000);

	public final char lowerCaseChar = name().charAt(0);
	public final String lowerCase = name();
	public final char upperCaseChar;
	public final String upperCase;
	public final long num;

	ChineseDigit(final char upperCaseChar, final long num) {
		this.upperCaseChar = upperCaseChar;
		upperCase = String.valueOf(upperCaseChar);
		this.num = num;
	}

	public long toNum() {
		return num;
	}

	public static final Pattern CHINESE_NUM_SEQUENCE_PATTER;

	static {
		final var values = values();
		final Set<Character> chars = Sets.newHashSet();
		Arrays.stream(values).forEach(value -> {
			chars.add(value.lowerCaseChar);
			chars.add(value.upperCaseChar);
		});
		final String sb = chars.stream().map(String::valueOf).collect(Collectors.joining());
		CHINESE_NUM_SEQUENCE_PATTER = Pattern.compile('[' + sb + "]+");
	}

	public static boolean isChineseDigit(final char c) {
		return Arrays.stream(values()).map(e -> e.lowerCaseChar).anyMatch(e -> e == c);
	}

	public static boolean isChineseNumSequence(final String s) {
		return CHINESE_NUM_SEQUENCE_PATTER.matcher(s).matches();
	}

	public static ChineseDigit of(final char c) {
		return Arrays.stream(values())//
				.filter(value -> c == value.lowerCaseChar || c == value.upperCaseChar)//
				.findFirst().orElse(null);
	}

	public static ChineseDigit of(final String s) {
		return Arrays.stream(values())//
				.filter(value -> s.equals(value.lowerCase) || s.equals(value.upperCase))//
				.findFirst().orElse(null);
	}

	public static String toUpperCase(final String s) {
		String rt = s;
		for (final ChineseDigit digit : values()) {
			rt = rt.replace(digit.upperCaseChar, digit.lowerCaseChar);
		}
		return rt;
	}

	public static String toLowerCase(final String s) {
		String rt = s;
		for (final ChineseDigit digit : values()) {
			rt = rt.replace(digit.lowerCaseChar, digit.upperCaseChar);
		}
		return rt;
	}
}
