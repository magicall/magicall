/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

public enum TwentyMode implements ChineseNumMode {
	二十 {
		@Override
		public boolean matches(final String s) {
			return s.contains(二十.name()) && !s.contains(廿.name()) && !s.contains(卄.name());
		}

		@Override
		public String trans(final String s) {
			final var name = name();
			return s.replace(廿.name(), name).replace(卄.name(), name);
		}
	},
	廿 {
		@Override
		public boolean matches(final String s) {
			return !s.contains(二十.name()) && s.contains(廿.name()) && !s.contains(卄.name());
		}

		@Override
		public String trans(final String s) {
			final var name = name();
			return s.replace(二十.name(), name).replace("贰拾", name).replace("弍拾", name);
		}
	},
	卄 {
		@Override
		public boolean matches(final String s) {
			return !s.contains(二十.name()) && !s.contains(廿.name()) && s.contains(卄.name());
		}

		@Override
		public String trans(final String s) {
			final var name = name();
			return s.replace(二十.name(), name).replace("贰拾", name).replace("弍拾", name);
		}
	},
	混合 {
		@Override
		public boolean matches(final String s) {
			return true;
		}

		@Override
		public String trans(final String s) {
			return 二十.trans(s);
		}
	}
}
