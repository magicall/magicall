/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.CharKit;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class ChineseNumParser {
	public static final String NEGATIVE_SIGN = "负";
	public static final String POSITIVE_SIGN = "正";
	public static final String POINT = "点";

	private static final String[] SMALL_GROUP_UNITS = {"", "十", "百", "千"};
	private static final int GROUP_BIT_THRESHOLD = 4;
	////每一组最多为“a千b百c十d”，7个字
	private static final int SMALL_GROUP_MAX_LEN = 7;
	//这些mode都还没用上。
	private boolean use〇InLowerCase = true;
	private TwoMode twoMode = TwoMode.二;
	private TwentyMode twentyMode = TwentyMode.二十;
	private ThirtyMode thirtyMode = ThirtyMode.三十;
	private BigUnitMode bigUnitMode = BigUnitMode.亿亿;
	private CaseMode caseMode = CaseMode.小写;
	private boolean upperCase;

	/**
	 * 仅进行数码解析，不计算数位。
	 *
	 * @return
	 */
	public String digitParse(final long num) {
		if (num == 0) {
			return say0();
		}
		final long numToUse;
		final boolean negative;
		if (num < 0) {
			negative = true;
			if (num == Long.MIN_VALUE) {
				numToUse = Long.MAX_VALUE;
			} else {
				numToUse = -num;
			}
		} else {
			negative = false;
			numToUse = num;
		}
		final StringBuilder sb = new StringBuilder();
		LongStream.iterate(numToUse, n -> n != 0, n -> n / 10)//
				.map(n -> n % 10)//
				.forEach(mod -> {
					if (mod == 0) {
						sb.append(use〇InLowerCase ? ChineseDigit.〇.lowerCaseChar : ChineseDigit.〇.upperCaseChar);
					} else {
						sb.append(ChineseDigit.values()[(int) mod].lowerCaseChar);
					}
				});
		if (negative) {
			sb.append(NEGATIVE_SIGN);
		}
		if (num == Long.MIN_VALUE) {//9223372036854775807 -> -9223372036854775808
			sb.replace(0, 1, ChineseDigit.八.name());
		}
		return sb.reverse().toString();
	}

	/**
	 * 将一个阿拉伯数转化为中文数。
	 *
	 * @return
	 */
	public String parse(final long num) {
		/*
		  算法如下：
		  1，数字按每4位分一小组，每小组按位翻译每一位的数码。
		  2，四位分别加小单位：千、百、十、（无）。
		  3，去掉“零”字后面的小单位。
		  4，每小组合并重复的“零”。
		  5，去掉每小组末尾的零。
		  6，大组加组单位：亿、万。
		  7，（根据配置）在一小组内开头如为“一十”则省略“一”字。
		  8，亿后直接跟万的，则省略万。
		 */
		if (num == 0) {
			return say0();
		}
		final String parsed = digitParse(num);
		final String digitStr = num < 0 ? parsed.substring(1) : parsed;//去掉“负”字。

		//数字按每4位分一小组，每小组按位翻译每一位的数码。
		final List<BigGroup> bigGroups = Lists.newArrayList();
		SmallGroup lastGroup = null;
		BigGroup curBigGroup = null;
		for (String s = digitStr; !s.isEmpty(); ) {
			final int last4BitIndex = Math.max(0, s.length() - 4);
			final var smallGroup = new SmallGroup(s.substring(last4BitIndex));
			if (lastGroup == null) {
				lastGroup = smallGroup;
			} else {
				if (curBigGroup == null) {
					curBigGroup = new BigGroup();
					curBigGroup.set万Group(smallGroup);
				} else {
					curBigGroup.set亿Group(smallGroup);
					bigGroups.add(curBigGroup);
					curBigGroup = null;
				}
			}
			s = s.substring(0, last4BitIndex);
		}
		if (curBigGroup != null) {
			bigGroups.add(curBigGroup);
		}
		final var sb = new StringBuilder();
		IntStream.iterate(bigGroups.size() - 1, i -> i >= 0, i -> i - 1)//倒序
				.mapToObj(i -> bigGroups.get(i).toCnNum())//
				.forEach(sb::append);

		assert lastGroup != null;
		sb.append(lastGroup.toCnNum());
		if (sb.charAt(0) == say0Char()) {
			sb.deleteCharAt(0);
		}
		if (num < 0) {
			sb.insert(0, NEGATIVE_SIGN);
		}
		return sb.toString().replace("亿万", "亿");//8，亿后直接跟万的，则省略万。
	}

	public long parse(final String cnNum) {
		/*
		  算法如下：
		  1，按“亿或万”分为小组。注：所以不能支持带有“万亿”、“亿亿”等字样的大数，所以理论上可以解析的最大数为（一万亿-1）。
		  	若有“亿”，应得3组
		  		若只有2组
		  			若有“万”，则第3组是0。如：一亿一千万
		  			若无“万”，则第2组是0。如：一亿〇一
		  		若只有1组，则第2、3组都是0。如：一亿
		  2，若小组以“十”开头，在前面补一。注：此时其实已知其为001x，可直接处理。
		  3，每小组，按“千百十”：
		  		找该字
		  			若无该字，补1个0。
		  			若有该字，取其前一位，翻译为阿拉伯数码。
		  4，查看最后一个字，若非“千百十”之一，即为个位数，翻译为阿拉伯数码。
		  5，若该组阿拉伯数码长度<4，末尾用0补足到4。
		  6，将整个字符串解析为数值。
		 */
		if (cnNum == null) {
			throw new NumberFormatException("null");
		}
		if (cnNum.isBlank()) {
			throw exception(cnNum);
		}
		final var sb = new StringBuilder();
		final String s;
		if (cnNum.startsWith(NEGATIVE_SIGN)) {
			s = cnNum.substring(1);
			sb.append('-');
		} else if (cnNum.startsWith(POSITIVE_SIGN)) {
			s = cnNum.substring(1);
		} else {
			s = cnNum;
		}
		final var smallGroupParts = s.split("[亿万]");
		Arrays.stream(smallGroupParts).forEach(smallGroupPart -> parseSmallGroupPart(sb, smallGroupPart));

		if (smallGroupParts.length == 1) {
			if (s.contains("亿")) {
				IntStream.range(0, GROUP_BIT_THRESHOLD * 2).forEach(i -> sb.append(0));
			} else if (s.contains("万")) {
				IntStream.range(0, GROUP_BIT_THRESHOLD).forEach(i -> sb.append(0));
			}
		} else if (smallGroupParts.length == 2) {
			if (s.contains("亿")) {
				if (s.contains("万")) {
					IntStream.range(0, GROUP_BIT_THRESHOLD).forEach(i -> sb.append(0));
				} else {
					sb.insert(sb.length() - GROUP_BIT_THRESHOLD, "0000");
				}
			}
		}
		return Long.parseLong(sb.toString());
	}

	private void parseSmallGroupPart(final StringBuilder sb, final String smallGroupPart) {
		//特殊处理“十几”的情况
		final var c1 = smallGroupPart.charAt(0);
		if (c1 == '十') {
			sb.append("001");
			final var length = smallGroupPart.length();
			if (length == 1) {
				sb.append(0);
				return;
			}
			if (length > 2) {
				throw exception(smallGroupPart);
			}
			sb.append(toNum(smallGroupPart.charAt(1)));
			return;
		}
		final String strToUse = smallGroupPart.replace(say0(), "");
		if (strToUse.isEmpty()) {
			sb.append(0);
			return;
		}
		final var lenToUse = strToUse.length();
		final char[] units = {'千', '百', '十'};
		final var lenAtBeginning = sb.length();
		int startIndex = 0;
		var remainMaxLen = SMALL_GROUP_MAX_LEN;
		for (final char unit : units) {
			if (lenToUse - startIndex > remainMaxLen) {
				throw exception(smallGroupPart);
			}
			final var unitIndex = startIndex + 1;
			if (unitIndex < lenToUse) {
				if (unit == strToUse.charAt(unitIndex)) {
					sb.append(toNum(strToUse.charAt(startIndex)));
					startIndex += 2;
				} else {
					sb.append(0);
				}
			} else {
				sb.append(0);
			}
			remainMaxLen -= 2;
		}
		if (lenToUse - startIndex > remainMaxLen) {
			throw exception(smallGroupPart);
		}
		final var lastChar = strToUse.charAt(lenToUse - 1);
		if (!CharKit.in(lastChar, units)) {
			sb.append(toNum(lastChar));
		}
		final var addedLen = sb.length() - lenAtBeginning;
		if (addedLen < 4) {
			sb.append("0".repeat(Math.max(0, 4 - addedLen)));
		}
	}

	private static NumberFormatException exception(final String s) {
		return new NumberFormatException("For input string: \"" + s + '"');
	}

	private static long toNum(final char c) {
		final var s = String.valueOf(c);
		final var digit = ChineseDigit.of(s);
		if (digit == null || digit.compareTo(ChineseDigit.九) > 0) {
			throw new NumberFormatException(s);
		}
		return digit.num;
	}

	private final class BigGroup {
		private SmallGroup 亿Group;
		private SmallGroup 万Group;

		private StringBuilder toCnNum() {
			//6，大组加组单位：亿、万。
			if (亿Group == null) {
				return 万Group.toCnNum().append('万');
			} else {
				return 亿Group.toCnNum().append('亿').append(万Group.toCnNum().append('万'));
			}
		}

		private void set亿Group(final SmallGroup group) {
			亿Group = group;
		}

		private void set万Group(final SmallGroup group) {
			万Group = group;
		}
	}//BigGroup

	private final class SmallGroup {
		private final String digits;//<=10000

		private SmallGroup(final String digits) {
			this.digits = digits;
		}

		StringBuilder toCnNum() {
			final var length = digits.length();
			final var sb = new StringBuilder(GROUP_BIT_THRESHOLD * 2 - 1);
			//2，四位分别加小单位：千、百、十、（无）。
			IntStream.range(0, length)//
					.forEach(i -> sb.append(digits.charAt(i)).append(SMALL_GROUP_UNITS[length - i - 1]));
			//3，去掉“零”字后面的小单位。
			for (int i = 0; i < sb.length(); i++) {//循环中会改变sb的长度，所以sb.length()不能提前获取（所以也不能改为IntStream）
				if (sb.charAt(i) == say0Char()) {
					final int nextIndex = i + 1;
					if (nextIndex < sb.length()) {
						sb.deleteCharAt(nextIndex);
					}
				}
			}
			//4，每小组合并重复的“零”。
			for (int i = 0; i < sb.length(); i++) {//循环中会改变sb的长度，所以sb.length()不能提前获取
				if (sb.charAt(i) == say0Char()) {
					final int start = i + 1;
					int end = start;
					for (; end < sb.length(); ) {
						if (sb.charAt(end) == say0Char()) {
							end++;
						} else {
							break;
						}
					}
					sb.delete(start, end);
				}
			}
			//5，去掉每小组末尾的零。
			final var lastIndex = sb.length() - 1;
			if (sb.charAt(lastIndex) == say0Char()) {
				sb.deleteCharAt(lastIndex);
			}
			return sb;
		}
	}//SmallGroup

	public String say0() {
		return String.valueOf(say0Char());
	}

	private char say0Char() {
		return use〇InLowerCase ? ChineseDigit.〇.lowerCaseChar : ChineseDigit.〇.upperCaseChar;
	}

	public String say2() {
		return String.valueOf(say2Char());
	}

	private char say2Char() {
		return twoMode.name().charAt(0);
	}

	public String say20() {
		return twentyMode.name();
	}

	public String say30() {
		return thirtyMode.name();
	}

	public TwoMode getTwoMode() {
		return twoMode;
	}

	public ChineseNumParser withTwoMode(final TwoMode twoMode) {
		this.twoMode = twoMode;
		return this;
	}

	public TwentyMode getTwentyMode() {
		return twentyMode;
	}

	public ChineseNumParser withTwentyMode(final TwentyMode twentyMode) {
		this.twentyMode = twentyMode;
		return this;
	}

	public ThirtyMode getThirtyMode() {
		return thirtyMode;
	}

	public ChineseNumParser withThirtyMode(final ThirtyMode thirtyMode) {
		this.thirtyMode = thirtyMode;
		return this;
	}

	public BigUnitMode getBigUnitMode() {
		return bigUnitMode;
	}

	public ChineseNumParser withBigUnitMode(final BigUnitMode bigUnitMode) {
		this.bigUnitMode = bigUnitMode;
		return this;
	}

	public boolean isUsingUpperCase() {
		return upperCase;
	}

	public ChineseNumParser useUpperCase() {
		upperCase = true;
		return this;
	}

	public ChineseNumParser useLowerCase() {
		upperCase = false;
		return this;
	}

	public boolean isUsing〇InLowerCase() {
		return use〇InLowerCase;
	}

	public ChineseNumParser use〇InLowerCase() {
		use〇InLowerCase = true;
		return this;
	}

	public ChineseNumParser use零InLowerCase() {
		use〇InLowerCase = false;
		return this;
	}

	public ChineseNumParser use小写() {
		caseMode = CaseMode.小写;
		return this;
	}

	public ChineseNumParser use大写() {
		caseMode = CaseMode.大写;
		return this;
	}

	public CaseMode getCaseMode() {
		return caseMode;
	}
}
