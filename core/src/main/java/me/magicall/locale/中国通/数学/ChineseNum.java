/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;//package cn.linelaw.support.i18n;
//
//public class ChineseNum extends Number {
//
//	@Serial
//	private static final long serialVersionUID = -3719035647424450489L;
//	private static final ChineseNumParser PARSER = new ChineseNumParser();
//	private static final ChineseNumParser UPPERCASE_PARSER = new ChineseNumParser().useUpperCase();
//	private static final ChineseNumParser LOWERCASE_PARSER = new ChineseNumParser().useLowerCase();
//
//	public static ChineseNum valueOf(final String s) {
//
//	}
//
//	private final Number num;
//	private transient String cnNum;
//
//	public ChineseNum(final String cnNum) {
//		num = PARSER.parse(cnNum);
//		this.cnNum = cnNum;
//	}
//
//	public ChineseNum(final Number num) {
//		this.num = num;
//		cnNum = PARSER.parse(num.longValue());
//	}
//
//	@Override
//	public int intValue() {
//		return num.intValue();
//	}
//
//	@Override
//	public long longValue() {
//		return num.longValue();
//	}
//
//	@Override
//	public float floatValue() {
//		return num.floatValue();
//	}
//
//	@Override
//	public double doubleValue() {
//		return num.doubleValue();
//	}
//
//	/**
//	 * 大写中文数字。其中“0”必为“零”，“2”必为“贰”。
//	 *
//	 * @return
//	 */
//	public String toUpperCase() {
//		return null;//todo
//	}
//
//	/**
//	 * 小写中文数字。此时zeroMode、twoMode有效。
//	 *
//	 * @return
//	 */
//	public String toLowerCase() {
//		return null;//todo
//	}
//
//	@Override
//	public int hashCode() {
//		return num.hashCode();
//	}
//
//	@Override
//	public boolean equals(final Object x) {
//		if (x == this) {
//			return true;
//		}
//		if (!(x instanceof ChineseNum)) {
//			return false;
//		}
//		return num.equals(((ChineseNum) x).num);
//	}
//
//	@Override
//	public String toString() {
//		return toLowerCase();
//	}
//
//	@Override
//	public ChineseNum clone() throws CloneNotSupportedException {
//		return (ChineseNum) super.clone();
//	}
//}
