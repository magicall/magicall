/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

public enum TwoMode implements ChineseNumMode {
	二 {
		@Override
		public boolean matches(final String s) {
			return s.contains(二.name()) && !s.contains(两.name());
		}

		@Override
		public String trans(final String s) {
			return s.replace(两.name(), name());
		}
	},
	两 {
		@Override
		public boolean matches(final String s) {
			return !s.contains(二.name()) && s.contains(两.name());
		}

		@Override
		public String trans(final String s) {
			return s.replace(二.name(), name());
		}
	},
	混合 {
		@Override
		public boolean matches(final String s) {
			return true;
		}

		@Override
		public String trans(final String s) {
			return 二.trans(s);
		}
	};

	private final String lowerCase = name().substring(0, 1);
	private final String upperCase = name().substring(1);

	public String lowerCase() {
		return lowerCase;
	}

	public String upperCase() {
		return upperCase;
	}
}
