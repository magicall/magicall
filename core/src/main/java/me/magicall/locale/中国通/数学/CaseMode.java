/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public enum CaseMode implements ChineseNumMode {
	小写 {
		@Override
		char transFrom(final ChineseDigit digit) {
			return digit.upperCaseChar;
		}
	},
	大写 {
		@Override
		char transTo(final ChineseDigit digit) {
			return digit.upperCaseChar;
		}
	},
	/**
	 * 可识别大小写混合的汉语数字。输出时使用小写。由于适用性比小写广，建议总是用混合代替小写。
	 */
	混合 {
		@Override
		public boolean matches(final String s) {
			return Stream.of(values()).filter(e -> e != this).anyMatch(e -> e.matches(s));
		}

		@Override
		Predicate<ChineseDigit> transFromPredicate(final char c) {
			return d -> Stream.of(values()).filter(e -> e != this).anyMatch(e -> e.transFrom(d) == c);
		}
	};

	@Override
	public boolean matches(final String s) {
		for (int i = 0; i < s.length(); i++) {
			final var c = s.charAt(i);
			final var digit = findChineseDigit(d -> transTo(d) == c);
			if (digit == null) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String trans(final String s) {
		final var sb = new StringBuilder();
		IntStream.range(0, s.length()).forEach(i -> {
			final var c = s.charAt(i);
			final var digit = findChineseDigit(transFromPredicate(c));
			sb.append(digit == null ? c : transTo(digit));
		});
		return sb.toString();
	}

	Predicate<ChineseDigit> transFromPredicate(final char c) {
		return d -> transFrom(d) == c;
	}

	private static ChineseDigit findChineseDigit(final Predicate<ChineseDigit> predicate) {
		return Arrays.stream(ChineseDigit.values())//
				.filter(predicate)//
				.findFirst().orElse(null);
	}

	char transFrom(final ChineseDigit digit) {
		return digit.lowerCaseChar;
	}

	char transTo(final ChineseDigit digit) {
		return digit.lowerCaseChar;
	}
}
