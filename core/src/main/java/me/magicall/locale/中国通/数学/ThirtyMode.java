/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

public enum ThirtyMode implements ChineseNumMode {
	三十 {
		@Override
		public String trans(final String s) {
			return s.replace(卅.name(), name());
		}

		@Override
		public boolean matches(final String s) {
			return s.contains(三十.name()) && !s.contains(卅.name());
		}
	},
	卅 {
		@Override
		public boolean matches(final String s) {
			return !s.contains(三十.name()) && s.contains(卅.name());
		}

		@Override
		public String trans(final String s) {
			final var target = 三十.name();
			return s.replace(target, name()).replace(ChineseDigit.toUpperCase(target), name());
		}
	},
	混合 {
		@Override
		public boolean matches(final String s) {
			return true;
		}

		@Override
		public String trans(final String s) {
			return 三十.trans(s);
		}
	}
}
