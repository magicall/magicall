/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

public interface ChineseNumMode {
	/**
	 * 是否匹配本模式。
	 *
	 * @return
	 */
	boolean matches(String s);

	/**
	 * 把非本模式的汉语数字转换成本模式。
	 *
	 * @return
	 */
	String trans(String s);

	default char trans(final char c) {
		return trans(String.valueOf(c)).charAt(0);
	}
}
