/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.语文;

import java.io.Serial;
import java.io.Serializable;
import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class 拼音比较器 implements Comparator<String>, Serializable {

	public static final 拼音比较器 INSTANCE = new 拼音比较器();

	@Serial
	private static final long serialVersionUID = -55168513689436578L;
	private static final Collator COLLATOR = Collator.getInstance(Locale.SIMPLIFIED_CHINESE);

	private 拼音比较器() {
	}

	@Override
	public int compare(final String o1, final String o2) {
		return COLLATOR.compare(o1, o2);
	}
}
