/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通.数学;

public enum TwoUpperCaseMode implements ChineseNumMode {
	贰 {
		@Override
		public String trans(final String s) {
			return s.replace(弍.c, c);
		}

		@Override
		public boolean matches(final String s) {
			return !s.contains(弍.name());
		}
	},
	弍 {
		@Override
		public String trans(final String s) {
			return s.replace(贰.c, c);
		}

		@Override
		public boolean matches(final String s) {
			return !贰.matches(s);
		}
	};

	final char c = name().charAt(0);
}
