/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.locale.中国通;

import java.util.regex.Pattern;

public class ChinaMainlandUtil {

	private static final Pattern ZIP_CODE = Pattern.compile("\\d{6}");

	public static boolean isZipCode(final String s) {
		return matches(ZIP_CODE, s);
	}

	private static boolean matches(final Pattern pattern, final String s) {
		return pattern.matcher(s).matches();
	}
}
