/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.Comparator;
import java.util.Objects;

public class IdName implements Comparable<IdName> {
	private static final Comparator<IdName> COMPARATOR = Comparator.comparing(IdName::id,
			Comparator.nullsLast(Comparator.naturalOrder()));
	public String id;
	public String name;

	public IdName() {
	}

	public IdName(final Object id, final String name) {
		this(String.valueOf(id), name);
	}

	public IdName(final String id, final String name) {
		this.id = id;
		this.name = name;
	}

	public String id() {
		return id;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final IdName idName = (IdName) o;
		return Objects.equals(id(), idName.id());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id());
	}

	@Override
	public int compareTo(final IdName o) {
		return COMPARATOR.compare(this, o);
	}
}
