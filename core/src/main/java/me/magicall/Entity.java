/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.project.SnapshotEnabled;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.Objects;

/**
 * 领域中的实体对象。
 * 代表具有唯一标识的事物，即使它们的属性发生变化，它们仍然是同一个实体。
 * 每个实体都有一个唯一的标识符，如ID，用于区分不同的实体实例。
 * 通常用于描述具有生命周期和状态的领域概念，如用户、订单、车辆等。
 * 可以是可变的，即它们的状态可以在其生命周期内发生变化。
 *
 * @param <_Id> 领域对象的id的类型
 * @param <_ValObjType> 领域对象的值对象的类型。
 */
@FunctionalInterface
public interface Entity<_Id, _ValObjType> extends Identified<_Id>, SnapshotEnabled<_ValObjType> {

	/**
	 * 返回当前状态的快照，其所有属性值等于本实体对象的当前属性值。
	 * 返回对象应当是只读的；或者对其修改不会改变原始对象（本对象）。
	 * 默认实现：抛出 {@link OpNotAllowedException}
	 *
	 * @return 值对象。
	 */
	@Override
	default _ValObjType snapshot() {
		throw new OpNotAllowedException(Thing.of(getClass().getSimpleName(), id()), "snapshot");
	}

	/**
	 * 修改自身所有属性值为参数指定的对应属性值。
	 * 若有一些属性不能修改，可自行决定抛出异常还是静默。
	 * 默认实现：完全不修改，直接返回自身。
	 *
	 * @param newVal 参考对象
	 * @return 已修改了值的实体对象。
	 */
	default Entity<_Id, _ValObjType> accept(final _ValObjType newVal) {
		return this;
	}

	//=============================================================================

	static boolean eq(final Class<? extends Entity<?, ?>> c, final Object a, final Object b) {
		return c.isInstance(a) && c.isInstance(b)//
				&& Objects.equals(((Identified<?>) a).id(), ((Identified<?>) b).id());
	}

	static int hash(final Entity<?, ?> o) {
		return Objects.hash(o.id());
	}
}
