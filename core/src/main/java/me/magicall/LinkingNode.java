/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.stream.Stream;

/**
 * 有线网络中的一个节点。
 * “有线”表示线很重要。
 *
 * @param <T> 端点的类型。
 * @param <_LinkPayloadType> 负载的类型
 */
public interface LinkingNode<T extends LinkingNode<T, _LinkPayloadType>, _LinkPayloadType> extends Node<T> {

	Stream<Link<T, T, _LinkPayloadType>> incommings();

	Stream<Link<T, T, _LinkPayloadType>> outgoings();
}
