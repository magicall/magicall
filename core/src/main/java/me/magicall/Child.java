/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

/**
 * 实现本接口，意味着对象有父级对象。
 * 注意：父级对象不一定也有父级对象。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Child<_Parent> {
	_Parent parent();

	default boolean isRoot() {
		return parent() == null;
	}

	default boolean isChildOf(final _Parent maybeParent) {
		return maybeParent != null && maybeParent.equals(parent());
	}

	static <T extends Child<T>> T findRootOf(final T o) {
		T e = o;
		while (!e.isRoot()) {
			e = e.parent();
		}
		return e;
	}
}
