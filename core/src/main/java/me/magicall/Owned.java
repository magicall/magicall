/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.Objects;

/**
 * 实现本接口意味着被其他对象持有/从属于其他对象，即有“主人”。
 * 注意：这种从属关系应当是“紧密”的，具有必然性。比如“类”和“成员变量”，一个成员变量必属于一个类。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface Owned<_Owner> {

	_Owner owner();

	default boolean isBelongsTo(final _Owner owner) {
		return Objects.equals(owner(), owner);
	}
}
