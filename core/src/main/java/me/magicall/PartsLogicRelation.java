/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.Composite.PartsRelation;
import me.magicall.logic.LogicRelation;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 组成部分之间的关系。
 */
public enum PartsLogicRelation implements Named, PartsRelation {
	与(LogicRelation.与) {
		@Override
		public <T> boolean test(final Stream<? extends T> items, final Predicate<? super T> predicate) {
			return items.allMatch(predicate);
		}

		@Override
		public boolean reduce(final Stream<Boolean> bools) {
			return bools.allMatch(Boolean::booleanValue);
		}
	},
	或(LogicRelation.或) {
		@Override
		public <T> boolean test(final Stream<? extends T> items, final Predicate<? super T> predicate) {
			return items.anyMatch(predicate);
		}

		@Override
		public boolean reduce(final Stream<Boolean> bools) {
			return bools.anyMatch(Boolean::booleanValue);
		}
	};

	public final LogicRelation logicRelation;

	PartsLogicRelation(final LogicRelation logicRelation) {
		this.logicRelation = logicRelation;
	}

	public abstract <T> boolean test(Stream<? extends T> items, Predicate<? super T> predicate);

	/**
	 * 判断一组布尔值是否符合本关系的规定。
	 *
	 * @return
	 */
	public abstract boolean reduce(Stream<Boolean> bools);
}
