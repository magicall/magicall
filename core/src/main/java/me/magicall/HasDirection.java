/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.relation.Relation;

public interface HasDirection {
	Thing from();

	/**
	 * 修改关系的开端节点。
	 * 若允许修改，本方法应确保：原关系不复存在；期望修改成的关系（新开端节点→末端节点）必然存在。
	 *
	 * @return
	 */
	default Relation from(final Thing from) {
		throw new OpNotAllowedException(Thing.of(HasDirection.class.getSimpleName(), toString()), "from", from);
	}

	Thing to();

	/**
	 * 修改关系的末端节点。
	 * 若允许修改，本方法应确保：原关系不复存在；期望修改成的关系（开端节点→新末端节点）必然存在。
	 *
	 * @return
	 */
	default Relation to(final Thing to) {
		throw new OpNotAllowedException(Thing.of(HasDirection.class.getSimpleName(), toString()), "to", to);
	}
}
