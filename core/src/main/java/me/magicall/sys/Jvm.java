/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sys;

public class Jvm {
	private static final Runtime RUNTIME = Runtime.getRuntime();

	private Jvm() {
	}

	public static long totalMemory() {
		return RUNTIME.totalMemory();
	}

	public static long freeMemory() {
		return RUNTIME.freeMemory();
	}

	public static long usedMemory() {
		return RUNTIME.totalMemory() - RUNTIME.freeMemory();
	}
}
