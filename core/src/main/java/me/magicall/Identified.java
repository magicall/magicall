/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.runtime.RuntimeKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 实现本接口，表示对象有id。
 *
 * @param <_Id> id的类型。
 */
@FunctionalInterface
public interface Identified<_Id> {

	/**
	 * 获取id。
	 *
	 * @return id
	 */
	_Id id();

	//=======================

	static <I, G extends Identified<I>> List<I> toIds(final Iterable<G> source) {
		return CollKit.stream(source).map(Identified::id).toList();
	}

	static <I, G extends Identified<I>> Map<I, G> map(final Iterable<G> source) {
		return CollKit.stream(source).collect(Collectors.toMap(Identified::id, Function.identity()));
	}

	static String randomId() {
		return UUID.randomUUID().toString();
	}

	static int sysRuntimeHashId(final Object o) {
		return RuntimeKit.objHash(o);
	}
}
