/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.opinion;

import com.google.common.collect.Range;
import me.magicall.math.MathKit;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

public interface Opinion<_ContentType> {

	_ContentType content();

	float certainty();

	default boolean isIndefinite() {
		return Opinion.isIndefinite(certainty());
	}

	/**
	 * 是否确定。
	 *
	 * @return
	 */
	default boolean isConfirmed() {
		return isConfirmed(certainty());
	}

	//=====================================================

	float CERTAINTY_INDEFINITE = 0;
	float CERTAINTY_CONFIRMED = 1.0F;
	float CERTAINTY_DENIED = -1.0F;
	Range<Float> CERTAINTY_FULL_RANGE = Range.closed(CERTAINTY_INDEFINITE, CERTAINTY_CONFIRMED);

	Comparator<Opinion<?>> CERTAINTY_ASC = Comparator.comparingDouble(Opinion::certainty);
	Comparator<Opinion<?>> CERTAINTY_DESC = CERTAINTY_ASC.reversed();

	@SuppressWarnings("unchecked")
	static <C, O extends Opinion<C>> Comparator<O> opinionCertaintyAscComparator() {
		return (Comparator<O>) CERTAINTY_ASC;
	}

	@SuppressWarnings("unchecked")
	static <C, O extends Opinion<C>> Comparator<O> opinionCertaintyDescComparator() {
		return (Comparator<O>) CERTAINTY_DESC;
	}

	static <C, O extends Opinion<C>> Opinion<C> mostCertain(final Stream<O> opinions) {
		return opinions.max(CERTAINTY_DESC).orElse(null);
	}

	static boolean isConfirmed(final float certainty) {
		return MathKit.eq(certainty, CERTAINTY_CONFIRMED);
	}

	static boolean isIndefinite(final float certainty) {
		return MathKit.eq(CERTAINTY_INDEFINITE, certainty);
	}

	static String str(final Opinion<?> opinion) {
		return StrKit.format("{0} √{1}", opinion.content(), opinion.certainty());
	}

	static int hash(final Opinion<?> opinion) {
		return Objects.hash(opinion.content());
	}

	static boolean eq(final Opinion<?> opinion, final Object other) {
		return other instanceof final Opinion<?> o && opinion.content().equals(o.content());
	}
}
