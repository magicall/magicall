/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cfg;

import me.magicall.Described;
import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.scope.Scoped;

import java.util.Objects;

/**
 * 配置项。
 */
public interface Cfg extends Named, Described<Cfg>, Scoped {
	String TYPE = Cfg.class.getSimpleName();

	@Override
	default Cfg renameTo(final String newName) {
		return (Cfg) Named.super.renameTo(newName);
	}

	/**
	 * 配置项的值。
	 *
	 * @return
	 */
	Object val();

	/**
	 * 使用新的值。
	 *
	 * @return
	 */
	default Cfg withVal(final Object val) {
		throw new OpNotAllowedException(Thing.of(TYPE, name()), "withVal", Thing.of("Object", val));
	}

	/**
	 * 备注。
	 * 跟“描述”的区别是：描述是给使用者用的，备注是给管理者用的。
	 *
	 * @return
	 */
	String note();

	/**
	 * 使用新的备注。
	 *
	 * @return
	 */
	default Cfg withNote(final String note) {
		throw new OpNotAllowedException(Thing.of(TYPE, name()), "withNote", Thing.of("String", note));
	}

	static String str(final Cfg one) {
		return StrKit.format("{0}（{1}）={2}", one.name(), one.description(), one.val());
	}

	static int hash(final Cfg one) {
		return Objects.hash(one.name());
	}

	static boolean eq(final Cfg one, final Object other) {
		return other instanceof final Cfg o && Objects.equals(one.name(), o.name());
	}
}
