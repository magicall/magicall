/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cfg;

import me.magicall.scope.Scope;

public class CfgDto implements Cfg {
	public Scope scope;
	public String name;
	public String description;
	public Object val;
	public String note;

	public CfgDto() {
	}

	public CfgDto(final String name, final String val) {
		this(name, "", val);
	}

	public CfgDto(final String name, final String description, final String val) {
		this(name, description, val, "");
	}

	public CfgDto(final String name, final String description, final String val, final String note) {
		this.name = name;
		this.description = description;
		this.val = val;
		this.note = note;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public Cfg renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public Cfg describedAs(final String description) {
		this.description = description;
		return this;
	}

	@Override
	public Object val() {
		return val;
	}

	@Override
	public Cfg withVal(final Object val) {
		this.val = val;
		return this;
	}

	@Override
	public String note() {
		return note;
	}

	@Override
	public Cfg withNote(final String note) {
		this.note = note;
		return this;
	}

	@Override
	public String toString() {
		return Cfg.str(this);
	}

	@Override
	public int hashCode() {
		return Cfg.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Cfg.eq(this, other);
	}
}
