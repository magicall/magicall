/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.time.Instant;
import java.util.Objects;

/**
 * 打标签的信息。是标签和资源的关联关系的值对象。
 */
public interface Tagging {

	Tag tag();

	Thing against();

	Instant taggingTime();

	static String str(final Tagging tagging) {
		return StrKit.format("{0} → {1}", tagging.tag(), tagging.against());
	}

	static int hash(final Tagging tagging) {
		return Objects.hash(tagging.tag(), tagging.against());
	}

	static boolean eq(final Tagging tagging, final Object other) {
		if (other instanceof final Tagging o) {
			return tagging.tag().equals(o.tag()) && tagging.against().equals(o.against());
		}
		return false;
	}
}
