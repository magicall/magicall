/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag;

import me.magicall.Described;
import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.ThingDto;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.scope.Scoped;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * 标签。
 * todo：考虑是否应该有id。
 */
public interface Tag extends Named, Described<Tag>, Scoped {
	@Override
	default Tag renameTo(final String newName) {
		return (Tag) Named.super.renameTo(newName);
	}

	/**
	 * 获取本标签的所有东西。
	 *
	 * @return
	 */
	Stream<Tagging> taggings();

	/**
	 * 获取打了本标签的某类东西。
	 *
	 * @return
	 */
	default Stream<Tagging> taggingsOf(final String type) {
		Objects.requireNonNull(type);
		return taggings().filter(tagging -> tagging.against().type().equals(type));
	}

	/**
	 * 判断一个东西是否打了本标签。
	 *
	 * @return
	 */
	default boolean isTaggedTo(final Thing thing) {
		return taggings().anyMatch(tagging -> tagging.against().equals(thing));
	}

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	Tag tag(Stream<Thing> things);

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	default Tag tag(final Collection<Thing> things) {
		return tag(things.stream());
	}

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	default Tag tag(final Thing... things) {
		return tag(Stream.of(things));
	}

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	default Tag tag(final String thingType, final Stream<String> thingIds) {
		return tag(thingIds.map(id -> new ThingDto(thingType, id)));
	}

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	default Tag tag(final String thingType, final Collection<String> thingIds) {
		return tag(thingType, thingIds.stream());
	}

	/**
	 * 给指定东西打上本标签。
	 *
	 * @return
	 */
	default Tag tag(final String thingType, final String... thingIds) {
		return tag(thingType, Stream.of(thingIds));
	}

	default Tag untag(final Stream<Tagging> taggings) {
		return untagThings(taggings.map(Tagging::against));
	}

	default Tag untag(final Collection<Tagging> taggings) {
		return untag(taggings.stream());
	}

	default Tag untag(final Tagging... taggings) {
		return untag(Stream.of(taggings));
	}

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	Tag untagThings(Stream<Thing> things);

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThings(final Collection<Thing> things) {
		return untagThings(things.stream());
	}

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThings(final Thing... things) {
		return untagThings(Stream.of(things));
	}

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThings(final String thingType, final Stream<String> thingIds) {
		return untagThings(thingIds.map(id -> new ThingDto(thingType, id)));
	}

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThings(final String thingType, final Collection<String> thingIds) {
		return untagThings(thingType, thingIds.stream());
	}

	/**
	 * 从指定东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThings(final String thingType, final String... thingIds) {
		return untagThings(thingType, Stream.of(thingIds));
	}

	/**
	 * 从指定类型的所有东西上移除本标签。
	 *
	 * @return
	 */
	default Tag untagThingsOf(final String thingType) {
		untag(taggingsOf(thingType));
		return this;
	}

	//===========================================

	static String str(final Tag tag) {
		return StrKit.format("Tag#{0}", tag.name());
	}

	static int hash(final Tag tag) {
		return Objects.hash(tag.name());
	}

	static boolean eq(final Tag tag, final Object o) {
		return o instanceof final Tag other && tag.name().equals(other.name());
	}
}
