/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag;

import me.magicall.Thing;
import me.magicall.scope.Scope;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class TagDto implements Tag {
	public Scope scope;
	public String name;
	public String description;
	public List<Tagging> taggings;

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public TagDto renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public String description() {
		return description;
	}

	@Override
	public TagDto describedAs(final String description) {
		this.description = description;
		return this;
	}

	@Override
	public Stream<Tagging> taggings() {
		return taggings == null ? Stream.empty() : taggings.stream();
	}

	@Override
	public Tag tag(final Stream<Thing> things) {
		if (taggings != null) {
			things.forEach(thing -> taggings.add(new TaggingDto(this, thing)));
		}
		return this;
	}

	@Override
	public Tag untagThings(final Stream<Thing> things) {
		things.forEach(thing -> {
			for (final Iterator<Tagging> iterator = taggings.iterator(); iterator.hasNext(); ) {
				final Tagging tagging = iterator.next();
				final var against = tagging.against();
				if (against.equals(thing)) {
					iterator.remove();
				}
			}
		});
		return this;
	}

	@Override
	public String toString() {
		return Tag.str(this);
	}

	@Override
	public int hashCode() {
		return Tag.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Tag.eq(this, o);
	}
}
