/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.tag;

import me.magicall.Thing;

import java.time.Instant;

public class TaggingDto implements Tagging {
	public Tag tag;
	public Thing against;
	public Instant taggingTime;

	public TaggingDto() {
	}

	public TaggingDto(final Tag tag, final Thing against) {
		this(tag, against, Instant.now());
	}

	public TaggingDto(final Tag tag, final Thing against, final Instant taggingTime) {
		this.tag = tag;
		this.against = against;
		this.taggingTime = taggingTime;
	}

	@Override
	public Tag tag() {
		return tag;
	}

	@Override
	public Thing against() {
		return against;
	}

	@Override
	public Instant taggingTime() {
		return taggingTime;
	}

	@Override
	public String toString() {
		return Tagging.str(this);
	}

	@Override
	public int hashCode() {
		return Tagging.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Tagging.eq(this, o);
	}
}
