/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.relation;

import me.magicall.HasWeight;
import me.magicall.Thing;
import me.magicall.scope.Scope;

import java.util.Objects;

public class RelationDto implements Relation {
	public Scope scope;
	public Thing from;
	public Thing to;
	public Thing forwardPayload = Thing.EMPTY;
	public Thing backwardPayload = Thing.EMPTY;
	public float weight = HasWeight.COMMON_WEIGHT;

	@Override
	public RelationDto from(final Thing from) {
		this.from = from;
		return this;
	}

	@Override
	public RelationDto to(final Thing to) {
		this.to = to;
		return this;
	}

	@Override
	public RelationDto weight(final float weight) {
		this.weight = weight;
		return this;
	}

	@Override
	public RelationDto forwardWith(final Thing forwardPayload) {
		this.forwardPayload = forwardPayload;
		return this;
	}

	@Override
	public RelationDto backwardWith(final Thing backwardPayload) {
		this.backwardPayload = backwardPayload;
		return this;
	}

	@Override
	public RelationDto putIn(final Scope newScope) {
		scope = newScope;
		return this;
	}

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public Thing from() {
		return from;
	}

	@Override
	public Thing to() {
		return to;
	}

	@Override
	public Thing forwardPayload() {
		return Objects.requireNonNullElse(forwardPayload, Thing.EMPTY);
	}

	@Override
	public Thing backwardPayload() {
		return Objects.requireNonNullElse(backwardPayload, Thing.EMPTY);
	}

	@Override
	public float weight() {
		return weight;
	}

	@Override
	public String toString() {
		return Relation.str(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Relation.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Relation.hash(this);
	}
}
