/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.relation;

import me.magicall.HasDirection;
import me.magicall.HasWeight;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.scope.Scope;
import me.magicall.scope.Scoped;

import java.util.Objects;

/**
 * 表示两物之关系。
 * 唯一性：{@link #scope()}+{@link #from()}+{@link #to()}+{@link #forwardPayload()}+{@link #backwardPayload()}。权重不是唯一性的一部分。
 */
public interface Relation extends Scoped, HasDirection, HasWeight {
	String TYPE = "relation";

	Thing forwardPayload();

	/**
	 * 修改关系的正向载荷。
	 *
	 * @return
	 */
	default Relation forwardWith(final Thing forwardPayload) {
		throw new OpNotAllowedException(Thing.of(TYPE, toString()), "→", forwardPayload);
	}

	Thing backwardPayload();

	/**
	 * 修改关系的反向载荷。
	 *
	 * @return
	 */
	default Relation backwardWith(final Thing backwardPayload) {
		throw new OpNotAllowedException(Thing.of(TYPE, toString()), "←", backwardPayload);
	}

	static String str(final Relation relation) {
		final var scope = relation.scope();
		final var from = relation.from();
		final var to = relation.to();
		final var forwardPayload = relation.forwardPayload();
		final var backwardPayload = relation.backwardPayload();
		return StrKit.format("({1}-{3}→/←{4}-{2})@{0}", scope, from, to, forwardPayload, backwardPayload);
	}

	static int hash(final Relation relation) {
		return Objects.hash(relation.from(), relation.to(), relation.forwardPayload(), relation.backwardPayload(),
												relation.scope());
	}

	static boolean eq(final Relation relation, final Object o) {
		return o instanceof final Relation other//
					 && check(relation, other.scope(), other.from(), other.to(), other.forwardPayload(), other.backwardPayload());
	}

	static boolean check(final Relation relation, final Scope scope, final Thing from, final Thing to,
											 final Thing forwardPayload, final Thing backwardPayload) {
		return check(relation, scope, from, to)//
					 && relation.forwardPayload().equals(forwardPayload)//
					 && relation.backwardPayload().equals(backwardPayload);
	}

	static boolean check(final Relation relation, final Scope scope, final Thing from, final Thing to) {
		return relation.scope().equals(scope) && checkFromTo(relation, from, to);
	}

	static boolean checkFromTo(final Relation relation, final Thing from, final Thing to) {
		return relation.from().equals(from) && relation.to().equals(to);
	}
}
