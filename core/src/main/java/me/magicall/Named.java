/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 具名之物。
 */
@FunctionalInterface
public interface Named {
	String TYPE = Named.class.getSimpleName();

	String name();

	default String fullName() {
		return name();
	}

	default String shortName() {
		return name();
	}

	default Named renameTo(final String newName) {
		throw new OpNotAllowedException(Thing.of(TYPE, name()), "renameTo", Thing.of(TYPE, newName));
	}

	//=================================================================

	static Named of(final String name) {
		return new SimpleNamed(name);
	}

	/**
	 * 将一个有名字的对象的列表转化为它们的名字的列表。
	 *
	 * @param source 来源
	 * @return 名字列表
	 */
	static List<String> toNames(final Collection<? extends Named> source) {
		return source.stream().map(Named::name).toList();
	}

	/**
	 * 将一个有名字的对象的集合转化为“名字-对象本身”的映射表。
	 * 注意：若原集合中存在同名元素，则名字后迭代的元素将覆盖先迭代的元素，所以返回的映射表的大小可能小于原集合大小。
	 *
	 * @param source 来源
	 * @param <T> 元素的类型
	 * @return 名字-元素的映射
	 */
	static <T extends Named> Map<String, T> map(final Stream<T> source) {
		return source.collect(Collectors.toMap(Named::name, Function.identity()));
	}

	/**
	 * 返回一个判断器，判断是否同名。
	 *
	 * @param name 名字
	 * @param <T> 元素的类型
	 * @return 判断器
	 */
	static <T extends Named> Predicate<T> predicate(final String name) {
		return named -> Objects.equals(named.name(), name);
	}

	//============================================================================

	/**
	 * 为了equals、hashCode、toString，不得不写一个实现类。java这点真不爽！
	 */
	class SimpleNamed implements Named {
		private final NamedDto raw;

		public SimpleNamed(final String name) {
			this(new NamedDto(name));
		}

		public SimpleNamed(final NamedDto raw) {
			this.raw = raw;
		}

		@Override
		public String name() {
			return raw.name();
		}

		@Override
		public String toString() {
			return raw.toString();
		}

		@Override
		public int hashCode() {
			return raw.hashCode();
		}

		@Override
		public boolean equals(final Object o) {
			return raw.equals(o);
		}
	}
}
