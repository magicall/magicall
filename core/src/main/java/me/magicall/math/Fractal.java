/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import me.magicall.HasParts;

/**
 * 分形。
 * 实现此接口，意味着包含若干个“部分”，每个部分与本体具有相同结构，即也“包含若干个‘部分’，每个部分……”
 */
@FunctionalInterface
public interface Fractal<_Part extends Fractal<_Part>> extends HasParts<_Part> {
}
