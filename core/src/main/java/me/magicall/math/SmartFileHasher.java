package me.magicall.math;

import me.magicall.program.lang.java.贵阳DearSun.exception.EmpFileException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SmartFileHasher {
	private static final int MAX_BUFFER = 4 * 1024 * 1024;// 4MB
	private static final int MIN_BUFFER = 8 * 1024;// 8KB
	// 根据文件大小选择处理策略的阈值（单位：字节）
	private static final long SMALL_FILE_THRESHOLD = 10 * 1024 * 1024;    // 10MB
	private static final long LARGE_FILE_THRESHOLD = 1024 * 1024 * 1024;  // 1GB

	/**
	 * 智能计算文件哈希值
	 * <10MB	BufferedInputStream	减少系统调用，适合小文件快速读取
	 * 10MB ~ 1GB	FileChannel + 直接内存	平衡内存开销与I/O效率
	 * >1GB	内存映射分段处理	避免大内存占用，接近硬件极限性能
	 * @param file      目标文件
	 * @param algorithm 哈希算法（如 SHA-256、MD5）
	 * @return 十六进制哈希字符串
	 */
	public static String calculateHash(final File file, final String algorithm) {
		// 1. 参数校验
		if (!file.exists()) {
			throw new EmpFileException("file");
		}

		final MessageDigest digest;
		try {
			digest = MessageDigest.getInstance(algorithm);
		} catch (final NoSuchAlgorithmException e) {
			throw new WrongArgException("algorithm", algorithm, e);
		}

		// 2. 根据文件大小选择最优策略
		try {
			final long fileSize = file.length();
			if (fileSize < SMALL_FILE_THRESHOLD) {
				hashWithBufferedIO(file, digest);
			} else if (fileSize < LARGE_FILE_THRESHOLD) {
				hashWithFileChannel(file, digest);
			} else {
				hashWithMemoryMap(file, digest);
			}
			return bytesToHex(digest.digest());
		} catch (final FileNotFoundException e) {
			throw new EmpFileException("file", e);
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
	}

	// ==================== 策略实现 ====================

	/** 小文件：BufferedInputStream + 动态缓冲区 */
	private static void hashWithBufferedIO(final File file, final MessageDigest digest)
			throws IOException, FileNotFoundException {
		try (final InputStream is = new BufferedInputStream(new FileInputStream(file))) {
			final byte[] buffer = new byte[calculateBufferSize(file.length())];
			int bytesRead;
			while ((bytesRead = is.read(buffer)) != -1) {
				digest.update(buffer, 0, bytesRead);
			}
		}
	}

	/** 中等文件：FileChannel + 堆外内存 */
	private static void hashWithFileChannel(final File file, final MessageDigest digest)
			throws IOException, FileNotFoundException {
		try (final var inputStream = new FileInputStream(file);//
				 final FileChannel channel = inputStream.getChannel()) {
			final ByteBuffer buffer = ByteBuffer.allocateDirect(calculateBufferSize(file.length()));
			while (channel.read(buffer) != -1) {
				buffer.flip();
				digest.update(buffer);
				buffer.clear();
			}
		}
	}

	/** 大文件：内存映射 + 分段处理 */
	private static void hashWithMemoryMap(final File file, final MessageDigest digest)
			throws IOException, FileNotFoundException {
		try (final var randomAccessFile = new RandomAccessFile(file, "r");
				 final FileChannel channel = randomAccessFile.getChannel()) {
			long position = 0;
			long remaining = channel.size();
			while (remaining > 0) {
				final long chunkSize = Math.min(remaining, Integer.MAX_VALUE);
				final MappedByteBuffer buffer = channel.map(MapMode.READ_ONLY, position, chunkSize);
				digest.update(buffer);
				position += chunkSize;
				remaining -= chunkSize;
			}
		}
	}

	// ==================== 辅助方法 ====================

	/** 动态计算缓冲区大小（优化内存使用） */
	private static int calculateBufferSize(final long fileSize) {
		return (int) Math.min(Math.max(fileSize / 100, MIN_BUFFER), MAX_BUFFER);
	}

	/** 字节数组转十六进制字符串 */
	private static String bytesToHex(final byte[] bytes) {
		final StringBuilder hexString = new StringBuilder(2 * bytes.length);
		for (final byte b : bytes) {
			hexString.append(String.format("%02x", b));
		}
		return hexString.toString();
	}
}