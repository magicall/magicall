/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import me.magicall.logic.ComparisonOp;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * 单值对单值的比较符。
 */
public enum CommonComparableComparisonOp implements ComparisonOp<Comparable<?>, Comparable<?>> {
	等于("=") {
		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult == 0;
		}
	},
	不等于("≠", "!=") {
		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult != 0;
		}
	},
	大于(">", "＞") {
		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult > 0;
		}
	},
	大于等于("≥", ">=") {
		@Override
		public String shortName() {
			return "不小于";
		}

		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult >= 0;
		}
	},
	小于("<", "＜") {
		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult < 0;
		}
	},
	小于等于("≤", "<=") {
		@Override
		public String shortName() {
			return "不大于";
		}

		@Override
		public boolean acceptCompareResult(final int compareResult) {
			return compareResult <= 0;
		}
	};

	private final String[] signs;

	CommonComparableComparisonOp(final String... signs) {
		this.signs = signs;
	}

	public <T> boolean test(final T o1, final T o2, final Comparator<? super T> comparator) {
		return acceptCompareResult(comparator.compare(o1, o2));
	}

	@Override
	public boolean test(final Comparable<?> o, final Comparable<?> o2) {
		return acceptCompareResult(((Comparable) o).compareTo(o2));
	}

	public abstract boolean acceptCompareResult(final int compareResult);

	public Stream<String> signs() {
		return Stream.of(signs);
	}

	public String sign() {
		return signs[0];
	}

	public String join(final Object val) {
		return StrKit.format("{0} {1}", signs[0], val);
	}

	public String join(final Object one, final Object other) {
		return StrKit.format("{0} {1} {2}", one, signs[0], other);
	}

	@Override
	public String toString() {
		return signs[0];
	}

	public static CommonComparableComparisonOp fromSign(final String sign) {
		return Stream.of(values()).filter(e -> e.signs().anyMatch(sign::equals)).findFirst().orElse(null);
	}
}
