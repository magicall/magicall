/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

/**
 * 二维空间上的一个坐标点。
 *
 * @author Liang Wenjian.
 */
public class Point {
	private double x;
	private double y;

	public Point() {
	}

	public Point(final double x, final double y) {
		_setX(x);
		_setY(y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setX(final double x) {
		_setX(x);
	}

	private void _setX(final double x) {
		checkX(x);
		this.x = x;
	}

	protected void checkX(final double x) {
	}

	public void setY(final double y) {
		_setY(y);
	}

	private void _setY(final double y) {
		checkY(y);
		this.y = y;
	}

	protected void checkY(final double y) {
	}

	public static Point of(final double x, final double y) {
		return new Point(x, y);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !getClass().isAssignableFrom(o.getClass())) {
			return false;
		}

		final var point = (Point) o;

		return MathKit.eq(getX(), point.getX()) && MathKit.eq(getY(), point.getY());
	}

	@Override
	public int hashCode() {
		return 31 * Double.hashCode(getX()) + Double.hashCode(getY());
	}

	@Override
	public String toString() {
		return "(" + x + ',' + y + ')';
	}
}
