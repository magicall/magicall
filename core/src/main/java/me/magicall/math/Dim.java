/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import me.magicall.logic.Concept;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * 数学领域中的“维度”：我们所关切的若干已知或未知事物都具有若干种相似的属性，这些属性的种类称为“维度”。比如“长度”、“价格”。
 * 特性：
 * 1，不同维度之间正交。
 * 2，一个维度的值域可以有限也可以无限。
 * 3，事物的属性值之间可以离散也可以连续。
 * 4，事物的属性值之间可以“能比较大小”也可以“不能比较大小”。
 */
public interface Dim<_Val> extends Concept {
	/**
	 * 值的java类型。
	 * 技术方法。
	 *
	 * @return
	 */
//	EnhancedClass<_Val> valJavaType();

	/**
	 * 计量单位。
	 *
	 * @return 若无则返回null。
	 */
	String unit();

	/**
	 * 值域。注意：值的数量可能无限。
	 *
	 * @return 若值不可枚举则返回null；若本维度没有任何一种可能值（虽然闻所未闻，但逻辑上可以存在），则返回空流。
	 */
	Stream<_Val> availableVals();

	default boolean isValEnumerable() {
		return availableVals() == null;
	}

//下面这些属性只对“值可以比较的维度”有用。
//	/**
//	 * 最大值。
//	 *
//	 * @return 若无则返回null。
//	 */
//	T maxVal();
//
//	/**
//	 * 最小值。
//	 *
//	 * @return 若无则返回null。
//	 */
//	T minVal();
//
//	/**
//	 * 步进。
//	 *
//	 * @return 若无则返回null。比如文本类型的值基本不存在步进。
//	 */
//	T valStep();

	static String str(final Dim<?> dim) {
		return StrKit.format("{0}（{1}）", dim.name(), dim.unit());
	}

	static int hash(final Dim<?> dim) {
		return Objects.hash(dim.name());
	}

	static boolean eq(final Dim<?> dim, final Object other) {
		return other instanceof final Dim<?> o && Objects.equals(dim.name(), o.name());
	}
}
