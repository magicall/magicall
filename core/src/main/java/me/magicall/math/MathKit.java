/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

public interface MathKit {

	/**
	 * 计算分位数。
	 *
	 * @param percentPlace 标杆位置【=(1-top n)%】
	 * @return
	 */
	static double percentile(final Stream<Double> vals, final double percentPlace) {
		final List<Double> list = vals.sorted().toList();
		final var v = percentPlace * (list.size() - 1);
		final int intPart = (int) Math.floor(v);
		final var fractionalPart = v - intPart;
		return (1 - fractionalPart) * list.get(intPart) + fractionalPart * list.get(intPart + 1);
	}

	/**
	 * 计算分位数。
	 *
	 * @param percentPlace 标杆位置【=(1-top n)%】
	 * @return
	 */
	static double amountPercentile(final Stream<BigDecimal> vals, final double percentPlace) {
		final List<BigDecimal> list = vals.sorted().toList();
		final var v = BigDecimal.valueOf(percentPlace).multiply(BigDecimal.valueOf(list.size() - 1));
		final int intPart = v.intValue();
		final var fractionalPart = v.subtract(BigDecimal.valueOf(intPart));
		//即：(1 - fractionalPart) * list.get(intPart) + fractionalPart * list.get(intPart + 1)
		return BigDecimal.ONE.subtract(fractionalPart)//
				.multiply(list.get(intPart))//
				.add(fractionalPart.multiply(list.get(intPart + 1)))//
				.doubleValue();
	}

	static boolean eq(final float a, final float b) {
		return Kits.FLOAT.equals(a, b);
	}

	static boolean eq(final double a, final double b) {
		return Kits.DOUBLE.equals(a, b);
	}
}
