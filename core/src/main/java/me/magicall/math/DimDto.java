/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import java.util.List;
import java.util.stream.Stream;

public class DimDto<_Val> implements Dim<_Val> {
	public String name;
	public String definition;
	//	public EnhancedClass<_Val> valJavaType;
	public String unit;
	public List<_Val> availableVals;

	@Override
	public String name() {
		return name;
	}

	@Override
	public DimDto<_Val> renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public String definition() {
		return definition;
	}

	@Override
	public DimDto<_Val> redefine(final String newDefinition) {
		definition = newDefinition;
		return this;
	}

//	@Override
//	public EnhancedClass<_Val> valJavaType() {
//		return valJavaType;
//	}

	@Override
	public String unit() {
		return unit;
	}

	@Override
	public Stream<_Val> availableVals() {
		return availableVals == null ? Stream.empty() : availableVals.stream();
	}

	@Override
	public String toString() {
		return Dim.str(this);
	}

	@Override
	public int hashCode() {
		return Dim.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Dim.eq(this, o);
	}
}
