/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.math;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class EncryptKit {
	private static final Pattern MINUS_PATTERN = Pattern.compile("-", Pattern.LITERAL);

	private static final char[] CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	private static final BigInteger _62 = new BigInteger(String.valueOf(CHARS.length));

	public static String randomRadix62Uuid() {
		final var uuid = UUID.randomUUID().toString();
		final var pureUuid = MINUS_PATTERN.matcher(uuid).replaceAll(Matcher.quoteReplacement(""));
		return numStrToRadix52(pureUuid);
	}

	public static String toRadix62(final long num) {
		return numStrToRadix52(Long.toHexString(num));
	}

	public static String toRadix62(final int num) {
		return numStrToRadix52(Integer.toHexString(num));
	}

	private static String numStrToRadix52(final String pureUuid) {
		final var sb = new StringBuilder();
		Stream.iterate(new BigInteger(pureUuid, 16), bi -> bi.compareTo(BigInteger.ZERO) > 0, bi -> bi.divide(_62))
				.map(bi -> bi.mod(_62)).map(mod -> CHARS[mod.intValue()]).forEach(sb::append);
		return sb.reverse().toString();
	}

	private static String bytesToStr(final byte[] bytes) {
		final var sb = new StringBuilder();
		for (final var element : bytes) {
			int i = element;
			if (i < 0) {
				i += 256;
			}
			if (i < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(i));
		}
		return sb.toString();
	}

	public static String md5(final String plainText) {
		if (plainText == null) {
			return null;
		}
		try {
			final var md = MessageDigest.getInstance("MD5");
			final var b = md.digest(plainText.getBytes());
			return bytesToStr(b);
		} catch (final NoSuchAlgorithmException e) {
			//almost impossible
			throw new UnknownException(e);
		}
	}
}
