/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.data;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * 数据分片。比“分页（{@link Page}）”少了“总数”，适用于不需要关心总数的场景。
 */
public class Slice<T> implements List<T> {
	/**
	 * 偏移量，从0开始。
	 */
	public final int offset;
	/**
	 * 限制数量。本分片元素数量最多不超过此值。
	 */
	public final int limit;
	private final List<T> raw;

	public Slice(final int offset, final int limit, final List<T> raw) {
		this.offset = offset;
		this.limit = limit;
		this.raw = raw;
	}

	public int offset() {
		return offset;
	}

	public int limit() {
		return limit;
	}

	public static <T> Slice<T> fromAll(final Stream<T> all, final int offset, final int limit) {
		return fromSliceElements(offset, limit, all.skip(offset).limit(limit));
	}

	public static <T> Slice<T> fromSliceElements(final int offset, final int limit, final Stream<T> sliceElements) {
		return new Slice<>(offset, limit, sliceElements.toList());
	}

	@Override
	public T get(final int index) {
		return raw.get(index);
	}

	@Override
	public int size() {
		return raw.size();
	}

	@Override
	public boolean isEmpty() {
		return raw.isEmpty();
	}

	@Override
	public boolean contains(final Object o) {
		return raw.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return raw.iterator();
	}

	@Override
	public Object[] toArray() {
		return raw.toArray();
	}

	@Override
	public <T1> T1[] toArray(final T1[] a) {
		return raw.toArray(a);
	}

	@Override
	public boolean add(final T t) {
		return raw.add(t);
	}

	@Override
	public boolean remove(final Object o) {
		return raw.remove(o);
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		return raw.containsAll(c);
	}

	@Override
	public boolean addAll(final Collection<? extends T> c) {
		return raw.addAll(c);
	}

	@Override
	public boolean addAll(final int index, final Collection<? extends T> c) {
		return raw.addAll(index, c);
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		return raw.removeAll(c);
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		return raw.retainAll(c);
	}

	@Override
	public void replaceAll(final UnaryOperator<T> operator) {
		raw.replaceAll(operator);
	}

	@Override
	public void sort(final Comparator<? super T> c) {
		raw.sort(c);
	}

	@Override
	public void clear() {
		raw.clear();
	}

	@Override
	public boolean equals(final Object o) {
		return raw.equals(o);
	}

	@Override
	public int hashCode() {
		return raw.hashCode();
	}

	@Override
	public T set(final int index, final T element) {
		return raw.set(index, element);
	}

	@Override
	public void add(final int index, final T element) {
		raw.add(index, element);
	}

	@Override
	public T remove(final int index) {
		return raw.remove(index);
	}

	@Override
	public int indexOf(final Object o) {
		return raw.indexOf(o);
	}

	@Override
	public int lastIndexOf(final Object o) {
		return raw.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return raw.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(final int index) {
		return raw.listIterator(index);
	}

	@Override
	public List<T> subList(final int fromIndex, final int toIndex) {
		return raw.subList(fromIndex, toIndex);
	}

	@Override
	public Spliterator<T> spliterator() {
		return raw.spliterator();
	}

	@Override
	public <T1> T1[] toArray(final IntFunction<T1[]> generator) {
		return raw.toArray(generator);
	}

	@Override
	public boolean removeIf(final Predicate<? super T> filter) {
		return raw.removeIf(filter);
	}

	@Override
	public Stream<T> stream() {
		return raw.stream();
	}

	@Override
	public Stream<T> parallelStream() {
		return raw.parallelStream();
	}

	@Override
	public void forEach(final Consumer<? super T> action) {
		raw.forEach(action);
	}
}
