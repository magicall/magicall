/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.data;

import java.util.List;
import java.util.stream.Stream;

/**
 * 更贴近自然语言的分页。
 * 从1开始。不知道也不需要知道有没有上一页下一页。
 */
public record Page<T>(int size, int curPage, List<T> items, long allCount) {
	public static final int FIRST_PAGE = 1;
	public static final Page<Object> EMPTY = new Page<>(0, FIRST_PAGE, List.of(), 0);

	public Page(final int size, final List<T> items, final long allCount) {
		this(size, FIRST_PAGE, items, allCount);
	}

	public Page(final int size, final List<T> items) {
		this(size, FIRST_PAGE, items);
	}

	public Page(final int size, final int curPage, final List<T> items) {
		this(size, curPage, items, items.size());
	}

	public static <T> Page<T> of(final int size, final int curPage, final Stream<T> stream) {
		final var skip = (curPage - 1) * size;
		final var list = stream.toList();
		final var allCount = list.size();
		return new Page<>(size, curPage, list.subList(skip, Math.min(allCount, skip + size)), allCount);
	}

	public static <T> Page<T> of(final int size, final int curPage, final Stream<T> stream, final long allCount) {
		final var skip = (curPage - 1) * size;
		final var list = stream.skip(skip).limit(size).toList();
		return new Page<>(size, curPage, list, allCount);
	}

	@SuppressWarnings("unchecked")
	public static <T> Page<T> empty(final int size, final int curPage) {
		return (Page<T>) EMPTY;
	}
}
