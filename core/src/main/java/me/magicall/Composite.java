/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import me.magicall.Composite.PartsRelation;

/**
 * 组合。组合的部分之间具有一定的关系。
 * Combination：指的是将两个或两个以上的不同元素或事物混合或联合在一起，形成一个整体。它强调的是不同部分的汇集，这些部分共同作用以产生新的结果或具有新的特性。（不同元素）
 * Composite：一个由多个同类元素组成的新元素，特别是当这些元素的性质在合成过程中保持并赋予了新元素不同的特性时。在材料科学中尤其常见。
 */
public interface Composite<_Part, _PartsRelation extends PartsRelation> extends HasParts<_Part> {
	_PartsRelation partsRelation();

	@FunctionalInterface
	interface PartsRelation extends Named {
	}
}
