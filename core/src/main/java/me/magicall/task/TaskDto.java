/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.task;

import me.magicall.scope.Scope;

import java.time.Instant;

public class TaskDto implements Task {
	public Scope scope;
	public String name;
	public String type;
	public String description;
	public Double allCount;
	public Double doneCount;
	public Instant startTime;
	public Instant endTime;

	@Override
	public Scope scope() {
		return scope;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String type() {
		return type;
	}

	@Override
	public String description() {
		return description == null ? description() : description;
	}

	public Task describedBy(final String newDescription) {
		description = newDescription;
		return this;
	}

	@Override
	public double allCount() {
		return allCount == null ? allCount() : allCount;
	}

	@Override
	public Task setAllCount(final double allCount) {
		this.allCount = allCount;
		return this;
	}

	@Override
	public double doneCount() {
		return doneCount == null ? doneCount() : doneCount;
	}

	@Override
	public Task setDoneCount(final double doneCount) {
		this.doneCount = doneCount;
		return this;
	}

	@Override
	public Instant endTime() {
		return endTime;
	}

	@Override
	public Instant startTime() {
		return startTime;
	}

	@Override
	public void endAt(final Instant endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return Task.str(this);
	}

	@Override
	public int hashCode() {
		return Task.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Task.eq(this, o);
	}
}
