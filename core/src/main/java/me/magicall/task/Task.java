/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.task;

import me.magicall.Described;
import me.magicall.Named;
import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.scope.Scoped;
import me.magicall.time.MayEnd;
import me.magicall.time.WillStart;

import java.time.Instant;
import java.util.Objects;

public interface Task extends Named, Scoped, Described<Task>, WillStart, MayEnd {
	float MIN_PROGRESS = 0.0F;
	float MAX_PROGRESS = 1.0F;

	String type();

	/**
	 * 总共需要完成的数量。
	 *
	 * @return
	 */
	default double allCount() {
		return 100;
	}

	default Task setAllCount(final double allCount) {
		throw new OpNotAllowedException(Thing.of(Task.class.getSimpleName(), toString()), "setAllCount",
																		Thing.ofNum(allCount));
	}

	/**
	 * 已完成的数量。
	 *
	 * @return
	 */
	default double doneCount() {
		return 0;
	}

	default Task setDoneCount(final double doneCount) {
		throw new OpNotAllowedException(Thing.of(Task.class.getSimpleName(), toString()), "setDoneCount",
																		Thing.ofNum(doneCount));
	}

	/**
	 * 进度百分比。
	 *
	 * @return
	 */
	default Float progress() {
		if (allCount() == 0) {
			return null;
		}
		return (float) (doneCount() / allCount()) * 100;
	}

	/**
	 * 标记该任务在指定的时间结束了。
	 */
	void endAt(Instant endTime);

	/**
	 * 结束、终止。
	 */
	@Override
	default void end() {
		endAt(Instant.now());
	}

	/**
	 * 完成。
	 * 会自动管理状态。
	 */
	default void finish() {
		setDoneCount(allCount());
		end();
	}

	static String str(final Task one) {
		return StrKit.format("Task'{0}'({3}:{4})({1}~{2})", one.name(), one.startTime(), one.endTime(), stateOf(one),
												 one.progress());
	}

	static int hash(final Task one) {
		return Objects.hash(one.name(), one.scope());
	}

	static boolean eq(final Task one, final Object o) {
		return o instanceof final Task other//
					 && Objects.equals(one.name(), other.name()) && Objects.equals(one.scope(), other.scope());
	}

	static ProgressState stateOf(final Task task) {
		if (!task.isStarted()) {
			return CommonProgressState.未开始;
		}
		if (task.isEnded()) {
			return CommonProgressState.已结束;
		}
		return CommonProgressState.进行中;
	}
}
