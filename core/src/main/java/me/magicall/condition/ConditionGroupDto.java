/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import com.google.common.collect.Lists;
import me.magicall.PartsLogicRelation;

import java.util.List;
import java.util.stream.Stream;

public class ConditionGroupDto implements ConditionGroup {
	public String name;
	public List<Condition> parts;
	public PartsLogicRelation partsRelation;
	public int weight;

	@Override
	public Stream<Condition> parts() {
		if (parts == null) {
			parts = Lists.newArrayList();
		}
		return parts.stream();
	}

	@Override
	public PartsLogicRelation partsRelation() {
		return partsRelation;
	}

	@Override
	public float weight() {
		return weight;
	}

	@Override
	public String toString() {
		return Condition.str(this);
	}

	@Override
	public int hashCode() {
		return Condition.hash(this);
	}

	@Override
	public boolean equals(final Object other) {
		return Condition.eq(this, other);
	}
}
