/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import java.util.function.Supplier;

public interface ConditionContext {
	<T> T getVal(String varName);

	default boolean hasVal(final String varName) {
		return getVal(varName) != null;
	}

	default <T> T getOrInit(final String varName, final Supplier<T> initValSupplier) {
		final T val = getVal(varName);
		if (val != null) {
			return val;
		}
		final var newVal = initValSupplier.get();
		setVal(varName, newVal);
		return newVal;
	}

	void setVal(String varName, Object val);

	/**
	 * @return 旧值
	 */
	<T> T replaceVal(String varName, T newVal);

	default void removeVal(final String varName) {
		replaceVal(varName, null);
	}
}
