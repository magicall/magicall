/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.Composite;
import me.magicall.PartsLogicRelation;

import java.util.stream.Stream;

public interface ConditionGroup extends Condition, Composite<Condition, PartsLogicRelation> {
	@Override
	default ConditionAsConditionItem left() {
		return new ConditionAsConditionItem(parts());
	}

	@Override
	default ConditionItemComparator comparator() {
		return ConditionPartsComparator.of(partsRelation());
	}

	@Override
	default ConditionItem right() {
		return null;//无右项
	}

	enum ConditionPartsComparator implements ConditionItemComparator {
		全部符合(PartsLogicRelation.与),
		任一符合(PartsLogicRelation.或);

		private final PartsLogicRelation partsLogicRelation;

		ConditionPartsComparator(final PartsLogicRelation partsLogicRelation) {
			this.partsLogicRelation = partsLogicRelation;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean test(final Object o, final Object o2) {
			//o将是左项产出的实际值。
			return partsLogicRelation.reduce((Stream<Boolean>) o);
		}

		static ConditionPartsComparator of(final PartsLogicRelation partsLogicRelation) {
			return partsLogicRelation == PartsLogicRelation.与 ? 全部符合 : 任一符合;
		}
	}

	class ConditionAsConditionItem implements ConditionItem {
		private final Stream<Condition> parts;

		public ConditionAsConditionItem(final Stream<Condition> parts) {
			this.parts = parts;
		}

		@Override
		public boolean isRef() {
			return false;
		}

		@Override
		public Stream<Condition> val() {
			return parts;
		}

		@Override
		public Object factValIn(final ConditionContext context) {
			return parts.map(part -> part.isSatisfiedIn(context));
		}
	}
}
