/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.Described;
import me.magicall.Named;

import java.util.function.BiPredicate;

/**
 * 条件项比较符。诸如“等于”、“大于”、“小于”之类。
 */
public interface ConditionItemComparator
		extends BiPredicate<Object, Object>, Named, Described<ConditionItemComparator> {

	@Override
	default String description() {
		return name();
	}
}
