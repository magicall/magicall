/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import com.google.common.collect.Maps;

import java.util.Map;

public class SimpleConditionContext implements ConditionContext {

	private final Map<String, Object> map = Maps.newHashMap();

	@Override
	@SuppressWarnings("unchecked")
	public <T> T getVal(final String varName) {
		return (T) map.get(varName);
	}

	@Override
	public void setVal(final String varName, final Object val) {
		replaceVal(varName, val);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T replaceVal(final String varName, final T newVal) {
		return (T) map.put(varName, newVal);
	}
}
