/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

public class ConditionItemDto implements ConditionItem {
	public boolean isRef;
	public Object val;

	public ConditionItemDto() {
	}

	public ConditionItemDto(final boolean isRef, final Object val) {
		this.isRef = isRef;
		this.val = val;
	}

	@Override
	public boolean isRef() {
		return isRef;
	}

	@Override
	public Object val() {
		return val;
	}

	@Override
	public String toString() {
		return ConditionItem.str(this);
	}

	@Override
	public int hashCode() {
		return ConditionItem.hash(this);
	}

	@Override
	public boolean equals(final Object o) {
		return ConditionItem.eq(this, o);
	}
}
