/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.Thing;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.Objects;

/**
 * 条件项，是条件的左项或右项。
 */
public interface ConditionItem {

	/**
	 * 本条件项的值是否为引用。
	 *
	 * @return
	 */
	boolean isRef();

	default ConditionItem isRef(final boolean isRef) {
		throw new OpNotAllowedException(Thing.of(ConditionItem.class.getSimpleName(), toString()), "isRef",
				Thing.of("bool", isRef));
	}

	/**
	 * 条件项的值配置。
	 * 若值是“引用”（{@link #isRef()}==true） ，则为引用的唯一标识符，需要另行获取具体值；否则为具体值。
	 *
	 * @return
	 */
	Object val();

	default ConditionItem val(final Object val) {
		throw new OpNotAllowedException(Thing.of(ConditionItem.class.getSimpleName(), toString()), "val",
				Thing.ofUnknownType(val));
	}

	default Object factValIn(final ConditionContext context) {
		final var val = val();
		if (isRef()) {
			final var valThing = (Thing) val;
			return cast(valThing.type(), context.getVal(valThing.idInType()));
		}
		if (val instanceof final Thing valThing) {
			return cast(valThing.type(), valThing.idInType());
		}
		return val;
	}

	//==========================================================

	static Object cast(final String type, final String valStr) {
		if (Thing.STR_TYPE_SIGN.equals(type)) {
			return valStr;
		}
		if (Thing.NUM_TYPE_SIGN.equals(type)) {
			return Kits.DOUBLE.parse(valStr);
		}
		if (Thing.BOOL_TYPE_SIGN.equals(type)) {
			return Kits.BOOL.parse(valStr);
		}
		return valStr;
	}

	static ConditionItemDto of(final boolean isRef, final Object val) {
		return new ConditionItemDto(isRef, val);
	}

	static ConditionItem ofRef(final Thing val) {
		return of(true, val);
	}

	private static ConditionItem ofNotRef(final Object val) {
		return of(false, val);
	}

	static ConditionItem of(final String val) {
		return ofNotRef(Thing.ofStr(val));
	}

	static ConditionItem of(final Number val) {
		return ofNotRef(Thing.ofNum(val));
	}

	static ConditionItem of(final Boolean val) {
		return ofNotRef(Thing.ofBool(val));
	}

	static ConditionItem of(final Object val) {
		return ofNotRef(Thing.ofObj(val));
	}

	//-------------------------------------------------

	static String str(final ConditionItem item) {
		return item.isRef() ? "→" + item.val() : String.valueOf(item.val());
	}

	static int hash(final ConditionItem item) {
		return Objects.hash(item.isRef(), item.val());
	}

	static boolean eq(final ConditionItem one, final Object other) {
		return other instanceof final ConditionItem o//
				&& Objects.equals(one.isRef(), o.isRef())//
				&& Objects.equals(one.val(), o.val());
	}
}
