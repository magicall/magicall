/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.Described;
import me.magicall.Entity;

@Deprecated //改用Condition
/**
 * 条件组元素，是条件组的组成部分，可以是条件，或者也是条件组（子级条件组）。
 */ public interface ConditionGroupItem<_MyType extends ConditionGroupItem<_MyType>>
		extends Entity<String, _MyType>, Described<ConditionGroupItem<_MyType>> {

	/**
	 * 在指定上下文中是否满足条件。
	 *
	 * @return
	 */
	boolean isSatisfiedIn(ConditionContext context);
}
