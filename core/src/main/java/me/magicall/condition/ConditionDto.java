/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

public class ConditionDto implements Condition {
	public ConditionItem left;
	public ConditionItemComparator comparator = CommonConditionItemComparator.EQUALS;
	public ConditionItem right;
	public float weight;

	@Override
	public ConditionItem left() {
		return left;
	}

	@Override
	public ConditionItemComparator comparator() {
		return comparator;
	}

	@Override
	public ConditionItem right() {
		return right;
	}

	@Override
	public String toString() {
		return Condition.str(this);
	}

	@Override
	public int hashCode() {
		return Condition.hash(this);
	}

	@Override
	public boolean equals(final Object obj) {
		return Condition.eq(this, obj);
	}

	@Override
	public float weight() {
		return weight;
	}
}
