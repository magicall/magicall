/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.Described;
import me.magicall.HasWeight;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.util.Objects;

/**
 * 条件
 */
public interface Condition extends Described<Condition>, HasWeight {

	/**
	 * 左项
	 *
	 * @return
	 */
	ConditionItem left();

	/**
	 * 比较符
	 *
	 * @return
	 */
	ConditionItemComparator comparator();

	/**
	 * 右项。
	 * 可能为空，因为有些比较操作只需要一个操作数。
	 *
	 * @return
	 */
	ConditionItem right();

	/**
	 * 指定上下文是否满足本条件
	 *
	 * @return
	 */
	default boolean isSatisfiedIn(final ConditionContext context) {
		return comparator().test(getV(left(), context), getV(right(), context));
	}

	private static Object getV(final ConditionItem conditionItem, final ConditionContext context) {
		if (conditionItem == null) {
			return null;
		}
		return conditionItem.factValIn(context);
	}

	static String str(final Condition one) {
		final var description = one.description();
		if (!Kits.STR.isEmpty(description)) {
			return description;
		}
		final var right = one.right();
		if (right == null) {
			return StrKit.format("{0}: {1}", one.comparator(), one.left());
		}
		return StrKit.format("{0} {1} {2}", one.left(), one.comparator(), right);
	}

	static int hash(final Condition one) {
		return Objects.hash(one.left(), one.comparator(), one.right());
	}

	static boolean eq(final Condition one, final Object other) {
		return other instanceof final Condition o && Objects.equals(one.left(), o.left())//
				&& Objects.equals(one.comparator(), o.comparator())//
				&& Objects.equals(one.right(), o.right());
	}
}
