/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.condition;

import me.magicall.program.lang.LabelStyle;
import me.magicall.program.lang.java.贵阳DearSun.ObjKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public enum CommonConditionItemComparator implements ConditionItemComparator {
	EQUALS {
		@Override
		public boolean test(final Object a, final Object b) {
			return Objects.equals(a, b);
		}
	},
	GREATER {
		@Override
		public boolean test(final Object a, final Object b) {
			if (a instanceof final Comparable comparable) {
				return comparable.compareTo(b) > 0;
			}
			throw new UnknownException();
		}
	},
	GREATER_EQUALS {
		@Override
		public boolean test(final Object a, final Object b) {
			if (a instanceof final Comparable comparable) {
				return comparable.compareTo(b) >= 0;
			}
			throw new UnknownException();
		}
	},
	LESS {
		@Override
		public boolean test(final Object a, final Object b) {
			if (a instanceof final Comparable comparable) {
				return comparable.compareTo(b) < 0;
			}
			throw new UnknownException();
		}
	},
	LESS_EQUALS {
		@Override
		public boolean test(final Object a, final Object b) {
			if (a instanceof final Comparable comparable) {
				return comparable.compareTo(b) <= 0;
			}
			throw new UnknownException();
		}
	},
	CONTAINS {
		@Override
		public boolean test(final Object a, final Object b) {
			if (a instanceof final String s && b instanceof final String s2) {
				return s.contains(s2);
			}
			final Set<?> c1 = ObjKit.toList(a).collect(Collectors.toSet());
			return ObjKit.toList(b).allMatch(c1::contains);
		}
	},
	CONTAINED {
		@Override
		public boolean test(final Object a, final Object b) {
			return CONTAINS.test(b, a);
		}
	};

	@Override
	public String description() {
		return LabelStyle.UNDERLINE_SEPARATED.convertTo(LabelStyle.SPACE_SEPARATED, name().toLowerCase());
	}
}
