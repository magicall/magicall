/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.util.Objects;

public class NamedDto implements Named {
	public String name;

	public NamedDto() {
	}

	public NamedDto(final String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public NamedDto renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public String toString() {
		return name();
	}

	@Override
	public int hashCode() {
		return Objects.hash(name());
	}

	@Override
	public boolean equals(final Object other) {
		return other instanceof final Named o && name().equals(o.name());
	}
}
