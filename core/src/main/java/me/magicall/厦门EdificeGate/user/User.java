/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.厦门EdificeGate.user;

import me.magicall.Entity;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.Named;

public interface User extends Entity<String, User>, Named {
	String RESOURCE_NAME = "用户";

	@Override
	default User renameTo(final String newName) {
		return (User) Named.super.renameTo(newName);
	}

	/**
	 * 备注。
	 *
	 * @return
	 */
	String note();

	User note(String note);

	@Override
	default User snapshot() {
		final var rt = new UserDto();
		rt.id = id();
		rt.name = name();
		rt.note = note();
		return rt;
	}

	//==============================================

	static String str(final User one) {
		return StrKit.format("{0}#{1}''{2}''({3})", RESOURCE_NAME, one.id(), one.name(), one.note());
	}

	static int hash(final User one) {
		return Entity.hash(one);
	}

	static boolean eq(final User one, final Object other) {
		return other instanceof User && Entity.eq(User.class, one, other);
	}
}