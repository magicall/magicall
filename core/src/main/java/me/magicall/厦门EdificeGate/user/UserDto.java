/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.厦门EdificeGate.user;

public class UserDto implements User {
	public String id;
	public String name;
	public String note;

	@Override
	public String id() {
		return id;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public UserDto renameTo(final String newName) {
		name = newName;
		return this;
	}

	@Override
	public String note() {
		return note;
	}

	@Override
	public UserDto note(final String note) {
		this.note = note;
		return this;
	}

	@Override
	public boolean equals(final Object o) {
		return User.eq(this, o);
	}

	@Override
	public int hashCode() {
		return User.hash(this);
	}

	@Override
	public String toString() {
		return User.str(this);
	}
}
