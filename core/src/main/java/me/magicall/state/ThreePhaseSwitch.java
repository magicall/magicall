/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.state;

/**
 * 三相开关。
 * 可以用原生的布尔类型实现两相状态，但有时两相不够用。
 * 使用{@link ThreePhaseState}时，需要自行用切换后的新状态对象覆盖原状态对象，而使用本类对象则不需要。
 *
 * @author Liang Wenjian.
 */
public class ThreePhaseSwitch {
	private ThreePhaseState state;

	public ThreePhaseSwitch() {
		this(ThreePhaseState.UNSET);
	}

	public ThreePhaseSwitch(final ThreePhaseState state) {
		this.state = state;
	}

	public void turnOn() {
		state = state.turnOn();
	}

	public void turnOff() {
		state = state.turnOff();
	}

	public void unset() {
		state = state.unset();
	}

	public boolean isOn() {
		return state.isOn();
	}

	public boolean isOff() {
		return state.isOff();
	}

	public boolean isUnset() {
		return state.isUnset();
	}
}
