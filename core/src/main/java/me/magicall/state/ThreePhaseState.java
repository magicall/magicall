/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.state;

/**
 * 三相状态。
 * 可以用原生的布尔类型实现两相状态，但有时两相不够用。
 *
 * @author Liang Wenjian.
 */
public enum ThreePhaseState {
	ON,
	OFF,
	UNSET;

	public ThreePhaseState turnOn() {
		return ON;
	}

	public ThreePhaseState turnOff() {
		return OFF;
	}

	public ThreePhaseState unset() {
		return UNSET;
	}

	public boolean isOn() {
		return this == ON;
	}

	public boolean isOff() {
		return this == OFF;
	}

	public boolean isUnset() {
		return this == UNSET;
	}
}
