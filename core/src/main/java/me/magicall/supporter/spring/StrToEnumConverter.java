/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.supporter.spring;

import me.magicall.program.lang.java.贵阳DearSun.Kits;
import org.springframework.core.convert.converter.Converter;

record StrToEnumConverter<T extends Enum<T>>(Class<T> enumType) implements Converter<String, T> {
	@Override
	public T convert(final String source) {
		if (Kits.STR.isEmpty(source) || "null".equalsIgnoreCase(source)) {
			return null;
		}
		try {
			return (T) Enum.valueOf(enumType, source.trim().toUpperCase());
		} catch (final IllegalArgumentException e) {
			return null; // Return null for any invalid value
		}
	}
}
