/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.supporter.spring;

import com.google.common.collect.BoundType;
import com.google.common.collect.Range;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import org.springframework.core.convert.converter.Converter;

public abstract class AbsStrToRangeConverter<T extends Comparable<T>> implements Converter<String, Range<T>> {
	private static final char LEFT_OPEN_CHAR = '(';
	private static final char RIGHT_OPEN_CHAR = ')';
	private static final char RIGHT_CLOSE_CHAR = ']';
	private static final char LEFT_CLOSE_CHAR = '[';

	@Override
	public Range<T> convert(final String source) {
		final var indexOfComma = source.indexOf(',');
		if (indexOfComma < 0) {
			throw ex(source);
		}
		final var lowerStr = source.substring(1, indexOfComma).strip();
		final var lower = parse(lowerStr);
		final var upperStr = source.substring(indexOfComma + 1, source.length() - 1).strip();
		final var upper = parse(upperStr);
		final var leftBoundType = checkBoundType(source.charAt(0), LEFT_OPEN_CHAR, LEFT_CLOSE_CHAR, source);
		final var rightBoundType = checkBoundType(source.charAt(source.length() - 1), RIGHT_OPEN_CHAR, RIGHT_CLOSE_CHAR,
				source);
		return Range.range(lower, leftBoundType, upper, rightBoundType);
	}

	protected abstract T parse(final String s);

	private static BoundType checkBoundType(final char c, final char openChar, final char closeChar,
																					final String source) {
		if (c == openChar) {
			return BoundType.OPEN;
		} else if (c == closeChar) {
			return BoundType.CLOSED;
		} else {
			throw ex(source);
		}
	}

	protected static WrongArgException ex(final String source) {
		return new WrongArgException("?", source, "[a,b] [a,b) (a,b] (a,b)");
	}
}
