/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.computer;

public enum SizeUnit {
	BYTE(1),
	KB(BYTE),
	MB(KB),
	GB(MB),
	TB(GB),
	PB(TB),
	;

	private static final int SCALE = 1024;

	public final long size;

	SizeUnit(final int size) {
		this.size = size;
	}

	SizeUnit(final SizeUnit pre) {
		size = SCALE * pre.size;
	}

	public long toBytes(final int num) {
		return size * num;
	}

	public long toBytes() {
		return toBytes(1);
	}

	public long toOtherSize(final int num, final SizeUnit sizeUnit) {
		return size / sizeUnit.size * num;
	}
}
