/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.computer;

import me.magicall.program.lang.java.JavaConst;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public enum FileType implements FilenameFilter, FileFilter {
	JPG(DataType.IMG, "jpeg"),
	GIF(DataType.IMG),
	BMP(DataType.IMG),
	ICO(DataType.IMG),
	PNG(DataType.IMG),

	TXT(DataType.TEXT),
	XML(DataType.TEXT),
	HTML(DataType.TEXT, "htm"),
	PROPERTIES(DataType.TEXT),
	JSP(DataType.TEXT),
	JS(DataType.TEXT),
	ASP(DataType.TEXT),
	PHP(DataType.TEXT),
	PY(DataType.TEXT),

	WAV(DataType.AUDIO),
	MP3(DataType.AUDIO),

	MP4(DataType.VIDEO),
	RMVB(DataType.VIDEO),

	BIN(DataType.BINARY),
	;

	private static final Map<String, FileType> SUFFIX_MAP = new HashMap<>();

	static {
		Arrays.stream(values()).forEach(fileType -> {
			Arrays.stream(fileType.suffixes).forEach(s -> SUFFIX_MAP.put(s, fileType));
		});
	}

	private final DataType dataType;
	private final String[] suffixes;

	FileType(final DataType dataType, final String... otherSuffixes) {
		this.dataType = dataType;
		suffixes = new String[1 + otherSuffixes.length];
		suffixes[0] = '.' + name().toLowerCase();
		IntStream.range(0, otherSuffixes.length).forEach(i -> suffixes[i + 1] = '.' + otherSuffixes[i].toLowerCase());
	}

	public static String getNameWithoutSuffix(final File file) {
		final var fileName = file.getName();
		final var lastIndex = fileName.lastIndexOf('.');
		if (lastIndex == JavaConst.NOT_FOUND_INDEX) {
			return fileName;
		} else {
			return fileName.substring(0, lastIndex);
		}
	}

	public static FileType getByFileName(final String fileName) {
		if (fileName.indexOf('.') >= 0) {
			return SUFFIX_MAP.get(fileName.substring(fileName.lastIndexOf('.')).toLowerCase());
		}
		return null;
	}

	public DataType getDataType() {
		return dataType;
	}

	public String[] getSuffixes() {
		return suffixes.clone();
	}

	@Override
	public boolean accept(final File dir, final String name) {
		return Arrays.stream(suffixes).anyMatch(name::endsWith);
	}

	@Override
	public boolean accept(final File file) {
		return accept(null, file.getName());
	}
}
