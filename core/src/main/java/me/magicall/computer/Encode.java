/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.computer;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public enum Encode {

	UTF8,
	GBK,
	GB2312,
	ISO8859_1,
	ASCII;

	private final String name;

	Encode() {
		name = name();
	}

	public String getName() {
		return name;
	}

	public Charset charset() {
		return Charset.forName(name);
	}

	public String toString(final byte[] bytes) {
		try {
			return new String(bytes, name);
		} catch (final UnsupportedEncodingException e) {
			throw new UnknownException(e);
		}
	}
}
