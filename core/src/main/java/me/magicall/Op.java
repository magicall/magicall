/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

/**
 * 操作。
 */
public enum Op implements Named {
	/**
	 * 元接触。获取目标的元信息（比如存在性、可操作性等）的操作。
	 */
	META,
	/**
	 * 获取目标。获取目标的操作。
	 */
	GET,
	/**
	 * 增加新目标。
	 */
	ADD,
	/**
	 * 修改目标。改变目标的任何状态的操作。
	 */
	UPDATE,
	/**
	 * 清除目标。“清除”的含义是改变目标的存在性，具体实现可以是物理删除、逻辑删除或其他手段，所以不使用“DELETE”一词。
	 */
	CLEAR,
	;
}
