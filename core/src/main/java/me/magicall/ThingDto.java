/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall;

import java.io.Serial;
import java.io.Serializable;

public class ThingDto implements Thing, Serializable {
	@Serial
	private static final long serialVersionUID = -533161370753523575L;

	public String type;
	public String idInType;

	public ThingDto() {
	}

	public ThingDto(final String type, final Object idInType) {
		this.type = type;
		this.idInType = String.valueOf(idInType);
	}

	@Override
	public String type() {
		return type;
	}

	@Override
	public String idInType() {
		return idInType;
	}

	@Override
	public String toString() {
		return Thing.str(this);
	}

	@Override
	public boolean equals(final Object o) {
		return Thing.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Thing.hash(this);
	}
}
