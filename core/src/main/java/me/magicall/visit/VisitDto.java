/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.visit;

import me.magicall.Op;
import me.magicall.Thing;

import java.time.Instant;

public class VisitDto implements Visit {
	public String id;
	public String userId;
	public Op op;
	public Thing resource;
	public String result;
	public Instant createTime;
	public String ref;

	@Override
	public String id() {
		return id;
	}

	@Override
	public String userId() {
		return userId;
	}

	@Override
	public Thing resource() {
		return resource;
	}

	@Override
	public Op op() {
		return op;
	}

	@Override
	public String result() {
		return result;
	}

	@Override
	public Instant createTime() {
		return createTime;
	}

	@Override
	public String getRef() {
		return ref;
	}

	@Override
	public void setResult(final String result) {
		this.result = result;
	}

	@Override
	public boolean equals(final Object o) {
		return Visit.eq(this, o);
	}

	@Override
	public int hashCode() {
		return Visit.hash(this);
	}

	@Override
	public String toString() {
		return Visit.str(this);
	}
}
