/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.visit;

import me.magicall.Entity;
import me.magicall.Op;
import me.magicall.Thing;
import me.magicall.lifecycle.Creation;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.Objects;

public interface Visit extends Creation, Entity<String, Visit> {
	String userId();

	Thing resource();

	Op op();

	String result();

	default void setResult(final String result) {
		throw new OpNotAllowedException(Thing.of(Visit.class.getSimpleName(), "?"), "setResult", Thing.of("str", result));
	}

	/**
	 * 从哪里来的操作。
	 *
	 * @return
	 */
	String getRef();

	//================================================

	static String str(final Visit visit) {
		return StrKit.format("{0} {1}-{2}→{3}({4})", visit.createTime(), visit.userId(), visit.op(), visit.resource(),
				visit.getRef());
	}

	static int hash(final Visit visit) {
		return Objects.hash(visit.createTime(), visit.getRef(), visit.userId(), visit.op(), visit.resource());
	}

	static boolean eq(final Visit visit, final Object o) {
		if (o instanceof final Visit other) {
			return Objects.equals(visit.createTime(), other.createTime())//
					&& Objects.equals(visit.getRef(), other.getRef())//
					&& Objects.equals(visit.userId(), other.userId())//
					&& visit.op() == other.op()//
					&& Objects.equals(visit.resource(), other.resource());
		}
		return false;
	}
}
