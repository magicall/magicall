/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event.impl.spring;

import me.magicall.event.Bulletin;
import me.magicall.event.Notice;
import me.magicall.event.Plan;
import me.magicall.event.PlanPublisher;
import org.springframework.context.ApplicationEventPublisher;

public class SpringEventPublisherAdapter implements PlanPublisher, Bulletin {
	private final ApplicationEventPublisher springEventPublisher;

	public SpringEventPublisherAdapter(final ApplicationEventPublisher springEventPublisher) {
		this.springEventPublisher = springEventPublisher;
	}

	@Override
	public void publish(final Plan<?, ?> plan) {
		if (springEventPublisher != null) {
			springEventPublisher.publishEvent(plan);
		}
	}

	@Override
	public void announce(final Notice<?, ?> notice) {
		if (springEventPublisher != null) {
			springEventPublisher.publishEvent(notice);
		}
	}
}
