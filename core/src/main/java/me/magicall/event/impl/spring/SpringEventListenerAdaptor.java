/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event.impl.spring;

import com.google.common.collect.Lists;
import me.magicall.event.Notice;
import me.magicall.event.NoticeWatcher;
import me.magicall.event.NoticeWatcherRegistry;
import me.magicall.event.Plan;
import me.magicall.event.PlanAdviser;
import me.magicall.event.PlanAdviserRegistry;
import org.springframework.context.event.EventListener;

import java.util.List;
import java.util.Objects;

public class SpringEventListenerAdaptor implements PlanAdviserRegistry, NoticeWatcherRegistry {

	private final List<PlanAdviser<?, ?>> advisers = Lists.newLinkedList();
	private final List<NoticeWatcher<?, ?>> listeners = Lists.newLinkedList();

	@Override
	public void reg(final PlanAdviser<?, ?> planAdviser) {
		advisers.add(planAdviser);
	}

	@Override
	public void remove(final PlanAdviser<?, ?> planAdviser) {
		advisers.remove(planAdviser);
	}

	@EventListener(Plan.class)
	public void receive(final Plan<?, ?> plan) {
		advisers.stream().filter(planAdviser -> planAdviser.isInterestedIn(plan))//
				.map(planAdviser -> planAdviser.adviseBefore((Plan) plan))//
				.filter(Objects::nonNull)//
				.forEach(plan::receive);
	}

	@Override
	public void reg(final NoticeWatcher<?, ?> noticeWatcher) {
		listeners.add(noticeWatcher);
	}

	@Override
	public void remove(final NoticeWatcher<?, ?> noticeWatcher) {
		listeners.remove(noticeWatcher);
	}

	@EventListener(Notice.class)
	public void receive(final Notice<?, ?> notice) {
		listeners.stream().filter(listener -> listener.isInterestedIn(notice))
				.forEach(listener -> listener.doAfter((Notice) notice));
	}
}
