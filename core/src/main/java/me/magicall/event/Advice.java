/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import me.magicall.Op;

import java.time.Instant;

/**
 * 建议。
 * 只有时间、目标、新值、操作可改。
 *
 * @param <_TargetOwner> 计划针对的对象的类型
 * @param <_TargetValType> 计划针对的对象的类型
 */
public interface Advice<_TargetOwner, _TargetValType> extends Plan<_TargetOwner, _TargetValType> {

	/**
	 * 获取本建议针对的计划。
	 *
	 * @return 源计划。
	 */
	Plan<_TargetOwner, _TargetValType> getRawPlan();

	/**
	 * 提出本建议的原因。
	 *
	 * @return 原因。
	 */
	@Override
	Notice<?, ?> getReason();

	/**
	 * 建议的时间，默认同原计划。
	 *
	 * @return 建议的时间。
	 */
	@Override
	default Instant getScheduleTime() {
		return getRawPlan().getScheduleTime();
	}

	/**
	 * 建议的操作，默认同原计划。
	 *
	 * @return 建议的操作。
	 */
	@Override
	default Op getOp() {
		return getRawPlan().getOp();
	}

	/**
	 * 新值，默认同原计划。
	 *
	 * @return 建议的新值。
	 */
	@Override
	default _TargetValType getNewVal() {
		return getRawPlan().getNewVal();
	}

	/**
	 * 建议的目标，默认同原计划。
	 *
	 * @return 建议的目标。
	 */
	@Override
	default Target<_TargetOwner, _TargetValType> getTarget() {
		return getRawPlan().getTarget();
	}
}
