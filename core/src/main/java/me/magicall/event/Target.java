/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import me.magicall.Named;
import me.magicall.Owned;

/**
 * 目标。
 * 一个目标是可以作为一个行为的宾语的东西，可以是某个对象、某组对象、某属性的值等。
 *
 * @param <_Owner> 目标的主人。当操作的是属性时，“主人”才有用。
 * @param <_ValType> 目标的值类型。
 */
public interface Target<_Owner, _ValType> extends Named, Owned<_Owner> {

	/**
	 * 当目标是属性时，“主人”即拥有该属性值的对象。
	 *
	 * @return 主人
	 */
	@Override
	_Owner owner();

	/**
	 * 当目标是属性时，名字才有用，它表示属性名字。
	 *
	 * @return 属性名
	 */
	@Override
	String name();

	/**
	 * 目标的值。
	 * 若目标针对某个对象（如创建、删除），则目标的值即该对象；若针对某组对象，则目标的值是一个集合；若针对某属性，则值是该属性的值。
	 *
	 * @return 值
	 */
	_ValType getVal();
}
