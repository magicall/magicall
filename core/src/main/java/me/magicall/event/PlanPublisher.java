/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

/**
 * 计划发布器。
 *
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface PlanPublisher {

	void publish(Plan<?, ?> plan);

	PlanPublisher DO_NOTHING = plan -> {
	};

	static void publishIfCan(final Plan<?, ?> plan, final PlanPublisher planPublisher) {
		if (planPublisher != null) {
			planPublisher.publish(plan);
		}
	}
}
