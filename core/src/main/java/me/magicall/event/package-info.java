/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * 一般用法：
 * 领域对象、领域服务等对象在做某事之前，可发布一个计划（{@link me.magicall.event.Plan}），声明将要执行该计划。
 * 计划是可以改变的。
 * 计划顾问（{@link me.magicall.event.PlanAdviser}）可根据需要，对该计划提出修改建议。达到“前置拦截”的效果。
 * 领域对象、领域服务等对象根据计划做该事情，可听取计划顾问的建议，也可丢弃。
 * 做完事情之后，可发布一个事件（{@link me.magicall.event.Notice}），声明已发生该事。
 * 事件是不可改变的。
 * 事件监听器（{@link me.magicall.event.NoticeWatcher}）根据需要监听感兴趣的事件，执行后续事件。
 * 后续事件不应影响前置事件。
 */
package me.magicall.event;