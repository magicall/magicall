/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

public class SimpleTarget<_Owner, _ValType> implements Target<_Owner, _ValType> {

	private final _Owner owner;
	private final String name;
	private final _ValType val;

	public SimpleTarget(final _Owner owner, final String name, final _ValType val) {
		this.owner = owner;
		this.name = name;
		this.val = val;
	}

	@Override
	public _ValType getVal() {
		return val;
	}

	@Override
	public _Owner owner() {
		return owner;
	}

	@Override
	public String name() {
		return name;
	}

	public static <_Owner, _ValType> SimpleTarget<_Owner, _ValType> toOneObj(final _Owner owner, final _ValType obj) {
		return new SimpleTarget<>(owner, obj.getClass().getSimpleName(), obj);
	}

	public static <_Owner, _ValType> SimpleTarget<_Owner, _ValType> toOneObj(final _Owner owner,
																																					 final Class<_ValType> clazz) {
		return new SimpleTarget<>(owner, clazz.getSimpleName(), null);
	}

	public static <_Owner, _ValType> SimpleTarget<_Owner, _ValType> toProp(final _Owner owner, final String propName,
																																				 final _ValType obj) {
		return new SimpleTarget<>(owner, propName, obj);
	}
}
