/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import me.magicall.Op;

import java.time.Instant;

public interface PlanRealizedNotice<_TargetOwner, _TargetValType> extends Notice<_TargetOwner, _TargetValType> {
	Plan<_TargetOwner, _TargetValType> getPlan();

	@Override
	default Instant getHappenedTime() {
		return getPlan().getScheduleTime();
	}

	@Override
	default Op getOp() {
		return getPlan().getOp();
	}

	@Override
	default Object owner() {
		return getPlan().owner();
	}

	@Override
	default Target<_TargetOwner, _TargetValType> getTarget() {
		return getPlan().getTarget();
	}

	//==============================因果链

	@Override
	default Notice<?, ?> getReason() {
		return getPlan().getReason();
	}

	@Override
	default boolean isTriggeredBy(final Notice<?, ?> other) {
		return getPlan().isTriggeredBy(other);
	}

	@Override
	default boolean isEffectOf(final Notice<?, ?> other) {
		return getPlan().isEffectOf(other);
	}
}
