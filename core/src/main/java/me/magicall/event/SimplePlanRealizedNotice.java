/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

public class SimplePlanRealizedNotice<_TargetOwner, _TargetValType>
		implements PlanRealizedNotice<_TargetOwner, _TargetValType> {

	private final Plan<_TargetOwner, _TargetValType> plan;
	private final Object result;

	public SimplePlanRealizedNotice(final Plan<_TargetOwner, _TargetValType> plan) {
		this(plan, plan.getNewVal());
	}

	public SimplePlanRealizedNotice(final Plan<_TargetOwner, _TargetValType> plan, final Object result) {
		this.plan = plan;
		this.result = result;
	}

	@Override
	public Plan<_TargetOwner, _TargetValType> getPlan() {
		return plan;
	}

	@Override
	public Object getResult() {
		return result;
	}
}
