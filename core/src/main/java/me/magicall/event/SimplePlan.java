/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import com.google.common.collect.Lists;
import me.magicall.Op;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.time.Instant;
import java.util.List;
import java.util.function.Predicate;

/**
 * 计划的简单实现类。
 *
 * @param <_TargetOwner> 计划针对的对象的主人的类型。
 * @param <_TargetValType> 计划针对的对象的类型。
 */
public class SimplePlan<_TargetOwner, _TargetValType> implements Plan<_TargetOwner, _TargetValType> {

	protected final Instant instant;
	protected final Object owner;
	protected final Op op;
	protected final Target<_TargetOwner, _TargetValType> target;
	protected final _TargetValType newVal;
	protected final Predicate<_TargetValType> condition;
	protected final Notice<?, ?> reason;
	protected final List<Advice<_TargetOwner, _TargetValType>> advices = Lists.newArrayList();

	protected SimplePlan(final Instant instant, final Object owner, final Op op,
											 final Target<_TargetOwner, _TargetValType> target, final _TargetValType newVal,
											 final Predicate<_TargetValType> condition, final Notice<?, ?> reason) {
		this.instant = instant;
		this.owner = owner;
		this.op = op;
		this.target = target;
		this.newVal = newVal;
		this.reason = reason;
		this.condition = condition;
	}

	@Override
	public Plan<_TargetOwner, _TargetValType> receive(final Advice<_TargetOwner, _TargetValType> advice) {
		final var now = Instant.now();
		if (now.isAfter(instant)) {
			throw new WrongStatusException("scheduleTime", instant, ">" + now);
		}
		advices.add(advice);
		return this;
	}

	@Override
	public Instant getScheduleTime() {
		return instant;
	}

	@Override
	public Object owner() {
		return owner;
	}

	@Override
	public Op getOp() {
		return op;
	}

	@Override
	public Target<_TargetOwner, _TargetValType> getTarget() {
		return target;
	}

	@Override
	public Notice<?, ?> getReason() {
		return reason;
	}

	@Override
	public List<Advice<_TargetOwner, _TargetValType>> getAdvices() {
		return advices;
	}

	@Override
	public _TargetValType getNewVal() {
		return newVal;
	}
//	@Override
//	public Predicate<_TargetValType> getCondition() {
//		return condition;
//	}
}
