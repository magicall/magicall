/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import me.magicall.Op;

import java.time.Instant;

public class SimpleNotice<_TargetOwner, _TargetValType> implements Notice<_TargetOwner, _TargetValType> {

	private final Object owner;
	private final Instant happenedTime;
	private final Notice<?, ?> reason;
	private final Op op;
	private final Target<_TargetOwner, _TargetValType> target;
	private final Object result;

	protected SimpleNotice(final Object owner, final Instant happenedTime, final Notice<?, ?> reason, final Op op,
												 final Target<_TargetOwner, _TargetValType> target, final Object result) {
		this.owner = owner;
		this.happenedTime = happenedTime;
		this.reason = reason;
		this.op = op;
		this.target = target;
		this.result = result;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public Instant getHappenedTime() {
		return happenedTime;
	}

	@Override
	public Op getOp() {
		return op;
	}

	@Override
	public Object owner() {
		return owner;
	}

	@Override
	public Target<_TargetOwner, _TargetValType> getTarget() {
		return target;
	}

	@Override
	public Notice<?, ?> getReason() {
		return reason;
	}

	public static class Builder<_TargetOwner, _TargetValType> {
		private final Object owner;
		private Instant happenedTime;
		private Notice<?, ?> reason;
		private Op op;
		private Target<_TargetOwner, _TargetValType> target;
		private Object result;

		protected Builder(final Object owner) {
			this.owner = owner;
		}

		/**
		 * 本事件发生的时间。
		 *
		 * @param happenedTime 时间。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> when(final Instant happenedTime) {
			this.happenedTime = happenedTime;
			return this;
		}

		/**
		 * 设置本事件针对的目标的操作。
		 *
		 * @param op 操作。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> done(final Op op) {
			this.op = op;
			return this;
		}

		/**
		 * 设置本事件针对的目标。
		 *
		 * @param target 目标。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> against(final Target<_TargetOwner, _TargetValType> target) {
			this.target = target;
			return this;
		}

		/**
		 * 设置本事件的原因。
		 *
		 * @param reason 原因。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> because(final Notice<?, ?> reason) {
			this.reason = reason;
			return this;
		}

		/**
		 * 设置本事件的结果。
		 *
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> got(final Object result) {
			this.result = result;
			return this;
		}

		public Notice<_TargetOwner, _TargetValType> build() {
			return new SimpleNotice<>(owner, happenedTime, reason, op, target, result);
		}
	}
}
