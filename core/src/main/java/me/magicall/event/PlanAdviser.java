/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

/**
 * 计划顾问。
 *
 * @param <_TargetOwner> 计划针对的对象的主人的类型。
 * @param <_TargetValType> 计划针对的对象的类型。
 */
public interface PlanAdviser<_TargetOwner, _TargetValType> {

	boolean isInterestedIn(final Plan<?, ?> plan);

	Advice<_TargetOwner, _TargetValType> adviseBefore(final Plan<_TargetOwner, _TargetValType> plan);
}
