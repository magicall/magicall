/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import me.magicall.Op;
import me.magicall.Owned;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 * 计划。
 * 一个计划是对“什么时候，谁，做某事”的一种描述。
 * 在执行该行为之前，发布计划，感兴趣的计划顾问（{@link PlanAdviser}）可以提出建议。行为的执行者可自行酌情听取或不听取建议。
 * 从而成为一种前置拦截机制，为执行者提供了一定的扩展性。
 * <pre>
 * 例1：
 * BookServices想要获取所有Book的列表，在获取之前先发布一个“访问计划（{@link Plan}”）。
 * 由于通常的“获取”是指从数据库中查询。如果想要增加缓存，可以新建一个计划顾问，对访问计划提出建议。建议的目标值放入已从缓存中加载到的数据。
 * </pre>
 * <pre>
 * 例2：
 * UserServices想要修改指定User的密码属性，在修改前先发布一个“修改计划”。
 * 如果想要修改拦截-对密码进行加密，则可以新建一个计划顾问，对修改计划提出建议，把加密后的密码设为建议（{@link Advice}）的目标值({@link Target#getVal()})。
 * </pre>
 * <pre>
 * 	scheduleTime: 预定的时间。
 * 	owner: 计划的主人。
 * 	op: 打算执行的操作，即行为。
 * 	target: 打算执行的操作的针对目标，即行为的宾语。
 * 		owner: 目标的主人。目标可视为主人的某种属性。
 * 		name: 目标在主人处的属性名。
 * 		val: 目标的当前值。
 * 	condition: 对目标的筛选条件。
 * 	newVal: 打算赋予目标的新值。
 * </pre>
 *
 * @param <_TargetOwner> 计划针对的对象的主人的类型。
 * @param <_TargetValType> 计划针对的对象的类型。
 */
public interface Plan<_TargetOwner, _TargetValType> extends Owned<Object>, Comparable<Plan<?, ?>> {

	Comparator<Plan<?, ?>> SCHEDULE_TIME_COMPARATOR = Comparator.nullsLast(Comparator.comparing(Plan::getScheduleTime));

	/**
	 * 计划执行的时间，通常是未来。为null表示无所谓执行时间，可能会在任何时候执行，通常是尽快，或在优先级排序时排到最后（既然无所谓）。
	 *
	 * @return 执行时间。
	 */
	Instant getScheduleTime();

	@Override
	default int compareTo(final Plan<?, ?> o) {
		return SCHEDULE_TIME_COMPARATOR.compare(this, o);
	}

	/**
	 * 计划的主人，是提出本计划的东西，即本{@link Plan}对象的创建者。通常是某个领域对象或领域服务。
	 *
	 * @return 计划的主人。不会为null。
	 */
	@Override
	Object owner();

	/**
	 * 本计划的操作类型。
	 *
	 * @return 本计划的操作类型，不会为null。
	 */
	Op getOp();

	/**
	 * 计划针对的目标。可以是一个对象、一组对象、对象的一个属性。
	 * 若{@link #getOp()}为{@link Op#GET}或{@link Op#META}，则目标或目标的值可能为null，表示目标尚未确定。
	 *
	 * @return 计划针对的目标。
	 */
	Target<_TargetOwner, _TargetValType> getTarget();

//	/**
//	 * 本计划针对的操作对象的限制条件，只有符合条件的才会操作。
//	 *
//	 * @return
//	 */
//	Predicate<_TargetValType> getCondition();

	/**
	 * 新值。对于更新操作（{@link Op#UPDATE}）有用。
	 *
	 * @return 计划中要使用的新值。若非更新操作，则返回null。
	 */
	_TargetValType getNewVal();

	//=========================事件链

	/**
	 * 触发本事件的原因事件。
	 *
	 * @return 原因。
	 */
	Notice<?, ?> getReason();

	/**
	 * 获取根源原因。
	 *
	 * @return 根源原因。
	 */
	default Notice<?, ?> getRootReason() {
		Notice<?, ?> e = null;
		for (var reason = getReason(); reason != null; reason = reason.getReason()) {
			e = reason;
		}
		return e;
	}

	/**
	 * 是否由指定事件直接触发。
	 *
	 * @param notice 另一事件。
	 * @return 是否由指定事件直接触发。
	 */
	default boolean isTriggeredBy(final Notice<?, ?> notice) {
		return Objects.equals(notice, getReason());
	}

	/**
	 * 是否指定事件的效果之一。“效果”指本事件是由另一事件直接或间接触发。
	 *
	 * @param notice 另一事件。
	 * @return 是否指定事件的效果之一。
	 */
	default boolean isEffectOf(final Notice<?, ?> notice) {
		final var reason = getReason();
		return reason != null && notice.isReasonOf(reason);
	}

	//=========================意见和建议

	/**
	 * 收到建议。
	 * 若此时已过了事件的计划执行时间，则抛出{@link WrongStatusException}。
	 *
	 * @param advice 建议。
	 */
	Plan<_TargetOwner, _TargetValType> receive(Advice<_TargetOwner, _TargetValType> advice);

	/**
	 * 获取建议列表。
	 *
	 * @return
	 */
	List<Advice<_TargetOwner, _TargetValType>> getAdvices();

	/**
	 * 是否收到过建议。
	 *
	 * @return 是否收到过建议。
	 */
	default boolean hasAdvices() {
		return !getAdvices().isEmpty();
	}

	/**
	 * 收到的第一个建议
	 *
	 * @return 第一个建议。
	 */
	default Advice<_TargetOwner, _TargetValType> getFirstAdvice() {
		return hasAdvices() ? getAdvices().get(0) : null;
	}

	/**
	 * 收到的最后一个建议
	 *
	 * @return 最后一个建议。
	 */
	default Advice<_TargetOwner, _TargetValType> getLastAdvice() {
		if (hasAdvices()) {
			final var advices = getAdvices();
			return advices.get(advices.size() - 1);
		}
		return null;
	}

	/**
	 * 获取指定计划顾问对本计划的建议。
	 *
	 * @param adviser 计划顾问。
	 * @return 建议
	 */
	default Advice<_TargetOwner, _TargetValType> getAdviceFrom(final PlanAdviser<_TargetOwner, _TargetValType> adviser) {
		final var advices = getAdvices();
		return IntStream.iterate(advices.size() - 1, i -> i >= 0, i -> i - 1).mapToObj(advices::get)
				.filter(advice -> adviser.equals(advice.owner()))//
				.findFirst().orElse(null);
	}

	static <_TargetOwner, _TargetValType> Builder<_TargetOwner, _TargetValType> from(final Object owner) {
		return new Builder<>(owner);
	}

	class Builder<_TargetOwner, _TargetValType> {
		private Instant instant;
		private final Object owner;
		private Op op;
		private Target<_TargetOwner, _TargetValType> target;
		private Notice<?, ?> reason;
		private _TargetValType newVal;
		private Predicate<_TargetValType> condition;

		public Builder(final Object owner) {
			this.owner = owner;
		}

		/**
		 * 设定本计划执行的时间。
		 *
		 * @param instant 时间
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> scheduleAt(final Instant instant) {
			this.instant = instant;
			return this;
		}

		public Builder<_TargetOwner, _TargetValType> rightNow() {
			return scheduleAt(Instant.now());
		}

		/**
		 * 设置本计划针对的目标的操作。
		 *
		 * @param op 操作
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> goingTo(final Op op) {
			this.op = op;
			return this;
		}

		/**
		 * 设置本计划针对的目标。
		 *
		 * @param target 目标。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> against(final Target<_TargetOwner, _TargetValType> target) {
			this.target = target;
			return this;
		}

		/**
		 * 设置本计划的原因。
		 *
		 * @param reason 原因。
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> because(final Notice<?, ?> reason) {
			this.reason = reason;
			return this;
		}

		/**
		 * 设置本计划针对的目标的新值。
		 *
		 * @param newVal 新值
		 * @return 本Builder
		 */
		public Builder<_TargetOwner, _TargetValType> letValBe(final _TargetValType newVal) {
			this.newVal = newVal;
			return this;
		}

		public Plan<_TargetOwner, _TargetValType> build() {
			return new SimplePlan<>(instant, owner, op, target, newVal, condition, reason);
		}
	}//Builder
}
