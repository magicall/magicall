/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

import java.util.List;

public class SimpleAdvice<_TargetOwner, _TargetValType> implements Advice<_TargetOwner, _TargetValType> {

	protected final Plan<_TargetOwner, _TargetValType> rawPlan;

	public SimpleAdvice(final Object owner, final Plan<_TargetOwner, _TargetValType> rawPlan) {
		this.rawPlan = rawPlan;
	}

	@Override
	public Plan<_TargetOwner, _TargetValType> getRawPlan() {
		return rawPlan;
	}

	@Override
	public Object owner() {
		return rawPlan.owner();
	}

	@Override
	public Notice<?, ?> getReason() {
		return rawPlan.getReason();
	}

	@Override
	public Plan<_TargetOwner, _TargetValType> receive(final Advice<_TargetOwner, _TargetValType> advice) {
		return rawPlan.receive(advice);
	}

	@Override
	public List<Advice<_TargetOwner, _TargetValType>> getAdvices() {
		return rawPlan.getAdvices();
	}
}
