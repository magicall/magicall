/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.event;

/**
 * 布告栏。用于发布通告。
 */
@FunctionalInterface
public interface Bulletin {
	void announce(Notice<?, ?> notice);

	Bulletin DO_NOTHING = event -> {
	};

	static void publishIfCan(final Notice<?, ?> notice, final Bulletin bulletin) {
		if (bulletin != null) {
			bulletin.announce(notice);
		}
	}
}
