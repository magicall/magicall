/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.meta;

import me.magicall.Thing;
import me.magicall.meta.Meta;

import java.time.Instant;

public interface MetaServices {

	/**
	 * 为新创建的事务创建元数据。
	 *
	 * @return
	 */
	default Meta create(final Thing thingOwner, final Thing createdThing) {
		return create(thingOwner, createdThing, Instant.now());
	}

	/**
	 * 为新创建的事务创建元数据。
	 *
	 * @return
	 */
	Meta create(Thing thingOwner, Thing createdThing, Instant createTime);

	Meta findMetaOf(Thing thing);
}
