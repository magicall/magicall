/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz;

import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;

/**
 * 具名且名字唯一的资源的服务。
 */
public interface UniqNameResourceCrudServices<T extends Named, _ValObj> extends CrudServices<T, _ValObj> {
	default T findByName(final String name) {
		return filter(e -> name.equals(e.name())).findFirst().orElse(null);
	}

	default T getByName(final String name) {
		final var one = findByName(name);
		if (one == null) {
			throw new NoSuchThingException(resourceName(), name);
		}
		return one;
	}

	default T dropByName(final String name) {
		final var one = findByName(name);
		return one == null ? null : drop(one);
	}
}
