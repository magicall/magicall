/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz;

import me.magicall.Entity;
import me.magicall.Op;
import me.magicall.event.Notice;
import me.magicall.event.Plan;
import me.magicall.event.SimplePlanRealizedNotice;
import me.magicall.event.SimpleTarget;

public interface SimpleEntityServicesSupport<_Id, _Entity extends Entity<_Id, _Entity>, _PersistentObj>
		extends EntityAsVoCrudServices<_Id, _Entity>, PersistentServicesSupport<_Entity, _Entity, _PersistentObj> {

	@Override
	default _Entity checkExist(final _Entity vo) {
		return dbToDomain(findDbEntityByDomain(vo));
	}

	@Override
	@SuppressWarnings("unchecked")
	default _Entity merge(final _Entity exist, final _Entity newValue) {
		return (_Entity) exist.accept(newValue);
	}

	/**
	 * 根据指定id获取一个领域对象。
	 * 获取前发布{@link Plan}，获取后发布{@link Notice}。
	 *
	 * @return 领域对象。
	 */
	@Override
	default _Entity find(final _Id id) {
		final _Entity raw = EntityAsVoCrudServices.super.find(id);
		final var plan = Plan.<Object, _Entity>from(this).rightNow().goingTo(Op.GET)
				.against(new SimpleTarget<>(this, resourceName(), raw)).build();
		publish(plan);
		final _Entity rt;
		if (plan.hasAdvices()) {
			rt = plan.getLastAdvice().getTarget().getVal();
		} else {
			rt = raw;
		}
		final var event = new SimplePlanRealizedNotice<>(plan, rt);
		publish(event);
		return rt;
	}
}
