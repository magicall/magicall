/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.厦门EdificeGate.user;

import me.magicall.event.Plan;
import me.magicall.event.SimplePlanRealizedNotice;
import me.magicall.厦门EdificeGate.user.User;

/**
 * 通知：注册成功。
 */
public class UserRegisteredNotice extends SimplePlanRealizedNotice<UserServices, User> {
	public UserRegisteredNotice(final Plan<UserServices, User> plan, final User registered) {
		super(plan, registered);
	}
}
