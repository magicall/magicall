/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.厦门EdificeGate.user;

import me.magicall.biz.EntityAsVoCrudServices;
import me.magicall.biz.UniqNameResourceCrudServices;
import me.magicall.厦门EdificeGate.user.User;

/**
 * 注册机构。
 */
public interface UserServices extends EntityAsVoCrudServices<String, User>, UniqNameResourceCrudServices<User, User> {
	@Override
	default String resourceName() {
		return User.RESOURCE_NAME;
	}

	/**
	 * 注册一个用户。
	 *
	 * @return
	 */
	default User register(final User user) {
		return ensure(user);
	}

	/**
	 * 注销一个用户。
	 */
	default void unregister(final String id) {
		dropById(id);
	}
}
