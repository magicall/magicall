/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz;

import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import me.magicall.Identified;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 一个简单的内存仓库。
 * 使用一个列表来存储对象。
 *
 * @author Liang Wenjian.
 */
public class SimpleMemoryRepo<K, V> {

	private final List<V> entities = Lists.newArrayList();
	private final Function<V, K> idFetcher;

	public SimpleMemoryRepo(final Function<V, K> idFetcher) {
		this.idFetcher = idFetcher;
	}

	public V get(final K id) {
		return entities.stream().filter(entity -> Objects.equals(idFetcher.apply(entity), id)).findFirst().orElse(null);
	}

	public boolean exists(final K id) {
		return get(id) != null;
	}

	public V save(final V entity) {
		return save0(entity);
	}

	protected <S extends V> S save0(final S entity) {
		if (!exists(idFetcher.apply(entity))) {
			entities.add(entity);
		}
		return entity;
	}

	public <S extends V> Iterable<S> save(final S... entities) {
		return save(Arrays.asList(entities));
	}

	public <S extends V> Iterable<S> save(final Iterable<S> entities) {
		return Streams.stream(entities).map(this::save0).collect(Collectors.toList());
	}

	public void del(final V entity) {
		entities.remove(entity);
	}

	public void del(final V... entities) {
		del(Arrays.asList(entities));
	}

	public void del(final Iterable<? extends V> entities) {
		for (final V entity : entities) {
			del(entity);
		}
	}

	public void delById(final K id) {
		final var one = get(id);
		if (one != null) {
			del(one);
		}
	}

	public List<V> all() {
		return entities;
	}

	public static <K, V extends Identified<K>> SimpleMemoryRepo<K, V> usingId() {
		return new SimpleMemoryRepo<>(Identified::id);
	}
}
