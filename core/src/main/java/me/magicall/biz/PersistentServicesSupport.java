/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz;

import me.magicall.Op;
import me.magicall.event.Bulletin;
import me.magicall.event.Notice;
import me.magicall.event.Plan;
import me.magicall.event.PlanPublisher;
import me.magicall.event.SimplePlanRealizedNotice;
import me.magicall.event.SimpleTarget;
import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 会保存的对象的服务支持类。
 * *ServicesSupport 类是带有实现方案的。
 */
public interface PersistentServicesSupport<T, _ValObj, _DbEntity> extends CrudServices<T, _ValObj> {

	Repo<_DbEntity> repo();

	T dbToDomain(_DbEntity dbEntity);

	_DbEntity voToDb(_ValObj vo);

	_DbEntity findDbEntityByDomain(T domainEntity);

	T checkExist(_ValObj vo);

	T merge(T exist, _ValObj newValue);

	//-------------------------------------streaming

	/**
	 * 流式获取所有指定类型的资源对象。
	 * 获取前发布{@link Plan}，获取后发布{@link Notice}。
	 *
	 * @return 资源对象流。
	 */
	@Override
	default Stream<T> streaming() {
		return repoAll().map(this::dbToDomain);
	}

	default Stream<_DbEntity> filterEntities(final Predicate<_DbEntity> filter) {
		return repoAll().filter(filter);
	}

	default Stream<_DbEntity> repoAll() {
		return repo().all().stream();
	}

	//-----------------------------------------ensure

	/**
	 * 创建前发布{@link Plan}，创建后发布{@link Notice}。
	 *
	 * @param one 值对象。
	 * @return 已确保存在的领域对象。
	 */
	@Override
	default T ensure(final _ValObj one) {
		if (one == null) {
			throw new NullValException(resourceName());
		}
		final T exist = checkExist(one);
		if (exist == null) {
			final Plan<?, _ValObj> plan = publish(buildCreatePlan(one));
			final _ValObj voToUse = plan == null || !plan.hasAdvices() ? one : checkCreatePlanAdvice(plan);
			final var rt = save(voToUse, one);
			publish(buildCreationNotice(plan, rt));
			return rt;
		} else {
			final Plan<?, T> plan = publish(buildMergePlan(exist, one));
			//todo:暂时不管advice			final _ValueObject voToUse = plan == null || !plan.hasAdvices() ? vo : plan.getLastAdvice().getNewVal();
			final var merged = merge(exist, one);
			publish(buildMergeNotice(plan, merged));
			return merged;
		}
	}

	default T save(final _ValObj voToUse, final _ValObj rawVo) {
		final var entity = voToDb(voToUse);
		return dbToDomain(repoSave(entity, rawVo));
	}

	default _DbEntity repoSave(final _DbEntity entity, final _ValObj vo) {
		return repo().store(Stream.of(entity)).get(0);
	}

	default Plan<?, _ValObj> buildCreatePlan(final _ValObj vo) {
		return Plan.<Object, _ValObj>from(this)//
				.rightNow()//
				.goingTo(Op.ADD)//
				.against(new SimpleTarget<>(this, resourceName(), vo)).build();
	}

	default _ValObj checkCreatePlanAdvice(final Plan<?, _ValObj> plan) {
		return plan.getLastAdvice().getTarget().getVal();
	}

	default Notice<?, ?> buildCreationNotice(final Plan<?, _ValObj> plan, final T created) {
		return new SimplePlanRealizedNotice<>(plan, created);
	}

	@SuppressWarnings("unchecked")
	default Plan<?, T> buildMergePlan(final T exist, final _ValObj vo) {
		return (Plan<?, T>) Plan.from(this)//
				.rightNow()//
				.goingTo(Op.UPDATE)//
				.against(new SimpleTarget<>(this, resourceName(), exist))//
				.letValBe(vo).build();
	}

	default SimplePlanRealizedNotice<?, ?> buildMergeNotice(final Plan<?, ?> mergePlan, final T merged) {
		return new SimplePlanRealizedNotice<>(mergePlan, merged);
	}

	//---------------------------drop

	/**
	 * 丢弃指定类型的指定资源对象集。
	 * 丢弃前发布{@link Plan}，丢弃后发布{@link Notice}。
	 *
	 * @param some 要删除的对象。
	 * @return 被删除的对象。
	 */
	@Override
	default Stream<T> drop(final Stream<T> some) {
		final var rawList = some.collect(Collectors.toList());
		final Plan<?, List<T>> plan = buildDropPlan(rawList);
		final List<T> toDel;
		if (plan == null) {
			toDel = rawList;
		} else {
			publish(plan);
			if (plan.hasAdvices()) {
				toDel = plan.getLastAdvice().getTarget().getVal();
			} else {
				toDel = rawList;
			}
		}
		repoDel(toDel);
		publishDroppedNotice(plan, toDel);
		return toDel.stream();
	}

	default Plan<?, List<T>> buildDropPlan(final List<T> raw) {
		return Plan.<Object, List<T>>from(this).rightNow()//
				.goingTo(Op.CLEAR).against(new SimpleTarget<>(this, resourceName(), raw)).build();
	}

	default void repoDel(final List<T> toDel) {
		repo().del(toDel.stream().map(this::findDbEntityByDomain).filter(Objects::nonNull));
	}

	default void publishDroppedNotice(final Plan<?, List<T>> plan, final List<T> deleted) {
		final Notice<?, ?> notice = buildDroppedNotice(plan, deleted);
		if (notice != null) {
			publish(notice);
		}
	}

	default Notice<?, ?> buildDroppedNotice(final Plan<?, List<T>> plan, final List<T> deleted) {
		return new SimplePlanRealizedNotice<>(plan, deleted);
	}

	//------------------------发布

	//todo:下面这些方法应该移到父类

	default PlanPublisher planPublisher() {
		return PlanPublisher.DO_NOTHING;
	}

	default <_TargetOwner, _ValType> Plan<_TargetOwner, _ValType> publish(final Plan<_TargetOwner, _ValType> plan) {
		if (plan != null) {
			Objects.requireNonNullElse(planPublisher(), PlanPublisher.DO_NOTHING).publish(plan);
		}
		return plan;
	}

	default Bulletin bulletin() {
		return Bulletin.DO_NOTHING;
	}

	default void publish(final Notice<?, ?> notice) {
		Objects.requireNonNullElse(bulletin(), Bulletin.DO_NOTHING).announce(notice);
	}
}
