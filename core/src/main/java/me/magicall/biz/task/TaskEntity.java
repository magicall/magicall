/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.task;

import me.magicall.Entity;
import me.magicall.task.Task;

/**
 * 领域对象：任务记录。
 * （暂时）不是任务本身，是针对任务的信息记录，如开始时间、结束时间、状态、进度等。
 */
public interface TaskEntity extends Task, Entity<String, TaskEntity> {
	static int hash(final TaskEntity one) {
		return Entity.hash(one);
	}

	static boolean eq(final Task one, final Object o) {
		return Entity.eq(TaskEntity.class, one, o);
	}
}
