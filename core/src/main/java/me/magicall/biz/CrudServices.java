/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz;

import me.magicall.Thing;
import me.magicall.data.Slice;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 所有 find* 方法，若对象不存在则返回null或空集；
 * 所有 get* 方法，若对象不存在则抛异常。
 */
public interface CrudServices<T, _ValObj> {
	/**
	 * 资源名。
	 * 用于组织文字（比如抛出异常时）。
	 *
	 * @return 资源名
	 */
	String resourceName();

	/**
	 * 流式获取所有指定类型的资源对象。
	 *
	 * @return 资源对象流
	 */
	Stream<T> streaming();

	default long count() {
		return streaming().count();
	}

	/**
	 * 获取符合过滤器条件的资源对象。
	 *
	 * @param filter 过滤器。
	 * @return 符合过滤器条件的资源对象。
	 */
	default Stream<T> filter(final Predicate<? super T> filter) {
		return streaming().filter(filter);
	}

	default long count(final Predicate<? super T> filter) {
		return filter(filter).count();
	}

	default Slice<T> slice(final int offset, final int limit) {
		return Slice.fromAll(streaming(), offset, limit);
	}

	default Slice<T> slice(final Predicate<? super T> filter, final int offset, final int limit) {
		return Slice.fromAll(filter(filter), offset, limit);
	}

	/**
	 * 寻找指定位置的资源对象。
	 *
	 * @param placeBase1 资源对象的位置，从1开始。
	 * @return 若不存在，返回null。
	 */
	default T findAt(final int placeBase1) {
		return streaming().skip(placeBase1 - 1).findFirst().orElse(null);
	}

	/**
	 * 获取指定位置的资源对象。若不存在，抛出 {@link NoSuchThingException}。若期望当元素不存在时不抛异常，使用 {@link #findAt(int)} 。
	 *
	 * @param placeBase1 资源对象的位置，从1开始。
	 * @return 若不存在，抛出 {@link NoSuchThingException}。
	 * @throws NoSuchThingException 若未找到，抛出 {@link NoSuchThingException}。
	 */
	default T getAt(final int placeBase1) {
		final var one = findAt(placeBase1);
		if (one == null) {
			throw new NoSuchThingException(resourceName(), placeBase1);
		}
		return one;
	}

	//------------------------------------------------------

	/**
	 * 确保参数所指定的资源对象集存在。
	 * 即：
	 * 若已存在，则确保已有对象的所有属性值与参数的相应属性值相等，然后返回已存在的对象。
	 * 若未存在，则创建，然后返回新创建的对象。
	 * 注：事务粒度（是否允许部分成功）由实现类决定。
	 *
	 * @return
	 */
	default Stream<T> ensure(final Stream<_ValObj> some) {
		throw new OpNotAllowedException(Thing.of(getClass().getSimpleName(), "?"), "ensure",
				Thing.of(resourceName(), some.toList()));
	}

	/**
	 * 确保参数所指定的资源对象存在。
	 * 即：
	 * 若已存在，则确保已有对象的所有属性值与参数的相应属性值相等，然后返回已存在的对象。
	 * 若未存在，则创建，然后返回新创建的对象。
	 * 注：事务粒度（是否允许部分成功）由实现类决定。
	 *
	 * @param one 要创建的对象。
	 * @return 已存在或已创建的对象。
	 */
	default T ensure(final _ValObj one) {
		return one == null ? null : ensure(Stream.of(one)).findFirst().orElse(null);
	}

	//------------------------------------------------------

	/**
	 * 确保指定资源对象集中的资源不再存在。
	 * 即：本来就不存在的不会报错（因为已经不存在了）。
	 * 注：事务粒度（是否允许部分成功）由实现类决定。
	 *
	 * @param some 要删除的对象。
	 * @return 被删除的对象。
	 */
	default Stream<T> drop(final Stream<T> some) {
		throw new OpNotAllowedException(Thing.of(getClass().getSimpleName(), "?"), "drop",
				Thing.of(resourceName(), some.toList()));
	}

	/**
	 * 丢弃符合过滤器的若干资源。
	 * 即：本来就不存在的不会报错（因为已经不存在了）。
	 * 注：事务粒度（是否允许部分成功）由实现类决定。
	 *
	 * @param filter 过滤器。
	 * @return 被删除的对象。
	 */
	default Stream<T> drop(final Predicate<? super T> filter) {
		return drop(filter(filter));
	}

	/**
	 * 丢弃指定的资源对象。使用 {@link Object#equals(Object)} 对比。若指定的资源对象不存在，则无事发生。
	 *
	 * @param one 指定的资源对象。
	 * @return 被删除的对象。若指定的资源对象不存在，则返回null。
	 */
	default T drop(final T one) {
		return drop(Stream.of(one)).findFirst().orElse(null);
	}

	/**
	 * 丢弃指定位置的资源对象。若指定的资源对象不存在，则无事发生。
	 *
	 * @param placeBase1 资源对象的位置，从1开始。
	 * @return 被删除的对象。若指定的资源对象不存在，则返回null。
	 */
	default T dropAt(final int placeBase1) {
		final var one = findAt(placeBase1);
		if (one == null) {
			return null;
		}
		return drop(one);
	}
}
