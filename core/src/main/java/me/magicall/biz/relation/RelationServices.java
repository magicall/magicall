/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.relation;

import me.magicall.Thing;
import me.magicall.relation.Relation;
import me.magicall.relation.RelationDto;
import me.magicall.scope.Scope;

import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface RelationServices {
	/**
	 * 获取指定范围内所有关系。
	 *
	 * @return
	 */
	Stream<Relation> relationsInScope(Scope scope);

	/**
	 * 在指定范围内获取指定事物的入度关系。与出度关系（{@link #outgoingsOf(Scope, Thing)}）类似，但方向相反。
	 *
	 * @return
	 */
	default Stream<Relation> incomingsOf(final Scope scope, final Thing thing) {
		return relationsInScope(scope).filter(e -> e.to().equals(thing));
	}

	/**
	 * 在指定范围内获取指定事物的出度关系。与入度关系（{@link #incomingsOf(Scope, Thing)}）类似，但方向相反。
	 *
	 * @return
	 */
	default Stream<Relation> outgoingsOf(final Scope scope, final Thing thing) {
		return relationsInScope(scope).filter(e -> e.from().equals(thing));
	}

	//------------------------------------

	/**
	 * 确保指定关系存在。
	 *
	 * @return
	 */
	Relation ensure(Relation relation);

	/**
	 * 在指定范围里关联二物。
	 *
	 * @return
	 */
	default Relation relate(final Scope scope, final Thing from, final Thing to, final Thing forwardPayload,
													final Thing backwardPayload, final float weight) {
		final var dto = new RelationDto();
		dto.scope = scope;
		dto.from = from;
		dto.to = to;
		dto.forwardPayload = forwardPayload;
		dto.backwardPayload = backwardPayload;
		dto.weight = weight;
		return ensure(dto);
	}

	//-----------------------------------------

	/**
	 * 取消符合指定条件的关系。
	 */
	void unrelate(Predicate<? super Relation> filter);

	/**
	 * 在指定范围里取消指定二物的关系。
	 */
	default void unrelate(final Scope scope, final Thing from, final Thing to) {
		unrelate(relation -> relation.scope().equals(scope) && relation.from().equals(from) && relation.to().equals(to));
	}

	/**
	 * 在指定范围里，取消指定二物带有指定双向关系名的所有关系，无论其权重如何。
	 */
	default void unrelate(final Scope scope, final Thing from, final Thing to, final Thing forwardPayload,
												final Thing backwardPayload) {
		unrelate(relation -> Relation.check(relation, scope, from, to, forwardPayload, backwardPayload));
	}

	/**
	 * 确保指定关系不再存在。
	 * 注意：要求权重也等同。
	 */
	default void unrelate(final Relation relation) {
		unrelate(Predicate.isEqual(relation));
	}

	default void unrelate(final Stream<Relation> relations) {
		final var collect = relations.collect(Collectors.toSet());
		unrelate(collect::contains);
	}
}
