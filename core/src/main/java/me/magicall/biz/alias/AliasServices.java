/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.alias;

import me.magicall.Thing;
import me.magicall.alias.AliasGroup;

import java.util.stream.Stream;

/**
 * 不局限于别名，表示的是“事物a和事物b相同”。所以针对的是{@link Thing}。
 */
public interface AliasServices {

	/**
	 * 获取一个事物的所有别名。
	 * 若其没有别名，则返回一个只有它自己的别名组。
	 *
	 * @return 不会为null。
	 */
	AliasGroup aliasGroupOf(Thing thing);

	/**
	 * 确保参数中的名字互为别名。
	 *
	 * @return
	 */
	AliasGroup makeAlias(Stream<Thing> things);

	/**
	 * 确保参数中的名字互为别名。
	 *
	 * @return
	 */
	default AliasGroup makeAlias(final Thing... things) {
		return makeAlias(Stream.of(things));
	}

	/**
	 * 判断两者是否别名。
	 *
	 * @return
	 */
	default boolean isAlias(final Thing a, final Thing b) {
		return aliasGroupOf(a).contains(b);
	}
}
