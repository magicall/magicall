/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.cfg;

import me.magicall.Entity;
import me.magicall.cfg.Cfg;

public interface CfgEntity extends Cfg, Entity<String, Cfg> {
	static int hash(final CfgEntity one) {
		return Entity.hash(one);
	}

	static boolean eq(final CfgEntity a, final Object b) {
		return Entity.eq(CfgEntity.class, a, b);
	}
}
