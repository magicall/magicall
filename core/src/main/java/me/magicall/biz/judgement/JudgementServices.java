/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.judgement;

import me.magicall.Thing;
import me.magicall.biz.CrudServices;
import me.magicall.judge.Judgement;
import me.magicall.opinion.Opinion;

import java.util.stream.Stream;

public interface JudgementServices extends CrudServices<Judgement<Thing, Thing>, Judgement<Thing, Thing>> {
	@Override
	default String resourceName() {
		return "判断";
	}

	/**
	 * 获取某判断者的判断集。
	 *
	 * @return
	 */
	default Stream<Judgement<Thing, Thing>> judgementsBy(final Thing judge) {
		return filter(judgement -> judgement.getJudge().equals(judge));
	}

	/**
	 * 获取针对某物的判断集。
	 *
	 * @return
	 */
	default Stream<Judgement<Thing, Thing>> judgementsAgainst(final Thing against) {
		return filter(judgement -> judgement.against().equals(against));
	}

	/**
	 * 获取某判断针对对某物的判断集。
	 *
	 * @return
	 */
	default Stream<Judgement<Thing, Thing>> judgementsAgainstBy(final Thing against, final Thing judge) {
		return filter(judgement -> judgement.getJudge().equals(judge) && judgement.against().equals(against));
	}

	/**
	 * 获取所有确定性的判断。
	 *
	 * @return
	 */
	default Stream<Judgement<Thing, Thing>> confirmedJudgements() {
		return filter(Judgement::isConfirmed);
	}

	/**
	 * 获取针对某物的判断的最高确信度。
	 *
	 * @return
	 */
	default double maxCertaintyAgainst(final Thing against) {
		return judgementsAgainst(against)//
				.mapToDouble(Judgement::certainty)//
				.max().orElse(Opinion.CERTAINTY_INDEFINITE);
	}
}
