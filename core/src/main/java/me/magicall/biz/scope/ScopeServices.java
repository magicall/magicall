/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.scope;

import me.magicall.biz.EntityCrudServices;
import me.magicall.scope.Scope;

import java.util.stream.Stream;

public interface ScopeServices extends EntityCrudServices<String, ScopeEntity, Scope> {
	@Override
	default String resourceName() {
		return "范围";
	}

	default Stream<ScopeEntity> roots() {
		return streaming().filter(this::isRoot);
	}

	default boolean isRoot(final Scope scope) {
		return scope.biggerScopes().findAny().isEmpty();
	}
}
