/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.scope;

import me.magicall.Entity;
import me.magicall.scope.Scope;

public interface ScopeEntity extends Scope, Entity<String, Scope> {
	static int hash(final ScopeEntity one) {
		return Entity.hash(one);
	}

	static boolean eq(final ScopeEntity a, final Object b) {
		return Entity.eq(ScopeEntity.class, a, b);
	}
}
