/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.condition;

import me.magicall.Entity;
import me.magicall.condition.Condition;
import me.magicall.condition.ConditionGroup;

public interface ConditionGroupEntity extends ConditionEntity, ConditionGroup {
	static String str(final ConditionEntity one) {
		return Condition.str(one);
	}

	static int hash(final ConditionEntity one) {
		return Entity.hash(one);
	}

	static boolean eq(final ConditionEntity a, final Object b) {
		return Entity.eq(ConditionGroupEntity.class, a, b);
	}
}
