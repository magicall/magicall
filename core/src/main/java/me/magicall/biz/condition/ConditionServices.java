/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.biz.condition;

import me.magicall.biz.EntityAsVoCrudServices;
import me.magicall.program.lang.java.贵阳DearSun.coll.NodeWalker;

import java.util.stream.Stream;

public interface ConditionServices extends EntityAsVoCrudServices<String, ConditionEntity> {
	@Override
	default String resourceName() {
		return "条件";
	}

	/**
	 * 返回中不包含子条件。
	 *
	 * @return
	 */
	@Override
	Stream<ConditionEntity> streaming();

	/**
	 * 拍平的所有条件，包括所有子条件。
	 *
	 * @return
	 */
	default Stream<ConditionEntity> flatStream() {
		return streaming().flatMap(condition -> {
			if (condition instanceof ConditionGroupEntity) {
				return NodeWalker.deepFirst()//
						.walk(condition,//
								e -> e instanceof final ConditionGroupEntity group ? group.parts().map(ConditionGroupEntity.class::cast)
																																	 : Stream.of());
			} else {
				return Stream.of(condition);
			}
		});
	}
}
