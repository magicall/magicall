/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cache.impl.spring;

import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.SimpleCacheResolver;

import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassNameAsCacheNameCacheResolver extends SimpleCacheResolver implements CacheResolver {
	public ClassNameAsCacheNameCacheResolver(final CacheManager cacheManager) {
		super(cacheManager);
	}

	@Override
	protected Collection<String> getCacheNames(final CacheOperationInvocationContext<?> context) {
		final Class<?> targetClass = context.getTarget().getClass();
		final String className;
		if (Proxy.isProxyClass(targetClass)) {
			className = targetClass.getInterfaces()[0].getName();
		} else {
			className = targetClass.getName();
		}
		final var cacheNames = super.getCacheNames(context);
		if (cacheNames == null || cacheNames.isEmpty()) {
			return List.of(className);
		}
		return Stream.concat(Stream.of(className), cacheNames.stream()).collect(Collectors.toList());
	}
}
