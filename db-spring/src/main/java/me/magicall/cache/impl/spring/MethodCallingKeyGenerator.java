/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.cache.impl.spring;

import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import org.springframework.cache.interceptor.KeyGenerator;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.stream.Stream;

public class MethodCallingKeyGenerator implements KeyGenerator {
	@Override
	public Object generate(final Object target, final Method method, final Object... params) {
		final String paramsPart;
		if (params.length == 0) {
			paramsPart = "";
		} else if (params.length == 1 && params[0] instanceof Stream) {
			paramsPart = "Stream";
		} else {
			paramsPart = Arrays.toString(params);
		}
		final Class<?> aClass = target.getClass();
		final var className = (Proxy.isProxyClass(aClass) ? aClass.getInterfaces()[0] : aClass).getSimpleName();
		return StrKit.format("{0}.{1}({2})", className, method.getName(), paramsPart);
	}
}
