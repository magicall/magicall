/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjpa;

import me.magicall.cache.impl.spring.ClassNameAsCacheNameCacheResolver;
import me.magicall.cache.impl.spring.MethodCallingKeyGenerator;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.FluentQuery.FetchableFluentQuery;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * 加入了缓存（spring-cache）支持的jpa repo类的便利父接口。
 * 对JpaRepository的所有读方法都加上了缓存，缓存区名字生成方式见{@link ClassNameAsCacheNameCacheResolver}，缓存key生成方式见{@link MethodCallingKeyGenerator}
 * 所有写方法都清除缓存区下所有缓存数据。
 * 注：仅适用于数据量总体不太大的情况。
 */
@NoRepositoryBean
@CacheConfig(cacheResolver = "classNameAsCacheNameCacheResolver", keyGenerator = "methodCallingKeyGenerator")
public interface CachedSpringJpaRepoSupport<_Id, _Entity> extends SpringJpaRepoSupport<_Id, _Entity> {

	@CacheEvict(allEntries = true)
	@Override
	default <S extends _Entity> List<S> store(final Stream<S> entities) {
		return SpringJpaRepoSupport.super.store(entities);
	}

	@Cacheable
	@Override
	default List<_Entity> all() {
		return SpringJpaRepoSupport.super.all();
	}

	@CacheEvict(allEntries = true)
	@Override
	default void del(final Stream<_Entity> entities) {
		SpringJpaRepoSupport.super.del(entities);
	}

	//--------------------------读方法

	@Cacheable
	@Override
	List<_Entity> findAll();

	@Cacheable
	@Override
	List<_Entity> findAll(Sort sort);

	@Cacheable
	@Override
	List<_Entity> findAllById(Iterable<_Id> ids);

	/**
	 * 根据id获取一个实体对象。
	 * 由底层实现（比如hibernate）决定是立即查数据库获得真正的实体对象，还是返回一个代理对象，在访问其非id属性时才延迟查询数据库。
	 * 若该对象不存在，会抛出异常。
	 *
	 * @param id must not be {@literal null}.
	 * @return
	 */
	@Cacheable(key = "#id")//与其他的“根据id获取实体”共用缓存数据，所以把key设置成相同
	@Override
	_Entity getReferenceById(_Id id);

	/**
	 * 与 {@link  #getReferenceById(Object)} 完全相同。
	 *
	 * @return
	 * @deprecated 根据父接口，已过时，用 {@link #getReferenceById(Object)} 代替。
	 */
	@Deprecated
	@Cacheable(key = "#id")//与其他的“根据id获取实体”共用缓存数据，所以把key设置成相同
	@Override
	_Entity getOne(_Id id);

	/**
	 * 与 {@link  #getReferenceById(Object)} 完全相同。
	 *
	 * @return
	 * @deprecated 根据父接口，已过时，用 {@link #getReferenceById(Object)} 代替。
	 */
	@Deprecated
	@Cacheable(key = "#id")//与其他的“根据id获取实体”共用缓存数据，所以把key设置成相同
	@Override
	_Entity getById(_Id id);

	@Cacheable
	@Override
	<S extends _Entity> List<S> findAll(Example<S> example);

	@Cacheable
	@Override
	<S extends _Entity> List<S> findAll(Example<S> example, Sort sort);

	@Cacheable
	@Override
	Page<_Entity> findAll(Pageable pageable);

	@Cacheable
	@Override
	Optional<_Entity> findById(_Id id);

	@Cacheable
	@Override
	boolean existsById(_Id id);

	@Cacheable
	@Override
	long count();

	@Cacheable
	@Override
	<S extends _Entity> Optional<S> findOne(Example<S> example);

	@Cacheable
	@Override
	<S extends _Entity> Page<S> findAll(Example<S> example, Pageable pageable);

	@Cacheable
	@Override
	<S extends _Entity> long count(Example<S> example);

	@Cacheable
	@Override
	<S extends _Entity> boolean exists(Example<S> example);

	@Cacheable
	@Override
	<S extends _Entity, R> R findBy(Example<S> example, Function<FetchableFluentQuery<S>, R> queryFunction);

	//--------------------------写方法

	@CacheEvict(allEntries = true)
	@Override
	<S extends _Entity> List<S> saveAll(Iterable<S> entities);

	@CacheEvict(allEntries = true)
	@Override
	void flush();

	@CacheEvict(allEntries = true)
	@Override
	<S extends _Entity> S saveAndFlush(S entity);

	@CacheEvict(allEntries = true)
	@Override
	<S extends _Entity> List<S> saveAllAndFlush(Iterable<S> entities);

	/**
	 * @param entities entities to be deleted. Must not be {@literal null}.
	 * @deprecated 根据父接口，已过时
	 */
	@Deprecated
	@CacheEvict(allEntries = true)
	@Override
	default void deleteInBatch(final Iterable<_Entity> entities) {
		SpringJpaRepoSupport.super.deleteInBatch(entities);
	}

	@CacheEvict(allEntries = true)
	@Override
	void deleteAllInBatch(Iterable<_Entity> entities);

	@CacheEvict(allEntries = true)
	@Override
	void deleteAllByIdInBatch(Iterable<_Id> ids);

	@CacheEvict(allEntries = true)
	@Override
	void deleteAllInBatch();

	@CacheEvict(allEntries = true)
	@Override
	<S extends _Entity> S save(S entity);

	@CacheEvict(allEntries = true)
	@Override
	void deleteById(_Id id);

	@CacheEvict(allEntries = true)
	@Override
	void delete(_Entity entity);

	@CacheEvict(allEntries = true)
	@Override
	void deleteAllById(Iterable<? extends _Id> ids);

	@CacheEvict(allEntries = true)
	@Override
	void deleteAll(Iterable<? extends _Entity> entities);

	@CacheEvict(allEntries = true)
	@Override
	void deleteAll();
}
