/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjpa;

import me.magicall.biz.Repo;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.stream.Stream;

@NoRepositoryBean
public interface SpringJpaRepoSupport<_Id, _Entity> extends JpaRepository<_Entity, _Id>, Repo<_Entity> {
	@Override
	default <S extends _Entity> List<S> store(final Stream<S> entities) {
		final var list = entities.toList();
		if (list.isEmpty()) {
			return list;
		}
		return saveAll(list);
	}

	@Override
	default List<_Entity> all() {
		return findAll(Sort.by(Direction.ASC, "id"));
	}

	@Override
	default void del(final Stream<_Entity> entities) {
		final var collected = entities.toList();
		if (!collected.isEmpty()) {
			deleteAll(collected);
		}
	}
}
