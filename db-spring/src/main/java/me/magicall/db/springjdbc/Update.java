/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.meta.DbColumn;
import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.ModelMapTransformer;
import me.magicall.db.outsea.UpdateSqlConfig;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;
import me.magicall.db.util.DbUtil;
import me.magicall.db.util.HandleNullValueStrategy;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Update<T> extends AbsDataAccessor<T, Integer, UpdateSqlConfig<T>> {

	private final SqlBuilder<UpdateSqlConfig<T>> sqlBuilder = sqlConfig -> {
		final var mainModelName = sqlConfig.getMainModelName();
		final var tableMeta = tableMetaAccessor.getTableMetaIgnoreCase(mainModelName);

		final var refedModel = sqlConfig.getRefedModel();
		final var valueMap = modelMapTransformer.modelToMap(refedModel);
		if (Kits.MAP.isEmpty(valueMap)) {
			throw new IllegalArgumentException("can't parse refed model to map with value:" + refedModel);
		}

		final var sb = new StringBuilder("UPDATE ");
		sb.append(modelNameTableNameTransformer.modelNameToTableName(mainModelName)).append(" SET ");

		final var fieldFilter = sqlConfig.getFieldFilter();

		final var primaryKey = tableMeta.getPrimaryKey();
		List<DbColumn> primaryKeyColumns;
		if (primaryKey == null) {
			primaryKeyColumns = Kits.LIST.emptyVal();
		} else {
			primaryKeyColumns = primaryKey.getColumns();
		}
		final var columns = tableMeta.getColumns();
		var changed = false;
		final Map<String, Object> namedParams = new HashMap<>();
		for (final var column : columns) {
			if (primaryKeyColumns.contains(column)) {//主键不update
				continue;
			}
			final var columnName = column.name();
			final var fieldName = fieldNameColumnNameTransformer.columnNameToFieldName(columnName);
			if (fieldFilter == null || fieldFilter.accept(tableMeta, column)) {
				if (valueMap.containsKey(fieldName)) {//若refedModel中某字段未设置值,不update它
					final var value = valueMap.get(fieldName);
					final var paramedName = mainModelName + DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR + columnName;
					final var handleNullValueStrategy = sqlConfig.getHandleNullValueStrategy();
					if (value == null) {
						if (handleNullValueStrategy == HandleNullValueStrategy.ESCAPE) {
							continue;
						}
						if (handleNullValueStrategy == HandleNullValueStrategy.USE_DEFAULT_VALUE) {
							final var defaultValue = column.getDefaultValue();
							if (defaultValue == null && !column.getNullable()) {
								throw new IllegalArgumentException("column " + columnName + " cannot be null");
							}
							namedParams.put(paramedName, defaultValue);
						} else if (handleNullValueStrategy == HandleNullValueStrategy.STAY_NULL) {
							namedParams.put(paramedName, null);
						}
					} else {
						namedParams.put(paramedName, value);
					}
					sb.append(columnName).append('=').append(':').append(paramedName).append(',');
					changed = true;
				}
			}
		}
		if (changed) {
			sb.deleteCharAt(sb.length() - 1);
		} else {
			throw new IllegalArgumentException("nothing to update");
		}

		final var conditions = sqlConfig.getConditions();
		SqlUtil.appendWhere(sb, tableMeta, fieldNameColumnNameTransformer, conditions, namedParams);

		return new ParamedSqlAndParams(sb.toString(), namedParams);
	};

	public Update(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	@Override
	public UpdateSqlConfig<T> createSqlConfig(final String mainModelName) {
		return new UpdateSqlConfig<>(mainModelName);
	}

	@Override
	protected Integer exe(final String sql, final Map<String, ?> params, final UpdateSqlConfig<T> sqlConfig) {
		return namedJdbc.update(sql, params);
	}

	@Override
	protected SqlBuilder<UpdateSqlConfig<T>> getSqlBuilder() {
		return sqlBuilder;
	}

	@Override
	public ModelMapTransformer<T> getModelMapTransformer() {
		return super.getModelMapTransformer();
	}

	@Override
	public void setModelMapTransformer(final ModelMapTransformer<T> modelMapTransformer) {
		super.setModelMapTransformer(modelMapTransformer);
	}
}
