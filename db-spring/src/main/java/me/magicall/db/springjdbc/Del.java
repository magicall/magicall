/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.DelSqlConfig;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;
import me.magicall.db.util.DbUtil;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.HashMap;
import java.util.Map;

public class Del<T> extends AbsDataAccessor<T, Integer, DelSqlConfig<T>> {

	private final SqlBuilder<DelSqlConfig<T>> sqlBuilder = sqlConfig -> {
		final var mainModelName = sqlConfig.getMainModelName();
		final var sb = DbUtil.buildDeleteFromTable(modelNameTableNameTransformer.modelNameToTableName(mainModelName));
		//where
		final Map<String, Object> namedParams = new HashMap<>();
		SqlUtil.appendWhere(sb, tableMetaAccessor.getTableMetaIgnoreCase(mainModelName),//
				fieldNameColumnNameTransformer, sqlConfig.getConditions(), namedParams);
		//order by
		DbUtil.appendOrderBy(sb, sqlConfig.getFieldComparator());
		//limit
		DbUtil.appendLimit(sb, sqlConfig.getPageInfo());

		return new ParamedSqlAndParams(sb.toString(), namedParams);
	};

	public Del(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	@Override
	public DelSqlConfig<T> createSqlConfig(final String mainModelName) {
		return new DelSqlConfig<>(mainModelName);
	}

	@Override
	protected Integer exe(final String sql, final Map<String, ?> params, final DelSqlConfig<T> sqlConfig) {
		return namedJdbc.update(sql, params);
	}

	@Override
	protected SqlBuilder<DelSqlConfig<T>> getSqlBuilder() {
		return sqlBuilder;
	}
}
