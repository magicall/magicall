/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc.cached;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import me.magicall.db.outsea.DataAccessor;
import me.magicall.db.outsea.SqlConfig;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public abstract class AbsCachedDataAccessor<R, C extends SqlConfig> implements DataAccessor<R, C> {

	protected final LoadingCache<C, R> cache//
			= expireAfterWrite((CacheBuilder) CacheBuilder.newBuilder())//
			.build(new CacheLoader<C, R>() {
				@Override
				public R load(final C key) {
					return wrapped().exe(key);
				}
			});

	@Override
	public C createSqlConfig(final String mainModelName) {
		return wrapped().createSqlConfig(mainModelName);
	}

	@Override
	public R exe(final C sqlConfig) {
		try {
			return cache.get(sqlConfig);
		} catch (final ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected CacheBuilder<C, R> expireAfterWrite(final CacheBuilder<C, R> cacheBuilder) {
		return cacheBuilder.expireAfterAccess(expireMillsecondAfterWrite(), TimeUnit.MILLISECONDS);
	}

	protected long expireMillsecondAfterWrite() {
		return TimeUnit.MINUTES.toMillis(30);
	}

	protected abstract DataAccessor<R, C> wrapped();

	protected void clear() {
		cache.invalidateAll();
	}
}
