/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.meta.DbColumn;
import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.AddSqlConfig;
import me.magicall.db.outsea.ModelMapTransformer;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;
import me.magicall.db.util.DbUtil;
import me.magicall.db.util.OptionOnExist;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.FieldValueAccessor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Add<T> extends AbsDataAccessor<T, Integer, AddSqlConfig<T>> {

	private final SqlBuilder<AddSqlConfig<T>> sqlBuilder = sqlConfig -> {
		final var mainModelName = sqlConfig.getMainModelName();
		final var tableMeta = tableMetaAccessor.getTableMetaIgnoreCase(mainModelName);
		if (tableMeta == null) {
			throw new IllegalArgumentException("no such table");
		}
		//insert | insert ignore
		final var sb = new StringBuilder("insert ");
		final var onExist = sqlConfig.getOptionOnExist();
		if (onExist == OptionOnExist.IGNORE) {
			sb.append("ignore ");
		}
		//into fields
		final var fieldFilter = sqlConfig.getFieldFilter();
		sb.append("into ").append(tableMeta.name()).append('(');
		final var columns = tableMeta.getColumns();
		final List<DbColumn> columnsToUse = new ArrayList<>(columns.size());
		if (fieldFilter == null) {
			columns.forEach(column -> {
				sb.append(DbUtil.quoteDbName(column.name())).append(',');
				columnsToUse.add(column);
			});
		} else {
			columns.forEach(column -> {
				final var columnName = column.name();
				if (fieldFilter.accept(tableMeta, column)) {
					sb.append(DbUtil.quoteDbName(columnName)).append(',');
					columnsToUse.add(column);
				}
			});
		}
		sb.deleteCharAt(sb.length() - 1)
				//values
				.append(")values");

		final Map<String, Object> namedParams = new HashMap<>();

		final var newValue = sqlConfig.getRefedModel();
		appendNewValue(sb, mainModelName, newValue, columnsToUse, namedParams);
		//more values
		final var otherNewValues = sqlConfig.getOtherNewValues();
		if (!Kits.LIST.isEmpty(otherNewValues)) {
			var index = 0;
			for (final var t : otherNewValues) {
				sb.append(',');
				appendNewValue(sb, mainModelName + index, t, columnsToUse, namedParams);
				++index;
			}
		}
		//on duplicate key
		if (onExist == OptionOnExist.REPLACE) {
			sb.append(" on duplicate key update ");
			final var primaryKey = tableMeta.getPrimaryKey();
			List<DbColumn> primaryKeyColumns;
			if (primaryKey == null) {
				primaryKeyColumns = Kits.LIST.emptyVal();
			} else {
				primaryKeyColumns = primaryKey.getColumns();
			}
			//主键不update
			columnsToUse.stream().filter(column -> !primaryKeyColumns.contains(column))
					.map(column -> DbUtil.quoteDbName(column.name())).forEach(
							quotedColumnName -> sb.append(quotedColumnName).append("=values(").append(quotedColumnName).append("),"));
			sb.deleteCharAt(sb.length() - 1);
		}

		return new ParamedSqlAndParams(sb.toString(), namedParams);
	};

	public Add(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	private void appendNewValue(final StringBuilder sb, //
															final String modelName, final T newModel, final List<DbColumn> columnsToUse,//
															final Map<String, Object> namedParams) {
		sb.append('(');
		final var valueMap = modelMapTransformer.modelToMap(newModel);

		var changed = false;
		for (final var column : columnsToUse) {//是该表的字段才管它.
			final var columnName = column.name();
			final var paramedName = modelName + DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR + columnName;
			sb.append(':').append(paramedName).append(',');

			final var fieldName = fieldNameColumnNameTransformer.columnNameToFieldName(columnName);
			final var value = valueMap.get(fieldName);
			if (value == null && !column.getNullable() && !column.getHasDefaultValue()) {
				throw new EmptyValueException(column);
			}
			namedParams.put(paramedName, value);

			changed = true;
		}
		sb.deleteCharAt(sb.length() - 1);//it is '(' or ','
		if (changed) {
			sb.append(')');
		}
	}

	@Override
	public AddSqlConfig<T> createSqlConfig(final String mainModelName) {
		return new AddSqlConfig<>(mainModelName);
	}

	@Override
	protected Integer exe(final String sql, final Map<String, ?> params, final AddSqlConfig<T> sqlConfig) {
		//注:mysql返回的自增id在以下情况下不太对:
		//同时插入多条记录,并且其中有一条记录设置了主键;
		//或 在insert on duplicate key情况下,有记录触发了update
		//或 该表有联合主键,其中一个字段是自增的,另一个字段不是自增的,并且同时插入多条记录,并且多条记录中那个自增的字段是相等的,此时返回的自增key不正确.
		final var onExist = sqlConfig.getOptionOnExist();
		if (onExist != OptionOnExist.IGNORE && onExist != OptionOnExist.REPLACE) {
			final var tableMeta = tableMetaAccessor.getTableMetaIgnoreCase(sqlConfig.getMainModelName());
			final var key = tableMeta.getPrimaryKey();
			final var primaryKeyColumns = key.getColumns();
			if (primaryKeyColumns.size() > 1) {
				//来到这里有可能是:该表有联合主键,其中一个字段是自增的,另一个字段不是自增的,并且同时插入多条记录,并且多条记录中那个自增的字段是相等的,此时返回的自增key不正确.
				return namedJdbc.update(sql, params);
			}
			final var newValues = sqlConfig.getRefedModels();
			if (hasId(newValues, primaryKeyColumns)) {//这里有可能是"同时插入多条记录,并且其中有一条记录设置了主键"
				return namedJdbc.update(sql, params);
			} else {
				//填充自增主键
				final KeyHolder keyHolder = new GeneratedKeyHolder();
				final var paramedSqlAndParams = getSqlBuilder().buildSql(sqlConfig);
				final var changed = namedJdbc.update(paramedSqlAndParams.paramedSql,//
						new MapSqlParameterSource(paramedSqlAndParams.params), keyHolder);
				final var keyList = keyHolder.getKeyList();
				var index = 0;
				for (final var t : newValues) {
					final var returnKey = keyList.get(index);
					index++;
					primaryKeyColumns.stream().filter(DbColumn::getAutoInc).map(DbColumn::name)
							.map(columnName -> fieldNameColumnNameTransformer.columnNameToFieldName(columnName))
							.forEach(fieldName -> fieldValueAccessor.setValue(t, fieldName, returnKey.values().iterator().next()));
				}
				return changed;
			}
		} else {//来到这里有可能是"insert on duplicate key情况下,有记录触发了update"
			return namedJdbc.update(sql, params);
		}
	}

	@Override
	protected SqlBuilder<AddSqlConfig<T>> getSqlBuilder() {
		return sqlBuilder;
	}

	private boolean hasId(final List<T> newValues, final List<DbColumn> primaryKeyColumns) {
		for (final var t : newValues) {
			for (final var column : primaryKeyColumns) {
				final var fieldName = fieldNameColumnNameTransformer.columnNameToFieldName(column.name());
				if (column.getAutoInc() && fieldValueAccessor.getValue(t, fieldName) != null) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public ModelMapTransformer<T> getModelMapTransformer() {
		return super.getModelMapTransformer();
	}

	@Override
	public void setModelMapTransformer(final ModelMapTransformer<T> modelMapTransformer) {
		super.setModelMapTransformer(modelMapTransformer);
	}

	@Override
	public FieldValueAccessor<? super T> getFieldValueAccessor() {
		return super.getFieldValueAccessor();
	}

	@Override
	public void setFieldValueAccessor(final FieldValueAccessor<? super T> fieldValueAccessor) {
		super.setFieldValueAccessor(fieldValueAccessor);
	}
}
