/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.CountSqlConfig;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.HashMap;
import java.util.Map;

public class Count<T> extends AbsDataAccessor<T, Integer, CountSqlConfig<T>> {

	private final SqlBuilder<CountSqlConfig<T>> sqlBuilder = sqlConfig -> {
		final var sb = new StringBuilder();
		final Map<String, Object> namedParams = new HashMap<>();
		//select from where
		SqlUtil.buildSelectCountFromWhere(sb, namedParams, tableMetaAccessor,//
				fieldNameColumnNameTransformer, sqlConfig);

		return new ParamedSqlAndParams(sb.toString(), namedParams);
	};

	public Count(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	@Override
	public CountSqlConfig<T> createSqlConfig(final String mainModelName) {
		return new CountSqlConfig<>(mainModelName);
	}

	@Override
	protected Integer exe(final String sql, final Map<String, ?> params, final CountSqlConfig<T> sqlConfig) {
		return namedJdbc.queryForObject(sql, params, Integer.class);
	}

	@Override
	protected SqlBuilder<CountSqlConfig<T>> getSqlBuilder() {
		return sqlBuilder;
	}
}
