/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.magicall.computer.Encode;
import me.magicall.db.dbms.CommonDBMS;
import me.magicall.db.dbms.DBMS;
import me.magicall.db.meta.BaseDbMeta;
import me.magicall.db.meta.DbColumn;
import me.magicall.db.meta.ForeignKey;
import me.magicall.db.meta.Key;
import me.magicall.db.meta.Key.KeyType;
import me.magicall.db.meta.TableMeta;
import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.util.DbUtil;
import me.magicall.db.util.FieldType;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.time.TimeFormatter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SpringJdbcTableMetaAccessor implements TableMetaAccessor, InitializingBean {

	private static final Pattern JOIN_PATTERN = Pattern.compile(
			"\\s*(?:join\\s+)?`[a-zA-Z0-9_]+`\\.`([a-zA-Z0-9_]+)`\\s+`([a-zA-Z0-9_]+)`\\s*");
	private static final String SELECT = "select ",//
			FROM = " from ",//
			AS = " AS ";
	private final DBMS dbms = CommonDBMS.MYSQL;
	private final DriverManagerDataSource dataSource;
	private final String dbName;
	private Map<String, TableMeta> tableMetaMap;

	public SpringJdbcTableMetaAccessor(final DriverManagerDataSource dataSource, final String dbName) {
		super();
		this.dataSource = dataSource;
		this.dbName = dbName;
	}

	//==============================================
	public TableMeta getTableMetaUsingModelName(final String modelName) {
		return getTableMetaUsingTableName(DbUtil.javaNameToDbName(modelName));
	}

	public TableMeta getTableMetaUsingTableName(final String tableName) {
		if (tableMetaMap == null) {
			tableMetaMap = Maps.newConcurrentMap();
			return null;
		} else {
			return tableMetaMap.get(tableName.toLowerCase());
		}
	}

	@Override
	public TableMeta getTableMetaIgnoreCase(final String tableNameOrModelName) {
		return getTableMetaUsingTableName(DbUtil.javaNameToDbName(tableNameOrModelName));
	}

	public void build() {
		final var metaJdbc = newJdbcOptions();

		final var tableMetas = parseTableMetas(metaJdbc);
		final Map<String, TableMeta> tableMap = tableMetas.stream()
				.collect(Collectors.toMap(BaseDbMeta::name, Function.identity(), (a, b) -> b));

		parseColumnMetas(metaJdbc, tableMap);
		parseKeys(metaJdbc, tableMap);

		buildView(metaJdbc, tableMap);

		final Map<String, TableMeta> tmp = tableMap.entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey().toLowerCase(), Entry::getValue, (a, b) -> b, Maps::newConcurrentMap));

		tableMetaMap = tmp;
	}

	protected void parseKeys(final JdbcOperations metaJdbc, final Map<String, TableMeta> tableMap) {
		//键
		final Map<String, Key> keyMap = Maps.newHashMap();

		final var keys = metaJdbc.queryForList("select * from KEY_COLUMN_USAGE where `TABLE_SCHEMA`='" + dbName + '\'');
		for (final var keyInfo : keys) {
			final var name = String.valueOf(keyInfo.get("CONSTRAINT_NAME"));
			//所属表
			final var tableName = String.valueOf(keyInfo.get("TABLE_NAME"));
			final var tableMeta = tableMap.get(tableName);

			final var columnName = String.valueOf(keyInfo.get("COLUMN_NAME"));
			final var column = tableMeta.getColumn(columnName);
			if ("PRIMARY".equalsIgnoreCase(name)) {//主键
				var key = tableMeta.getPrimaryKey();
				if (key == null) {
					key = new Key();
					key.renameTo(name);
					key.setType(KeyType.PRIMARY);
					tableMeta.setPrimaryKey(key);
				}
				key.add(column);
			} else {//非主键索引
				var key = keyMap.get(name);
				if (key == null) {
					key = new Key();
					key.renameTo(name);
					final var positionInUniqueConstraint = keyInfo.get("POSITION_IN_UNIQUE_CONSTRAINT");
					key.setType(positionInUniqueConstraint == null ? KeyType.UNIQUE : KeyType.COMMON);

					tableMeta.getKeys().add(key);
					keyMap.put(name, key);
				}
				key.add(column);
			}
			//外键
			final var referencedTableSchema = keyInfo.get("REFERENCED_TABLE_SCHEMA");
			if (referencedTableSchema != null) {
				final var refedTableName = String.valueOf(keyInfo.get("REFERENCED_TABLE_NAME"));
				final var refedTable = tableMap.get(refedTableName);
				final var refedColumnName = String.valueOf(keyInfo.get("REFERENCED_COLUMN_NAME"));

				final var foreignKey = new ForeignKey();
				foreignKey.renameTo(name);
				foreignKey.setReferencingColumn(column);
				foreignKey.setReferencingTable(tableMeta);
				foreignKey.setReferencedTable(refedTable);
				foreignKey.setReferencedColumn(refedTable.getColumn(refedColumnName));
			}
		}//for keys
	}

	protected void parseColumnMetas(final JdbcOperations metaJdbc, final Map<String, TableMeta> tableMap) {
		//列的基本信息
		final var columns = metaJdbc.queryForList("select * from COLUMNS where `TABLE_SCHEMA`='" + dbName + '\'');
		//for columns
		columns.forEach(columnInfo -> {
			final var tableMeta = tableMap.get(columnInfo.get("TABLE_NAME"));
			final var column = new DbColumn();
			final var extra = String.valueOf(columnInfo.get("Extra"));
			final var autoInc = extra.contains("auto_increment");
			column.setAutoInc(autoInc);
			column.setComment(String.valueOf(columnInfo.get("COLUMN_COMMENT")));
			column.renameTo(String.valueOf(columnInfo.get("COLUMN_NAME")));
			column.setNullable(Kits.BOOL.fromString(String.valueOf(columnInfo.get("IS_NULLABLE"))));
			final var typeString = String.valueOf(columnInfo.get("COLUMN_TYPE"));
			final var unsigned = typeString.contains("unsigned");
			column.setUnsigned(unsigned);
			final var zeroFill = typeString.contains("zerofill");
			column.setZeroFill(zeroFill);
			final var typeName = String.valueOf(columnInfo.get("DATA_TYPE"));
			final var fieldType = dbms.getType(typeName);
			column.setType(fieldType);
			if (FieldType.VARCHAR == fieldType) {
				final var lenStr = String.valueOf(columnInfo.get("CHARACTER_MAXIMUM_LENGTH"));
				if (!Kits.STR.isEmpty(lenStr)) {
					column.setLength(Kits.INT.fromString(lenStr));
				}
			}
			final var defaultValue = columnInfo.get("COLUMN_DEFAULT");
			if (defaultValue == null) {
				if (autoInc) {
					column.setHasDefaultValue(true);
				}
			} else {
				final var defaultValueStr = String.valueOf(defaultValue);
				if (fieldType == FieldType.TIMESTAMP) {
					if ("CURRENT_TIMESTAMP".equalsIgnoreCase(defaultValueStr)) {
						column.setDefaultValue(null);
						column.setHasDefaultValue(true);
					} else {
						column.setDefaultValue(TimeFormatter.Y2_M2_D2_H2_MIN2_S2.parse(defaultValueStr));
					}
				} else {
					final var kit = fieldType.kit;
					if (kit != null) {
						column.setDefaultValue(kit.fromString(defaultValueStr));
					}
				}
			}
			tableMeta.add(column);
		});
	}

	protected Collection<TableMeta> parseTableMetas(final JdbcOperations metaJdbc) {
		//表的基本信息
		final var tables = metaJdbc.queryForList("select * from TABLES where `TABLE_SCHEMA`='" + dbName + '\'');

		final Collection<TableMeta> tableMetas = Lists.newArrayListWithExpectedSize(tables.size());
		//			tableMeta.setDefaultCharsetName(defaultCharsetName);
		//for tables
		tables.forEach(tableInfo -> {
			final var name = String.valueOf(tableInfo.get("TABLE_NAME"));
			final var tableMeta = new TableMeta();
			tableMeta.setComment(String.valueOf(tableInfo.get("TABLE_COMMENT")));
			tableMeta.setDbName(String.valueOf(tableInfo.get("TABLE_SCHEMA")));
			tableMeta.renameTo(name);
			tableMetas.add(tableMeta);
		});
		return tableMetas;
	}

	protected JdbcOperations newJdbcOptions() {
		final var ds = new DriverManagerDataSource();
		{//连接数据库
			ds.setDriverClassName(dbms.getDriverClassName());
			ds.setUsername(dataSource.getUsername());
			ds.setPassword(dataSource.getPassword());
			final var url = dataSource.getUrl();
			final var slashIndex = url.indexOf("//");
			final var colonIndex = url.indexOf(":", slashIndex);
			final var slash2Index = url.indexOf("/", colonIndex);
			int portEndIndex;
			if (slash2Index < 0) {
				portEndIndex = url.indexOf("?", colonIndex);
				if (portEndIndex < 0) {
					portEndIndex = url.length();
				}
			} else {
				portEndIndex = slash2Index;
			}
			ds.setUrl(dbms.formatUrl(url.substring(slashIndex + 2, colonIndex),//
					Kits.INT.fromString(url.substring(colonIndex + 1, portEndIndex)),//port
					"information_schema",//database name
					Collections.singletonMap("characterEncoding", Encode.GBK.getName())));
		}
		final JdbcOperations metaJdbc = new JdbcTemplate(ds);
		return metaJdbc;
	}

	public void buildView(final JdbcOperations metaJdbc, final Map<String, TableMeta> tableMap) {
		final var tables = metaJdbc.queryForList("select * from VIEWS where `TABLE_SCHEMA`='" + dbName + '\'');

		for (final var tableInfo : tables) {
			final var name = String.valueOf(tableInfo.get("TABLE_NAME"));
			final var viewDefinition = String.valueOf(tableInfo.get("VIEW_DEFINITION"));
			//(select
			//	`p`.`id` AS `id`,
			//	`t`.`name` AS `device_type_name`
			//from
			//		`anosi_asis`.`device_product` `p`
			//	join `anosi_asis`.`device_brand` `b`
			//	join `anosi_asis`.`device_type` `t`
			//where
			//(
			//		(`p`.`device_type_id` = `t`.`id`)
			//	and (`p`.`device_brand_id` = `b`.`id`)
			//))
			//(select
			//	`anosi_asis`.`staff`.`id` AS `id`,
			//	`anosi_asis`.`staff`.`name` AS `name`,
			//from `anosi_asis`.`staff`
			//)
			final var viewMeta = new TableMeta();
			viewMeta.setComment(String.valueOf(tableInfo.get("TABLE_COMMENT")));
			viewMeta.setDbName(String.valueOf(tableInfo.get("TABLE_SCHEMA")));
			viewMeta.renameTo(name);

			final Map<String, String> tableShortNameMap = new HashMap<>();
			{//from part
				final var from = StrKit.subStringAfter(viewDefinition, FROM).trim();
				final var fromMatcher = JOIN_PATTERN.matcher(from);
				while (fromMatcher.find()) {
					tableShortNameMap.put(fromMatcher.group(2), fromMatcher.group(1));
				}
			}

			{//select part
				final var select = StrKit.middle(viewDefinition, SELECT, false, FROM, false).trim();
				final var fields = select.split(",");
				// for field
				Arrays.stream(fields).map(String::trim).forEach(field -> {
					final var asIndex = StrKit.indexOfIgnoreCase(field, AS);
					final var fieldNameWithTableName = field.substring(0, asIndex).trim();
					final var lastDotIndex = fieldNameWithTableName.lastIndexOf('.');
					final var secondLastDotIndex = fieldNameWithTableName.lastIndexOf('.', lastDotIndex - 1);
					final var tableShortName = DbUtil.unquote(
							fieldNameWithTableName.substring(secondLastDotIndex + 1, lastDotIndex));
					final var tableName = Kits.STR.nullOrDefaultVal(tableShortNameMap.get(tableShortName), tableShortName);
					final var tableColumnName = DbUtil.unquote(fieldNameWithTableName.substring(lastDotIndex + 1));
					final var tableMeta = tableMap.get(tableName);
					final var tableColumn = tableMeta.getColumn(tableColumnName);
					final var viewColumn = new DbColumn(tableColumn);
					viewColumn.renameTo(DbUtil.unquote(StrKit.subStringAfter(field, AS)));
					viewColumn.setComment(tableMeta.getComment() + tableColumn.getComment());
					viewMeta.add(viewColumn);
				});
			}

			tableMap.put(name, viewMeta);
		}//for views
	}

	@Override
	public void afterPropertiesSet() {
		final var start = System.currentTimeMillis();
		build();
		final var end = System.currentTimeMillis();
		System.out.println("@@@@@@SpringJdbcTableMetaAccessor.build() cost:" + (end - start));
	}

	public void dropCache() {
		if (tableMetaMap != null) {
			tableMetaMap.clear();
		}
	}

	public String getDbName() {
		return dbName;
	}

	@Override
	public Collection<TableMeta> tableMetas() {
		return new ArrayList<>(tableMetaMap.values());
	}
}
