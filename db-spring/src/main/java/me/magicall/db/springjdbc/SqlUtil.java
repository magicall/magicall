/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.Condition;
import me.magicall.db.FieldComparator;
import me.magicall.db.FieldFilter;
import me.magicall.db.meta.DbColumn;
import me.magicall.db.meta.ForeignKey;
import me.magicall.db.meta.TableMeta;
import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.CountSqlConfig;
import me.magicall.db.outsea.FieldNameColumnNameTransformer;
import me.magicall.db.outsea.GetOneSqlConfig;
import me.magicall.db.outsea.ModelMapTransformer;
import me.magicall.db.util.DbOrder;
import me.magicall.db.util.DbUtil;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

class SqlUtil {

	private static final Comparator<String> LENGTH_ASC = Comparator.comparingInt(String::length);

	public static StringBuilder appendWhere(final StringBuilder sb, final TableMeta tableMeta,//
																					final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,//
																					final List<Condition> conditions, final Map<String, Object> namedParams) {
		if (!Kits.COLL.isEmpty(conditions)) {
			sb.append(" WHERE 1=1 ");
			final var columnNames = tableMeta.getColumnNames();
			conditions.forEach(condition -> {
				sb.append(" AND ");
				final var fieldName = condition.getFieldName();
				final var columnName = fieldNameColumnNameTransformer.fieldNameToColumnName(fieldName);
				if (columnNames.contains(columnName)) {
					condition.getConditionOperator()
							.buildSqlUsingColumnName(sb, columnName, (sb1, fieldName1, index, refedValue) -> {
								final var paramedName = fieldName1 + '#' + index;
								namedParams.put(paramedName, refedValue);
								return paramedName;
							}, condition.getRefedValues());
				}
			});
		}
		return sb;
	}

	public static Map<String, ModelMapping> buildModelMappings(final TableMetaAccessor tableMetaAccessor,
																														 final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
//
																														 final GetOneSqlConfig<?> sqlConfig) {
		final var mainModelName = sqlConfig.getMainModelName();
		final var mainTableMeta = tableMetaAccessor.getTableMetaIgnoreCase(mainModelName);
		if (mainTableMeta == null) {
			throw new IllegalArgumentException("model '" + mainModelName + "' has no tableMeta");
		}

		return buildModelMappings(mainModelName, mainTableMeta, sqlConfig.getOtherModelsNames(),
				fieldNameColumnNameTransformer);
	}

	public static Map<String, String> buildSelectFromWhere(final StringBuilder sb, final Map<String, Object> namedParams,
																												 final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
																												 final GetOneSqlConfig<?> sqlConfig,
																												 final Map<String, ModelMapping> modelMappings) {
		final var fieldFilter = sqlConfig.getFieldFilter();
		final var conditions = sqlConfig.getConditions();
		final var mainModelName = sqlConfig.getMainModelName();

		final Map<String, String> resultLabelToFieldNameMapping = new HashMap<>();
		//select
		buildSelect(sb, fieldNameColumnNameTransformer, modelMappings, fieldFilter, resultLabelToFieldNameMapping);
		//from
		appendFrom(sb, modelMappings);
		//where
		appendWhereOfConditions(sb, namedParams, fieldNameColumnNameTransformer, mainModelName, conditions, modelMappings);
		//构建外键条件
		appendWhereOfForeignKey(sb, modelMappings);
		return resultLabelToFieldNameMapping;
	}

	/**
	 *
	 */
	protected static void buildSelect(final StringBuilder sb,
																		final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
																		final Map<String, ModelMapping> modelMappings, final FieldFilter fieldFilter,
																		final Map<String, String> resultLabel_fieldName_mapping) {
		sb.append("SELECT ");
		modelMappings.values().forEach(modelMapping -> {
			final var tableMeta = modelMapping.tableMeta;
			final var columns = tableMeta.getColumns();
			columns.forEach(column -> {
				final var fieldName = fieldNameColumnNameTransformer.columnNameToFieldName(column.name());
				if (fieldFilter == null || fieldFilter.accept(tableMeta, column)) {
					final var asName = //modelMapping.isMainModel ? fieldName : //
							modelMapping.asName + DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR + fieldName;
					sb.append(/* modelMapping.isMainModel ? mainModelName : */modelMapping.asName)//
							.append('.').append(column.name()).append(" AS ")//
							.append(asName).append(',');
					resultLabel_fieldName_mapping.put(asName, modelMapping.modelName + '.' + fieldName);
				}
			});
		});
		sb.deleteCharAt(sb.length() - 1);
	}

	public static <T> void buildSelectCountFromWhere(final StringBuilder sb, final Map<String, Object> namedParams,//
																									 final TableMetaAccessor tableMetaAccessor,
																									 final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
//
																									 final CountSqlConfig<T> sqlConfig) {
		final var mainModelName = sqlConfig.getMainModelName();
		final var mainTableMeta = tableMetaAccessor.getTableMetaIgnoreCase(mainModelName);
		if (mainTableMeta == null) {
			throw new IllegalArgumentException("model '" + mainModelName + "' has no tableMeta");
		}
		final var otherModelsNames = sqlConfig.getOtherModelsNames();
		final var conditions = sqlConfig.getConditions();

		final var modelMappings = buildModelMappings(mainModelName, mainTableMeta, otherModelsNames,
				fieldNameColumnNameTransformer);
		//select
		sb.append("SELECT COUNT(1)");
		//from
		appendFrom(sb, modelMappings);
		//where
		appendWhereOfConditions(sb, namedParams, fieldNameColumnNameTransformer, mainModelName, conditions, modelMappings);
		//构建外键条件
		appendWhereOfForeignKey(sb, modelMappings);
	}

	private static void appendWhereOfConditions(final StringBuilder sb, final Map<String, Object> namedParams,//
																							final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
																							final String mainModelName,//
																							final List<Condition> conditions,
																							final Map<String, ModelMapping> modelMappings) {
		sb.append(" WHERE 1=1 ");
		if (Kits.COLL.isEmpty(conditions)) {
			return;
		}
		for (final var condition : conditions) {
			var fieldName = condition.getFieldName();
			if (!fieldName.startsWith(mainModelName + '.')) {
				fieldName = mainModelName + '.' + fieldName;
			}
			final var dotIndex = fieldName.lastIndexOf('.');
			assert dotIndex > 0;
			final var modelName = fieldName.substring(0, dotIndex);
			final var shortFieldName = fieldName.substring(dotIndex + 1);

			final var modelMapping = modelMappings.get(modelName);
			assert modelMapping != null;

			final var columnName = fieldNameColumnNameTransformer.fieldNameToColumnName(shortFieldName);
			if (containsField(modelMapping.tableMeta, shortFieldName)) {
				//条件中必须用columnName,比较操蛋
				final var resultColumnName = tableNameAs(modelMapping) + '.' + columnName;
				condition.getConditionOperator().buildSqlUsingColumnName(sb.append(" AND "), resultColumnName,
						(sb1, resultColumnName1, index, refedValue) -> {
							final var paramedName = resultColumnName1 + '#' + index;
							namedParams.put(paramedName, refedValue);
							return paramedName;
						}, condition.getRefedValues());
			}
		}
	}

	private static boolean containsField(final TableMeta tableMeta, final String fieldName) {
		return tableMeta.getColumns().stream().anyMatch(column -> javaNameEqualsDbName(fieldName, column.name()));
	}

	private static void appendWhereOfForeignKey(final StringBuilder sb, final Map<String, ModelMapping> modelMappings) {
		modelMappings.values().stream().forEach(modelMapping -> {
			final var foreignKey = modelMapping.foreignKey;
			if (foreignKey != null) {
				sb.append(" AND ")
						//
						.append(modelMapping.asName).append('.').append(foreignKey.getReferencedColumn().name()).append('=')//
						.append(modelMapping.parent.asName)//
						.append('.').append(foreignKey.getReferencingColumn().name());
			}
		});
	}

	private static void appendFrom(final StringBuilder sb, final Map<String, ModelMapping> modelMappings) {
		sb.append(modelMappings.values().stream()
				.map(modelMapping -> modelMapping.tableMeta.name() + " AS " + tableNameAs(modelMapping) + ',')
				.collect(Collectors.joining("", " FROM ", "")));
		sb.deleteCharAt(sb.length() - 1);
	}

	private static String tableNameAs(final ModelMapping modelMapping) {
		return modelMapping.asName;
	}

	private static boolean javaNameEqualsDbName(final String javaName, final String dbName) {
		return javaName.replace("_", "").toLowerCase().equals(dbName.replace("_", "").toLowerCase());
	}

	private static Map<String, ModelMapping> buildModelMappings(final String mainModelName, final TableMeta mainTableMeta,
																															final Collection<String> otherModelsNames,
																															final FieldNameColumnNameTransformer fieldNameColumnNameTransformer) {
		//main model
		final var mainModelMapping = new ModelMapping();
		mainModelMapping.modelName = mainModelName;
		mainModelMapping.tableMeta = mainTableMeta;
		mainModelMapping.asName = mainModelName;//"";
		mainModelMapping.isMainModel = true;
		mainModelMapping.shortModelName = mainModelName;
		//key is modelName
		final Map<String, ModelMapping> modelMappings = new HashMap<>();
		modelMappings.put(mainModelMapping.modelName, mainModelMapping);
		//other models
		//这里用名字长度排一个序,为了确保父model出现在子model之前
		final List<String> otherModelsNames0 = otherModelsNames.stream().sorted(LENGTH_ASC).collect(Collectors.toList());

		buildOtherModelMapping(mainModelName, modelMappings, otherModelsNames0);

		//处理出现在父model之前的model.它们现在还没有parentModel
		//for (final Entry<String, ModelMapping> e : modelMappings.entrySet())
		modelMappings.entrySet().forEach(e -> {
			final var modelMapping = e.getValue();
			if (!modelMapping.isMainModel && modelMapping.parent == null) {
				final var parentModelMapping = modelMappings.get(modelMapping.parentModelName);
				if (parentModelMapping == null) {
					throw new IllegalArgumentException(
							"parent modelName of '" + e.getKey() + "'(" + modelMapping.parentModelName + ") has no tableMeta");
				} else {
					modelMapping.parent = parentModelMapping;
					withParent(modelMapping, parentModelMapping);
				}
			}
		});
		return modelMappings;
	}

	/**
	 *
	 */
	protected static void buildOtherModelMapping(final String mainModelName,
																							 final Map<String, ModelMapping> modelMappings,
																							 final List<String> otherModelsNames0) {
		if (!Kits.LIST.isEmpty(otherModelsNames0)) {
			//service.outDevice service.outDevice.deviceProduct
			// service.outDevice.deviceProduct.deviceType
			//for otherModelsNames
			otherModelsNames0.stream().map(otherModelName -> buildMapping(mainModelName, modelMappings, otherModelName))
					.forEach(modelMapping -> modelMappings.put(modelMapping.modelName, modelMapping));
		}
	}

	protected static void withParent(final ModelMapping modelMapping, final ModelMapping parentModelMapping) {
		final var foreignKey = findForeignKey(parentModelMapping.tableMeta, modelMapping);
		modelMapping.foreignKey = foreignKey;
		modelMapping.tableMeta = foreignKey.getReferencedTable();
		modelMapping.asName = modelMapping.parent.asName + DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR
				+ modelMapping.shortModelName;
	}

	protected static ModelMapping buildMapping(final String mainModelName, final Map<String, ModelMapping> modelMappings,
																						 String otherModelName) {
		final var modelMapping = new ModelMapping();
		//自动加上mainModelName
		if (!otherModelName.startsWith(mainModelName + '.')) {
			otherModelName = mainModelName + '.' + otherModelName;
		}
		modelMapping.modelName = otherModelName;

		final var dotIndex = otherModelName.lastIndexOf('.');
		assert dotIndex >= 0;
		modelMapping.parentModelName = otherModelName.substring(0,
				dotIndex);//service service.outDevice service.outDevice.deviceProduct
		modelMapping.shortModelName = otherModelName.substring(dotIndex + 1);//outDevce deviceProduct deviceType

		modelMapping.foreignKeyColumnName = modelMapping.shortModelName
				+ "Id";// fieldNameColumnNameTransformer.fieldNameToColumnName(modelMapping.shortModelName) + "_id";
		// out_device_id
		final var parentModelMapping = modelMappings.get(modelMapping.parentModelName);
		if (parentModelMapping != null) {
			modelMapping.parent = parentModelMapping;
			final var parentTableMeta = parentModelMapping.tableMeta;
			if (parentTableMeta != null) {
				withParent(modelMapping, parentModelMapping);
			}
		}
		return modelMapping;
	}

	private static ForeignKey findForeignKey(final TableMeta parentTableMeta, final ModelMapping modelMapping) {
		final var foreignKeys = parentTableMeta.getForeignKeys();
		for (final var foreignKey : foreignKeys) {
			if (javaNameEqualsDbName(modelMapping.foreignKeyColumnName, foreignKey.getReferencingColumn().name())) {
				return foreignKey;
			}
		}
		assert false;
		return null;
	}

	public static <T> T handlerResultMap(final Map<String, String> resultLabelToFieldNameMapping,//
																			 final String mainModelName, final Map<String, Object> resultMap,
																			 final ModelMapTransformer<T> modelMapTransformer) {
		return modelMapTransformer.mapToModel(resultMap, mainModelName);
	}

	public static Object getValue(final DbColumn column, final ResultSet resultSet, final int i) throws SQLException {
		if (column == null) {
			return resultSet.getObject(i);
		}
		final var type = column.getType();
		if (type == null) {
			return resultSet.getObject(i);
		}
		final var kit = type.getKit();
		if (kit == null) {
			return resultSet.getObject(i);
		}
		final var string = resultSet.getString(i);
		if (column.getNullable() && string == null) {
			return null;
		}
		return kit.fromString(string);
	}

	public static <T> Map<String, Object> mapRow(final TableMetaAccessor tableMetaAccessor,//
																							 final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
																							 final GetOneSqlConfig<T> sqlConfig,//
																							 final Map<String, String> resultLabelToFieldNameMapping,
																							 final Map<String, ModelMapping> modelNameToModelMapping,//
																							 final ResultSet rs) throws SQLException {
		final var resultSetMetaData = rs.getMetaData();
		final var columnCount = resultSetMetaData.getColumnCount();
		final Map<String, Object> rt = new HashMap<>();
		for (var i = DbUtil.RESULT_SET_COLUMN_START_INDEX; i <= columnCount; ++i) {
			final var label = resultSetMetaData.getColumnLabel(i);
			final var fieldName = resultLabelToFieldNameMapping.get(label);

			final var dotIndex = fieldName.lastIndexOf('.');
			assert dotIndex >= 0;
			final var modelName = fieldName.substring(0, dotIndex);
			final var modelMapping = modelNameToModelMapping.get(modelName);

			final var tableMeta = modelMapping.tableMeta;
			if (tableMeta == null) {
				System.out.println("@@@@@@SqlUtil.mapRow():" + label);
			}
			final var columnName = StrKit.subStringAfterLastSeq(//
					fieldNameColumnNameTransformer.fieldNameToColumnName(label), DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR);

			final var value = getValue(tableMeta.getColumn(columnName), rs, i);

			rt.put(fieldName, value);
		}
		return rt;
	}

	public static StringBuilder appendOrderBy(final StringBuilder sb, final FieldComparator<?> fieldComparator,
																						final String mainModelName,//
																						final FieldNameColumnNameTransformer fieldNameColumnNameTransformer) {
		if (fieldComparator != null) {
			final var comparingFieldsNamesAndOrders = fieldComparator.getComparingFieldsNamesAndOrders();
			if (!Kits.COLL.isEmpty(comparingFieldsNamesAndOrders)) {
				sb.append(" ORDER BY ");
				comparingFieldsNamesAndOrders.forEach(t -> {
					final var fieldName = t.first;
					final var order = t.second;
					sb.append(mainModelName).append('.').append(fieldNameColumnNameTransformer.fieldNameToColumnName(fieldName))
							.append(' ');
					sb.append(Objects.requireNonNullElse(order, DbOrder.ASC).toSql()).append(',');
				});
				sb.deleteCharAt(sb.length() - 1);
			}
		}
		return sb;
	}

	public static class ModelMapping {
		String modelName;//全名
		TableMeta tableMeta;
		ModelMapping parent;
		String parentModelName;//全名
		ForeignKey foreignKey;
		String asName;//全名
		String shortModelName;
		String foreignKeyColumnName;
		boolean isMainModel;
	}
}
