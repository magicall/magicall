/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.GetOneSqlConfig;
import me.magicall.db.outsea.ModelMapTransformer;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;
import me.magicall.db.springjdbc.SqlUtil.ModelMapping;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.HashMap;
import java.util.Map;

public class GetOne<T> extends AbsDataAccessor<T, T, GetOneSqlConfig<T>> {

	private static final ThreadLocal<Map<String, String>> LOCAL_ResultLabel_TO_FieldName_MAPPING = new ThreadLocal<>();
	private static final ThreadLocal<Map<String, ModelMapping>> LOCAL_modelName_TO_ModelMapping_MAPPING
			= new ThreadLocal<>();

	private final SqlBuilder<GetOneSqlConfig<T>> sqlBuilder = sqlConfig -> {
		final var sb = new StringBuilder();

		final var modelNameToModelMapping = SqlUtil.buildModelMappings(tableMetaAccessor, fieldNameColumnNameTransformer,
				sqlConfig);
		LOCAL_modelName_TO_ModelMapping_MAPPING.set(modelNameToModelMapping);
		//select from where
		final Map<String, Object> namedParams = new HashMap<>();
		final var resultLabelToFieldNameMapping = SqlUtil.buildSelectFromWhere(sb, namedParams,
				fieldNameColumnNameTransformer, sqlConfig, modelNameToModelMapping);
		LOCAL_ResultLabel_TO_FieldName_MAPPING.set(resultLabelToFieldNameMapping);

		return new ParamedSqlAndParams(sb.toString(), namedParams);
	};

	public GetOne(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	@Override
	public GetOneSqlConfig<T> createSqlConfig(final String mainModelName) {
		return new GetOneSqlConfig<>(mainModelName);
	}

	@Override
	protected T exe(final String sql, final Map<String, ?> params, final GetOneSqlConfig<T> sqlConfig) {
		final var resultLabelToFieldNameMapping = LOCAL_ResultLabel_TO_FieldName_MAPPING.get();
		final var modelNameToModelMapping = LOCAL_modelName_TO_ModelMapping_MAPPING.get();
		try {
			final var resultMap = namedJdbc.queryForObject(sql, params,
					(rs, rowNum) -> SqlUtil.mapRow(tableMetaAccessor, fieldNameColumnNameTransformer,//
							sqlConfig, resultLabelToFieldNameMapping, modelNameToModelMapping, rs));
			return SqlUtil.handlerResultMap(resultLabelToFieldNameMapping, sqlConfig.getMainModelName(), resultMap,
					modelMapTransformer);
		} catch (final EmptyResultDataAccessException ignored) {
			return null;
		}
	}

	@Override
	protected SqlBuilder<GetOneSqlConfig<T>> getSqlBuilder() {
		return sqlBuilder;
	}

	@Override
	public ModelMapTransformer<T> getModelMapTransformer() {
		return super.getModelMapTransformer();
	}

	@Override
	public void setModelMapTransformer(final ModelMapTransformer<T> modelMapTransformer) {
		super.setModelMapTransformer(modelMapTransformer);
	}
}
