/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.program.lang.java.贵阳DearSun.BeanKit;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;

import java.lang.reflect.Method;
import java.time.Instant;
import java.util.Date;

public class __Helper {
	private static Method findGetter(final String fieldName, final Class<?> modelClass) {
		Method getter;
		try {
			getter = modelClass.getMethod(BeanKit.toGetterName(fieldName));
		} catch (final NoSuchMethodException e) {
			getter = null;
		}
		return getter;
	}

	public static Object checkInstant(final Object rawVal, final String fieldName, final Class<?> modelClass) {
		//对jdk8的java.time包特殊支持。
		final var getter = findGetter(fieldName, modelClass);
		if (getter != null && Instant.class == getter.getReturnType()) {
			if (rawVal instanceof Date) {
				final var date = (Date) rawVal;
				return Instant.ofEpochMilli(date.getTime());
			}
			if (rawVal instanceof String) {
				return Instant.parse((String) rawVal);
			}
			throw new WrongStatusException(fieldName, rawVal, Instant.class);
		}
		return rawVal;
	}
}
