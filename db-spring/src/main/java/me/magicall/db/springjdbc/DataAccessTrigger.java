/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import me.magicall.db.outsea.SqlConfig;
import me.magicall.db.springjdbc.SqlBuilder.ParamedSqlAndParams;

public interface DataAccessTrigger<R, C extends SqlConfig> {

	void before(C sqlConfig);

	void beforeExe(C sqlConfig, ParamedSqlAndParams paramedSqlAndParams);

	void afterExe(R result, C sqlConfig);

	class DataAccessTriggerAdaptor<R, C extends SqlConfig> implements DataAccessTrigger<R, C> {

		private static final DataAccessTriggerAdaptor<Object, SqlConfig> EMPTY = new DataAccessTriggerAdaptor<>();

		@SuppressWarnings("unchecked")
		public static <R, C extends SqlConfig> DataAccessTriggerAdaptor<R, C> empty() {
			return (DataAccessTriggerAdaptor<R, C>) EMPTY;
		}

		@Override
		public void before(final C sqlConfig) {
		}

		@Override
		public void beforeExe(final C sqlConfig, final ParamedSqlAndParams paramedSqlAndParams) {
		}

		@Override
		public void afterExe(final R result, final C sqlConfig) {
		}
	}
}
