/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import com.google.common.collect.Maps;
import me.magicall.program.lang.LabelStyle;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 这个类可能跟AbsDataAccessor重了。
 */
public abstract class AbsDbRepoUsingJdbc<T> {
	protected final JdbcOperations jdbcOperations;

	public AbsDbRepoUsingJdbc(final JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}

	protected String getSelectListSql() {
		return "SELECT * FROM " + getTableName();
	}

	protected String getSelectOneSql() {
		return "SELECT * FROM " + getTableName() + " WHERE id=?";
	}

	protected String getDeleteSql() {
		return "DELETE FROM " + getTableName() + " WHERE id=?";
	}

	public List<T> list() {
		return jdbcOperations.queryForList(getSelectListSql()).stream()//
				.map(this::toVo).collect(Collectors.toList());
	}

	public T findBy(final String id) {
		return toVo(jdbcOperations.queryForMap(getSelectOneSql(), id));
	}

	public void del(final String id) {
		jdbcOperations.update(getDeleteSql(), id);
	}

	protected static String dbNameToModelName(final String dbName) {
		return LabelStyle.UNDERLINE_SEPARATED.convertTo(LabelStyle.CAMEL, dbName);
	}

	protected Map<String, Object> mapRow(final Map<String, Object> row) {
		final Map<String, Object> rt = Maps.newHashMapWithExpectedSize(row.size());
		row.forEach((colName, rawVal) -> {
			final var fieldName = dbNameToModelName(colName);
			final var val = __Helper.checkInstant(rawVal, fieldName, getModelClass());
			rt.put(fieldName, val);
		});
		return rt;
	}

	protected T toVo(final Map<String, Object> row) {
		return fromMap(mapRow(row));
	}

	protected abstract Class<T> getModelClass();

	protected abstract String getTableName();

	protected abstract T fromMap(final Map<String, Object> voMap);
}
