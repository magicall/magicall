/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import me.magicall.db.meta.DbColumn;
import me.magicall.db.meta.ForeignKey;
import me.magicall.db.meta.TableMeta;
import me.magicall.db.meta.TableMetaAccessor;
import me.magicall.db.outsea.GetOneSqlConfig;
import me.magicall.db.util.DbUtil;
import me.magicall.program.lang.LabelStyle;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class AbsGet<T, R, C extends GetOneSqlConfig<T>> extends AbsDataAccessor<T, R, C> {

	public AbsGet() {
		super();
	}

	public AbsGet(final NamedParameterJdbcOperations namedJdbc, final TableMetaAccessor tableMetaAccessor) {
		super(namedJdbc, tableMetaAccessor);
	}

	protected Map<String, String> buildSelectFromWhere(final StringBuilder sb, final Map<String, Object> namedParams,
																										 final C sqlConfig) {
		final var builder = new SqlBuilderForGet(tableMetaAccessor, sqlConfig, namedParams);
		return builder.buildSql(sb);
	}

	/**
	 * 确保名字是以mainModelName.开头的
	 *
	 * @return
	 */
	protected String ensureStartsWithMainModelName(final String name, final GetOneSqlConfig<T> sqlConfig) {
		final var mainModelName = sqlConfig.getMainModelName();
		final var mainModelNameDot = mainModelName + '.';
		if (name.startsWith(mainModelNameDot)) {
			return name;
		} else {
			return mainModelNameDot + name;
		}
	}

	protected static class Mapping {
		String fullName;
		String shortName;
		String label;
		TableMeta tableMeta;
		String tableName;

		String containerModelName;
	}

	private static class As {
		String left;
		String right;

		public As(final String left, final String right) {
			super();
			this.left = left;
			this.right = right;
		}

		@Override
		public String toString() {
			return left.equals(right) ? left : left + ' ' + right;
		}
	}

	protected class SqlBuilderForGet {

		private final TableMetaAccessor tableMetaAccessor;
		private final GetOneSqlConfig<T> sqlConfig;

		private final Map<String, Mapping> modelFullNameMappings = Maps.newHashMap();
		private final Map<String, Object> namedParamValueMap;
		private Collection<String> modelFullNames;

		public SqlBuilderForGet(final TableMetaAccessor tableMetaAccessor,//
														final GetOneSqlConfig<T> sqlConfig,//
														final Map<String, Object> namedParamValueMap) {
			super();
			this.tableMetaAccessor = tableMetaAccessor;
			this.sqlConfig = sqlConfig;
			this.namedParamValueMap = namedParamValueMap;
		}

		public Map<String, String> buildSql(final StringBuilder sb) {
			sortModelNames();
			buildMappings();
			final var rt = buildSelect(sb);
			buildFrom(sb);
			buildWhere(sb);
			return rt;
		}

		void sortModelNames() {
			//按照名字从短到长排序,则父model肯定在子model之前
			final var otherModelNames = sqlConfig.getOtherModelsNames();
			final List<String> list = new ArrayList<>(otherModelNames.size() + 1);
			final var mainModelName = sqlConfig.getMainModelName();
			list.add(mainModelName);
			otherModelNames.stream().map(otherModelName -> ensureStartsWithMainModelName(otherModelName, sqlConfig))
					.forEach(list::add);
			list.sort(Comparator.comparing(String::length));
			modelFullNames = list;
		}

		private void addMapping(final String fullName) {
			final var mapping = new Mapping();
			mapping.fullName = fullName;
			final var lastDotIndex = fullName.lastIndexOf('.');
			if (lastDotIndex < 0) {
				mapping.shortName = fullName;
			} else {
				mapping.shortName = fullName.substring(lastDotIndex + 1);
			}
			mapping.label = convertToLabel(fullName);

			final var containerModelName = getContainerName(fullName);
			if (containerModelName == null) {
				mapping.tableMeta = tableMetaAccessor.getTableMetaIgnoreCase(fullName);
			} else {
				mapping.containerModelName = containerModelName;
				final var container = modelFullNameMappings.get(containerModelName);
				mapping.tableMeta = findRefedTableMeta(container.tableMeta, mapping.shortName);
			}

			mapping.tableName = DbUtil.javaNameToDbName(mapping.shortName);

			modelFullNameMappings.put(fullName, mapping);
		}

		void buildMappings() {
			modelFullNames.forEach(this::addMapping);
		}

		private String convertToLabel(final String fullName) {
			return fullName.replace('.', DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR_CHAR);
		}

		Map<String, String> buildSelect(final StringBuilder sb) {
			final List<As> as = Lists.newLinkedList();
			//main model
			final var mainModelName = sqlConfig.getMainModelName();
			final var mainModelMapping = modelFullNameMappings.get(mainModelName);
			final var mainModelTableMeta = mainModelMapping.tableMeta;
			final var mainModelColumns = mainModelTableMeta.getColumns();
			mainModelColumns.stream().map(DbColumn::name).forEach(columnName -> {
				final var fieldName = DbUtil.dbNameToJavaName(columnName);
				as.add(new As(mainModelMapping.fullName + '.' + columnName, fieldName));
			});

			//other models
			modelFullNames.stream().filter(modelFullName -> !modelFullName.equals(mainModelName))
					.map(modelFullNameMappings::get).forEach(mapping -> {
						final var containerModelName = getContainerName(mapping.fullName);
						final var containerMapping = modelFullNameMappings.get(containerModelName);
						final var tableMeta = findRefedTableMeta(containerMapping.tableMeta, mapping.shortName);
						if (tableMeta != null) {
							final var columns = tableMeta.getColumns();
							columns.stream().map(DbColumn::name).forEach(columnName -> {
								final var fieldName = DbUtil.dbNameToJavaName(columnName);
								as.add(new As(mapping.tableName + '.' + columnName,//
										mapping.label + DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR + fieldName));
							});
						}
					});

			sb.append(as.stream().map(a -> String.valueOf(a) + ',').collect(Collectors.joining("", " SELECT ", "")));
			sb.deleteCharAt(sb.length() - 1);

			final Map<String, String> rt = as.stream().collect(
					Collectors.toMap(a -> a.right, a -> a.right.replace(DbUtil.TABLE_NAME_COLUMN_NAME_SEPARATOR_CHAR, '.'),
							(a1, b) -> b));
			return rt;
		}

		void buildFrom(final StringBuilder sb) {
			//main model
			final var mainModelName = sqlConfig.getMainModelName();
			final var mainModelMapping = modelFullNameMappings.get(mainModelName);
			sb.append(" FROM ").append(new As(mainModelMapping.tableName, mainModelName));
			//other models
			modelFullNames.stream().filter(modelName -> !modelName.equals(mainModelName)).map(modelFullNameMappings::get)
					.forEach(mapping -> {
						final var tableMeta = mapping.tableMeta;
						final var containerModelName = getContainerName(mapping.fullName);
						final var containerTableMeta = modelFullNameMappings.get(containerModelName).tableMeta;
						final var foreignKey = findForeignKey(tableMeta, containerTableMeta);
						final var containerMapping = modelFullNameMappings.get(containerModelName);
						sb.append(" LEFT JOIN ")//
								.append(new As(tableMeta.name(), mapping.label))//
								.append(" ON ")//
								.append(containerMapping.label).append('.')//
								.append(foreignKey.getReferencingColumn().name())//
								.append('=')//
								.append(mapping.label).append('.').append(foreignKey.getReferencedColumn().name());
					});
		}

		private String getContainerName(final String modelFullName) {
			final var lastDotIndex = modelFullName.lastIndexOf('.');
			if (lastDotIndex < 0) {
				return null;
			}
			return modelFullName.substring(0, lastDotIndex);
		}

		private TableMeta findRefedTableMeta(final TableMeta tableMeta, final String modelName) {
			final var foreignKeys = tableMeta.getForeignKeys();
			if (!Kits.COLL.isEmpty(foreignKeys)) {
				return foreignKeys.stream()
						.filter(foreignKey -> isSame(modelName + "Id", foreignKey.getReferencingColumn().name())).findFirst()
						.map(ForeignKey::getReferencedTable).orElse(null);
			}
			return null;
		}

		private ForeignKey findForeignKey(final TableMeta refedTableMeta, final TableMeta refingtableMeta) {
			final var foreignKeys = refingtableMeta.getForeignKeys();
			if (Kits.COLL.isEmpty(foreignKeys)) {
				throw new RuntimeException("no foreign key in `" + refingtableMeta + '`');
			}
			for (final var foreignKey : foreignKeys) {
				if (foreignKey.getReferencedTable().equals(refedTableMeta)) {
					return foreignKey;
				}
			}
			throw new RuntimeException("no foreign key from `" + refingtableMeta + "` to `" + refedTableMeta + '`');
		}

		void buildWhere(final StringBuilder sb) {
			final var conditions = sqlConfig.getConditions();
			if (!Kits.COLL.isEmpty(conditions)) {
				final var mainModelName = sqlConfig.getMainModelName();
				sb.append(" WHERE 1=1 ");
				for (final var condition : conditions) {
					var fieldName = condition.getFieldName();
					if (!fieldName.startsWith(mainModelName + '.')) {
						fieldName = mainModelName + '.' + fieldName;
					}
					final var dotIndex = fieldName.lastIndexOf('.');
					assert dotIndex > 0;
					final var modelName = fieldName.substring(0, dotIndex);
					final var shortFieldName = fieldName.substring(dotIndex + 1);

					final var modelMapping = modelFullNameMappings.get(modelName);
					assert modelMapping != null;

					final var columnName = DbUtil.javaNameToDbName(shortFieldName);
					if (containsField(modelMapping.tableMeta, shortFieldName)) {
						//条件中必须用columnName,比较操蛋
						final var resultColumnName = modelMapping.label + '.' + columnName;
						condition.getConditionOperator().buildSqlUsingColumnName(sb.append(" AND "), resultColumnName,
								(sb1, resultColumnName1, index, refedValue) -> {
									final var paramedName = resultColumnName1 + '#' + index;
									namedParamValueMap.put(paramedName, refedValue);
									return paramedName;
								}, condition.getRefedValues());
					}
				}
			}
		}

		private boolean containsField(final TableMeta tableMeta, final String fieldName) {
			return tableMeta.getColumns().stream().anyMatch(column -> isSame(fieldName, column.name()));
		}

		private boolean isSame(final String fieldName, final String dbName) {
			final var dbNameParts = splitDbName(dbName);
			final var javaNameParts = splitJavaName(fieldName);
			if (dbNameParts.length != javaNameParts.length) {
				return false;
			}
			return IntStream.range(0, dbNameParts.length).allMatch(i -> dbNameParts[i].equalsIgnoreCase(javaNameParts[i]));
		}

		private String[] splitDbName(final String name) {
			return LabelStyle.UNDERLINE_SEPARATED.splitWords(name);
		}

		private String[] splitJavaName(final String name) {
			return LabelStyle.CAMEL.splitWords(name);
		}

		public GetOneSqlConfig<?> getSqlConfig() {
			return sqlConfig;
		}
	}
}
