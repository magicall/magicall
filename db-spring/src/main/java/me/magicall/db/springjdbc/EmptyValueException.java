/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.springjdbc;

import org.springframework.dao.DataAccessException;

import java.io.Serial;

public class EmptyValueException extends DataAccessException {

	@Serial
	private static final long serialVersionUID = -4149138921170993933L;

	public final Object item;

	public EmptyValueException(final Object item) {
		super(item + "不能为空");
		this.item = item;
	}

	public EmptyValueException(final Object item, final Throwable cause) {
		super(item + "不能为空", cause);
		this.item = item;
	}
}
