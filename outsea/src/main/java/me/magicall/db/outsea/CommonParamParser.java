/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import com.google.common.collect.Lists;
import me.magicall.db.Condition;
import me.magicall.db.ConditionOperator;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public enum CommonParamParser implements ParamParser {
	NULL_VALUE {
		@Override
		public boolean accept(final String paramName, final Object paramValue) {
			return "null".equalsIgnoreCase(String.valueOf(paramValue));
		}

		@Override
		public Condition parse(final String paramName, final Object paramValue) {
			return new Condition(paramName, ConditionOperator.IS, Lists.newArrayList((Object) null));
		}
	},
	NOT_NULL_VALUE {
		@Override
		public boolean accept(final String paramName, final Object paramValue) {
			return "notnull".equalsIgnoreCase(String.valueOf(paramValue));
		}

		@Override
		public Condition parse(final String paramName, final Object paramValue) {
			return new Condition(paramName, ConditionOperator.IS_NOT, Lists.newArrayList((Object) null));
		}
	},
	ARR_VALUE {
		@Override
		public boolean accept(final String paramName, final Object paramValue) {
			return paramValue instanceof Object[] || paramValue.getClass().isArray();
		}

		@Override
		public Condition parse(final String paramName, final Object paramValue) {
			if (paramValue instanceof Object[]) {
				return new Condition(paramName, ConditionOperator.IN, Lists.newArrayList((Object[]) paramValue));
			}
			//基本类型数组
			final var len = Array.getLength(paramValue);
			final List<Object> list = IntStream.range(0, len).mapToObj(i -> Array.get(paramValue, i))
					.collect(Collectors.toCollection(() -> Lists.newArrayListWithExpectedSize(len)));
			return new Condition(paramName, ConditionOperator.IN, list);
		}
	},
	COLL_VALUE {
		@Override
		public boolean accept(final String paramName, final Object paramValue) {
			return paramValue instanceof Iterable<?>;
		}

		@Override
		public Condition parse(final String paramName, final Object paramValue) {
			final List<Object> list = new LinkedList<>();
			final var iterable = (Iterable<?>) paramValue;
			for (final Object name2 : iterable) {
				list.add(name2);
			}
			return new Condition(paramName, ConditionOperator.IN, list);
		}
	},
	;
}
