/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.util.DbUtil;

public enum CommonFieldNameColumnNameTransformer implements FieldNameColumnNameTransformer {

	COMMON {
		@Override
		public String fieldNameToColumnName(final String fieldName) {
			return DbUtil.javaNameToDbName(fieldName);
		}

		@Override
		public String columnNameToFieldName(final String columnName) {
			return DbUtil.dbNameToJavaName(columnName);
		}
	},
	SAME_NAME {
		@Override
		public String fieldNameToColumnName(final String fieldName) {
			return fieldName;
		}

		@Override
		public String columnNameToFieldName(final String columnName) {
			return columnName;
		}
	},
	;
}
