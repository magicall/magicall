/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.util.PageInfo;

public class ListSqlConfig<T> extends GetOneSqlConfig<T> {

	public ListSqlConfig(final String mainModelName) {
		super(mainModelName);
	}

	@Override
	public PageInfo getPageInfo() {
		return super.getPageInfo();
	}

	@Override
	public void setPageInfo(final PageInfo pageInfo) {
		super.setPageInfo(pageInfo);
	}
}
