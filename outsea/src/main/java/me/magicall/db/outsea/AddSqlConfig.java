/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.FieldFilter;
import me.magicall.db.util.HandleNullValueStrategy;
import me.magicall.db.util.OptionOnExist;

import java.util.Collection;
import java.util.List;

public class AddSqlConfig<T> extends AbsSqlConfig<T> {

	private OptionOnExist optionOnExist = OptionOnExist.EXCEPTION;

	public AddSqlConfig(final String mainModelName) {
		super(mainModelName);
	}

	@Override
	public HandleNullValueStrategy getHandleNullValueStrategy() {
		return super.getHandleNullValueStrategy();
	}

	@Override
	public void setHandleNullValueStrategy(final HandleNullValueStrategy handleNullValueStrategy) {
		super.setHandleNullValueStrategy(handleNullValueStrategy);
	}

	@Override
	public T getRefedModel() {
		return super.getRefedModel();
	}

	@Override
	public void setRefedModel(final T refedModel) {
		super.setRefedModel(refedModel);
	}

	@Override
	public void addOtherRefedModels(final List<T> otherRefedModels) {
		super.addOtherRefedModels(otherRefedModels);
	}

	@SafeVarargs
	@Override
	public final void addOtherRefedModels(final T... otherRefedModels) {
		super.addOtherRefedModels(otherRefedModels);
	}

	@SafeVarargs
	@Override
	public final void setRefedModels(final T refedModel, final T... otherRefedModels) {
		super.setRefedModels(refedModel, otherRefedModels);
	}

	@Override
	public List<T> getRefedModels() {
		return super.getRefedModels();
	}

	@SafeVarargs
	@Override
	public final void setRefedModels(final T... refedModels) {
		super.setRefedModels(refedModels);
	}

	@Override
	public void setRefedModels(final Collection<? extends T> refedModels) {
		super.setRefedModels(refedModels);
	}

	public OptionOnExist getOptionOnExist() {
		return optionOnExist;
	}

	public void setOptionOnExist(final OptionOnExist optionOnExist) {
		this.optionOnExist = optionOnExist;
	}

	@Override
	public List<T> getOtherNewValues() {
		return super.getOtherNewValues();
	}

	@Override
	public FieldFilter getFieldFilter() {
		return super.getFieldFilter();
	}

	@Override
	public void setFieldFilter(final FieldFilter fieldFilter) {
		super.setFieldFilter(fieldFilter);
	}
}
