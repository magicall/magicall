/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.util.DbUtil;

public enum CommonModelNameTableNameTransformer implements ModelNameTableNameTransformer {

	COMMON {
		@Override
		public String modelNameToTableName(final String modelName) {
			return DbUtil.javaNameToDbName(modelName);
		}

		@Override
		public String tableNameToModelName(final String tableName) {
			return DbUtil.dbNameToJavaName(tableName);
		}
	},
	SAME_NAME {
		@Override
		public String modelNameToTableName(final String modelName) {
			return modelName;
		}

		@Override
		public String tableNameToModelName(final String tableName) {
			return tableName;
		}
	},
	;
}
