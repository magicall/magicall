/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

public enum CommonNameTransformer implements ModelNameTableNameTransformer, FieldNameColumnNameTransformer {
	COMMON(CommonFieldNameColumnNameTransformer.COMMON, CommonModelNameTableNameTransformer.COMMON),
	SAME_NAME(CommonFieldNameColumnNameTransformer.SAME_NAME, CommonModelNameTableNameTransformer.SAME_NAME),
	;
	private final FieldNameColumnNameTransformer fieldNameColumnNameTransformer;
	private final ModelNameTableNameTransformer modelNameTableNameTransformer;

	CommonNameTransformer(final FieldNameColumnNameTransformer fieldNameColumnNameTransformer,
												final ModelNameTableNameTransformer modelNameTableNameTransformer) {
		this.fieldNameColumnNameTransformer = fieldNameColumnNameTransformer;
		this.modelNameTableNameTransformer = modelNameTableNameTransformer;
	}

	@Override
	public String modelNameToTableName(final String modelName) {
		return modelNameTableNameTransformer.modelNameToTableName(modelName);
	}

	@Override
	public String tableNameToModelName(final String tableName) {
		return modelNameTableNameTransformer.tableNameToModelName(tableName);
	}

	@Override
	public String fieldNameToColumnName(final String fieldName) {
		return fieldNameColumnNameTransformer.fieldNameToColumnName(fieldName);
	}

	@Override
	public String columnNameToFieldName(final String columnName) {
		return fieldNameColumnNameTransformer.columnNameToFieldName(columnName);
	}
}
