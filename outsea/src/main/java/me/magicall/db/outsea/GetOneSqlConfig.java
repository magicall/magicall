/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.Condition;
import me.magicall.db.FieldComparator;
import me.magicall.db.FieldFilter;
import me.magicall.db.util.HandleNullValueStrategy;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class GetOneSqlConfig<T> extends AbsSqlConfig<T> {

	public GetOneSqlConfig(final String mainModelName) {
		super(mainModelName);
	}

	@Override
	public FieldComparator<? super T> getFieldComparator() {
		return super.getFieldComparator();
	}

	@Override
	public void setFieldComparator(final FieldComparator<? super T> fieldComparator) {
		super.setFieldComparator(fieldComparator);
	}

	@Override
	public List<Condition> getConditions() {
		return super.getConditions();
	}

	public void addConditions(final String key, final Object value) {
		super.addConditions(new Condition(key, value));
	}

	@Override
	public void addConditions(final Condition... conditions) {
		super.addConditions(conditions);
	}

	@Override
	public void addConditions(final Collection<? extends Condition> conditions) {
		super.addConditions(conditions);
	}

	@Override
	public HandleNullValueStrategy getHandleNullValueStrategy() {
		return super.getHandleNullValueStrategy();
	}

	@Override
	public void setHandleNullValueStrategy(final HandleNullValueStrategy handleNullValueStrategy) {
		super.setHandleNullValueStrategy(handleNullValueStrategy);
	}

	@Override
	public Collection<String> getOtherModelsNames() {
		return super.getOtherModelsNames();
	}

	@Override
	public void addOtherModelsNames(final String... otherModelsNames) {
		super.addOtherModelsNames(otherModelsNames);
	}

	@Override
	public FieldFilter getFieldFilter() {
		return super.getFieldFilter();
	}

	@Override
	public void setFieldFilter(final FieldFilter fieldFilter) {
		super.setFieldFilter(fieldFilter);
	}

	@Override
	public void addConditions(final Map<String, ?> params) {
		super.addConditions(params);
	}
}
