/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.outsea;

import me.magicall.db.Condition;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CountSqlConfig<T> extends AbsSqlConfig<T> {

	public CountSqlConfig(final String mainModelName) {
		super(mainModelName);
	}

	@Override
	public List<Condition> getConditions() {
		return super.getConditions();
	}

	@Override
	public void addConditions(final Condition... conditions) {
		super.addConditions(conditions);
	}

	@Override
	public void addConditions(final Collection<? extends Condition> conditions) {
		super.addConditions(conditions);
	}

	@Override
	public Collection<String> getOtherModelsNames() {
		return super.getOtherModelsNames();
	}

	@Override
	public void addOtherModelsNames(final Collection<String> otherModelsNames) {
		super.addOtherModelsNames(otherModelsNames);
	}

	@Override
	public void addOtherModelsNames(final String... otherModelsNames) {
		super.addOtherModelsNames(otherModelsNames);
	}

	@Override
	public void addConditions(final Map<String, ?> params) {
		super.addConditions(params);
	}
}
