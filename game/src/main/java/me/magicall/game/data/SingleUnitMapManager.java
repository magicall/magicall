/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import me.magicall.game.map.Coordinate;
import me.magicall.game.map.NoSuchUnitException;
import me.magicall.game.unit.Unit;
import me.magicall.game.unit.UnitsHolder;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

/**
 * 地图上每个坐标最多只有一个单位的{@link UnitsHolder}。
 *
 * @author MaGiCalL
 */
public class SingleUnitMapManager<C extends Coordinate, U extends Unit> implements UnitsHolder<C, U> {

	private final Map<C, U> unitsInMap;

	public SingleUnitMapManager(final Map<C, U> unitsInMap) {
		this.unitsInMap = unitsInMap;
	}

	public SingleUnitMapManager() {
		this(Maps.newHashMap());
	}

	@Override
	public Collection<U> getUnits(final C coordinate) {
		final var unit = unitsInMap.get(coordinate);
		if (unit == null) {
			return Kits.LIST.emptyVal();
		} else {
			return Kits.LIST.cast(Lists.newArrayList(unit));
		}
	}

	@Override
	public U getFirstUnit(final C coordinate) {
		return unitsInMap.get(coordinate);
	}

	@Override
	public Collection<U> getUnits() {
		return unitsInMap.values();
	}

	@Override
	public C getCoordinate(final U unit) {
		return unitsInMap.entrySet().stream().filter(entry -> unit.equals(entry.getValue())).findFirst().map(Entry::getKey)
				.orElse(null);
	}

	@Override
	public void addUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		if (units.size() > 1) {
			throw new RuntimeException();//TODO
		}
		unitsInMap.computeIfAbsent(coordinate, k -> units.iterator().next());
	}

	@SafeVarargs
	@Override
	public final void addUnits(final C coordinate, final U... units) throws NoSuchUnitException {
		if (units.length > 1) {
			throw new RuntimeException();//TODO
		}
		unitsInMap.computeIfAbsent(coordinate, k -> units[0]);
	}

	@Override
	public void removeUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		final Unit unit = unitsInMap.get(coordinate);
		if (unit == null) {
			return;
		}
		if (units.contains(unit)) {
			removeAllUnits(coordinate);
		}
	}

	@Override
	public void removeAllUnits(final C coordinate) {
		unitsInMap.remove(coordinate);
	}

	public void setUnit(final C coordinate, final U unit) {
		unitsInMap.put(coordinate, unit);
	}
}
