/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import java.util.Collection;
import java.util.Map.Entry;

import me.magicall.game.map.Coordinate;
import me.magicall.game.map.NoSuchUnitException;
import me.magicall.game.unit.Unit;
import me.magicall.game.unit.UnitsHolder;

/**
 * <pre>
 * {@link UnitsHolder}的通用实现类。
 * </pre>
 */
public class CommonUnitsHolder<C extends Coordinate, U extends Unit> implements UnitsHolder<C, U> {

	private final Multimap<C, U> unitsInMap;

	public CommonUnitsHolder() {
		unitsInMap = MultimapBuilder.hashKeys().linkedHashSetValues().build();
	}

	public CommonUnitsHolder(final int coordinatesCount) {
		unitsInMap = MultimapBuilder.hashKeys(coordinatesCount).linkedHashSetValues().build();
	}

	@Override
	public Collection<U> getUnits(final C coordinate) {
		return unitsInMap.get(coordinate);
	}

	@Override
	public Collection<U> getUnits() {
		return Lists.newArrayList(unitsInMap.values());
	}

	@Override
	public C getCoordinate(final U unit) {
		return unitsInMap.entries().stream().filter(entry -> unit.equals(entry.getValue())).findFirst().map(Entry::getKey)
				.orElse(null);
	}

	@Override
	public void addUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		unitsInMap.putAll(coordinate, units);
	}

	@Override
	public void removeUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		unitsInMap.get(coordinate).removeAll(units);
	}

	@Override
	public void removeAllUnits(final C coordinate) {
		unitsInMap.removeAll(coordinate);
	}
}
