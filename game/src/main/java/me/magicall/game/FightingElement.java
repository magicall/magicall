/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game;

import me.magicall.game.event.EventTarget;
import me.magicall.game.event.EventTrigger;

/**
 * 一场游戏中的元素。
 *
 * @author MaGiCalL
 */
@FunctionalInterface
public interface FightingElement extends EventTrigger, EventTarget {

	/**
	 * 相应的游戏。
	 *
	 * @return
	 */
	<F extends Fighting> F getFighting();
}
