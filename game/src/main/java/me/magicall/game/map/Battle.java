/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.map;

import me.magicall.game.FightingElement;
import me.magicall.game.unit.Unit;
import me.magicall.game.unit.UnitsHolder;

/**
 * <pre>
 * 战场/游戏中的地图。
 * 主要功能：持有游戏地图信息、坐标信息、坐标-单位映射信息。
 * </pre>
 */
public interface Battle<C extends Coordinate, U extends Unit> extends UnitsHolder<C, U>, FightingElement {

	//	/**
	//	 * 获取一个坐标。
	//	 *
	//	 * @param indexes
	//	 * @return
	//	 */
	//	C getCoordinate(final int... indexes);

	//	/**
	//	 * 遍历地图。
	//	 *
	//	 * @param traverseHandlers
	//	 */
	//	default void traverse(final Collection<? extends MapTraverseHandler<C, U>> traverseHandlers) {
	//		final int height = getHeight();
	//		final int width = getWidth();
	//		for (int i = 0; i < height; ++i) {
	//			for (int j = 0; j < width; ++j) {
	//				final C coordinate = getCoordinate(i, j);
	//				for (final MapTraverseHandler<C, U> h : traverseHandlers) {
	//					if (!h.handle(coordinate, getUnits(coordinate))) {
	//						return;
	//					}
	//				}
	//			} //for j
	//		} //for i
	//	}
	//
	//	/**
	//	 * 遍历地图。
	//	 *
	//	 * @param traverseHandlers
	//	 */
	//	default void traverse(final MapTraverseHandler<C, U>... traverseHandlers) {
	//		traverse(Lists.newArrayList(traverseHandlers));
	//	}
	//
	//	/**
	//	 * 遍历地图。
	//	 *
	//	 * @param traverseHandler
	//	 */
	//	default void traverse(final MapTraverseHandler<C, U> traverseHandler) {
	//		traverse(Lists.newArrayList(traverseHandler));
	//	}
	//
	//	/**
	//	 * 地图遍历器，用于GameMap.traverse()方法
	//	 *
	//	 * @author MaGiCalL
	//	 */
	//	@FunctionalInterface
	//	interface MapTraverseHandler<C extends Coordinate, U extends Unit> {
	//		/**
	//		 * @param coordinate
	//		 * @return 是否继续。
	//		 */
	//		boolean handle(C coordinate, Collection<U> units);
	//	}
}
