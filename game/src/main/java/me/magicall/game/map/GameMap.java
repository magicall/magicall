/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.map;

import me.magicall.Named;

/**
 * <pre>
 * 游戏地图。
 * 只包含游戏的基本配置信息，在游戏开始之前供玩家选择地图时使用。
 * </pre>
 *
 * @author MaGiCalL
 */
public interface GameMap extends Named {

	/**
	 * 本地图最多可供多少玩家进行游戏
	 *
	 * @return
	 */
	int getMaxPlayersCount();

	/**
	 * 地图宽度
	 *
	 * @return
	 */
	int getWidth();

	/**
	 * 地图高度
	 *
	 * @return
	 */
	int getHeight();
}
