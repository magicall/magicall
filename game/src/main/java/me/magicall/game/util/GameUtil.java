/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.util;

import me.magicall.game.io.GameOutput;
import me.magicall.game.io.OutputTarget;
import me.magicall.game.map.Coordinate;

import java.util.Comparator;

public class GameUtil {

	public static final Comparator<Coordinate> COORDINATE_COMPARATOR = (o1, o2) -> {
		final var is1 = o1.getCoordinateIndexes();
		final var is2 = o2.getCoordinateIndexes();
		for (var i = 0; i < is1.length; ++i) {
			if (is1[i] > is2[i]) {
				return 1;
			} else if (is1[i] < is2[i]) {
				return -1;
			}
		}
		return 0;
	};

	public static void showException(final OutputTarget outputTarget, final GameOutput gameOutput, final Throwable e) {
		var throwable = e;
		for (var t = throwable.getCause(); t != null; t = throwable.getCause()) {
			throwable = t;
		}
		gameOutput.output(outputTarget, throwable.getLocalizedMessage());
	}
}
