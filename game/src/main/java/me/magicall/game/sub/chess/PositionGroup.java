/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.chess;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class PositionGroup {

	private final SortedSet<Position> positions;

	public PositionGroup() {
		positions = new TreeSet<>();
	}

	public Collection<Position> getPositions() {
		return positions;
	}

	public void addPosition(final Position position) {
		positions.add(position);
	}

	@Override
	public String toString() {
		final String sb = positions.stream().map(c -> String.valueOf(c) + ' ').collect(Collectors.joining());
		return sb;
	}
}
