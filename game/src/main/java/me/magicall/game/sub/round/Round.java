/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import me.magicall.game.player.Fighter;
import me.magicall.game.skill.SkillOperation;
import me.magicall.Identified;

import java.util.List;

/**
 * 一个回合
 *
 * @author MaGiCalL
 */
public interface Round extends Identified<Integer> {

	Multimap<Fighter, SkillOperation> getSkillOperations();

	@Override
	Integer id();

	interface FinishedRound extends Round {
	}

	interface CurRound<R extends FinishedRound> extends Round {

		default void addSkillOptions(final Fighter fighter, final SkillOperation... skillOperations) {
			addSkillOptions(fighter, Lists.newArrayList(skillOperations));
		}

		void addSkillOptions(Fighter fighter, List<SkillOperation> skillOperations);

		R finish();
	}

	class CommonFinishedRound implements FinishedRound {
		private final int id;
		private final Multimap<Fighter, SkillOperation> skillOperations = MultimapBuilder.linkedHashKeys().arrayListValues()
				.build();

		public CommonFinishedRound(final int id, final Multimap<Fighter, SkillOperation> skillOperations) {
			this.id = id;
			this.skillOperations.putAll(skillOperations);
		}

		@Override
		public Integer id() {
			return id;
		}

		@Override
		public Multimap<Fighter, SkillOperation> getSkillOperations() {
			return skillOperations;
		}

		@Override
		public String toString() {
			return "Round(" + id() + ") : " + skillOperations.toString();
		}
	}
}
