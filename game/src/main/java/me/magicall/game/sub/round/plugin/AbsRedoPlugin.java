/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.plugin;

import java.util.List;

import me.magicall.game.Fighting;
import me.magicall.game.io.OperatingException;
import me.magicall.game.sub.round.Round;
import me.magicall.game.sub.round.skill.UndoManager;
import me.magicall.game.sub.round.skill.UndoRedoManager;

public abstract class AbsRedoPlugin<G extends Fighting, R extends Round> extends AbsUndoPlugin<G, R> {

	@Override
	protected UndoManager<R> newUndoManager(final int maxUndoRoundCount) {
		return new UndoRedoManager<>(maxUndoRoundCount);
	}

	@Override
	protected void addSkill(final Fighting fighting) {
		super.addSkill(fighting);
		//		final Fighter[] fighters = fighting.getPlayerRoles();
		//		for (final Fighter fighter : fighters) {
		//			fighter.addSkill(new Redo());//玩家得到一个技能：Redo
		//		}
	}

	private class Redo extends Undo {

		@Override
		public String getTip() {
			return "重做撤销";
		}

		@Override
		protected void checkRoundsCount() throws OperatingException {
			final var undoRedoManager = (UndoRedoManager<R>) getUndoManager();
			if (undoRedoManager.getRedosCount() == 0) {
				throw new OperatingException("当前没有可" + getTip() + "的回合");
			}
		}

		@Override
		protected void restoreMap(final G game, final int stepCount) {
			final var undoRedoManager = (UndoRedoManager<R>) getUndoManager();
			final var canceledRounds = undoRedoManager.redo(stepCount);
			undoRounds(game, canceledRounds, false);
		}
	}
}
