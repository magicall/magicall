/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.plugin;

import me.magicall.game.Fighting;
import me.magicall.game.sub.round.Round;

public interface RoundPlugin {

	void startingRound(Fighting fighting, Round round);

	void addingRound(Fighting fighting, Round round);

	void endingRound(Fighting fighting, Round round);

	void endedRound(Fighting fighting, Round round);
}
