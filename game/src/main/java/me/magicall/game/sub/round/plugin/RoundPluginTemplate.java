/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.plugin;

import me.magicall.game.Fighting;
import me.magicall.game.sub.round.Round;

public class RoundPluginTemplate<G extends Fighting, R extends Round> implements RoundPlugin {

	@SuppressWarnings("unchecked")
	@Override
	public final void addingRound(final Fighting fighting, final Round round) {
		addingRoundInternal((G) fighting, (R) round);
	}

	protected void addingRoundInternal(final G game, final R round) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public final void endedRound(final Fighting fighting, final Round round) {
		endedRoundInternal((G) fighting, (R) round);
	}

	protected void endedRoundInternal(final G game, final R round) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public final void endingRound(final Fighting fighting, final Round round) {
		endingRoundInternal((G) fighting, (R) round);
	}

	protected void endingRoundInternal(final G game, final R round) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public final void startingRound(final Fighting fighting, final Round round) {
		startingRoundInternal((G) fighting, (R) round);
	}

	protected void startingRoundInternal(final G game, final R round) {
	}
}
