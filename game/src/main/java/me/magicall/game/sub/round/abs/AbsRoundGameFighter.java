/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.abs;

import me.magicall.game.player.Player;
import me.magicall.game.skill.SkillsManager;
import me.magicall.game.sub.round.Round;
import me.magicall.game.sub.round.RoundFighting;
import me.magicall.game.sub.round.RoundGameFighter;

public abstract class AbsRoundGameFighter<G extends RoundFighting, R extends Round> implements RoundGameFighter {

	protected final SkillsManager skillManager = new SkillsManager();
	private final G game;
	private final String name;
	private Player player;

	protected AbsRoundGameFighter(final G game, final String name) {
		this.game = game;
		this.name = name;
	}

	protected AbsRoundGameFighter(final G game) {
		this.game = game;
		name = getClass().getSimpleName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void play(final Round round) {
		playInternal((R) round);
	}

	@Override
	public String name() {
		return name;
	}

	protected abstract void playInternal(final R round);

	@Override
	public String toString() {
		return name();
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	@Override
	public G getFighting() {
		return game;
	}
}
