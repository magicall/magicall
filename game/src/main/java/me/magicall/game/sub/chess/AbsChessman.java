/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.chess;

import me.magicall.game.player.Fighter;
import me.magicall.game.skill.SkillsHolder;
import me.magicall.game.skill.SkillsHolder.SkillsHolderWrapper;
import me.magicall.game.skill.SkillsManager;
import me.magicall.ui.Profile;

public abstract class AbsChessman implements Chessman, SkillsHolderWrapper {

	protected final ConceptChessman cfg;
	protected final Fighter owner;
	protected final SkillsManager skillManager;

	public AbsChessman(final ConceptChessman cfg, final Fighter owner) {
		this(cfg, owner, new SkillsManager());
	}

	public AbsChessman(final ConceptChessman cfg, final Fighter owner, final SkillsManager skillManager) {
		this.cfg = cfg;
		this.owner = owner;
		this.skillManager = skillManager;
	}

	@Override
	public Profile getProfile() {
		return cfg.getProfile();
	}

	@Override
	public Fighter owner() {
		return owner;
	}

	@Override
	public SkillsHolder rawSkillOwner() {
		return skillManager;
	}

	public boolean is(final ConceptChessman conceptChessman) {
		return name().equals(conceptChessman.name());
	}
}
