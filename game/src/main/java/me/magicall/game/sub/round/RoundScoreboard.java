/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round;

import me.magicall.game.Scoreboard;
import me.magicall.game.player.Team;

/**
 * @author Liang Wenjian.
 */
public interface RoundScoreboard extends Scoreboard {

	int getRoundCount();

	class CommonRoundScoreboard implements RoundScoreboard {

		private final int roundCount;
		private final Team leader;
		private final Team laggard;

		public CommonRoundScoreboard(final int roundCount, final Team leader, final Team laggard) {
			this.roundCount = roundCount;
			this.leader = leader;
			this.laggard = laggard;
		}

		@Override
		public int getRoundCount() {
			return roundCount;
		}

		@Override
		public Team getLeader() {
			return leader;
		}

		@Override
		public Team getLaggard() {
			return laggard;
		}
	}
}
