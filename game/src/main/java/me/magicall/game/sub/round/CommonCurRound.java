/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import java.util.List;

import me.magicall.game.player.Fighter;
import me.magicall.game.skill.SkillOperation;
import me.magicall.game.sub.round.Round.CommonFinishedRound;
import me.magicall.game.sub.round.Round.CurRound;

public class CommonCurRound implements CurRound<CommonFinishedRound> {

	private final int id;
	private final Multimap<Fighter, SkillOperation> skillOperations = MultimapBuilder.linkedHashKeys().arrayListValues()
			.build();

	public CommonCurRound(final int id, final Multimap<Fighter, SkillOperation> skillOperations) {
		this.id = id;
		this.skillOperations.putAll(skillOperations);
	}

	public CommonCurRound(final int id) {
		this.id = id;
	}

	@Override
	public Integer id() {
		return id;
	}

	@Override
	public Multimap<Fighter, SkillOperation> getSkillOperations() {
		return skillOperations;
	}

	@Override
	public void addSkillOptions(final Fighter fighter, final List<SkillOperation> skillOperations) {
		this.skillOperations.putAll(fighter, skillOperations);
	}

	@Override
	public CommonFinishedRound finish() {
		return new CommonFinishedRound(id, skillOperations);
	}

	@Override
	public String toString() {
		return "Round(" + id() + ") : " + skillOperations.toString();
	}
}
