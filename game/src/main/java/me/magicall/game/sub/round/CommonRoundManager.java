/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round;

import com.google.common.collect.Lists;

import java.util.List;

import me.magicall.game.GameException;
import me.magicall.game.sub.round.Round.FinishedRound;

public class CommonRoundManager {

	protected final List<FinishedRound> rounds;
	protected CommonCurRound curRound;

	protected CommonRoundManager(final List<FinishedRound> rounds) {
		this.rounds = rounds;
	}

	public CommonRoundManager() {
		this(Lists.newLinkedList());
	}

	public List<FinishedRound> getRounds() {
		return rounds;
	}

	public boolean addRound(final FinishedRound round) {
		return rounds.add(round);
	}

	public FinishedRound popLastRound() {
		return rounds == null ? null : rounds.remove(rounds.size() - 1);
	}

	public FinishedRound getLastRound() {
		return rounds.get(rounds.size());
	}

	public int getRoundsCount() {
		return rounds.size();
	}

	public CommonCurRound getCurRound() {
		return curRound;
	}

	public CommonCurRound startRound() {
		if (curRound != null) {
			throw new GameException("cur round unfinished");
		}
		curRound = new CommonCurRound(rounds.size() + 1);
		return curRound;
	}

	public FinishedRound endCurRound() {
		final FinishedRound finished = curRound.finish();
		rounds.add(finished);
		curRound = null;
		return finished;
	}
}
