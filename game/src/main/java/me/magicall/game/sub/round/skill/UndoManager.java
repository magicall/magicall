/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.skill;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import me.magicall.game.sub.round.CommonRoundManager;
import me.magicall.game.sub.round.Round;

public class UndoManager<R extends Round> extends CommonRoundManager implements Undoable {

	protected int maxUndoRoundCount;

	public UndoManager(final int maxUndoRoundCount) {
		this.maxUndoRoundCount = maxUndoRoundCount;
	}

	@Override
	public R undo() {
		if (maxUndoRoundCount == 0) {
			throw roundCountTooLarge(1);
		}
		//		return popLastRound();TODO
		return null;
	}

	private RuntimeException roundCountTooLarge(final int roundCount) {
		return new IllegalArgumentException(
				"max undo step available is:" + maxUndoRoundCount + ", " + roundCount + " is too more");
	}

	@Override
	public List<R> undo(final int undoRoundCount) {
		if (undoRoundCount > maxUndoRoundCount) {
			throw roundCountTooLarge(undoRoundCount);
		}
		final var countToUse = Math.min(undoRoundCount, rounds.size());
		final List<R> list = Lists.newArrayListWithExpectedSize(countToUse);
		//恢复?
		//			list.add(round);TODO
		if (IntStream.range(0, countToUse).mapToObj(i -> popLastRound()).anyMatch(Objects::isNull)) {
			return list;
		}
		return list;
	}

	public int getMaxUndoRoundCount() {
		return maxUndoRoundCount;
	}

	public void setMaxUndoRoundCount(final int maxUndoRoundCount) {
		this.maxUndoRoundCount = maxUndoRoundCount;
	}
}
