/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.chess;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.regex.Pattern;

import me.magicall.game.Fighting;
import me.magicall.game.map.Coordinate;
import me.magicall.game.skill.Command;
import me.magicall.game.skill.NoTargetToBeSelectingException;
import me.magicall.game.skill.TargetSelector;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

public class PositionSelector implements TargetSelector {

	protected static final Pattern POSITION_PATTERN = Pattern.compile("\\s*,\\s*");

	@Override
	public Collection<Position> select(final Fighting fighting, final Command command)
			throws NoTargetToBeSelectingException {
		final var string = (String) command.getData();
		try {
			final var pos = getPositionInputSplitPattern().split(string);
			if (pos.length != 2) {
				throw new NoTargetToBeSelectingException("输入有误，请输入两个数字。row,column");
			}
			final int row = Kits.INT.fromString(pos[0]);
			final int column = Kits.INT.fromString(pos[1]);
			final Coordinate position = null;//TODO fighting.getBattle().getCoordinate(row, column);
			return Kits.COLL.cast(Lists.newArrayList(position));
		} catch (final Exception e) {
			throw new NoTargetToBeSelectingException("输入有误，请输入两个数字。row,column", e);
		}
	}

	protected Pattern getPositionInputSplitPattern() {
		return POSITION_PATTERN;
	}
}
