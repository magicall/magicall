/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.chess;

public class CommonChessBoard implements ChessBoard {

	private final String name;
	private final int maxPlayersCount;
	private final int rowsCount;
	private final int columnsCount;

	public CommonChessBoard(final String name, final int maxPlayersCount, final int rowsCount, final int columnsCount) {
		this.name = name;
		this.maxPlayersCount = maxPlayersCount;
		this.rowsCount = rowsCount;
		this.columnsCount = columnsCount;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public int getColumnsCount() {
		return columnsCount;
	}

	@Override
	public int getRowsCount() {
		return rowsCount;
	}

	@Override
	public int getMaxPlayersCount() {
		return maxPlayersCount;
	}

	//	public void setUnit(final Coordinate position, final U unit) {
	//		final SingleUnitMapManager<Coordinate, Unit> uh = baseGamingMap.getUnitsHolder();
	//		uh.setUnit(position, unit);
	//	}
	//
	//	protected UnitsHolder buildUnitsHolder() {
	//		return new SingleUnitMapManager<Position, U>();
	//	}
	//
	//	@Override
	//	public Position getCoordinate(final int... coordinateNumbers) {
	//		return positions[coordinateNumbers[0]][coordinateNumbers[1]];
	//	}
	//
	//	@Override
	//	public G getFighting() {
	//		return baseGamingMap.getFighting();
	//	}
	//
	//	@Override
	//	public void addUnit(final Coordinate coordinate, final Unit unit) throws NoSuchUnitException {
	//		baseGamingMap.addUnit(coordinate, unit);
	//	}
	//
	//	@Override
	//	public void addUnits(final Coordinate coordinate, final Collection<? extends Unit> units) throws
	//			NoSuchUnitException {
	//		baseGamingMap.addUnits(coordinate, units);
	//	}
	//
	//	@Override
	//	public void addUnits(final Coordinate coordinate, final Unit... units) throws NoSuchUnitException {
	//		baseGamingMap.addUnits(coordinate, units);
	//	}
	//
	//	@Override
	//	public U getFirstUnit(final Coordinate coordinate) {
	//		return baseGamingMap.getFirstUnit(coordinate);
	//	}
	//
	//	@Override
	//	public Collection<U> getUnits(final Coordinate coordinate) {
	//		return baseGamingMap.getUnits(coordinate);
	//	}
	//
	//	@Override
	//	public void removeAllUnits(final Coordinate coordinate) {
	//		baseGamingMap.removeAllUnits(coordinate);
	//	}
	//
	//	@Override
	//	public void removeUnit(final Coordinate coordinate, final Unit unit) throws NoSuchUnitException {
	//		baseGamingMap.removeUnit(coordinate, unit);
	//	}
	//
	//	@Override
	//	public void removeUnits(final Coordinate coordinate, final Collection<? extends Unit> units)
	//			throws NoSuchUnitException {
	//		baseGamingMap.removeUnits(coordinate, units);
	//	}
	//
	//	@Override
	//	public void removeUnits(final Coordinate coordinate, final Unit... units) throws NoSuchUnitException {
	//		baseGamingMap.removeUnits(coordinate, units);
	//	}

	//	protected ChessBoardDisplayer getChessBoardDisplayer() {
	//		return ChessBoardDisplayer.INSTANCE;
	//	}

	//	@Override
	//	public String toString() {
	//		final ChessBoardDisplayer chessBoardDisplayer = getChessBoardDisplayer();
	//		final char[][] chars = chessBoardDisplayer.toCharSquare(this);//, blankCountBetweenNeighborCross());
	//		traverse(new ChessBoardTraverseHandler() {
	//			@Override
	//			protected boolean handle(final Position position) {
	//				final Chessman chessman = getFirstUnit(position);
	//				if (chessman != null) {
	//					chars[positionRowToBoardRowIndex(position.getRow())][positionColumnToBoardColumnIndex(position
	// .getColumn()
	//					)]//
	//							= chessman.getShowChar();
	//				}
	//				return true;
	//			}
	//		});
	//		return chessBoardDisplayer.toString(chars);
	//	}
	//
	//	protected int blankCountBetweenNeighborCross() {
	//		return 0;
	//	}
	//
	//	protected int positionRowToBoardRowIndex(final int positionRow) {
	//		return positionRow;
	//	}
	//
	//	protected int positionColumnToBoardColumnIndex(final int positionColumn) {
	//		return positionColumn;
	//	}
}
