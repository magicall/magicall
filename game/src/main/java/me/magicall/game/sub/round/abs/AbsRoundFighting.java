/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.abs;

import me.magicall.game.Fighting;
import me.magicall.game.GameOverException;
import me.magicall.game.event.WarStartingEvent;
import me.magicall.game.map.Coordinate;
import me.magicall.game.player.Fighter;
import me.magicall.game.sub.round.CommonRoundManager;
import me.magicall.game.sub.round.Round.CurRound;
import me.magicall.game.sub.round.Round.FinishedRound;
import me.magicall.game.sub.round.RoundFighting;
import me.magicall.game.unit.Unit;

import java.util.Arrays;
import java.util.List;

public abstract class AbsRoundFighting<_Coordinate extends Coordinate, _Unit extends Unit>
		implements RoundFighting<_Coordinate, _Unit>, Fighting<_Coordinate, _Unit> {

	protected final CommonRoundManager roundManager;

	protected AbsRoundFighting() {
		this(new CommonRoundManager());
	}

	protected AbsRoundFighting(final CommonRoundManager roundManager) {
		this.roundManager = roundManager;
	}

	@Override
	public void start() {
		publishEvent(new WarStartingEvent(this));
		while (!isFinished()) {
			getRoundManager().startRound();
			try {
				round();
			} catch (final GameOverException ignored) {
				break;
			} finally {
				getRoundManager().endCurRound();
			}
		}
	}

	protected void round() {
		Arrays.<Fighter>stream(getFighters()).forEach(Fighter::startRound);
	}

	protected CommonRoundManager getRoundManager() {
		return roundManager;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <F extends FinishedRound> List<F> getRounds() {
		return (List<F>) getRoundManager().getRounds();
	}

	@Override
	public int getRoundsCount() {
		return getRoundManager().getRoundsCount();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <F extends FinishedRound, C extends CurRound<F>> C getCurRound() {
		return (C) getRoundManager().getCurRound();
	}
}
