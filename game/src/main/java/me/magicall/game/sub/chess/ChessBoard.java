/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.chess;

import me.magicall.game.map.GameMap;

/**
 * 棋盘。棋类游戏的地图。
 */
public interface ChessBoard extends GameMap {

	/**
	 * <pre>
	 * 棋盘的列数量。
	 * 棋类游戏地图常用术语是行数、列数，故用来替代高度、宽度。
	 * </pre>
	 *
	 * @return
	 */
	int getColumnsCount();

	/**
	 * <pre>
	 * 棋盘的行数量。
	 * 棋类游戏地图常用术语是行数、列数，故用来替代高度、宽度。
	 * </pre>
	 *
	 * @return
	 */
	int getRowsCount();

	@Override
	default int getHeight() {
		return getRowsCount();
	}

	@Override
	default int getWidth() {
		return getColumnsCount();
	}
}
