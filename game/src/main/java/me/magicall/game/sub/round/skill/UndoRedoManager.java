/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round.skill;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import me.magicall.game.sub.round.Round;
import me.magicall.game.sub.round.Round.FinishedRound;
import me.magicall.game.sub.round.skill.Undoable.Redoable;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

public class UndoRedoManager<R extends Round> extends UndoManager<R> implements Redoable {

	private Deque<R> redos;

	public UndoRedoManager(final int maxUndoRoundCount) {
		super(maxUndoRoundCount);
	}

	private R redoInternal() {
		final var nextRound = redos.pollLast();
		//		rounds.add(nextRound);TODO
		return nextRound;
	}

	@Override
	public R redo() {
		if (!Kits.COLL.isEmpty(redos)) {
			return redoInternal();
		}
		return null;
	}

	@Override
	public List<R> redo(final int redoRoundCount) {
		if (!Kits.COLL.isEmpty(redos)) {
			final List<R> list = IntStream.range(0, redoRoundCount).mapToObj(i -> redoInternal())
					.collect(Collectors.toCollection(() -> new ArrayList<>(redoRoundCount)));
			return list;
		}
		return Kits.LIST.emptyVal();
	}

	@Override
	public FinishedRound popLastRound() {
		final var round = super.popLastRound();
		if (round == null) {
			return null;
		}
		if (redos == null) {
			redos = new LinkedList<>();
		}
		//		redos.add(round);TODO
		return round;
	}

	public Deque<R> getRedos() {
		return redos;
	}

	public int getRedosCount() {
		return redos.size();
	}

	//	public boolean addRound(final R round) {
	//		if (!super.addRound(round)) {
	//			return false;
	//		}
	//		//		//drop the redos
	//		//		if (!Kits.COLL.isEmpty(redos)) {
	//		//			redos = null;
	//		//		}
	//		return true;
	//	}
}
