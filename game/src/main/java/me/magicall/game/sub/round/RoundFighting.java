/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.sub.round;

import java.util.List;

import me.magicall.game.Fighting;
import me.magicall.game.map.Coordinate;
import me.magicall.game.sub.round.Round.CurRound;
import me.magicall.game.sub.round.Round.FinishedRound;
import me.magicall.game.unit.Unit;

public interface RoundFighting<_Coordinate extends Coordinate, _Unit extends Unit>
		extends Fighting<_Coordinate, _Unit> {

	default int getRoundsCount() {
		return getRounds().size();
	}

	<F extends FinishedRound> List<F> getRounds();

	/**
	 * 当前回合
	 *
	 * @return
	 */
	<F extends FinishedRound, C extends CurRound<F>> C getCurRound();
}
