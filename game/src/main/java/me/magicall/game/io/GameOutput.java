/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.io;

/**
 * 游戏输出端，是与用户交互的接口。对于游戏程序来说是输出端。
 *
 * @author MaGiCalL
 */
@FunctionalInterface
public interface GameOutput {
	/**
	 * @param source 触发输出的对象，通常是“this”
	 * @param content 输出的内容
	 */
	void output(OutputTarget source, Object content);
}
