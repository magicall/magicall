/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.io;

import me.magicall.game.GameException;

import java.io.Serial;

public class OperatingException extends GameException {

	@Serial
	private static final long serialVersionUID = -6385667895115676090L;

	public OperatingException() {
		super();
	}

	public OperatingException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public OperatingException(final String message) {
		super(message);
	}

	public OperatingException(final Throwable cause) {
		super(cause);
	}
}
