/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.io;

public abstract class AbsGameInput implements GameInput {

	private volatile InputHandler cur;

	@Override
	public void requestInput(final InputHandler inputHandler) throws OperatingException {
		//synchronized?
		cur = inputHandler;
		final var nextInput = nextInput();
		cur.handle(nextInput);
	}

	protected abstract Object nextInput();
}