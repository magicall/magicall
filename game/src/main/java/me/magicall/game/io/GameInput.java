/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.io;

/**
 * 游戏输入端。是与用户交互的接口。对于游戏程序来说是输入端；对用户来说是输出端。
 * 如：鼠标、键盘、麦克风、遥控器、手柄、触摸屏等等。
 *
 * @author MaGiCalL
 */
@FunctionalInterface
public interface GameInput {
	/**
	 * 向输入端请求一个输入,然后回调InputHandler参数的方法
	 *
	 * @throws OperatingException
	 */
	void requestInput(final InputHandler inputHandler) throws OperatingException;
}
