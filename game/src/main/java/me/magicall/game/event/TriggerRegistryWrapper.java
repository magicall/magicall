/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.event;

import java.util.List;

/**
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface TriggerRegistryWrapper extends TriggerRegistry {

	TriggerRegistry rawTriggerRegistry();

	@Override
	default <_Event extends Event> void remove(final Trigger<_Event> trigger) {
		rawTriggerRegistry().remove(trigger);
	}

	@Override
	default <_Event extends Event> void reg(final Trigger<_Event> trigger) {
		rawTriggerRegistry().reg(trigger);
	}

	@Override
	default <_Event extends Event> void reg(final Trigger<_Event> trigger, final Class<?> rawClass) {
		rawTriggerRegistry().reg(trigger, rawClass);
	}

	@Override
	default <_Event extends Event> void setListeners(final List<Trigger<_Event>> triggers) {
		rawTriggerRegistry().setListeners(triggers);
	}
}
