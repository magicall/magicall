/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.event;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

/**
 * 事件.事件可以是一个技能的一次施放效果、一个单位的死亡、一场游戏的结束等等。
 *
 * @author MaGiCalL
 */
public interface Event {

	/**
	 * 事件的来源,可能是单位、游戏、技能、物品、环境、地图、甚至是事件本身。
	 *
	 * @return
	 */
	Object getSource();

	Event getCause();

	ZonedDateTime getHappenTime();

	/**
	 * 触发事件的第一类目标，是事件的“主语”（Subject）。
	 *
	 * @return
	 */
	List<? extends EventTrigger> getTriggers();

	@SuppressWarnings("unchecked")
	default <T extends EventTrigger> T getFirstTrigger() {
		final Collection<? extends EventTrigger> triggers = getTriggers();
		if (triggers == null) {
			return null;
		} else {
			return (T) triggers.iterator().next();
		}
	}

	/**
	 * 触发事件的第二类目标，是事件的“宾语”（Object）。有的事件没有宾语。
	 *
	 * @return
	 */
	List<? extends EventTarget> getTargets();

	@SuppressWarnings("unchecked")
	default <T extends EventTarget> T getFirstTarget() {
		final var targets = getTargets();
		if (targets == null) {
			return null;
		} else {
			return (T) targets.get(0);
		}
	}
}
