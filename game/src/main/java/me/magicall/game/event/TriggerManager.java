/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.event;

import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 事件监听器的管理器
 *
 * @author MaGiCalL
 */
public class TriggerManager {

	protected Map<Class<? extends Event>, Collection<Trigger<?>>> map;

	public TriggerManager() {
		this(Maps.newHashMap());
	}

	public TriggerManager(final Map<Class<? extends Event>, Collection<Trigger<?>>> map) {
		this.map = map;
	}

	public <T> void addEventListener(final Class<? extends Event> eventClass, final Trigger<?> trigger) {
		final var triggers = map.computeIfAbsent(eventClass, k -> newEventListenerCollection());
		triggers.add(trigger);
	}

	public void removeEventListener(final Class<? extends Event> eventClass, final Trigger<?> trigger) {
		final var triggers = map.get(eventClass);
		if (triggers != null) {
			triggers.remove(trigger);
		}
	}

	public void removeEventListeners(final Class<? extends Event> eventClass) {
		map.remove(eventClass);
	}

	protected Collection<Trigger<?>> newEventListenerCollection() {
		return new LinkedList<>();
	}

	/**
	 * 主要方法。会调用EventHandler的createEvent方法创建一个事件；
	 * 触发监听此事件的所有事件监听器的前操作；
	 * 然后调用EventHandler的handleEvent方法；
	 * 最后触发此事件的所有事件监听器的后操作。
	 *
	 * @return 事件
	 */
	public Event handle(final EventHandler eventHandler) {
		final var event = eventHandler.createEvent();

		//		before(eventHandler, event);
		//
		//		eventHandler.handleEvent(event);
		//
		//		after(eventHandler, event);

		return event;
	}

	@SuppressWarnings("rawtypes")
	protected <T> Collection<Trigger> getEventListeners(final Event event) {
		final Collection<Trigger> rt = new LinkedList<>();
		map.entrySet().forEach(e -> {
			final Class<?> c = e.getKey();
			if (c.isAssignableFrom(event.getClass())) {
				rt.addAll(e.getValue());
			}
		});
		return rt;
	}
	//
	//	protected <T> void before(final EventHandler<T> eventHandler, final Event<T> event) {
	//		final Collection<Trigger> eventListeners = getEventListeners(event);
	//		for (final Trigger listener : eventListeners) {
	//			listener.before(event);
	//		}
	//	}
	//
	//	protected <T> void after(final EventHandler<T> eventHandler, final Event<T> event) {
	//		final Collection<Trigger> eventListeners = getEventListeners(event);
	//		for (final Trigger<T> listener : eventListeners) {
	//			listener.after(event);
	//		}
	//	}
}
