/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.event;

import com.google.common.collect.Lists;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author Liang Wenjian.
 */
public class CommonEvent implements Event {

	private final Object source;
	private final Event cause;
	private final ZonedDateTime happenTime;
	private final List<? extends EventTrigger> triggers;
	private final List<? extends EventTarget> targets;

	public CommonEvent(final Object source, final List<? extends EventTrigger> triggers, final EventTarget... targets) {
		this(source, null, triggers, Lists.newArrayList(targets));
	}

	public CommonEvent(final Object source, final List<? extends EventTrigger> triggers,
										 final List<? extends EventTarget> targets) {
		this(source, null, triggers, targets);
	}

	public CommonEvent(final Object source, final EventTrigger trigger, final EventTarget... targets) {
		this(source, null, trigger, targets);
	}

	public CommonEvent(final Object source, final Event cause, final EventTrigger trigger, final EventTarget... targets) {
		this(source, cause, trigger, Lists.newArrayList(targets));
	}

	public CommonEvent(final Object source, final EventTrigger trigger, final List<? extends EventTarget> targets) {
		this(source, null, trigger, targets);
	}

	public CommonEvent(final Object source, final Event cause, final EventTrigger trigger,
										 final List<? extends EventTarget> targets) {
		this(source, cause, Lists.newArrayList(trigger), targets);
	}

	public CommonEvent(final Object source, final Event cause, final List<? extends EventTrigger> triggers,
										 final List<? extends EventTarget> targets) {
		this.source = source;
		this.cause = cause;
		happenTime = ZonedDateTime.now();
		this.triggers = Lists.newArrayList(triggers);
		this.targets = Lists.newArrayList(targets);
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public Event getCause() {
		return cause;
	}

	@Override
	public ZonedDateTime getHappenTime() {
		return happenTime;
	}

	@Override
	public List<? extends EventTrigger> getTriggers() {
		return triggers;
	}

	@Override
	public List<? extends EventTarget> getTargets() {
		return targets;
	}
}
