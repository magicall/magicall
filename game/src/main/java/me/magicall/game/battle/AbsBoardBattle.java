/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.battle;

import me.magicall.game.GameException;
import me.magicall.game.data.SingleUnitMapManager;
import me.magicall.game.sub.chess.Chessman;
import me.magicall.game.sub.chess.Position;
import me.magicall.game.unit.UnitsHolder;
import me.magicall.game.unit.UnitsHolderWrapper;

/**
 * @author Liang Wenjian.
 */
public abstract class AbsBoardBattle implements UnitsHolderWrapper<Position, Chessman>, BoardBattle {
	protected final Position[][] positions;
	protected final UnitsHolder<Position, Chessman> unitsHolder;
	private final int columnsCount;
	private final int rowsCount;

	public AbsBoardBattle(final int columnsCount, final int rowsCount) {
		this.columnsCount = columnsCount;
		this.rowsCount = rowsCount;
		positions = new Position[columnsCount][rowsCount];
		unitsHolder = new SingleUnitMapManager<>();
	}

	@Override
	public UnitsHolder<Position, Chessman> rawUnitsHolder() {
		return unitsHolder;
	}

	@Override
	public Chessman findUnitInColumn(final int columnIndex, final String unitName) {
		for (final var position : positions[columnIndex]) {
			final var units = getUnits(position);
			for (final var unit : units) {
				if (unitName.equals(unit.name())) {
					return unit;
				}
			}
		}
		throw new GameException();
	}

	@Override
	public boolean isInRowBound(final int row) {
		return row > 0 && row <= rowsCount;
	}

	@Override
	public boolean isInColumnBound(final int column) {
		return column > 0 && column <= columnsCount;
	}
}
