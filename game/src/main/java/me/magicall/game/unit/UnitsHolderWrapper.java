/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.unit;

import java.util.Collection;

import me.magicall.game.map.Coordinate;
import me.magicall.game.map.NoSuchUnitException;
import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

/**
 * @author Liang Wenjian.
 */
@FunctionalInterface
public interface UnitsHolderWrapper<C extends Coordinate, U extends Unit>
		extends UnitsHolder<C, U>, Wrapper<UnitsHolder<C, U>> {

	UnitsHolder<C, U> rawUnitsHolder();

	@Override
	default UnitsHolder<C, U> unwrap() {
		return rawUnitsHolder();
	}

	@Override
	default boolean hasUnit(final C coordinate) {
		return rawUnitsHolder().hasUnit(coordinate);
	}

	@Override
	default Collection<U> getUnits(final C coordinate) {
		return rawUnitsHolder().getUnits(coordinate);
	}

	@Override
	default Collection<U> getUnits() {
		return rawUnitsHolder().getUnits();
	}

	@Override
	default C getCoordinate(final U unit) {
		return rawUnitsHolder().getCoordinate(unit);
	}

	@Override
	default void addUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		rawUnitsHolder().addUnits(coordinate, units);
	}

	@Override
	default void removeUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException {
		rawUnitsHolder().removeUnits(coordinate, units);
	}

	@Override
	default U getFirstUnit(final C coordinate) {
		return rawUnitsHolder().getFirstUnit(coordinate);
	}

	@Override
	default void addUnits(final C coordinate, final U... units) throws NoSuchUnitException {
		rawUnitsHolder().addUnits(coordinate, units);
	}

	@Override
	default void removeUnits(final C coordinate, final U... units) throws NoSuchUnitException {
		rawUnitsHolder().removeUnits(coordinate, units);
	}

	@Override
	default void removeAllUnits(final C coordinate) {
		rawUnitsHolder().removeAllUnits(coordinate);
	}

	@Override
	default void moveUnit(final U unit, final C coordinate) {
		rawUnitsHolder().moveUnit(unit, coordinate);
	}
}
