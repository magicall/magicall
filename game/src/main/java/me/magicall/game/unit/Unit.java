/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.unit;

import java.util.Objects;

import me.magicall.game.FightingElement;
import me.magicall.game.event.EventTarget;
import me.magicall.game.event.EventTrigger;
import me.magicall.game.player.Fighter;
import me.magicall.game.skill.SkillsHolder;
import me.magicall.Owned;
import me.magicall.ui.HasProfile;

/**
 * 单位，一场游戏中的单位。
 * 在魔兽争霸对战中，每个玩家可能有众多的单位，包括建筑、兵、英雄等；
 * 在dota中，“英雄”是一个单位，小兵也是一个单位；
 * 在三国杀中，“武将”是一个单位；
 * 在中国象棋中，帅士相车马炮兵都是单位。
 *
 * @author MaGiCalL
 */
public interface Unit extends HasProfile, SkillsHolder, Owned<Fighter>, FightingElement, EventTrigger, EventTarget {

	default boolean isSameOwner(final Unit other) {
		return Objects.equals(owner(), other.owner());
	}
}
