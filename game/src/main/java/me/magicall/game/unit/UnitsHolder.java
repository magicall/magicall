/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.unit;

import com.google.common.collect.Lists;

import java.util.Collection;

import me.magicall.game.map.Coordinate;
import me.magicall.game.map.NoSuchUnitException;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

/**
 * 坐标-单位的对应者。一个坐标上可能有多个单位，棋类游戏通常只有一个。
 *
 * @author MaGiCalL
 */
public interface UnitsHolder<C extends Coordinate, U extends Unit> {

	default boolean hasUnit(final C coordinate) {
		return getUnits(coordinate).isEmpty();
	}

	Collection<U> getUnits(C coordinate);

	default U getFirstUnit(final C coordinate) {
		final var units = getUnits(coordinate);
		if (Kits.COLL.isEmpty(units)) {
			return null;
		} else {
			return units.iterator().next();
		}
	}

	Collection<U> getUnits();

	C getCoordinate(U unit);

	void addUnits(final C coordinate, final Collection<? extends U> units) throws NoSuchUnitException;

	default void addUnits(final C coordinate, final U... units) throws NoSuchUnitException {
		addUnits(coordinate, Lists.newArrayList(units));
	}

	void removeUnits(C coordinate, final Collection<? extends U> units) throws NoSuchUnitException;

	default void removeUnits(final C coordinate, final U... units) throws NoSuchUnitException {
		removeUnits(coordinate, Lists.newArrayList(units));
	}

	default void removeAllUnits(final C coordinate) {
		final var units = getUnits(coordinate);
		if (!Kits.COLL.isEmpty(units)) {
			removeUnits(coordinate, units);
		}
	}

	default void moveUnit(final U unit, final C coordinate) {
		removeUnits(coordinate, unit);
		addUnits(coordinate, unit);
	}
}
