/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class SkillsManager implements SkillsHolder {

	protected Map<Character, Skill> skills;

	public SkillsManager() {
		this(new LinkedHashMap<>());
	}

	public SkillsManager(final Map<Character, Skill> skills) {
		this.skills = skills;
	}

	@Override
	public boolean hasSkill(final Skill skill) {
		return skills.containsValue(skill);
	}

	@Override
	public Collection<Skill> getSkills() {
		return skills.values();
	}

	@Override
	public void addSkill(final Skill skill) {
		skills.put(Character.toLowerCase(skill.getHotKey()), skill);
	}

	@Override
	public void removeSkill(final Skill skill) {
		skills.remove(Character.toLowerCase(skill.getHotKey()));
	}

	@Override
	public Skill getSkill(final String name) {
		return skills.get(Character.toLowerCase(name.charAt(0)));
	}
}
