/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import java.util.Collection;

import me.magicall.game.Fighting;
import me.magicall.game.io.OperatingException;
import me.magicall.game.player.Player;

public abstract class SkillTemplate implements Skill {

	@Override
	public char getHotKey() {
		return name().charAt(0);
	}

	@Override
	public String name() {
		return getClass().getSimpleName();
	}

	@Override
	public String getTip() {
		return name();
	}

	protected abstract void action(Fighting fighting, Player player, Collection<?> targets) throws OperatingException;

	protected abstract Collection<?> select(final Fighting fighting, Player player) throws NoTargetSelectingException;

	@Override
	public void action(final Fighting fighting, final Player player)
			throws OperatingException, NoTargetSelectingException {
		final var targets = select(fighting, player);
		action(fighting, player, targets);
	}
}
