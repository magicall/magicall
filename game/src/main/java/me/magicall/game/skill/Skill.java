/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import me.magicall.game.Fighting;
import me.magicall.game.io.OperatingException;
import me.magicall.game.io.OutputTarget;
import me.magicall.game.player.Player;
import me.magicall.Named;
import me.magicall.ui.HasTip;

/**
 * 一个原始的技能类，与具体角色、玩家、游戏无关
 *
 * @author MaGiCalL
 */
public interface Skill extends Named, HasTip, OutputTarget {

	void action(Fighting fighting, Player player) throws OperatingException, NoTargetSelectingException;

	char getHotKey();
}
