/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import com.google.common.collect.Lists;

import java.util.Collection;

import me.magicall.game.player.Fighter;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

/**
 * 一次技能的施放
 *
 * @author MaGiCalL
 */
public class SkillOperation {

	private Fighter fighter;

	private Skill skill;

	private Collection<?> targets;

	private Collection<? extends Object> skillArgs;

	@Override
	public String toString() {
		final var sb = new StringBuilder(fighter.name()).append(' ')//
				.append(skill.name()).append(" → ")//
				.append(targets).append(" {").append(skillArgs).append('}');
		return sb.toString();
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(final Skill skill) {
		this.skill = skill;
	}

	public <T> Collection<T> getTargets() {
		return Kits.COLL.cast(targets);
	}

	public void setTargets(final Object... targets) {
		this.targets = Lists.newArrayList(targets);
	}

	public void setTargets(final Collection<?> targets) {
		this.targets = targets;
	}

	public void setTarget(final Object target) {
		targets = Lists.newArrayList(target);
	}

	@SuppressWarnings("unchecked")
	public <P extends Fighter> P getFighter() {
		return (P) fighter;
	}

	public void setFighter(final Fighter fighter) {
		this.fighter = fighter;
	}

	public <A extends Object> Collection<A> getSkillArgs() {
		return Kits.COLL.cast(skillArgs);
	}

	public void setSkillArgs(final Object... skillArgs) {
		this.skillArgs = Lists.newArrayList(skillArgs);
	}

	public void setSkillArgs(final Collection<? extends Object> skillArgs) {
		this.skillArgs = skillArgs;
	}

	public void setSkillArg(final Object skillArg) {
		skillArgs = Lists.newArrayList(skillArg);
	}
}
