/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import java.util.Collection;

import me.magicall.game.Fighting;
import me.magicall.game.io.InputHandler;
import me.magicall.game.io.OperatingException;
import me.magicall.game.player.Player;
import me.magicall.game.sub.chess.Position;
import me.magicall.game.sub.chess.PositionSelector;

public abstract class SelectingTargetsSkillTemplate extends SkillTemplate implements TargetedSkill {

	protected SelectingTargetsSkillTemplate() {
	}

	@Override
	protected Collection<?> select(final Fighting fighting, final Player player) throws NoTargetSelectingException {
		final var tip = selectingTargetTip(fighting, player);
		player.getGameOutput().output(this, tip);

		class A implements InputHandler {
			Collection<?> units;

			@Override
			public void handle(final Object nextInput) throws OperatingException {
				final var targetSelector = getTargetSelector();
				try {
					units = targetSelector.select(fighting, () -> nextInput);
				} catch (final NoTargetToBeSelectingException e) {
					throw new OperatingException(e);
				}
			}
		}
		final var a = new A();
		try {
			player.getGameInput().requestInput(a);
		} catch (final OperatingException e) {
			throw new NoTargetSelectingException(e);
		}
		return a.units;
	}

	protected String selectingTargetTip(final Fighting fighting, final Player player) {
		return "选择目标：";
	}

	@Override
	public TargetSelector getTargetSelector() {
		return new PositionSelector();
	}

	@Override
	protected final void action(final Fighting fighting, final Player player, final Collection<?> targets)
			throws OperatingException {
		action(fighting, player, (Position) targets.iterator().next());
	}

	protected abstract void action(Fighting fighting, Player player, Position position) throws OperatingException;
}
