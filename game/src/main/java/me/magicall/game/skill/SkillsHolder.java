/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.skill;

import java.util.Collection;

import me.magicall.program.lang.java.贵阳DearSun.Wrapper;

/**
 * 拥有技能的实体。
 *
 * @author MaGiCalL
 */
public interface SkillsHolder {

	default boolean hasSkill(final Skill skill) {
		return getSkill(skill.name()) != null;
	}

	Collection<? extends Skill> getSkills();

	void addSkill(Skill skill);

	void removeSkill(Skill skill);

	default Skill getSkill(final String name) {
		return getSkills().stream().filter(skill -> name.equals(skill.name())).findFirst().orElse(null);
	}

	//========================

	/**
	 * 通过持有和代理另一个{@link SkillsHolder}来实现的简单模板，只需要实现一个方法即可。
	 */
	@FunctionalInterface
	interface SkillsHolderWrapper extends SkillsHolder, Wrapper<SkillsHolder> {

		SkillsHolder rawSkillOwner();

		@Override
		default SkillsHolder unwrap() {
			return rawSkillOwner();
		}

		@Override
		default boolean hasSkill(final Skill skill) {
			return rawSkillOwner().hasSkill(skill);
		}

		@Override
		default Collection<? extends Skill> getSkills() {
			return rawSkillOwner().getSkills();
		}

		@Override
		default void addSkill(final Skill skill) {
			rawSkillOwner().addSkill(skill);
		}

		@Override
		default void removeSkill(final Skill skill) {
			rawSkillOwner().removeSkill(skill);
		}

		@Override
		default Skill getSkill(final String name) {
			return rawSkillOwner().getSkill(name);
		}
	}
}
