/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game;

import java.io.Serial;

public class GameException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 8151255331280076580L;

	public GameException() {
		super();
	}

	public GameException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public GameException(final String message) {
		super(message);
	}

	public GameException(final Throwable cause) {
		super(cause);
	}
}
