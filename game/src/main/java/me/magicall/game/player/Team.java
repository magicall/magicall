/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.player;

import me.magicall.game.FightingElement;
import me.magicall.ui.HasProfile;

/**
 * <pre>
 * 游戏中的团队。
 * 同一个队伍里的所有玩家的胜利条件和失败条件都是一致的，同赢同输。
 * 有的游戏每个团队只有一个游戏角色（Fighter），有些则更多。
 * 比如，在象棋中，Team红方、黑方；
 * 在dota中，Team是近卫军团、天灾军团；
 * 在8人局三国杀中，Team是主忠方、反贼、内奸；
 * 在三国杀国战中，Team是魏国、蜀国、吴国、群雄、野心家；
 * </pre>
 *
 * @author MaGiCalL
 */
public interface Team extends HasProfile, FightingElement {

	Fighter[] getFighters();
}
