/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.player;

import java.util.Collection;

import me.magicall.game.io.GameInput;
import me.magicall.game.io.GameOutput;
import me.magicall.game.skill.Skill;
import me.magicall.game.skill.SkillsManager;

public class CommonPlayer implements Player {

	private final String name;

	//	private Fighting game;

	private GameInput gameInput;
	private GameOutput gameOutput;
	private SkillsManager skillManager;

	public CommonPlayer() {
		name = getClass().getSimpleName();
	}

	public CommonPlayer(final String name) {
		this.name = name;
	}

	@Override
	public GameInput getGameInput() {
		return gameInput;
	}

	public void setGameInput(final GameInput gameInput) {
		this.gameInput = gameInput;
	}

	@Override
	public GameOutput getGameOutput() {
		return gameOutput;
	}

	public void setGameOutput(final GameOutput gameOutput) {
		this.gameOutput = gameOutput;
	}

	@Override
	public String name() {
		return name;
	}

	//	public Fighting getFighting() {
	//		return game;
	//	}
	//
	//	public void setGame(final Fighting game) {
	//		this.game = game;
	//	}

	public boolean hasSkill(final Skill skill) {
		return skillManager.hasSkill(skill);
	}

	public Collection<Skill> getSkills() {
		return skillManager.getSkills();
	}

	public void addSkill(final Skill skill) {
		skillManager.addSkill(skill);
	}

	public void removeSkill(final Skill skill) {
		skillManager.removeSkill(skill);
	}

	public Skill getSkill(final String name) {
		return skillManager.getSkill(name);
	}
}
