/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.player;

import me.magicall.game.Fighting;
import me.magicall.ui.Profile;

public class CommonTeam implements Team {

	private final Fighting fighting;
	private final Profile profile;
	private final Fighter[] fighters;

	public CommonTeam(final Fighting fighting, final Profile profile, final Fighter... players) {
		super();
		this.fighting = fighting;
		this.profile = profile;
		fighters = players;
	}

	@Override
	public Fighter[] getFighters() {
		return fighters;
	}

	@Override
	public Profile getProfile() {
		return profile;
	}

	@Override
	public Fighting getFighting() {
		return fighting;
	}
}
