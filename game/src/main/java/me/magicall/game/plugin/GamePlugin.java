/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.plugin;

import me.magicall.game.Fighting;

/**
 * 游戏插件。是一种监听器，监听游戏即将开始、游戏即将结束时、游戏结束后。
 *
 * @author MaGiCalL
 */
public interface GamePlugin {

	/**
	 * 游戏即将开始时执行的操作。
	 */
	void gameStarting(Fighting fighting);

	/**
	 * 游戏即将结束时的操作。
	 */
	void gameEnding(Fighting fighting);

	/**
	 * 游戏结束后的操作。
	 */
	void gameEnded(Fighting fighting);
}
