/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.plugin;

import me.magicall.game.Fighting;
import me.magicall.game.config.GameOption;

/**
 * 游戏启动器的插件。是一种监听器，监听游戏启动器启动游戏前后、主机玩家进行游戏选项。
 *
 * @author MaGiCalL
 */
public interface GameLauncherPlugin {

	void beforeGameStart(Fighting fighting);

	void afterGame(Fighting fighting);

	void beforeConfigGameOption(GameOption gameOption);
}
