/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game;

import me.magicall.game.event.EventPublisher;
import me.magicall.game.event.EventTrigger;
import me.magicall.game.event.TriggerRegistryWrapper;
import me.magicall.game.map.Coordinate;
import me.magicall.game.player.Fighter;
import me.magicall.game.player.Team;
import me.magicall.game.unit.Unit;

/**
 * 一场游戏
 *
 * @author MaGiCalL
 */
public interface Fighting<_Coordinate extends Coordinate, _Unit extends Unit>
		extends EventPublisher, TriggerRegistryWrapper, EventTrigger {

	/**
	 * 游戏是否已经结束。
	 *
	 * @return
	 */
	boolean isFinished();

	/**
	 * 游戏开始。
	 */
	void start();

	<T extends Team> T[] getTeams();

	/**
	 * 获取记分牌。若已结束，则获取的是最终结果。
	 *
	 * @return
	 */
	<S extends Scoreboard> S getScoreboard();

	<F extends Fighter> F[] getFighters();

	void moveUnit(_Unit unit, _Coordinate coordinate);

	_Coordinate getCoordinate(_Unit unit);

	boolean hasUnit(_Coordinate coordinate);
}
