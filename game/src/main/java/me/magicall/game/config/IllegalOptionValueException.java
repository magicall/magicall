/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.config;

import me.magicall.game.io.OperatingException;

import java.io.Serial;

public class IllegalOptionValueException extends OperatingException {

	@Serial
	private static final long serialVersionUID = -6003967301543806084L;

	public IllegalOptionValueException() {
		super();
	}

	public IllegalOptionValueException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IllegalOptionValueException(final String message) {
		super(message);
	}

	public IllegalOptionValueException(final Throwable cause) {
		super(cause);
	}
}
