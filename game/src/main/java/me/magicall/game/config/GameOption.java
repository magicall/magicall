/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.config;

import java.util.List;

import me.magicall.game.map.GameMap;
import me.magicall.game.player.Player;

/**
 * 游戏的选项。
 *
 * @author MaGiCalL
 */
public interface GameOption<_Player extends Player, _Map extends GameMap> extends GameConfig<_Player, _Map> {

	void setMainPlayer(_Player player);

	/**
	 * 获取游戏的所有选项。
	 *
	 * @return
	 */
	List<OptionItem> getOptionItems();

	/**
	 * 增加一个选项。
	 */
	void addOptionItem(OptionItem optionItem);
}
