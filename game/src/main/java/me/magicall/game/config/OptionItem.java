/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.config;

import me.magicall.game.io.GameOutput;
import me.magicall.game.io.OutputTarget;
import me.magicall.Named;

/**
 * 游戏参数选项。
 *
 * @author MaGiCalL
 */
public interface OptionItem extends Named, OutputTarget {

	/**
	 * 输出本参数可能的值或者提示
	 */
	void showAvailableValues(GameOutput gameOutput);

	/**
	 * 设置本参数的值
	 */
	void setValue(GameOption gameOption, Object inputObject) throws IllegalOptionValueException;
}
