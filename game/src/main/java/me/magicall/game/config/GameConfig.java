/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game.config;

import me.magicall.game.map.GameMap;
import me.magicall.game.player.Player;

/**
 * 一场游戏的配置
 *
 * @author MaGiCalL
 */
public interface GameConfig<_Player extends Player, _Map extends GameMap> {

	/**
	 * 获取本场主要玩家。在联网游戏中是主机的玩家。
	 *
	 * @return
	 */
	_Player getMainPlayer();

	/**
	 * 获取本场游戏的所有玩家。
	 *
	 * @return
	 */
	_Player[] getPlayers();

	/**
	 * 获取本场游戏的玩家数量。
	 *
	 * @return
	 */
	int getPlayersCount();

	/**
	 * 获取初始化的地图。
	 *
	 * @return
	 */
	_Map getMap();
}
