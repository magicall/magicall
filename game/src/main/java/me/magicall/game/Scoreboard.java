/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.game;

import me.magicall.game.player.Team;

/**
 * 记分牌。
 *
 * @author Liang Wenjian.
 */
public interface Scoreboard {

	/**
	 * 获取领先者。
	 *
	 * @return
	 */
	Team getLeader();

	/**
	 * 获取落后者。
	 *
	 * @return
	 */
	Team getLaggard();
}
