/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.ssl;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import me.magicall.program.lang.java.贵阳DearSun.exception.NetworkException;

import java.util.Properties;

public class SSHConnection implements AutoCloseable {
	private static final int DEFAULT_SSH_PORT = 22;
	// 需要了可以打开
//	private static final String S_PATH_FILE_PRIVATE_KEY = "~/.ssh/id_rsa";
	// 需要了可以打开
//	private static final String S_PATH_FILE_KNOWN_HOSTS = "~/.ssh/known_hosts";

	private final JSch jsch;

	/**
	 * 自定义的中转端口，需要和数据源接口设置一样
	 */
	private int localPort;

	/**
	 * 连接到哪个服务端的SSH
	 */
	private String remoteServer;
	/**
	 * 服务器端SSH端口 默认是22
	 */
	private int remoteSshPort = DEFAULT_SSH_PORT;
	/**
	 * SSH用户名
	 */
	private String remoteSshUser;
	/**
	 * SSH密码
	 */
	private String remoteSshPassword;
	/**
	 * 服务端的本地数据库服务
	 */
	private String remoteDbServer;
	/**
	 * 服务端的本地数据库服务端口
	 */
	private int remoteDbPort;

	private Session session; //represents each ssh session

	public SSHConnection() {
		jsch = new JSch();
		// 需要用到了开启
//		jsch.setKnownHosts(S_PATH_FILE_KNOWN_HOSTS);
//		jsch.addIdentity(S_PATH_FILE_PRIVATE_KEY);
	}

	public void connect() {
		try {
			session = jsch.getSession(remoteSshUser, remoteServer, remoteSshPort);

			session.setPassword(remoteSshPassword);

			final Properties config = new Properties();
			config.setProperty("StrictHostKeyChecking", "no");
			session.setConfig(config);
			// 去连接
			session.connect();
			//  设置转发
			session.setPortForwardingL(localPort, remoteDbServer, remoteDbPort);
		} catch (final JSchException e) {
			throw new NetworkException(e);
		}
	}

	@Override
	public void close() {
		session.disconnect();
	}

	public int getLocalPort() {
		return localPort;
	}

	public void setLocalPort(final int localPort) {
		this.localPort = localPort;
	}

	public String getRemoteServer() {
		return remoteServer;
	}

	public void setRemoteServer(final String remoteServer) {
		this.remoteServer = remoteServer;
	}

	public int getRemoteSshPort() {
		return remoteSshPort;
	}

	public void setRemoteSshPort(final int remoteSshPort) {
		this.remoteSshPort = remoteSshPort;
	}

	public String getRemoteSshUser() {
		return remoteSshUser;
	}

	public void setRemoteSshUser(final String remoteSshUser) {
		this.remoteSshUser = remoteSshUser;
	}

	public String getRemoteSshPassword() {
		return remoteSshPassword;
	}

	public void setRemoteSshPassword(final String remoteSshPassword) {
		this.remoteSshPassword = remoteSshPassword;
	}

	public String getRemoteDbServer() {
		return remoteDbServer;
	}

	public void setRemoteDbServer(final String remoteDbServer) {
		this.remoteDbServer = remoteDbServer;
	}

	public int getRemoteDbPort() {
		return remoteDbPort;
	}

	public void setRemoteDbPort(final int remoteDbPort) {
		this.remoteDbPort = remoteDbPort;
	}
}

