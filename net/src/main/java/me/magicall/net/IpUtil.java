/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net;

import jakarta.servlet.http.HttpServletRequest;
import me.magicall.net.http.Http;

import java.util.Arrays;
import java.util.regex.Pattern;

public class IpUtil {

	private static final String QUOTED_IP_SEPARATOR = Pattern.quote(Http.IP_SEPARATOR);

	public static boolean isValidIpv4(final String ip) {
		if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			return false;
		}
		final var ss = ip.split(QUOTED_IP_SEPARATOR, -1);//当字符的最后就是分隔符时，负数控制不丢弃分割后最后的空字符串，默认的0则丢弃
		if (ss.length != 4) {
			return false;
		}
		for (final var s : ss) {
			try {
				final var i = Integer.parseInt(s.trim());
				if (i < 0 || i > 255) {
					return false;
				}
			} catch (final NumberFormatException ignored) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 取客户端的真实IP，考虑了反向代理等因素的干扰
	 *
	 * @return
	 */
	public static String getRemoteAddr(final HttpServletRequest request) {
		final var xffs = request.getHeaders("X-Forwarded-For");
		if (xffs.hasMoreElements()) {
			final var xff = xffs.nextElement();
			final var ip = resolveClientIPFromXFF(xff);
			if (isValidIpv4(ip)) {
				return ip;
			}
		}
		{
			final var ip = request.getHeader("Proxy-Client-IP");
			if (isValidIpv4(ip)) {
				return ip;
			}
		}
		{
			final var ip = request.getHeader("WL-Proxy-Client-IP");
			if (isValidIpv4(ip)) {
				return ip;
			}
		}
		return request.getRemoteAddr();
	}

	/**
	 * 从X-Forwarded-For头部中获取客户端的真实IP。
	 * X-Forwarded-For并不是RFC定义的标准HTTP请求Header
	 * ，可以参考http://en.wikipedia.org/wiki/X-Forwarded-For
	 *
	 * @param xff X-Forwarded-For头部的值
	 * @return 如果能够解析到client IP，则返回表示该IP的字符串，否则返回null
	 */
	private static String resolveClientIPFromXFF(final String xff) {
		final var ss = xff.split(",");
		for (var i = ss.length - 1; i >= 0; i--) {//x-forward-for链反向遍历
			final var ip = ss[i];
			if (isValidIpv4(ip)) { //判断ip是否合法，是否是公司机房ip
				return ip;
			}
		}

		//如果反向遍历没有找到格式正确的外网IP，那就正向遍历找到第一个格式合法的IP
		return Arrays.stream(ss).filter(IpUtil::isValidIpv4).findFirst().orElse(null);
	}

	public static void main(final String... args) {
		final String[] ips = {//
				"127.0.0.1",//
				" 255 . 255 . 255 . 255 ",//
				"0.0.0.0",//
				"256.1.1.1",//
				"a.b.c.d",//
				"1.1.1.1.1",//
				"1.1.1",//
				"1.1.1.1.",//
				".1.1.1.1",//
		};
		Arrays.stream(ips).map(s -> "@@@@@@" + isValidIpv4(s)).forEach(System.out::println);
	}
}
