/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.socket;

import me.magicall.program.lang.java.贵阳DearSun.io.IOKit;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;

public class ObjectSenderClient {
	private final String host;
	private final int port;

	public ObjectSenderClient(final String host, final int port) {
		this.host = host;
		this.port = port;
	}

	public Object send(final Serializable obj) {
		final var client = new Client(host, port);

		class SendObjSocketHandler implements SocketHandler {

			private ObjectOutputStream objectOutputStream;
			private Object returnFromServer;

			public SendObjSocketHandler() {
				super();
			}

			@Override
			public void handleSocket(final Socket socket) {
			}

			@Override
			public void handleStream(final InputStream inputStream, final OutputStream outputStream) {
				try {
					objectOutputStream = new ObjectOutputStream(outputStream);
					objectOutputStream.writeObject(obj);
					objectOutputStream.flush();

					// 注:关闭ObjectInputStream将关闭它包装的InputStream,而关闭这个从Socket中来的inputStream将关闭Socket!
					// 因此此时这个ObjectInputStream不能close!我靠
					final var objectInputStream = new ObjectInputStream(inputStream);
					final var obj = objectInputStream.readObject();
					returnFromServer = obj;
				} catch (final IOException e) {
					e.printStackTrace();
				} catch (final ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}

		final var socketHandler = new SendObjSocketHandler();
		try {
			client.add(socketHandler);
			client.connect();
			return socketHandler.returnFromServer;
		} finally {
			IOKit.close(socketHandler.objectOutputStream);
		}
	}
}
