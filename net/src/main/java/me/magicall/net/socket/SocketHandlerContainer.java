/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.socket;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.coll.ListWrapper;

import java.io.IOException;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketHandlerContainer implements ListWrapper<SocketHandler> {

	protected final int port;
	protected final ExecutorService pool;
	private final List<SocketHandler> socketHandlers = Lists.newArrayList();

	protected SocketHandlerContainer(final int port) {
		this(port, 0);
	}

	protected SocketHandlerContainer(final int port, final int poolSize) {
		this.port = port;
		pool = Executors.newFixedThreadPool(poolSize == 0 ? 8 : poolSize);
	}

	protected void handleSocket(final Socket socket) {
		pool.execute(() -> handleSocketInternal(socket));
	}

	protected void handleSocketInternal(final Socket socket) {
		socketHandlers.forEach(h -> h.handleSocket(socket));

		try (final var inputStream = socket.getInputStream(); final var outputStream = socket.getOutputStream()) {
			socketHandlers.forEach(h -> h.handleStream(inputStream, outputStream));
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<SocketHandler> unwrap() {
		return socketHandlers;
	}

	public void add(final me.magicall.net.socket.SocketHandler... elements) {
		Collections.addAll(unwrap(), elements);
	}

	public void add(final Collection<? extends SocketHandler> elements) {
		unwrap().addAll(elements);
	}

	public boolean add(final int index, final Collection<? extends SocketHandler> c) {
		return unwrap().addAll(index, c);
	}

	public boolean add(final int index, final me.magicall.net.socket.SocketHandler... elements) {
		return add(index, Arrays.asList(elements));
	}

	public boolean remove(final Object... targets) {
		return remove(Arrays.asList(targets));
	}

	public boolean remove(final Collection<?> targets) {
		return unwrap().removeAll(targets);
	}
}
