/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.socket;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.IOException;
import java.net.Socket;

public class Client extends SocketHandlerContainer {

	private final String host;

	public Client(final String host, final int port) {
		this(host, port, 0);
	}

	public Client(final String host, final int port, final int poolSize) {
		super(port, poolSize);
		this.host = host;
	}

	public void connect() {
		try {
			handleSocket(new Socket(host, port));
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
	}
}
