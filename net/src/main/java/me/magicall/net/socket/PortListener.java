/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.socket;

import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.io.IOKit;

import java.io.IOException;
import java.net.ServerSocket;

public class PortListener extends SocketHandlerContainer {

	private boolean listenOnce;

	private ServerSocket serverSocket;

	public PortListener(final int port, final int poolSize) {
		super(port, poolSize);
	}

	public PortListener(final int port, final int poolSize, final boolean listenOnce) {
		super(port, poolSize);
		this.listenOnce = listenOnce;
	}

	public void listen() throws IOException {
		close();
		serverSocket = new ServerSocket(port);
		System.out.println("@@@@@@PortListener.listen():listening " + port);
		pool.submit(() -> {
			do {
				try {
					handleSocket(serverSocket.accept());
				} catch (final IOException e) {
					e.printStackTrace();
				}
			} while (serverSocket != null && !serverSocket.isClosed() && !isListenOnce());
		});
	}

	public boolean isListenOnce() {
		return listenOnce;
	}

	public void setListenOnce(final boolean listenOnce) {
		this.listenOnce = listenOnce;
	}

	public void close() {
		final var e = IOKit.close(serverSocket);
		if (e != null) {
			throw new UnknownException(e);
		}
		serverSocket = null;
	}

	public int getPort() {
		return port;
	}
}
