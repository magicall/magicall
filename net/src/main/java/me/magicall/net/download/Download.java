/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.download;

import me.magicall.net.http.OfficialRequestHeader;
import me.magicall.net.http.UrlConnector;
import me.magicall.net.http.UrlConnectorConfig;
import me.magicall.net.http.UrlConnectorContentHandler;
import me.magicall.time.TimeConst;
import me.magicall.time.TimeFormatter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Date;

public class Download {

	//	private int threadsCount;
	private final UrlConnectorConfig config;
	private File target;

	public Download(final UrlConnectorConfig config) {
		this.config = config;
	}

	public static void main(final String... args) {
		System.out.println("@@@@@@start");
		final var config = new UrlConnectorConfig();
		config.setConnectTimeout((int) TimeConst.MINUTE);
		//		config.setCookies(null);
		config.setReadTimeout((int) TimeConst.MINUTE);
		config.setUrl("http://img.24meinv.com/hdmv/simei520/simei8/DSC00020%20.jpg");

		final var download = new Download(config);
		download.setTarget(new File("d:\\" + TimeFormatter.Y4M2D2H2MIN2S2MS3.format(new Date()) + ".jpg"));
		download.download();
	}

	public void download() {
		checkDir();
		final var dataCache = new DataCache();
		OutputStream out = null;
		//		new Thread() {
		//			@Override
		//			public void run() {
		try {
			final var connector = new UrlConnector(config);
			final var end = 100;
			final var start = 0;
			connector.add(new SetPart(start, end));
			connector.add(dataCache);
			connector.downBytes();
			out = new FileOutputStream(target);
			dataCache.byteArrayOutputStream.writeTo(out);
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
		//			}
		//		}.start();
	}

	private void checkDir() {
		final var parent = target.getParentFile();
		if (!parent.exists()) {
			if (!parent.mkdirs()) {
				System.out.println("@@@@@@mkdirs fail");
				return;
			}
		}
	}

	public File getTarget() {
		return target;
	}

	public void setTarget(final File target) {
		this.target = target;
	}

	private static class SetPart implements UrlConnectorContentHandler {

		int start;
		int end;

		public SetPart(final int start, final int end) {
			super();
			this.start = start;
			this.end = end;
		}

		@Override
		public void beforeConnect(final HttpURLConnection conn) {
			OfficialRequestHeader.Range.set(conn, "bytes=" + start + '-' + (end - 1));
		}

		@Override
		public void handleData(final byte[] bs) {
		}
	}

	private static class DataCache implements UrlConnectorContentHandler {

		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		@Override
		public void beforeConnect(final HttpURLConnection conn) {
		}

		@Override
		public void handleData(final byte[] bs) {
			try {
				byteArrayOutputStream.write(bs);
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}
}
