/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.ListWrapper;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UrlConnector implements ListWrapper<UrlConnectorContentHandler> {

	private final UrlConnectorConfig config;
	private final List<UrlConnectorContentHandler> urlConnectorContentHandlers = Lists.newArrayList();

	public UrlConnector(final UrlConnectorConfig config) {
		this.config = config;
	}

	public void downBytes() {
		final var url = config.getUrl();
		final var urlObject = Http.parseOrNull(url);
		if (urlObject == null) {
			return;
		}
		final var conn = buildConnection(urlObject);
		buildConnection(url, conn);
		handleBeforeDownload(conn);
		final var code = connect(conn);
		if (Http.isSuccess(code)) {
			final ByteArrayOutputStream byteArrayOutputStream;
			try (final var in = conn.getInputStream()) {
				final var contentLen = conn.getContentLength();
				var readSteamEachSize = config.getReadSteamEachSize();
				final var contentRange = conn.getHeaderField("Content-Range");
				if (contentRange != null) {
					final int fetchedTotalBytes = Kits.INT.fromString(StrKit.subStringAfter(contentRange, "/"));
					if (readSteamEachSize != fetchedTotalBytes) {
						readSteamEachSize = fetchedTotalBytes;
					}
				}
				byteArrayOutputStream = contentLen < 0 ? new ByteArrayOutputStream() : new ByteArrayOutputStream(contentLen);
				final var tmp = new byte[(int) readSteamEachSize];
				try {
					for (var r = in.read(tmp); r != -1; r = in.read(tmp)) {
						byteArrayOutputStream.write(tmp, 0, r);
					}
				} catch (final IOException e) {
					throw new UnknownException(e);
				}
			} catch (final IOException e) {
				throw new UnknownException(e);
			}

			conn.disconnect();

			final var bs = byteArrayOutputStream.toByteArray();
			handleDownloaded(bs);
		} else {
		}
	}

	public int connect(final HttpURLConnection conn) {
		try {
			conn.connect();
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
		final int code;
		try {
			code = conn.getResponseCode();
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
		return code;
	}

	private static HttpURLConnection buildConnection(final URL urlObject) {
		try {
			final var conn = (HttpURLConnection) urlObject.openConnection();
			return conn;
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
	}

	private void handleBeforeDownload(final HttpURLConnection conn) {
		getUrlConnectorContentHandlers().forEach(handler -> handler.beforeConnect(conn));
	}

	private void handleDownloaded(final byte[] bs) {
		getUrlConnectorContentHandlers().forEach(handler -> handler.handleData(bs));
	}

	private void buildConnection(final String url, final HttpURLConnection conn) {
		final var host = Http.hostOf(url);
		final var reffer = StrKit.subStringBefore(url, "/", true);

		try {
			conn.setRequestMethod("GET");
		} catch (final ProtocolException e) {
			throw new UnknownException(e);
		}

		//请求头
		OfficialRequestHeader.Host.set(conn, host);
		OfficialRequestHeader.User_Agent.set(conn, config.getRequestSender().getUserAgent());

		final var acceptLanguage = config.getAcceptLanguage();
		if (!Kits.STR.isEmpty(acceptLanguage)) {
			OfficialRequestHeader.Accept_Language.set(conn, acceptLanguage);
		}

		final var acceptEncoding = config.getAcceptEncoding();
		if (!Kits.STR.isEmpty(acceptEncoding)) {
			OfficialRequestHeader.Accept_Encoding.set(conn, acceptEncoding);
		}

		final var acceptCharset = config.getAcceptCharset();
		if (!Kits.STR.isEmpty(acceptCharset)) {
			OfficialRequestHeader.Accept_Charset.set(conn, acceptCharset);
		}

		final var keepAlive = config.getKeepAlive();
		if (!Kits.STR.isEmpty(keepAlive)) {
			OfficialRequestHeader.Keep_Alive.set(conn, keepAlive);
		}

		final var headConnection = config.getHeadConnection();
		if (!Kits.STR.isEmpty(headConnection)) {
			OfficialRequestHeader.Connection.set(conn, headConnection);
		}
		OfficialRequestHeader.Referer.set(conn, reffer);

		conn.setConnectTimeout(config.getConnectTimeout());
		conn.setReadTimeout(config.getReadTimeout());
	}

	protected List<UrlConnectorContentHandler> getUrlConnectorContentHandlers() {
		return unwrap();
	}

	@Override
	public List<UrlConnectorContentHandler> unwrap() {
		return urlConnectorContentHandlers;
	}

	public void add(final UrlConnectorContentHandler... elements) {
		Collections.addAll(unwrap(), elements);
	}

	public void add(final Collection<? extends UrlConnectorContentHandler> elements) {
		unwrap().addAll(elements);
	}

	public boolean add(final int index, final Collection<? extends UrlConnectorContentHandler> c) {
		return unwrap().addAll(index, c);
	}

	public boolean add(final int index, final UrlConnectorContentHandler... elements) {
		return add(index, Arrays.asList(elements));
	}

	public boolean remove(final Object... targets) {
		return remove(Arrays.asList(targets));
	}

	public boolean remove(final Collection<?> targets) {
		return unwrap().removeAll(targets);
	}
}
