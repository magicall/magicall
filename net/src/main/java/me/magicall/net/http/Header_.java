/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

/**
 * @author Liang Wenjian.
 */
public class Header_<V> implements Header<V> {
	private final String name;
	private final V value;

	public Header_(final String name) {
		this(name, null);
	}

	public Header_(final String name, final V value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public Header<V> withValue(final V value) {
		return new Header_<>(name(), value);
	}
}
