/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import jakarta.servlet.http.HttpServletRequest;

import java.net.HttpURLConnection;

@Deprecated
public enum OfficialRequestHeader {
	Accept,
	//
	Accept_Encoding,
	//
	Accept_Language,
	//
	Accept_Charset,
	//
	Cache_Control,
	//
	Connection,
	//
	Cookie,
	//
	Host,
	//
	Keep_Alive,
	//
	Range,
	//
	Referer,
	//
	User_Agent,
	//
	;

	private final String headName;

	OfficialRequestHeader() {
		headName = name().replace('_', '-');
	}

	public String getValue(final HttpServletRequest request) {
		return request.getHeader(getHeadName());
	}

	public String getHeadName() {
		return headName;
	}

	public void set(final HttpURLConnection conn, final Object value) {
		conn.setRequestProperty(headName, String.valueOf(value));
	}
}
