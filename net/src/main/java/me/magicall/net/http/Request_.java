/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.util.Set;

/**
 * @author Liang Wenjian.
 */
public class Request_ implements Request {

	private final RequestLine requestLine;
	private final Set<Header<?>> headers;
	private final String body;

	public Request_(final RequestLine requestLine, final Set<Header<?>> headers, final String body) {
		this.requestLine = requestLine;
		this.headers = headers;
		this.body = body;
	}

	@Override
	public RequestLine getRequestLine() {
		return requestLine;
	}

	@Override
	public Request addCookie(final String name, final String value) {
		return null;
	}

	@Override
	public Set<Header<?>> getHeaders() {
		return headers;
	}

	@Override
	public String getBody() {
		return body;
	}

	@Override
	public Request with(final RequestLine startLine) {
		return new Request_(startLine, getHeaders(), getBody());
	}

	@Override
	public Request with(final Header<?> header) {
		headers.add(header);
		return new Request_(getStartLine(), getHeaders(), getBody());
	}

	@Override
	public Request withBody(final Object body) {
		return new Request_(getRequestLine(), getHeaders(), String.valueOf(body));
	}

	@Override
	public Request withBody(final Object body, final String contentType) {
		return null;//todo
	}
}
