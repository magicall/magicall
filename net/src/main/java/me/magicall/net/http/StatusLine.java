/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

/**
 * @author Liang Wenjian.
 */
public interface StatusLine extends StartLine<StatusLine> {

	Status getStatus();

	default int getStatusCode() {
		return getStatus().getCode();
	}

	default String getReasonPhrase() {
		return getStatus().getReasonPhrase();
	}

	StatusLine toStatus(Status status);

	default StatusLine toStatus(final int code) {
		return toStatus(getStatus().to(code));
	}

	@Override
	default StringBuilder appendTo(final StringBuilder stringBuilder) {
		getVersion().appendTo(stringBuilder);
		stringBuilder.append(' ');
		return getStatus().appendTo(stringBuilder);
	}
}
