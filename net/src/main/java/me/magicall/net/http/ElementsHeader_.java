/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Sets;
import me.magicall.net.http.ElementsHeader.Element;

import java.util.Set;

/**
 * @author Liang Wenjian.
 */
public class ElementsHeader_ extends Header_<Set<Element>> implements ElementsHeader {

	public ElementsHeader_(final String name, final Set<Element> value) {
		super(name, value);
	}

	@Override
	public ElementsHeader withValue(final Set<Element> value) {
		return new ElementsHeader_(name(), value);
	}

	@Override
	public ElementsHeader add(final Element element) {
		final Set<Element> value = Sets.newHashSet(getValue());
		value.add(element);
		return new ElementsHeader_(name(), value);
	}

	@Override
	public Set<Element> getElements() {
		return getValue();
	}
}
