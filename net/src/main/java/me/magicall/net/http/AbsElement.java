/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Maps;
import me.magicall.net.http.ElementsHeader.Element;

import java.util.Collections;
import java.util.Map;

/**
 * @author Liang Wenjian.
 */
public abstract class AbsElement implements Element {

	private final Map<String, String> params;

	protected AbsElement() {
		this(Collections.emptyMap());
	}

	protected AbsElement(final Map<String, String> params) {
		this.params = Maps.newHashMap(params);
	}

	@Override
	public Map<String, String> getParams() {
		return Collections.unmodifiableMap(params);
	}
}
