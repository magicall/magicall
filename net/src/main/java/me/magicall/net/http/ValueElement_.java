/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Maps;
import me.magicall.net.http.ElementsHeader.Element.ValueElement;

import java.util.Map;

/**
 * @author Liang Wenjian.
 */
public class ValueElement_<V> extends AbsElement implements ValueElement<V> {

	private final V value;

	public ValueElement_(final V value) {
		this.value = value;
	}

	public ValueElement_(final V value, final Map<String, String> params) {
		super(params);
		this.value = value;
	}

	@Override
	public ValueElement_<V> withParam(final String name, final String value) {
		final Map<String, String> map = Maps.newHashMap(getParams());
		map.put(name, value);
		return new ValueElement_<>(getValue(), map);
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public ValueElement_<V> withValue(final V value) {
		return new ValueElement_<>(getValue(), getParams());
	}
}
