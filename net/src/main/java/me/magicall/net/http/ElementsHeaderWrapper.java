/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Sets;
import me.magicall.program.lang.LabelStyle;

import java.util.Set;

/**
 * @author Liang Wenjian.
 */
public abstract class ElementsHeaderWrapper implements ElementsHeader {

	private final ElementsHeader raw;

	protected ElementsHeaderWrapper(final String headerElementName, final String headerElementValue) {
		this(new NameValueElement_(headerElementName, headerElementValue));
	}

	protected ElementsHeaderWrapper(final String headerValue) {
		this(new ValueElement_<>(headerValue));
	}

	protected ElementsHeaderWrapper(final Element element) {
		final var headerName = LabelStyle.CAMEL.convertTo(LabelStyle.MINUS_SEPARATED, getClass().getSimpleName());
		raw = new ElementsHeader_(headerName, Sets.newHashSet(element));
	}

	protected ElementsHeaderWrapper(final ElementsHeader raw) {
		this.raw = raw;
	}

	@Override
	public StringBuilder appendTo(final StringBuilder stringBuilder) {
		return raw.appendTo(stringBuilder);
	}

	@Override
	public String name() {
		return raw.name();
	}

	@Override
	public Set<Element> getValue() {
		return raw.getValue();
	}

	@Override
	public Set<Element> getElements() {
		return raw.getElements();
	}
}
