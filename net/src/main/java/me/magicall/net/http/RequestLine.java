/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.net.URI;

/**
 * @author Liang Wenjian.
 */
public interface RequestLine extends StartLine<RequestLine> {

	Method getMethod();

	URI getUri();

	RequestLine with(Method method);

	RequestLine with(URI uri);

	@Override
	default StringBuilder appendTo(final StringBuilder stringBuilder) {
		getMethod().appendTo(stringBuilder);
		stringBuilder.append(' ').append(getUri().getPath()).append(' ');
		return getVersion().appendTo(stringBuilder);
	}
}
