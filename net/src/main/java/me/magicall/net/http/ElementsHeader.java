/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Sets;
import me.magicall.Named;
import me.magicall.net.http.ElementsHeader.Element;
import me.magicall.program.lang.LabelStyle;

import java.util.Map;
import java.util.Set;

/**
 * <pre>
 * header  = [ element ] *( "," [ element ] )
 * element = name [ "=" [ value ] ] *( ";" [ param ] )
 * param   = name [ "=" [ value ] ]
 *
 * name    = token
 * value   = ( token | quoted-string )
 *
 * token         = 1*&lt;any char except "=", ",", ";", &lt;"&gt; and
 *                       white space&gt;
 * quoted-string = &lt;"&gt; *( text | quoted-char ) &lt;"&gt;
 * text          = any char except &lt;"&gt;
 * quoted-char   = "\" char
 * </pre>
 */
public interface ElementsHeader extends Header<Set<Element>> {
	@Override
	default String name() {
		return LabelStyle.CAMEL.convertTo(LabelStyle.MINUS_SEPARATED, getClass().getSimpleName());
	}

	@Override
	default Set<Element> getValue() {
		return getElements();
	}

	Set<Element> getElements();

	@Override
	ElementsHeader withValue(Set<Element> value);

	@Override
	default ElementsHeader clearValue() {
		return withValue(Sets.newHashSet());
	}

	ElementsHeader add(Element element);

	@Override
	default StringBuilder appendTo(final StringBuilder stringBuilder) {
		stringBuilder.append(name()).append(':');
		final var value = getValue();
		if (value != null) {
			var first = true;
			for (final var element : value) {
				if (first) {
					first = false;
				} else {
					stringBuilder.append(',');
				}
				element.appendTo(stringBuilder);
			}
		}
		return stringBuilder.append(HEADER_SEPARATOR);
	}

	//======================================

	interface Element extends HttpPart {

		Map<String, String> getParams();

		default int getParamCount() {
			final Map<String, ?> params = getParams();
			if (params != null) {
				return params.size();
			}
			return 0;
		}

		default String getParamByName(final String name) {
			final var params = getParams();
			if (params != null) {
				return params.get(name);
			}
			return null;
		}

		Element withParam(String name, String value);

		//==========================================

		/**
		 * 例子：
		 * <pre>
		 * Accept-Language: zh-CN,zh;q=0.8
		 * </pre>
		 */
		interface ValueElement<V> extends Element {

			V getValue();

			ValueElement<V> withValue(V value);

			@Override
			default StringBuilder appendTo(final StringBuilder stringBuilder) {
				final Object value = getValue();
				if (value != null) {
					stringBuilder.append('=').append(value.toString());
				}
				return appendParams(stringBuilder, this);
			}
		}//ValueElement

		interface NameValueElement extends Element, Named {
			String getValue();

			NameValueElement withValue(String value);

			default boolean hasValue() {
				return getValue() == null;
			}

			@Override
			default StringBuilder appendTo(final StringBuilder stringBuilder) {
				stringBuilder.append(name());
				final var value = getValue();
				if (value != null) {
					stringBuilder.append('=').append(value);
				}
				return appendParams(stringBuilder, this);
			}
		}//NameValueElement
	}//Element

	private static StringBuilder appendParams(final StringBuilder appendable, final Element element) {
		final var params = element.getParams();
		if (params != null) {
			appendable.append(';');
			params.entrySet().forEach(entry -> {
				appendable.append(entry.getKey());
				final var value = entry.getValue();
				if (value != null) {
					appendable.append('=').append(value);
				}
			});
		}
		return appendable;
	}
}//ElementHeader
