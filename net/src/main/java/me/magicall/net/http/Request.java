/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;

import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Liang Wenjian.
 */
public interface Request extends Message<Request, RequestLine> {

	@Override
	default RequestLine getStartLine() {
		return getRequestLine();
	}

	RequestLine getRequestLine();

	default Method getMethod() {
		return getStartLine().getMethod();
	}

	default URI getUri() {
		return getStartLine().getUri();
	}

	default Set<Cookie> getCookies() {
		return getHeaders().stream().filter(e -> Cookie.class.isAssignableFrom(e.getClass())).map(Cookie.class::cast)
				.collect(Collectors.toSet());
	}

	default Cookie getCookie(final String cookieName) {
		return CollKit.first(getCookies(), Named.predicate(cookieName));
	}

	default Map<String, String> getStrCookies() {
		final Map<String, String> rt = getCookies().stream()
				.collect(Collectors.toMap(Named::name, Header::getValue, (a, b) -> b));
		return rt;
	}

	default Request with(final Method method) {
		return with(getRequestLine().with(method));
	}

	default Request with(final URI uri) {
		return with(getRequestLine().with(uri));
	}

	Request addCookie(String name, String value);

	default Request addCookies(final Collection<Cookie> cookies) {
		cookies.forEach(cookie -> addCookie(cookie.name(), cookie.getValue()));
		return this;
	}
}
