/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.coll.CollKit;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 提供只读接口的消息接口。
 *
 * @author Liang Wenjian.
 */
public interface Message<M extends Message<M, L>, L extends StartLine<L>> extends HttpPart {

	StartLine<L> getStartLine();

	default Version getVersion() {
		return getStartLine().getVersion();
	}

	Set<Header<?>> getHeaders();

	default Header<?> getHeader(final String headerName) {
		return CollKit.first(getHeaders(), Named.predicate(headerName));
	}

	default String getStrHeader(final String headerName) {
		return HttpPart.str(getHeader(headerName));
	}

	default Map<String, String> getStrHeaders() {
		final Map<String, String> rt = getHeaders().stream()
				.collect(Collectors.toMap(Named::name, header -> HttpPart.str(header), (a, b) -> b));
		return rt;
	}

	String getBody();

	M with(L startLine);

	default M with(final Version version) {
		return with(getStartLine().with(version));
	}

	M with(Header<?> header);

	M withBody(Object body);

	M withBody(Object body, String contentType);

	@Override
	default StringBuilder appendTo(final StringBuilder stringBuilder) {
		getStartLine().appendTo(stringBuilder);
		stringBuilder.append(Http.LINE_SEPARATOR);
		getHeaders().forEach(header -> {
			header.appendTo(stringBuilder);
			stringBuilder.append(Http.LINE_SEPARATOR);
		});
		return stringBuilder.append(Http.LINE_SEPARATOR).append(getBody());
	}
}
