/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

/**
 * @author Liang Wenjian.
 */
public class StatusLine_ implements StatusLine {

	private final Version version;
	private final Status status;

	public StatusLine_(final Version version, final Status status) {
		this.version = version;
		this.status = status;
	}

	@Override
	public Version getVersion() {
		return version;
	}

	@Override
	public StatusLine with(final Version version) {
		return new StatusLine_(version, getStatus());
	}

	@Override
	public Status getStatus() {
		return status;
	}

	@Override
	public StatusLine toStatus(final Status status) {
		return new StatusLine_(getVersion(), status);
	}
}
