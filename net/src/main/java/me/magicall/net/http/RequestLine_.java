/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.net.URI;

/**
 * @author Liang Wenjian.
 */
public class RequestLine_ implements RequestLine {

	private final Method method;
	private final URI uri;
	private final Version version;

	public RequestLine_(final URI uri) {
		this(uri, HttpVersion._2_0);
	}

	public RequestLine_(final URI uri, final Version version) {
		this(OfficialMethod.GET, uri, version);
	}

	public RequestLine_(final Method method, final URI uri) {
		this(method, uri, HttpVersion._2_0);
	}

	public RequestLine_(final Method method, final URI uri, final Version version) {
		this.method = method;
		this.uri = uri;
		this.version = version;
	}

	@Override
	public Method getMethod() {
		return method;
	}

	@Override
	public URI getUri() {
		return uri;
	}

	@Override
	public Version getVersion() {
		return version;
	}

	@Override
	public RequestLine with(final Method method) {
		return new RequestLine_(method, getUri(), getVersion());
	}

	@Override
	public RequestLine with(final URI uri) {
		return new RequestLine_(getMethod(), uri, getVersion());
	}

	@Override
	public RequestLine with(final Version version) {
		return new RequestLine_(getMethod(), getUri(), version);
	}
}
