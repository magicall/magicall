/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.time.MayEnd;

import java.util.Date;

/**
 * @author Liang Wenjian.
 */
public interface Cookie extends Header<String>, MayEnd {

	String getAttribute(String name);

	default boolean containsAttribute(final String name) {
		return getAttribute(name) != null;
	}

	String getComment();

	String getCommentURL();

	Date getExpiryDate();

	boolean isPersistent();

	String getDomain();

	String getPath();

	int[] getPorts();

	boolean isSecure();

	int getVersion();
}
