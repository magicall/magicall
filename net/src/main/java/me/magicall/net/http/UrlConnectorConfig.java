/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.computer.SizeUnit;
import me.magicall.program.lang.java.贵阳DearSun.coll.TwoTuple;
import me.magicall.time.TimeConst;

import java.nio.charset.Charset;
import java.util.SortedSet;
import java.util.TreeSet;

public class UrlConnectorConfig {
	private final RequestSender requestSender = CommonWebBrowsers.CHROME;
	private final SortedSet<TwoTuple<Charset, Float>> charsets = new TreeSet<>(
			(o1, o2) -> o2.second.compareTo(o1.second));
	private String url;
	//	private int possibleSize = SizeUnit.KB.toBytes();
	private int connectTimeout = (int) TimeConst.MINUTE;
	private int readTimeout = (int) TimeConst.MINUTE;
	private long readSteamEachSize = SizeUnit.KB.toBytes(4);
	private String cookies;
	private String acceptMetaType = "*/*";
	private String acceptLanguage = "zh-cn,zh;q=0.5";
	private String acceptEncoding = "gzip,deflate";
	//	private String acceptCharset = "GB2312,utf-8;q=0.7,*;q=0.7";
	private String keepAlive = "100";
	private String headConnection = "keep-alive";

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	//	public void setMetaType(final MetaType metaType) {
	//		setPossibleSize(metaType.possibleSize);
	//	}
	//
	//	public int getPossibleSize() {
	//		return possibleSize;
	//	}
	//
	//	public void setPossibleSize(final int possibleSize) {
	//		this.possibleSize = possibleSize;
	//	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(final int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(final int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public long getReadSteamEachSize() {
		return readSteamEachSize;
	}

	public void setReadSteamEachSize(final int readSteamEachSize) {
		this.readSteamEachSize = readSteamEachSize;
	}

	public String getCookies() {
		return cookies;
	}

	public void setCookies(final String cookies) {
		this.cookies = cookies;
	}

	public String getAcceptMetaType() {
		return acceptMetaType;
	}

	public void setAcceptMetaType(final String acceptMetaType) {
		this.acceptMetaType = acceptMetaType;
	}

	public String getAcceptLanguage() {
		return acceptLanguage;
	}

	public void setAcceptLanguage(final String acceptLanguage) {
		this.acceptLanguage = acceptLanguage;
	}

	public String getAcceptEncoding() {
		return acceptEncoding;
	}

	public void setAcceptEncoding(final String acceptEncoding) {
		this.acceptEncoding = acceptEncoding;
	}

	public String getAcceptCharset() {
		if (charsets.isEmpty()) {
			//			return "GBK,utf-8;q=0.7,*;q=0.7";
			return "*";
		}
		final var sb = new StringBuilder();
		charsets.forEach(t -> {
			sb.append(t.first);
			if (t.second != 1) {
				sb.append(";q=").append(t.second);
			}
			sb.append(',');
		});
		return sb.deleteCharAt(sb.length() - 1).toString();
		//		return acceptCharset;
	}

	//	public void setAcceptCharset(final String acceptCharset) {
	//		this.acceptCharset = acceptCharset;
	//	}

	public String getKeepAlive() {
		return keepAlive;
	}

	public void setKeepAlive(final String keepAlive) {
		this.keepAlive = keepAlive;
	}

	public String getHeadConnection() {
		return headConnection;
	}

	public void setHeadConnection(final String headConnection) {
		this.headConnection = headConnection;
	}

	public RequestSender getRequestSender() {
		return requestSender;
	}

	public Charset getFirstCharset() {
		return charsets.isEmpty() ? null : charsets.first().first;
	}

	public UrlConnectorConfig addCharset(final Charset charset, final float weight) {
		charsets.add(new TwoTuple<>(charset, weight));
		return this;
	}

	public UrlConnectorConfig addCharset(final Charset charset) {
		charsets.add(new TwoTuple<>(charset, 1.0f));
		return this;
	}
}
