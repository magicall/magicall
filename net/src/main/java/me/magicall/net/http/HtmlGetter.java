/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.net.HttpURLConnection;

public class HtmlGetter {

	private final UrlConnectorConfig config;

	public HtmlGetter(final UrlConnectorConfig config) {
		super();
		this.config = config;
	}

	public String get() {
		final var connector = new UrlConnector(config);
		final var toString = new NetStreamToString();
		connector.add(toString);
		connector.downBytes();
		return toString.getContent();
	}

	private class NetStreamToString implements UrlConnectorContentHandler {
		private String content;

		@Override
		public void beforeConnect(final HttpURLConnection conn) {
		}

		@Override
		public void handleData(final byte[] bs) {
			final var charset = config.getFirstCharset();
			content = charset == null ? new String(bs) : new String(bs, charset);
		}

		public String getContent() {
			return content;
		}
	}
}
