/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.net.http.ElementsHeader.Element;
import me.magicall.net.http.ElementsHeader.Element.ValueElement;

import java.util.Map;

/**
 * @author Liang Wenjian.
 */
public abstract class AbsValueElement<V> implements ValueElement<V> {

	protected final ValueElement_<V> raw;

	protected AbsValueElement(final V value) {
		raw = new ValueElement_<>(value);
	}

	protected AbsValueElement(final ValueElement_<V> raw) {
		this.raw = raw;
	}

	@Override
	public V getValue() {
		return raw.getValue();
	}

	@Override
	public ValueElement<V> withValue(final V value) {
		return newInstanceWithRawHeader(raw.withValue(value));
	}

	@Override
	public Map<String, String> getParams() {
		return raw.getParams();
	}

	@Override
	public Element withParam(final String name, final String value) {
		return newInstanceWithRawHeader(raw.withParam(name, value));
	}

	protected abstract ValueElement<V> newInstanceWithRawHeader(ValueElement_<V> localeValueElement);
}
