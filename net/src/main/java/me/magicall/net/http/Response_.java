/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.util.Set;

/**
 * @author Liang Wenjian.
 */
public class Response_ implements Response {
	private final StatusLine statusLine;
	private final Set<Header<?>> headers;
	private final String body;

	public Response_(final StatusLine statusLine, final Set<Header<?>> headers, final String body) {
		this.statusLine = statusLine;
		this.headers = headers;
		this.body = body;
	}

	@Override
	public StatusLine getStatusLine() {
		return statusLine;
	}

	@Override
	public Set<Header<?>> getHeaders() {
		return headers;
	}

	@Override
	public String getBody() {
		return body;
	}

	@Override
	public Response with(final StatusLine startLine) {
		return new Response_(startLine, getHeaders(), getBody());
	}

	@Override
	public Response with(final Header<?> header) {
		headers.add(header);
		return new Response_(getStatusLine(), getHeaders(), getBody());
	}

	@Override
	public Response withBody(final Object body) {
		return new Response_(getStatusLine(), getHeaders(), String.valueOf(body));
	}

	@Override
	public Response withBody(final Object body, final String contentType) {
		return null;//todo
	}
}
