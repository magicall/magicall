/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Maps;
import me.magicall.NameValue;
import me.magicall.NameValue.NameValue_;
import me.magicall.net.http.ElementsHeader.Element;
import me.magicall.net.http.ElementsHeader.Element.NameValueElement;

import java.util.Map;

/**
 * @author Liang Wenjian.
 */
public class NameValueElement_ extends AbsElement implements NameValueElement {

	private final NameValue<String> nameValue;

	private NameValueElement_(final NameValue<String> nameValue, final Map<String, String> params) {
		super(params);
		this.nameValue = nameValue;
	}

	public NameValueElement_(final String name) {
		this(name, null);
	}

	public NameValueElement_(final String name, final String value) {
		this(new NameValue_<>(name, value), Maps.newHashMap());
	}

	@Override
	public String getValue() {
		return nameValue.val();
	}

	@Override
	public String name() {
		return nameValue.name();
	}

	@Override
	public NameValueElement withValue(final String value) {
		return new NameValueElement_(new NameValue_<>(nameValue.name(), value), getParams());
	}

	@Override
	public Element withParam(final String name, final String value) {
		final Map<String, String> map = Maps.newHashMap(getParams());
		map.put(name, value);
		return new NameValueElement_(nameValue, map);
	}
}
