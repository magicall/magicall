/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.util.Locale;

/**
 * @author Liang Wenjian.
 */
public class AcceptLanguage extends AbsValueElement<Locale> {

	public AcceptLanguage(final Locale locale) {
		super(locale);
	}

	private AcceptLanguage(final ValueElement_<Locale> localeElementsHeader) {
		super(localeElementsHeader);
	}

	@Override
	protected ValueElement<Locale> newInstanceWithRawHeader(final ValueElement_<Locale> localeValueElement) {
		return new AcceptLanguage(localeValueElement);
	}
}
