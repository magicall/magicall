/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.Named;

/**
 * @author Liang Wenjian.
 */
public interface Header<V> extends HttpPart, Named {

	String HEADER_SEPARATOR = Http.LINE_SEPARATOR;
	String WILDCARD = "*/*";

	@Override
	default StringBuilder appendTo(final StringBuilder stringBuilder) {
		stringBuilder.append(name()).append(':');
		final Object value = getValue();
		if (value != null) {
			if (value instanceof HttpPart) {
				((HttpPart) value).appendTo(stringBuilder);
			} else {
				stringBuilder.append(value.toString());
			}
		}
		return stringBuilder.append(HEADER_SEPARATOR);
	}

	V getValue();

	Header<V> withValue(V value);

	default Header<V> clearValue() {
		return withValue(null);
	}
}
