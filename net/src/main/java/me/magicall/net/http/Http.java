/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import me.magicall.computer.Encode;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public interface Http {

	char IP_SEPARATOR_CHARACTER = '.';
	String IP_SEPARATOR = String.valueOf(IP_SEPARATOR_CHARACTER);

	char URI_PARAMETER_SEPARATOR_CHAR = '?';
	String URI_PARAMETER_SEPARATOR = String.valueOf(URI_PARAMETER_SEPARATOR_CHAR);

	char PARAMETER_SEPARATOR_CHAR = '&';
	String PARAMETER_SEPARATOR = String.valueOf(PARAMETER_SEPARATOR_CHAR);

	char URL_SEPARATOR_CHARACTER = '/';
	String URL_SEPARATOR = String.valueOf(URL_SEPARATOR_CHARACTER);

	String LOCALHOST = "localhost";
	String LOCALHOST_IP = "127.0.0.1";
	String HTTP = "http";
	int DEFAULT_HTTP_PORT = 80;
	String HTTPS = "https";
	int DEFAULT_HTTPS_PORT = 443;
	String PROTOCOL_SEPARATOR = "://";
	String LINE_SEPARATOR = "\r\n";

	static boolean isSuccess(final int statusCode) {
		return statusCode / 100 == 2;
	}

	/**
	 * 将字符串解析为一个URL对象。
	 *
	 * @return 若所提供字符串不能解析为URL则返回null。
	 */
	static URL parseOrNull(final String url) {
		try {
			return new URL(url);
		} catch (final MalformedURLException ignored) {
			return null;
		}
	}

	/**
	 * 判断所提供字符串是否合法url。
	 *
	 * @return
	 */
	static boolean isValidUrl(final String toTest) {
		return parseOrNull(toTest) == null;
	}

	/**
	 * 判断所参数是否为根路径。
	 *
	 * @return
	 */
	static boolean isRootPath(final URL url) {
		return URL_SEPARATOR.equals(url.getPath());
	}

	/**
	 * 判断所提供字符串是否为合法url且为根路径。
	 *
	 * @return
	 */
	static boolean isRootPath(final String url) {
		final var parsed = parseOrNull(url);
		return parsed != null && isRootPath(parsed);
	}

	/**
	 * 获取所提供字符串的host。
	 *
	 * @return
	 */
	static String hostOf(final String url) {
		final var parsed = parseOrNull(url);
		return parsed == null ? url : parsed.getHost();
	}

	/**
	 * 获取参数的根域名。
	 *
	 * @return
	 */
	static String rootDomainOf(final URL url) {
		return rootDomainOf0(url.getHost());
	}

	/**
	 * 获取所提供字符串的根域名
	 *
	 * @return
	 */
	static String rootDomainOf(final String url) {
		final var host = hostOf(url);
		return host == null ? null : rootDomainOf0(host);
	}

	/**
	 * 将参数按utf8编码为%xx%xx的格式。
	 *
	 * @return
	 */
	static String encodeUrl(final String toEncode) {
		return encodeUrl(toEncode, Encode.UTF8);
	}

	/**
	 * 按指定的编码将参数编码为%xx%xx的格式。
	 *
	 * @return
	 */
	static String encodeUrl(final String toEncode, final Encode encode) {
		try {
			return URLEncoder.encode(toEncode, encode.getName());
		} catch (final UnsupportedEncodingException ignored) {//impossible
			return null;
		}
	}

	/**
	 * 将参数的%xx%xx按utf8解码。
	 *
	 * @return
	 */
	static String decodeUrl(final String encoded) {
		return decodeUrl(encoded, Encode.UTF8);
	}

	/**
	 * 按指定的编码将参数的%xx%xx解码。
	 *
	 * @return
	 */
	static String decodeUrl(final String encoded, final Encode encode) {
		try {
			return URLDecoder.decode(encoded, encode.getName());
		} catch (final UnsupportedEncodingException ignored) {//impossible
			return null;
		}
	}

	private static String rootDomainOf0(final String host) {
		if (host == null) {
			return null;
		}
		final var lastDot = host.lastIndexOf('.');
		assert lastDot > 0;
		final var lastDot2 = host.lastIndexOf('.', lastDot - 1);
		return host.substring(lastDot2 + 1);
	}

	//static RuntimeException serverErrException(final Status status, final Request req) {
	//	switch (status) {
	//		case NOT_IMPLEMENTED:
	//			return new WrongStatusException("request.uri", req.getUri());
	//		case BAD_GATEWAY:
	//		case SERVICE_UNAVAILABLE:
	//		case GATEWAY_TIMEOUT:
	//			return new NetworkException();
	//		case HTTP_VERSION_NOT_SUPPORTED:
	//			return new WrongArgException("request.httpVersion", req);
	//		case VARIANT_ALSO_NEGOTIATES:
	//			return new WrongStatusException("resource");
	//		case INSUFFICIENT_STORAGE:
	//			return new WrongStatusException("storage");
	//		case LOOP_DETECTED:
	//			return new WrongStatusException("loop", "infinity");
	//		case BANDWIDTH_LIMIT_EXCEEDED:
	//			return new WrongStatusException("bandwidth");
	//		case NOT_EXTENDED:
	//			throw new UnknownException();
	//		case NETWORK_AUTHENTICATION_REQUIRED:
	//			return new OperationNotAvailableException("request", "network");
	//		case INTERNAL_SERVER_ERROR:
	//		default:
	//			return new UnknownException();
	//	}
	//}
}
