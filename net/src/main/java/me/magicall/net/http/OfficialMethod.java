/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Map;

/**
 * 官方的http请求方法。
 *
 * @author Liang Wenjian.
 */
public enum OfficialMethod implements Method {
	GET(false, true),
	HEAD(false, false),
	POST,
	PUT,
	PATCH,
	DELETE(false, true),
	OPTIONS(false, false),
	TRACE;

	private final boolean hasRequestBody;
	private final boolean responseBodyAvailable;

	OfficialMethod() {
		this(true, true);
	}

	OfficialMethod(final boolean hasRequestBody, final boolean responseBodyAvailable) {
		this.hasRequestBody = hasRequestBody;
		this.responseBodyAvailable = responseBodyAvailable;
	}

	@Override
	public boolean hasRequestBody() {
		return hasRequestBody;
	}

	@Override
	public boolean isResponseBodyAvailable() {
		return responseBodyAvailable;
	}

	private static final Map<String, OfficialMethod> NAME_MAP = Maps.newHashMapWithExpectedSize(8);

	static {
		Arrays.stream(values()).forEach(method -> NAME_MAP.put(method.name(), method));
	}

	public static OfficialMethod of(final String name) {
		return NAME_MAP.get(name.toUpperCase());
	}
}
