/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.net.http;

import java.util.Arrays;

public enum HttpVersion implements Version {
	_1_1(1, 1),
	_1_0(1, 0),
	_0_9(0, 9),
	_2_0(2, 0);

	private final int major;
	private final int minor;

	HttpVersion(final int major, final int minor) {
		this.major = major;
		this.minor = minor;
	}

	@Override
	public int getMajor() {
		return major;
	}

	@Override
	public int getMinor() {
		return minor;
	}

	@Override
	public Version to(final int major, final int minor) {
		return Arrays.stream(values()).filter(v -> v.getMajor() == major && v.getMinor() == minor).findFirst().orElse(null);
	}

	@Override
	public String toString() {
		return HttpPart.str(this);
	}
}
