///*
// * Copyright (c) 2024 Liang Wenjian
// * magicall is licensed under Mulan PSL v2.
// * You can use this software according to the terms and conditions of the Mulan PSL v2.
// * You may obtain a copy of Mulan PSL v2 at:
// *          http://license.coscl.org.cn/MulanPSL2
// * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// * See the Mulan PSL v2 for more details.
// */
//
//package me.magicall.web.spring;
//
//import com.google.common.collect.Lists;
//import me.magicall.program.lang.java.贵阳DearSun.ArrKit;
//import me.magicall.abnormal.AbnormalGroup;
//import me.magicall.abnormal.Abnormality;
//import me.magicall.abnormal.Advice;
//import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
//import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
//import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
//import org.springframework.core.annotation.AnnotationUtils;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Objects;
//
///**
// * 处理异常-http响应码的顾问。
// *
// * @author Liang Wenjian.
// */
//@RestControllerAdvice
//public class ExceptionToHttpStatusCodeAdviser {
//
//	@ExceptionHandler(WrongArgException.class)
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	public AbnormalGroup _400(final WrongArgException e) {
//		final List<Abnormality> abnormalities = Lists.newArrayList(//
//						new Abnormality(e.getMessage(), new Advice(e.getArgName(), e.getActualVal(), e.getExpectVal())));
//		handleSuppressed(e, abnormalities);
//		return new AbnormalGroup(abnormalities);
//	}
//
//	//@ExceptionHandler(WrongArgException.class)
//	//@ResponseStatus(HttpStatus.FORBIDDEN)
//	//public void _401() {
//	//}
//
//	@ExceptionHandler(OpNotAllowedException.class)
//	@ResponseStatus(HttpStatus.FORBIDDEN)
//	public AbnormalGroup _403(final OpNotAllowedException e) {
//		final List<Abnormality> abnormalities = Lists.newArrayList(
//				new Abnormality(e.getMessage(), new Advice("operation", e.opName, "right operation."),
//						new Advice("target", e.target, "right target."),
//						new Advice("role", "unknown", "more powerful role.")));
//		handleSuppressed(e, abnormalities);
//		return new AbnormalGroup(abnormalities);
//	}
//
//	@ExceptionHandler(NoSuchThingException.class)
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	public AbnormalGroup _404(final NoSuchThingException e) {
//		final List<Abnormality> abnormalities = Lists.newArrayList(
//				new Abnormality(e.getMessage(), new Advice("thingType", e.getThingType(), "right thing type."),
//						new Advice("thingId", e.getThingId(), "right thing id.")));
//		handleSuppressed(e, abnormalities);
//		return new AbnormalGroup(abnormalities);
//	}
//
//	@ExceptionHandler
//	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)//500
//	public AbnormalGroup _500(final Exception e) throws Exception {
//		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
//			throw e;
//		}
//		final List<Abnormality> abnormalities = Lists.newArrayList(new Abnormality(e.getMessage()));
//		handleSuppressed(e, abnormalities);
//		return new AbnormalGroup(abnormalities);
//	}
//
//	private void handleSuppressed(final Throwable e, final List<Abnormality> abnormalities) {
//		final var suppressed = e.getSuppressed();
//		if (!ArrKit.isEmpty(suppressed)) {
//			Arrays.stream(suppressed).map(this::toAbnormalityGroup).filter(Objects::nonNull).forEach(abnormalities::add);
//		}
//	}
//
//	private Abnormality toAbnormalityGroup(final Throwable throwable) {
//		//todo
//		return null;
//	}
//}
