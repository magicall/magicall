/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 无论是注解 {@link Order}、实现 {@link Ordered} 接口，都把优先级提高到了无注解、未实现接口的bean之上。
 * 本类先实现了 {@link Ordered} 接口，因此天然比没有实现的bean优先级高，但这实现了“互相之间设置优先级为最低”。
 */
public interface CustomResponseBodyAdviser<T> extends ResponseBodyAdvice<T>, Ordered {
	@Override
	default int getOrder() {
		return 0;
	}
}
