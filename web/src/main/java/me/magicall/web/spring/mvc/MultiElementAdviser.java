/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import com.google.common.collect.Maps;
import me.magicall.data.Page;
import me.magicall.program.lang.java.贵阳DearSun.ClassKit;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 将 ResponseBodyAdvice 的功能扩展到多值数据（如 Stream、List 等）中的每一个元素。
 * 当 Controller 返回的是某种多值类型（例如 Stream<T>、List<T>）时，Spring 的 ResponseBodyAdvice 默认只能作用在返回值整体上，而不能逐个处理集合内的元素。
 */
public abstract class MultiElementAdviser<T> implements CustomResponseBodyAdviser<T> {
	protected final List<ResponseBodyAdvice<Object>> advisers;

	public MultiElementAdviser(final List<ResponseBodyAdvice<?>> advisers) {
		this.advisers = (List) advisers;
	}

	@Override
	public boolean supports(final MethodParameter returnType,
													final Class<? extends HttpMessageConverter<?>> converterType) {
		return multiValType().isAssignableFrom(returnType.getParameterType());
	}

	protected abstract Class<?> multiValType();

	private final Map<Class<?>, MethodParameter> methodParameterCache = Maps.newConcurrentMap();

	private MethodParameter getCachedMethodParameter(final Class<?> elementType) {
		return methodParameterCache.computeIfAbsent(elementType, k -> buildElementMethodParameter(elementType));
	}

	@Override
	public T beforeBodyWrite(final T body, final MethodParameter returnType, final MediaType selectedContentType,
													 final Class<? extends HttpMessageConverter<?>> selectedConverterType,
													 final ServerHttpRequest request, final ServerHttpResponse response) {
		if (body == null || advisers.isEmpty()) {
			return body;
		}
		// 获取Stream的泛型类型。通常只有一个类型，但不确定的泛型类型可能有多个上限。
		final Class<?> elementTypes = extractDeepElementType(returnType.getGenericParameterType());
		if (elementTypes == null) {
			return body;
		}
		final Stream<?> stream = bodyToStream(body);
		final var advised = stream.map(
																	e -> tryToAdvise(selectedContentType, selectedConverterType, request, response, e, elementTypes))//
															.filter(Objects::nonNull);
		return streamToReturnType(advised, body);
	}

	private Object tryToAdvise(final MediaType selectedContentType,
														 final Class<? extends HttpMessageConverter<?>> selectedConverterType,
														 final ServerHttpRequest request, final ServerHttpResponse response, final Object element,
														 final Class<?> elementTypes) {
		final var methodParameter = getCachedMethodParameter(elementTypes);
		return advisers.stream()//
									 .filter(adviser -> adviser.supports(methodParameter, selectedConverterType))//
									 .map(adviser -> adviser.beforeBodyWrite(element, methodParameter, selectedContentType,
																													 selectedConverterType, request, response));
	}

	protected abstract Stream<?> bodyToStream(T rawBody);

	protected abstract T streamToReturnType(Stream<Object> advisedBodyAsStream, T rawBody);

	private static Class<?> extractDeepElementType(final Type type) {
		if (type instanceof final ParameterizedType parameterizedType) {
			final Type firstTypeArg = parameterizedType.getActualTypeArguments()[0];
			return extractDeepElementType(firstTypeArg);
		} else if (type instanceof final Class<?> clazz) {
			return clazz;
		} else if (type instanceof final WildcardType wildcardType) {
			final Type[] upperBounds = wildcardType.getUpperBounds();
			if (upperBounds.length > 0) {
				return extractDeepElementType(upperBounds[0]);
			}
		}
		return null; // 无法提取类型
	}

	private static MethodParameter buildElementMethodParameter(final Class<?> elementType) {
		//MethodParameter第一个参数不能为null，随便整个方法给它。
		final var method = ClassKit.methodOf(Date.class, "clone");
		return new MethodParameter(method, -1) {
			@Override
			public Class<?> getParameterType() {
				return elementType;
			}
		};
	}

	//======================================================

	@RestControllerAdvice
	public static class CollectionElementAdviser extends MultiElementAdviser<Collection<?>> {
		//避免跟下面这些兄弟重复处理
		private static final List<Class<?>> CONFLICTING_ADVISER_CLASSES = List.of(ListElementAdviser.class,
																																							SetElementAdviser.class);

		private final List<ResponseBodyAdvice<?>> conflictingAdvisers;

		public CollectionElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
			conflictingAdvisers = advisers.stream()//
																		.filter(e -> CONFLICTING_ADVISER_CLASSES.contains(e.getClass())).toList();
		}

		@Override
		public int getOrder() {
			//避免跟ListElementAdviser和SetElementAdviser重复处理
			return LOWEST_PRECEDENCE;
		}

		@Override
		public boolean supports(final MethodParameter returnType,
														final Class<? extends HttpMessageConverter<?>> converterType) {
			final var superSupports = super.supports(returnType, converterType);
			if (!superSupports) {
				return false;
			}
			return conflictingAdvisers.isEmpty()//
						 || conflictingAdvisers.stream().noneMatch(e -> e.supports(returnType, converterType));
		}

		@Override
		protected Class<?> multiValType() {
			return Collection.class;
		}

		@Override
		protected Stream<?> bodyToStream(final Collection<?> rawBody) {
			return rawBody.stream();
		}

		@Override
		protected Collection<?> streamToReturnType(final Stream<Object> advisedBodyAsStream, final Collection<?> rawBody) {
			return advisedBodyAsStream.toList();
		}
	}

	//------------------------------------------------------

	@RestControllerAdvice
	public static class ListElementAdviser extends MultiElementAdviser<List<?>> {

		public ListElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
		}

		@Override
		protected Class<?> multiValType() {
			return List.class;
		}

		@Override
		protected Stream<?> bodyToStream(final List<?> rawBody) {
			return rawBody.stream();
		}

		@Override
		protected List<?> streamToReturnType(final Stream<Object> advisedBodyAsStream, final List<?> rawBody) {
			return advisedBodyAsStream.toList();
		}
	}

	//---------------------------------------------------------

	@RestControllerAdvice
	public static class SetElementAdviser extends MultiElementAdviser<Set<?>> {

		public SetElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
		}

		@Override
		protected Class<?> multiValType() {
			return Set.class;
		}

		@Override
		protected Stream<?> bodyToStream(final Set<?> rawBody) {
			return rawBody.stream();
		}

		@Override
		protected Set<?> streamToReturnType(final Stream<Object> advisedBodyAsStream, final Set<?> rawBody) {
			return advisedBodyAsStream.collect(Collectors.toSet());
		}
	}

	//------------------------------------------------------

	@RestControllerAdvice
	public static class StreamElementAdviser extends MultiElementAdviser<Stream<?>> {

		public StreamElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
		}

		@Override
		protected Class<?> multiValType() {
			return Stream.class;
		}

		@Override
		protected Stream<?> bodyToStream(final Stream<?> rawBody) {
			return rawBody;
		}

		@Override
		protected Stream<?> streamToReturnType(final Stream<Object> advisedBodyAsStream, final Stream<?> rawBody) {
			return advisedBodyAsStream;
		}
	}

	//-----------------------------------------------------

	@RestControllerAdvice
	public static class ObjArrElementAdviser extends MultiElementAdviser<Object[]> {

		public ObjArrElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
		}

		@Override
		protected Class<?> multiValType() {
			return Object[].class;
		}

		@Override
		protected Stream<?> bodyToStream(final Object[] rawBody) {
			return Stream.of(rawBody);
		}

		@Override
		protected Object[] streamToReturnType(final Stream<Object> advisedBodyAsStream, final Object[] rawBody) {
			return advisedBodyAsStream.toArray();
		}
	}

	//-----------------------------------------------------

	@RestControllerAdvice
	public static class PageElementAdviser extends MultiElementAdviser<Page<?>> {

		public PageElementAdviser(@Lazy final List<ResponseBodyAdvice<?>> advisers) {
			super(advisers);
		}

		@Override
		protected Class<?> multiValType() {
			return Page.class;
		}

		@Override
		protected Stream<?> bodyToStream(final Page<?> rawBody) {
			return rawBody.items().stream();
		}

		@Override
		protected Page<?> streamToReturnType(final Stream<Object> advisedBodyAsStream, final Page<?> rawBody) {
			return Page.of(rawBody.size(), rawBody.curPage(), advisedBodyAsStream, rawBody.allCount());
		}
	}
}
