/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import org.springframework.context.annotation.Primary;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.Objects;
import java.util.stream.Stream;

@Primary
@ControllerAdvice
class DropNullElementInReturningStream implements CustomResponseBodyAdviser<Stream<?>> {
	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}

	@Override
	public boolean supports(final MethodParameter returnType,
													final Class<? extends HttpMessageConverter<?>> converterType) {
		return Stream.class.isAssignableFrom(returnType.getParameterType());
	}

	@Override
	public Stream<?> beforeBodyWrite(final Stream<?> body, final MethodParameter returnType,
																	 final MediaType selectedContentType,
																	 final Class<? extends HttpMessageConverter<?>> selectedConverterType,
																	 final ServerHttpRequest request, final ServerHttpResponse response) {
		if (body == null) {
			return null;
		}
		return body.filter(Objects::nonNull);
	}
}
