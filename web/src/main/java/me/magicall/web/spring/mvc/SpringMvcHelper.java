/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import me.magicall.Entity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.function.Function;

public interface SpringMvcHelper {

	static ResponseEntity<Void> entityWithLocation(final String id) {
		return ResponseEntity.created(
				ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri()).build();
	}

	static <T extends Entity<?, ?>> void addIdToDomainConverter(final FormatterRegistry registry,
																															final Class<T> domainClass,
																															final Converter<String, T> idToDomainConverter) {
		//支持把Controller方法的@PathVariable id段直接转化为domain对象。
		registry.addConverter(String.class, domainClass, idToDomainConverter);
	}

	static <T extends Entity<?, ?>> void addIdToDomainConverter(final FormatterRegistry registry,
																															final Class<T> domainClass,
																															final Function<String, T> idToDomainConverter) {
		addIdToDomainConverter(registry, domainClass, (Converter<String, T>) idToDomainConverter::apply);
	}

	/**
	 * 让jackson可以用接口反序列化。
	 */
	static <I> void jacksonSupportDeserializeInterface(final ObjectMapper objectMapper, final Class<I> interfaceClass,
																										 final Class<? extends I> implClass) {
		final var simpleModule = new SimpleModule();
		simpleModule.addAbstractTypeMapping(interfaceClass, implClass);
		objectMapper.registerModule(simpleModule);
	}

	/**
	 * 让jackson支持jdk8的java.time包。
	 */
	static void jacksonSupportJavaTime(final List<HttpMessageConverter<?>> converters) {
		//让jackson支持jdk8的time包。
		converters.stream().filter(MappingJackson2HttpMessageConverter.class::isInstance)
				.map(converter -> ((MappingJackson2HttpMessageConverter) converter).getObjectMapper()).forEach(objectMapper -> {
					objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
					objectMapper.registerModule(new JavaTimeModule());
				});
	}
}
