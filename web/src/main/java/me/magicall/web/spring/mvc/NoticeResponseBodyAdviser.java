///*
// * Copyright (c) 2024 Liang Wenjian
// * magicall is licensed under Mulan PSL v2.
// * You can use this software according to the terms and conditions of the Mulan PSL v2.
// * You may obtain a copy of Mulan PSL v2 at:
// *          http://license.coscl.org.cn/MulanPSL2
// * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// * See the Mulan PSL v2 for more details.
// */
//
//package me.magicall.web.spring.mvc;
//
//import me.magicall.Op;
//import me.magicall.event.Bulletin;
//import me.magicall.event.Plan;
//import me.magicall.event.PlanPublisher;
//import me.magicall.event.SimpleTarget;
//import org.springframework.core.MethodParameter;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//
//public class NoticeResponseBodyAdviser implements CustomResponseBodyAdviser<Object> {
//	private final PlanPublisher planPublisher;
//	private final Bulletin bulletin;
//
//	public NoticeResponseBodyAdviser(final PlanPublisher planPublisher, final Bulletin bulletin) {
//		this.planPublisher = planPublisher;
//		this.bulletin = bulletin;
//	}
//
//	@Override
//	public boolean supports(final MethodParameter returnType,
//													final Class<? extends HttpMessageConverter<?>> converterType) {
//		return true;
//	}
//
//	@Override
//	public Object beforeBodyWrite(final Object body, final MethodParameter returnType,
//																final MediaType selectedContentType,
//																final Class<? extends HttpMessageConverter<?>> selectedConverterType,
//																final ServerHttpRequest request, final ServerHttpResponse response) {
//		final var plan = Plan.from(this)//
//												 .goingTo(Op.ADD)//
//												 .against(new SimpleTarget<>(this, resourceName(), terminology)).build();
//		planPublisher.publish();
//		return null;
//	}
//
//	@Override
//	public int getOrder() {
//		return CustomResponseBodyAdviser.super.getOrder();
//	}
//}
