/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import cn.hutool.core.io.FileUtil;
import me.magicall.program.lang.java.贵阳DearSun.exception.NullValException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface SpringWebKit {
	static File transfer(final MultipartFile uploadFile) {
		final long size = uploadFile.getSize();
		if (size == 0L) {
			throw new WrongArgException("file`size", 0, ">0");
		}
		final String originalFilename = uploadFile.getOriginalFilename();
		if (originalFilename == null) {
			throw new NullValException("name");
		}
		final String nameWithoutExtension = FileUtil.mainName(originalFilename);
		final String extension = FileUtil.extName(originalFilename);
		final File file;
		try {
			file = File.createTempFile(nameWithoutExtension, '.' + extension);
		} catch (final IOException e) {
			e.printStackTrace();
			throw new UnknownException("创建文件失败。", e);
		}
		try {
			uploadFile.transferTo(file);
		} catch (final IOException e) {
			e.printStackTrace();
			throw new UnknownException(e);
		}
		return file;
	}
}
