/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import org.springframework.web.method.support.HandlerMethodArgumentResolver;

/**
 * 标记接口，以区分自定义的 {@link HandlerMethodArgumentResolver} 和springmvc默认的 {@link HandlerMethodArgumentResolver} 实例。
 */
public interface CustomArgumentResolver extends HandlerMethodArgumentResolver {
}
