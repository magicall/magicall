/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import me.magicall.program.lang.java.贵阳DearSun.exception.NetworkException;
import me.magicall.program.lang.java.贵阳DearSun.exception.NoSuchThingException;
import me.magicall.program.lang.java.贵阳DearSun.exception.OpNotAllowedException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongArgException;
import me.magicall.program.lang.java.贵阳DearSun.exception.WrongStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;
import java.util.Objects;

@RestControllerAdvice
public class ExceptionAdviser {
	private final Log logger = LogFactory.get(getClass());

	@ExceptionHandler(WrongArgException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	Object _400(final WrongArgException e) {
		log(e);
		return Map.of("argName", e.getArgName(),//
				"actualVal", Objects.requireNonNullElse(e.getActualVal(), ""),//
				"expectVal", Objects.requireNonNullElse(e.getExpectVal(), ""));
	}

	@ExceptionHandler(OpNotAllowedException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	Object _403(final OpNotAllowedException e) {
		log(e);
		return Map.of("target", e.target, "operation", e.opName);
	}

	@ExceptionHandler(NoSuchThingException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	Object _404(final NoSuchThingException e) {
		log(e);
		return Map.of("thingType", e.getThingType(), "thingId", Objects.requireNonNullElse(e.getThingId(), ""));
	}

	@ExceptionHandler(WrongStatusException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	Object _409(final WrongStatusException e) {
		log(e);
		return Map.of("statusName", e.getStatusName(),//
				"actualVal", Objects.requireNonNullElse(e.getActualVal(), ""),//
				"expectVal", Objects.requireNonNullElse(e.getExpectVal(), ""));
	}

	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	Object _500(final Throwable e) {
		log(e);
		final var s = HttpStatus.INTERNAL_SERVER_ERROR;
		return s.value() + " " + s.name();
	}

	@ExceptionHandler(NetworkException.class)
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	Object _502(final NetworkException e) {
		log(e);
		return Map.of("url", e.getUrl());
	}

	private void log(final Throwable e) {
		logger.error(e);
	}
}
