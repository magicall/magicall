/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import me.magicall.text.msg.ResponseHeader;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * TODO: 未生效。可能跟返回的Content-Type为json有关，具体未详。暂时不要使用。
 *
 * @author Liang Wenjian
 */
@RestControllerAdvice
public class PutReturnValToHeader implements CustomResponseBodyAdviser<Object> {

	@Override
	public boolean supports(final MethodParameter returnType,
													final Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(final Object body, final MethodParameter returnType,
																final MediaType selectedContentType,
																final Class<? extends HttpMessageConverter<?>> selectedConverterType,
																final ServerHttpRequest request, final ServerHttpResponse response) {
		final var method = returnType.getMethod();
		if (method.isAnnotationPresent(ResponseHeader.class)) {
			final String headerName = method.getAnnotation(ResponseHeader.class).value();
			response.getHeaders().add(headerName, String.valueOf(body));
			return null;
		}
		return body;
	}
}
