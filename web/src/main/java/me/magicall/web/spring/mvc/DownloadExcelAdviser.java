/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import me.magicall.office.excel.Excel;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.springframework.core.MethodParameter;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestControllerAdvice
public class DownloadExcelAdviser implements CustomResponseBodyAdviser<Object> {
	@Override
	public boolean supports(final MethodParameter returnType,
													final Class<? extends HttpMessageConverter<?>> converterType) {
		return Excel.class.isAssignableFrom(returnType.getParameterType());
	}

	@Override
	public Object beforeBodyWrite(final Object returnVal, final MethodParameter returnType,
																final MediaType selectedContentType,
																final Class<? extends HttpMessageConverter<?>> selectedConverterType,
																final ServerHttpRequest request, final ServerHttpResponse response) {
		final Excel excel = (Excel) returnVal;
		final String filename = excel.sheets().findFirst().orElseThrow(UnknownException::new).name() + ".xlsx";
		final HttpHeaders headers = response.getHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDisposition(ContentDisposition.builder("form-data").name("attachment")//
				.filename(filename, StandardCharsets.UTF_8)//必须指定filename的字符集，否则不支持utf8字符。
				.build());

		try {
			excel.writeTo(response.getBody());
		} catch (final IOException e) {
			throw new UnknownException(e);
		}
		return null;
	}
}
