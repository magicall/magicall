/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.spring.mvc;

import me.magicall.Entity;
import me.magicall.biz.EntityCrudServices;
import me.magicall.program.lang.java.贵阳DearSun.exception.EmpStrException;
import me.magicall.program.lang.java.贵阳DearSun.exception.NotNumException;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.stream.Stream;

/**
 * <pre>
 * 对于类似 {@code @RequestMapping("/{id}") } 这种url，本来对应的方法形参是 {@code @PathVariable String id}，只支持基本类型和少数几个常用类型。
 * 本类使 {@code @PathVariable} 的形参能够直接接受实体类对象。
 * 例：
 * <code>{@code
 *  @RequestMapping("/users/{id}")
 *  public User one(@PathVariable User user) {...}
 *  }
 * </code>
 * 开发者继承几个具体子类{@link LongIdEntityPathVarArgResolver}、{@link IntIdEntityPathVarArgResolver}、{@link StrIdEntityPathVarArgResolver}即可。
 * </pre>
 */
public abstract class EntityPathVarArgResolver<_Id, _Entity extends Entity<_Id, ?>> implements CustomArgumentResolver {
	@Override
	public boolean supportsParameter(final MethodParameter parameter) {
		return parameter.hasParameterAnnotation(PathVariable.class)//
					 && supportsType(parameter.getParameterType());
	}

	protected boolean supportsType(final Class<?> type) {
		return supportTypes().anyMatch(e -> e == type);
	}

	protected abstract Stream<Class<?>> supportTypes();

	@Override
	public Object resolveArgument(final MethodParameter parameter, final ModelAndViewContainer mavContainer,
																final NativeWebRequest webRequest, final WebDataBinderFactory binderFactory) {
		final PathVariable pathVariable = parameter.getParameterAnnotation(PathVariable.class);
		if (pathVariable == null) {//不可能到这里，因为supportsParameter保证了存在PathVariable注解
			throw new UnknownException();
		}
		final String id = webRequest.getParameter(pathVariable.value());
		if (id == null || id.isEmpty()) {
			throw new EmpStrException("id");
		}
		return getResourceById(id, parameter);
	}

	protected _Entity getResourceById(final String idStr, final MethodParameter parameter) {
		final var id = transId(idStr);
		return servicesFor(parameter, id).find(id);
	}

	protected abstract EntityCrudServices<_Id, _Entity, ?> servicesFor(MethodParameter parameter, _Id id);

	protected abstract _Id transId(final String idStr);

	//======================================================

	public abstract static class NumIdEntityPathVarArgResolver<_Id extends Number, _Entity extends Entity<_Id, ?>>
			extends EntityPathVarArgResolver<_Id, _Entity> {

		@Override
		protected _Id transId(final String idStr) {
			try {
				return numFromStr(idStr);
			} catch (final NumberFormatException e) {
				throw new NotNumException("id", idStr, e);
			}
		}

		protected abstract _Id numFromStr(final String idStr);
	}

	//-----------------------------------------------------

	public abstract static class IntIdEntityPathVarArgResolver<_Entity extends Entity<Integer, ?>>
			extends NumIdEntityPathVarArgResolver<Integer, _Entity> {

		@Override
		protected Integer numFromStr(final String idStr) {
			return Integer.parseInt(idStr);
		}
	}

	//-----------------------------------------------------

	public abstract static class LongIdEntityPathVarArgResolver<_Entity extends Entity<Long, ?>>
			extends NumIdEntityPathVarArgResolver<Long, _Entity> {

		@Override
		protected Long numFromStr(final String idStr) {
			return Long.parseLong(idStr);
		}
	}

	//-----------------------------------------------------

	public abstract static class StrIdEntityPathVarArgResolver<_Entity extends Entity<String, ?>>
			extends EntityPathVarArgResolver<String, _Entity> {

		@Override
		protected String transId(final String idStr) {
			return idStr;
		}
	}
}
