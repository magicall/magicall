/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.html;

import java.util.ArrayList;
import java.util.List;

/**
 * 一个很简单很简单的html表格,简单到令人发笑!
 *
 * @author Liang Wenjian
 */
public class Table {

	private final List<List<Object>> table;
	private String title;
	private List<Object> curRow;

	public Table() {
		table = new ArrayList<>();
	}

	public Table addRow() {
		curRow = new ArrayList<>();
		table.add(curRow);
		return this;
	}

	public Table addCell(final Object element) {
		curRow.add(element);
		return this;
	}

	@Override
	public String toString() {
		final var sb = new StringBuilder();
		if (title != null) {
			sb.append(title).append("<br/>");
		}
		sb.append("<table border='1'>");
		table.forEach(row -> {
			sb.append("<tr>");
			row.forEach(cell -> sb.append("<td>").append(cell).append("</td>"));
			sb.append("</tr>");
		});
		sb.append("</table>").append("<br/>");
		return sb.toString();
	}

	public String getTitle() {
		return title;
	}

	public Table setTitle(final String title) {
		this.title = title;
		return this;
	}

	public boolean isEmpty() {
		return table.isEmpty();
	}
}
