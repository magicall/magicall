/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.web.util;

import jakarta.servlet.http.HttpServletResponse;
import me.magicall.net.http.Http;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;
import me.magicall.program.lang.java.贵阳DearSun.io.ByteArrayBuffer;
import me.magicall.program.lang.java.贵阳DearSun.io.IOKit;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class UploadDownload {

	/**
	 * 下载文件
	 *
	 * @param absolutePath 文件的绝对路径
	 */
	public static void download(final HttpServletResponse response, final String absolutePath)//
	{
		final var f = new File(absolutePath);
		download(response, f);
	}

	/**
	 *
	 */
	public static void download(final HttpServletResponse response, final File f) {
		if (!f.exists()) {
			throw new RuntimeException("文件已丢失,请联系管理员");
		}
		final var fileShortName = StrKit.subStringAfterLastSeq(f.getAbsolutePath(), File.separator);

		response.setContentType("application/x-msdownload;charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment;filename=" + Http.encodeUrl(fileShortName));
		final var fileLen = f.length();
		response.setHeader("Content-Length", String.valueOf(fileLen));

		final var buffer = new ByteArrayBuffer((int) fileLen);//XXX:file可能过大否?
		IOKit.io(() -> new BufferedInputStream(new FileInputStream(f)), in -> {
			final var bs = new byte[2048];
			for (var read = in.read(bs); read != -1; read = in.read(bs)) {
				buffer.append(bs, 0, read);
			}
		}, e -> {
			if (e instanceof FileNotFoundException) {
				throw new RuntimeException("文件" + fileShortName + "已丢失,请联系管理员。");
			} else {
				throw new RuntimeException("读取文件" + fileShortName + "出错,请稍后再试");
			}
		});

		if (!buffer.isEmpty()) {
			IOKit.io(response::getOutputStream, out -> {
				out.write(buffer.toByteArray());
				out.flush();
			}, e -> {
				throw new RuntimeException("处理文件" + fileShortName + "出错,请稍后再试");
			});
		}
	}
}
