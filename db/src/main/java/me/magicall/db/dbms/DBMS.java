/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.dbms;

import me.magicall.db.util.FieldType;

import java.util.Map;

public interface DBMS {

	FieldType getType(String dbColumnTypeName);

	String getResultColumnNameOfShowCreateTable();

	String getDriverClassName();

	int getDefaultPort();

	String formatUrl(String host, int port, String dbName, Map<String, ?> params);

	String formatUrl(String host, String dbName, Map<String, ?> params);

	String formatUrl(String host, int port, Map<String, ?> params);

	String formatUrl(String host, Map<String, ?> params);
}
