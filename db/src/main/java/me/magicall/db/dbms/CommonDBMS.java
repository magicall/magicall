/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.dbms;

import me.magicall.db.util.FieldType;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum CommonDBMS implements DBMS {
	MYSQL("com.mysql.jdbc.Driver", 3306,//
			"Create Table",//
			new Object[][]{//
					{"binary", FieldType.VARBINARY},//
					{"datetime", FieldType.TIMESTAMP},//
					{"enum", FieldType.CHAR},//
					{"float", FieldType.REAL},//
					{"int", FieldType.INTEGER},//
					//						have no "longblob",
					{"mediumblob", FieldType.BLOB}, {"mediumint", FieldType.INTEGER},//
					{"numeric", FieldType.DECIMAL},//
					{"real", FieldType.DOUBLE},//
					{"set", FieldType.CHAR},//
					{"text", FieldType.VARCHAR}, {"tinyblob", FieldType.BINARY}, {"tinytext", FieldType.VARCHAR},//
					{"year", FieldType.DATE},//
			}),
	//
	;

	private final String driverClassName;
	private final int defaultPort;
	private final Map<String, FieldType> specialDataTypeRef;
	private final String resultColumnNameOfShowCreateTable;

	CommonDBMS(final String driverClassName,//
						 final int defaultPort,//
						 final String showCreateTableColumnName, final Object[]... os) {
		this.driverClassName = driverClassName;
		this.defaultPort = defaultPort;
		resultColumnNameOfShowCreateTable = showCreateTableColumnName;
		specialDataTypeRef = new HashMap<>();
		Arrays.stream(os).forEach(o -> specialDataTypeRef.put((String) o[0], (FieldType) o[1]));
	}

	private static FieldType getTypeFromFieldType(final String dbColumnTypeName) {
		final var types = FieldType.values();
		return Arrays.stream(types).filter(t -> t.name().equalsIgnoreCase(dbColumnTypeName)).findFirst().orElse(null);
	}

	@Override
	public FieldType getType(final String dbColumnTypeName) {
		final var t = specialDataTypeRef.get(dbColumnTypeName.toLowerCase());
		return t == null ? getTypeFromFieldType(dbColumnTypeName) : t;
	}

	@Override
	public String getResultColumnNameOfShowCreateTable() {
		return resultColumnNameOfShowCreateTable;
	}

	@Override
	public String getDriverClassName() {
		return driverClassName;
	}

	@Override
	public int getDefaultPort() {
		return defaultPort;
	}

	@Override
	public String formatUrl(final String host, final int port, final Map<String, ?> params) {
		return formatUrl(host, port, Kits.STR.emptyVal(), params);
	}

	@Override
	public String formatUrl(final String host, final String dbName, final Map<String, ?> params) {
		return formatUrl(host, getDefaultPort(), dbName, params);
	}

	@Override
	public String formatUrl(final String host, final Map<String, ?> params) {
		return formatUrl(host, getDefaultPort(), Kits.STR.emptyVal(), params);
	}

	@Override
	public String formatUrl(final String host, final int port, final String dbName, final Map<String, ?> params) {
		final var sb = new StringBuilder("jdbc:mysql://").append(host).append(':').append(port);
		if (!Kits.STR.isEmpty(dbName)) {
			sb.append('/').append(dbName);
		}
		if (!Kits.MAP.isEmpty(params)) {
			sb.append('?');
			params.entrySet().forEach(e -> sb.append(e.getKey()).append('=').append(e.getValue()).append('&'));
		}
		return sb.toString();
	}
}
