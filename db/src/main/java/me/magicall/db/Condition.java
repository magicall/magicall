/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db;

import com.google.common.collect.Lists;
import me.magicall.program.lang.java.贵阳DearSun.ArrKit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * 条件。
 * 最终会用来构建成where子句。
 * 例子：
 * Condition《Date》 c=new Condition《Date》("startTime",ConditionOperator.BETWEEN, new Date(0L), new Date());
 * 表示 where start_time between '1970-01-01 00:00:00' and now()
 *
 * @author MaGiCalL
 */
public class Condition {

	private String fieldName;

	private ConditionOperator conditionOperator;
	/**
	 * 参考值.
	 */
	private List<Object> refedValues;

	public Condition() {
		super();
	}

	public Condition(final String fieldName, final ConditionOperator conditionOperator, final Object refedValue,
									 final Object... refedValues) {
		this.fieldName = fieldName;
		this.conditionOperator = conditionOperator;
		final List<Object> list = new ArrayList<>();
		if (refedValue == null) {
			list.add(null);
		} else {
			final var castToList = castToList(refedValue);
			list.addAll(castToList);
		}
		Collections.addAll(list, refedValues);
		this.refedValues = list;
	}

	public Condition(final String fieldName, final Object refedValues) {
		this.fieldName = fieldName;
		if ("null".equalsIgnoreCase(String.valueOf(refedValues))) {
			conditionOperator = ConditionOperator.IS;
			this.refedValues = Lists.newArrayList((Object) null);
		} else if ("notnull".equalsIgnoreCase(String.valueOf(refedValues))) {
			conditionOperator = ConditionOperator.IS_NOT;
			this.refedValues = Lists.newArrayList((Object) null);
		} else if (refedValues instanceof Object[]) {
			conditionOperator = ConditionOperator.IN;
			this.refedValues = Lists.newArrayList((Object[]) refedValues);
		} else if (refedValues instanceof Collection<?>) {
			conditionOperator = ConditionOperator.IN;
			this.refedValues = new ArrayList<>((Collection<?>) refedValues);
		} else {
			conditionOperator = ConditionOperator.EQUALS;
			final List<Object> list = new ArrayList<>(1);
			list.add(refedValues);
			this.refedValues = list;
		}
	}

	public Condition(final String fieldName, final Object... refedValues) {
		this.fieldName = fieldName;
		conditionOperator = ConditionOperator.IN;
		this.refedValues = Lists.newArrayList(refedValues);
	}

	public Condition(final String fieldName, final Collection<?> refedValues) {
		this.fieldName = fieldName;
		conditionOperator = ConditionOperator.IN;
		this.refedValues = new ArrayList<>(refedValues);
	}

	public Condition(final String fieldName) {
		this.fieldName = fieldName;
		conditionOperator = ConditionOperator.IS_NOT;
		refedValues = Lists.newArrayList((Object) null);
	}

	private static List<Object> castToList(final Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Collection<?>) {
			return new ArrayList<>((Collection<?>) o);
		}
		if (o.getClass().isArray()) {
			return ArrKit.asList(o);
		}
		if (o instanceof Enumeration<?>) {
			return Collections.list((Enumeration<Object>) o);
		}
		return Lists.newArrayList(o);
	}

	public void setReferModels(final Object... referValues) {
		refedValues = Arrays.asList(referValues);
	}

	public List<?> getRefedValues() {
		return refedValues;
	}

	public void setRefedValues(final List<Object> referValues) {
		refedValues = referValues;
	}

	public ConditionOperator getConditionOperator() {
		return conditionOperator;
	}

	public void setConditionOperator(final ConditionOperator conditionOperator) {
		this.conditionOperator = conditionOperator;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public String toString() {
		return fieldName + ' ' + conditionOperator + ' ' + refedValues;
	}
}
