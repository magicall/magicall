/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db;

import com.google.common.collect.Lists;
import me.magicall.Identified;
import me.magicall.db.util.DbOrder;
import me.magicall.program.lang.java.贵阳DearSun.coll.TwoTuple;

import java.util.List;

/**
 * 只有一个字段参与排序的比较器类。
 *
 * @author MaGiCalL
 */
public class OneFieldComparator<T> extends FieldComparator<T> {
	/**
	 * 根据id字段逆向排序的比较器。
	 */
	public static final FieldComparator<Identified<?>> ID_DESC = new OneFieldComparator<>("id", DbOrder.DESC);
	/**
	 * 根据id字段正向排序的比较器。
	 */
	public static final FieldComparator<Identified<?>> ID_ASC = new OneFieldComparator<>("id", DbOrder.ASC);
	private final List<TwoTuple<String, DbOrder>> list;

	public OneFieldComparator(final String fieldName, final DbOrder dbOrder) {
		list = Lists.newArrayList(new TwoTuple<>(fieldName, dbOrder));
	}

	@Override
	public List<TwoTuple<String, DbOrder>> getComparingFieldsNamesAndOrders() {
		return list;
	}
}
