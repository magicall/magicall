/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.util;

import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.StrKit;

public class PageInfo {

	public static final PageInfo ALL_PAGE_INFO = offsetSize(0, Integer.MAX_VALUE);
	public static final PageInfo NONE_PAGE_INFO = offsetSize(0, 0);
	public static final PageInfo FIRST_ONE = offsetSize(0, 1);
	private PageMode pageMode = PageMode.FROM_1;
	private int page;
	private int size;
	private Integer offset;

	public PageInfo() {
		super();
	}

	public static PageInfo offsetSize(final int offset, final int size) {
		final var pageInfo = new PageInfo();
		pageInfo.setOffset(offset);
		pageInfo.setSize(size);
		return pageInfo;
	}

	public static PageInfo pageWithMode(final PageMode pageMode, final int page, final int size) {
		final var pageInfo = new PageInfo();
		pageInfo.setPage(pageMode, page);
		pageInfo.setSize(size);
		return pageInfo;
	}

	public static PageInfo pageAndSize(final int page, final int size) {
		final var pageInfo = new PageInfo();
		pageInfo.setPage(page);
		pageInfo.setSize(size);
		return pageInfo;
	}

	public int getOffset() {
		if (offset == null) {
			offset = (pageMode == PageMode.FROM_0 ? page : page - 1) * size;
		}
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public int getPage() {
		return page;
	}

	public void setPage(final int page) {
		this.page = page;
	}

	public void setPage(final PageMode pageMode, final int page) {
		this.pageMode = pageMode;
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	public PageMode getPageMode() {
		return pageMode;
	}

	public void setPageMode(final PageMode pageMode) {
		this.pageMode = pageMode;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 1;
		result = prime * result + (offset == null ? 0 : offset.hashCode());
		result = prime * result + page;
		result = prime * result + (pageMode == null ? 0 : pageMode.hashCode());
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final var other = (PageInfo) obj;
		if (offset == null) {
			if (other.offset != null) {
				return false;
			}
		} else if (!offset.equals(other.offset)) {
			return false;
		}
		if (page != other.page) {
			return false;
		}
		if (pageMode != other.pageMode) {
			return false;
		}
		return size == other.size;
	}

	public enum PageMode {
		FROM_0,
		FROM_1;
		public final int firstPage;

		PageMode() {
			final var name = name();
			firstPage = Kits.INT.fromString(StrKit.subStringAfter(name, "_"));
		}

		public int getFirstPage() {
			return firstPage;
		}
	}
}
