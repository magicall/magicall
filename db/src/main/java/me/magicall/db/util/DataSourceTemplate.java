/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.util;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.SQLException;

public abstract class DataSourceTemplate implements DataSource {
	@Override
	public int getLoginTimeout() {
		return 0;
	}

	@Override
	public void setLoginTimeout(final int seconds) {
		throw new UnsupportedOperationException("DataSource#setLoginTimeout not supported.");
	}

	@Override
	public PrintWriter getLogWriter() {
		throw new UnsupportedOperationException("DataSource#getLogWriter not supported.");
	}

	@Override
	public void setLogWriter(final PrintWriter out) {
		throw new UnsupportedOperationException("DataSource#setLogWriter not supported.");
	}

	@Override
	public boolean isWrapperFor(final Class<?> iface) {
		return DataSource.class.equals(iface);
	}

	@Override
	public <T> T unwrap(final Class<T> iface) throws SQLException {
		if (!DataSource.class.equals(iface)) {
			throw new SQLException(
					"DataSource [" + getClass().getName() + "] can't be unwrapped as [" + iface.getName() + "].");
		}
		return iface.cast(this);
	}
}
