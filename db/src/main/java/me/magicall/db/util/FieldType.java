/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.util;

import me.magicall.Identified;
import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.Kit;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

import java.sql.Types;

/**
 * 将java.sql.Types的所有int型的type代理为枚举
 *
 * @author MaGiCalL
 */
public enum FieldType implements Named, Identified<Integer> {
	ARRAY,
	BIGINT(Kits.LONG),
	/**
	 * byte[]
	 */
	BINARY,
	BIT(Kits.BOOL),
	/**
	 * byte[]
	 */
	BLOB,
	BOOLEAN(Kits.BOOL),
	/**
	 * String
	 */
	CHAR(Kits.STR),
	/**
	 * String
	 */
	CLOB,
	DATALINK,
	DATE(Kits.DATE),
	/**
	 * BigDecimal
	 */
	DECIMAL,
	DISTINCT,
	DOUBLE(Kits.DOUBLE),
	FLOAT(Kits.FLOAT),
	INTEGER(Kits.INT),
	JAVA_OBJECT(Kits.OBJ),
	LONGNVARCHAR,
	/**
	 * byte[]
	 */
	LONGVARBINARY,
	/**
	 * byte[]
	 */
	LONGVARCHAR,
	NCHAR,
	NCLOB,
	NULL,
	NUMERIC(Kits.DOUBLE),
	NVARCHAR,
	OTHER(Kits.OBJ),
	REAL(Kits.FLOAT),
	REF,
	ROWID,
	SMALLINT(Kits.INT),
	SQLXML,
	STRUCT,
	TIME(Kits.DATE),
	TIMESTAMP(Kits.DATE),
	TINYINT(Kits.BOOL),
	VARBINARY,
	VARCHAR(Kits.STR);

	public final Kit<?> kit;
	private final int javaSqlTypesConst;

	FieldType() {
		this(Kits.OBJ);
	}

	FieldType(final Kit<?> kit) {
		this.kit = kit;
		var i = 0;//DEFAULT is java.sql.Types.NULL
		try {
			i = Types.class.getField(name()).getInt(null);
		} catch (final IllegalArgumentException | NoSuchFieldException | IllegalAccessException |
									 SecurityException ignored) {
		}
		javaSqlTypesConst = i;
	}

	private int getValue() {
		return javaSqlTypesConst;
	}

	@Override
	public Integer id() {
		return getValue();
	}

	public Kit<?> getKit() {
		return kit;
	}
}
