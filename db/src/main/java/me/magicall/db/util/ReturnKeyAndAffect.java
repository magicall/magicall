/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.util;

public class ReturnKeyAndAffect {
	public static final ReturnKeyAndAffect EMPTY = new ReturnKeyAndAffect(null, 0);

	private final Number returnKey;
	private final int affect;

	public ReturnKeyAndAffect(final Number returnKey, final int affect) {
		super();
		this.returnKey = returnKey;
		this.affect = affect;
	}

	public Number getReturnKey() {
		return returnKey;
	}

	public int getAffect() {
		return affect;
	}

	public boolean success() {
		return affect > 0;
	}

	@Override
	public String toString() {
		return "ReturnKeyAndAffect [returnKey=" + returnKey + ", affect=" + affect + ']';
	}
}
