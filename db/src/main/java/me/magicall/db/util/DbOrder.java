/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.util;

/**
 * order by语句的两个顺序枚举.
 *
 * @author MaGiCalL
 */
public enum DbOrder /* implements SqlElement */ {

	/**
	 * 正序,asc
	 */
	ASC,
	//
	/**
	 * 逆序,desc
	 */
	DESC,
	//
	;

	private final String sql;

	DbOrder() {
		sql = ' ' + name() + ' ';
	}

	//	@Override
	public String toSql() {
		return sql;
	}

	//	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		return sb.append(sql);
	}
}
