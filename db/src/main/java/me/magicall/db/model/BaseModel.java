/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.model;

import me.magicall.Identified;
import me.magicall.program.lang.java.贵阳DearSun.coll.MapWrapper;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseModel<I extends Comparable<I>> //
		implements Identified<I>, Comparable<BaseModel<I>> {

	private BaseModel() {
		//只有本类中的子类能访问这个构造方法，因此保证本类只有下面这几个直接子类
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + '#' + id();
	}

	@Override
	public int hashCode() {
		final var id = id();
		return id == null ? 0 : id.hashCode();
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null) {
			return false;
		}
		if (this == o) {
			return true;
		}
		if (!(o instanceof BaseModel<?>)) {
			return false;
		}
		final var m = (BaseModel<?>) o;
		final Object mid = m.id();
		final var id = id();
		if (id == null || mid == null) {
			return equalsWithoutId(m);
		}
		return id.equals(mid);
	}

	protected boolean equalsWithoutId(final BaseModel<?> o) {
		return false;
	}

	@Override
	public int compareTo(final BaseModel<I> o) {
		return Comparator.<BaseModel<I>>nullsFirst(Comparator.comparing(Identified::id)).compare(this, o);
	}

	//====================================vvv子类vvv

	public static class MapModel<I extends Comparable<I>> extends BaseModel<I> //
			implements MapWrapper<String, Object> {
		private final Map<String, Object> map;

		public MapModel() {
			map = new HashMap<>();
		}

		public MapModel(final Map<String, Object> map) {
			this.map = new HashMap<>(map);
		}

		@SuppressWarnings("unchecked")
		@Override
		public I id() {
			return (I) map.get(idFieldName());
		}

		public void setId(final I id) {
			map.put(idFieldName(), id);
		}

		public void copyField(final MapModel<?> otherMapModel, final String fieldName) {
			set(fieldName, otherMapModel.get(fieldName));
		}

		protected String idFieldName() {
			return "id";
		}

		@Override
		public Object get(final Object key) {
			return map.get(key instanceof Enum ? ((Enum) key).name() : key);
		}

		@SuppressWarnings("unchecked")
		public <T> T get(final String key) {
			return (T) map.get(key);
		}

		public Object set(final String key, final Object value) {
			return put(key, value);
		}

		public Object set(final Enum<?> key, final Object value) {
			return set(key.name(), value);
		}

		@Override
		public String toString() {
			return map.toString();
		}

		@Override
		public Map<String, Object> unwrap() {
			return map;
		}
	}
}
