/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db;

/**
 * 根据底层框架的规则,将值拼接到StringBuilder上.
 * 与底层的框架相关.比如SpringJDBC的NamedParameterJdbcOptions,需要以":"开头.
 *
 * @author MaGiCalL
 */
@FunctionalInterface
public interface SqlValueHandler {

	/**
	 * @param sb 用来装sql语句的StringBuilder
	 * @param fieldName 参数名(数据库列名/model的字段名)
	 * @param refedValueIndex 参数序号
	 * @param refedValue 参数值
	 */
	String handle(final StringBuilder sb, final String fieldName, int refedValueIndex, Object refedValue);
}
