/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import java.util.List;

/**
 * 数据库表的"键"
 *
 * @author MaGiCalL
 */
public class Key extends BaseDbMetaWithMultiColumns {

	private KeyType type;

	public List<DbColumn> getColumns() {
		return unwrap();
	}

	public boolean getUnique() {
		return type != KeyType.COMMON;
	}

	@Override
	public String toString() {
		final var sb = new StringBuilder();
		//		UNIQUE KEY `name` (`name`,`address`),

		final var type = getType();
		if (type != KeyType.COMMON) {
			sb.append(type).append(' ');
		}
		sb.append("KEY ");
		if (type != KeyType.PRIMARY) {
			sb.append('`').append(name()).append('`');
		}
		sb.append('(');
		final var fields = getColumns();
		fields.forEach(f -> sb.append('`').append(f.name()).append("`,"));
		return sb.deleteCharAt(sb.length() - 1).append(')').toString();
	}

	public KeyType getType() {
		return type;
	}

	public void setType(final KeyType type) {
		this.type = type;
	}

	public enum KeyType {
		/**
		 * 主键
		 */
		PRIMARY,
		/**
		 * 唯一键
		 */
		UNIQUE,
		/**
		 * 普通键
		 */
		COMMON
	}
}
