/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import com.google.common.collect.Lists;
import me.magicall.Named;
import me.magicall.db.util.DbUtil;
import me.magicall.program.lang.java.贵阳DearSun.coll.ListWrapper;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class BaseDbMeta implements Named, HasColumns, ListWrapper<DbColumn> {

	protected String name;
	private final List<DbColumn> dbColumns = Lists.newArrayList();

	@Override
	public List<DbColumn> unwrap() {
		return dbColumns;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public BaseDbMeta renameTo(final String name) {
		this.name = name;
		return this;
	}

	@Override
	public DbColumn[] getColumnsArray() {
		final var fields = unwrap();
		return fields.toArray(new DbColumn[0]);
	}

	@Override
	public String toString() {
		return name();
	}

	public String getJavaName() {
		return DbUtil.dbNameToJavaName(name());
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 1;
		result = prime * result + (name == null ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final var other = (BaseDbMeta) obj;
		return Objects.equals(name, other.name);
	}

	public void add(final DbColumn... elements) {
		Collections.addAll(unwrap(), elements);
	}

	public void add(final Collection<? extends DbColumn> elements) {
		unwrap().addAll(elements);
	}

	public boolean add(final int index, final Collection<? extends DbColumn> c) {
		return unwrap().addAll(index, c);
	}

	public boolean add(final int index, final DbColumn... elements) {
		return add(index, Arrays.asList(elements));
	}

	public boolean remove(final Object... targets) {
		return remove(Arrays.asList(targets));
	}

	public boolean remove(final Collection<?> targets) {
		return unwrap().removeAll(targets);
	}
}
