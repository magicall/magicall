/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

public class ForeignKey extends BaseDbMeta {

	private TableMeta referencingTable;
	private DbColumn referencingColumn;
	private TableMeta referencedTable;
	private DbColumn referencedColumn;

	public TableMeta getReferencedTable() {
		return referencedTable;
	}

	public void setReferencedTable(final TableMeta refedTable) {
		referencedTable = refedTable;
		refedTable.getReferencedForeignKeys().add(this);
	}

	public DbColumn getReferencedColumn() {
		return referencedColumn;
	}

	public void setReferencedColumn(final DbColumn refedColumn) {
		referencedColumn = refedColumn;
	}

	public DbColumn getReferencingColumn() {
		return referencingColumn;
	}

	public void setReferencingColumn(final DbColumn column) {
		referencingColumn = column;
	}

	public TableMeta getReferencingTable() {
		return referencingTable;
	}

	public void setReferencingTable(final TableMeta referencingTable) {
		this.referencingTable = referencingTable;
		referencingTable.getForeignKeys().add(this);
	}
}
