/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import me.magicall.Named;
import me.magicall.db.util.DbUtil;

public class FieldMeta implements Named {

	private final DbColumn column;

	private Class<?> type;

	public FieldMeta(final DbColumn column) {
		super();
		this.column = column;
	}

	@Override
	public String name() {
		return DbUtil.dbNameToJavaName(column.name());
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(final Class<?> type) {
		this.type = type;
	}

	public String getComment() {
		return column.getComment();
	}

	public int getLength() {
		return column.getLength();
	}

	public Object getDefaultValue() {
		return column.getDefaultValue();
	}

	public boolean getNullable() {
		return column.getNullable();
	}

	public boolean getHasDefaultValue() {
		return column.getHasDefaultValue();
	}
}
