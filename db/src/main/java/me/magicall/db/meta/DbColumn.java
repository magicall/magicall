/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import me.magicall.Named;
import me.magicall.db.util.FieldType;
import me.magicall.program.lang.java.贵阳DearSun.Kits;

public class DbColumn implements Named, HasComment {

	private String name;
	private FieldType type;
	private int length;
	private Object defaultValue;
	private Boolean hasDefaultValue;
	//	private Collaction collaction;
	private boolean nullable;
	private boolean unsigned;
	private boolean autoInc;
	private boolean zeroFill;
	private String comment;

	public DbColumn() {
		super();
	}

	public DbColumn(final DbColumn other) {
		this();
		setAutoInc(other.getAutoInc());
		setComment(other.getComment());
		setDefaultValue(other.getDefaultValue());
		setHasDefaultValue(other.getHasDefaultValue());
		setLength(other.getLength());
		renameTo(other.name());
		setNullable(other.getNullable());
		setType(other.getType());
		setUnsigned(other.getUnsigned());
		setZeroFill(other.getZeroFill());
	}

	@Override
	public String toString() {
		final var sb = new StringBuilder("`").append(name()).append('`').append(' ');
		sb.append(getType()).append(' ');

		final var len = getLength();
		if (len > 0) {
			sb.append('(').append(len).append(')').append(' ');
		}

		if (getUnsigned()) {
			sb.append("unsigned ");
		}

		if (getZeroFill()) {
			sb.append("zerofill ");
		}

		if (!getNullable()) {
			sb.append("NOT NULL ");
		}

		final var defValue = getDefaultValue();
		if (defValue != null) {
			sb.append(" DEFAULT '").append(defValue).append("' ");
		}

		final var comment = getComment();
		if (!Kits.STR.isEmpty(comment)) {
			sb.append("COMMENT '").append(comment).append("' ");
		}
		return sb.toString();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public DbColumn renameTo(final String name) {
		this.name = name;
		return this;
	}

	@Override
	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public FieldType getType() {
		return type;
	}

	public void setType(final FieldType type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(final int length) {
		this.length = length;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(final Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean getNullable() {
		return nullable;
	}

	public void setNullable(final boolean nullable) {
		this.nullable = nullable;
	}

	public boolean getUnsigned() {
		return unsigned;
	}

	public void setUnsigned(final boolean unsigned) {
		this.unsigned = unsigned;
	}

	public boolean getAutoInc() {
		return autoInc;
	}

	public void setAutoInc(final boolean autoInc) {
		this.autoInc = autoInc;
	}

	public boolean getZeroFill() {
		return zeroFill;
	}

	public void setZeroFill(final boolean zeroFill) {
		this.zeroFill = zeroFill;
	}

	public boolean getHasDefaultValue() {
		return hasDefaultValue == null ? getDefaultValue() != null : hasDefaultValue;
	}

	public void setHasDefaultValue(final boolean hasDefaultValue) {
		this.hasDefaultValue = hasDefaultValue;
	}
}
