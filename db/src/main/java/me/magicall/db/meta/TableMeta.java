/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import me.magicall.db.util.DbUtil;
import me.magicall.program.lang.java.贵阳DearSun.Kits;
import me.magicall.program.lang.java.贵阳DearSun.exception.UnknownException;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

public class TableMeta extends BaseDbMetaWithMultiColumns implements HasComment {
	//	CREATE TABLE `batchtrace` (
	//			  `OutVoucherNO` varchar(12) NOT NULL,
	//			  `NO` smallint(3) NOT NULL,
	//			  `ID` int(11) unsigned NOT NULL,
	//			  `Quantity` double DEFAULT '0',
	//			  `VoucherName` varchar(10) DEFAULT NULL,
	//			  KEY `OutVoucherNO` (`OutVoucherNO`),
	//			  KEY `ID` (`ID`),
	//			  KEY `NO` (`NO`),
	//			  CONSTRAINT `batchtrace_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `kcmx` (`ID`) ON UPDATE CASCADE
	//	CONSTRAINT `tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
	//			) ENGINE=InnoDB DEFAULT CHARSET=utf8
	private String dbName;

	private Key primaryKey;
	private Collection<Key> keys = new LinkedHashSet<>();
	private Collection<ForeignKey> foreignKeys = new LinkedHashSet<>();
	private String defaultCharsetName;

	private String comment;

	//	private Engine engine;

	private Collection<ForeignKey> referencedForeignKeys = new LinkedHashSet<>();

	public TableMeta() {
		super();
	}

	public TableMeta(final String table) {
		super();
		renameTo(table);
	}

	public String getFullName() {
		final var db = getDbName();
		return Kits.STR.isEmpty(db) ? name() : db + '.' + name();
	}

	public DbColumn getColumn(final String columnName) {
		final var columns = getColumns();
		return columns.stream().filter(column -> column.name().equalsIgnoreCase(columnName)).findFirst().orElse(null);
	}

	public List<DbColumn> getColumns() {
		return unwrap();
	}

	public void setFields(final List<DbColumn> fields) {
		getColumns().addAll(fields);
	}

	public String getDefaultCharsetName() {
		return defaultCharsetName;
	}

	public void setDefaultCharsetName(final String defaultCharsetName) {
		this.defaultCharsetName = defaultCharsetName;
	}

	public Collection<Key> getKeys() {
		return keys;
	}

	public void setKeys(final Collection<Key> keys) {
		this.keys = keys;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(final String dbName) {
		this.dbName = dbName;
	}

	@Override
	public String toString() {
		return "TableMeta [" + name() + ']' + getColumns();
	}

	public Key getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(final Key primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Collection<ForeignKey> getForeignKeys() {
		return foreignKeys;
	}

	public void setForeignKeys(final Collection<ForeignKey> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	public Collection<ForeignKey> getReferencedForeignKeys() {
		return referencedForeignKeys;
	}

	public void setReferencedForeignKeys(final Collection<ForeignKey> referencedForeignKeys) {
		this.referencedForeignKeys = referencedForeignKeys;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 0;
		result = prime * result + (dbName == null ? 0 : dbName.hashCode());
		result = prime * result + (name == null ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final var other = (TableMeta) obj;
		if (dbName == null) {
			if (other.dbName != null) {
				return false;
			}
		} else if (!dbName.equals(other.dbName)) {
			return false;
		}
		return Objects.equals(name, other.name);
	}

	@Override
	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public ModelMeta getModelMeta() {
		final var meta = new ModelMeta();
		//TODO setType
		getColumns().forEach(column -> {
			final var fieldMeta = new FieldMeta(column);
			final var fieldType = column.getType();
			fieldMeta.setType(fieldType.getKit().supportedClasses().findFirst().orElseThrow(UnknownException::new));
			meta.add(fieldMeta);
		});
		meta.setComment(getComment());
		meta.setName(DbUtil.dbNameToJavaName(name()));
		return meta;
	}
}
