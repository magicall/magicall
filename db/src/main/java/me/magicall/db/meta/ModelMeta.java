/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db.meta;

import com.google.common.collect.Lists;
import me.magicall.Named;
import me.magicall.program.lang.java.贵阳DearSun.coll.ListWrapper;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ModelMeta implements Named, ListWrapper<FieldMeta> {

	private String name;
	private String comment;
	private final List<FieldMeta> fieldMetas = Lists.newArrayList();

	public List<FieldMeta> getFields() {
		return unwrap();
	}

	public String getComment() {
		return comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	@Override
	public String name() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	@Override
	public List<FieldMeta> unwrap() {
		return fieldMetas;
	}

	public void add(final me.magicall.db.meta.FieldMeta... elements) {
		Collections.addAll(unwrap(), elements);
	}

	public void add(final Collection<? extends FieldMeta> elements) {
		unwrap().addAll(elements);
	}

	public boolean add(final int index, final Collection<? extends FieldMeta> c) {
		return unwrap().addAll(index, c);
	}

	public boolean add(final int index, final me.magicall.db.meta.FieldMeta... elements) {
		return add(index, Arrays.asList(elements));
	}

	public boolean remove(final Object... targets) {
		return remove(Arrays.asList(targets));
	}

	public boolean remove(final Collection<?> targets) {
		return unwrap().removeAll(targets);
	}
}
