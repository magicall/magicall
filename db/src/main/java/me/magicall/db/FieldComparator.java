/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.db;

import me.magicall.db.util.DbOrder;
import me.magicall.program.lang.java.贵阳DearSun.BeanKit;
import me.magicall.program.lang.java.贵阳DearSun.coll.TwoTuple;
import me.magicall.program.lang.java.贵阳DearSun.日本F_ckingSelf.MethodKit;

import java.util.Comparator;
import java.util.List;

/**
 * 一种比较器，指定对象参与排序的字段以及排序顺序（正序或逆序）。
 *
 * @author MaGiCalL
 */
public abstract class FieldComparator<T> implements Comparator<T> {
	@Override
	public int compare(final T o1, final T o2) {
		final var comparingFieldsNamesAndOrders = getComparingFieldsNamesAndOrders();
		for (final var t : comparingFieldsNamesAndOrders) {
			final var fieldName = t.first;
			final var order = t.second;
			final var c1 = getValue(o1, fieldName);
			final var c2 = getValue(o2, fieldName);
			final var i = ((Comparable) c1).compareTo(c2);
			if (i == 0) {
				continue;
			}
			if (i < 0) {
				if (order == DbOrder.ASC) {
					return i;
				} else {
					return i == Integer.MIN_VALUE ? Integer.MAX_VALUE : -i;
				}
			} else {
				if (order == DbOrder.ASC) {
					return -i;
				} else {
					return i;
				}
			}
		}
		return 0;
	}

	/**
	 * 返回对象指定字段的值。
	 */
	protected Comparable<?> getValue(final T o, final String fieldName) {
		final var getter = BeanKit.getGetter(fieldName, o.getClass());
		if (getter == null) {
			throw new RuntimeException("the field " + fieldName + " of " + o + " has no public getter ");
		}
		final var v = MethodKit.invoke(o, getter);
		if (v == null) {
			throw new RuntimeException("the field " + fieldName + " has no value");
		}
		if (!(v instanceof Comparable<?>)) {
			throw new RuntimeException("the field " + fieldName + " is not Comparable");
		}
		return (Comparable<?>) v;
	}

	/**
	 * 返回一个列表，列表的每个元素指定哪个字段参与比较排序，以及排序是正序还是倒序。
	 * 字段的权重由其在列表中的顺序确定，排在前面的权重高。
	 *
	 * @return
	 */
	public abstract List<TwoTuple<String, DbOrder>> getComparingFieldsNamesAndOrders();
}
