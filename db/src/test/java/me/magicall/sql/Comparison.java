/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

public enum Comparison {
	EQUALS(Sql.EQUAL),
	NOT_EQUALS(Sql.NOT_EQUAL),
	GREATER_THAN(Sql.GREATER_THAN),
	GREATER_EQUALS(Sql.GREATER_EQUAL),
	LESS_THAN(Sql.LESS_THAN),
	LESS_EQUALS(Sql.LESS_EQUAL),
	LIKE(Sql.LIKE) {
		@Override
		public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
			return sb.append(' ').append(Sql.LIKE).append("'%").append(vals[0]).append("'%");
		}
	},
	NOT_LIKE(LIKE),
	IN(Sql.IN) {
		@Override
		public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
			var rt = sb.append(' ').append(sign).append('(');
			for (final Object v : vals) {
				rt = SqlKit.appendVal(rt, v).append(Sql.ITEMS_SEPARATOR);
			}
			return SqlKit.removeLastComma(rt);
		}
	},
	NOT_IN(IN),
	BETWEEN(Sql.BETWEEN) {
		@Override
		public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
			var rt = sb.append(' ').append(Sql.BETWEEN).append(' ');
			rt = SqlKit.appendVal(rt, vals[0]).append(' ').append(Sql.AND).append(' ');
			return SqlKit.appendVal(rt, vals[1]);
		}
	},
	IS_NULL(Sql.IS + ' ' + Sql.NULL) {
		@Override
		public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
			return sb.append(' ').append(sign);
		}
	},
	IS_NOT_NULL(IS_NULL) {
		@Override
		public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
			return sb.append(' ').append(Sql.IS).append(' ').append(Sql.NOT).append(' ').append(Sql.NULL);
		}
	};
	final String sign;
	private final Comparison positive;

	Comparison(final char sign) {
		this(String.valueOf(sign));
	}

	Comparison(final String sign) {
		this.sign = sign;
		positive = null;
	}

	Comparison(final Comparison positive) {
		sign = positive.sign;
		this.positive = positive;
	}

	public StringBuilder appendVals(final StringBuilder sb, final Object... vals) {
		if (positive == null) {
			return SqlKit.appendVal(sb.append(' ').append(sign).append(' '), vals[0]);
		} else {
			return positive.appendVals(sb.append(' ').append(Sql.NOT), vals);
		}
	}
}
