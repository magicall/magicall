/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

class SqlKit {

	static StringBuilder removeLastComma(final StringBuilder sb) {
		final var len = sb.length();
		if (len == 0) {
			return sb;
		}
		final var last = len - 1;
		if (sb.charAt(last) == Sql.ITEMS_SEPARATOR) {
			sb.deleteCharAt(last);
		}
		return sb;
	}

	static StringBuilder appendVal(final StringBuilder sb, final Object v) {
		var rt = sb;
		if (v == null) {
			rt.append("null");
		} else if (v instanceof String) {
			rt.append('\'').append((String) v).append('\'');
		} else if (v instanceof Float) {
			rt.append(v);
		} else if (v instanceof Double) {
			rt.append(v);
		} else if (v instanceof Number) {
			rt.append(((Number) v).longValue());
		} else if (v instanceof Col) {
			rt = ((Col) v).appendTo(rt);
		}
		return rt;
	}
}
