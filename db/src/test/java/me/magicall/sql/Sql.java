/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import java.util.Map;

public interface Sql {
	char WILDCARD_1 = '_', WILDCARD_N = '%';
	char ITEMS_SEPARATOR = ',';
	char ALL = '*';
	char EQUAL = '=', GREATER_THAN = '>', LESS_THAN = '<';
	String NOT_EQUAL = "<>", GREATER_EQUAL = ">=", LESS_EQUAL = "<=";
	String AND = "AND", OR = "OR", NOT = "NOT", IS = "IS", NULL = "NULL";
	String IN = "IN", BETWEEN = "BETWEEN", LIKE = "LIKE";
	String SELECT = "SELECT", INSERT = "INSERT", DELETE = "DELETE", UPDATE = "UPDATE",//
			FROM = "FROM", INTO = "INTO", JOIN = "JOIN", SET = "SET",//
			VALUE = "VALUE", VALUES = "VALUES", IGNORE = "IGNORE";
	String AS = "AS";
	String LEFT = "LEFT", RIGHT = "RIGHT", INNER = "INNER", OUTER = "OUTER";
	String DATABASE = "DATABASE", TABLE = "TABLE", CHARSET = "CHARSET", COLUMN = "COLUMN", PRIMARY = "PRIMARY",//
			KEY = "KEY", INDEX = "INDEX", UNIQUE = "UNIQUE";
	String CREATE = "CREATE", DEFAULT = "DEFAULT";

	static Table table(final String name) {
		return Table.of(name);
	}

	static Select select(final Col... cols) {
		return new Select(Lists.newArrayList(cols));
	}

	static Select selectAllFrom(final Table... tables) {
		return new Select(tables);
	}

	static Select countFrom(final Table... tables) {
		return new Select("count(*)", tables);
	}

	static Insert insert(final Multimap<Col, Object> assignments) {
		return new Insert(assignments);
	}

	static Insert insertIgnore(final Multimap<Col, Object> assignments) {
		return new Insert(assignments, true);
	}

	static Insert insert(final Map<Col, Object> assignments) {
		return insert0(assignments, false);
	}

	static Insert insertIgnore(final Map<Col, Object> assignments) {
		return insert0(assignments, true);
	}

	private static Insert insert0(final Map<Col, Object> assignments, final boolean ignore) {
		final var size = assignments.size();
		final Multimap<Col, Object> multimap = ArrayListMultimap.create(size, size);
		assignments.forEach(multimap::put);
		return new Insert(multimap, ignore);
	}

	static Update update(final Map<Col, Object> assignments) {
		return new Update(assignments);
	}

	static Delete deleteFrom(final Table table) {
		return new Delete(table);
	}
}
