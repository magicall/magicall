/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

public class LimitWithOffset extends Limit {
	private int offset;

	LimitWithOffset(final SqlPart preState, final int count) {
		super(preState, count);
	}

	public LimitWithOffset(final SqlPart preState, final int count, final int offset) {
		super(preState, count);
		this.offset = offset;
	}

	public LimitWithOffset offset(final int offset) {
		this.offset = offset;
		return this;
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		return super.appendTo(sb).append(',').append(offset);
	}
}
