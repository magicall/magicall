/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Map.Entry;

public class OrderBy implements SqlPart, CanLimit {

	public enum Order {
		ASC,
		DESC
	}

	private final SqlPart preState;
	private final Map<Col, Order> colOrders = Maps.newLinkedHashMap();

	OrderBy(final SqlPart preState, final Col col, final Order order) {
		this.preState = preState;
		colOrders.put(col, order);
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		var rt = preState.appendTo(sb);
		if (colOrders.isEmpty()) {
			return rt;
		}
		for (final Entry<Col, Order> entry : colOrders.entrySet()) {
			rt = entry.getKey().appendTo(rt);
			final var order = entry.getValue();
			if (order != null) {
				rt.append(order);
			}
			rt.append(Sql.ITEMS_SEPARATOR);
		}
		return SqlKit.removeLastComma(rt);
	}

	public OrderBy and(final Col otherCol, final Order order) {
		colOrders.put(otherCol, order);
		return this;
	}

	public OrderBy and(final Col otherCol) {
		return and(otherCol, null);
	}
}
