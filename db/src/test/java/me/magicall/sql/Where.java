/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

public class Where implements SqlPart, CanOrder, CanLimit {
	private final OperatingTable operatingTable;
	private final List<WhereCondition> conditions = Lists.newLinkedList();

	Where(final OperatingTable operatingTable, final Col col, final Comparison comparison, final Object... vals) {
		this.operatingTable = operatingTable;
		conditions.add(new WhereCondition(col, comparison, vals));
	}

	public Where and(final Col col, final Comparison comparison, final Object... vals) {
		final var isOr = false;
		return addCondition(isOr, col, comparison, vals);
	}

	public Where and(final Col col, final Object eqVal) {
		return and(col, Comparison.EQUALS, eqVal);
	}

	public Where and(final Map<Col, Object> eqConditions) {
		return eqConditions.entrySet().stream().reduce(this,//
				(where, entry) -> where.and(entry.getKey(), entry.getValue()), (a, b) -> b);
	}

	public Where or(final Col col, final Comparison comparison, final Object... vals) {
		return addCondition(true, col, comparison, vals);
	}

	public Where or(final Col col, final Object eqVal) {
		return or(col, Comparison.EQUALS, eqVal);
	}

	public Where or(final Map<Col, Object> eqConditions) {
		return eqConditions.entrySet().stream().reduce(this,//
				(where, entry) -> where.or(entry.getKey(), entry.getValue()), (a, b) -> b);
	}

	private Where addCondition(final boolean isOr, final Col col, final Comparison comparison, final Object... vals) {
		conditions.add(new WhereCondition(isOr, col, comparison, vals));
		return this;
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		var rt = operatingTable.appendTo(sb);
		if (conditions.isEmpty()) {
			return rt;
		}
		rt.append(" WHERE ");
		for (final WhereCondition condition : conditions) {
			rt = condition.appendTo(rt);
		}
		return rt;
	}
}
