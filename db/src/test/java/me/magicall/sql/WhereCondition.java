/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

public class WhereCondition implements SqlPart {
	private final Boolean isOr;
	private final Col col;
	private final Comparison comparison;
	private final Object[] vals;

	WhereCondition(final Col col, final Comparison comparison, final Object... vals) {
		this(null, col, comparison, vals);
	}

	WhereCondition(final Boolean isOr, final Col col, final Comparison comparison, final Object... vals) {
		this.isOr = isOr;
		this.col = col;
		this.comparison = comparison;
		this.vals = vals;
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		if (isOr != null) {
			sb.append(' ').append(isOr ? "OR" : "AND").append(' ');
		}
		return comparison.appendVals(col.appendTo(sb), vals);
	}
}
