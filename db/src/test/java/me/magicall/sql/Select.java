/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import com.google.common.collect.Lists;
import me.magicall.sql.OrderBy.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Select extends OperatingTable implements CanLimitWithOffset {
	private final List<Object> selectings = Lists.newLinkedList();

	Select(final List<Col> cols) {
		super(cols.stream().map(Col::table).collect(Collectors.toList()));
		selectings.addAll(cols);
	}

	Select(final Table... tables) {
		super(tables);
	}

	Select(final String s, final Table... tables) {
		super(tables);
		selectings.add(s);
	}

	@Override
	public SelectWhere where(final Map<Col, Object> eqConditions) {
		return (SelectWhere) super.where(eqConditions);
	}

	@Override
	public SelectWhere where(final Col col, final Object eqVal) {
		return (SelectWhere) super.where(col, eqVal);
	}

	@Override
	public SelectWhere where(final Col col, final Comparison comparison, final Object... vals) {
		return new SelectWhere(this, col, comparison, vals);
	}

	@Override
	public SelectOrderBy orderBy(final Col col, final Order order) {
		return new SelectOrderBy(this, col, order);
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		var rt = sb.append("SELECT ");
		for (final Object o : selectings) {
			if (o instanceof String) {
				rt.append((String) o);
			} else if (o instanceof Col) {
				rt = ((Col) o).appendTo(rt);
			}
			rt.append(Sql.ITEMS_SEPARATOR);
		}

		SqlKit.removeLastComma(rt).append(" FROM ");
		return appendTables(rt);
	}
}
