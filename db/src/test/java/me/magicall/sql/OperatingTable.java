/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class OperatingTable implements SqlPart, CanOrder, CanLimit {
	private final List<Table> tables;

	OperatingTable(final List<Table> tables) {
		this.tables = tables;
	}

	OperatingTable(final Table... tables) {
		this(Lists.newArrayList(tables));
	}

	public OperatingTable innerJoin(Col col1, Col col2) {
		return null;
	}

	public OperatingTable leftJoin(Col col1, Col col2) {
		return null;
	}

	public OperatingTable rightJoin(Col col1, Col col2) {
		return null;
	}

	public Where where(final Map<Col, Object> eqConditions) {
		Where rt = null;
		for (final Entry<Col, Object> entry : eqConditions.entrySet()) {
			if (rt == null) {
				rt = where(entry.getKey(), entry.getValue());
			} else {
				rt = rt.and(entry.getKey(), entry.getValue());
			}
		}
		return rt;
	}

	public Where where(final Col col, final Object eqVal) {
		return where(col, Comparison.EQUALS, eqVal);
	}

	public Where where(final Col col, final Comparison comparison, final Object... vals) {
		return new Where(this, col, comparison, vals);
	}

	StringBuilder appendTables(final StringBuilder rt) {
		return null;
	}
}
