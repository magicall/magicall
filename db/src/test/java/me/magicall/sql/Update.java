/*
 * Copyright (c) 2024 Liang Wenjian
 * magicall is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package me.magicall.sql;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Update extends OperatingTable {
	private final Map<Col, Object> assignments;

	Update(final Map<Col, Object> assignments) {
		super(assignments.keySet().stream().map(Col::table).collect(Collectors.toList()));
		this.assignments = assignments;
	}

	@Override
	public StringBuilder appendTo(final StringBuilder sb) {
		var rt = sb.append("UPDATE ");
		rt = appendTables(rt);
		rt.append(" SET ");
		for (final Entry<Col, Object> entry : assignments.entrySet()) {
			rt = entry.getKey().appendTo(rt).append(Sql.EQUAL);
			rt = SqlKit.appendVal(rt, entry.getValue()).append(Sql.ITEMS_SEPARATOR);
		}
		return SqlKit.removeLastComma(rt);
	}
}
